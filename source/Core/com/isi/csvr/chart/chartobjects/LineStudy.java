package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Point2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 9:25:47 AM
 */
public class LineStudy extends AbstractObject implements Extendible, TextContainer {
    private static final long serialVersionUID = 1072600473330203900L;

    //for text area / rectangle
    protected String text;
    protected Font font;
    protected boolean transparent;
    protected Color fillColor;
    protected Color fontColor;

    protected boolean extendedLeft;
    protected boolean extendedRight;
    protected int channelUnits; // Pramoda (2006-03-02)
    protected boolean isShowBorder;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();
    private SimpleDateFormat timeFormatter = null;
    private Font fibFont = new Font("Arial", Font.BOLD | Font.ITALIC, 9);

    public LineStudy() {
        super();
        text = null;
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        transparent = true;
        fillColor = new Color(240, 240, 230);
        fontColor = StockGraph.fontColor;
        extendedLeft = false;
        extendedRight = false;
        channelUnits = 2;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        isShowBorder = true;
    }

    public LineStudy(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        text = null;
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        transparent = true;
        fillColor = new Color(240, 240, 230);
        fontColor = StockGraph.fontColor;
        extendedLeft = false;
        extendedRight = false;
        channelUnits = 2;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        isShowBorder = true;
    }

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public void setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
    }

    public void assignValuesFrom(AbstractObject or) {
        super.assignValuesFrom(or);
        if (or instanceof LineStudy) {
            LineStudy ls = (LineStudy) or;
            text = ls.text;
            font = ls.font;
            transparent = ls.transparent;
            fontColor = ls.fontColor;
            fillColor = ls.fillColor;
            extendedLeft = ls.extendedLeft;
            extendedRight = ls.extendedRight;
            channelUnits = ls.channelUnits; // Pramoda - (2006-03-02)
            yArray = ls.yArray;
            xArray = ls.xArray;

            isShowBorder = ls.isShowBorder();//shashika
        }
    }

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);
        int x1, x2, y1, y2, x, y;
        g.setClip(rect);
        try {
            switch (objType) {
                case INT_LINE_HORIZ:
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    g.drawLine(rect.x, y1 - (int) Yadj, rect.x + rect.width, y1 - (int) Yadj);
                    break;
                case INT_LINE_VERTI:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    g.drawLine(x1 - Xadj, rect.y, x1 - Xadj, rect.y + rect.height);
                    break;
                case INT_FIBONACCI_ARCS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - (int) Yadj;
                        y2 = y2 - (int) Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - (int) Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - (int) Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    float len;
                    int r1, r2, r3, start;
                    len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                    r1 = Math.round(len * 0.618f);
                    r2 = Math.round(len * 0.5f);
                    r3 = Math.round(len * 0.382f);
                    int xP = x2;
                    int yP = y2;
                    start = (y2 > y1) ? 0 : 180;
                    g.drawArc(xP - r1, yP - r1, 2 * r1, 2 * r1, start, 180);
                    g.drawArc(xP - r2, yP - r2, 2 * r2, 2 * r2, start, 180);
                    g.drawArc(xP - r3, yP - r3, 2 * r3, 2 * r3, start, 180);

                    //adding tooltip data rows
                    dataRows.clear();

                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    float index1 = bIndex + (x1 - grfLeft) / xFactor;
                    float index2 = bIndex + (x2 - grfLeft) / xFactor;

                    long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
                    Date D1 = null, D2 = null;
                    D1 = new Date(newXarray[0]);
                    if (newXarray.length > 1) {
                        D2 = new Date(newXarray[1]);
                    }

                    String beginDate = timeFormatter.format(D1);
                    String endDate = timeFormatter.format(D2);

                    double begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
                    double end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
                    String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
                    String endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
                    dataRows.add(new ToolTipDataRow("38.20%", graph.GDMgr.formatPriceField(begin + (end - begin) * .382, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("50.00%", graph.GDMgr.formatPriceField(begin + (end - begin) * .5, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("61.80%", graph.GDMgr.formatPriceField(begin + (end - begin) * .618, ((WindowPanel) rect).isInThousands())));

                    break;
                case INT_FIBONACCI_FANS:
                    g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
                    float ratio;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - (int) Yadj;
                        y2 = y2 - (int) Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - (int) Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - (int) Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    if ((x1 == x2)) {
                        x = x1;
                        y = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / ((float) x2 - x1);
                        if (x1 > x2) {
                            x1 = x2;
                            y1 = y2;
                        }
                        x = x1 + grfWidth;
                        y = y1 + Math.round(ratio * grfWidth);
                    }
                    int yP1, yP2, yP3;
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    g.drawLine(x1, y1, x, yP1);
                    g.drawLine(x1, y1, x, yP2);
                    g.drawLine(x1, y1, x, yP3);

                    //adding tooltip data rows
                    dataRows.clear();

                    index1 = bIndex + (x1 - grfLeft) / xFactor;
                    index2 = bIndex + (x2 - grfLeft) / xFactor;

                    newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
                    D1 = null;
                    D2 = null;
                    D1 = new Date(newXarray[0]);
                    if (newXarray.length > 1) {
                        D2 = new Date(newXarray[1]);
                    }

                    beginDate = timeFormatter.format(D1);
                    endDate = timeFormatter.format(D2);

                    begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
                    end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
                    beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
                    endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
                    dataRows.add(new ToolTipDataRow("38.20%", graph.GDMgr.formatPriceField(begin + (end - begin) * .382, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("50.00%", graph.GDMgr.formatPriceField(begin + (end - begin) * .5, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("61.80%", graph.GDMgr.formatPriceField(begin + (end - begin) * .618, ((WindowPanel) rect).isInThousands())));

                    break;
                case INT_FIBONACCI_RETRACEMENTS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - (int) Yadj;
                        y2 = y2 - (int) Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - (int) Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - (int) Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    if ((x1 != x2)) {
                        if (x1 > x2) {
                            x1 = x2;
                            x2 = y1; //used as a tmp var
                            y1 = y2;
                            y2 = x2;
                        }
                        len = (y2 - y1);
                        float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f};
                        for (int i = 0; i < fibArr.length; i++) {
                            y = y2 - Math.round(fibArr[i] * len);
                            g.drawLine(x1, y, x1 + grfWidth, y);
                        }
                        g.drawLine(x1, y1, x1 + grfWidth, y1);
                        g.drawLine(x1, y2, x1 + grfWidth, y2);
                    }


                    break;
                case INT_FIBONACCI_ZONES:
                    y1 = grf_Top;
                    y2 = grf_Top + grf_Ht;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft - Xadj;
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    g.drawLine(x1, y1, x1, y2);
                    while (true) {
                        sum += curVal;
                        //System.out.println("���� writing fibonaaci value "+sum);
                        x = x1 + Math.round(sum * xFactor);
                        if (x > grfLeft + grfWidth) break;
                        g.drawLine(x, y1, x, y2);
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    break;
                case INT_SYMBOL:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    x1 = x1 - Xadj;
                    y1 = y1 - (int) Yadj;
                    g.drawRect(x1 - 10, y1 - 10, 20, 20);

                    //constructing tool tip data
                    dataRows.clear();
                    boolean currentMode = false; //TODO :
                    if (currentMode) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }


                    index1 = bIndex + (x1 - grfLeft) / xFactor;


                    newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1});
                    D1 = new Date(newXarray[0]);
                    beginDate = timeFormatter.format(D1);

                    begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
                    beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));

                    dataRows.add(new ToolTipDataRow(Language.getString("DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_TOOL_TIP_VALUE"), beginPrice));

                    break;
                case INT_TEXT:
                case INT_RECT:
                case INT_RECT_SELECTION:
                case INT_ELLIPSE:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - (int) Yadj;
                        y2 = y2 - (int) Yadj;
                    } else {
                        switch (movingPtIndex) {
                            case 0:
                                x1 = x1 - Xadj;
                                y1 = y1 - (int) Yadj;
                                break;
                            case 1:
                                x2 = x2 - Xadj;
                                y2 = y2 - (int) Yadj;
                                break;
                            case 2:
                                x1 = x1 - Xadj;
                                y2 = y2 - (int) Yadj;
                                break;
                            case 3:
                                x2 = x2 - Xadj;
                                y1 = y1 - (int) Yadj;
                                break;
                        }
                    }
                    if (objType == INT_ELLIPSE) {
                        g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    } else {
                        g.drawRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    }

                    //adding tooltip data rows
                    dataRows.clear();
                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    index1 = bIndex + (x1 - grfLeft) / xFactor;
                    index2 = bIndex + (x2 - grfLeft) / xFactor;

                    newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
                    D1 = null;
                    D2 = null;
                    D1 = new Date(newXarray[0]);
                    if (newXarray.length > 1) {
                        D2 = new Date(newXarray[1]);
                    }
                    beginDate = timeFormatter.format(D1);
                    endDate = timeFormatter.format(D2);

                    begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
                    end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
                    beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
                    endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_PRICE"), beginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_PRICE"), endPrice));

                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
        }
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        int x1, x2, y1, y2;
        float len;
        int r1, r2, r3, start;
        int extendX = 1000000;
        Graphics2D g = (Graphics2D) gg;
        boolean drawSelected = selected && !isPrinting;
        int x = 0, y = 0;

        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        try {
            switch (objType) {
                case INT_LINE_HORIZ:

                    if (!isVisible) return;
                    g.setFont(new Font("Arial", Font.BOLD, 10));
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);

                    //constructing the yvalue for print in both ends - added by charithn
                    String yValue = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
                    int valueWidth = g.getFontMetrics().stringWidth(yValue);

                    //draw y value in both ends
                    int adj = 5;
                    g.drawString(yValue, rect.x + adj, y1 - 2);
                    g.drawString(yValue, rect.x + rect.width - valueWidth - adj, y1 - 2);

                    //drawing the horizontal line
                    g.drawLine(rect.x, y1, rect.x + rect.width, y1);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(rect.x + rect.width / 2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(rect.x + rect.width / 2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_LINE_VERTI:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    g.drawLine(x1, rect.y, x1, rect.y + rect.height);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_SYMBOL:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    // g.setColor(color);//changte by sathyajith to make int_symbols themable
                    g.setFont(font);
                    FontMetrics fmx = g.getFontMetrics();
                    Point pt = getAlignedPosition(ALIGNMENT_CENTER, 20, 20,
                            fmx.stringWidth(text), fmx.getHeight(), fmx.getAscent());
                    g.drawString(text, x1 - 10 + pt.x, y1 - 10 + pt.y);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 + 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 + 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 + 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 + 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_TEXT:
                case INT_RECT:
                    //case INT_RECT_SELECTION:
                    int[] xPBorder = null;
                    int[] yPBorder = null;
                    Polygon poly = null;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (objType == INT_RECT || objType == INT_RECT_SELECTION) {
                        if (!transparent) {
                            g.setColor(fillColor);
                            g.fillRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                        }
                        g.setColor(color);
                        g.drawRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    } else {
                        int maxEX = Math.max(x1, x2);
                        int maxWY = Math.max(y1, y2);
                        int minEX = Math.min(x1, x2);
                        int minWY = Math.min(y1, y2);
                        int midX = Math.max(maxEX - 10, (x1 + x2) / 2);
                        int midY = Math.min(minWY + 10, (y1 + y2) / 2);
                        int[] xP = {minEX, minEX, maxEX, maxEX, midX};
                        int[] yP = {minWY, maxWY, maxWY, midY, minWY};
                        int[] xPB = {midX, midX, maxEX};
                        int[] yPB = {minWY, midY, midY};
                        xPBorder = xPB;
                        yPBorder = yPB;
                        poly = new Polygon(xP, yP, 5);
                        if (!transparent) {
                            g.setColor(fillColor);
                            g.fillPolygon(xP, yP, 5);
                        }
                        g.setColor(color);
                        //g.drawPolygon(xP, yP, 5);

                        if (isShowBorder) {
                            g.drawPolygon(xP, yP, 5);
                        }
                    }
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    //adding tooltip data rows
                    dataRows.clear();

                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    Date D1 = null;
                    Date D2 = null;
                    D1 = new Date(xArray[0]);
                    if (xArray.length > 1) {
                        D2 = new Date(xArray[1]);
                    }
                    String beginDate = timeFormatter.format(D1);
                    String endDate = timeFormatter.format(D2);
                    String beginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
                    String endPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());

                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_PRICE"), beginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_PRICE"), endPrice));

                    if (objType == INT_RECT || objType == INT_RECT_SELECTION) break;
                    Rectangle rectClip = new Rectangle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    if (rect.contains(rectClip)) {
                        g.setClip(poly);
                    } else {
                        g.setClip(rect.intersection(rectClip));
                    }
                    String[] sa = extractLines(text);
                    g.setColor(fontColor);
                    g.setFont(font);

                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    for (int i = 0; i < sa.length; i++) {
                        g.drawString(sa[i], Math.min(x1, x2) + 4, Math.min(y1, y2) + 5 * (i + 1) + g.getFontMetrics().getHeight());
                        //g.drawString(sa[i], Math.min(x1, x2) + 4, Math.min(y1, y2) + 15 * (i + 1) + 3);
                    }
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    if (!transparent) {
                        g.setColor(fillColor);
                        g.fillPolygon(xPBorder, yPBorder, 3);
                    }
                    g.setColor(color);
                    //g.drawPolygon(xPBorder, yPBorder, 3);
                    if (isShowBorder) {
                        g.drawPolygon(xPBorder, yPBorder, 3);
                    }
                    break;
                case INT_ELLIPSE:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (!transparent) {
                        g.setColor(fillColor);
                        g.fillOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    }
                    g.setColor(color);
                    g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    //adding tooltip data rows
                    dataRows.clear();

                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    D2 = null;
                    D1 = new Date(xArray[0]);
                    if (xArray.length > 1) {
                        D2 = new Date(xArray[1]);
                    }
                    beginDate = timeFormatter.format(D1);
                    endDate = timeFormatter.format(D2);
                    beginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
                    endPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());

                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_BEGIN_PRICE"), beginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("LINE_END_PRICE"), endPrice));
                    break;
                case INT_FIBONACCI_ARCS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                    r1 = Math.round(len * 0.618f);
                    r2 = Math.round(len * 0.5f);
                    r3 = Math.round(len * 0.382f);
                    start = (y2 > y1) ? 0 : 180;
                    g.drawArc(x2 - r1, y2 - r1, 2 * r1, 2 * r1, start, 180);
                    g.drawArc(x2 - r2, y2 - r2, 2 * r2, 2 * r2, start, 180);
                    g.drawArc(x2 - r3, y2 - r3, 2 * r3, 2 * r3, start, 180);
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

                    }
                    //adding tooltip data rows
                    dataRows.clear();

                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    D1 = new Date(xArray[0]);
                    D2 = null;
                    if (xArray.length > 1) {
                        D2 = new Date(xArray[1]);
                    }

                    String trendBeginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
                    String trendEndPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());
                    beginDate = timeFormatter.format(D1);
                    endDate = timeFormatter.format(D2);

                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), trendBeginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), trendEndPrice));
                    dataRows.add(new ToolTipDataRow("38.20%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .382, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("50.00%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .5, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("61.80%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .618, ((WindowPanel) rect).isInThousands())));
                    //dataRows.add(new ToolTipDataRow("100.00%", GraphDataManager.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * 1, ((WindowPanel) rect).isInThousands())));
                    //dataRows.add(new ToolTipDataRow("138.20%", GraphDataManager.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * 1.382, ((WindowPanel) rect).isInThousands())));
                    //dataRows.add(new ToolTipDataRow("161.80%", GraphDataManager.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * 1.618, ((WindowPanel) rect).isInThousands())));

                    break;
                case INT_FIBONACCI_FANS:
                    float ratio = 1f;
                    if (xArr[0] < xArr[1]) {
                        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    } else {
                        x1 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                        y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    }

                    if ((x1 == x2)) {
                        x = x1;
                        y = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / ((float) x2 - x1);
                        int pixGap = (x2 > x1) ? rect.width : -rect.width;
                        x = x1 + pixGap;
                        y = y1 + Math.round(ratio * pixGap);
                    }
                    int yP1, yP2, yP3;
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    g.drawLine(x1, y1, x, yP1);
                    g.drawLine(x1, y1, x, yP2);
                    g.drawLine(x1, y1, x, yP3);
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }

                    //drawing angles
                    g.setFont(fibFont);
                    g.setColor(color);
                    float pctLength1 = -((float) (y2 - y1)) * 0.382f;
                    float pctLength2 = -((float) (y2 - y1)) * 0.5f;
                    float pctLength3 = -((float) (y2 - y1)) * 0.618f;

                    String sLine1 = "38.2 %";
                    String sLine2 = "50 %";
                    String sLine3 = "61.8 %";
                    float online1 = pctLength1 - y1;
                    float online2 = pctLength2 - y1;
                    float online3 = pctLength3 - y1;

                    float ratio1 = (float) (pctLength1) / ((float) x2 - x1);
                    float ratio2 = (float) (pctLength2) / ((float) x2 - x1);
                    float ratio3 = (float) (pctLength3) / ((float) x2 - x1);

                    //when draw neer  graph right
                    int grfRight = grfLeft + grfWidth;

                    //diaganal ratio
                    float ratioDiag;
                    if (-(y2 - y1) >= 0) {
                        ratioDiag = (float) (-(grf_Top - y1)) / ((float) grfRight - x1);
                    } else {
                        ratioDiag = (float) (-(grf_Top + grf_Ht - y1)) / ((float) grfRight - x1);

                    }

                    // Edited by Mevan @ 2009-07-17 for Negative angles 
                    if (ratioDiag > 0) {

                        if (ratio1 > ratioDiag) { //thorough xtop
                            int topx1 = (int) (((float) -(grf_Top - y1)) / ratio1);
                            g.drawString(sLine1, topx1 + x1, grf_Top + 10);

                        } else { //y right
                            int edgey1 = (int) (((float) (grfRight - x1)) * ratio1);
                            g.drawString(sLine1, grfRight - 20, y1 - edgey1);
                        }

                        if (ratio2 > ratioDiag) {
                            int topx2 = (int) (((float) -(grf_Top - y1)) / ratio2);
                            g.drawString(sLine2, topx2 + x1, grf_Top + 10);
                        } else {
                            int edgey2 = (int) (((float) (grfRight - x1)) * ratio2);
                            g.drawString(sLine2, grfRight - 20, y1 - edgey2);
                        }

                        if (ratio3 > ratioDiag) {
                            int topx3 = (int) (((float) -(grf_Top - y1)) / ratio3);
                            g.drawString(sLine3, topx3 + x1, grf_Top + 10);
                        } else {
                            int edgey3 = (int) (((float) (grfRight - x1)) * ratio3);
                            g.drawString(sLine3, grfRight - 20, y1 - edgey3);

                        }
                    } else {
//                        ratioDiag = -ratioDiag;
//                        ratio1 = -ratio1;
//                        ratio2 = -ratio2;
//                        ratio3 = -ratio3;
                        // condition is reversed as the values are negative 
                        if (ratio1 < ratioDiag) { //thorough xtop
                            int topx1 = (int) (((float) -(grf_Top + grf_Ht - y1)) / ratio1);
                            g.drawString(sLine1, topx1 + x1, grf_Top + grf_Ht);
                            //g.drawString("Ratio : " + ratio1 + "  -diagRatio :" + ratioDiag + "   topX : " + ( topx1+x1) + "   Y :" + (grf_Top+grf_Ht - 10), grfLeft + 50, grf_Top + 10);
                        } else { //y right
                            int edgey1 = (int) (((float) (grfRight - x1)) * ratio1);
                            g.drawString(sLine1, grfRight - 20, y1 - edgey1);
                            //g.drawString("Ratio : " + ratio1 + "   -diagRatio :" + ratioDiag + "   topX : " + (grfRight - 20) + "   Y :" + (y1 - edgey1), grfLeft + 50, grf_Top + 10);
                        }
                        if (ratio2 < ratioDiag) {
                            int topx2 = (int) (((float) -(grf_Top + grf_Ht - y1)) / ratio2);
                            g.drawString(sLine2, topx2 + x1, grf_Top + grf_Ht);
                        } else {
                            int edgey2 = (int) (((float) (grfRight - x1)) * ratio2);
                            g.drawString(sLine2, grfRight - 20, y1 - edgey2);
                        }
                        if (ratio3 < ratioDiag) {
                            int topx3 = (int) (((float) -(grf_Top + grf_Ht - y1)) / ratio3);
                            g.drawString(sLine3, topx3 + x1, grf_Top + grf_Ht);
                        } else {
                            int edgey3 = (int) (((float) (grfRight - x1)) * ratio3);
                            g.drawString(sLine3, grfRight - 20, y1 - edgey3);

                        }
                    }

                    //adding tooltip data rows
                    dataRows.clear();

                    if (graph.isCurrentMode()) {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
                    } else {
                        timeFormatter = new SimpleDateFormat("dd/MM/yy");
                    }

                    D1 = new Date(xArray[0]);
                    D2 = null;
                    if (xArray.length > 1) {
                        D2 = new Date(xArray[1]);
                    }

                    trendBeginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
                    trendEndPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());
                    beginDate = timeFormatter.format(D1);
                    endDate = timeFormatter.format(D2);

                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), trendBeginPrice));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
                    dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), trendEndPrice));
                    dataRows.add(new ToolTipDataRow("38.20%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .382, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("50.00%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .50, ((WindowPanel) rect).isInThousands())));
                    dataRows.add(new ToolTipDataRow("61.80%", graph.GDMgr.formatPriceField(yArray[0] + (yArray[1] - yArray[0]) * .618, ((WindowPanel) rect).isInThousands())));


                    break;
                case INT_FIBONACCI_RETRACEMENTS:
                    if (xArr[0] < xArr[1]) {
                        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    } else {
                        x1 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                        y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    }
                    len = (y2 - y1);
                    if (len != 0) {
                        float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 1.618f, 2.618f, 4.236f}; // added 0.764f - pramoda
                        g.setStroke(dottedPen);
                        g.setFont(font);
                        FontMetrics fm = g.getFontMetrics(font);
                        int strHt = fm.getHeight() / 3;
                        for (int i = 0; i < fibArr.length; i++) {
                            y = y2 - Math.round(fibArr[i] * len);
                            g.drawLine(x1, y, rect.x + rect.width, y);
                            String s = percentformat.format(fibArr[i]);
                            int strWd = fm.stringWidth(s);
                            g.drawString(s, x1 - strWd - halfBox, y + strHt);
                        }
                        g.setStroke(pen);
                        g.drawLine(x1, y1, rect.x + rect.width, y1);
                        g.drawLine(x1, y2, rect.x + rect.width, y2);
                        String s = "0.0 %";
                        int strWd = fm.stringWidth(s);
                        g.drawString(s, x1 - strWd - halfBox, y2 + strHt);
                        s = "100.0 %";
                        strWd = fm.stringWidth(s);
                        g.drawString(s, x1 - strWd - halfBox, y1 + strHt);
                    }
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_FIBONACCI_ZONES:
                    y1 = grf_Top;
                    y2 = grf_Top + grf_Ht;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    g.setStroke(dottedPen);
                    g.drawLine(x1, y1, x1, y2);
                    while (true) {
                        sum += curVal;
                        //System.out.println("���� writing fibonaaci value "+sum);
                        x = Math.round((xArr[0] + sum - bIndex) * xFactor) + grfLeft;
                        //if (x>grfLeft+grfWidth) break;
                        if (sum > eIndex) break;
                        g.setColor(color);
                        g.setStroke(pen);
                        g.drawLine(x, y1, x, y2);
                        if (drawSelected) {
                            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                            g.setStroke(selectPen);
                            g.fillRect(x - halfBox, (y1 + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                            g.setColor(Color.BLACK);
                            g.drawRect(x - halfBox, (y1 + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                        }
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        g.setClip(null);
    }

    private String[] extractLines(String text) {
        String[] sa;
        try {
            sa = text.split("\n");
            return sa;
        } catch (Exception ex) {
            String[] sa1 = {text};
            return sa1;
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        if (!rect.contains(x, y) || !isMove) return false;
        int x1, x2, y1, y2, x0, y0;
        boolean result = false;
        switch (objType) {
            case INT_LINE_HORIZ:
                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                if (((y - y1) * (y - y1) < 4) && (rect.contains(x, y))) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    movingPtIndex = 0;
                    return true;
                }
                break;
            case INT_LINE_VERTI:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                if (((x - x1) * (x - x1) < 4) && (rect.contains(x, y))) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    movingPtIndex = 0;
                    return true;
                }
                break;
            case INT_SYMBOL:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                Rectangle Rs = new Rectangle(x1 - 13, y1 - 13, 26, 26);
                if (Rs.contains(x, y)) {
                    movingPtIndex = 0;
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
                break;
            case INT_RECT:
            case INT_RECT_SELECTION:
            case INT_TEXT:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                if (isCursorOnMoveHandles(x1, y1, x2, y2, x, y, false)) {
                    if (!selected)
                        selected = select;
                    objectMoving = false;
                    return true;
                }
                result = isOnTheLine(x1, y1, x1, y2, x, y, select, isMove);

                if (result) return result;
                result = isOnTheLine(x2, y1, x2, y2, x, y, select, isMove);

                if (result) return result;
                result = isOnTheLine(x1, y1, x2, y1, x, y, select, isMove) ||
                        isOnTheLine(x1, y2, x2, y2, x, y, select, isMove);

                if (result) return result;
                if ((objType == INT_TEXT) || ((objType == INT_RECT) && (!transparent))) {
                    Rectangle R = new Rectangle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    if (R.contains(x, y)) {
                        movingPtIndex = 0;
                        if (!selected)
                            selected = select;
                        objectMoving = true;
                        return true;
                    }
                }
                return result;
            case INT_ELLIPSE:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                if (isCursorOnMoveHandles(x1, y1, x2, y2, x, y, false)) {
                    if (!selected)
                        selected = select;
                    objectMoving = false;
                    return true;
                }
                float a, b, p, q;
                a = Math.abs(x1 - x2) / 2f;
                b = Math.abs(y1 - y2) / 2f;
                p = Math.abs(x1 + x2) / 2f;
                q = Math.abs(y1 + y2) / 2f;
                int gap = 2;
                boolean insideTheOuterEllipse = ((b + gap) * (b + gap) * (x - p) * (x - p) +
                        (a + gap) * (a + gap) * (y - q) * (y - q) <=
                        (a + gap) * (a + gap) * (b + gap) * (b + gap));
                if (!transparent) {
                    result = insideTheOuterEllipse;
                } else {
                    boolean outsideTheInnerEllipse = (a < gap) || (b < gap) || ((b - gap) * (b - gap) * (x - p) * (x - p) +
                            (a - gap) * (a - gap) * (y - q) * (y - q) >=
                            (a - gap) * (a - gap) * (b - gap) * (b - gap));
                    result = insideTheOuterEllipse && outsideTheInnerEllipse;
                }
                if (result) {
                    if (!selected)
                        selected = select;
                    movingPtIndex = 0;
                    objectMoving = true;
                }
                return result;
            case INT_FIBONACCI_ARCS:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
                    return true;
                }
                float len;
                int start;
                len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                start = (y2 > y1) ? 0 : 180;
                result = isOnFibonacciArc(x2, y2, len, start, x, y);
                if (result) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    //setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
                }
                return result;
            case INT_FIBONACCI_FANS:
                float ratio = 1f;
                if (xArray[0] < xArray[1]) {
                    x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
                        return true;
                    }
                } else {
                    x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (isOnTheLine(x2, y2, x1, y1, x, y, select, isMove)) {
                        return true;
                    }
                }
                if ((x1 == x2)) {
                    x0 = x1;
                    y0 = grf_Top - grf_Ht;
                } else {
                    ratio = (float) (y2 - y1) / ((float) x2 - x1);
                    x0 = x1 + grfWidth;
                    y0 = y1 + Math.round(ratio * grfWidth);
                }
                int yP1, yP2, yP3;
                yP1 = Math.round(0.618f * y1 + 0.382f * y0);
                yP2 = Math.round(0.5f * y1 + 0.5f * y0);
                yP3 = Math.round(0.382f * y1 + 0.618f * y0);
                if (isOnLineForMove(x1, y1, x0, yP1, x, y, select)) {
                    return true;
                }
                if (isOnLineForMove(x1, y1, x0, yP2, x, y, select)) {
                    return true;
                }
                if (isOnLineForMove(x1, y1, x0, yP3, x, y, select)) {
                    return true;
                }
                return false;
            case INT_FIBONACCI_RETRACEMENTS:
                if (xArray[0] < xArray[1]) {
                    x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
                        return true;
                    }
                } else {
                    x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    if (isOnTheLine(x2, y2, x1, y1, x, y, select, isMove)) {
                        return true;
                    }
                }
                if ((x1 != x2)) {
                    len = (y2 - y1);
                    float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f}; // added 0.764f - pramoda
                    for (int i = 0; i < fibArr.length; i++) {
                        y0 = y2 - Math.round(fibArr[i] * len);
                        if (isOnLineForMove(x1, y0, x1 + grfWidth, y0, x, y, select)) {
                            return true;
                        }
                    }
                    if (isOnLineForMove(x1, y1, x1 + grfWidth, y1, x, y, select)) {
                        return true;
                    }
                    if (isOnLineForMove(x1, y2, x1 + grfWidth, y2, x, y, select)) {
                        return true;
                    }
                }
                return false;
            case INT_FIBONACCI_ZONES:
                y1 = grf_Top;
                y2 = grf_Top + grf_Ht;
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                int preVal = 0;
                int curVal = 1;
                int tmpVal = 0;
                int sum = 0;
                if (isOnLineForMove(x1, y1, x1, y2, x, y, select)) {
                    return true;
                }
                while (true) {
                    sum += curVal;
                    //System.out.println("���� writing fibonaaci value "+sum);
                    x0 = Math.round((xArray[0] + sum - bIndex) * xFactor) + grfLeft;
                    if (x0 > grfLeft + grfWidth) break;
                    if (isOnLineForMove(x0, y1, x0, y2, x, y, select)) {
                        return true;
                    }
                    tmpVal = curVal;
                    curVal += preVal;
                    preVal = tmpVal;
                }
                return false;
            default:
                return false;
        }
        return false;
    }

    /*protected boolean isCursorOnMoveHandles(int x1, int y1, int x2, int y2, int x, int y, boolean select){
        boolean result = false;
        if (((x1-x)*(x1-x)<=halfSquared)&&((y1-y)*(y1-y)<=halfSquared)){
//			movingPoint.setLocation(x1, y1);
//			fixedPoint.setLocation(x2, y2);
            movingPtIndex = 0;
            result = true;
        }else if (((x2-x)*(x2-x)<=halfSquared)&&((y2-y)*(y2-y)<=halfSquared)){
//			movingPoint.setLocation(x2, y2);
//			fixedPoint.setLocation(x1, y1);
            movingPtIndex = 1;
            result = true;
        }else if (((x1-x)*(x1-x)<=halfSquared)&&((y2-y)*(y2-y)<=halfSquared)){
//			movingPoint.setLocation(x1, y2);
//			fixedPoint.setLocation(x2, y1);
            movingPtIndex = 2;
            result = true;
        }else if (((x2-x)*(x2-x)<=halfSquared)&&((y1-y)*(y1-y)<=halfSquared)){
//			movingPoint.setLocation(x2, y1);
//			fixedPoint.setLocation(x1, y2);
            movingPtIndex = 3;
            result = true;
        }
        if (result){
            if (!selected)
                selected = select;
            objectMoving = (objType==INT_SYMBOL);
            return true;
        }
        return false;
    }*/

    protected boolean isOnFibonacciArc(int x2, int y2, float len, int start, int x, int y) {
        int r1, r2, r3;
        r1 = Math.round(len * 0.618f);
        r2 = Math.round(len * 0.5f);
        r3 = Math.round(len * 0.382f);
        boolean isInRightHalf = false;
        if (start == 0) {
            isInRightHalf = (x < x2 + r1) && (x > x2 - r1) && (y > y2 - r1) && (y < y2);
        } else {
            isInRightHalf = (x < x2 + r1) && (x > x2 - r1) && (y < y2 + r1) && (y > y2);
        }
        boolean result = isOnEllipse(r1, r1, x2, y2, x, y);
        result = result || isOnEllipse(r2, r2, x2, y2, x, y);
        result = result || isOnEllipse(r3, r3, x2, y2, x, y);
        result = result && isInRightHalf;
        if (result) objectMoving = true;
        return result;
    }

    protected boolean isOnEllipse(int a, int b, int p, int q, int x, int y) {
        int gap = 2;
        boolean insideTheOuterEllipse = ((b + gap) * (b + gap) * (x - p) * (x - p) +
                (a + gap) * (a + gap) * (y - q) * (y - q) <=
                (a + gap) * (a + gap) * (b + gap) * (b + gap));
        boolean outsideTheInnerEllipse = (a < gap) || (b < gap) || ((b - gap) * (b - gap) * (x - p) * (x - p) +
                (a - gap) * (a - gap) * (y - q) * (y - q) >=
                (a - gap) * (a - gap) * (b - gap) * (b - gap));
        return insideTheOuterEllipse && outsideTheInnerEllipse;
    }


    //text
    public String getText() {
        return text;
    }

    public void setText(String s) {
        text = s;
    }

    //font
    public Font getFont() {
        return font;
    }

    public void setFont(Font f) {
        font = f;
    }

    //fillColor
    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fc) {
        fillColor = fc;
    }

    //fontColor
    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fc) {
        fontColor = fc;
    }

    //transparent
    public boolean isTransparent() {
        return transparent;
    }

    public void setTransparent(boolean b) {
        transparent = b;
    }

    //extendedLeft
    public boolean isExtendedLeft() {
        return extendedLeft;
    }

    public void setExtendedLeft(boolean extendedLeft) {
        this.extendedLeft = extendedLeft;
    }

    //extendedRight
    public boolean isExtendedRight() {
        return extendedRight;
    }

    public void setExtendedRight(boolean extendedRight) {
        this.extendedRight = extendedRight;
    }

    //channelUnits
    public int getChannelUnits() {
        return channelUnits;
    }

    public void setChannelUnits(int channelUnits) {
        this.channelUnits = channelUnits;
    }

    public String toString(boolean currMode) {
        if (getValueString(currMode) != null && getValueString(currMode).length() > 0) {
            return getShortName() + " (" + getValueString(currMode) + ")";
        } else {
            return getShortName();
        }
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        Date D1, D2 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }
        ///////////////////////////////////////
        switch (objType) {
            case INT_LINE_SLOPE:
            case INT_ARROW_LINE_SLOPE:
                return timeFormatter.format(D1) + ", " +
                        graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands()) + ", " +
                        timeFormatter.format(D2) + ", " +
                        graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());

            case INT_LINE_HORIZ:
                return graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
            case INT_LINE_VERTI:
                return timeFormatter.format(D1);
            case INT_DATA_LINE_VERTI:
                return "";
            case INT_SYMBOL:
                return timeFormatter.format(D1) + ", " +
                        graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
            case INT_RECT:
                /*  return timeFormatter.format(D1) + ", " +
                        GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands())+", "+
                        timeFormatter.format(D2)+", "+
              GraphDataManager.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());*/
                return "";
            case INT_TEXT:
                /*return timeFormatter.format(D1) + ", " +
                        GraphDataManager.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());*/
                return "";
            case INT_ELLIPSE:
                /*return timeFormatter.format(D1) + ", " +
                        GraphDataManager.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands()) + ", " +
                        timeFormatter.format(D2) + ", " +
                        GraphDataManager.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());*/
                return "";
            case INT_POLYGON:
                return timeFormatter.format(D1) + ", " +
                        graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
            case INT_FIBONACCI_ARCS:
            case INT_FIBONACCI_FANS:
            case INT_FIBONACCI_RETRACEMENTS:
            case INT_FIBONACCI_ZONES:
            default:
                return "";
        }
    }

    public String getShortName() {
        switch (objType) {
            case INT_LINE_SLOPE:
                return Language.getString("TRND_LINE");
            case INT_ARROW_LINE_SLOPE:
                return Language.getString("ARROW_HEAD_TRND_LINE");
            case INT_LINE_HORIZ:
                return Language.getString("HIRIZONTAL_LINE");
            case INT_LINE_VERTI:
                return Language.getString("VERTICAL_LINE");
            case INT_DATA_LINE_VERTI:
                return "";
            case INT_SYMBOL:
                return Language.getString("SYMBOL");
            case INT_RECT:
                return Language.getString("RECTANGLE");
            case INT_TEXT:
                return Language.getString("TEXT_NOTE");
            case INT_ELLIPSE:
                return Language.getString("ELLIPSE");
            case INT_POLYGON:
                return Language.getString("POLYGON");
            case INT_FIBONACCI_ARCS:
                return Language.getString("GRAPH_OBJ_FIB_ARCS");
            case INT_FIBONACCI_FANS:
                return Language.getString("GRAPH_OBJ_FIB_FANS");
            case INT_FIBONACCI_RETRACEMENTS:
                return Language.getString("GRAPH_OBJ_FIB_RETRC");
            case INT_FIBONACCI_ZONES:
                return Language.getString("GRAPH_OBJ_FIB_ZONES");
            default:
                return Language.getString("TRND_LINE");
        }
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadFromLayout(xpath, document, preExpression);
        this.text = TemplateFactory.loadProperty(xpath, document, preExpression + "/Text");
        String fontName = TemplateFactory.loadProperty(xpath, document, preExpression + "/FontName");
        int fontStyle = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/FontStyle"));
        int fontSize = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/FontSize"));
        this.font = new TWFont(fontName, fontStyle, fontSize);
        this.transparent = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/Transparent"));
        this.isShowBorder = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/ShowBorder"));

        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/FontColor").split(",");
        this.fontColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
        saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/FillColor").split(",");
        this.fillColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
        this.extendedLeft = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/ExtendedLeft"));
        this.extendedRight = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/ExtendedRight"));
        this.channelUnits = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/ChannelUnits"));
    }

    public void saveToLayout(org.w3c.dom.Element xmlobj, org.w3c.dom.Document document) {
        super.saveToLayout(xmlobj, document);
        TemplateFactory.saveProperty(xmlobj, document, "Text", this.text);
        TemplateFactory.saveProperty(xmlobj, document, "FontName", this.font.getFontName());
        TemplateFactory.saveProperty(xmlobj, document, "FontStyle", Integer.toString(this.font.getStyle()));
        TemplateFactory.saveProperty(xmlobj, document, "FontSize", Integer.toString(this.font.getSize()));
        TemplateFactory.saveProperty(xmlobj, document, "Transparent", Boolean.toString(this.transparent));
        TemplateFactory.saveProperty(xmlobj, document, "ShowBorder", Boolean.toString(this.isShowBorder));

        String strColor = this.fontColor.getRed() + "," + this.fontColor.getGreen() + "," + this.fontColor.getBlue();
        TemplateFactory.saveProperty(xmlobj, document, "FontColor", strColor);
        strColor = this.fillColor.getRed() + "," + this.fillColor.getGreen() + "," + this.fillColor.getBlue();
        TemplateFactory.saveProperty(xmlobj, document, "FillColor", strColor);
        TemplateFactory.saveProperty(xmlobj, document, "ExtendedLeft", Boolean.toString(this.extendedLeft));
        TemplateFactory.saveProperty(xmlobj, document, "ExtendedRight", Boolean.toString(this.extendedRight));
        TemplateFactory.saveProperty(xmlobj, document, "ChannelUnits", Integer.toString(this.channelUnits));
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }


    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        for (int i = 0; i < xArray.length; i++) {
            int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
            double y0 = graph.GDMgr.getPixelFortheYValue(yArray[i], pnlID);

            float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[i]});
            double x0 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            Point2D.Double p2d = new Point2D.Double(x0, y0);

            if (r.contains(p2d)) {
                return true;
            }
        }
        return false;

    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        extendedLeft = false;
        extendedRight = false;
        drawingOnBackground = true;
        isFreeStyle = false;
        channelUnits = 2;
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        if (objType == INT_TEXT) {
            transparent = true;
        } else {
            transparent = false;
        }
        if (objType == INT_SYMBOL) {
            ((ObjectSymbol) this).setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18));
        }
        fontColor = StockGraph.fontColor;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        fillColor = new Color(240, 240, 230);
        isShowBorder = true;

    }
}