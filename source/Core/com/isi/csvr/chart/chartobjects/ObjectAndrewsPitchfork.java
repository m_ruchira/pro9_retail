package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Aug 17, 2009
 * Time: 4:48:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectAndrewsPitchfork extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_ANDREWS_PITCHFORK;
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();

    public ObjectAndrewsPitchfork() {
        super();
        objType = AbstractObject.INT_ANDREWS_PITCHFORK;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
    }

    public ObjectAndrewsPitchfork(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_ANDREWS_PITCHFORK;
        showOnAllPanels = false;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        penStyle = GraphDataManager.STYLE_SOLID;
        isShowingDragToolTip = true;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    // extending LineVertical independently to draw on all windows ################################################
    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);

        grfLeft = rect.x + StockGraph.clearance;
        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        int x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        int x3 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft;
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        int y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        int y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, grf_Ht + grf_Top);

        if (objectMoving) {
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;
            x3 = x3 - Xadj;

            y1 = y1 - (int) Yadj;
            y2 = y2 - (int) Yadj;
            y3 = y3 - (int) Yadj;
        } else {
            if (movingPtIndex == 0) {
                x1 = x1 - Xadj;
                y1 = y1 - (int) Yadj;
            } else if (movingPtIndex == 1) {
                x2 = x2 - Xadj;
                y2 = y2 - (int) Yadj;
            } else if (movingPtIndex == 2) {
                x3 = x3 - Xadj;
                y3 = y3 - (int) Yadj;
            }
        }

        int midX = (x2 + x3) / 2;
        int midY = (y2 + y3) / 2;

        g.drawLine(x2, y2, x3, y3);
        float m = 1;
        int otherMidX = 0;
        int otherMidY = 0;
        int otherX2 = 0, otherY2 = 0, otherY3 = 0, otherX3 = 0;

        if (x1 > midX) {

            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x));
            otherMidX = rect.x;

            otherX2 = rect.x;
            otherY2 = (int) (y2 - m * (x2 - rect.x));
            otherX3 = rect.x;
            otherY3 = (int) (y3 - m * (x3 - rect.x));
        } else if (x1 < midX) {

            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x - rect.width));
            otherMidX = rect.x + rect.width;

            otherX2 = rect.x + rect.width;
            otherY2 = (int) (y2 - m * (x2 - rect.x - rect.width));
            otherX3 = rect.x + rect.width;
            otherY3 = (int) (y3 - m * (x3 - rect.x - rect.width));
        } else {
            otherMidY = rect.y + rect.height;
            otherMidX = x1;

            otherX2 = x2;
            otherY2 = rect.y + rect.height;
            otherX3 = x3;
            otherY3 = rect.y + rect.height;

            if (y1 > midY) {
                otherY2 = rect.y;
                otherY3 = rect.y;
                otherMidY = rect.y;
            }

        }

        //drawing the first point and the middle point of 2 and 3
        g.drawLine(x1, y1, otherMidX, otherMidY);
        //drawing the second point and the bottom most point
        g.drawLine(x2, y2, otherX2, otherY2);
        //drawing the third point and the bottom most point
        g.drawLine(x3, y3, otherX3, otherY3);

        g.drawLine(x1, y1, x2, y2);

        dataRows.clear();

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        float index2 = bIndex + (x2 - grfLeft) / xFactor;
        float index3 = bIndex + (x3 - grfLeft) / xFactor;

        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2, index3});

        Date D1 = new Date(newXarray[0]);
        Date D2 = new Date(newXarray[1]);
        Date D3 = new Date(newXarray[2]);
        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        double handle = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
        double tine1 = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
        double tine2 = getYValueForthePixel(y3, rect, minY, yFactor, grf_Ht + grf_Top);

        String handlePrice = String.valueOf(graph.GDMgr.formatPriceField(handle, ((WindowPanel) rect).isInThousands()));
        String tine1Price = String.valueOf(graph.GDMgr.formatPriceField(tine1, ((WindowPanel) rect).isInThousands()));
        String tine2Price = String.valueOf(graph.GDMgr.formatPriceField(tine2, ((WindowPanel) rect).isInThousands()));

        String handleDate = timeFormatter.format(D1);
        String tine1Date = timeFormatter.format(D2);
        String tine2Date = timeFormatter.format(D3);

        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_HANDLE_LINE") + " " + Language.getString("GRAPH_APDATE"), handleDate));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_HANDLE_LINE") + " " + Language.getString("GRAPH_APPRICE"), handlePrice));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_1") + " " + Language.getString("GRAPH_APDATE"), tine1Date));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_1") + " " + Language.getString("GRAPH_APPRICE"), tine1Price));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_2") + " " + Language.getString("GRAPH_APDATE"), tine2Date));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_2") + " " + Language.getString("GRAPH_APPRICE"), tine2Price));

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {

        gg.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);

        grfLeft = rect.x + StockGraph.clearance;
        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        int x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        int x3 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft;
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        int y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        int y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, grf_Ht + grf_Top);

        int midX = (x2 + x3) / 2;
        int midY = (y2 + y3) / 2;

        //g.setColor(Color.RED);
        g.drawLine(x2, y2, x3, y3);
        //g.setColor(color);
        float m = 1;
        int otherMidX = 0;
        int otherMidY = 0;
        int otherX2 = 0, otherY2 = 0, otherY3 = 0, otherX3 = 0;

        if (x1 > midX) {

            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x));
            otherMidX = rect.x;

            otherX2 = rect.x;
            otherY2 = (int) (y2 - m * (x2 - rect.x));
            otherX3 = rect.x;
            otherY3 = (int) (y3 - m * (x3 - rect.x));
        } else if (x1 < midX) {

            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x - rect.width));
            otherMidX = rect.x + rect.width;

            otherX2 = rect.x + rect.width;
            otherY2 = (int) (y2 - m * (x2 - rect.x - rect.width));
            otherX3 = rect.x + rect.width;
            otherY3 = (int) (y3 - m * (x3 - rect.x - rect.width));
        } else {
            otherMidY = rect.y + rect.height;
            otherMidX = x1;

            otherX2 = x2;
            otherY2 = rect.y + rect.height;
            otherX3 = x3;
            otherY3 = rect.y + rect.height;
            if (y1 > midY) {
                otherY2 = rect.y;
                otherY3 = rect.y;
                otherMidY = rect.y;
            }
        }


        //drawing the first point and the middle point of 2 and 3
        g.drawLine(x1, y1, otherMidX, otherMidY);
        //drawing the second point and the bottom most point
        g.drawLine(x2, y2, otherX2, otherY2);
        //drawing the third point and the bottom most point
        g.drawLine(x3, y3, otherX3, otherY3);

        float[] dashArr = {3.0f, 3.0f};
        BasicStroke BS_1p0f_2by2_dashed = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 1.0f);

        ((Graphics2D) g).setStroke(BS_1p0f_2by2_dashed);
        g.drawLine(x1, y1, x2, y2);
        String str1 = "50%";
        String str2 = "100%";
        String str3 = "0%";

        // Draw percentage Strings
        drawStringsOnGraphics(g, grfLeft, grfWidth, grf_Top, grf_Ht, x1, y1, otherMidX, otherMidY, m, str1);
        drawStringsOnGraphics(g, grfLeft, grfWidth, grf_Top, grf_Ht, x2, y2, otherX2, otherY2, m, str2);
        drawStringsOnGraphics(g, grfLeft, grfWidth, grf_Top, grf_Ht, x3, y3, otherX3, otherY3, m, str3);

        boolean drawSelected = selected && !isPrinting;

        if (drawSelected) {
            g.setStroke(selectPen);

            gg.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            gg.fillRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);
            gg.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

            gg.setColor(Color.BLACK);
            gg.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            gg.drawRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);
            gg.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        }

        dataRows.clear();
        String handlePrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
        String tine1Price = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());
        String tine2Price = graph.GDMgr.formatPriceField(yArray[2], ((WindowPanel) rect).isInThousands());

        Date D1 = new Date(xArray[0]);
        Date D2 = new Date(xArray[1]);
        Date D3 = new Date(xArray[2]);
        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        String handleDate = timeFormatter.format(D1);
        String tine1Date = timeFormatter.format(D2);
        String tine2Date = timeFormatter.format(D3);

        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_HANDLE_LINE") + " " + Language.getString("GRAPH_APDATE"), handleDate));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_HANDLE_LINE") + " " + Language.getString("GRAPH_APPRICE"), handlePrice));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_1") + " " + Language.getString("GRAPH_APDATE"), tine1Date));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_1") + " " + Language.getString("GRAPH_APPRICE"), tine1Price));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_2") + " " + Language.getString("GRAPH_APDATE"), tine2Date));
        dataRows.add(new ToolTipDataRow(Language.getString("GRAPH_APLINE_2") + " " + Language.getString("GRAPH_APPRICE"), tine2Price));
    }

    protected void drawStringsOnGraphics(Graphics gg, int grfLeft, int grfWidth, int grf_Top, int grf_Ht, int x1, int y1, int x2, int y2, float ratioLine, String str1) {

        gg.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setFont(new Font("Tahoma", Font.PLAIN, 10));

        int grfRight = grfLeft + grfWidth;

        float ratioDiag;


        if (x2 == x1) {
            if (y2 > y1) {
                g.drawString(str1, x1, grf_Top + grf_Ht);
            } else {
                g.drawString(str1, x1, grf_Top + 10);
            }

        } else if (x2 > x1) {
            if (-(y2 - y1) >= 0) {
                ratioDiag = (float) (-(grf_Top - y1)) / ((float) grfRight - x1);
            } else {
                ratioDiag = (float) (-(grf_Top + grf_Ht - y1)) / ((float) grfRight - x1);
            }
            ratioLine = -ratioLine;
            if (ratioDiag > 0) {

                if (ratioLine > ratioDiag) { //thorough xtop
                    int topx1 = (int) (((float) -(grf_Top - y1)) / ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(topx1 + x1 - 1, grf_Top + 1, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);

                    g.drawString(str1, topx1 + x1, grf_Top + 10);
                } else { //y right
                    int edgey1 = (int) (((float) (grfRight - x1)) * ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(grfRight - 21, y1 - edgey1 - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, grfRight - 20, y1 - edgey1);
                }
            } else {
                if (ratioLine < ratioDiag) { //thorough xtop
                    int topx1 = (int) (((float) -(grf_Top + grf_Ht - y1)) / ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(topx1 + x1 - 1, grf_Top + grf_Ht - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, topx1 + x1, grf_Top + grf_Ht);
                } else { //y right
                    int edgey1 = (int) (((float) (grfRight - x1)) * ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(grfRight - 21, y1 - edgey1 - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, grfRight - 20, y1 - edgey1);
                }
            }
        } else {
            if (-(y2 - y1) >= 0) {
                ratioDiag = (float) (-(grf_Top - y1)) / ((float) x1 - grfLeft);
            } else {
                ratioDiag = (float) (-(grf_Top + grf_Ht - y1)) / ((float) x1 - grfLeft);
            }
            ratioLine = -ratioLine;
            ratioDiag = -ratioDiag;
            if (ratioDiag > 0) {
                if (ratioLine > ratioDiag) { //thorough xtop
                    int topx1 = (int) (((float) -(grf_Top + grf_Ht - y1)) / ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(topx1 + x1 - 1, grf_Top + grf_Ht - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, topx1 + x1, grf_Top + grf_Ht);
                } else { //y right
                    int edgey1 = (int) (((float) (grfLeft - x1)) * ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(grfLeft - 1, y1 - edgey1 - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, grfLeft, y1 - edgey1);
                }
            } else {

                if (ratioLine < ratioDiag) { //thorough xtop
                    int topx1 = (int) (((float) -(y1 - grf_Top)) / ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(x1 - topx1, grf_Top + 1, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, x1 - topx1, grf_Top + 10);
                } else { //y right
                    int edgey1 = (int) (((float) (x1 - grfLeft)) * ratioLine);
                    g.setColor(graph.getGraphAreaLeftColor());
                    g.fillRect(grfLeft - 1, y1 + edgey1 - 9, 25, 10);
                    g.setColor(color);
                    g.setStroke(pen);
                    g.drawString(str1, grfLeft, y1 + edgey1);
                }
            }
        }

        g.setPaintMode();
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {


        Rectangle rectangle = new Rectangle(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        if (!rectangle.contains(x, y) || !isMove) return false;

        grfLeft = rect.x + StockGraph.clearance;
        int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        int x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
        int x3 = Math.round((xArray[2] - bIndex) * xFactor) + grfLeft;
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        int y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        int y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, grf_Ht + grf_Top);

        int midX = (x2 + x3) / 2;
        int midY = (y2 + y3) / 2;


        float m = 1;
        int otherMidX = 0;
        int otherMidY = 0;
        int otherX2 = 0, otherY2 = 0, otherY3 = 0, otherX3 = 0;

        if (x1 > midX) {
            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x));
            otherMidX = rect.x;

            otherX2 = rect.x;
            otherY2 = (int) (y2 - m * (x2 - rect.x));
            otherX3 = rect.x;
            otherY3 = (int) (y3 - m * (x3 - rect.x));
        } else if (x1 < midX) {
            m = (float) (y1 - midY) / (float) (x1 - midX);

            otherMidY = (int) (y1 - m * (x1 - rect.x - rect.width));
            otherMidX = rect.x + rect.width;

            otherX2 = rect.x + rect.width;
            otherY2 = (int) (y2 - m * (x2 - rect.x - rect.width));
            otherX3 = rect.x + rect.width;
            otherY3 = (int) (y3 - m * (x3 - rect.x - rect.width));
        } else {
            otherMidY = rect.y + rect.height;
            otherMidX = x1;

            otherX2 = x2;
            otherY2 = rect.y + rect.height;
            otherX3 = x3;
            otherY3 = rect.y + rect.height;
            if (y1 > midY) {
                otherY2 = rect.y;
                otherY3 = rect.y;
                otherMidY = rect.y;
            }
        }
        if (isOnTheLine(x1, y1, otherMidX, otherMidY, x, y, select, isMove, 0)) {
            return true;
        } else if (isOnTheLine(x2, y2, otherX2, otherY2, x, y, select, isMove, 1)) {
            return true;
        } else if (isOnTheLine(x3, y3, otherX3, otherY3, x, y, select, isMove, 2)) {
            return true;
        } else if (isOnTheLine(x2, y2, x3, y3, x, y, select, isMove, 1)) {
            return true;
        } else if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            return true;
        }

        return false;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_PITCHFORK");
    }

    protected String getValueString(boolean currMode) {
        return "";
    }

    protected boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select, boolean isMove, int index) {
        if (!isMove) {
            return false;
        }
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = index;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
            onFirstPoint = false;
            if (!selected)
                selected = select;
            movingPtIndex = index + 1;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = index;
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    @Override
    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

}
