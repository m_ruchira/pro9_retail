package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectChannelProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:23:12 PM
 */
public class ObjectStandardError extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_STD_ERROR_CHANNEL;

    private boolean extendedLeft;
    private boolean extendedRight;
    private float units = 2f;
    private StockGraph graph;

    public ObjectStandardError() {
        super();
        objType = INT_STD_ERROR_CHANNEL;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction

       /* isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectChannelProperty op = (ObjectChannelProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.extendedLeft = op.isExtendedLeft();
            this.extendedRight = op.isExtendedRight();
            this.units = op.getNoOfUnits();
        }*/

    }

    public ObjectStandardError(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, null);
        objType = INT_STD_ERROR_CHANNEL;
        this.graph = graph;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectChannelProperty op = (ObjectChannelProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.extendedLeft = op.isExtendedLeft();
            this.extendedRight = op.isExtendedRight();
            this.units = op.getNoOfUnits();
        }
    }

    public static boolean calculateStandardError(StockGraph graph, int mdX, int muX, int[] faX, int[] faY,
                                                 double[] stdErr, Rectangle rect, byte OHLCPriority) {
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        if (pnlID != graph.getIDforPanel(graph.GDMgr.getBaseCP().getRect()))
            return false; //TODO: Added to avoid wrong regression lines on separate indicator panels, we have to correct this later
        int graphIndex = 0; //TODO: this is wrong when it comes to indicator panels

        mdX = Math.max(mdX, 0);
        muX = Math.min(muX, graph.GDMgr.getGraphStoreSize() - 1);
        if (mdX >= muX) return false;
        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, zigmaYY = 0, yi;
        ChartPoint cPoint;
        double count = 0;
        for (int xi = mdX; xi <= muX; xi++) {
            cPoint = graph.GDMgr.readChartPoint(graph.GDMgr.getTimeMillisec(xi), graphIndex);
            if (cPoint != null) {
                yi = cPoint.getValue(OHLCPriority);
                zigmaX += xi;
                zigmaY += yi;
                zigmaXX += xi * xi;
                zigmaYY += yi * yi;
                zigmaXY += xi * yi;
                count++;
            }
        }
        if (count > 0) {
            double Sxx = zigmaXX - zigmaX * zigmaX / count;
            double Sxy = zigmaXY - zigmaX * zigmaY / count;
            double b1 = Sxy / Sxx;
            double b0 = zigmaY / count - b1 * zigmaX / count;

            if (count > 2) {
                try {
                    double tmpExprn = zigmaYY - zigmaY * zigmaY / count - b1 * Sxy;
                    stdErr[0] = Math.sqrt(tmpExprn / (count - 2));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            faY[0] = graph.GDMgr.getPixelFortheYValue((b0 + b1 * mdX), pnlID);
            faY[1] = graph.GDMgr.getPixelFortheYValue((b0 + b1 * muX), pnlID);
            faX[0] = graph.GDMgr.getPixelFortheIndex(mdX);
            faX[1] = graph.GDMgr.getPixelFortheIndex(muX);

            return true;
        } else {
            return false;
        }
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectStandardError) {
            this.extendedLeft = ((ObjectStandardError) ao).extendedLeft;
            this.extendedRight = ((ObjectStandardError) ao).extendedRight;
            this.units = ((ObjectStandardError) ao).units;
        }
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        int x1, x2, prevX1, prevX2, diff;
        if (objectMoving) {
            x1 = prevX1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
            x2 = prevX2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
            diff = Math.abs(x1 - x2);
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;
        } else {
            x1 = prevX1 = graph.GDMgr.getPixelFortheIndex(xArr[movingPtIndex]);
            x2 = prevX2 = graph.GDMgr.getPixelFortheIndex(xArr[1 - movingPtIndex]);
            diff = Math.abs(x1 - x2);
            x1 = x1 - Xadj;
        }
        int xEnd = graph.GDMgr.getPixelFortheIndex(graph.GDMgr.getLastNotNullIndexOfGraphStore());
        if (x1 > xEnd) {
            x1 = xEnd;
            if (objectMoving) {
                x2 = xEnd - diff;
            } else {
                x2 = prevX2;
            }
        }
        if (x2 > xEnd) {
            if (objectMoving) {
                x1 = xEnd - diff;
            } else {
                x1 = prevX1;
            }
            x2 = xEnd;
        }
        int X1 = Math.round(graph.GDMgr.getIndexForthePixel(x1));
        int X2 = Math.round(graph.GDMgr.getIndexForthePixel(x2));
        int[] faX = new int[2];
        int[] faY = new int[2];
        double[] stdErr = new double[1];
        stdErr[0] = 0d;

        if (calculateStandardError(graph, Math.min(X1, X2), Math.max(X1, X2), faX, faY, stdErr,
                rect, OHLCPriority)) {
            int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
            int stdDevPix = Math.round(units * graph.GDMgr.getPixelGapFortheYValueGap(stdErr[0], pnlID));
            g.drawLine(faX[0], faY[0], faX[1], faY[1]);
            g.drawLine(faX[0], faY[0] + stdDevPix, faX[1], faY[1] + stdDevPix);
            g.drawLine(faX[0], faY[0] - stdDevPix, faX[1], faY[1] - stdDevPix);
            g.drawLine(faX[0], rect.y + GraphDataManager.titleHeight, faX[0], rect.y + rect.height);
            g.drawLine(faX[1], rect.y + GraphDataManager.titleHeight, faX[1], rect.y + rect.height);
        }
    }

    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                               int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        boolean drawSelected = selected && !isPrinting;
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect);

        int x1, x2, y1, y2;
        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
        x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
        y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        int X1 = Math.round(graph.GDMgr.getIndexForthePixel(x1));
        int X2 = Math.round(graph.GDMgr.getIndexForthePixel(x2));
        int[] faX = new int[2];
        int[] faY = new int[2];
        double[] stdErr = new double[1];
        stdErr[0] = 0d;
        int stdDevPix = 0;
        if (calculateStandardError(graph, Math.min(X1, X2), Math.max(X1, X2), faX, faY, stdErr,
                rect, OHLCPriority)) {
            stdDevPix = Math.round(units * graph.GDMgr.getPixelGapFortheYValueGap(stdErr[0], pnlID));
        }

        int tempX1 = faX[0];
        int tempX2 = faX[1];
        int tempY1 = faY[0];
        int tempY2 = faY[1];
        if (extendedLeft) {
            float ratio = 1f;
            if ((faX[0] == faX[1])) {
                tempY1 = rect.y + GraphDataManager.titleHeight;
            } else {
                ratio = (float) (faY[1] - faY[0]) / (float) (faX[1] - faX[0]);
                int pixGap = (faX[1] > faX[0]) ? rect.width : -rect.width;
                tempX1 = faX[0] - pixGap;
                tempY1 = faY[0] - Math.round(ratio * pixGap);
            }
        }
        if (extendedRight) {
            float ratio = 1f;
            if ((faX[0] == faX[1])) {
                tempY2 = rect.y + rect.height;
            } else {
                ratio = (float) (faY[1] - faY[0]) / (float) (faX[1] - faX[0]);
                int pixGap = (faX[1] > faX[0]) ? rect.width : -rect.width;
                tempX2 = faX[1] + pixGap;
                tempY2 = faY[1] + Math.round(ratio * pixGap);
            }
        }
        g.drawLine(tempX1, tempY1, tempX2, tempY2);
        g.drawLine(tempX1, tempY1 + stdDevPix, tempX2, tempY2 + stdDevPix);
        g.drawLine(tempX1, tempY1 - stdDevPix, tempX2, tempY2 - stdDevPix);

        if (drawSelected) {
            g.fillRect(faX[0] - halfBox, faY[0] - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(faX[1] - halfBox, faY[1] - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.black);
            g.drawRect(faX[0] - halfBox, faY[0] - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(faX[1] - halfBox, faY[1] - halfBox, halfBox * 2, halfBox * 2);
        }
    }

    public boolean isCursorOnObject(int x, int y, float[] xArr, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        int x1, x2, y1, y2;
        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
        x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
        y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            return true;
        }

        int X1 = Math.round(graph.GDMgr.getIndexForthePixel(x1));
        int X2 = Math.round(graph.GDMgr.getIndexForthePixel(x2));
        int[] faX = new int[2];
        int[] faY = new int[2];
        double[] stdErr = new double[1];
        stdErr[0] = 0d;
        int stdDevPix = 0;
        if (calculateStandardError(graph, Math.min(X1, X2), Math.max(X1, X2), faX, faY, stdErr,
                rect, OHLCPriority)) {
            stdDevPix = Math.round(units * graph.GDMgr.getPixelGapFortheYValueGap(stdErr[0], pnlID));
        }

        int tempX1 = faX[0];
        int tempX2 = faX[1];
        int tempY1 = faY[0];
        int tempY2 = faY[1];
        if (extendedLeft) {
            float ratio = 1f;
            if ((faX[0] == faX[1])) {
                tempY1 = rect.y + GraphDataManager.titleHeight;
            } else {
                ratio = (float) (faY[1] - faY[0]) / (float) (faX[1] - faX[0]);
                int pixGap = (faX[1] > faX[0]) ? rect.width : -rect.width;
                tempX1 = faX[0] - pixGap;
                tempY1 = faY[0] - Math.round(ratio * pixGap);
            }
        }
        if (extendedRight) {
            float ratio = 1f;
            if ((faX[0] == faX[1])) {
                tempY2 = rect.y + rect.height;
            } else {
                ratio = (float) (faY[1] - faY[0]) / (float) (faX[1] - faX[0]);
                int pixGap = (faX[1] > faX[0]) ? rect.width : -rect.width;
                tempX2 = faX[0] + pixGap;
                tempY2 = faY[0] + Math.round(ratio * pixGap);
            }
        }
        return rect.contains(x, y) && (isOnLineForMove(tempX1, tempY1, tempX2, tempY2, x, y, select) ||
                isOnLineForMove(tempX1, tempY1 + stdDevPix, tempX2, tempY2 + stdDevPix, x, y, select) ||
                isOnLineForMove(tempX1, tempY1 - stdDevPix, tempX2, tempY2 - stdDevPix, x, y, select));
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormat;
        if (currMode) {
            timeFormat = new SimpleDateFormat("HH:mm");
        } else {
            timeFormat = new SimpleDateFormat("dd/MM/yy");
        }
        Date DT1 = new Date(xArray[0]);
        Date DT2 = new Date(xArray[1]);
        return timeFormat.format(DT1) + ", " + timeFormat.format(DT2);
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_STD_ERROR_CHANNEL");
    }

    public boolean isExtendedLeft() {
        return extendedLeft;
    }

    public void setExtendedLeft(boolean extendedLeft) {
        this.extendedLeft = extendedLeft;
    }

    public boolean isExtendedRight() {
        return extendedRight;
    }

    public void setExtendedRight(boolean extendedRight) {
        this.extendedRight = extendedRight;
    }

    public float getUnits() {
        return units;
    }

    public void setUnits(float units) {
        this.units = units;
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        double y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        double x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        double y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        double x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        boolean inside = false;
        double xMin = Math.min(x1, x2);
        double xMax = Math.max(x1, x2);

        //if line line points doesnot contains inside the rectangle
        for (double i = xMin; i < xMax; i = i + 1) {
            double y = m * i + c;
            if (r.contains(i, y)) {
                inside = true;
                break;
            }
        }
        if (!inside) {
            return false;
        }

        int x0 = r.x;
        int y0 = r.y;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //m is negative and intersects the rectangle
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }
        return false;

    }
}
