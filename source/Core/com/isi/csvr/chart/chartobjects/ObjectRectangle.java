package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectRectangularProperty;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:02:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectRectangle extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_RECT;

    public ObjectRectangle() {
        super();
        objType = AbstractObject.INT_RECT;
        drawingOnBackground = true;
        transparent = false;
    }

    public ObjectRectangle(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_RECT;
        drawingOnBackground = true;
        transparent = false;
        isShowingDragToolTip = true;

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectRectangularProperty orp = (ObjectRectangularProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = orp.getLineColor();
            this.penStyle = orp.getLineStyle();
            this.penWidth = orp.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.fillColor = orp.getBackGroundColor();
            this.drawingOnBackground = orp.isSendBehindChrts();
            this.transparent = orp.isTransparent();
            this.isFreeStyle = orp.isFreeStyle();
        }
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        int id = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        double yy0 = graph.GDMgr.getPixelFortheYValue(yArray[0], id);
        double yy1 = graph.GDMgr.getPixelFortheYValue(yArray[1], id);

        float[] index = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        double xx0 = graph.GDMgr.getPixelFortheIndex(index[0]);
        double xx1 = graph.GDMgr.getPixelFortheIndex(index[1]);

        Rectangle rec = null;
        if (xx1 > xx0) {
            rec = new Rectangle((int) xx0, (int) yy0, (int) Math.abs(xx0 - xx1), (int) Math.abs(yy0 - yy1));
        } else {
            rec = new Rectangle((int) xx1, (int) yy1, (int) Math.abs(xx0 - xx1), (int) Math.abs(yy0 - yy1));
        }

        if (!rec.contains(r) && (r.intersects(rec) || r.contains(rec))) {
            return true;
        }
        return false;
    }
}
