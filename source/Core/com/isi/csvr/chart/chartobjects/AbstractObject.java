/*
 * Classname:     AbstractObject.java
 * Written By:    Udaka Liyanapathirana
 * Version:       Chart 1.0_beta
 * Date:          21st August 2002
 * Copyright:     (c) 2002 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

package com.isi.csvr.chart.chartobjects;


import com.isi.csvr.chart.*;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Extending classes must extend the following methods
 * 1. protected void drawDragImage(Graphics g, float[] xArr,
 * int Xadj, int Yadj, int grfLeft, int grfWidth,
 * float bIndex, int grf_Top, int grf_Ht,
 * double minY, float xFactor, double yFactor)
 * 2. protected void drawOnGraphics(Graphics gg, float[] xArr,
 * boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
 * int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex)
 * 3. protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
 * int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor)
 * 4. public String getShortName()
 * 5. protected String getValueString(boolean currMode)
 * <p/>
 * They may also extend the following:
 * 1. Constructor - if additional properties need to be initiallized or exsisting may initiallized differently
 * 2. public void assignValuesFrom(AbstractObject or) - if more properties are added in the subsequent classes
 * 3. public String toString(boolean currMode)
 * 4. protected void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) - if more properties are added in the subsequent classes
 * 5. protected void saveToLayout(org.w3c.dom.Element chart, org.w3c.dom.Document document) - if more properties are added in the subsequent classes
 * <p/>
 * Also shall set the showOnAllPanels to true if necessary on the sub-class
 */

public class AbstractObject implements Serializable {
    public final static long UID_LINE_SLOPE = 1072600473330203901L;
    public final static long UID_LINE_HORIZ = 1072600473330203902L;
    public final static long UID_LINE_VERTI = 1072600473330203903L;
    public final static long UID_SYMBOL = 1072600473330203904L;
    public final static long UID_RECT = 1072600473330203905L;
    public final static long UID_TEXT = 1072600473330203906L;
    public final static long UID_ELLIPSE = 1072600473330203907L;
    public final static long UID_POLYGON = 1072600473330203908L;
    public final static long UID_REGRESSION = 1072600473330203909L;
    public final static long UID_FIBONACCI_ARCS = 1072600473330203910L;
    public final static long UID_FIBONACCI_FANS = 1072600473330203911L;
    public final static long UID_FIBONACCI_RETRACEMENTS = 1072600473330203912L;
    public final static long UID_FIBONACCI_ZONES = 1072600473330203913L;
    public final static long UID_STD_ERROR_CHANNEL = 1072600473330203914L;
    public final static long UID_FIBONACCI_EXTENSIONS = 1072600473330203915L;
    public final static long UID_ARROW_LINE_SLOPE = 1072600473330203916L;
    public final static long UID_ZIG_ZAG = 1072600473330203917L;
    public final static long UID_ARC = 1072600473330203918L;
    public final static long UID_CYCEL_LINE = 1072600473330203919L;
    public final static long UID_ANDREWS_PITCHFORK = 1072600473334542323L;
    public final static int INT_NONE = 0;
    public final static int INT_LINE_SLOPE = 1;
    public final static int INT_LINE_HORIZ = 2;
    public final static int INT_LINE_VERTI = 3;
    public final static int INT_SYMBOL = 4;
    public final static int INT_RECT = 5;
    public final static int INT_TEXT = 6;
    public final static int INT_ELLIPSE = 7;
    public final static int INT_POLYGON = 8;
    public final static int INT_REGRESSION = 9;
    public final static int INT_FIBONACCI_ARCS = 10;
    public final static int INT_FIBONACCI_FANS = 11;
    public final static int INT_FIBONACCI_RETRACEMENTS = 12;
    public final static int INT_FIBONACCI_ZONES = 13;
    public final static int INT_STD_ERROR_CHANNEL = 14;
    public final static int INT_GANN_LINE = 15;
    public final static int INT_GANN_FAN = 16;
    public final static int INT_GANN_GRID = 17;
    public final static int INT_RAFF_REGRESSION = 18;
    public static final int INT_STD_DEV_CHANNEL = 19;
    public static final int INT_EQUI_DIST_CHANNEL = 20;
    public static final int INT_ZOOM_IN = 21;
    public static final int INT_ZOOM_OUT = 22;
    public static final int INT_BOX_ZOOM = 23;
    public static final int INT_DRAG_ZOOM = 24;
    public static final int INT_DATA_LINE_VERTI = 25;
    public static final int INT_RECT_SELECTION = 26;
    public static final int INT_FIBONACCI_EXTENSIONS = 27;
    public static final int INT_ARROW_LINE_SLOPE = 28;
    public static final int INT_ZIG_ZAG = 29;
    public static final int INT_CYCLE_LINE = 30;
    public static final int INT_ARC = 31;
    public static final int INT_ANDREWS_PITCHFORK = 32;
    public final static int ALIGNMENT_TOPLEFT = 1;
    public final static int ALIGNMENT_TOP = 2;
    public final static int ALIGNMENT_TOPRIGHT = 3;
    public final static int ALIGNMENT_LEFT = 4;
    public final static int ALIGNMENT_CENTER = 5;
    public final static int ALIGNMENT_RIGHT = 6;
    public final static int ALIGNMENT_BOTTOMLEFT = 7;
    public final static int ALIGNMENT_BOTTOM = 8;
    public final static int ALIGNMENT_BOTTOMRIGHT = 9;
    public static final int DISPLAY_NORMAL = 0;
    public int DISPLAY_TYPE = AbstractObject.DISPLAY_NORMAL;
    public static final int DISPLAY_SATATIC = 1;
    public static final int DISPLAY_MOVABLE = 2;
    public static final int DISPLAY_HIDE = 3;
    private static final long serialVersionUID = 1072600473330203802L;
    public static Color selectedColor = Color.BLUE;
    public static Color lineColor = Theme.getColor("GRAPH_SYMBOL_COLOR");
    public static BasicStroke selectPen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
    public static BasicStroke dottedPen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_DASH, 0.5f);
    protected static int halfBox = 3;
    protected static int halfSquared = halfBox * halfBox;
    protected static DecimalFormat percentformat = new DecimalFormat("##0.0%");
    public long[] xArray = null;
    public double[] yArray = null;
    public float[] indexArray = null; // corresponds to the original index array without snapping to closest index
    transient public Rectangle rect = null;
    public int pnlID;
    public boolean objectMoving;
    public int movingPtIndex = 0;
    public boolean showOnAllPanels = false;
    public boolean isVisible = true;
    //private boolean onPriceGraph;
    protected boolean onFirstPoint;
    protected boolean selected;
    protected int objType;
    protected byte OHLCPriority = GraphDataManager.INNER_Close;
    transient protected ChartProperties target = null;
    //protected boolean drawingOnBackground = true;
    protected boolean drawingOnBackground = false;
    //protected boolean hidden;
    protected Color color;
    protected float penWidth;
    protected byte penStyle;
    transient protected BasicStroke pen;
    //added by charithn to keep the design
    protected boolean isUsingUserDefault = false;
    protected boolean isFreeStyle = false;
    protected boolean isShowingDragToolTip = false;
    protected StockGraph graph;

    public AbstractObject() {
        selected = false;
        objType = INT_LINE_SLOPE;  //usually lien sop eused as a deafult object.so no point of using as same for arrowLIneslope
        OHLCPriority = GraphDataManager.INNER_Close;
        color = lineColor;
        penWidth = 1f;
        penStyle = GraphDataManager.STYLE_SOLID;
        pen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
    }

    public AbstractObject(long[] xArr, double[] yArr, float[] indexArr, ChartProperties cp, Rectangle r, StockGraph sg) {
        this();
        xArray = xArr;
        yArray = yArr;
        target = cp;
        rect = r;
        indexArray = indexArr;
        graph = sg;

        //set the default mode from the chartoptions window for all the objcets
        //isFreeStyle = ChartOptions.getCurrentOptions().gen_is_free_style;
    }

    //########## used before creating an object - so cannot be extended in sub classes ##########
    public static boolean isMouseDownHasHighPriority(int anObjType) {
        switch (anObjType) {
            case INT_LINE_SLOPE:
            case INT_ARROW_LINE_SLOPE:
            case INT_RECT:
            case INT_RECT_SELECTION:
            case INT_TEXT:
            case INT_POLYGON:
            case INT_ELLIPSE:
            case INT_FIBONACCI_ARCS:
            case INT_FIBONACCI_FANS:
            case INT_FIBONACCI_RETRACEMENTS:
            case INT_FIBONACCI_EXTENSIONS:

                return true;
        }
        return false;
    }

    public static AbstractObject CreateObject(int objType, long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp,
                                              Rectangle r, ArrayList panels, StockGraph graph) {
        switch (objType) {
            case INT_LINE_SLOPE:
                return new ObjectLineSlope(xArr, yArr, indexArray, cp, r, graph);
            case INT_ARROW_LINE_SLOPE:
                return new ObjectArrowLineSlope(xArr, yArr, indexArray, cp, r, graph);
            case INT_LINE_HORIZ:
                return new ObjectLineHorizontal(xArr, yArr, cp, r, graph);
            case INT_LINE_VERTI:
                return new ObjectLineVertical(xArr, yArr, indexArray, cp, r, panels, graph);
            case INT_SYMBOL:
                return new ObjectSymbol(xArr, yArr, indexArray, cp, r, graph);
            case INT_RECT:
                return new ObjectRectangle(xArr, yArr, indexArray, cp, r, graph);
            case INT_TEXT:
                return new ObjectTextNote(xArr, yArr, indexArray, cp, r, graph);
            case INT_ELLIPSE:
                return new ObjectEllipse(xArr, yArr, indexArray, cp, r, graph);
            case INT_POLYGON:
                return new ObjectPolygon(xArr, yArr, cp, r, graph);
            case INT_REGRESSION:
                return new ObjectRegression();
            case INT_FIBONACCI_ARCS:
                return new ObjectFibonacciArcs(xArr, yArr, indexArray, cp, r, graph);
            case INT_FIBONACCI_FANS:
                return new ObjectFibonacciFans(xArr, yArr, indexArray, cp, r, graph);
            case INT_FIBONACCI_RETRACEMENTS:
                return new ObjectFibonacciRetracements(xArr, yArr, indexArray, cp, r, graph);
            case INT_FIBONACCI_EXTENSIONS:
                return new ObjectFibonacciExtensions(xArr, yArr, indexArray, cp, r, graph);
            case INT_FIBONACCI_ZONES:
                return new ObjectFibonacciZones(xArr, yArr, cp, r, panels, graph);
            case INT_GANN_LINE:
                return new ObjectGannLine(xArr, yArr, cp, r, graph);
            case INT_GANN_FAN:
                return new ObjectGannFan(xArr, yArr, cp, r, graph);
            case INT_GANN_GRID:
                return new ObjectGannGrid(xArr, yArr, cp, r, graph);
            case INT_STD_ERROR_CHANNEL: //Just needed to pop object properties
                return new ObjectStandardError();
            case INT_RAFF_REGRESSION: //Just needed to pop object properties
                return new ObjectRaffRegression();
            case INT_STD_DEV_CHANNEL: //Just needed to pop object properties
                return new ObjectStandardDeviationChannel();
            case INT_EQUI_DIST_CHANNEL: //Just needed to pop object properties
                return new ObjectEquiDistantChannel();
            case INT_DATA_LINE_VERTI:
                return new ObjectDataLineVertical(xArr, yArr, indexArray, cp, r, panels, graph);
            case INT_RECT_SELECTION:
                return new ObjectRectangularSelection(xArr, yArr, cp, r);
            case INT_ZIG_ZAG:
                return new ObjectZigZag(xArr, yArr, indexArray, cp, r, graph);
            case INT_CYCLE_LINE:
                return new ObjectCycleLine(xArr, yArr, indexArray, cp, r, graph);
            case INT_ARC:
                return new ObjectArc(xArr, yArr, indexArray, cp, r, graph);
            case INT_ANDREWS_PITCHFORK:
                return new ObjectAndrewsPitchfork(xArr, yArr, indexArray, cp, r, graph);
            default:
                return new ObjectLineSlope(xArr, yArr, indexArray, cp, r, graph); //why this set as default ? sathyajith  02/02/2009
        }
    }

    public static AbstractObject CreateObject(int objType, long[] xArr, double[] yArr, ChartProperties cp,
                                              Rectangle r, ArrayList panels, StockGraph graph) {
        switch (objType) {
            case INT_REGRESSION:
                return new ObjectRegression(xArr, yArr, cp, r, graph);
            case INT_STD_ERROR_CHANNEL:
                return new ObjectStandardError(xArr, yArr, cp, r, graph);
            case INT_RAFF_REGRESSION:
                return new ObjectRaffRegression(xArr, yArr, cp, r, graph);
            case INT_STD_DEV_CHANNEL:
                return new ObjectStandardDeviationChannel(xArr, yArr, cp, r, graph);
            case INT_EQUI_DIST_CHANNEL:
                return new ObjectEquiDistantChannel(xArr, yArr, cp, r, graph);
            default:
                return new ObjectLineSlope(xArr, yArr, null, cp, r, graph);
        }
    }

    public static Point getAlignedPosition(int alignment, int w, int h,
                                           int strW, int strH, int ascent) {
        Point pt = new Point();
        switch (alignment) {
            case ALIGNMENT_CENTER:
                pt.x = (w - strW) / 2;
                pt.y = (h - strH) / 2 + ascent;
                break;
        }
        return pt;
    }

    public void assignValuesFrom(AbstractObject or) {
        //to be extended by the sub classes extending this if additional properties are added subsequently
        xArray = or.xArray;
        yArray = or.yArray;
        indexArray = or.indexArray;

        onFirstPoint = or.onFirstPoint;
        selected = or.selected;
        objType = or.objType;
        OHLCPriority = or.OHLCPriority;
        target = or.target;
        rect = or.rect;
        pnlID = or.pnlID;
        drawingOnBackground = or.drawingOnBackground;

        color = or.color;
        penWidth = or.penWidth;
        penStyle = or.penStyle;
        pen = or.pen;
        isUsingUserDefault = or.isUsingUserDefault;
        isFreeStyle = or.isFreeStyle;
    }

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    public void drawDragImage(Graphics g, float[] xArr,
                              int Xadj, double Yadj, float bIndex,
                              double minY, float xFactor, double yFactor, boolean snapToPrice, int[] snapPixYArr) {

        drawDragImage(g, xArr, Xadj, Yadj, rect.x + StockGraph.clearance,
                rect.width - 2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                        StockGraph.clearance, rect.height - StockGraph.titleHeight -
                        2 * StockGraph.clearance, minY, xFactor, yFactor, snapToPrice, snapPixYArr);
    }

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        //to be implemented by the sub classes extending this
    }

    public int getMovingPtIndex() {
        return movingPtIndex;
    }

    public void setMovingPtIndex(int index) {
        movingPtIndex = index;
    }

    public int getPixelFortheYValue(double value, Rectangle rect, double minY, double yFactor, int grfBottom) {
        int pixel;
        WindowPanel r = (WindowPanel) rect;
        if (r.isSemiLog() && (minY > 0)) {
            pixel = grfBottom - (int) Math.round((Math.log(value) - Math.log(minY)) * yFactor);
        } else {
            pixel = grfBottom - (int) Math.round((value - minY) * yFactor);
        }
        return pixel;
    }

    public double getYValueForthePixel(int pixel, Rectangle rect, double minY, double yFactor, int grfBottom) {
        double Yvalue;
        WindowPanel r = (WindowPanel) rect;

        if (r.isSemiLog() && minY > 0) {
            Yvalue = Math.exp((grfBottom - pixel) / yFactor + Math.log(minY));
        } else {
            Yvalue = (grfBottom - pixel) / yFactor + minY;
        }

        return Yvalue;
    }

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, float bIndex, double minY,
                               float xFactor, double yFactor, float eIndex) {
        drawOnGraphics(gg, xArr, isPrinting, rect.x + StockGraph.clearance, rect.width -
                2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                StockGraph.clearance, rect.height - StockGraph.titleHeight -
                2 * StockGraph.clearance, minY, xFactor, yFactor, eIndex);
    }

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        //to be implemented by the sub classes extending this
    }

    protected boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select, boolean isMove) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);

        if (!isMove) {
            return false;
        }
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = 0;
            if (movingPtIndex == 0) {
                //System.out.println("wrong");
            }
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
            onFirstPoint = false;
            if (!selected)
                selected = select;
            movingPtIndex = 1;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = 0;      //TODO :CHARITH-nned to finalize
            if (movingPtIndex == 0) {
                //System.out.println("wrong");
            }
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    //this is same as isOnTheLine except it doesnt check for resizing
    protected boolean isOnLineForMove(int x1, int y1, int x2, int y2, int x, int y, boolean select) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = 0;
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    public boolean isShownOnAllPanels() {
        return showOnAllPanels;
    }

    /*
    xArray here is in pixels
    */
    public boolean isCursorOnObject(int x, int y, float[] xArray, boolean select,
                                    float bIndex, double minY, float xFactor, double yFactor, boolean isMove) {
        return isCursorOnObject(x, y, xArray, select, rect.x + StockGraph.clearance,
                rect.width - 2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                        StockGraph.clearance, rect.height - 2 * StockGraph.clearance - StockGraph.titleHeight,
                minY, xFactor, yFactor, isMove);
    }

    /*
    xArray here is in pixels
    */
    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        // to be implemented by the sub classes extending this
        if (!rect.contains(x, y)) return false;
        return false;
    }

    public String getShortName() {
        // to be overriden by the sub classes implementing this
        return "";
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle rect, StockGraph graph) {
        // to be implemented by the sub classes extending this
        return false;
    }

    //gives a objects specific properties in display tooltip
    public ArrayList<ToolTipDataRow> getToolTipRows() {
        // to be implemented by the sub classes extending this
        return null;
    }

    public long[] getXArr() {
        return xArray;
    }

    //xArray
    public void setXArr(long[] xArr) {
        xArray = xArr;
    }

    public double[] getYArr() {
        return yArray;
    }

    //yArray
    public void setYArr(double[] yArr) {
        yArray = yArr; //actual points
    }

    public float[] getIndexArray() {
        return indexArray;
    }

    public void setIndexArray(float[] indexArray) {
        this.indexArray = indexArray;
    }

    //objType
    public int getObjType() {
        return objType;
    }

    public void setObjType(int i) {
        objType = i;
    }

    //selected
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean bool) {
        selected = bool;
    }

    //color
    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    //penStyle
    public byte getPenStyle() {
        return penStyle;
    }

    public void setPenStyle(byte ps) {
        penStyle = ps;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    public void setPenStyle(Byte PS) {
        setPenStyle(PS.byteValue());
    }

    //penWidth
    public float getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(float pw) {
        penWidth = pw;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    //pen
    public BasicStroke getPen() {
        return pen;
    }

    protected void setPen(BasicStroke p) {
        pen = p;
    }

    //objectMoving
    public boolean isObjectMoving() {
        return objectMoving;
    }

    //OhlcPriority
    public byte getOHLCPriority() {
        return OHLCPriority;
    }

    public void setOHLCPriority(byte p) {
        OHLCPriority = p;
    }

    //target
    public ChartProperties getTarget() {
        return target;
    }

    //rect
    public Rectangle getRect() {
        return rect;
    }

    public void setRect(Rectangle r) {
        rect = r;
    }

    //drawingOnBackground
    public boolean isDrawingOnBackground() {
        return drawingOnBackground;
    }

    public void setDrawingOnBackground(boolean onBackGround) {
        drawingOnBackground = onBackGround;
    }

    public boolean isUsingUserDefault() {
        return isUsingUserDefault;
    }

    public void setUsingUserDefault(boolean usingUserDefault) {
        isUsingUserDefault = usingUserDefault;
    }

    public boolean isFreeStyle() {
        return isFreeStyle;
    }

    public void setFreeStyle(boolean freeStyle) {
        isFreeStyle = freeStyle;
    }

    public boolean isShowingDragToolTip() {
        return isShowingDragToolTip;
    }

    public void setShowingDragToolTip(boolean showingDragToolTip) {
        isShowingDragToolTip = showingDragToolTip;
    }

    public String toString(boolean currMode) {
        // to be overriden by the sub classes implementing this
        if (getValueString(currMode).length() > 0) {
            return getShortName() + " (" + getValueString(currMode) + ")";
        } else {
            return getShortName();
        }
    }

    protected String getValueString(boolean currMode) {
        // to be overriden by the sub classes implementing this
        return "";
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        String strXArray = TemplateFactory.loadProperty(xpath, document, preExpression + "/xArray");
        if ((strXArray != null) && (!strXArray.equals(""))) {
            String[] saXArray = strXArray.split(";");
            this.xArray = new long[saXArray.length];
            for (int i = 0; i < this.xArray.length; i++) {
                this.xArray[i] = Long.parseLong(saXArray[i]);
            }
        }
        String strYArray = TemplateFactory.loadProperty(xpath, document, preExpression + "/yArray");
        if ((strYArray != null) && (!strYArray.equals(""))) {
            String[] saYArray = strYArray.split(";");
            this.yArray = new double[saYArray.length];
            for (int i = 0; i < this.yArray.length; i++) {
                this.yArray[i] = Double.parseDouble(saYArray[i]);
            }
        }
        //load oroginal index array. added by charithn
        String strxIndexArray = TemplateFactory.loadProperty(xpath, document, preExpression + "/xIndexArray");
        if ((strxIndexArray != null) && (!strxIndexArray.equals(""))) {
            String[] saXIndexArray = strxIndexArray.split(";");
            this.indexArray = new float[saXIndexArray.length];
            for (int i = 0; i < this.indexArray.length; i++) {
                this.indexArray[i] = Float.parseFloat(saXIndexArray[i]);
            }
        }
        selected = false;

        this.OHLCPriority = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/OHLCPriority"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/Color").split(",");
        this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
        this.setPenStyle(Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/LineStyle")));
        this.setPenWidth(Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/LineThickness")));
        String strDrawOnBack = TemplateFactory.loadProperty(xpath, document, preExpression + "/DrawingOnBackground");
        if ((strDrawOnBack != null) && (!strDrawOnBack.equals(""))) {
            this.drawingOnBackground = Boolean.parseBoolean(strDrawOnBack);
        }
        String strFreeStyle = TemplateFactory.loadProperty(xpath, document, preExpression + "/IsFreeStyle");
        if ((strFreeStyle != null) && (!strFreeStyle.equals(""))) {
            this.isFreeStyle = Boolean.parseBoolean(strFreeStyle);
        }
    }

    public void saveToLayout(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        String strXArray = "";
        if ((xArray != null) && (xArray.length > 0)) {
            strXArray = Long.toString(xArray[0]);
            for (int i = 1; i < this.xArray.length; i++) {
                strXArray = strXArray + ";" + Long.toString(xArray[i]);
            }
        }
        TemplateFactory.saveProperty(chart, document, "xArray", strXArray);
        String strYArray = "";
        if ((yArray != null) && (yArray.length > 0)) {
            strYArray = Double.toString(yArray[0]);
            for (int i = 1; i < this.yArray.length; i++) {
                strYArray = strYArray + ";" + Double.toString(yArray[i]);
            }
        }
        TemplateFactory.saveProperty(chart, document, "yArray", strYArray);

        //saving original index array. added by charithn
        String strXIndexArray = "";
        if ((indexArray != null) && (indexArray.length > 0)) {
            strXIndexArray = Double.toString(indexArray[0]);
            for (int i = 1; i < this.indexArray.length; i++) {
                strXIndexArray = strXIndexArray + ";" + Double.toString(indexArray[i]);
            }
        }
        TemplateFactory.saveProperty(chart, document, "xIndexArray", strXIndexArray);

        TemplateFactory.saveProperty(chart, document, "OHLCPriority", Byte.toString(this.OHLCPriority));
        String strColor = this.color.getRed() + "," + this.color.getGreen() + "," + this.color.getBlue();
        TemplateFactory.saveProperty(chart, document, "Color", strColor);
        TemplateFactory.saveProperty(chart, document, "LineStyle", Byte.toString(this.penStyle));
        TemplateFactory.saveProperty(chart, document, "LineThickness", Float.toString(this.penWidth));
        TemplateFactory.saveProperty(chart, document, "DrawingOnBackground", Boolean.toString(this.drawingOnBackground));
        TemplateFactory.saveProperty(chart, document, "IsFreeStyle", Boolean.toString(this.isFreeStyle));
    }

    protected boolean isCursorOnMoveHandles(int x1, int y1, int x2, int y2, int x, int y, boolean select) {
        boolean result = false;
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
//			movingPoint.setLocation(x1, y1);
//			fixedPoint.setLocation(x2, y2);
            movingPtIndex = 0;
            result = true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
//			movingPoint.setLocation(x2, y2);
//			fixedPoint.setLocation(x1, y1);
            movingPtIndex = 1;
            result = true;
        } else if (((x1 - x) * (x1 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
//			movingPoint.setLocation(x1, y2);
//			fixedPoint.setLocation(x2, y1);
            movingPtIndex = 2;
            result = true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
//			movingPoint.setLocation(x2, y1);
//			fixedPoint.setLocation(x1, y2);
            movingPtIndex = 3;
            result = true;
        }
        if (result) {
            if (!selected)
                selected = select;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        }
        return false;
    }

    public void assignDefaultValues() {
        //selected = false;
        if (objType == INT_LINE_SLOPE || objType == INT_ARROW_LINE_SLOPE || objType == INT_LINE_VERTI || objType == INT_TEXT) {
            isFreeStyle = false;
        } else if (objType == INT_FIBONACCI_EXTENSIONS) {
            ((ObjectFibonacciExtensions) this).font = Theme.getDefaultFont(Font.PLAIN, 9);
            Hashtable<Integer, FibonacciExtensionLine> lines = ((ObjectFibonacciExtensions) this).getDefaultFibonacciExtensionLines();
            for (int i = 0; i < lines.size(); i++) {
                FibonacciExtensionLine line = lines.get(i);
                ((ObjectFibonacciExtensions) this).fibonacciLines.put(i, line);     // set the fibonacci line with objects
            }
        } else if (objType == INT_FIBONACCI_RETRACEMENTS) {
            ((ObjectFibonacciRetracements) this).font = Theme.getDefaultFont(Font.PLAIN, 9);
            Hashtable<Integer, FibonacciRetracementiLine> lines = ((ObjectFibonacciRetracements) this).getDefaultFibonacciLines();
            for (int i = 0; i < lines.size(); i++) {
                FibonacciRetracementiLine line = lines.get(i);
                ((ObjectFibonacciRetracements) this).fibonacciLines.put(i, line);     // set the fibonacci line with objects
            }
        }
        OHLCPriority = GraphDataManager.INNER_Close;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        penWidth = 1f;
        penStyle = GraphDataManager.STYLE_SOLID;
        pen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
    }


}