package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.StockGraph;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:13:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectPolygon extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_POLYGON;

    public ObjectPolygon() {
        super();
        objType = AbstractObject.INT_POLYGON;
    }

    public ObjectPolygon(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = AbstractObject.INT_POLYGON;
    }

}
