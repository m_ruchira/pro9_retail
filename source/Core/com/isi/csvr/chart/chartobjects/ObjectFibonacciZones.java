package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:21:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFibonacciZones extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_FIBONACCI_ZONES;

    private ArrayList panels;

    public ObjectFibonacciZones() {
        super();
        objType = AbstractObject.INT_FIBONACCI_ZONES;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectFibonacciZones(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, ArrayList panels, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = AbstractObject.INT_FIBONACCI_ZONES;
        this.panels = panels;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
        }
    }

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            grfLeft = rect.x + StockGraph.clearance;
            grf_Top = rect.y;
            grf_Ht = rect.height;
            grfWidth = rect.width;
            int y2 = grf_Top + grf_Ht;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft - Xadj;
            int preVal = 0;
            int curVal = 1;
            int tmpVal = 0;
            int sum = 0;
            g.drawLine(x1, grf_Top, x1, y2);
            while (true) {
                sum += curVal;
                //System.out.println("���� writing fibonaaci value "+sum);
                if (xFactor <= 0.0) break;//addded by sathyajith to avoid  unended looping for no data charts
                int x = x1 + Math.round(sum * xFactor);
                if (x > grfLeft + grfWidth) break;
                g.drawLine(x, grf_Top, x, y2);
                tmpVal = curVal;
                curVal += preVal;
                preVal = tmpVal;
            }
        }
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        boolean drawSelected = selected && !isPrinting;

        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            grfLeft = rect.x + StockGraph.clearance;
            grf_Top = rect.y;
            grf_Ht = rect.height;
            grfWidth = rect.width;
            int y2 = grf_Top + grf_Ht;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            int preVal = 0;
            int curVal = 1;
            int tmpVal = 0;
            int sum = 0;
            g.setClip(rect.x, rect.y, rect.width, rect.height);
            g.drawLine(x1, grf_Top, x1, y2);
            while (true) {
                sum += curVal;
                if (xFactor <= 0.0) break;//addded by sathyajith to avoid  unended looping for no data charts
                int x = Math.round((xArr[0] + sum - bIndex) * xFactor) + grfLeft;
                if (sum > eIndex) break;
                g.drawLine(x, grf_Top, x, y2);
                if (drawSelected) {
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    g.fillRect(x - halfBox, (grf_Top + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                    g.drawRect(x - halfBox, (grf_Top + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                }
                tmpVal = curVal;
                curVal += preVal;
                preVal = tmpVal;
            }
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            grfLeft = rect.x + StockGraph.clearance;
            grf_Top = rect.y;
            grf_Ht = rect.height;
            grfWidth = rect.width;
            int y1 = grf_Top;
            int y2 = grf_Top + grf_Ht;
            int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
            int preVal = 0;
            int curVal = 1;
            int tmpVal = 0;
            int sum = 0;
            if (isOnLineForMove(x1, y1, x1, y2, x, y, select)) {
                this.rect = rect;
                return true;
            }
            while (true) {
                sum += curVal;
                //System.out.println("���� writing fibonaaci value "+sum);
                if (xFactor <= 0.0) break;//addded by sathyajith to avoid  unended looping for no data chart
                int x0 = Math.round((xArray[0] + sum - bIndex) * xFactor) + grfLeft;
                if (x0 > grfLeft + grfWidth) break;
                if (isOnLineForMove(x0, y1, x0, y2, x, y, select)) {
                    this.rect = rect;
                    return true;
                }
                tmpVal = curVal;
                curVal += preVal;
                preVal = tmpVal;
            }
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_FIB_ZONES");
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        Date D1;
        D1 = new Date(xArray[0]);
        return timeFormatter.format(D1);
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        int x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        if (r.x > x1 || (x1 > r.x && x1 < r.x + r.width)) {
            return true;
        }
        return false;

    }


}
