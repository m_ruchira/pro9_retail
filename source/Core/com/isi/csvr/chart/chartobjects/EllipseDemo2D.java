package com.isi.csvr.chart.chartobjects;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Arc2D;
import java.awt.geom.QuadCurve2D;

public class EllipseDemo2D extends JPanel {
    final static BasicStroke stroke = new BasicStroke(2.0f);

    public static void main(String s[]) {
        JFrame f = new JFrame("");
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        JPanel applet = new EllipseDemo2D();
        f.getContentPane().add("Center", applet);
        //applet.init();
        f.pack();
        f.setSize(new Dimension(600, 600));
        f.show();
    }

    public void init() {
        setBackground(Color.white);
        setForeground(Color.white);
    }

    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setPaint(Color.gray);
        int x = 0;
        int y = 0;

        g2.setStroke(stroke);
        //g2.draw(new Ellipse2D.Double(x, y, 200, 200));

        QuadCurve2D.Double curve = new QuadCurve2D.Double(20, 20, 200, 500, 300, 20);
        g2.draw(new Arc2D.Double(x, y, 200, 200, 0, 180, Arc2D.OPEN));

        //g2.drawArc(x, y,300,300, 0 , 180);
        //g2.draw(curve);

        //g2.drawRect(200,200, 2,2);
        g2.drawString("Ellipse2D", x, 250);

    }
}