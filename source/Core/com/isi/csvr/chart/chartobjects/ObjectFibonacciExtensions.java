package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectFibExtProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Sep 16, 2008
 * Time: 7:29:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFibonacciExtensions extends AbstractObject {

    public static final int ID_TREND = 0;
    public static final int ID_RETR = 1;
    public static final int ID_BASE = 2;
    public static final int ID_BEGIN = 3;
    public static final int ID_END = 4;
    public static final int ID_ONE = 5;
    public static final int ID_TWO = 6;
    public static final int ID_THREE = 7;
    public static final int ID_FOUR = 8;
    public static final int ID_FIVE = 9;
    public static final int ID_SIX = 10;
    public static final int ID_SEVEN = 11;
    public static final int ID_EIGHT = 12;
    public static final int ID_NINE = 13;
    public static final int ID_TEN = 14;
    public static final int ID_ELEVEN = 15;
    public static final int ID_TWELVE = 16;
    public static final int ID_THIRTEEN = 17;
    private static final long serialVersionUID = AbstractObject.UID_FIBONACCI_EXTENSIONS;
    public static float[] defaultLevels = new float[]{.382f, .618f, 1.382f, 1.618f};
    public static int CAPACITY = 18;
    public static double EXCLUDE_VALUE = 99999999D;
    protected Hashtable fibonacciLines;
    protected Font font;

    protected int noOfPointsFilled;
    DecimalFormat priceFormat = new DecimalFormat("###,##0.00");
    SimpleDateFormat timeFormatter = null;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();
    private boolean isUsingSameProperties = false;

    public ObjectFibonacciExtensions() {

    }

    public ObjectFibonacciExtensions(long[] xarr, double[] yarr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xarr, yarr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_FIBONACCI_EXTENSIONS;
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        fibonacciLines = new Hashtable(CAPACITY);
        noOfPointsFilled = 0;
        isShowingDragToolTip = true;
        drawingOnBackground = false;

        Hashtable<Integer, FibonacciExtensionLine> lines = getDefaultFibonacciExtensionLines();

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectFibExtProperty op = (ObjectFibExtProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            lines = op.getTable();     //overrides the default properties here....
            isUsingSameProperties = op.isUsingSameProperties();
            isFreeStyle = op.isFreeStyleMode();
        }
        for (int i = 0; i < lines.size(); i++) {
            FibonacciExtensionLine line = lines.get(i);
            fibonacciLines.put(i, line);     // set the fibonacci line with objects
        }
    }

    public Hashtable getFibonacciLines() {
        return fibonacciLines;
    }

    public void setFibonacciLines(Hashtable fibonacciLines) {
        this.fibonacciLines = fibonacciLines;
    }

    public Hashtable sortHashTable(Hashtable table) {
        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        Hashtable tableSort = new Hashtable();
        for (int key : UIkeys) {
            FibonacciExtensionLine line = (FibonacciExtensionLine) table.get(key);
            tableSort.put(key, line);
        }

        return tableSort;
    }

    public void assignValuesFrom(AbstractObject or) {
        super.assignValuesFrom(or);
        if (or instanceof ObjectFibonacciExtensions) {
            //this.fibonacciLines.clear();
            this.fibonacciLines = sortHashTable(((ObjectFibonacciExtensions) or).getFibonacciLines());
            this.font = ((ObjectFibonacciExtensions) or).font;
            isUsingSameProperties = ((ObjectFibonacciExtensions) or).isUsingSameProperties;
        }
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_FIB_EXTENSIONS");
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

    public boolean isUsingSameProperties() {
        return isUsingSameProperties;
    }

    public void setUsingSameProperties(boolean value) {
        this.isUsingSameProperties = value;
    }


    public String getValueString(boolean currentMode) {
        SimpleDateFormat formatter = null;
        if (currentMode) {
            formatter = new SimpleDateFormat("HH:mm");
        } else {
            formatter = new SimpleDateFormat("dd/mm/yyyy");
        }
        Date date = new Date(xArray[0]);
        //return formatter.format(date);
        return "";
    }

    /*
    isCursorOnObject(x, y, xArray, select, rect.x + StockGraph.clearance,
                rect.width - 2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                StockGraph.clearance, rect.height - 2 * StockGraph.clearance - StockGraph.titleHeight,
                minY, xFactor, yFactor);

    int grfLeft = rect.x + StockGraph.clearance
    int grfWidth = rect.width - 2 * StockGraph.clearance
    int grf_top = rect.y + StockGraph.titleHeight + StockGraph.clearance
    int grf_Ht = rect.height - 2 * StockGraph.clearance - StockGraph.titleHeight
     */
    public boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        if (!rect.contains(x, y) || !isMove) return false;
        int x1, x2, y1, y2, x3, y3;  //xarray is the  graph store index array

        try {
            /* =========== line between first and second points ========== */
            if (xArray[0] < xArray[1]) {  // x1 < x2

                //converting xarray index array to pixel coorinates
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates

                if (isOnTheLine(x1, y1, x2, y2, x, y, select, 0)) {
                    return true;
                }
            } else {   // x2 < x1
                x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates

                if (isOnTheLine(x2, y2, x1, y1, x, y, select, 0)) {
                    return true;
                }
            }
            /* ====== end of line between first and second points ========= */

            /* ====== line between second and third point ================= */
            if (xArray[1] < xArray[2]) {  // x2 < x3

                //converting xarray index array to pixel coorinates
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates
                x3 = Math.round((xArray[2] - bIndex) * xFactor) + grfLeft; // x3 in pixel coordinates

                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y3 in pixel coordinates

                if (isOnTheLine(x2, y2, x3, y3, x, y, select, 1)) {
                    return true;
                }
            } else {   // x3 < x2
                x2 = Math.round((xArray[2] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates
                x3 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft; // x3 in pixel coordinates

                y2 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                y3 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y3 in pixel coordinates

                if (isOnTheLine(x3, y3, x2, y2, x, y, select, 1)) {
                    return true;
                }
            }
            /* ====== end of line between second and third points =========== */

            if (x1 != x2) {
                int len = y2 - y1;
                Hashtable<Integer, FibonacciExtensionLine> lines = getDefaultFibonacciExtensionLines();
                for (int i = ID_BEGIN; i < lines.size(); i++) {

                    FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(i);
                    if (line.isEnabled()) {
                        int y0 = y3 + (int) Math.round(line.getPercentageValue() * len);
                        if (isOnLineForMove(x3, y0, x3 + grfWidth, y0, x, y, select)) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("============ exception - isCursorOnObject() =========");
        }
        return false;
    }

    /*
    executes when an object is already drawn and trying to drag the image.
    GraphDataManager - drawDraggedImageForHiLightedObject

    drawDragImage(g, xArr, Xadj, Yadj, rect.x + StockGraph.clearance,
                (rect.width - 2 * StockGraph.clearance),  bIndex,  (rect.y +  StockGraph.titleHeight +  StockGraph.clearance),
                (rect.height - StockGraph.titleHeight - 2 * StockGraph.clearance), minY, xFactor, yFactor, false, null);

      Xadj = (int) (MouseDownPoint.getX() - PreMouseDragPoint.getX());
      Yadj = (int) (MouseDownPoint.getY() - PreMouseDragPoint.getY());
      grfLeft = rect.x + StockGraph.clearance
      grfWidth =  rect.width - 2 * StockGraph.clearance
      int grf_top = rect.y + StockGraph.titleHeight + StockGraph.clearance
      int grf_Ht = rect.height - 2 * StockGraph.clearance - StockGraph.titleHeight
     */
    public void drawDragImage(Graphics g, float[] xArr,
                              int Xadj, double Yadj, int grfLeft, int grfWidth,
                              float bIndex, int grf_Top, int grf_Ht,
                              double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
        int x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates
        int x3 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x3 in pixel coordinates

        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
        int y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
        int y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y3 in pixel coordinates

        if (objectMoving) {
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;
            x3 = x3 - Xadj;
            y1 = y1 - (int) Yadj;
            y2 = y2 - (int) Yadj;
            y3 = y3 - (int) Yadj;
        } else {
            if (movingPtIndex == 0) {
                x1 = x1 - Xadj;
                y1 = y1 - (int) Yadj;
            } else if (movingPtIndex == 1) {
                x2 = x2 - Xadj;
                y2 = y2 - (int) Yadj;
            } else if (movingPtIndex == 2) {
                x3 = x3 - Xadj;
                y3 = y3 - (int) Yadj;
            }
        }

        g.drawLine(x1, y1, x2, y2);  // drawing the trend line
        g.drawLine(x2, y2, x3, y3);  //drawing the retracement line

        if (x1 != x2) {
            /*if (x1 > x2) {
                x1 = x2;
                x2 = y1; //used as a tmp var
                y1 = y2;
                y2 = x2;
            }*/
            int len = y2 - y1;
            int yMax = Integer.MIN_VALUE;
            int yMin = Integer.MAX_VALUE;
            for (int i = ID_BEGIN; i < fibonacciLines.size(); i++) {
                FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(i);
                if (line.isEnabled()) {
                    int y = y3 + (int) Math.round(line.getPercentageValue() * len);
                    g.drawLine(x3, y, x3 + grfWidth, y);   // drawing fibonacci lines
                    if (yMax < y) {
                        yMax = y;
                    }
                    if (yMin > y) {
                        yMin = y;
                    }
                }
            }
            //drawing the vertical line
            g.drawLine(x3, yMin, x3, yMax);
        }

        //adding tooltip data rows
        dataRows.clear();

        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        float index2 = bIndex + (x2 - grfLeft) / xFactor;
        float index3 = bIndex + (x3 - grfLeft) / xFactor;

        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2, index3});
        Date D1, D2 = null, D3 = null;
        D1 = new Date(newXarray[0]);
        if (newXarray.length > 1) {
            D2 = new Date(newXarray[1]);
            D3 = new Date(newXarray[2]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);

        double begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
        double end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
        double retrEnd = getYValueForthePixel(y3, rect, minY, yFactor, grf_Ht + grf_Top);
        String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
        String endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

        String retrEndDate = timeFormatter.format(D3);
        String retrEndPrice = String.valueOf(graph.GDMgr.formatPriceField(retrEnd, ((WindowPanel) rect).isInThousands()));

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("RETR_END_DATE"), retrEndDate));
        dataRows.add(new ToolTipDataRow(Language.getString("RETR_END_PRICE"), retrEndPrice));


        int len = (y2 - y1);
        for (int i = ID_ONE; i < fibonacciLines.size(); i++) {

            FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(i);
            if (line.isEnabled()) {

                int y = y2 - (int) Math.round(line.getPercentageValue() * len);

                String val;

                if (newXarray[0] < newXarray[1]) {
                    val = priceFormat.format(begin + (1.0 - line.getPercentageValue()) * (end - begin));
                } else {
                    val = priceFormat.format(end + (1.0 - line.getPercentageValue()) * (begin - end));
                }
                dataRows.add(new ToolTipDataRow(String.valueOf((line.getPercentageValue() * 1000) / 10) + "%", String.valueOf(val)));
            }
        }
    }

    protected void drawOnGraphics(Graphics g, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {

        Graphics2D gg = (Graphics2D) g;
        boolean drawSelected = selected && !isPrinting;

        gg.setColor(color);
        gg.setStroke(pen);
        gg.setClip(rect);

        int x1, x2 = 0, y1 = 0, y2 = 0, x3, y3;

        try {
            if (xArr[0] < xArr[1]) {  // x1 < x2

                //converting xarray index array to pixel coorinates
                x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates

            } else {   // x2 < x1
                x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
            }

            if (xArr[1] < xArr[2]) {  // x2 < x3

                //converting xarray index array to pixel coorinates
                //x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x3 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                //y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates

            } else {   // x3 < x2
                // x2 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                x3 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                //y2 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                y3 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
            }

            /* ===================== important part ================= */
            int len = (y2 - y1);
            int yMin = Integer.MAX_VALUE;
            int yMax = Integer.MIN_VALUE;

            Date D1 = null, D2 = null, D3 = null;
            D1 = new Date(xArray[0]);
            if (xArray.length > 2) {
                D2 = new Date(xArray[1]);
                D3 = new Date(xArray[2]);
            }
            if (graph.isCurrentMode()) {
                timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
            } else {
                timeFormatter = new SimpleDateFormat("dd/MM/yy");
            }

            String trendBeginDate = timeFormatter.format(D1);
            String trendEndDate = timeFormatter.format(D2);
            String retrEndDate = timeFormatter.format(D3);

            String trendBeginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
            String trendEndPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());
            String retrEndPrice = graph.GDMgr.formatPriceField(yArray[2], ((WindowPanel) rect).isInThousands());


            dataRows.clear();
            dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), trendBeginDate));
            dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), trendBeginPrice));
            dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), trendEndDate));
            dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), trendEndPrice));
            dataRows.add(new ToolTipDataRow(Language.getString("RETR_END_DATE"), retrEndDate));
            dataRows.add(new ToolTipDataRow(Language.getString("RETR_END_PRICE"), retrEndPrice));

            if (len != 0) {
                gg.setStroke(dottedPen);
                gg.setFont(font);
                FontMetrics fm = g.getFontMetrics(font);
                int strHt = fm.getHeight() / 3;

                for (int i = ID_BEGIN; i < fibonacciLines.size(); i++) {

                    FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(i);
                    if (line.isEnabled()) {

                        //set line colors, line style and line thickness
                        gg.setColor(line.getLineColor());
                        BasicStroke pen = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                        gg.setStroke(pen);

                        int y = y3 + (int) Math.round(line.getPercentageValue() * len); // y in pixels

                        if (yMax < y) {
                            yMax = y;
                        }
                        if (yMin > y) {
                            yMin = y;
                        }

                        // y = p3 + (p2-p1) * percentage
                        String PriceVal = priceFormat.format(yArray[2] + (line.getPercentageValue()) * (yArray[1] - yArray[0]));

                        dataRows.add(new ToolTipDataRow(String.valueOf((line.getPercentageValue() * 1000) / 10) + "%", String.valueOf(PriceVal)));
                        /*if (xArr[0] < xArr[1]) {     // y = p3 + (p2-p1) * percentage
                            val = priceFormat.format(yArray[2] + (line.getPercentageValue()) * (yArray[1] - yArray[0]));
                        } else {
                            val = priceFormat.format(yArray[2] + (line.getPercentageValue()) * (yArray[0] - yArray[1]));
                        }*/

                        int valueWidth = g.getFontMetrics().stringWidth(PriceVal);
                        gg.drawLine(x3, y, rect.x + rect.width - valueWidth, y);
                        String percentaegStr = percentformat.format(line.getPercentageValue());
                        int strWd = fm.stringWidth(percentaegStr);
                        gg.drawString(percentaegStr, x3 - strWd - halfBox, y + strHt);
                        gg.drawString(PriceVal, rect.x + rect.width - valueWidth, y); //TODO
                    }
                }

                //drawing the trend, retr and base lines..

                FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(ID_TREND);
                Stroke stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());

                //drawing the major trend line
                gg.drawLine(x1, y1, x2, y2);

                //drawing the price values in top and bottom of trend lines
                String trend1Val = priceFormat.format(yArray[0]);
                int trend1Width = g.getFontMetrics().stringWidth(trend1Val);
                gg.drawString(trend1Val, x1 - trend1Width - 4, y1);

                line = (FibonacciExtensionLine) fibonacciLines.get(ID_RETR);
                trend1Val = priceFormat.format(yArray[1]);
                trend1Width = g.getFontMetrics().stringWidth(trend1Val);
                gg.setColor(line.getLineColor());
                gg.drawString(trend1Val, x2 - trend1Width - 4, y2);

                //drawing the retracement line
                line = (FibonacciExtensionLine) fibonacciLines.get(ID_RETR);
                stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());
                gg.drawLine(x2, y2, x3, y3);

                //drawing the base line
                line = (FibonacciExtensionLine) fibonacciLines.get(ID_BASE);
                stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());
                gg.drawLine(x3, yMin, x3, yMax);
            }
            if (drawSelected) {
                /* =========================== trend line ==================================*/
                /*FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(ID_TREND);
                Stroke stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());

                //drawing the major trend line
                gg.drawLine(x1, y1, x2, y2);

                //drawing the price values in top and bottom of trend lines
                String trend1Val = priceFormat.format(yArray[0]);
                int trend1Width = g.getFontMetrics().stringWidth(trend1Val);
                gg.drawString(trend1Val, x1 - trend1Width - 4, y1);

                trend1Val = priceFormat.format(yArray[1]);
                trend1Width = g.getFontMetrics().stringWidth(trend1Val);
                gg.drawString(trend1Val, x2 - trend1Width - 4, y2);

                //drawing the retracement line
                line = (FibonacciExtensionLine) fibonacciLines.get(ID_RETR);
                stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());
                gg.drawLine(x2, y2, x3, y3);

                //drawing the base line
                line = (FibonacciExtensionLine) fibonacciLines.get(ID_BASE);
                stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                gg.setStroke(stroke);
                gg.setColor(line.getLineColor());
                gg.drawLine(x3, yMin, x3, yMax);*/

                gg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                gg.setStroke(selectPen);

                //filling the selected rectangles
                FibonacciExtensionLine line = (FibonacciExtensionLine) fibonacciLines.get(ID_TREND);
                gg.setColor(line.getLineColor());
                gg.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);

                line = (FibonacciExtensionLine) fibonacciLines.get(ID_RETR);
                gg.setColor(line.getLineColor());
                gg.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

                line = (FibonacciExtensionLine) fibonacciLines.get(ID_BEGIN);
                gg.setColor(line.getLineColor());
                gg.fillRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);

                //drawing the selected rectangles
                line = (FibonacciExtensionLine) fibonacciLines.get(ID_TREND);
                gg.setColor(line.getLineColor());
                gg.setColor(Color.BLACK);
                gg.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                gg.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

                line = (FibonacciExtensionLine) fibonacciLines.get(ID_RETR);
                gg.setColor(line.getLineColor());
                gg.setColor(Color.BLACK);
                gg.drawRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);
            }
        } catch (Exception ex) {

            System.out.println("exception occured in drawOnGraphics() ");
        }
    }

    public Hashtable<Integer, FibonacciExtensionLine> getDefaultFibonacciExtensionLines() {

        Hashtable<Integer, FibonacciExtensionLine> lines = new Hashtable<Integer, FibonacciExtensionLine>();

        //trend line
        Color trendLineColor = new Color(0, 153, 0);
        FibonacciExtensionLine trendLine = new FibonacciExtensionLine(Double.POSITIVE_INFINITY, trendLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_TREND, trendLine);

        //retracement line
        Color retrLineColor = new Color(204, 0, 50);
        FibonacciExtensionLine retrLine = new FibonacciExtensionLine(Double.NEGATIVE_INFINITY, retrLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_RETR, retrLine);

        //base line
        Color baseLineColor = new Color(0, 0, 255);
        FibonacciExtensionLine baseLine = new FibonacciExtensionLine(EXCLUDE_VALUE, baseLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_BASE, baseLine);

        //0% line
        Color trendBeginLineColor = new Color(250, 102, 0);
        FibonacciExtensionLine trendBeginLine = new FibonacciExtensionLine(0.0, trendBeginLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_BEGIN, trendBeginLine);

        //100% line
        Color trendEndLineColor = new Color(30, 160, 130);
        FibonacciExtensionLine trendEndLine = new FibonacciExtensionLine(1.0, trendEndLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_END, trendEndLine);

        //first line - 38.2%
        Color firstLineColor = new Color(25, 100, 255);
        FibonacciExtensionLine firstLine = new FibonacciExtensionLine(.382d, firstLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_ONE, firstLine);

        //second line - 61.8%
        Color secondLineColor = new Color(102, 0, 204);
        FibonacciExtensionLine secondLine = new FibonacciExtensionLine(.618d, secondLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_TWO, secondLine);

        //third line - 138.2%
        Color thirdLineColor = new Color(255, 102, 0);
        FibonacciExtensionLine thirdLine = new FibonacciExtensionLine(1.382d, thirdLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_THREE, thirdLine);

        //fourth line - 161.8%
        Color fourthLineColor = new Color(0, 204, 204);
        FibonacciExtensionLine fourthLine = new FibonacciExtensionLine(1.618d, fourthLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_FOUR, fourthLine);

        lines.put(ID_FIVE, new FibonacciExtensionLine());
        lines.put(ID_SIX, new FibonacciExtensionLine());
        lines.put(ID_SEVEN, new FibonacciExtensionLine());
        lines.put(ID_EIGHT, new FibonacciExtensionLine());
        lines.put(ID_NINE, new FibonacciExtensionLine());

        return lines;
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        try {
            pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
            int y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

            float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
            int x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            int y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

            indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
            int x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            int y3 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

            indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
            int x3 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            //if end points within rectangle
            if (r.contains(x1, y1) || r.contains(x2, y2) || r.contains(x3, y3)) {
                return true;
            }

            double m = (y2 - y1) / (x2 - x1);
            double c = (y1 - m * x1);

            int x0 = r.x;

            double y = m * x0 + c;

            //m is postitve and intersects the rectangle
            if (y > r.y && y < (r.y + r.height)) {
                return true;
            }

            x0 = x0 + r.width;
            y = m * x0 + c;

            if (y > r.y && y < (r.y + r.height)) {
                return true;
            }

            //added later
            double x = (r.y - c) / m;

            if (x > r.x && x < (r.x + r.width)) {
                return true;
            }

            x = ((r.y + r.height) - c) / m;
            if (x > r.x && x < (r.x + r.width)) {
                return true;
            }

            //between second and third points
            m = (y3 - y2) / (x3 - x2);
            c = (y2 - m * x2);

            x0 = r.x;

            y = m * x0 + c;

            //m is postitve and intersects the rectangle
            if (y > r.y && y < (r.y + r.height)) {
                return true;
            }

            x0 = x0 + r.width;
            y = m * x0 + c;

            if (y > r.y && y < (r.y + r.height)) {
                return true;
            }

            //added later
            x = (r.y - c) / m;

            if (x > r.x && x < (r.x + r.width)) {
                return true;
            }

            x = ((r.y + r.height) - c) / m;
            if (x > r.x && x < (r.x + r.width)) {
                return true;
            }

            //detect the other lines


        } catch (Exception e) {
            return false;
        }
        return false;
    }

    protected boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select, int index) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = index;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
            onFirstPoint = false;
            if (!selected)
                selected = select;
            movingPtIndex = index + 1;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = index;
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    public Hashtable getSavedLines() {
        boolean isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(AbstractObject.INT_FIBONACCI_EXTENSIONS);
        if (isUsingUserDefault) {
            ObjectFibExtProperty op = (ObjectFibExtProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(AbstractObject.INT_FIBONACCI_EXTENSIONS);
            return op.getTable();    //overrides the default properties here....
        }
        return getDefaultFibonacciExtensionLines();
    }


}
