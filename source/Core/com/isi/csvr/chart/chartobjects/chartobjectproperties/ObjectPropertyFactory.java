package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import com.isi.csvr.chart.chartobjects.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 21, 2009
 * Time: 7:16:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectPropertyFactory {

    /**
     * ***************************** saveing object properties *************************************
     */

    //for the objects have default properties. like Horizontal line, verticacl line, Fibonacci arcs, fibonacci fans etc..
    public static void saveDefaultObjectProperties(AbstractObject oR) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        ObjectDefaultProperty op = new ObjectDefaultProperty(oR.getObjType());

        items.add(op.setObjectID());
        items.add(op.setLineColor(oR.getColor()));
        items.add(op.setLineStyle(oR.getPenStyle()));
        items.add(op.setLineThickness(oR.getPenWidth()));
        items.add(op.setFreeStyle(oR.isFreeStyle()));

        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), items);
    }

    //for the objects have default properties. like rectangle, ellipse etc...
    public static void saveRectangularObjectProperties(LineStudy oR) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        ObjectRectangularProperty op = new ObjectRectangularProperty(oR.getObjType());

        items.add(op.setObjectID());
        items.add(op.setLineColor(oR.getColor()));
        items.add(op.setLineStyle(oR.getPenStyle()));
        items.add(op.setLineThickness(oR.getPenWidth()));
        items.add(op.setFreeStyle(oR.isFreeStyle()));

        items.add(op.setBackGroundColor(oR.getFillColor()));
        items.add(op.setSendBehindChrts(oR.isDrawingOnBackground()));
        items.add(op.setTransparent(oR.isTransparent()));

        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), items);
    }

    //for the objects have default properties. like line slope, line arrow headed, line regression etc...
    public static void saveLineObjectProperties(AbstractObject oR) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        ObjectLineProperty op = new ObjectLineProperty(oR.getObjType());

        items.add(op.setObjectID());
        items.add(op.setLineColor(oR.getColor()));
        items.add(op.setLineStyle(oR.getPenStyle()));
        items.add(op.setLineThickness(oR.getPenWidth()));
        items.add(op.setFreeStyle(oR.isFreeStyle()));

        if (oR instanceof ObjectLineSlope) {

            ObjectLineSlope os = (ObjectLineSlope) oR;

            items.add(op.setExtendedLeft(os.isExtendedLeft()));
            items.add(op.setExtendedRight(os.isExtendedRight()));

            items.add(op.setShowBarCount(os.isbarCount()));
            items.add(op.setShowNetChange(os.isnetChange()));
            items.add(op.setShowPctChange(os.ispctChange()));
            items.add(op.setAngle(os.getAngle()));

        } else if (oR instanceof ObjectArrowLineSlope) {

            ObjectArrowLineSlope os = (ObjectArrowLineSlope) oR;

            items.add(op.setExtendedLeft(os.isExtendedLeft()));
            items.add(op.setExtendedRight(os.isExtendedRight()));

            items.add(op.setShowBarCount(os.isbarCount()));
            items.add(op.setShowNetChange(os.isnetChange()));
            items.add(op.setShowPctChange(os.ispctChange()));
        } else if (oR instanceof ObjectRegression) {

            ObjectRegression os = (ObjectRegression) oR;

            items.add(op.setExtendedLeft(os.isExtendedLeft()));
            items.add(op.setExtendedRight(os.isExtendedRight()));
        }

        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), items);

    }

    //for the objects have default properties. like line std dev. channel, erro channel, raff regression etc...
    public static void saveChannelObjectProperties(AbstractObject oR) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        ObjectChannelProperty op = new ObjectChannelProperty(oR.getObjType());

        items.add(op.setObjectID());
        items.add(op.setLineColor(oR.getColor()));
        items.add(op.setLineStyle(oR.getPenStyle()));
        items.add(op.setLineThickness(oR.getPenWidth()));
        items.add(op.setFreeStyle(oR.isFreeStyle()));

        if (oR instanceof ObjectStandardDeviationChannel) {

            ObjectStandardDeviationChannel osd = (ObjectStandardDeviationChannel) oR;
            items.add(op.setExtendedLeft(osd.isExtendedLeft()));
            items.add(op.setExtendedRight(osd.isExtendedRight()));
            items.add(op.setNoOfUnits(osd.getUnits()));
        } else if (oR instanceof ObjectRaffRegression) {

            ObjectRaffRegression orr = (ObjectRaffRegression) oR;
            items.add(op.setExtendedLeft(orr.isExtendedLeft()));
            items.add(op.setExtendedRight(orr.isExtendedRight()));
        } else if (oR instanceof ObjectStandardError) {

            ObjectStandardError osr = (ObjectStandardError) oR;
            items.add(op.setExtendedLeft(osr.isExtendedLeft()));
            items.add(op.setExtendedRight(osr.isExtendedRight()));
            items.add(op.setNoOfUnits(osr.getUnits()));
        }

        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), items);

    }

    //for the object text note
    public static void saveTextObjectProperties(LineStudy oR) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        ObjectTextNoteProperty op = new ObjectTextNoteProperty(oR.getObjType());

        items.add(op.setObjectID());
        items.add(op.setLineColor(oR.getColor()));
        items.add(op.setLineStyle(oR.getPenStyle()));
        items.add(op.setLineThickness(oR.getPenWidth()));
        items.add(op.setFreeStyle(oR.isFreeStyle()));

        items.add(op.setText(oR.getText()));
        items.add(op.setFillColor(oR.getFillColor()));
        items.add(op.setTextColor(oR.getFontColor()));
        items.add(op.setTransparent(oR.isTransparent()));
        items.add(op.setFont(oR.getFont()));
        items.add(op.setShowBorder(oR.isShowBorder()));
        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), items);
    }

    public static void saveFibonacciRetrProperties(AbstractObject oR) {

        ObjectFibonacciRetrProperty op = new ObjectFibonacciRetrProperty(oR);
        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), op.getDataItems());
    }

    public static void saveFibonacciExtProperties(AbstractObject oR) {

        ObjectFibExtProperty op = new ObjectFibExtProperty(oR);
        ObjectPropertyXMLService.saveObjectProperties(oR.getObjType(), op.getDataItems());
    }

    /**
     * ***************************** loading object properties ************************************
     */

    public static ObjectDefaultProperty loadDefaultObjectProperties(ArrayList<ObjectDataItem> items) {

        ObjectDefaultProperty op = new ObjectDefaultProperty();
        op.assignvaluesFrom(items);

        return op;
    }

    public static ObjectRectangularProperty loadRectangularObjectProperties(ArrayList<ObjectDataItem> items) {

        ObjectRectangularProperty op = new ObjectRectangularProperty();
        op.assignvaluesFrom(items);

        return op;
    }

    public static ObjectLineProperty loadLineObjectProperties(ArrayList<ObjectDataItem> items) {

        ObjectLineProperty olp = new ObjectLineProperty();
        olp.assignvaluesFrom(items);

        return olp;
    }

    public static ObjectChannelProperty loadChannelObjectProperties(ArrayList<ObjectDataItem> items) {

        ObjectChannelProperty olp = new ObjectChannelProperty();
        olp.assignvaluesFrom(items);

        return olp;
    }

    public static ObjectTextNoteProperty loadTextNoteObjectProperties(ArrayList<ObjectDataItem> items) {

        ObjectTextNoteProperty op = new ObjectTextNoteProperty();
        op.assignvaluesFrom(items);

        return op;

    }

    public static ObjectFibonacciRetrProperty loadFibinacciRetrProperties(ArrayList<ObjectDataItem> items) {

        ObjectFibonacciRetrProperty op = new ObjectFibonacciRetrProperty();

        Hashtable table = new Hashtable();

        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            if (item.getName().equals(ObjectFibonacciRetrProperty.OBJECT_ID)) { //skip the first element <objectID>12</objectOD>
                continue;
            } else if (item.getName().equals(ObjectFibonacciRetrProperty.IS_USING_SAME_PROPERTIES)) {
                op.setUsingSameProperties(Boolean.parseBoolean(String.valueOf(item.getValue())));
                continue;
            } else if (item.getName().equals(ObjectFibonacciRetrProperty.IS_FREE_STYLE)) {
                op.setFreeStyleMode(Boolean.parseBoolean(String.valueOf(item.getValue())));
                continue;
            }
            String[] tokens = item.getValue().toString().split(";");

            boolean enabled = Boolean.parseBoolean(tokens[0]);
            double percentage = Double.parseDouble(tokens[1]);
            Color color = new Color(new Integer(tokens[2]));
            byte penStyle = Byte.parseByte(tokens[3]);
            float penWIdth = Float.parseFloat(tokens[4]);
            FibonacciRetracementiLine line = new FibonacciRetracementiLine(percentage, color, penStyle, penWIdth, enabled);

            String name = item.getName();
            name = name.replace(ObjectFibonacciRetrProperty.STR_LINE, "");
            table.put(Integer.parseInt(name), line);

            //System.out.println("*** percentage ***");
        }

        op.setTable(table);

        return op;

    }

    public static ObjectFibExtProperty loadFibinacciExtProperties(ArrayList<ObjectDataItem> items) {

        ObjectFibExtProperty op = new ObjectFibExtProperty();

        Hashtable table = new Hashtable();

        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            if (item.getName().equals(ObjectFibExtProperty.OBJECT_ID)) { //skip the first element <objectID>12</objectOD>
                continue;
            } else if (item.getName().equals(ObjectFibExtProperty.IS_USING_SAME_PROPERTIES)) {
                op.setUsingSameProperties(Boolean.parseBoolean(String.valueOf(item.getValue())));
                continue;
            } else if (item.getName().equals(ObjectFibExtProperty.IS_FREE_STYLE)) {
                op.setFreeStyleMode(Boolean.parseBoolean(String.valueOf(item.getValue())));
                continue;
            }
            String[] tokens = item.getValue().toString().split(";");

            boolean enabled = Boolean.parseBoolean(tokens[0]);
            double percentage = Double.parseDouble(tokens[1]);
            Color color = new Color(new Integer(tokens[2]));
            byte penStyle = Byte.parseByte(tokens[3]);
            float penWIdth = Float.parseFloat(tokens[4]);
            FibonacciExtensionLine line = new FibonacciExtensionLine(percentage, color, penStyle, penWIdth, enabled);

            String name = item.getName();
            name = name.replace(ObjectFibExtProperty.STR_LINE, "");
            table.put(Integer.parseInt(name), line);

            //System.out.println("*** percentage ***");
        }

        op.setTable(table);

        return op;

    }

}
