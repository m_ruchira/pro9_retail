package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 22, 2009
 * Time: 7:38:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectRectangularProperty extends ObjectDefaultProperty {

    private String BACKGROUND_COLOR = "backgroundColor";
    private String IS_SEND_BEHIND_CHARTS = "isSendBehindChrts";
    private String IS_TRANSPARENT = "isTransparent";

    private Color backGroundColor;
    private boolean isSendBehindChrts = false;
    private boolean isTransparent = false;

    public ObjectRectangularProperty(int objectId) {
        super(objectId);
    }

    public ObjectRectangularProperty() {
        super();
    }

    public Color getBackGroundColor() {
        return backGroundColor;
    }

    public ObjectDataItem setBackGroundColor(Color backGroundColor) {
        this.backGroundColor = backGroundColor;
        return new ObjectDataItem(BACKGROUND_COLOR, backGroundColor.getRGB());
    }

    public boolean isSendBehindChrts() {
        return isSendBehindChrts;
    }

    public ObjectDataItem setSendBehindChrts(boolean sendBehindChrts) {
        isSendBehindChrts = sendBehindChrts;
        return new ObjectDataItem(IS_SEND_BEHIND_CHARTS, isSendBehindChrts);
    }

    public boolean isTransparent() {
        return isTransparent;
    }

    public ObjectDataItem setTransparent(boolean transparent) {
        isTransparent = transparent;
        return new ObjectDataItem(IS_TRANSPARENT, isTransparent);
    }

    protected void assignvaluesFrom(ArrayList<ObjectDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(BACKGROUND_COLOR)) {
            this.backGroundColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(IS_SEND_BEHIND_CHARTS)) {
            this.isSendBehindChrts = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_TRANSPARENT)) {
            this.isTransparent = Boolean.parseBoolean(propertyValue.toString());
        }
    }
}
