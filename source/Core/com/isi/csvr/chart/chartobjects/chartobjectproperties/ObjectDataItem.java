package com.isi.csvr.chart.chartobjects.chartobjectproperties;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 21, 2009
 * Time: 7:44:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectDataItem {

    private String name;
    private Object value;

    public ObjectDataItem(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Object getValue() {
        return value;
    }
}
