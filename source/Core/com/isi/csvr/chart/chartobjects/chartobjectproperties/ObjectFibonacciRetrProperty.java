package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.chartobjects.FibonacciRetracementiLine;
import com.isi.csvr.chart.chartobjects.ObjectFibonacciRetracements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 23, 2009
 * Time: 7:01:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFibonacciRetrProperty extends ObjectDefaultProperty {

    protected final static String IS_USING_SAME_PROPERTIES = "isUsingSameProperties";
    protected static final String STR_LINE = "line";
    protected boolean isUsingSameProperties = false;
    protected boolean isFreeStyle = false;
    Hashtable table;
    private ObjectFibonacciRetracements ofr;

    public ObjectFibonacciRetrProperty(int objectID) {
        super(objectID);
    }

    public ObjectFibonacciRetrProperty() {
        super();
    }

    public ObjectFibonacciRetrProperty(AbstractObject oR) {
        this.ofr = (ObjectFibonacciRetracements) oR;
        this.table = ofr.getFibonacciLines();
    }

    public ArrayList<ObjectDataItem> getDataItems() {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();
        items.add(new ObjectDataItem(OBJECT_ID, AbstractObject.INT_FIBONACCI_RETRACEMENTS));
        items.add(new ObjectDataItem(IS_USING_SAME_PROPERTIES, ofr.isUsingSameProperties()));
        items.add(new ObjectDataItem(IS_FREE_STYLE, ofr.isFreeStyle()));

        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        for (int key : UIkeys) {
            //int rowID = (Integer) ids.nextElement();
            FibonacciRetracementiLine line = (FibonacciRetracementiLine) table.get(key);
            String lineItem = "" + line.isEnabled() + ";" + line.getPercentageValue() + ";" +
                    line.getLineColor().getRGB() + ";" + line.getPenStyle() + ";" + line.getPenWidth();
            items.add(new ObjectDataItem(String.valueOf(STR_LINE + key), lineItem));
        }

        return items;
    }

    public Hashtable getTable() {
        return table;
    }

    public void setTable(Hashtable table) {
        this.table = table;
    }

    public boolean isUsingSameProperties() {
        return isUsingSameProperties;
    }

    public void setUsingSameProperties(boolean usingSameProperties) {
        isUsingSameProperties = usingSameProperties;
    }

    public boolean isFreeStyleMode() {
        return isFreeStyle;
    }

    public void setFreeStyleMode(boolean free) {
        isFreeStyle = free;
    }
}
