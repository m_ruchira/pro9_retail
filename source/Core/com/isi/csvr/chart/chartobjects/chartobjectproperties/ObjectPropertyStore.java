package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.chartobjects.LineStudy;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 20, 2009
 * Time: 1:15:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectPropertyStore {

    private static Hashtable<Integer, Object> propertyStore = new Hashtable<Integer, Object>();
    private static ObjectPropertyStore self;

    public static ObjectPropertyStore getSharedInstance() {
        if (self == null) {
            self = new ObjectPropertyStore();
            reloadPropertyStore();
        }
        //reloadPropertyStore();  //TODO : may not need here..can shift to up
        return self;
    }

    //this must be called when an object sets is defaults or deletes it default properties..
    public static void reloadPropertyStore() {

        propertyStore.clear();
        ArrayList<Integer> idList = ObjectPropertyXMLService.getSavedObjects();

        for (int i = 0; i < idList.size(); i++) {
            Integer key = idList.get(i);
            ArrayList<ObjectDataItem> items = ObjectPropertyXMLService.loadProperties(key);

            //TODO : need to complete the rest of the objects...
            switch (key) {
                case AbstractObject.INT_LINE_HORIZ:
                case AbstractObject.INT_LINE_VERTI:
                case AbstractObject.INT_FIBONACCI_ARCS:
                case AbstractObject.INT_FIBONACCI_FANS:
                case AbstractObject.INT_ZIG_ZAG:
                case AbstractObject.INT_FIBONACCI_ZONES:
                case AbstractObject.INT_GANN_LINE:
                case AbstractObject.INT_GANN_GRID:
                case AbstractObject.INT_GANN_FAN:
                case AbstractObject.INT_EQUI_DIST_CHANNEL:
                case AbstractObject.INT_ARC:
                case AbstractObject.INT_CYCLE_LINE:
                case AbstractObject.INT_ANDREWS_PITCHFORK:

                    ObjectDefaultProperty op = ObjectPropertyFactory.loadDefaultObjectProperties(items);
                    propertyStore.put(key, op);
                    break;

                case AbstractObject.INT_RECT:
                case AbstractObject.INT_ELLIPSE:

                    ObjectRectangularProperty orp = ObjectPropertyFactory.loadRectangularObjectProperties(items);
                    propertyStore.put(key, orp);
                    break;

                case AbstractObject.INT_LINE_SLOPE:
                case AbstractObject.INT_ARROW_LINE_SLOPE:
                case AbstractObject.INT_REGRESSION:

                    ObjectLineProperty olp = ObjectPropertyFactory.loadLineObjectProperties(items);
                    propertyStore.put(key, olp);
                    break;

                case AbstractObject.INT_STD_DEV_CHANNEL:
                case AbstractObject.INT_STD_ERROR_CHANNEL:
                case AbstractObject.INT_RAFF_REGRESSION:

                    ObjectChannelProperty ocp = ObjectPropertyFactory.loadChannelObjectProperties(items);
                    propertyStore.put(key, ocp);
                    break;

                case AbstractObject.INT_TEXT:

                    ObjectTextNoteProperty otp = ObjectPropertyFactory.loadTextNoteObjectProperties(items);
                    propertyStore.put(key, otp);
                    break;


                case AbstractObject.INT_FIBONACCI_RETRACEMENTS:

                    ObjectFibonacciRetrProperty ofp = ObjectPropertyFactory.loadFibinacciRetrProperties(items);
                    propertyStore.put(key, ofp);
                    break;

                case AbstractObject.INT_FIBONACCI_EXTENSIONS:

                    ObjectFibExtProperty ofe = ObjectPropertyFactory.loadFibinacciExtProperties(items);
                    propertyStore.put(key, ofe);
                    break;
            }
        }
    }

    public Hashtable<Integer, Object> getPropertyStore() {
        return propertyStore;
    }

    public boolean hasDefauiltProperties(int objectID) {
        Object op = propertyStore.get(new Integer(objectID));
        if (op == null) {
            return false;
        }
        return true;
    }

    public void deleteDefaultProperties(AbstractObject oR) {
        ObjectPropertyXMLService.deleteObjectProperties(oR.getObjType());
        reloadPropertyStore();
    }

    public void saveDefaultProperties(AbstractObject oR) {

        int objType = oR.getObjType();

        switch (objType) {

            //TODO: this set is already completed.. have common properties like line color, line thickness, line style
            case AbstractObject.INT_LINE_HORIZ:
            case AbstractObject.INT_LINE_VERTI:
            case AbstractObject.INT_FIBONACCI_ARCS:
            case AbstractObject.INT_FIBONACCI_FANS:
            case AbstractObject.INT_ZIG_ZAG:
            case AbstractObject.INT_FIBONACCI_ZONES:
            case AbstractObject.INT_GANN_LINE:
            case AbstractObject.INT_GANN_GRID:
            case AbstractObject.INT_GANN_FAN:
            case AbstractObject.INT_EQUI_DIST_CHANNEL:
            case AbstractObject.INT_ARC:
            case AbstractObject.INT_CYCLE_LINE:
            case AbstractObject.INT_ANDREWS_PITCHFORK:

                ObjectPropertyFactory.saveDefaultObjectProperties(oR);
                break;

            //TODO : need to complete the propertes. extend left, right etc..
            case AbstractObject.INT_LINE_SLOPE:
            case AbstractObject.INT_ARROW_LINE_SLOPE:
            case AbstractObject.INT_REGRESSION:

                ObjectPropertyFactory.saveLineObjectProperties(oR);
                break;

            //TODO: this set is already completed.. have extra properties like background color, send behind and, transparent proerty
            case AbstractObject.INT_RECT:
            case AbstractObject.INT_ELLIPSE:

                ObjectPropertyFactory.saveRectangularObjectProperties((LineStudy) oR);
                break;

            case AbstractObject.INT_STD_DEV_CHANNEL:
            case AbstractObject.INT_STD_ERROR_CHANNEL:
            case AbstractObject.INT_RAFF_REGRESSION:

                ObjectPropertyFactory.saveChannelObjectProperties(oR);
                break;

            case AbstractObject.INT_TEXT:
                ObjectPropertyFactory.saveTextObjectProperties((LineStudy) oR);
                break;

            case AbstractObject.INT_FIBONACCI_RETRACEMENTS:
                ObjectPropertyFactory.saveFibonacciRetrProperties(oR);
                break;

            case AbstractObject.INT_FIBONACCI_EXTENSIONS:
                ObjectPropertyFactory.saveFibonacciExtProperties(oR);
                break;
        }
        //refrshes the property store for new changes 
        reloadPropertyStore();
    }
}
