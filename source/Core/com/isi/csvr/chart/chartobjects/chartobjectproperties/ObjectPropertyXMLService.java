package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import com.isi.csvr.shared.Settings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 20, 2009
 * Time: 4:10:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectPropertyXMLService {

    private static final String ROOT_ELEMENT = "objectStore";
    private static final String CHILD_ELEMENT = "object";
    private static final String OBJECT_ID = ObjectDefaultProperty.OBJECT_ID;
    //private static final String filePath = "./Charts/ops.dll";
    private static final String filePath = Settings.CHART_DATA_PATH + "/ops.dll";

    private static boolean isObjectAlreadyExists(int objectID) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(new File(filePath).toURI().toString());

            NodeList list = dom.getElementsByTagName(CHILD_ELEMENT);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
                String id = nlName.item(0).getTextContent();

                if (Integer.parseInt(id) == objectID) {
                    return true;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        //System.out.println("********* not found *******");

        return false;
    }

    public static void deleteObjectProperties(int objectID) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(new File(filePath).toURI().toString());
            Element root = dom.getDocumentElement();

            NodeList list = dom.getElementsByTagName(CHILD_ELEMENT);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
                String id = nlName.item(0).getTextContent();

                if (Integer.parseInt(id) == objectID) {
                    root.removeChild(elem);
                    break;
                }
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "true");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(dom);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveObjectProperties(int objID, ArrayList<ObjectDataItem> properties) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {

            docBuilder = dbFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File(filePath).toURI().toString());
            Element root = doc.getDocumentElement();

            if (isObjectAlreadyExists(objID)) {

                Element ele = doc.createElement(CHILD_ELEMENT);
                root.appendChild(ele);

                for (ObjectDataItem item : properties) {

                    Element eleName = doc.createElement(item.getName());
                    String str = String.valueOf(item.getValue());
                    Text text = doc.createTextNode(str);
                    ele.appendChild(eleName);
                    eleName.appendChild(text);
                }

                ////////////////////////////////////

                NodeList list = doc.getElementsByTagName(CHILD_ELEMENT);
                int count = list.getLength();

                for (int i = 0; i < count; i++) {

                    Element elem = (Element) list.item(i);
                    NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
                    String id = nlName.item(0).getTextContent();

                    if (Integer.parseInt(id) == objID) {
                        root.replaceChild(ele, elem);
                        break;
                    }
                }
            } else {
                Element ele = doc.createElement(CHILD_ELEMENT);
                root.appendChild(ele);
                //add general settings to xml file

                for (ObjectDataItem item : properties) {

                    Element eleName = doc.createElement(item.getName());
                    String str = String.valueOf(item.getValue());
                    Text text = doc.createTextNode(str);
                    ele.appendChild(eleName);
                    eleName.appendChild(text);
                }
            }
            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //ObjectPropertyStore.reloadPropertyStore();
    }

    public static ArrayList<ObjectDataItem> loadProperties(int objectID) {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();

        Document doc = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = null;
            docBuilder = dbFactory.newDocumentBuilder();
            doc = docBuilder.parse(new File(filePath).toURI().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList list = doc.getElementsByTagName(CHILD_ELEMENT);
        int count = list.getLength();

        for (int i = 0; i < count; i++) {

            Element elem = (Element) list.item(i);
            NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
            String id = nlName.item(0).getTextContent();

            if (Integer.parseInt(id) == objectID) {

                NodeList properties = elem.getChildNodes();
                for (int j = 0; j < properties.getLength(); j++) {

                    String name = properties.item(j).getNodeName();
                    String value = properties.item(j).getTextContent();
                    //System.out.println(name + " : " + value);
                    items.add(new ObjectDataItem(name, value));
                }
            }
        }
        return items;

    }

    public static ArrayList<Integer> getSavedObjects() {

        ArrayList<Integer> objects = new ArrayList<Integer>();

        Document doc = null;
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = null;
            docBuilder = dbFactory.newDocumentBuilder();
            doc = docBuilder.parse(new File(filePath).toURI().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList list = doc.getElementsByTagName(CHILD_ELEMENT);
        int count = list.getLength();


        for (int i = 0; i < count; i++) {

            Element elem = (Element) list.item(i);
            NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
            String id = nlName.item(0).getTextContent();

            objects.add(new Integer(id));
        }

        return objects;
    }

}
