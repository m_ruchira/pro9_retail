package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 22, 2009
 * Time: 10:03:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectLineProperty extends ObjectDefaultProperty {

    protected final String IS_EXTENDED_LEFT = "isExtendedLeft";
    protected final String IS_EXTENDED_RIGHT = "isExtendedRight";
    protected final String IS_SHOW_NET_CHANGE = "isShowNetChange";
    protected final String IS_SHOW_PCT_CHANGE = "isShowPctChange";
    protected final String IS_SHOW_BAR_COUNT = "isShowBarCount";
    protected final String ANGLE = "angle";
    private boolean isExtendedLeft = false;
    private boolean isExtendedRight = false;
    private boolean isShowNetChange = false;
    private boolean isShowPctChange = false;
    private boolean isShowBarCount = false;
    private double angle = 45;

    public ObjectLineProperty(int objectId) {
        super(objectId);
    }

    public ObjectLineProperty() {
        super();
    }

    public boolean isExtendedLeft() {
        return isExtendedLeft;
    }

    public ObjectDataItem setExtendedLeft(boolean extendedLeft) {
        isExtendedLeft = extendedLeft;
        return new ObjectDataItem(IS_EXTENDED_LEFT, extendedLeft);
    }

    public boolean isExtendedRight() {
        return isExtendedRight;
    }

    public ObjectDataItem setExtendedRight(boolean extendedRight) {
        isExtendedRight = extendedRight;
        return new ObjectDataItem(IS_EXTENDED_RIGHT, isExtendedRight);
    }

    public boolean isShowNetChange() {
        return isShowNetChange;
    }

    public ObjectDataItem setShowNetChange(boolean showNetChange) {
        isShowNetChange = showNetChange;
        return new ObjectDataItem(IS_SHOW_NET_CHANGE, isShowNetChange);
    }

    public boolean isShowPctChange() {
        return isShowPctChange;
    }

    public double getAngle() {
        return angle;
    }

    public ObjectDataItem setShowPctChange(boolean showPctChange) {
        isShowPctChange = showPctChange;
        return new ObjectDataItem(IS_SHOW_PCT_CHANGE, isShowPctChange);
    }

    public ObjectDataItem setAngle(double angle) {
        this.angle = angle;
        return new ObjectDataItem(ANGLE, angle);
    }

    public boolean isShowBarCount() {
        return isShowBarCount;
    }

    public ObjectDataItem setShowBarCount(boolean showBarCount) {
        isShowBarCount = showBarCount;
        return new ObjectDataItem(IS_SHOW_BAR_COUNT, isShowBarCount);
    }

    protected void assignvaluesFrom(ArrayList<ObjectDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(IS_EXTENDED_LEFT)) {
            this.isExtendedLeft = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_EXTENDED_RIGHT)) {
            this.isExtendedRight = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_SHOW_BAR_COUNT)) {
            this.isShowBarCount = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_SHOW_NET_CHANGE)) {
            this.isShowNetChange = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_SHOW_PCT_CHANGE)) {
            this.isShowPctChange = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(ANGLE)) {
            this.angle = Double.parseDouble(propertyValue.toString());
        }
    }
}
