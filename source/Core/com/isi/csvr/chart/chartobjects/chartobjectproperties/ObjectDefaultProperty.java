package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import java.awt.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 20, 2009
 * Time: 1:33:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectDefaultProperty {

    protected static final String OBJECT_ID = "objectID";
    protected static final String IS_FREE_STYLE = "isFreeStyle";
    protected final String LINE_COLOR = "lineColor";
    protected final String LINE_STYLE = "lineStyle";
    protected final String LINE_THICKNESS = "lineThickness";
    protected int objectId;
    protected Color lineColor;
    protected byte lineStyle;
    protected float lineThickness;
    protected boolean isFreeStyle;

    public ObjectDefaultProperty(int objectId) {
        this.objectId = objectId;
    }

    public ObjectDefaultProperty() {

    }

    public ObjectDataItem setObjectID() {
        return new ObjectDataItem(OBJECT_ID, objectId);
    }

    public Color getLineColor() {
        return lineColor;
    }

    public ObjectDataItem setLineColor(Color lineColor) {
        this.lineColor = lineColor;
        return new ObjectDataItem(LINE_COLOR, lineColor.getRGB());
    }

    public byte getLineStyle() {
        return lineStyle;
    }

    public ObjectDataItem setLineStyle(byte lineStyle) {
        this.lineStyle = lineStyle;
        return new ObjectDataItem(LINE_STYLE, lineStyle);
    }

    public float getLineThickness() {
        return lineThickness;
    }

    public ObjectDataItem setLineThickness(float lineThickness) {
        this.lineThickness = lineThickness;
        return new ObjectDataItem(LINE_THICKNESS, lineThickness);
    }

    public boolean isFreeStyle() {
        return isFreeStyle;
    }

    public ObjectDataItem setFreeStyle(boolean free) {
        this.isFreeStyle = free;
        return new ObjectDataItem(IS_FREE_STYLE, isFreeStyle);
    }

    /*public String getOBJECT_ID() {
        return OBJECT_ID;
    }*/

    protected void assignvaluesFrom(ArrayList<ObjectDataItem> items) {

        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    private void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(OBJECT_ID)) {
            this.objectId = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(LINE_COLOR)) {
            this.lineColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(LINE_STYLE)) {
            this.lineStyle = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(LINE_THICKNESS)) {
            this.lineThickness = Float.parseFloat(propertyValue.toString());
        } else if (propertyName.equals(IS_FREE_STYLE)) {
            this.isFreeStyle = Boolean.parseBoolean(propertyValue.toString());
        }
    }

    //saves the font string as "Arial,8,1" - "fontName,FontSize,fontStyle"
    protected String convertFontToString(Font font) {
        return (font.getName() + "," + font.getSize() + "," + font.getStyle());
    }

    protected Font getFontFromString(String strFont) {
        StringTokenizer tokenizer = new StringTokenizer(strFont, ","); //tokenize from the " , "

        String name = tokenizer.nextToken();
        int size = Integer.parseInt(tokenizer.nextToken());
        int style = Integer.parseInt(tokenizer.nextToken());

        return new Font(name, style, size);
    }
}
