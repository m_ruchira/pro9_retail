package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.chartobjects.FibonacciExtensionLine;
import com.isi.csvr.chart.chartobjects.ObjectFibonacciExtensions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Mar 2, 2009
 * Time: 2:47:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFibExtProperty extends ObjectDefaultProperty {

    protected final static String IS_USING_SAME_PROPERTIES = "isUsingSameProperties";
    protected static final String STR_LINE = "line";
    protected boolean isUsingSameProperties = false;
    protected boolean isFreeStyle = false;
    Hashtable table;
    private ObjectFibonacciExtensions ofe;

    public ObjectFibExtProperty(int objectID) {
        super(objectID);
    }

    public ObjectFibExtProperty() {
        super();
    }

    public ObjectFibExtProperty(AbstractObject oR) {
        this.ofe = (ObjectFibonacciExtensions) oR;
        this.table = ofe.getFibonacciLines();
    }

    public ArrayList<ObjectDataItem> getDataItems() {

        ArrayList<ObjectDataItem> items = new ArrayList<ObjectDataItem>();
        items.add(new ObjectDataItem(OBJECT_ID, AbstractObject.INT_FIBONACCI_EXTENSIONS));
        items.add(new ObjectDataItem(IS_USING_SAME_PROPERTIES, ofe.isUsingSameProperties()));
        items.add(new ObjectDataItem(IS_FREE_STYLE, ofe.isFreeStyle()));

        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        for (int key : UIkeys) {
            //int rowID = (Integer) ids.nextElement();
            FibonacciExtensionLine line = (FibonacciExtensionLine) table.get(key);
            String lineItem = "" + line.isEnabled() + ";" + line.getPercentageValue() + ";" +
                    line.getLineColor().getRGB() + ";" + line.getPenStyle() + ";" + line.getPenWidth();
            items.add(new ObjectDataItem(String.valueOf(STR_LINE + key), lineItem));
        }

        return items;
    }

    public Hashtable getTable() {
        return table;
    }

    public void setTable(Hashtable table) {
        this.table = table;
    }

    public boolean isUsingSameProperties() {
        return isUsingSameProperties;
    }

    public void setUsingSameProperties(boolean usingSameProperties) {
        isUsingSameProperties = usingSameProperties;
    }

    public boolean isFreeStyleMode() {
        return isFreeStyle;
    }

    public void setFreeStyleMode(boolean free) {
        isFreeStyle = free;
    }
}
