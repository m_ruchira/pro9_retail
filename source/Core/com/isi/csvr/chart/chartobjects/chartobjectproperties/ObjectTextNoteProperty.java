package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 23, 2009
 * Time: 2:14:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectTextNoteProperty extends ObjectDefaultProperty {

    private final String TEXT = "text";
    private final String TEXT_COLOR = "textColor";
    private final String FILL_COLOR = "fillColor";
    private final String IS_TRANSPARENT = "isTransparent";
    private final String FONT = "font";
    private final String IS_SHOW_BORDER = "isShowBorder";
    private String text = "";
    private Color textColor;
    private Color fillColor;
    private boolean isTransparent;
    private Font font;
    private boolean isShowBorder;

    public ObjectTextNoteProperty() {
        super();
    }

    public ObjectTextNoteProperty(int objectId) {
        super(objectId);
    }

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public ObjectDataItem setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
        return new ObjectDataItem(IS_SHOW_BORDER, isShowBorder);
    }

    public String getText() {
        return text;
    }

    public ObjectDataItem setText(String text) {
        this.text = text;
        return new ObjectDataItem(TEXT, text);
    }

    public Color getTextColor() {
        return textColor;
    }

    public ObjectDataItem setTextColor(Color textColor) {
        this.textColor = textColor;
        return new ObjectDataItem(TEXT_COLOR, textColor.getRGB());
    }

    public Color getFillColor() {
        return fillColor;
    }

    public ObjectDataItem setFillColor(Color fillColor) {
        this.fillColor = fillColor;
        return new ObjectDataItem(FILL_COLOR, fillColor.getRGB());
    }

    public boolean isTransparent() {
        return isTransparent;
    }

    public ObjectDataItem setTransparent(boolean transparent) {
        isTransparent = transparent;
        return new ObjectDataItem(IS_TRANSPARENT, isTransparent);
    }

    public Font getFont() {
        return font;
    }

    public ObjectDataItem setFont(Font font) {
        this.font = font;
        return new ObjectDataItem(FONT, convertFontToString(font));
    }

    protected void assignvaluesFrom(ArrayList<ObjectDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TEXT)) {
            this.text = String.valueOf(propertyValue);
        } else if (propertyName.equals(FILL_COLOR)) {
            this.fillColor = new Color(Integer.parseInt(String.valueOf(propertyValue)));
        } else if (propertyName.equals(TEXT_COLOR)) {
            this.textColor = new Color(Integer.parseInt(String.valueOf(propertyValue)));
        } else if (propertyName.equals(IS_TRANSPARENT)) {
            this.isTransparent = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(FONT)) {
            this.font = getFontFromString(String.valueOf(propertyValue.toString()));
        } else if (propertyName.equals(IS_SHOW_BORDER)) {
            this.isShowBorder = Boolean.parseBoolean(propertyValue.toString());
        }
    }
}
