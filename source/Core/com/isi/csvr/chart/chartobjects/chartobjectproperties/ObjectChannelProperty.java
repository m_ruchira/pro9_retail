package com.isi.csvr.chart.chartobjects.chartobjectproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 23, 2009
 * Time: 9:33:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectChannelProperty extends ObjectDefaultProperty {

    private final String IS_EXTENDED_LEFT = "isExtendedLeft";
    private final String IS_EXTENDED_RIGHT = "isExtendedRight";
    private final String NO_OF_UNITS = "noOfUnits";
    private float noOfUnits = 2;
    private boolean isExtendedLeft = false;
    private boolean isExtendedRight = false;

    public ObjectChannelProperty(int objectId) {
        super(objectId);
    }

    public ObjectChannelProperty() {
        super();
    }

    public boolean isExtendedLeft() {
        return isExtendedLeft;
    }

    public ObjectDataItem setExtendedLeft(boolean extendedLeft) {
        isExtendedLeft = extendedLeft;
        return new ObjectDataItem(IS_EXTENDED_LEFT, extendedLeft);
    }

    public boolean isExtendedRight() {
        return isExtendedRight;
    }

    public ObjectDataItem setExtendedRight(boolean extendedRight) {
        isExtendedRight = extendedRight;
        return new ObjectDataItem(IS_EXTENDED_RIGHT, isExtendedRight);
    }

    public float getNoOfUnits() {
        return noOfUnits;
    }

    public ObjectDataItem setNoOfUnits(float noOfUnits) {
        this.noOfUnits = noOfUnits;
        return new ObjectDataItem(NO_OF_UNITS, noOfUnits);
    }

    protected void assignvaluesFrom(ArrayList<ObjectDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            ObjectDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    private void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(IS_EXTENDED_LEFT)) {
            this.isExtendedLeft = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(IS_EXTENDED_RIGHT)) {
            this.isExtendedRight = Boolean.parseBoolean(propertyValue.toString());
        } else if (propertyName.equals(NO_OF_UNITS)) {
            this.noOfUnits = Float.parseFloat(propertyValue.toString());
        }
    }
}
