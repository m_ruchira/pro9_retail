package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Arc2D;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Apr 24, 2009
 * Time: 5:53:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectCycleLine extends AbstractObject {

    public static final int DEFAULT_GAP = 100;
    private int gap = DEFAULT_GAP;
    public static final int MINIMUM_GAP = 30;

    // 0 - default, 1 - vertical,
    public static final byte CYCLE_LINE_TYPE = 0;
    protected byte defaultType = CYCLE_LINE_TYPE;
    public static final byte VERTICAL_LINE_TYPE = 1;
    private static final long serialVersionUID = AbstractObject.UID_CYCEL_LINE;

    public ObjectCycleLine() {
        super();
        objType = AbstractObject.INT_CYCLE_LINE;
        showOnAllPanels = false;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
    }

    public ObjectCycleLine(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_CYCLE_LINE;
        showOnAllPanels = false;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        drawingOnBackground = false;
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectCycleLine) {
            this.defaultType = ((ObjectCycleLine) ao).defaultType;
        }
    }

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);

        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        if (objectMoving) {
            int xx1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            int xx2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            gap = Math.abs(xx2 - xx1);
            int radius = gap / 2;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            x1 = x1 - Xadj;
            int y = rect.y + rect.height - radius;

            int x2 = x1;

            while (x2 <= rect.x + rect.width) {
                if (defaultType == CYCLE_LINE_TYPE) {
                    g.drawArc(x2, y, gap, gap, 0, 180);
                } else if (defaultType == VERTICAL_LINE_TYPE) {
                    g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                }
                x2 += gap;
            }
            x2 = x1 - gap;
            while (x2 >= rect.x - gap) {
                if (defaultType == CYCLE_LINE_TYPE) {
                    g.drawArc(x2, y, gap, gap, 0, 180);
                } else if (defaultType == VERTICAL_LINE_TYPE) {
                    g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                }
                x2 -= gap;
            }
        } else {  // object is resizing from a handle
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            int index = getMovingPtIndex();
            int difference = 0;
            int x2 = 0;
            int referenceX = 0;
            if (index != 0) {
                x2 = x1;
                difference = -Xadj / index;
            } else {
                int xx1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                int xx2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                gap = Math.abs(xx2 - xx1);

                difference = -Xadj;
                x2 = x1 - gap;
                referenceX = x2;
            }

            //drawing right hand side cycles
            int xx1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            int xx2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            gap = Math.abs(xx2 - xx1);
            gap = gap + difference;
            if (gap < 40) {
                gap = 40;
            }
            int radius = gap / 2;
            int y = rect.y + rect.height - radius;

            while (x2 <= rect.x + rect.width) {
                if (defaultType == CYCLE_LINE_TYPE) {
                    g.drawArc(x2, y, gap, gap, 0, 180);
                } else if (defaultType == VERTICAL_LINE_TYPE) {
                    g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                }
                x2 += gap;
            }

            //drawing left hand side cycles
            xx1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            xx2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            gap = Math.abs(xx2 - xx1);
            gap = gap + difference;
            if (gap < 40) {
                gap = 40;
            }
            if (movingPtIndex != 0) {
                x2 = x1 - gap;
            } else {
                x2 = referenceX;
            }
            while (x2 >= rect.x - gap) {
                if (defaultType == CYCLE_LINE_TYPE) {
                    g.drawArc(x2, y, gap, gap, 0, 180);
                } else if (defaultType == VERTICAL_LINE_TYPE) {
                    g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                }
                x2 -= gap;
            }
        }
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);

        int xx1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        int xx2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        gap = Math.abs(xx2 - xx1);
        if (gap < MINIMUM_GAP) {
            //return;
        }
        int radius = gap / 2;
        boolean drawSelected = selected && !isPrinting;
        g.setClip(rect.x, rect.y, rect.width, rect.height);

        //g.fillRect(rect.x, rect.y, 80, 80);
        int y = rect.y + rect.height - radius;
        int x2 = x1;

        //drawing the right side circles from the reference point.
        while (x2 <= rect.x + rect.width) {
            g.setColor(color);
            g.setStroke(pen);

            if (defaultType == CYCLE_LINE_TYPE) {
                g.drawArc(x2, y, gap, gap, 0, 180);
                if (drawSelected) {
                    g.fillRect(x2 - 3, rect.y + rect.height - 6, 6, 6); //fill the small rect from object color

                    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    g.setStroke(selectPen);
                    g.setColor(Color.BLACK);
                    g.drawRect(x2 - 3, rect.y + rect.height - 6, 6, 6);
                }
            } else if (defaultType == VERTICAL_LINE_TYPE) {
                g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                if (drawSelected) {
                    g.fillRect(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6); //fill the small rect from object color

                    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    g.setStroke(selectPen);
                    g.setColor(Color.BLACK);
                    g.drawRect(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6);
                }
            }
            x2 += gap;
        }

        //drawing the left side circles from the reference point.
        x2 = x1 - gap;
        while (x2 >= rect.x - gap) {
            g.setColor(color);
            g.setStroke(pen);

            if (defaultType == CYCLE_LINE_TYPE) {
                g.drawArc(x2, y, gap, gap, 0, 180);
                if (drawSelected) {
                    g.drawArc(x2, y, gap, gap, 0, 180);
                    g.fillRect(x2 - 3, rect.y + rect.height - 6, 6, 6); //fill the small rect from object color

                    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    g.setStroke(selectPen);
                    g.setColor(Color.BLACK);
                    g.drawRect(x2 - 3, rect.y + rect.height - 6, 6, 6);
                }
            } else if (defaultType == VERTICAL_LINE_TYPE) {
                g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                if (drawSelected) {
                    g.drawLine(x2, rect.y, x2, rect.y + rect.height);
                    g.fillRect(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6); //fill the small rect from object color

                    g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    g.setStroke(selectPen);
                    g.setColor(Color.BLACK);
                    g.drawRect(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6);
                }
            }
            x2 -= gap;
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        /*
       objectMoving = false;
       int radius = gap / 2;
        int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        int y1 = rect.y + rect.height - radius;

       int x2;

        int index = 0;
        for (x2 = x1; x2 <= rect.x + rect.width; x2 += gap) {
            Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
            Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);
            if (rectangle.contains(x, y)) {
                movingPtIndex = index;
               if (!selected) {
                   selected = select;
               }
               return true;
           }
           index++;
       }

       index = -1;
       for (x2 = x1 - gap; x2 >= rect.x - gap; x2 -= gap) {
           Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
           Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);
           if (rectangle.contains(x, y)) {
               movingPtIndex = index;
               if (!selected) {
                   selected = select;
               }
               return true;
           }
           index--;
       }

       //checking for object moving
       index = 0;
       for (x2 = x1; x2 <= rect.x + rect.width; x2 += gap) {
           Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
           Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);

           if (arc.intersects(x - 2, y - 2, 4, 4)) {
                if (!selected)
                    selected = select;
               objectMoving = true;
                return true;
            }
           index++;
       }

       index = -1;
       for (x2 = x1 - gap; x2 >= rect.x - gap; x2 -= gap) {
           Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
           Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);

            if (arc.intersects(x - 2, y - 2, 4, 4)) {
                if (!selected)
                    selected = select;
                objectMoving = true;
               return true;
           }
           index--;
       }

       return false;*/

        if (!rect.contains(x, y) || !isMove) return false;
        if (gap < MINIMUM_GAP) return false;
        objectMoving = false;
        int radius = gap / 2;
        int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        int y1 = rect.y + rect.height - radius;

        int x2;

        int index = 0;
        for (x2 = x1; x2 <= rect.x + rect.width; x2 += gap) {
            Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
            Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);

            if (defaultType == CYCLE_LINE_TYPE) {
                rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);
            } else if (defaultType == VERTICAL_LINE_TYPE) {
                rectangle = new Rectangle(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6);
            }
            if (rectangle.contains(x, y)) {
                movingPtIndex = index;
                if (!selected) {
                    selected = select;
                }
                return true;
            }
            index++;
        }

        index = -1;
        for (x2 = x1 - gap; x2 >= rect.x - gap; x2 -= gap) {
            Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
            // by default
            Rectangle rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);

            if (defaultType == CYCLE_LINE_TYPE) {
                rectangle = new Rectangle(x2 - 3, rect.y + rect.height - 6, 6, 6);
            } else if (defaultType == VERTICAL_LINE_TYPE) {
                rectangle = new Rectangle(x2 - 3, (rect.y + rect.height - 6) / 2, 6, 6);
            }

            if (rectangle.contains(x, y)) {
                movingPtIndex = index;
                if (!selected) {
                    selected = select;
                }
                return true;
            }
            index--;
        }

        //checking for object moving
        int factor = 8;
        for (x2 = x1; x2 <= rect.x + rect.width; x2 += gap) {
            if (defaultType == CYCLE_LINE_TYPE) {
                Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
                Arc2D.Double arcOut = new Arc2D.Double(x2 - factor, y1 - factor, gap + 2 * factor, gap + 2 * factor, 0, 180, Arc2D.OPEN);
                Arc2D.Double arcIn = new Arc2D.Double(x2 + factor, y1 + factor, gap - 2 * factor, gap - 2 * factor, 0, 180, Arc2D.OPEN);

                /*
                if (x2 == x1 - gap) {

                    Graphics g = ChartFrame.getSharedInstance().getActiveGraph().graph.getGraphics();
                    g.drawArc(x2 - factor, y1 - factor, gap + 2 * factor, gap + 2 * factor, 0, 180);
                    g.setColor(Color.RED);
                    g.drawArc(x2 + factor, y1 + factor, gap - 2 * factor, gap - 2 * factor, 0, 180);
                }
                */
                if (arcOut.contains(x, y) && !arcIn.contains(x, y)) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }

            } else if (defaultType == VERTICAL_LINE_TYPE) {
                if (this.isOnTheLine(x2, rect.y + GraphDataManager.titleHeight, x2, rect.y + rect.height - GraphDataManager.titleHeight, x, y, select, true)) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            }
            index++;
        }

        for (x2 = x1 - gap; x2 >= rect.x - gap; x2 -= gap) {

            if (defaultType == CYCLE_LINE_TYPE) {
                Arc2D.Double arc = new Arc2D.Double(x2, y1, gap, gap, 0, 180, Arc2D.OPEN);
                Arc2D.Double arcOut = new Arc2D.Double(x2 - factor, y1 - factor, gap + 2 * factor, gap + 2 * factor, 0, 180, Arc2D.OPEN);
                Arc2D.Double arcIn = new Arc2D.Double(x2 + factor, y1 + factor, gap - 2 * factor, gap - 2 * factor, 0, 180, Arc2D.OPEN);

                /*
                if (x2 == x1 - gap) {

                    Graphics g = ChartFrame.getSharedInstance().getActiveGraph().graph.getGraphics();
                    g.drawArc(x2 - factor, y1 - factor, gap + 2 * factor, gap + 2 * factor, 0, 180);
                    g.setColor(Color.RED);
                    g.drawArc(x2 + factor, y1 + factor, gap - 2 * factor, gap - 2 * factor, 0, 180);
                }
                */
                if (arcOut.contains(x, y) && !arcIn.contains(x, y)) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }

            } else if (defaultType == VERTICAL_LINE_TYPE) {
                if (this.isOnTheLine(x2, rect.y, x2, rect.y + rect.height, x, y, select, true)) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            }
        }

        return false;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_CYCLE_LINE");

    }

    protected String getValueString(boolean currMode) {
        return "";
    }

    public byte getDefaultType() {
        return defaultType;
    }

    public void setDefaultType(byte defaultType) {
        this.defaultType = defaultType;
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadFromLayout(xpath, document, preExpression);
        this.defaultType = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/mode"));

    }

    public void saveToLayout(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveToLayout(chart, document);
        TemplateFactory.saveProperty(chart, document, "mode", String.valueOf(defaultType));

    }

}
