package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectFibonacciRetrProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:20:05 PM
 */

public class ObjectFibonacciRetracements extends AbstractObject {

    public static final int CAPACITY = 16;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>(CAPACITY);
    public static final int ID_TREND = 0;
    public static final int ID_BEGIN = 1;
    public static final int ID_END = 2;
    public static final int ID_ONE = 3;
    public static final int ID_TWO = 4;
    public static final int ID_THREE = 5;
    public static final int ID_FOUR = 6;
    public static final int ID_FIVE = 7;
    public static final int ID_SIX = 8;
    public static final int ID_SEVEN = 9;
    public static final int ID_EIGHT = 10;
    public static final int ID_NINE = 11;
    public static final int ID_TEN = 12;
    public static final int ID_ELEVEN = 13;
    public static final int ID_TWELVE = 14;
    public static final int ID_THIRTEEN = 15;
    private static final long serialVersionUID = AbstractObject.UID_FIBONACCI_RETRACEMENTS;
    public static float[] defaultLevels = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f};
    protected ArrayList<Double> horizontalLines;
    protected Hashtable fibonacciLines;    //new design added by charithn
    protected Font font;
    DecimalFormat priceFormat = new DecimalFormat("###,##0.00");
    SimpleDateFormat timeFormatter = null;
    private boolean isUsingSameProperties = false;

    public ObjectFibonacciRetracements() {

    }

    public ObjectFibonacciRetracements(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_FIBONACCI_RETRACEMENTS;
        fibonacciLines = new Hashtable(CAPACITY);
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        isShowingDragToolTip = true;
        drawingOnBackground = false;

        Hashtable<Integer, FibonacciRetracementiLine> lines = getDefaultFibonacciLines();

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectFibonacciRetrProperty op = (ObjectFibonacciRetrProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            lines = op.getTable();     //overrides the default properties here....
            isUsingSameProperties = op.isUsingSameProperties();
            isFreeStyle = op.isFreeStyleMode();

        }
        for (int i = 0; i < lines.size(); i++) {
            FibonacciRetracementiLine line = lines.get(i);
            fibonacciLines.put(i, line);
        }
        /*for (int i = 0; i < defaultLevels.length; i++) {
            horizontalLines.add((double) defaultLevels[i]);
        }*/
    }

    public Hashtable sortHashTable(Hashtable table) {
        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
        Arrays.sort(UIkeys);

        Hashtable tableSort = new Hashtable();
        for (int key : UIkeys) {
            //int rowID = (Integer) ids.nextElement();
            FibonacciRetracementiLine line = (FibonacciRetracementiLine) table.get(key);
            tableSort.put(key, line);

        }

        return tableSort;
    }

    public Hashtable<Integer, FibonacciRetracementiLine> getDefaultFibonacciLines() {

        Hashtable<Integer, FibonacciRetracementiLine> lines = new Hashtable<Integer, FibonacciRetracementiLine>();

        //trend line
        Color trendLineColor = new Color(250, 0, 0);
        FibonacciRetracementiLine trendLine = new FibonacciRetracementiLine(Double.POSITIVE_INFINITY, trendLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_TREND, trendLine);

        //0% line
        Color trendBeginLineColor = new Color(250, 0, 250);
        FibonacciRetracementiLine trendBeginLine = new FibonacciRetracementiLine(0.0, trendBeginLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_BEGIN, trendBeginLine);

        //100% line
        Color trendEndLineColor = new Color(0, 160, 0);
        FibonacciRetracementiLine trendEndLine = new FibonacciRetracementiLine(1.0, trendEndLineColor, GraphDataManager.STYLE_SOLID, 1.0f, true);
        lines.put(ID_END, trendEndLine);

        //first line - 23.6%
        Color firstLineColor = new Color(25, 100, 255);
        FibonacciRetracementiLine firstLine = new FibonacciRetracementiLine(.236d, firstLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_ONE, firstLine);

        //second line - 38.2%
        Color secondLineColor = new Color(255, 51, 51);
        FibonacciRetracementiLine secondLine = new FibonacciRetracementiLine(.382d, secondLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_TWO, secondLine);

        //third line - 50.0%
        Color thirdLineColor = new Color(0, 0, 180);
        FibonacciRetracementiLine thirdLine = new FibonacciRetracementiLine(.5d, thirdLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_THREE, thirdLine);

        //fourth line - 61.8%
        Color fourthLineColor = new Color(204, 0, 0);
        FibonacciRetracementiLine fourthLine = new FibonacciRetracementiLine(.618d, fourthLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_FOUR, fourthLine);

        //fifth line - 76.4%
        Color fifthLineColor = new Color(0, 153, 153);
        FibonacciRetracementiLine fifthLine = new FibonacciRetracementiLine(.764d, fifthLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_FIVE, fifthLine);

        //fifth line - 161.8%
        Color sixthLineColor = new Color(255, 128, 0);
        FibonacciRetracementiLine sixthLine = new FibonacciRetracementiLine(1.618d, sixthLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_SIX, sixthLine);

        //fifth line - 261.8%
        Color seventhLineColor = new Color(0, 200, 200);
        FibonacciRetracementiLine seventhLine = new FibonacciRetracementiLine(2.618d, seventhLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_SEVEN, seventhLine);

        //fifth line - 423.6%
        Color eigtthLineColor = new Color(153, 152, 255);
        FibonacciRetracementiLine eightthLine = new FibonacciRetracementiLine(4.236d, eigtthLineColor, GraphDataManager.STYLE_DASH, 1.0f, true);
        lines.put(ID_EIGHT, eightthLine);

        lines.put(ID_NINE, new FibonacciRetracementiLine());
        lines.put(ID_TEN, new FibonacciRetracementiLine());
        lines.put(ID_ELEVEN, new FibonacciRetracementiLine());
        lines.put(ID_TWELVE, new FibonacciRetracementiLine());
        lines.put(ID_THIRTEEN, new FibonacciRetracementiLine());

        return lines;
    }

    public void assignValuesFrom(AbstractObject or) {
        /* super.assignValuesFrom(or);
        if (or instanceof ObjectFibonacciRetracements) {
            this.horizontalLines.clear();
            this.horizontalLines.addAll(((ObjectFibonacciRetracements) or).horizontalLines);
            font = ((ObjectFibonacciRetracements) or).font;
        }*/
        super.assignValuesFrom(or);
        if (or instanceof ObjectFibonacciRetracements) {
            //this.fibonacciLines.clear();
            this.fibonacciLines = sortHashTable(((ObjectFibonacciRetracements) or).getFibonacciLines());
            font = ((ObjectFibonacciRetracements) or).font;
            isUsingSameProperties = ((ObjectFibonacciRetracements) or).isUsingSameProperties;
        }
    }

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        int x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        int y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        if (objectMoving) {
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;
            y1 = y1 - (int) Yadj;
            y2 = y2 - (int) Yadj;
        } else {
            if (movingPtIndex == 0) {
                x1 = x1 - Xadj;
                y1 = y1 - (int) Yadj;
            } else {
                x2 = x2 - Xadj;
                y2 = y2 - (int) Yadj;
            }
        }
        g.drawLine(x1, y1, x2, y2);
        if ((x1 != x2)) {
            if (x1 > x2) {
                x1 = x2;
                x2 = y1; //used as a tmp var
                y1 = y2;
                y2 = x2;
            }
            int len = (y2 - y1);
            /*Object[] fibArr = horizontalLines.toArray();
            for (int i = 0; i < fibArr.length; i++) {
                int y = y2 - (int) Math.round((Double) fibArr[i] * len);
                g.drawLine(x1, y, x1 + grfWidth, y);
            }*/

            for (int i = ID_ONE; i < fibonacciLines.size(); i++) {

                FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
                if (line.isEnabled()) {
                    int y = y2 - (int) Math.round((Double) line.getPercentageValue() * len);
                    g.drawLine(x1, y, x1 + grfWidth, y);
                }
            }
            g.drawLine(x1, y1, x1 + grfWidth, y1);
            g.drawLine(x1, y2, x1 + grfWidth, y2);
        }

        //adding tooltip data rows
        dataRows.clear();

        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        float index2 = bIndex + (x2 - grfLeft) / xFactor;

        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
        Date D1, D2 = null;
        D1 = new Date(newXarray[0]);
        if (newXarray.length > 1) {
            D2 = new Date(newXarray[1]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);

        double begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
        double end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
        String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
        String endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));

        int len = (y2 - y1);
        for (int i = ID_ONE; i < fibonacciLines.size(); i++) {

            FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
            if (line.isEnabled()) {

                int y = y2 - (int) Math.round(line.getPercentageValue() * len);

                String val;

                if (newXarray[0] < newXarray[1]) {
                    val = priceFormat.format(begin + (1.0 - line.getPercentageValue()) * (end - begin));
                } else {
                    val = priceFormat.format(end + (1.0 - line.getPercentageValue()) * (begin - end));
                }
                dataRows.add(new ToolTipDataRow(String.valueOf((line.getPercentageValue() * 1000) / 10) + "%", String.valueOf(val)));
            }
        }

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {

        dataRows.clear();

        String beginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
        String endPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());

        Date D1 = null, D2 = null, D3 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }
        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        String trendBeginDate = timeFormatter.format(D1);
        String trendEndDate = timeFormatter.format(D2);

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), trendBeginDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), trendEndDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));

        Graphics2D g = (Graphics2D) gg;
        boolean drawSelected = selected && !isPrinting;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        int x1, x2, y1, y2;
        if (xArr[0] < xArr[1]) {
            x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        } else {
            x1 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        }
        int len = (y2 - y1);
        if (len != 0) {
            g.setStroke(dottedPen);
            g.setFont(font);
            FontMetrics fm = g.getFontMetrics(font);
            int strHt = fm.getHeight() / 3;

            //this is the for loop requireed for edit
            for (int i = ID_ONE; i < fibonacciLines.size(); i++) {

                FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
                if (line.isEnabled()) {
                    //set line colors, line style and line thickness
                    g.setColor(line.getLineColor());
                    BasicStroke pen = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                    g.setStroke(pen);

                    int y = y2 - (int) Math.round(line.getPercentageValue() * len);

                    String val;

                    if (xArr[0] < xArr[1]) {
                        val = priceFormat.format(yArray[0] + (1.0 - line.getPercentageValue()) * (yArray[1] - yArray[0]));
                    } else {
                        val = priceFormat.format(yArray[1] + (1.0 - line.getPercentageValue()) * (yArray[0] - yArray[1]));
                    }

                    dataRows.add(new ToolTipDataRow(String.valueOf((line.getPercentageValue() * 1000) / 10) + "%", String.valueOf(val)));

                    //added by sathyajith
                    int valueWidth = g.getFontMetrics().stringWidth(val);
                    g.drawLine(x1, y, rect.x + rect.width - valueWidth, y);
                    //String s = percentformat.format(fibArr[i]);
                    String s = percentformat.format(line.getPercentageValue());
                    int strWd = fm.stringWidth(s);
                    g.drawString(s, x1 - strWd - halfBox, y + strHt);
                    g.drawString(val, rect.x + rect.width - valueWidth, y); //TODO
                }
            }

            //drawing the trend line, 0% and 100% lines
            g.setStroke(pen);
            String y1Str;
            String y2Str;
            if (xArr[0] < xArr[1]) {
                y1Str = priceFormat.format(yArray[0]);
                y2Str = priceFormat.format(yArray[1]);
            } else {
                y1Str = priceFormat.format(yArray[1]);
                y2Str = priceFormat.format(yArray[0]);
            }

            int y1Width = g.getFontMetrics().stringWidth(y1Str);
            int y2Width = g.getFontMetrics().stringWidth(y2Str);

            if (fibonacciLines.size() > 0) {
                /* =========================== 0% line ==================================*/
                FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(1);
                g.setColor(line.getLineColor());
                Stroke secondPen = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                g.setStroke(secondPen);
                g.drawLine(x1, y2, rect.x + rect.width - y2Width, y2);


                String s = "0.0 %";
                int strWd = fm.stringWidth(s);
                g.drawString(s, x1 - strWd - halfBox, y2 + strHt);
                g.drawString(y2Str, rect.x + rect.width - y2Width, y2);

                /* =========================== 100% line ==================================*/
                line = (FibonacciRetracementiLine) fibonacciLines.get(2);
                g.setColor(line.getLineColor());
                Stroke firstPen = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                g.setStroke(firstPen);
                g.drawLine(x1, y1, rect.x + rect.width - y1Width, y1);

                s = "100.0 %";
                strWd = fm.stringWidth(s);
                g.drawString(s, x1 - strWd - halfBox, y1 + strHt);
                g.drawString(y1Str, rect.x + rect.width - y1Width, y1);
            }

        }
        if (drawSelected) {
            /* =========================== trend line ==================================*/
            if (fibonacciLines.size() > 0) {
                FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(0);
                Stroke stroke = PropertyDialogFactory.getBasicStroke(line.getPenStyle(), line.getPenWidth());
                g.setStroke(stroke);

                g.setColor(line.getLineColor());
                g.drawLine(x1, y1, x2, y2);

                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                g.setStroke(selectPen);
                g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                g.setColor(Color.BLACK);
                g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            }
        }
    }


    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        if (!rect.contains(x, y) || !isMove) return false;

        int x1, x2, y1, y2;

        if (xArray[0] < xArray[1]) {
            x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
            if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
                return true;
            }
        } else {
            x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
            if (isOnTheLine(x2, y2, x1, y1, x, y, select, isMove)) {
                return true;
            }
        }
        if ((x1 != x2)) {
            int len = (y2 - y1);
            /*Object[] fibArr = horizontalLines.toArray(); // added 0.764f - pramoda
            for (int i = 0; i < fibArr.length; i++) {
                int y0 = y2 - (int) Math.round((Double) fibArr[i] * len);
                if (isOnLineForMove(x1, y0, x1 + grfWidth, y0, x, y, select)) {
                    return true;
                }
            }*/
            for (int i = ID_ONE; i < fibonacciLines.size(); i++) {

                FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
                if (line.isEnabled()) {
                    int y0 = y2 - (int) Math.round((Double) line.getPercentageValue() * len);
                    if (isOnLineForMove(x1, y0, x1 + grfWidth, y0, x, y, select)) {
                        return true;
                    }
                }
            }
            if (isOnLineForMove(x1, y1, x1 + grfWidth, y1, x, y, select)) {
                return true;
            }
            if (isOnLineForMove(x1, y2, x1 + grfWidth, y2, x, y, select)) {
                return true;
            }
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_FIB_RETRC");
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        Date D1;
        D1 = new Date(xArray[0]);
        //return timeFormatter.format(D1);
        return "";
    }

    public ArrayList<Double> getHorizontalLines() {
        return horizontalLines;
    }

    public void setHorizontalLines(ArrayList<Double> horizontalLines) {
        this.horizontalLines = horizontalLines;
    }

    public Hashtable getFibonacciLines() {
        return fibonacciLines;
    }

    public void setFibonacciLines(Hashtable lines) {
        this.fibonacciLines = lines;
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        int y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        int x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        int y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        int x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        int x0 = r.x;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //added later
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        //detect the other lines
        int xx = Math.min(x1, x2);
        int yMin = Integer.MAX_VALUE;
        int yMax = Integer.MIN_VALUE;
        for (int i = ID_BEGIN; i < fibonacciLines.size(); i++) {

            FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
            if (line.isEnabled()) {
                int yy = y2 - (int) Math.round(line.getPercentageValue() * (y2 - y1));
                yMin = Math.min(yy, yMin);
                yMax = Math.max(yy, yMax);
            }
        }
        Rectangle rec = new Rectangle(xx, yMin, xx + 99999, yMax - yMin);
        if (r.intersects(rec) || r.contains(rec)) {
            return true;
        }

        return false;
    }

    @Override
    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

    public boolean isUsingSameProperties() {
        return isUsingSameProperties;
    }

    public void setUsingSameProperties(boolean usingSameProperties) {
        isUsingSameProperties = usingSameProperties;
    }

    public Hashtable getSavedLines() {
        boolean isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(AbstractObject.INT_FIBONACCI_RETRACEMENTS);
        if (isUsingUserDefault) {
            ObjectFibonacciRetrProperty op = (ObjectFibonacciRetrProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(AbstractObject.INT_FIBONACCI_RETRACEMENTS);
            return op.getTable();    //overrides the default properties here....
        }
        return getDefaultFibonacciLines();
    }
}
