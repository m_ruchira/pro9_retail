package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Sep 11, 2008
 * Time: 1:47:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectRectangularSelection extends LineStudy {

    public ObjectRectangularSelection() {
        super();
        objType = AbstractObject.INT_RECT_SELECTION;
    }

    public ObjectRectangularSelection(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r) {
        super(xArr, yArr, null, cp, r, null);
        DISPLAY_TYPE = AbstractObject.DISPLAY_SATATIC;
        objType = AbstractObject.INT_RECT_SELECTION;
    }
}
