package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.WindowPanel;
import com.isi.csvr.theme.Theme;

import java.awt.*;

/**
 * User: Pramoda
 * Date: Oct 4, 2006
 * Time: 12:01:43 PM
 */
public class ObjectOrderLine extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_LINE_HORIZ;

    private String descript;
    private Color fontColor;
    private String id;
    private boolean remove = false;

    public ObjectOrderLine() {
        super();
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = "";
        fontColor = new Color(0, 0, 0);
    }

    public ObjectOrderLine(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r) {
        super(xArr, yArr, null, cp, r, null);
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = "";
        fontColor = new Color(0, 0, 0);
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        g.drawLine(rect.x, y1, rect.x + rect.width, y1);
        g.setColor(fontColor);
        g.setFont(Theme.getDefaultFont(Font.PLAIN, 9));
        g.drawString(descript, rect.x + 2, y1 - 2);
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        return ((y - y1) * (y - y1) < 4) && (rect.contains(x, y));
    }

    public String getShortName() {
        return descript;
    }

    public String getValueString(boolean currMode) {
        return graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isRemove() {
        return remove;
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    public boolean equals(Object anotherLine) {
        boolean equals = false;
        if (!(anotherLine instanceof ObjectOrderLine)) return false;
        ObjectOrderLine objectOrderLine = (ObjectOrderLine) anotherLine;
        if (yArray[0] == objectOrderLine.yArray[0] && id.equals(objectOrderLine.id)) {
            equals = true;
        }
        return equals;
    }
}
