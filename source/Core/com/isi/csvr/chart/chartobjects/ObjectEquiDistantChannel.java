package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Pramoda
 * Date: Nov 1, 2006
 * Time: 1:57:33 PM
 */
public class ObjectEquiDistantChannel extends AbstractObject {

    public static final int INITIAL_DISTANCE = -50;

    private boolean extendedLeft;
    private boolean extendedRight;
    private double distance;

    private StockGraph graph;

    public ObjectEquiDistantChannel() {
        super();
        objType = INT_EQUI_DIST_CHANNEL;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectEquiDistantChannel(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, null);
        objType = INT_EQUI_DIST_CHANNEL;
        this.graph = graph;
        distance = graph.GDMgr.getYValueGapForthePixelGap(INITIAL_DISTANCE,
                graph.GDMgr.getIndexOfTheRect(graph.panels, r));
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));

        }
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectEquiDistantChannel) {
            this.extendedLeft = ((ObjectEquiDistantChannel) ao).extendedLeft;
            this.extendedRight = ((ObjectEquiDistantChannel) ao).extendedRight;
            this.distance = ((ObjectEquiDistantChannel) ao).distance;
        }
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        int x1, x2, y1, y2, dragDist;
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        if (objectMoving) {
            x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
            x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
            y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
            y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;
            y1 = y1 - (int) Yadj;
            y2 = y2 - (int) Yadj;
            dragDist = graph.GDMgr.getPixelGapFortheYValueGap(distance, pnlID);
        } else {
            if (movingPtIndex >= 0) { // sizing the line
                x1 = graph.GDMgr.getPixelFortheIndex(xArr[movingPtIndex]);
                x2 = graph.GDMgr.getPixelFortheIndex(xArr[1 - movingPtIndex]);
                y1 = graph.GDMgr.getPixelFortheYValue(yArray[movingPtIndex], pnlID);
                y2 = graph.GDMgr.getPixelFortheYValue(yArray[1 - movingPtIndex], pnlID);
                x1 = x1 - Xadj;
                y1 = y1 - (int) Yadj;
                dragDist = graph.GDMgr.getPixelGapFortheYValueGap(distance, pnlID);
            } else { // changing the distance
                x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
                x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
                y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
                y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);
                dragDist = graph.GDMgr.getPixelGapFortheYValueGap(distance, pnlID) - (int) Yadj;
            }
        }

        g.drawLine(x1, y1, x2, y2);
        g.drawLine(x1, y1 + dragDist, x2, y2 + dragDist);
    }

    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                               int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        boolean drawSelected = selected && !isPrinting;
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect);

        int x1, x2, y1, y2;
        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
        x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
        y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
        y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        int tempX1 = x1;
        int tempX2 = x2;
        int tempY1 = y1;
        int tempY2 = y2;
        if (extendedLeft) {
            float ratio = 1f;
            if ((x1 == x2)) {
                tempY1 = rect.y + GraphDataManager.titleHeight;
            } else {
                ratio = (float) (y2 - y1) / (float) (x2 - x1);
                int pixGap = (x2 > x1) ? rect.width : -rect.width;
                tempX1 = x1 - pixGap;
                tempY1 = y1 - Math.round(ratio * pixGap);
            }
        }
        if (extendedRight) {
            float ratio = 1f;
            if ((x1 == x2)) {
                tempY2 = rect.y + rect.height;
            } else {
                ratio = (float) (y2 - y1) / (float) (x2 - x1);
                int pixGap = (x2 > x1) ? rect.width : -rect.width;
                tempX2 = x1 + pixGap;
                tempY2 = y1 + Math.round(ratio * pixGap);
            }
        }
        int dist = graph.GDMgr.getPixelGapFortheYValueGap(distance, pnlID);
        g.drawLine(tempX1, tempY1, tempX2, tempY2);
        g.drawLine(tempX1, tempY1 + dist, tempX2, tempY2 + dist);

        if (drawSelected) {
            int x3, y3;
            x3 = (x1 + x2) / 2;
            y3 = (y1 + y2) / 2 + dist;
            g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.black);
            g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x3 - halfBox, y3 - halfBox, halfBox * 2, halfBox * 2);
        }
    }

    public boolean isCursorOnObject(int x, int y, float[] xArr, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        int x1, x2, y1, y2;
        xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
        x1 = graph.GDMgr.getPixelFortheIndex(xArr[0]);
        x2 = graph.GDMgr.getPixelFortheIndex(xArr[1]);
        y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);
        y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            return true;
        }
        int dist = graph.GDMgr.getPixelGapFortheYValueGap(distance, pnlID);
        int x3, y3;
        x3 = (x1 + x2) / 2;
        y3 = (y1 + y2) / 2 + dist;
        if (((x - x3) * (x - x3) <= halfBox * halfBox) && ((y - y3) * (y - y3) <= halfBox * halfBox)) {
            // changing distance
            movingPtIndex = -1;
            objectMoving = false;
            if (!selected) {
                selected = select;
            }
            return true;
        }

        int tempX1 = x1;
        int tempX2 = x2;
        int tempY1 = y1;
        int tempY2 = y2;
        if (extendedLeft) {
            float ratio = 1f;
            if ((x1 == x2)) {
                tempY1 = rect.y + GraphDataManager.titleHeight;
            } else {
                ratio = (float) (y2 - y1) / (float) (x2 - x1);
                int pixGap = (x2 > x1) ? rect.width : -rect.width;
                tempX1 = x1 - pixGap;
                tempY1 = y1 - Math.round(ratio * pixGap);
            }
        }
        if (extendedRight) {
            float ratio = 1f;
            if ((x1 == x2)) {
                tempY2 = rect.y + rect.height;
            } else {
                ratio = (float) (y2 - y1) / (float) (x2 - x1);
                int pixGap = (x2 > x1) ? rect.width : -rect.width;
                tempX2 = x1 + pixGap;
                tempY2 = y1 + Math.round(ratio * pixGap);
            }
        }
        return rect.contains(x, y) && (isOnLineForMove(tempX1, tempY1, tempX2, tempY2, x, y, select) ||
                isOnLineForMove(tempX1, tempY1 + dist, tempX2, tempY2 + dist, x, y, select));
    }

    public String getValueString(boolean currMode) {
        SimpleDateFormat timeFormat;
        if (currMode) {
            timeFormat = new SimpleDateFormat("HH:mm");
        } else {
            timeFormat = new SimpleDateFormat("dd/MM/yy");
        }
        Date DT1 = new Date(xArray[0]);
        Date DT2 = new Date(xArray[1]);
        return timeFormat.format(DT1) + ", " + timeFormat.format(DT2);
    }

    public String getShortName() {
        return Language.getString("OBJECT_EQU_DIST_CHANNEL");
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        double y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        double x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        double y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        double x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        boolean inside = false;
        double xMin = Math.min(x1, x2);
        double xMax = Math.max(x1, x2);

        //if line line points doesnot contains inside the rectangle
        for (double i = xMin; i < xMax; i = i + 1) {
            double y = m * i + c;
            if (r.contains(i, y)) {
                inside = true;
                break;
            }
        }
        if (!inside) {
            return false;
        }

        int x0 = r.x;
        int y0 = r.y;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //m is negative and intersects the rectangle
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }
        return false;

    }
}
