package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectLineProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 11:49:46 AM
 */
//public class ObjectLineSlope extends AbstractObject implements Extendible,InfoObjectDisplayble {
public class ObjectLineSlope extends AbstractObject implements InfoObjectDisplayble, Alarmable {

    public static final String ALARM_TMESSAGE = "AlarmMessage";
    private static final long serialVersionUID = AbstractObject.UID_LINE_SLOPE;
    TWDecimalFormat formatter = new TWDecimalFormat("#############.00");
    SimpleDateFormat timeFormatter = null;
    private boolean extendedLeft;
    private boolean extendedRight;
    private boolean alarmEnabled;
    private boolean barCount;
    private boolean netChange;
    private boolean pctChange;
    private boolean timeSpan;
    private double angle = -1;
    private boolean isAngle = false;
    private String alarmMesage = null;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();

    public ObjectLineSlope() {
        super();
        objType = AbstractObject.INT_LINE_SLOPE;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectLineSlope(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_LINE_SLOPE;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        alarmMesage = Language.getString("TREND_LINE_POPUP_MSG");
        isShowingDragToolTip = true;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectLineProperty op = (ObjectLineProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.extendedLeft = op.isExtendedLeft();
            this.extendedRight = op.isExtendedRight();
            this.barCount = op.isShowBarCount();
            this.pctChange = op.isShowPctChange();
            this.netChange = op.isShowNetChange();
            this.angle = op.getAngle();
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectLineSlope) {
            this.extendedLeft = ((ObjectLineSlope) ao).extendedLeft;
            this.extendedRight = ((ObjectLineSlope) ao).extendedRight;

            this.pctChange = ((ObjectLineSlope) ao).pctChange;
            this.barCount = ((ObjectLineSlope) ao).barCount;
            this.timeSpan = ((ObjectLineSlope) ao).timeSpan;
            this.netChange = ((ObjectLineSlope) ao).netChange;
            this.isAngle = ((ObjectLineSlope) ao).isAngle;
            this.angle = ((ObjectLineSlope) ao).angle;
            this.alarmEnabled = ((ObjectLineSlope) ao).alarmEnabled;
            this.alarmMesage = ((ObjectLineSlope) ao).alarmMesage;
        }
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        int x1 = 0, x2 = 0, y1, y2;

        int tempXX1 = 0;
        int tempXX2 = 0;
        int tempYY1 = 0;
        int tempYY2 = 0;

        if (objectMoving) {
            x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            //x1 = Math.round((indexArray[0] - bIndex) * xFactor) + grfLeft;
            //x2 = Math.round((indexArray[1] - bIndex) * xFactor) + grfLeft;

            y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;

            if (isSnapToPrice) {
                y1 = snapPixYArr[0];
                y2 = snapPixYArr[1];
            } else {

                if (((WindowPanel) rect).isSemiLog()) {
                    //move by the price difference (not the pixel diffrence)  //here y adj is a price gap (not a pixel gap

                    double y1Price = yArray[0] - Yadj; //deltaY
                    double y2Price = yArray[1] - Yadj;
                    y1 = getPixelFortheYValue(y1Price, rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(y2Price, rect, minY, yFactor, grf_Ht + grf_Top);
                } else {
                    y1 = y1 - (int) Yadj;
                    y2 = y2 - (int) Yadj;
                }
            }

            tempXX1 = (x1 <= x2) ? x1 : x2;
            tempXX2 = (x1 <= x2) ? x2 : x1;
            tempYY1 = (x1 <= x2) ? y1 : y2;
            tempYY2 = (x1 <= x2) ? y2 : y1;

            if (extendedLeft) {
                float gradient = (float) (y2 - y1) / (float) (x2 - x1);
                float interceptY = y1 - (gradient * x1);
                if ((x1 == x2)) {
                    tempYY1 = grf_Top + grf_Ht;
                } else {
                    if (gradient != 0) {
                        tempYY1 = (int) interceptY;
                        tempXX1 = 0;
                    } else {
                        tempXX1 = rect.x;
                    }
                }
            }
            if (extendedRight) {
                float gradient = (float) (y2 - y1) / (float) (x2 - x1);
                float interceptY = y1 - (gradient * x1);
                if ((x1 == x2)) {
                    tempYY2 = grf_Top - grf_Ht;
                } else {
                    if (gradient != 0) {
                        tempXX2 = grfLeft + grfWidth;
                        tempYY2 = (int) (gradient * tempXX2 + interceptY);
                    } else {
                        tempXX2 = rect.x + rect.width;
                    }
                }
            }

        } else {

            //System.out.println(movingPtIndex);

            //x1 = Math.round((indexArray[movingPtIndex] - bIndex) * xFactor) + grfLeft - Xadj;
            //x2 = Math.round((indexArray[1 - movingPtIndex] - bIndex) * xFactor) + grfLeft;

            if (isSnapToPrice) {
                y1 = snapPixYArr[movingPtIndex];
                y2 = snapPixYArr[1 - movingPtIndex];
            } else {
                if (movingPtIndex == 0) {
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top - (int) Yadj);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft - Xadj;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;

                } else {
                    y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
                    y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top - (int) Yadj);
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft - Xadj;
                }
            }

            tempXX1 = (x1 <= x2) ? x1 : x2;
            tempXX2 = (x1 <= x2) ? x2 : x1;
            tempYY1 = (x1 <= x2) ? y1 : y2;
            tempYY2 = (x1 <= x2) ? y2 : y1;

            if (extendedLeft) {
                float gradient = (float) (y2 - y1) / (float) (x2 - x1);
                float interceptY = y1 - (gradient * x1);
                if ((x1 == x2)) {
                    tempYY1 = grf_Top + grf_Ht;
                } else {
                    if (gradient != 0) {
                        tempYY1 = (int) interceptY;
                        tempXX1 = 0;
                    } else {
                        tempXX1 = rect.x;
                    }
                }
            }
            if (extendedRight) {
                float gradient = (float) (y2 - y1) / (float) (x2 - x1);
                float interceptY = y1 - (gradient * x1);
                if ((x1 == x2)) {
                    tempYY2 = grf_Top - grf_Ht;
                } else {
                    if (gradient != 0) {
                        tempXX2 = grfLeft + grfWidth;
                        tempYY2 = (int) (gradient * tempXX2 + interceptY);
                    } else {
                        tempXX2 = rect.x + rect.width;
                    }
                }
            }
        }
        //g.drawLine(x1, y1, x2, y2);
        g.drawLine(tempXX1, tempYY1, tempXX2, tempYY2);

        //constructing tool tip data
        int tempX1 = x1;
        int tempX2 = x2;
        int tempY1 = y1;
        int tempY2 = y2;

        dataRows.clear();

        boolean currentMode = graph.isCurrentMode();
        if (currentMode) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        double angleTemp = 0;
        angleTemp = -(double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
        angleTemp = Math.atan(angleTemp);
        if (tempY1 > tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp);
        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 360;
        }

//        angleTemp = (double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
//        angleTemp = Math.atan(angleTemp);
//        if (tempY1 < tempY2 && tempX1 < tempX2) {
//            angleTemp = Math.toDegrees(angleTemp);
//        } else if (tempY1 > tempY2 && tempX1 < tempX2) {
//            angleTemp = 180 - Math.toDegrees(angleTemp) ;
//        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
//            angleTemp = Math.toDegrees(angleTemp) + 180;
//        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
//            angleTemp = -Math.toDegrees(angleTemp);
//        }

        this.angle = angleTemp;
        double begin = getYValueForthePixel(tempY1, rect, minY, yFactor, grf_Ht + grf_Top);
        double end = getYValueForthePixel(tempY2, rect, minY, yFactor, grf_Ht + grf_Top);

        //System.out.println("begin " + begin + "  end" + end);
        String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
        String endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

        double dNetChange = end - begin;
        double dpctChange = 0d;
        if (begin != 0.0) dpctChange = dNetChange * 100 / begin;
        String sPctChg = formatter.format(dpctChange) + "%";

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        float index2 = bIndex + (x2 - grfLeft) / xFactor;

        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
        Date D1, D2 = null;
        D1 = new Date(newXarray[0]);
        if (newXarray.length > 1) {
            D2 = new Date(newXarray[1]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);

        int barCount = Math.abs(Math.round(index2 - index1));

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_ANGLE"), String.valueOf(formatter.format(angleTemp))));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_NET_CHANGE"), formatter.format(end - begin)));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_PCT_CHANGE"), sPctChg));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BARS"), String.valueOf(barCount)));
    }

    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                               int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        boolean drawSelected = selected && !isPrinting;
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect);
        g.setFont(new Font("Tahoma", Font.PLAIN, 11));
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        int tempX1 = (x1 <= x2) ? x1 : x2;
        int tempX2 = (x1 <= x2) ? x2 : x1;
        int tempY1 = (x1 <= x2) ? y1 : y2;
        int tempY2 = (x1 <= x2) ? y2 : y1;

        /* Edited by Mevan @ 2009-07-22
       *
       *  Fixed a bug which existed with extending left & right.
       *  Extending Left happens to the left Side of Screen & vise versa, regardless of the points 1. & 2.
       *  Y = mX + c : m=gradient , c=interceptY , m = (Y2-Y1)/(X2-X1)
       *
        */
//        if (extendedLeft) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    if (x1 < x2) {
//                        tempY1 = (int) interceptY;
//                        tempX1 = (int) ((tempY2 - interceptY) / gradient);
//                        tempX1 = 0;
//                    } else {
//                        tempX1 = grfLeft + grfWidth;
//                        tempY1 = (int) (gradient * tempX1 + interceptY);
//                    }
//                } else {
//                    tempY1 = tempY2;
//                    tempX1 = (x1 < x2) ? 0 : (rect.x + rect.width);
//                }
//            }
//        }
//        if (extendedRight) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    if (x1 > x2) {
//                        tempY2 = (int) interceptY;
//                        tempX2 = 0;
//                    } else {
//                        tempX2 = grfLeft + grfWidth;
//                        tempY2 = (int) (gradient * tempX2 + interceptY);
//                    }
//                } else {
//                    tempY2 = tempY1;
//                    tempX2 = (x1 < x2) ? rect.x + rect.width : 0;
//                }
//            }
//        }


//        if (extendedLeft) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            tempX1 = 0;
//            tempY1 = (int) interceptY;
//            if (x1 < x2) {
//                tempX2 = x2;
//                tempY2 = y2;
//            } else {
//                tempX2 = x1;
//                tempY2 = y1;
//            }
//            if (Float.isInfinite(gradient)) {
//                tempX2 = (gradient < 0) ? x2 : x2;
//                tempY2 = (gradient < 0) ? y2 : y2;
//            }
////            g.drawString(Float.toString(gradient), 50, 50);
//        }

//        if (extendedRight) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            tempX2 = grfLeft + grfWidth;
//            tempY2 = (int) (gradient * tempX2 + interceptY);
//            if (x1 < x2) {
//                tempX1 = x1;
//                tempY1 = y1;
//            } else {
//                tempX1 = x2;
//                tempY1 = y2;
//            }
//            if (Float.isInfinite(gradient)) {
//                tempX1 = (gradient < 0) ? x1 : x1;
//                tempY1 = (gradient < 0) ? y1 : y2;
//            }
//        }


        if (extendedLeft) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY1 = grf_Top + grf_Ht;
            } else {
                if (gradient != 0) {
                    tempY1 = (int) interceptY;
                    tempX1 = 0;
                } else {
                    tempX1 = rect.x;
                }
            }
        }
        if (extendedRight) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    tempX2 = grfLeft + grfWidth;
                    tempY2 = (int) (gradient * tempX2 + interceptY);
                } else {
                    tempX2 = rect.x + rect.width;
                }
            }
        }

        g.drawLine(tempX1, tempY1, tempX2, tempY2);

        /************************************ Object information Lables ************************************/

        double angleTemp = 0;
        angleTemp = -(double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
        angleTemp = Math.atan(angleTemp);
        if (tempY1 > tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp);
        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 360;
        }

        this.angle = angleTemp;


        Point barCountDrawPoint = getBarCountDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));
        Point netChangeDrawPoint = getNetChangeDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));
        Point pctChangeDrawPoint = getPctChangeDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));

        /***********************Bar Count***********************/
        if (barCount) {
            int iBarCount = Math.round(xArr[1] - xArr[0]);
            int x = (tempX1 + tempX2) / 2 + 20;
            int y = (tempY1 + tempY2) / 2 + 4;
            //g.drawString(Math.abs(iBarCount) + " " + Language.getString("GRAPH_TRENDS_BAR_COUNT"), x, y);
            g.drawString(Math.abs(iBarCount) + " " + Language.getString("GRAPH_TRENDS_BAR_COUNT"), barCountDrawPoint.x, barCountDrawPoint.y);
        }
        if (netChange) {
            double dNetChange = yArray[1] - yArray[0];
            String sNetChange = formatter.format(dNetChange);
            if (dNetChange > 0) sNetChange = "+" + sNetChange;
            int x = (tempX1 + tempX2) / 2 + 10;
            int y = (tempY1 + tempY2) / 2 + 10 + 4;
            //g.drawString(sNetChange, x, y);
            g.drawString(sNetChange, netChangeDrawPoint.x, netChangeDrawPoint.y);
        }
        /***********************Bar Count***********************/

        /***********************pctChange***********************/
        if (pctChange) {
            double dNetChange = yArray[1] - yArray[0];
            double dpctChange = 0d;
            if (yArray[0] != 0.0) dpctChange = dNetChange * 100 / yArray[0];
            String sPctChg = formatter.format(dpctChange);
            if (dpctChange > 0) sPctChg = "+" + sPctChg;
            int x = (tempX1 + tempX2) / 2 + 10;
            int y = (tempY1 + tempY2) / 2 + 20 + 4;
            //g.drawString(sPctChg + "%", x, y);
            g.drawString(sPctChg + "%", pctChangeDrawPoint.x, pctChangeDrawPoint.y);
        }
        /***********************pctChange***********************/


        /***********************timeSpan***********************/
        //ToDo need to pass the mode to drawOnGraphichs method
        /*   if (timeSpan) {
            int diffTimeSpan;
            if (true) {
                long lTimeSpan = xArray[1] - xArray[0];
                diffTimeSpan = (int) (lTimeSpan / (long) 1000 * 24 * 3600);
            }
            g.drawString("Time Span: " + diffTimeSpan , tempX1 - 20, tempY1 - 10);
            g.drawString("" + diffTimeSpan , tempX1 - 20, tempY1 - 10);
        }*/
        /***********************timeSpan***********************/

/************************************ end of Object information Lables ************************************/
        if (drawSelected) {
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            g.setStroke(selectPen);


            g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.BLACK);
            g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            //g.drawRect(tempX2 - halfBox, tempY2 - halfBox, halfBox * 2, halfBox * 2);
        }

        //constructing tool tip data
        dataRows.clear();
        double dNetChange = yArray[1] - yArray[0];
        double dpctChange = 0d;
        if (yArray[0] != 0.0) dpctChange = dNetChange * 100 / yArray[0];
        String sPctChg = formatter.format(dpctChange) + "%";

        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        Date D1, D2 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);
        String beginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
        String endPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());

        /*double angleTemp = 0;
        angleTemp = -(double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
        angleTemp = Math.atan(angleTemp);
        if (tempY1 > tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp);
        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 360;
        }


        this.angle = angleTemp;*/
        int barCount = Math.abs(Math.round(xArr[1] - xArr[0]));

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_ANGLE"), String.valueOf(formatter.format(angleTemp))));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_NET_CHANGE"), formatter.format(yArray[1] - yArray[0])));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_PCT_CHANGE"), sPctChg));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BARS"), String.valueOf(barCount)));
    }

    private Point getBarCountDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if (angle >= 0 && angle <= 45) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 15;
        } else if (angle >= 300 && angle <= 360) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 12;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 4;
        }
        return p;
    }

    private Point getNetChangeDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if (angle >= 0 && angle <= 45) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 25;
        } else if (angle >= 300 && angle <= 360) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 22;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 14;
        }
        return p;
    }

    private Point getPctChangeDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if (angle >= 0 && angle <= 45) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 35;
        } else if (angle >= 300 && angle <= 360) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 32;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 24;
        }
        return p;
    }

    public boolean isCursorOnObject(int x, int y, float[] xArr, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {


        if (!rect.contains(x, y) || !isMove)
            return false;// Added to return false if out of the chart area. - Mevan @ 2009-07-21
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        //x1 = Math.round((indexArray[0] - bIndex) * xFactor) + grfLeft;
        //x2 = Math.round((indexArray[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        //Added in order to select the extension - Pramoda
//        int tempX1 = x1;
//        int tempX2 = x2;
//        int tempY1 = y1;
//        int tempY2 = y2;

        int tempX1 = (x1 <= x2) ? x1 : x2;
        int tempX2 = (x1 <= x2) ? x2 : x1;
        int tempY1 = (x1 <= x2) ? y1 : y2;
        int tempY2 = (x1 <= x2) ? y2 : y1;

        /* Edited by Mevan @ 2009-07-21
       *
       *  Fixed a bug which existed with extending left & right.
       *  Extending Left happens from the point 1. & extending Right from point 2.,
       *  regardless of the screen position to avoid unwanted confusions.
       *  Y = mX + c : m=gradient , c=interceptY , m = (Y2-Y1)/(X2-X1)
       *
        */

        if (extendedLeft) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY1 = grf_Top + grf_Ht;
            } else {
                if (gradient != 0) {
                    tempY1 = (int) interceptY;
                    tempX1 = 0;
                } else {
                    tempX1 = rect.x;
                }
            }
        }
        if (extendedRight) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    tempX2 = grfLeft + grfWidth;
                    tempY2 = (int) (gradient * tempX2 + interceptY);
                } else {
                    tempX2 = rect.x + rect.width;
                }
            }
        }

//        if (extendedLeft) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY1 = grf_Top + grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    tempY1 = (int) interceptY;
//                    tempX1 = 0;
//                } else {
//                    tempX1 = rect.x;
//                }
//            }
////            g.drawString("X1 : " + tempX1 + "  Y1 : " + tempY1, 50, 75);    // display results on canvas for temporary usage
////            g.drawString("X2 : " + tempX2 + "  Y2 : " + tempY2, 50, 100);    // display results on canvas for temporary usage
//        }
//         if (extendedRight) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    tempX2 = grfLeft + grfWidth;
//                    tempY2 = (int) (gradient * tempX2 + interceptY);
//                } else {
//                    tempX2 = rect.x + rect.width;
//                }
//            }
////            g.drawString("X1 : " + tempX1 + "  Y1 : " + tempY1, 50, 75);    // display results on canvas for temporary usage
////            g.drawString("X2 : " + tempX2 + "  Y2 : " + tempY2, 50, 100);    // display results on canvas for temporary usage
//        }

//        if (extendedLeft) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    if (x1 < x2) {
//                        tempY1 = (int) interceptY;
//                        tempX1 = (int) ((tempY2 - interceptY) / gradient);
//                        tempX1 = 0;
//                    } else {
//                        tempX1 = grfLeft + grfWidth;
//                        tempY1 = (int) (gradient * tempX1 + interceptY);
//                    }
//                } else {
//                    tempY1 = tempY2;
//                    tempX1 = (x1 < x2) ? 0 : (rect.x + rect.width);
//                }
//            }
//        }
//        if (extendedRight) {
//            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
//            float interceptY = y1 - (gradient * x1);
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                if (gradient != 0) {
//                    if (x1 > x2) {
//                        tempY2 = (int) interceptY;
//                        tempX2 = 0;
//                    } else {
//                        tempX2 = grfLeft + grfWidth;
//                        tempY2 = (int) (gradient * tempX2 + interceptY);
//                    }
//                } else {
//                    tempY2 = tempY1;
//                    tempX2 = (x1 < x2) ? rect.x + rect.width : 0;
//                }
//            }
//        }

////        if (extendedLeft) {
////            float ratio;
////            if ((x1 == x2)) {
////                tempY1 = grf_Top - grf_Ht;
////            } else {
////                ratio = (float) (y2 - y1) / (float) (x2 - x1);
////                tempX1 = x1 - rect.width;
////                tempY1 = y1 - Math.round(ratio * rect.width);
////            }
////        }
////        if (extendedRight) {
////            float ratio;
////            if ((x1 == x2)) {
////                tempY2 = grf_Top - grf_Ht;
////            } else {
////                ratio = (float) (y2 - y1) / (float) (x2 - x1);
////                tempX2 = x1 + rect.width;
////                tempY2 = y1 + Math.round(ratio * rect.width);
////            }
////        }
//
//        if (extendedLeft) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY1 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                // System.out.println("Ratio "+ ratio );
//                //tempX1 = x1 - extendX;
//                //tempY1 = y1 - Math.round(ratio * extendX);
//                if (ratio != 0) {
//                    float c = y1 - (ratio * x1);
//                    tempY1 = getPixelFortheYValue(minY, rect, minY, yFactor, grf_Ht + grf_Top);
//                    tempX1 = (int) ((tempY1 - c) / ratio);
//                } else {
//                    //tempY1 = getPixelFortheYValue(minY, rect, minY, yFactor, grf_Ht + grf_Top);
//                    tempX1 = rect.x;
//
//                }
//            }
//
//            // g.drawString("Ratio : " + x2 +  "  Y : " + y2, 50,100);    // display results on canvas for temporary usage
//        }
//        if (extendedRight) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                //tempX2 = x1 + extendX;
//                //tempY2 = y1 + Math.round(ratio * extendX);
//                if (ratio != 0) {
//                    tempY2 = rect.y;
//                    float c = y1 - (ratio * x1);
//                    tempX2 = (int) ((tempY2 - c) / ratio);
//                } else {
//                    tempX2 = rect.x + rect.width;
//                }
//            }
//        }
        boolean isOnTheLine = isOnTheLine(tempX1, tempY1, tempX2, tempY2, x, y, select, isMove, x1, y1, x2, y2);
        if (isOnTheLine) {
            float xIndex = graph.GDMgr.getIndexForthePixel(x);
            long xTime = graph.GDMgr.getTimeMillisec(Math.round(xIndex));
            Date D1 = new Date(xTime);
            String beginDate = timeFormatter.format(D1);
            String lblDate = graph.isCurrentMode() ? Language.getString("TIME") : Language.getString("DATE");

            int panelID = graph.getPanelIDatPoint(x, y);
            double priceYVal = graph.GDMgr.getYValueForthePixel(y, panelID);
            DecimalFormat thousandformat = new DecimalFormat("###,###,###,##0");
            String strPriceTempVal = "";
            if (rect == graph.turnOverRect) {
                strPriceTempVal = thousandformat.format(priceYVal);
            } else if (rect == graph.volumeRect) {
                strPriceTempVal = thousandformat.format(priceYVal);
            } else {
                strPriceTempVal = graph.GDMgr.formatPriceField(priceYVal, ((WindowPanel) rect).isInThousands());
            }

            dataRows.add(new ToolTipDataRow(lblDate, beginDate));
            dataRows.add(new ToolTipDataRow(Language.getString("VALUE"), strPriceTempVal));
        }
        return isOnTheLine;
    }


    // overidden to handle rotating points. 
    protected boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select, boolean isMove, int ctrX1, int ctrY1, int ctrX2, int ctrY2) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);

        //System.out.println(x1 + "  "+ y1 + "  "+ x2 +"  "+  y2 + "  ctr=> "+ ctrX1 + "  "+ ctrY1 + "  "+ ctrX2 +"  "+  ctrY2 );

        if (!isMove) {
            return false;
        }
        if (((ctrX1 - x) * (ctrX1 - x) <= halfSquared) && ((ctrY1 - y) * (ctrY1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = 0;
            if (movingPtIndex == 0) {
                //System.out.println("wrong");
            }
            objectMoving = (objType == INT_SYMBOL);
            //objectMoving = false;
            return true;
        } else if (((ctrX2 - x) * (ctrX2 - x) <= halfSquared) && ((ctrY2 - y) * (ctrY2 - y) <= halfSquared)) {
            onFirstPoint = false;
            if (!selected)
                selected = select;
            movingPtIndex = 1;
            objectMoving = (objType == INT_SYMBOL);
            //objectMoving = false;
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = 0;      //TODO :CHARITH-nned to finalize
            if (movingPtIndex == 0) {
                //System.out.println("wrong");
            }
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;


        //---------------------------

//        System.out.println(x1 + "  "+ y1 + "  "+ x2 +"  "+  y2);
//
//        if (!isMove) {
//            return false;
//        }
//        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
//            onFirstPoint = true;
//            if (!selected)
//                selected = select;
//            movingPtIndex = 0;
//            if (movingPtIndex == 0) {
//                //System.out.println("wrong");
//            }
//            //objectMoving = (objType == INT_SYMBOL);
//            objectMoving = false;
//            return true;
//        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
//            onFirstPoint = false;
//            if (!selected)
//                selected = select;
//            movingPtIndex = 1;
//            //objectMoving = (objType == INT_SYMBOL);
//            objectMoving = false;
//            return true;
//        } else if ((x1 == x2) && (y1 == y2)) {
//            return false;
//        } else if (L2D.ptLineDist(x, y) <= 2.0) {
//            movingPtIndex = 0;      //TODO :CHARITH-nned to finalize
//            if (movingPtIndex == 0) {
//                //System.out.println("wrong");
//            }
//            if (x1 != x2) {
//                if ((x1 - x) * (x2 - x) < 0) {
//                    if (!selected)
//                        selected = select;
//                    objectMoving = true;
//                    return true;
//                }
//            } else if ((y1 - y) * (y2 - y) < 0) {
//                if (!selected)
//                    selected = select;
//                objectMoving = true;
//                return true;
//            }
//        }
//        return false;
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        Date D1, D2 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }
        /* return timeFormatter.format(D1) + ", " +
      GraphDataManager.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands()) + ", " +
      timeFormatter.format(D2) + ", " +
      GraphDataManager.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());*/
        return "";
    }

    public String getShortName() {
        return Language.getString("TRND_LINE");
    }

    public boolean isExtendedLeft() {
        return extendedLeft;
    }

    public void setExtendedLeft(boolean extendedLeft) {
        this.extendedLeft = extendedLeft;
    }

    public boolean isExtendedRight() {
        return extendedRight;
    }

    public void setExtendedRight(boolean extendedRight) {
        this.extendedRight = extendedRight;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean isAlarmEnabled) {
        this.alarmEnabled = isAlarmEnabled;
    }

    public boolean isbarCount() {
        return barCount;
    }

    public boolean isnetChange() {
        return netChange;
    }

    public boolean ispctChange() {
        return pctChange;
    }

    public boolean istimeSpan() {
        return timeSpan;
    }

    public double getAngle() {
        return angle;
    }

    public void setbarCount(boolean barCount) {
        this.barCount = barCount;
    }

    public void setnetChange(boolean netChange) {
        this.netChange = netChange;
    }

    public void setpctChange(boolean pctChange) {
        this.pctChange = pctChange;
    }

    public void settimeSpan(boolean timeSpan) {
        this.timeSpan = timeSpan;
    }

    //Should implement the intersection logic here
    public boolean isIntesectingLine(Line2D priceLine) {

        ChartProperties cp = graph.GDMgr.getBaseCP();
        int[] yPixelArr2 = graph.GDMgr.convertYValueArrayToPixelArr(getYArr(), graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
        float[] xIndexArr2 = getIndexArray();
        int[] xPixelArr2 = {graph.GDMgr.getPixelFortheIndex(xIndexArr2[0]), graph.GDMgr.getPixelFortheIndex(xIndexArr2[1])};
        Line2D lineSlope = new Line2D.Double(new Point(xPixelArr2[0], yPixelArr2[0]), new Point(xPixelArr2[1], yPixelArr2[1]));
        return lineSlope.intersectsLine(priceLine);

    }

    public String getAlarmMessage() {
        return alarmMesage;
    }

    public void setAlarmMessage(String msg) {
        this.alarmMesage = msg;
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

    public void setAngle(boolean angle) {
        this.isAngle = angle;
    }

    public boolean isAngle() {
        return isAngle;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void saveToLayout(org.w3c.dom.Element xmlobj, org.w3c.dom.Document document) {
        super.saveToLayout(xmlobj, document);
        TemplateFactory.saveProperty(xmlobj, document, "AlarmEnabled", Boolean.toString(this.alarmEnabled));
        TemplateFactory.saveProperty(xmlobj, document, "AlarmMessage", this.alarmMesage);
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadFromLayout(xpath, document, preExpression);
        this.alarmEnabled = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmEnabled"));
        this.alarmMesage = TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmMessage");
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        double y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        double x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        double y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        double x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        boolean inside = false;
        double xMin = Math.min(x1, x2);
        double xMax = Math.max(x1, x2);

        //if line line points doesnot contains inside the rectangle
        for (double i = xMin; i < xMax; i = i + 1) {
            double y = m * i + c;
            if (r.contains(i, y)) {
                inside = true;
                break;
            }
        }
        if (!inside) {
            return false;
        }

        int x0 = r.x;
        int y0 = r.y;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //m is negative and intersects the rectangle
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }
        return false;

    }
}
