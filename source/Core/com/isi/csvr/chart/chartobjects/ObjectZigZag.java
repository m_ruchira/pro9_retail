package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Feb 10, 2009
 * Time: 10:29:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectZigZag extends AbstractObject implements Alarmable {

    private static final long serialVersionUID = AbstractObject.UID_ZIG_ZAG;

    private String alarmMesage = null;
    private boolean alarmEnabled = false;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();

    public ObjectZigZag(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_ZIG_ZAG;
        alarmMesage = Language.getString("TREND_LINE_POPUP_MSG");
        isShowingDragToolTip = false;
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }

    }

    protected void drawOnGraphics(Graphics g, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {

        Graphics2D gg = (Graphics2D) g;
        boolean drawSelected = selected && !isPrinting;

        gg.setColor(color);
        gg.setStroke(pen);
        gg.setClip(rect.x, rect.y, rect.width, rect.height);

        int x1, x2, y1, y2, x3, y3;

        int[] x = new int[xArr.length];
        int[] y = new int[yArray.length];

        for (int i = 0; i < xArr.length; i++) {
            x[i] = Math.round((xArr[i] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
            y[i] = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
        }


        //gg.setColor(color);
        g.drawPolyline(x, y, x.length);

        Graphics2D g2 = (Graphics2D) g;
        if (drawSelected) {

            for (int i = 0; i < xArr.length; i++) {
                x[i] = Math.round((xArr[i] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                y[i] = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates

                gg.setColor(color);
                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                g2.setStroke(selectPen);
                g2.fillRect(x[i] - halfBox, y[i] - halfBox, halfBox * 2, halfBox * 2);
                g2.fillRect(x[i] - halfBox, y[i] - halfBox, halfBox * 2, halfBox * 2);
                g2.setColor(Color.BLACK);
                g2.drawRect(x[i] - halfBox, y[i] - halfBox, halfBox * 2, halfBox * 2);
                g2.drawRect(x[i] - halfBox, y[i] - halfBox, halfBox * 2, halfBox * 2);
            }
        }

        this.getToolTipRows().clear();
    }

    protected boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select, int movingIndex) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = movingIndex;
            objectMoving = (objType == INT_SYMBOL);
            //System.out.println("++++++++++++++++ moving Index-1 ++++++++++++++ " + movingPtIndex);
            return true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
            onFirstPoint = false;     //TODO:  holding from the moving point
            if (!selected)
                selected = select;
            //movingPtIndex = 1;
            movingPtIndex = movingIndex + 1;
            objectMoving = (objType == INT_SYMBOL);
            //System.out.println("++++++++++++++++ moving Index-2 ++++++++++++++ " + movingPtIndex);
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = movingIndex;
            //System.out.println("++++++++++++++++ moving Index-3 ++++++++++++++ " + movingPtIndex);
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectZigZag) {
            this.alarmEnabled = ((ObjectZigZag) ao).alarmEnabled;
            this.alarmMesage = ((ObjectZigZag) ao).alarmMesage;
        }
    }

    public boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        int x1, x2, y1, y2;

        if (!rect.contains(x, y) || !isMove) return false;
        if (xArray.length <= 1) {
            return false;
        }
        for (int i = 0; i < xArray.length; i++) {

            x1 = Math.round((xArray[i] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[i], rect, minY, yFactor, grf_Ht + grf_Top);

            if (i == xArray.length - 1) {
                x2 = Math.round((xArray[i - 1] - bIndex) * xFactor) + grfLeft;
                y2 = getPixelFortheYValue(yArray[i - 1], rect, minY, yFactor, grf_Ht + grf_Top);
            } else {
                x2 = Math.round((xArray[i + 1] - bIndex) * xFactor) + grfLeft;
                y2 = getPixelFortheYValue(yArray[i + 1], rect, minY, yFactor, grf_Ht + grf_Top);
            }

            int tempX1 = x1;
            int tempX2 = x2;
            int tempY1 = y1;
            int tempY2 = y2;

            if (isOnTheLine(tempX1, tempY1, tempX2, tempY2, x, y, select, i)) {
                return true;
            }

            /*float xx1 = xArray[i];
            float xx2;
            if (i < xArray.length - 1) {
                xx2 = xArray[i + 1];
                if (xx1 < xx2) {
                    x1 = Math.round((xArray[i] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                    x2 = Math.round((xArray[i + 1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                    y1 = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                    y2 = getPixelFortheYValue(yArray[i + 1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                } else {
                    x1 = Math.round((xArray[i + 1] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                    x2 = Math.round((xArray[i] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                    y1 = getPixelFortheYValue(yArray[i + 1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                    y2 = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                }
            } else {
                xx2 = xArray[i - 1];
                if (xx2 < xx1) {
                    x1 = Math.round((xArray[i] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                    x2 = Math.round((xArray[i - 1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                    y1 = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                    y2 = getPixelFortheYValue(yArray[i - 1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                } else {
                    x1 = Math.round((xArray[i - 1] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                    x2 = Math.round((xArray[i] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates

                    y1 = getPixelFortheYValue(yArray[i - 1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                    y2 = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
                }

            }

            if (isOnTheLine(x1, y1, x2, y2, x, y, select, i)) {
                //System.out.println("********** charith ********* " + i);
                return true;
            }*/
        }

        //Added in order to select the extension - Pramoda
        return false;
    }

    public void drawDragImage(Graphics g, float[] xArr,
                              int Xadj, double Yadj, int grfLeft, int grfWidth,
                              float bIndex, int grf_Top, int grf_Ht,
                              double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        Graphics2D gg = (Graphics2D) g;

        int[] x = new int[xArr.length];
        int[] y = new int[xArr.length];

        for (int i = 0; i < xArr.length; i++) {
            if (objectMoving) {
                x[i] = Math.round((xArr[i] - bIndex) * xFactor) + grfLeft - Xadj; // x1 in pixel coordinates
                y[i] = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)) - (int) Yadj; //y1 in pixel coordinates
            } else {

                int index = getMovingPtIndex();
                if (i == index) {
                    x[i] = Math.round((xArr[i] - bIndex) * xFactor) + grfLeft - Xadj; // x1 in pixel coordinates
                    y[i] = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)) - (int) Yadj; //y1 in pixel coordinates
                } else {
                    x[i] = Math.round((xArr[i] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
                    y[i] = getPixelFortheYValue(yArray[i], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
                }
            }
        }

        g.drawPolyline(x, y, x.length);

        this.getToolTipRows().clear();
    }

    public void saveToLayout(org.w3c.dom.Element xmlobj, org.w3c.dom.Document document) {
        super.saveToLayout(xmlobj, document);
        TemplateFactory.saveProperty(xmlobj, document, "AlarmEnabled", Boolean.toString(this.alarmEnabled));
        TemplateFactory.saveProperty(xmlobj, document, "AlarmMessage", this.alarmMesage);
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadFromLayout(xpath, document, preExpression);
        this.alarmEnabled = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmEnabled"));
        this.alarmMesage = TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmMessage");
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_ZIG_ZAG");
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmEnabled(boolean enabled) {
        this.alarmEnabled = enabled;
    }

    public String getAlarmMessage() {
        return alarmMesage;
    }

    public void setAlarmMessage(String msg) {
        this.alarmMesage = msg;
    }

    public boolean isIntesectingLine(Line2D priceLine) {

        ChartProperties cp = graph.GDMgr.getBaseCP();
        for (int i = 0; i < getYArr().length - 1; i++) {
            int[] yPixelArr = graph.GDMgr.convertYValueArrayToPixelArr(getYArr(), graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
            float[] xIndexArr = getIndexArray();
            int[] xPixelArr = {graph.GDMgr.getPixelFortheIndex(xIndexArr[i]), graph.GDMgr.getPixelFortheIndex(xIndexArr[i + 1])};
            Line2D lineSlope = new Line2D.Double(new Point(xPixelArr[0], yPixelArr[i]), new Point(xPixelArr[1], yPixelArr[i + 1]));
            if (lineSlope.intersectsLine(priceLine)) {
                return true;
            }
        }
        return false;
    }
}
