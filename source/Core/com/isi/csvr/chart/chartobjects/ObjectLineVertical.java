package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:00:54 PM
 */
public class ObjectLineVertical extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_LINE_VERTI;
    //SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
    SimpleDateFormat timeFormatter = new SimpleDateFormat("dd/MM/yy - HH:mm");

    private ArrayList panels;

    public ObjectLineVertical() {
        super();
        objType = AbstractObject.INT_LINE_VERTI;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectLineVertical(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, ArrayList panels, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_LINE_VERTI;
        this.panels = panels;
        showOnAllPanels = true;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        isShowingDragToolTip = false;

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    // extending LineVertical independently to draw on all windows ################################################

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            g.drawLine(x1 - Xadj, rect.y, x1 - Xadj, rect.y + rect.height);
        }

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setFont(new Font("Arial", Font.BOLD, 10));
        boolean drawSelected = selected && !isPrinting;
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);

            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            g.setClip(rect.x, rect.y, rect.width, rect.height);
            g.drawLine(x1, rect.y, x1, rect.y + rect.height);

            String yValue = "";

            if (graph.isCurrentMode()) {
                timeFormatter = new SimpleDateFormat("dd/MM/yy - HH:mm");
            } else {
                timeFormatter = new SimpleDateFormat("dd/MM/yy");
            }

            yValue = timeFormatter.format(new Date(xArray[0]));

            /* if (yValue.trim().endsWith("00:00")) {  //if the time is in hostory mode - ex: 2008-10-23-00:00
                int index = yValue.lastIndexOf("-");
                yValue = yValue.substring(0, index);
            }*/

            //drawing the date string in vertical direction
            if (i == 0) { // date should draw only for the top rectangle.
                int y1 = rect.y;
                g.translate(x1, y1);
                g.rotate(Math.toRadians(90));
                g.drawString(yValue, 20, y1 - 20);
                g.rotate(Math.toRadians(270));
                g.translate(-x1, -y1);
            }
            if (drawSelected) {
                g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                g.fillRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                g.drawRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
            }
        }

    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel rect = (WindowPanel) panels.get(i);
            grfLeft = rect.x + StockGraph.clearance;
            int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
            if (((x - x1) * (x - x1) < 4) && (rect.contains(x, y))) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                movingPtIndex = 0;
                this.rect = rect;
                return true;
            }
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("VERTICAL_LINE");
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
//            timeFormatter = new SimpleDateFormat("dd/MM/yy");
            timeFormatter = Language.isLTR() ? new SimpleDateFormat("dd/MM/yy") : new SimpleDateFormat("yy/MM/dd");
        }
        Date D1;
        D1 = new Date(xArray[0]);
        return timeFormatter.format(D1);
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        for (int i = 0; i < xArray.length; i++) {
            //int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, ao.getRect());
            //double y0 = graph.GDMgr.getPixelFortheYValue(yarr[i], pnlID);

            float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[i]});
            double x0 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            int x1 = r.x;
            int x2 = r.x + r.width;

            //System.out.println(" === x1 vertical ==== : " + x1);
            //System.out.println(" === x2 vertical ==== : " + x2);

            //System.out.println(" === x0 vertical ==== : " + x0);
            if (x0 > x1 && x0 < x2) {
                return true;
            }
        }
        return false;
    }

    // ############################################################################################################
}
