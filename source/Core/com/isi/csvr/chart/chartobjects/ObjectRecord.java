/*
 * Classname:     ObjectRecord.java
 * Written By:    Udaka Liyanapathirana
 * Version:       Chart 1.0_beta
 * Date:          21st August 2002
 * Copyright:     (c) 2002 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.io.Serializable;
import java.text.DecimalFormat;

public class ObjectRecord implements Serializable {
    public final static int INT_NONE = 0;
    public final static int INT_LINE_SLOPE = 1;
    public final static int INT_LINE_HORIZ = 2;
    public final static int INT_LINE_VERTI = 3;
    public final static int INT_SYMBOL = 4;
    public final static int INT_RECT = 5;
    public final static int INT_TEXT = 6;
    public final static int INT_ELLIPSE = 7;
    public final static int INT_POLYGON = 8;
    public final static int INT_REGRESSION = 9;
    public final static int INT_FIBONACCI_ARCS = 10;
    public final static int INT_FIBONACCI_FANS = 11;
    public final static int INT_FIBONACCI_RETRACEMENTS = 12;
    public final static int INT_FIBONACCI_ZONES = 13;
    public final static int INT_STD_ERROR_CHANNEL = 14;
    public final static int ALIGNMENT_TOPLEFT = 1;
    public final static int ALIGNMENT_TOP = 2;
    public final static int ALIGNMENT_TOPRIGHT = 3;
    public final static int ALIGNMENT_LEFT = 4;
    public final static int ALIGNMENT_CENTER = 5;
    public final static int ALIGNMENT_RIGHT = 6;
    public final static int ALIGNMENT_BOTTOMLEFT = 7;
    public final static int ALIGNMENT_BOTTOM = 8;
    public final static int ALIGNMENT_BOTTOMRIGHT = 9;
    private static final long serialVersionUID = 1072600473330203802L;
    public static Color selectedColor = Color.BLUE;//new Color(50, 200, 50);
    public static Color lineColor = new Color(20, 50, 20);
    public static BasicStroke selectPen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
    public static BasicStroke dottedPen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_DASH, 0.5f);
    static DecimalFormat percentformat = new DecimalFormat("##0.0%");
    public int pnlID;
    int halfBox = 3;
    int halfSquared = halfBox * halfBox;
    long[] xArray = null;
    float[] yArray = null;
    int movingPtIndex = 0;
    //private boolean onPriceGraph;
    private boolean onFirstPoint;
    private boolean selected;
    private int objType;
    private byte OHLCPriority = GraphDataManager.INNER_Close;
    transient private ChartProperties target = null;
    transient private Rectangle rect = null;
    private boolean hidden;
    private Color color;
    private float penWidth;
    private byte penStyle;
    transient private BasicStroke pen;
    private String text;
    private Font font;
    private boolean transparent;
    private Color fillColor;
    private Color fontColor;
    private boolean objectMoving;
    private boolean extendedLeft;
    private boolean extendedRight;
    private int channelUnits; // Pramoda (2006-03-02)

    public ObjectRecord() {
        //onPriceGraph = true;
        selected = false;
        objType = INT_LINE_SLOPE;
        hidden = false;
        OHLCPriority = GraphDataManager.INNER_Close;
        color = lineColor;
        penWidth = 1f;
        penStyle = GraphDataManager.STYLE_SOLID;
        pen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
        text = null;
        font = Theme.getDefaultFont(Font.PLAIN, 9);
        transparent = true;
        fillColor = new Color(240, 240, 230);
        fontColor = StockGraph.fontColor;
        extendedLeft = false;
        extendedRight = false;
        channelUnits = 2; // Pramoda (2006-03-02)
    }

    public ObjectRecord(long[] xArr, float[] yArr, ChartProperties cp, Rectangle r) {
        this();
        xArray = xArr;
        yArray = yArr;
        target = cp;
        rect = r;
    }

    public static boolean isMouseDownHasHighPriority(int anObjType) {
        switch (anObjType) {
            case INT_LINE_SLOPE:
            case INT_RECT:
            case INT_TEXT:
            case INT_POLYGON:
            case INT_ELLIPSE:
            case INT_FIBONACCI_ARCS:
            case INT_FIBONACCI_FANS:
            case INT_FIBONACCI_RETRACEMENTS:
                //case INT_FIBONACCI_ZONES:  not this
                return true;
        }
        return false;
    }

    public static Point getAlignedPosition(int alignment, int w, int h,
                                           int strW, int strH, int ascent) {
        Point pt = new Point();
        switch (alignment) {
            case ALIGNMENT_CENTER:
                pt.x = (w - strW) / 2;
                pt.y = (h - strH) / 2 + ascent;
                //System.out.println("strW "+strW+" strH "+strH+" ascent "+ascent+" pt.x "+pt.x+" pt.y "+pt.y);
                break;
        }
        return pt;
    }

    public void assignValuesFrom(ObjectRecord or) {
        xArray = or.xArray;
        yArray = or.yArray;

        onFirstPoint = or.onFirstPoint;
        selected = or.selected;
        objType = or.objType;
        OHLCPriority = or.OHLCPriority;
        target = or.target;
        rect = or.rect;
        pnlID = or.pnlID;

        hidden = or.hidden;
        color = or.color;
        penWidth = or.penWidth;
        penStyle = or.penStyle;
        pen = or.pen;
        text = or.text;
        font = or.font;
        transparent = or.transparent;
        fontColor = or.fontColor;
        fillColor = or.fillColor;
        extendedLeft = or.extendedLeft;
        extendedRight = or.extendedRight;
        channelUnits = or.channelUnits; // Pramoda - (2006-03-02)
    }

    public void drawDragImage(Graphics g, float[] xArr,
                              int Xadj, int Yadj, float bIndex,
                              float minY, float xFactor, float yFactor) {

        drawDragImage(g, xArr, Xadj, Yadj, rect.x + StockGraph.clearance,
                rect.width - 2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                        StockGraph.clearance, rect.height - StockGraph.titleHeight -
                        2 * StockGraph.clearance, minY, xFactor, yFactor);
    }

//	public Point2D.Float getFixedPoint(){
//		return new Point2D.Float(fixedPoint.x, fixedPoint.y);
//	}
//	public Point2D.Float getMovingPoint(){
//		return new Point2D.Float(movingPoint.x, movingPoint.y);
//	}
//	public void setFixedPoint(float x, float y){
//		fixedPoint.setLocation(x, y);
//	}
//	public void setMovingPoint(float x, float y){
//		movingPoint.setLocation(x, y);
//	}

    /*
    xArr here is in pixels (instead of in time like in xArray)
    yArr is not passed instead, calculated from yArray
    */
    private void drawDragImage(Graphics g, float[] xArr,
                               int Xadj, int Yadj, int grfLeft, int grfWidth,
                               float bIndex, int grf_Top, int grf_Ht,
                               float minY, float xFactor, float yFactor) {
        int x1, x2, y1, y2, x, y;
        try {
            switch (objType) {
                case INT_FIBONACCI_ARCS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - Yadj;
                        y2 = y2 - Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    float len;
                    int r1, r2, r3, start;
                    len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                    r1 = Math.round(len * 0.618f);
                    r2 = Math.round(len * 0.5f);
                    r3 = Math.round(len * 0.382f);
                    int xP = x2;
                    int yP = y2;
                    start = (y2 > y1) ? 0 : 180;
                    g.drawArc(xP - r1, yP - r1, 2 * r1, 2 * r1, start, 180);
                    g.drawArc(xP - r2, yP - r2, 2 * r2, 2 * r2, start, 180);
                    g.drawArc(xP - r3, yP - r3, 2 * r3, 2 * r3, start, 180);
                    break;
                case INT_FIBONACCI_FANS:
                    float ratio;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - Yadj;
                        y2 = y2 - Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    if ((x1 == x2)) {
                        x = x1;
                        y = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / ((float) x2 - x1);
                        if (x1 > x2) {
                            x1 = x2;
                            y1 = y2;
                        }
                        x = x1 + grfWidth;
                        y = y1 + Math.round(ratio * grfWidth);
                    }
                    int yP1, yP2, yP3;
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    g.drawLine(x1, y1, x, yP1);
                    g.drawLine(x1, y1, x, yP2);
                    g.drawLine(x1, y1, x, yP3);
                    break;
                case INT_FIBONACCI_RETRACEMENTS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - Yadj;
                        y2 = y2 - Yadj;
                    } else {
                        if (movingPtIndex == 0) {
                            x1 = x1 - Xadj;
                            y1 = y1 - Yadj;
                        } else {
                            x2 = x2 - Xadj;
                            y2 = y2 - Yadj;
                        }
                    }
                    g.drawLine(x1, y1, x2, y2);
                    if ((x1 != x2)) {
                        if (x1 > x2) {
                            x1 = x2;
                            x2 = y1; //used as a tmp var
                            y1 = y2;
                            y2 = x2;
                        }
                        len = (y2 - y1);
                        float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f};
                        for (int i = 0; i < fibArr.length; i++) {
                            y = y2 - Math.round(fibArr[i] * len);
                            g.drawLine(x1, y, x1 + grfWidth, y);
                        }
                        g.drawLine(x1, y1, x1 + grfWidth, y1);
                        g.drawLine(x1, y2, x1 + grfWidth, y2);
                    }
                    break;
                case INT_FIBONACCI_ZONES:
                    y1 = grf_Top;
                    y2 = grf_Top + grf_Ht;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft - Xadj;
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    g.drawLine(x1, y1, x1, y2);
                    while (true) {
                        sum += curVal;
                        //System.out.println("���� writing fibonaaci value "+sum);
                        x = x1 + Math.round(sum * xFactor);
                        if (x > grfLeft + grfWidth) break;
                        g.drawLine(x, y1, x, y2);
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    break;
                case INT_LINE_SLOPE:
                    if (objectMoving) {
                        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                        y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - Yadj;
                        y2 = y2 - Yadj;
                    } else {
                        x1 = Math.round((xArr[movingPtIndex] - bIndex) * xFactor) + grfLeft - Xadj;
                        x2 = Math.round((xArr[1 - movingPtIndex] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[movingPtIndex] - minY) * yFactor) + grf_Top - Yadj;
                        y2 = grf_Ht - Math.round((yArray[1 - movingPtIndex] - minY) * yFactor) + grf_Top;
                    }
                    g.drawLine(x1, y1, x2, y2);
                    break;
                case INT_LINE_HORIZ:
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    g.drawLine(rect.x, y1 - Yadj, rect.x + rect.width, y1 - Yadj);
                    break;
                case INT_LINE_VERTI:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    g.drawLine(x1 - Xadj, rect.y, x1 - Xadj, rect.y + rect.height);
                    break;
                case INT_SYMBOL:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    x1 = x1 - Xadj;
                    y1 = y1 - Yadj;
                    g.drawRect(x1 - 10, y1 - 10, 20, 20);
                    break;
                case INT_TEXT:
                case INT_RECT:
                case INT_ELLIPSE:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (objectMoving) {
                        x1 = x1 - Xadj;
                        x2 = x2 - Xadj;
                        y1 = y1 - Yadj;
                        y2 = y2 - Yadj;
                    } else {
                        switch (movingPtIndex) {
                            case 0:
                                x1 = x1 - Xadj;
                                y1 = y1 - Yadj;
                                break;
                            case 1:
                                x2 = x2 - Xadj;
                                y2 = y2 - Yadj;
                                break;
                            case 2:
                                x1 = x1 - Xadj;
                                y2 = y2 - Yadj;
                                break;
                            case 3:
                                x2 = x2 - Xadj;
                                y1 = y1 - Yadj;
                                break;
                        }
                    }
                    if (objType == INT_ELLIPSE) {
                        g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    } else {
                        g.drawRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    }
                    break;
                case INT_REGRESSION:
                case INT_STD_ERROR_CHANNEL:
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
        }
    }

    public int getMovingPtIndex() {
        return movingPtIndex;
    }

    private void setMovingPtIndex(int index) {
        movingPtIndex = index;
    }

    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, float bIndex, float minY,
                               float xFactor, float yFactor, float eIndex) {
        drawOnGraphics(gg, xArr, isPrinting, rect.x + StockGraph.clearance, rect.width -
                2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                StockGraph.clearance, rect.height - StockGraph.titleHeight -
                2 * StockGraph.clearance, minY, xFactor, yFactor, eIndex);
    }

    private void drawOnGraphics(Graphics gg, float[] xArr,
                                boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                int grf_Top, int grf_Ht, float minY, float xFactor, float yFactor, float eIndex) {
        int x1, x2, y1, y2;
        float len;
        int r1, r2, r3, start;
        int extendX = 1000000;
        Graphics2D g = (Graphics2D) gg;
        boolean drawSelected = selected && !isPrinting;
        int x = 0, y = 0;
        //if (drawSelected) g.setColor(selectedColor);
        //else
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        try {
            switch (objType) {
                case INT_REGRESSION:
                case INT_LINE_SLOPE:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;

                    //TODO: this doesn't seem to extend to infinity. at some point the extended line stops. Have to fix that
                    //TODO: this can be visible when zoomed in using the slider
                    // Added for extensions - Pramoda
                    int tempX1 = x1;
                    int tempX2 = x2;
                    int tempY1 = y1;
                    int tempY2 = y2;
                    if (extendedLeft) {
                        float ratio = 1f;
                        if ((x1 == x2)) {
                            tempY1 = grf_Top - grf_Ht;
                        } else {
                            ratio = (float) (y2 - y1) / (float) (x2 - x1);
                            tempX1 = x1 - extendX;
                            tempY1 = y1 - Math.round(ratio * extendX);
                        }
                    }
                    if (extendedRight) {
                        float ratio = 1f;
                        if ((x1 == x2)) {
                            tempY2 = grf_Top - grf_Ht;
                        } else {
                            ratio = (float) (y2 - y1) / (float) (x2 - x1);
                            tempX2 = x1 + extendX;
                            tempY2 = y1 + Math.round(ratio * extendX);
                        }
                    }
                    g.drawLine(tempX1, tempY1, tempX2, tempY2);

                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_LINE_HORIZ:
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    g.drawLine(rect.x, y1, rect.x + rect.width, y1);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(rect.x + rect.width / 2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(rect.x + rect.width / 2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_LINE_VERTI:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    g.drawLine(x1, rect.y, x1, rect.y + rect.height);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, rect.y + rect.height / 2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_SYMBOL:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    g.setColor(fontColor);
                    g.setFont(font);
                    FontMetrics fmx = g.getFontMetrics();
                    Point pt = getAlignedPosition(ALIGNMENT_CENTER, 20, 20,
                            fmx.stringWidth(text), fmx.getHeight(), fmx.getAscent());
                    g.drawString(text, x1 - 10 + pt.x, y1 - 10 + pt.y);
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 + 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 + 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 + 10 - halfBox, y1 - 10 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 + 10 - halfBox, y1 + 10 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_TEXT:
                case INT_RECT:
                    int[] xPBorder = null;
                    int[] yPBorder = null;
                    Polygon poly = null;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (objType == INT_RECT) {
                        g.setColor(color);
                        g.drawRect(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    } else {
                        int maxEX = Math.max(x1, x2);
                        int maxWY = Math.max(y1, y2);
                        int minEX = Math.min(x1, x2);
                        int minWY = Math.min(y1, y2);
                        int midX = Math.max(maxEX - 10, (x1 + x2) / 2);
                        int midY = Math.min(minWY + 10, (y1 + y2) / 2);
                        int[] xP = {minEX, minEX, maxEX, maxEX, midX};
                        int[] yP = {minWY, maxWY, maxWY, midY, minWY};
                        int[] xPB = {midX, midX, maxEX};
                        int[] yPB = {minWY, midY, midY};
                        xPBorder = xPB;
                        yPBorder = yPB;
                        poly = new Polygon(xP, yP, 5);
                        //g.setClip(poly);
                        if (!transparent) {
                            g.setColor(fillColor);
                            g.fillPolygon(xP, yP, 5);
                        }
                        g.setColor(color);
                        g.drawPolygon(xP, yP, 5);
                        //g.setClip(rect);
                    }
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    if (objType == INT_RECT) break;
                    Rectangle rectClip = new Rectangle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    //rectClip = rect.intersection(rectClip);
                    if (rect.contains(rectClip)) {
                        g.setClip(poly);
                    } else {
                        g.setClip(rect.intersection(rectClip));
                    }
                    String[] sa = extractLines(text);
                    g.setColor(fontColor);
                    g.setFont(font);

                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    for (int i = 0; i < sa.length; i++) {
                        g.drawString(sa[i], Math.min(x1, x2) + 4, Math.min(y1, y2) + 15 * (i + 1) - 3);
                    }
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                    if (!transparent) {
                        g.setColor(fillColor);
                        g.fillPolygon(xPBorder, yPBorder, 3);
                    }
                    g.setColor(color);
                    g.drawPolygon(xPBorder, yPBorder, 3);
                    break;
                case INT_ELLIPSE:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    g.drawOval(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x1 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_FIBONACCI_ARCS:
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                    r1 = Math.round(len * 0.618f);
                    r2 = Math.round(len * 0.5f);
                    r3 = Math.round(len * 0.382f);
                    start = (y2 > y1) ? 0 : 180;
                    g.drawArc(x2 - r1, y2 - r1, 2 * r1, 2 * r1, start, 180);
                    g.drawArc(x2 - r2, y2 - r2, 2 * r2, 2 * r2, start, 180);
                    g.drawArc(x2 - r3, y2 - r3, 2 * r3, 2 * r3, start, 180);
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

                    }
                    break;
                case INT_FIBONACCI_FANS:
                    float ratio = 1f;
                    if (xArr[0] < xArr[1]) {
                        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                        y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    } else {
                        x1 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                        y2 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    }
                    if ((x1 == x2)) {
                        x = x1;
                        y = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / ((float) x2 - x1);
                        x = x1 + extendX;
                        y = y1 + Math.round(ratio * extendX);
                    }
                    int yP1, yP2, yP3;
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    g.drawLine(x1, y1, x, yP1);
                    g.drawLine(x1, y1, x, yP2);
                    g.drawLine(x1, y1, x, yP3);
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_FIBONACCI_RETRACEMENTS:
                    if (xArr[0] < xArr[1]) {
                        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                        y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    } else {
                        x1 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                        x2 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                        y1 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                        y2 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    }
                    len = (y2 - y1);
                    if (len != 0) {
                        float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f}; // added 0.764f - pramoda
                        g.setStroke(dottedPen);
                        g.setFont(font);
                        FontMetrics fm = g.getFontMetrics(font);
                        int strHt = fm.getHeight() / 3;
                        for (int i = 0; i < fibArr.length; i++) {
                            y = y2 - Math.round(fibArr[i] * len);
                            g.drawLine(x1, y, rect.width, y);
                            String s = percentformat.format(fibArr[i]);
                            int strWd = fm.stringWidth(s);
                            g.drawString(s, x1 - strWd - halfBox, y + strHt);
                        }
                        g.setStroke(pen);
                        g.drawLine(x1, y1, rect.width, y1);
                        g.drawLine(x1, y2, rect.width, y2);
                        String s = "0.0 %";
                        int strWd = fm.stringWidth(s);
                        g.drawString(s, x1 - strWd - halfBox, y2 + strHt);
                        s = "100.0 %";
                        strWd = fm.stringWidth(s);
                        g.drawString(s, x1 - strWd - halfBox, y1 + strHt);
                    }
                    if (drawSelected) {
                        g.drawLine(x1, y1, x2, y2);
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                case INT_FIBONACCI_ZONES:
                    y1 = grf_Top;
                    y2 = grf_Top + grf_Ht;
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    g.setStroke(dottedPen);
                    g.drawLine(x1, y1, x1, y2);
                    while (true) {
                        sum += curVal;
                        //System.out.println("���� writing fibonaaci value "+sum);
                        x = Math.round((xArr[0] + sum - bIndex) * xFactor) + grfLeft;
                        //if (x>grfLeft+grfWidth) break;
                        if (sum > eIndex) break;
                        g.setColor(color);
                        g.setStroke(pen);
                        g.drawLine(x, y1, x, y2);
                        if (drawSelected) {
                            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                            g.setStroke(selectPen);
                            g.fillRect(x - halfBox, (y1 + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                            g.setColor(Color.BLACK);
                            g.drawRect(x - halfBox, (y1 + y2) / 2 - halfBox, halfBox * 2, halfBox * 2);
                        }
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    break;
                case INT_STD_ERROR_CHANNEL: // Added - Pramoda (2006-03-01)
                    x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    int y3 = grf_Ht - Math.round((yArray[2] - minY) * yFactor) + grf_Top;
                    int y4 = grf_Ht - Math.round((yArray[3] - minY) * yFactor) + grf_Top;
                    int y5 = grf_Ht - Math.round((yArray[4] - minY) * yFactor) + grf_Top;
                    int y6 = grf_Ht - Math.round((yArray[5] - minY) * yFactor) + grf_Top;

                    //TODO: this doesn't seem to extend to infinity. at some point the extended line stops. Have to fix that
                    //TODO: this can be visible when zoomed in using the slider
                    tempX1 = x1;
                    tempX2 = x2;
                    tempY1 = y1;
                    tempY2 = y2;
                    int tempY3 = y3;
                    int tempY4 = y4;
                    int tempY5 = y5;
                    int tempY6 = y6;
                    if (extendedLeft) {
                        ratio = 1f;
                        if ((x1 == x2)) {
                            tempY1 = grf_Top - grf_Ht;
                            tempY3 = grf_Top - grf_Ht;
                            tempY5 = grf_Top - grf_Ht;
                        } else {
                            ratio = (float) (y2 - y1) / (float) (x2 - x1);
                            tempX1 = x1 - extendX;
                            tempY1 = y1 - Math.round(ratio * extendX);
                            tempY3 = y3 - Math.round(ratio * extendX); //ratio is same because the lines are parallel
                            tempY5 = y5 - Math.round(ratio * extendX);
                        }
                    }
                    if (extendedRight) {
                        ratio = 1f;
                        if ((x1 == x2)) {
                            tempY2 = grf_Top - grf_Ht;
                            tempY4 = grf_Top - grf_Ht;
                            tempY6 = grf_Top - grf_Ht;
                        } else {
                            ratio = (float) (y2 - y1) / (float) (x2 - x1);
                            tempX2 = x1 + extendX;
                            tempY2 = y1 + Math.round(ratio * extendX);
                            tempY4 = y3 + Math.round(ratio * extendX);  //ratio is same because the lines are parallel
                            tempY6 = y5 + Math.round(ratio * extendX);
                        }
                    }
                    g.drawLine(tempX1, tempY1, tempX2, tempY2);
                    g.drawLine(tempX1, tempY3, tempX2, tempY4);
                    g.drawLine(tempX1, tempY5, tempX2, tempY6);

                    if (drawSelected) {
                        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
                        g.setStroke(selectPen);
                        g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                        g.setColor(Color.BLACK);
                        g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
                        g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        g.setClip(null);
    }

    private String[] extractLines(String text) {
        String[] sa;
        try {
            sa = text.split("\n");
            return sa;
        } catch (Exception ex) {
            String[] sa1 = {text};
            return sa1;
        }
    }

    private boolean isOnFibonacciArc(int x2, int y2, float len, int start, int x, int y) {
        int r1, r2, r3;
        r1 = Math.round(len * 0.618f);
        r2 = Math.round(len * 0.5f);
        r3 = Math.round(len * 0.382f);
        boolean isInRightHalf = false;
        if (start == 0) {
            isInRightHalf = (x < x2 + r1) && (x > x2 - r1) && (y > y2 - r1) && (y < y2);
        } else {
            isInRightHalf = (x < x2 + r1) && (x > x2 - r1) && (y < y2 + r1) && (y > y2);
        }
        boolean result = isOnEllipse(r1, r1, x2, y2, x, y);
        result = result || isOnEllipse(r2, r2, x2, y2, x, y);
        result = result || isOnEllipse(r3, r3, x2, y2, x, y);
        result = result && isInRightHalf;
        if (result) objectMoving = true;
        return result;
    }

    private boolean isOnEllipse(int a, int b, int p, int q, int x, int y) {
        int gap = 2;
        boolean insideTheOuterEllipse = ((b + gap) * (b + gap) * (x - p) * (x - p) +
                (a + gap) * (a + gap) * (y - q) * (y - q) <=
                (a + gap) * (a + gap) * (b + gap) * (b + gap));
        boolean outsideTheInnerEllipse = (a < gap) || (b < gap) || ((b - gap) * (b - gap) * (x - p) * (x - p) +
                (a - gap) * (a - gap) * (y - q) * (y - q) >=
                (a - gap) * (a - gap) * (b - gap) * (b - gap));
        return insideTheOuterEllipse && outsideTheInnerEllipse;
    }

//	private void setFixedAndMovingPoints(int x1, int y1, int x2, int y2,
//										 int x3, int y3, int x4, int y4){
//		if (onFirstPoint){
//			fixedPoint.setLocation(x3, y3);
//			movingPoint.setLocation(x1, y1);
//		}else{
//			fixedPoint.setLocation(x4, y4);
//			movingPoint.setLocation(x2, y2);
//		}
//	}

    private boolean isOnTheLine(int x1, int y1, int x2, int y2, int x, int y, boolean select) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
            onFirstPoint = true;
            if (!selected)
                selected = select;
            movingPtIndex = 0;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
            onFirstPoint = false;
            if (!selected)
                selected = select;
            movingPtIndex = 1;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        } else if ((x1 == x2) && (y1 == y2)) {
            return false;
        } else if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = 0;
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    //this is same as isOnTheLine except it doesnt check for resizing
    private boolean isOnLineForMove(int x1, int y1, int x2, int y2, int x, int y, boolean select) {
        Line2D.Float L2D;
        L2D = new Line2D.Float(x1, y1, x2, y2);
        if (L2D.ptLineDist(x, y) <= 2.0) {
            movingPtIndex = 0;
            if (x1 != x2) {
                if ((x1 - x) * (x2 - x) < 0) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
            } else if ((y1 - y) * (y2 - y) < 0) {
                if (!selected)
                    selected = select;
                objectMoving = true;
                return true;
            }
        }
        return false;
    }

    public boolean isCursorOnObject(int x, int y, float[] xArray, boolean select,
                                    float bIndex, float minY, float xFactor, float yFactor) {
        return isCursorOnObject(x, y, xArray, select, rect.x + StockGraph.clearance,
                rect.width - 2 * StockGraph.clearance, bIndex, rect.y + StockGraph.titleHeight +
                        StockGraph.clearance, rect.height - 2 * StockGraph.clearance - StockGraph.titleHeight,
                minY, xFactor, yFactor);
    }

    private boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                     int grf_Top, int grf_Ht, float minY, float xFactor, float yFactor) {
        int x1, x2, y1, y2, x0, y0;
        boolean result = false;
        switch (objType) {
            case INT_REGRESSION:
            case INT_LINE_SLOPE:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;

                //TODO:
                //Added in order to select the extension - Pramoda
                int tempX1 = x1;
                int tempX2 = x2;
                int tempY1 = y1;
                int tempY2 = y2;
                if (extendedLeft) {
                    float ratio = 1f;
                    if ((x1 == x2)) {
                        tempY1 = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / (float) (x2 - x1);
                        tempX1 = x1 - rect.width;
                        tempY1 = y1 - Math.round(ratio * rect.width);
                    }
                }
                if (extendedRight) {
                    float ratio = 1f;
                    if ((x1 == x2)) {
                        tempY2 = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / (float) (x2 - x1);
                        tempX2 = x1 + rect.width;
                        tempY2 = y1 + Math.round(ratio * rect.width);
                    }
                }
                result = isOnTheLine(tempX1, tempY1, tempX2, tempY2, x, y, select);

//				if (result){
//					setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
//				}
                return result;
            case INT_LINE_HORIZ:
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                if (((y - y1) * (y - y1) < 4) && (rect.contains(x, y))) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    movingPtIndex = 0;
                    return true;
                }
                //g.drawLine(grfLeft, y1, grfLeft+grfWidth, y1);
                break;
            case INT_LINE_VERTI:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                //g.drawLine(x1, grf_Top, x1, grf_Top+grf_Ht);
                if (((x - x1) * (x - x1) < 4) && (rect.contains(x, y))) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    movingPtIndex = 0;
                    return true;
                }
                break;
            case INT_SYMBOL:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                Rectangle Rs = new Rectangle(x1 - 13, y1 - 13, 26, 26);
                if (Rs.contains(x, y)) {
                    movingPtIndex = 0;
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    return true;
                }
                break;
            case INT_RECT:
            case INT_TEXT:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                if (isCursorOnMoveHandles(x1, y1, x2, y2, x, y, false)) {
                    if (!selected)
                        selected = select;
                    objectMoving = false;
                    return true;
                }
                result = isOnTheLine(x1, y1, x1, y2, x, y, select);
//				if (result){
//					setFixedAndMovingPoints(x1, y1, x1, y2, x2, y2, x2, y1);
//				}
                if (result) return result;
                result = isOnTheLine(x2, y1, x2, y2, x, y, select);
//				if (result){
//					setFixedAndMovingPoints(x2, y1, x2, y2, x1, y2, x1, y1);
//				}
                if (result) return result;
                result = isOnTheLine(x1, y1, x2, y1, x, y, select) ||
                        isOnTheLine(x1, y2, x2, y2, x, y, select);
//				if (result){
//				    setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
//				}
                if (result) return result;
                if (objType == INT_TEXT) {
                    Rectangle R = new Rectangle(Math.min(x1, x2), Math.min(y1, y2), Math.abs(x2 - x1), Math.abs(y2 - y1));
                    if (R.contains(x, y)) {
//						setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
                        movingPtIndex = 0;
                        if (!selected)
                            selected = select;
                        objectMoving = true;
                        return true;
                    }
                }
                return result;
            case INT_ELLIPSE:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                if (isCursorOnMoveHandles(x1, y1, x2, y2, x, y, false)) {
                    if (!selected)
                        selected = select;
                    objectMoving = false;
                    return true;
                }
                float a, b, p, q;
                a = Math.abs(x1 - x2) / 2f;
                b = Math.abs(y1 - y2) / 2f;
                p = Math.abs(x1 + x2) / 2f;
                q = Math.abs(y1 + y2) / 2f;
                int gap = 2;
                boolean insideTheOuterEllipse = ((b + gap) * (b + gap) * (x - p) * (x - p) +
                        (a + gap) * (a + gap) * (y - q) * (y - q) <=
                        (a + gap) * (a + gap) * (b + gap) * (b + gap));
                boolean outsideTheInnerEllipse = (a < gap) || (b < gap) || ((b - gap) * (b - gap) * (x - p) * (x - p) +
                        (a - gap) * (a - gap) * (y - q) * (y - q) >=
                        (a - gap) * (a - gap) * (b - gap) * (b - gap));
                result = insideTheOuterEllipse && outsideTheInnerEllipse;
                if (result) {
                    if (!selected)
                        selected = select;
//					setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
                    movingPtIndex = 0;
                    objectMoving = true;
                }
                return result;
            case INT_FIBONACCI_ARCS:
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                if (isOnTheLine(x1, y1, x2, y2, x, y, select)) {
                    return true;
                }
                float len;
                int start;
                len = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
                start = (y2 > y1) ? 0 : 180;
                //result = isOnTheLine(x1, y1, x2, y2, x, y, select);   result||
                result = isOnFibonacciArc(x2, y2, len, start, x, y);
                if (result) {
                    if (!selected)
                        selected = select;
                    objectMoving = true;
                    //setFixedAndMovingPoints(x1, y1, x2, y2, x2, y2, x1, y1);
                }
                return result;
            case INT_FIBONACCI_FANS:
                float ratio = 1f;
                if (xArray[0] < xArray[1]) {
                    x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (isOnTheLine(x1, y1, x2, y2, x, y, select)) {
                        return true;
                    }
                } else {
                    x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    if (isOnTheLine(x2, y2, x1, y1, x, y, select)) {
                        return true;
                    }
                }
                if ((x1 == x2)) {
                    x0 = x1;
                    y0 = grf_Top - grf_Ht;
                } else {
                    ratio = (float) (y2 - y1) / ((float) x2 - x1);
                    x0 = x1 + grfWidth;
                    y0 = y1 + Math.round(ratio * grfWidth);
                }
                int yP1, yP2, yP3;
                yP1 = Math.round(0.618f * y1 + 0.382f * y0);
                yP2 = Math.round(0.5f * y1 + 0.5f * y0);
                yP3 = Math.round(0.382f * y1 + 0.618f * y0);
                if (isOnLineForMove(x1, y1, x0, yP1, x, y, select)) {
                    return true;
                }
                if (isOnLineForMove(x1, y1, x0, yP2, x, y, select)) {
                    return true;
                }
                if (isOnLineForMove(x1, y1, x0, yP3, x, y, select)) {
                    return true;
                }
                return false;
            case INT_FIBONACCI_RETRACEMENTS:
                if (xArray[0] < xArray[1]) {
                    x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    if (isOnTheLine(x1, y1, x2, y2, x, y, select)) {
                        return true;
                    }
                } else {
                    x1 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                    x2 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                    y1 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                    y2 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                    if (isOnTheLine(x2, y2, x1, y1, x, y, select)) {
                        return true;
                    }
                }
                if ((x1 != x2)) {
                    len = (y2 - y1);
                    float[] fibArr = {0.236f, 0.382f, 0.5f, 0.618f, 0.764f, 1.618f, 2.618f, 4.236f}; // added 0.764f - pramoda
                    for (int i = 0; i < fibArr.length; i++) {
                        y0 = y2 - Math.round(fibArr[i] * len);
                        if (isOnLineForMove(x1, y0, x1 + grfWidth, y0, x, y, select)) {
                            return true;
                        }
                    }
                    if (isOnLineForMove(x1, y1, x1 + grfWidth, y1, x, y, select)) {
                        return true;
                    }
                    if (isOnLineForMove(x1, y2, x1 + grfWidth, y2, x, y, select)) {
                        return true;
                    }
                }
                return false;
            case INT_FIBONACCI_ZONES:
                y1 = grf_Top;
                y2 = grf_Top + grf_Ht;
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                int preVal = 0;
                int curVal = 1;
                int tmpVal = 0;
                int sum = 0;
                if (isOnLineForMove(x1, y1, x1, y2, x, y, select)) {
                    return true;
                }
                while (true) {
                    sum += curVal;
                    //System.out.println("���� writing fibonaaci value "+sum);
                    x0 = Math.round((xArray[0] + sum - bIndex) * xFactor) + grfLeft;
                    if (x0 > grfLeft + grfWidth) break;
                    if (isOnLineForMove(x0, y1, x0, y2, x, y, select)) {
                        return true;
                    }
                    tmpVal = curVal;
                    curVal += preVal;
                    preVal = tmpVal;
                }
                return false;
            case INT_STD_ERROR_CHANNEL:  // Pramoda - (2006-03-01)
                x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
                x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
                y1 = grf_Ht - Math.round((yArray[0] - minY) * yFactor) + grf_Top;
                y2 = grf_Ht - Math.round((yArray[1] - minY) * yFactor) + grf_Top;
                int y3 = grf_Ht - Math.round((yArray[2] - minY) * yFactor) + grf_Top;
                int y4 = grf_Ht - Math.round((yArray[3] - minY) * yFactor) + grf_Top;
                int y5 = grf_Ht - Math.round((yArray[4] - minY) * yFactor) + grf_Top;
                int y6 = grf_Ht - Math.round((yArray[5] - minY) * yFactor) + grf_Top;

                tempX1 = x1;
                tempX2 = x2;
                tempY1 = y1;
                tempY2 = y2;
                int tempY3 = y3;
                int tempY4 = y4;
                int tempY5 = y5;
                int tempY6 = y6;
                if (extendedLeft) {
                    ratio = 1f;
                    if ((x1 == x2)) {
                        tempY1 = grf_Top - grf_Ht;
                        tempY3 = grf_Top - grf_Ht;
                        tempY5 = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / (float) (x2 - x1);
                        tempX1 = x1 - rect.width;
                        tempY1 = y1 - Math.round(ratio * rect.width);
                        tempY3 = y3 - Math.round(ratio * rect.width); //ratio is same because the lines are parallel
                        tempY5 = y5 - Math.round(ratio * rect.width);
                    }
                }
                if (extendedRight) {
                    ratio = 1f;
                    if ((x1 == x2)) {
                        tempY2 = grf_Top - grf_Ht;
                        tempY4 = grf_Top - grf_Ht;
                        tempY6 = grf_Top - grf_Ht;
                    } else {
                        ratio = (float) (y2 - y1) / (float) (x2 - x1);
                        tempX2 = x1 + rect.width;
                        tempY2 = y1 + Math.round(ratio * rect.width);
                        tempY4 = y3 + Math.round(ratio * rect.width);
                        tempY6 = y5 + Math.round(ratio * rect.width);
                    }
                }

                result = isOnTheLine(tempX1, tempY1, tempX2, tempY2, x, y, select) ||
                        isOnTheLine(tempX1, tempY3, tempX2, tempY4, x, y, select) ||
                        isOnTheLine(tempX1, tempY5, tempX2, tempY6, x, y, select);

                return result;
            default:
                return false;
        }
        return false;
    }

    private boolean isCursorOnMoveHandles(int x1, int y1, int x2, int y2, int x, int y, boolean select) {
        boolean result = false;
        if (((x1 - x) * (x1 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
//			movingPoint.setLocation(x1, y1);
//			fixedPoint.setLocation(x2, y2);
            movingPtIndex = 0;
            result = true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
//			movingPoint.setLocation(x2, y2);
//			fixedPoint.setLocation(x1, y1);
            movingPtIndex = 1;
            result = true;
        } else if (((x1 - x) * (x1 - x) <= halfSquared) && ((y2 - y) * (y2 - y) <= halfSquared)) {
//			movingPoint.setLocation(x1, y2);
//			fixedPoint.setLocation(x2, y1);
            movingPtIndex = 2;
            result = true;
        } else if (((x2 - x) * (x2 - x) <= halfSquared) && ((y1 - y) * (y1 - y) <= halfSquared)) {
//			movingPoint.setLocation(x2, y1);
//			fixedPoint.setLocation(x1, y2);
            movingPtIndex = 3;
            result = true;
        }
        if (result) {
            if (!selected)
                selected = select;
            objectMoving = (objType == INT_SYMBOL);
            return true;
        }
        return false;
    }

    public String getShortName() {
        switch (objType) {
            case INT_LINE_SLOPE:
                return Language.getString("TRND_LINE");
            case INT_LINE_HORIZ:
                return Language.getString("HIRIZONTAL_LINE");
            case INT_LINE_VERTI:
                return Language.getString("VERTICAL_LINE");
            case INT_SYMBOL:
                return Language.getString("SYMBOL");
            case INT_RECT:
                return Language.getString("RECTANGLE");
            case INT_TEXT:
                return Language.getString("TEXT_NOTE");
            case INT_ELLIPSE:
                return Language.getString("ELLIPSE");
            case INT_POLYGON:
                return Language.getString("POLYGON");
            case INT_REGRESSION:
                return Language.getString("REGRESSION_LINE");
            case INT_FIBONACCI_ARCS:
                return Language.getString("GRAPH_OBJ_FIB_ARCS");
            case INT_FIBONACCI_FANS:
                return Language.getString("GRAPH_OBJ_FIB_FANS");
            case INT_FIBONACCI_RETRACEMENTS:
                return Language.getString("GRAPH_OBJ_FIB_RETRC");
            case INT_FIBONACCI_ZONES:
                return Language.getString("GRAPH_OBJ_FIB_ZONES");
            case INT_STD_ERROR_CHANNEL:
                return Language.getString("GRAPH_OBJ_STD_ERROR_CHANNEL");
            default:
                return Language.getString("TRND_LINE");
        }
    }

    public long[] getXArr() {
        return xArray;
    }

    //xArray
    public void setXArr(long[] xArr) {
        xArray = xArr;
    }

    public float[] getYArr() {
        return yArray;
    }

    //yArray
    public void setYArr(float[] yArr) {
        yArray = yArr;
    }

    //onPriceGraph
//	public boolean isOnPriceGraph(){
//		return onPriceGraph;
//	}
//    public void setOnPriceGraph(boolean bool){
//        onPriceGraph = bool;
//    }
    //objType
    public int getObjType() {
        return objType;
    }

    public void setObjType(int i) {
        objType = i;
    }

    //selected
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean bool) {
        selected = bool;
    }

    //hidden
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean bool) {
        hidden = bool;
    }

    //color
    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    //penStyle
    public byte getPenStyle() {
        return penStyle;
    }

    public void setPenStyle(byte ps) {
        penStyle = ps;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    public void setPenStyle(Byte PS) {
        setPenStyle(PS.byteValue());
    }

    //penWidth
    public float getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(float pw) {
        penWidth = pw;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    //pen
    public BasicStroke getPen() {
        return pen;
    }

    private void setPen(BasicStroke p) {
        pen = p;
    }

    //text
    public String getText() {
        return text;
    }

    public void setText(String s) {
        text = s;
    }

    //objectMoving
    public boolean isObjectMoving() {
        return objectMoving;
    }

    //OhlcPriority
    public byte getOHLCPriority() {
        return OHLCPriority;
    }

    public void setOHLCPriority(byte p) {
        OHLCPriority = p;
    }

    //target
    public ChartProperties getTarget() {
        return target;
    }

    //rect
    public Rectangle getRect() {
        return rect;
    }

    public void setRect(Rectangle r) {
        rect = r;
    }

    //font
    public Font getFont() {
        return font;
    }

    public void setFont(Font f) {
        font = f;
    }

    //fillColor
    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fc) {
        fillColor = fc;
    }

    //fontColor
    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fc) {
        fontColor = fc;
    }

    //transparent
    public boolean isTransparent() {
        return transparent;
    }

    public void setTransparent(boolean b) {
        transparent = b;
    }

    //Extension - Pramoda
    public boolean isExtendedLeft() {
        return extendedLeft;
    }

    public void setExtendedLeft(boolean extendedLeft) {
        this.extendedLeft = extendedLeft;
    }

    public boolean isExtendedRight() {
        return extendedRight;
    }
    //

    public void setExtendedRight(boolean extendedRight) {
        this.extendedRight = extendedRight;
    }

    //Pramoda (2006-03-02)
    public int getChannelUnits() {
        return channelUnits;
    }
    //

    public void setChannelUnits(int channelUnits) {
        this.channelUnits = channelUnits;
    }

	/*public String toString(boolean currMode){
        String s = getShortName()+" ("+getValueString(currMode)+")";
		return s;
	}*/

	/*private String getValueString(boolean currMode){
		SimpleDateFormat timeFormatter;
		if (currMode){
			timeFormatter = new SimpleDateFormat ("HH:mm");
		}else{
			timeFormatter = new SimpleDateFormat ("dd/MM/yy");
		}
		Date D1, D2 = null;
		D1 = new Date(xArray[0]);
		if (xArray.length>1){
			D2 = new Date(xArray[1]);
		}
		///////////////////////////////////////
		switch (objType) {
			case INT_LINE_SLOPE:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands())+", "+
						timeFormatter.format(D2)+", "+
						GraphDataManager.formatPriceField(yArray[1], ((WindowPanel)rect).isInThousands());
			case INT_LINE_HORIZ:
				return  GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands());
			case INT_LINE_VERTI:
				return  timeFormatter.format(D1);
			case INT_SYMBOL:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands());
			case INT_RECT:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands())+", "+
						timeFormatter.format(D2)+", "+
						GraphDataManager.formatPriceField(yArray[1], ((WindowPanel)rect).isInThousands());
			case INT_TEXT:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands());
			case INT_ELLIPSE:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands())+", "+
						timeFormatter.format(D2)+", "+
						GraphDataManager.formatPriceField(yArray[1], ((WindowPanel)rect).isInThousands());
			case INT_POLYGON:
				return  timeFormatter.format(D1)+", "+
						GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands());
			case INT_REGRESSION:
				return  timeFormatter.format(D1)+", "+
						timeFormatter.format(D2);
            case INT_STD_ERROR_CHANNEL: //Pramoda - 2006-03-01 TODO: how to do this??
                return  timeFormatter.format(D1)+", "+
                        GraphDataManager.formatPriceField(yArray[0], ((WindowPanel)rect).isInThousands())+", "+
                        timeFormatter.format(D2)+", " +
                        GraphDataManager.formatPriceField(yArray[1], ((WindowPanel)rect).isInThousands())+", ("+
                        GraphDataManager.formatPriceField(yArray[2], ((WindowPanel)rect).isInThousands())+", "+
                        GraphDataManager.formatPriceField(yArray[3], ((WindowPanel)rect).isInThousands())+", "+
                        GraphDataManager.formatPriceField(yArray[4], ((WindowPanel)rect).isInThousands())+", "+
                        GraphDataManager.formatPriceField(yArray[5], ((WindowPanel)rect).isInThousands())+")";
            case INT_FIBONACCI_ARCS:
            case INT_FIBONACCI_FANS:
            case INT_FIBONACCI_RETRACEMENTS:
            case INT_FIBONACCI_ZONES:
			default:
				return "";
		}
	}*/
}