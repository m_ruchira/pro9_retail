package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 11:59:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectLineHorizontal extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_LINE_HORIZ;

    public ObjectLineHorizontal() {
        super();
        objType = AbstractObject.INT_LINE_HORIZ;
    }

    public ObjectLineHorizontal(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = AbstractObject.INT_LINE_HORIZ;
        isShowingDragToolTip = false;

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
        }
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {
        for (int i = 0; i < xArray.length; i++) {
            int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
            double y0 = graph.GDMgr.getPixelFortheYValue(yArray[i], pnlID);

            //float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xarr[i]});
            //double x0 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

            int y1 = r.y;
            int y2 = r.y + r.height;

            //System.out.println("=== y1 horizontal === " + y1);
            //System.out.println("=== y2 horizontal === " + y2);

            //System.out.println("=== y0 horizontal === " + y0);

            if (y0 > y1 && y0 < y2) {
                return true;
            }
        }
        return false;
    }
}
