package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.WindowPanel;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * User: Pramoda
 * Date: Oct 5, 2006
 * Time: 12:14:55 PM
 */
public class ObjectHorizontalPriceLine extends AbstractObject {

    public static final int CURRENT_PRICE_LINE = 0;
    public static final int MIN_MAX_PRICE_LINE = 1;
    public static final int RESISTANCE_PRICE_LINE = 2;
    public static final int PREVIOUS_CLOSE_LINE = 3;

    private static final long serialVersionUID = AbstractObject.UID_LINE_HORIZ;
    private int type;
    private String descript;
    private boolean fillPattern;
    private int[] xCords;
    private int[] yCords;
    private String sLineType;


    public ObjectHorizontalPriceLine() {
        super();
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = Language.getString("CURRENT_PRICE");
        sLineType = "";
    }

    public ObjectHorizontalPriceLine(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = Language.getString("CURRENT_PRICE");
        sLineType = "";

    }

    public String getLineType() {
        return sLineType;
    }

    public void setLineType(String sLineType) {
        this.sLineType = sLineType;
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
//        g.setColor(Color.BLUE);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        Font font = Theme.getDefaultFont(Font.PLAIN, 9);
        g.setFont(font);
        DecimalFormat yValueFormat = new DecimalFormat("###,##0.00");
        String valueString = getLineType() + yValueFormat.format(yArray[0]);
        int tagWidth = g.getFontMetrics(font).stringWidth(valueString);
        int[] xCords = {grfLeft + grfWidth - tagWidth - 8, grfLeft + grfWidth - 6, grfLeft + grfWidth,
                grfLeft + grfWidth - 7, grfLeft + grfWidth - tagWidth - 8};
        this.xCords = xCords;
        int[] yCords = {y1 - 5, y1 - 5, y1, y1 + 5, y1 + 5};
        this.yCords = yCords;
        g.drawLine(rect.x, y1, xCords[0], y1);
        g.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_SOLID, 1f));
        if (fillPattern) {
            g.fillPolygon(xCords, yCords, 5);
            g.setColor(Theme.getColor("GRAPH_MIN_MAX_PRICE_COLOR"));
            g.drawPolygon(xCords, yCords, 5);
            g.drawString(valueString, grfLeft + grfWidth - tagWidth - 6, y1 + 4);
        } else {
            g.drawPolygon(xCords, yCords, 5);
            g.setColor(Theme.getColor("GRAPH_MIN_MAX_PRICE_COLOR"));
            g.drawString(valueString, grfLeft + grfWidth - tagWidth - 6, y1 + 4);
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        if (fillPattern) {
            if (xCords != null && yCords != null) {
                Polygon polygon = new Polygon(xCords, yCords, 5);
                return (polygon.contains(x, y)) && (rect.contains(x, y)) ||
                        ((y - y1) * (y - y1) < 4) && (rect.contains(x, y));
            } else {
                return false;
            }
        } else {
            return ((y - y1) * (y - y1) < 4) && (rect.contains(x, y));
        }
    }

    public String getShortName() {
        return descript;
    }

    public String getValueString(boolean currMode) {
        return graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }


    public boolean isFillPattern() {
        return fillPattern;
    }

    public void setFillPattern(boolean fillPattern) {
        this.fillPattern = fillPattern;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
