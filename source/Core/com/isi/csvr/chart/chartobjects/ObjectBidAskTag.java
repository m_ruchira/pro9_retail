package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.WindowPanel;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * User: Pramoda
 * Date: Oct 25, 2006
 * Time: 2:22:36 PM
 */
public class ObjectBidAskTag extends AbstractObject {

    private String descript;
    private boolean remove = false;
    private int[] xCords;
    private int[] yCords;

    public ObjectBidAskTag() {
        super();
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = "";
    }

    public ObjectBidAskTag(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r) {
        super(xArr, yArr, null, cp, r, null);
        objType = AbstractObject.INT_LINE_HORIZ;
        descript = "";
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        Font font = Theme.getDefaultFont(Font.PLAIN, 9);
        g.setFont(font);
        DecimalFormat yValueFormat = new DecimalFormat("###,##0.00");
        String valueString = yValueFormat.format(yArray[0]);
        int tagWidth = g.getFontMetrics(font).stringWidth(valueString);
        int[] xCords = {grfLeft + grfWidth - tagWidth - 8, grfLeft + grfWidth - 6, grfLeft + grfWidth,
                grfLeft + grfWidth - 7, grfLeft + grfWidth - tagWidth - 8};
        this.xCords = xCords;
        int[] yCords = {y1 - 5, y1 - 5, y1, y1 + 5, y1 + 5};
        this.yCords = yCords;
        g.fillPolygon(xCords, yCords, 5);
        g.setColor(Color.black);
        g.drawPolygon(xCords, yCords, 5);
        g.drawString(valueString, grfLeft + grfWidth - tagWidth - 6, y1 + 4);
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {
        if (xCords != null && yCords != null) {
            Polygon polygon = new Polygon(xCords, yCords, 5);
            return (polygon.contains(x, y)) && (rect.contains(x, y));
        } else {
            return false;
        }
    }

    public String getShortName() {
        return descript;
    }

    public String getValueString(boolean currMode) {
        return graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public boolean isRemove() {
        return remove;
    }

    public void setRemove(boolean remove) {
        this.remove = remove;
    }

    public boolean equals(Object anotherTag) {
        boolean equals = false;
        if (!(anotherTag instanceof ObjectBidAskTag)) return false;
        ObjectBidAskTag objectBidAskTag = (ObjectBidAskTag) anotherTag;
        if (descript.equals(objectBidAskTag.descript)) {
            equals = true;
        }
        return equals;
    }

}
