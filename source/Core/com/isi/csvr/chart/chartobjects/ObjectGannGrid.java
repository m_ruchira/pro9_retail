package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.text.DecimalFormat;

/**
 * User: Pramoda
 * Date: Aug 1, 2006
 * Time: 1:59:51 PM
 */
public class ObjectGannGrid extends AbstractObject {

    private float[] indexArray;

    public ObjectGannGrid() {
        super();
        objType = INT_GANN_GRID;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectGannGrid(long[] xArr, double[] yArr, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, null, cp, r, graph);
        objType = INT_GANN_GRID;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
        }
    }

    public static Point getGridDimensions(Rectangle rect, int x1, int y1, int xGap, int yGap, int[] pointCount) {
        int xGapCount = rect.width / xGap + 1;
        int yGapCount = rect.height / yGap + 1;
        pointCount[0] = (xGap > yGap) ? 2 * xGapCount + yGapCount : xGapCount + 2 * yGapCount;
        int preXCount = (x1 - rect.x) / xGap + 1;
        int preYCount = (y1 - rect.y) / yGap + 1;
        int x = x1 - xGap * preXCount;
        int y = y1 - yGap * preYCount;
        if (xGap > yGap) {
            y = y - yGap * xGapCount;
        } else {
            x = x - xGap * yGapCount;
        }
        return new Point(x, y);
    }

    public void assignValuesFrom(AbstractObject or) {
        super.assignValuesFrom(or);
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        x2 = x2 - Xadj;
        y2 = y2 - (int) Yadj;
        if (objectMoving) {
            x1 = x1 - Xadj;
            y1 = y1 - (int) Yadj;
        }
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        if ((x1 == x2) || (y1 == y2)) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            int xGap = Math.abs(x1 - x2);
            int yGap = Math.abs(y1 - y2);
            int[] pointCount = new int[1];
            pointCount[0] = 1;
            Point startPt = getGridDimensions(rect, x1, y1, xGap, yGap, pointCount);
            System.out.println("" + pointCount[0]);
            int xt, yt, xb, yb;
            if (xGap > yGap) {
                int xGapCount = this.rect.width / xGap + 2;
                xt = startPt.x;
                xb = xt + xGap * xGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    yt = startPt.y + i * yGap;
                    yb = yt + xGapCount * yGap;
                    g.drawLine(xt, yt, xb, yb);
                    g.drawLine(xb, yt, xt, yb);
                }
            } else {
                int yGapCount = this.rect.height / yGap + 2;
                yt = startPt.y;
                yb = yt + yGap * yGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    xt = startPt.x + i * xGap;
                    xb = xt + yGapCount * xGap;
                    g.drawLine(xt, yt, xb, yb);
                    g.drawLine(xb, yt, xt, yb);
                }
            }
        }


        /* float[] xArr = graph.GDMgr.convertXArrayTimeToIndex(xArray);
                    int x1, x2, y1, y2;
                    x1 = graph.getPixelFortheIndex(xArr[0]);
                    x2 = graph.getPixelFortheIndex(xArr[1]);
                    y1 = graph.getPixelFortheYValue(YArray[0], Rect);
                    y2 = graph.getPixelFortheYValue(YArray[1], Rect);
                    x2 = x2 - Xmove;
                    y2 = y2 - Ymove;
                    if (objectMoving) {
                        x1 = x1 - Xmove;
                        y1 = y1 - Ymove;
                    }
                    if ((x1 == x2)||(y1==y2)) {
                        //g.drawLine(x1, y1, x2, y2);
                        SharedMethods.DrawLine(hdc, x1, y1, x2, y2);
                    } else {
                        int xGap = Math.Abs(x1 - x2);
                        int yGap = Math.Abs(y1 - y2);
                        int pointCount = 1;
                        Point startPt = getGridDimensions(x1, y1, xGap, yGap, ref pointCount);
                        int xt, yt, xb, yb;
                        if (xGap > yGap) {
                            int xGapCount = this.Rect.Bounds.Width / xGap + 2;
                            xt = startPt.X;
                            xb = xt + xGap * xGapCount;
                            for (int i = 0; i < pointCount; i++) {
                                yt = startPt.Y + i * yGap;
                                yb = yt + xGapCount * yGap;
                                SharedMethods.DrawLine(hdc, xt, yt, xb, yb);
                                SharedMethods.DrawLine(hdc, xb, yt, xt, yb);
                            }
                        } else {
                            int yGapCount = this.Rect.Bounds.Height / yGap + 2;
                            yt = startPt.Y;
                            yb = yt + yGap * yGapCount;
                            for (int i = 0; i < pointCount; i++) {
                                xt = startPt.X + i * xGap;
                                xb = xt + yGapCount * xGap;
                                SharedMethods.DrawLine(hdc, xt, yt, xb, yb);
                                SharedMethods.DrawLine(hdc, xb, yt, xt, yb);
                            }
                        }
                    }
        */

    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        boolean drawSelected = selected && !isPrinting;
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect.x, rect.y, rect.width, rect.height);
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        if ((x1 == x2) || (y1 == y2)) {
            g.drawLine(x1, y1, x2, y2);
        } else {
            int xGap = Math.abs(x1 - x2);
            int yGap = Math.abs(y1 - y2);

            int[] pointCount = new int[1];
            pointCount[0] = 1;
            Point startPt = getGridDimensions(rect, x1, y1, xGap, yGap, pointCount);
            int xt, yt, xb, yb;
            if (xGap > yGap) {
                int xGapCount = this.rect.width / xGap + 2;
                xt = startPt.x;
                xb = xt + xGap * xGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    yt = startPt.y + i * yGap;
                    yb = yt + xGapCount * yGap;
                    g.drawLine(xt, yt, xb, yb);
                    g.drawLine(xb, yt, xt, yb);
                }
            } else {
                int yGapCount = this.rect.height / yGap + 2;
                yt = startPt.y;
                yb = yt + yGap * yGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    xt = startPt.x + i * xGap;
                    xb = xt + yGapCount * xGap;
                    g.drawLine(xt, yt, xb, yb);
                    g.drawLine(xb, yt, xt, yb);
                }
            }
        }

        if (drawSelected) {
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            g.setStroke(selectPen);
            g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.BLACK);
            g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        }
    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        if (!rect.contains(x, y) || !isMove) return false;
        indexArray = xArray;
        int x1, x2, y1, y2;
        x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            if (!objectMoving && (movingPtIndex == 0)) {
                objectMoving = true;
            }
            return true;
        }

        if (rect.contains(x, y)) {
            if ((x1 != x2) && (y1 != y2)) {
                int xGap = Math.abs(x1 - x2);
                int yGap = Math.abs(y1 - y2);
                int[] pointCount = new int[1];
                pointCount[0] = 1;
                Point startPt = getGridDimensions(rect, x1, y1, xGap, yGap, pointCount);
                int xt, yt, xb, yb;
                if (xGap > yGap) {
                    int xGapCount = this.rect.width / xGap + 2;
                    xt = startPt.x;
                    xb = xt + xGap * xGapCount;
                    for (int i = 0; i < pointCount[0]; i++) {
                        yt = startPt.y + i * yGap;
                        yb = yt + xGapCount * yGap;
                        if (isOnLineForMove(xt, yt, xb, yb, x, y, select) || isOnLineForMove(xb, yt, xt, yb, x, y, select)) {
                            return true;
                        }
                    }
                } else {
                    int yGapCount = this.rect.height / yGap + 2;
                    yt = startPt.y;
                    yb = yt + yGap * yGapCount;
                    for (int i = 0; i < pointCount[0]; i++) {
                        xt = startPt.x + i * xGap;
                        xb = xt + yGapCount * xGap;
                        if (isOnLineForMove(xt, yt, xb, yb, x, y, select) || isOnLineForMove(xb, yt, xt, yb, x, y, select)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("OBJECT_GANN_GRID");
    }

    public String getValueString(boolean currMode) {
        float indexGap = 0;
        if (indexArray != null) {
            indexGap = indexArray[1] - indexArray[0];
        }
        DecimalFormat indexFormat = new DecimalFormat("###,###0");
        DecimalFormat yValueFormat = new DecimalFormat("###,##0.00000");
        double yValGap = yArray[1] - yArray[0];
        return yValueFormat.format(yValGap) + " x " + indexFormat.format(indexGap);
    }

    //to check the abstract object is inside a given rectangle
    public boolean isInsideTheRectangle(Rectangle rect, StockGraph graph) {

        return true; // always true.. :-)
    }
}
