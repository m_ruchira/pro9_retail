package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeThreeParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Feb 12, 2007
 * Time: 2:25:03 PM
 */
public class MACDVolume extends ChartProperties implements Indicator {

    private MovingAverage signalLine;
    private Color signalColor;
    private int signalTimePeriods;
    private byte signalStyle;
    private ChartProperties innerSource;

    private MACDHistogram histogram;
    private String tableColumnHeading;

    private int MACD_TimePeriods_High = 26;
    private int MACD_TimePeriods_Low = 12;


    public MACDVolume(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_MACD_VOLUME") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MACD_VOLUME_ADJ");
        signalTimePeriods = 9;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;
        /* Following are essential for MACD Property dialogs */
//        signalLine = new MovingAverage(data, symbl, anID, signalColor, r);
//        signalLine.setPenStyle(signalStyle);
//        signalLine.setTimePeriods(9);
//        histogram = new MACDHistogram(data, symbl, anID, signalColor, r);
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeThreeParameterProperty idp = (IndicatorTypeThreeParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setSignalTimePeriods(idp.getTimePeriods());
            this.setSignalColor(idp.getLineColor());
            this.setSignalStyle(idp.getLineStyle2());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          byte stepEMA12, byte stepEMA26, byte stepRes1, byte stepRes2, byte stepRes3, byte stepRes4, byte srcIndex, byte destIndex) {

        ChartRecord cr;

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && Double.NEGATIVE_INFINITY != val && Double.POSITIVE_INFINITY != val && Indicator.NULL != (val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        //int loopBegin = bIndex + ((MACD) innerSource).getMACD_TimePeriods_High() - 1;
        int loopBegin = bIndex + 26 - 1;

        if (loopBegin >= al.size()) { //not enough data
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        int MACD_TimePeriods_Low = 9;
        int MACD_TimePeriods_High = 26;
        MovingAverage.getVariableMovingAverage(al, bIndex, MACD_TimePeriods_Low, MovingAverage.METHOD_VOLUME_ADJUSTED, GDM, stepRes3, stepRes4, srcIndex, stepEMA12);
        MovingAverage.getVariableMovingAverage(al, bIndex, MACD_TimePeriods_High, MovingAverage.METHOD_VOLUME_ADJUSTED, GDM, stepRes1, stepRes2, srcIndex, stepEMA26);

        int tmpBeginIndex = al.size();
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((cr.getValue(stepEMA12) == Indicator.NULL) || Double.isNaN(cr.getValue(stepEMA12)) ||
                    (cr.getValue(stepEMA26) == Indicator.NULL) || Double.isNaN(cr.getValue(stepEMA26))
                    ) {
                tmpBeginIndex = i + 1;
            } else {
                break;
            }
        }
        loopBegin = tmpBeginIndex;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(stepEMA12) - cr.getValue(stepEMA26);
            if (!Double.isNaN(val) && val != Double.NEGATIVE_INFINITY && val != Double.POSITIVE_INFINITY &&
                    Indicator.NULL != val) {
                cr.setValue(destIndex, val);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }


    }

    public static int getAuxStepCount() {
        return 6;
    }

//        public void updateSignalParams(IndicatorBase indicator) {
//            if (indicator is MovingAverage) {
//                this.signalColor = indicator.Color;
//                this.signalTimePeriods = indicator.T_im_ePeriods;
//                this.signalStyle = indicator.PenStyle;
//                this.signalLine.assignValuesFrom(indicator);
//                if (histogram != null) {
//                    histogram.SignalTimePeriods = signalLine.T_im_ePeriods;
//                    histogram.SignalMethod = signalLine.Method;
//                }
//            }
//        }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.signalTimePeriods = 9;
        this.signalStyle = GraphDataManager.STYLE_DASH;
        this.signalColor = Color.DARK_GRAY;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MACDVolume) {
            MACDVolume macdVolume = (MACDVolume) cp;
            this.signalTimePeriods = macdVolume.signalTimePeriods;
            this.signalStyle = macdVolume.signalStyle;
            this.signalColor = macdVolume.signalColor;
            this.innerSource = macdVolume.innerSource;
            if ((this.signalLine != null) && (macdVolume.signalLine != null)) {
                this.signalLine.assignValuesFrom(macdVolume.signalLine);
            }
            if ((this.histogram != null) && (macdVolume.histogram != null)) {
                this.histogram.assignValuesFrom(macdVolume.histogram);
            }
        }
    }

    protected void loadFromTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadTemplate(xpath, document, preExpression, false);
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveToTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        this.innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this calculation includes 6 intermediate steps: EMA26 and EMA12
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();
        int loopBegin = MACD_TimePeriods_High - 1;
        if (loopBegin >= al.size()) return;
        ChartRecord cr;

        //setting step size 2
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(6);
        }
        // steps involved
        byte stepEMA26 = ChartRecord.STEP_1;
        byte stepEMA12 = ChartRecord.STEP_2;
        byte stepRes1 = ChartRecord.STEP_3;
        byte stepRes2 = ChartRecord.STEP_4;
        byte stepRes3 = ChartRecord.STEP_5;
        byte stepRes4 = ChartRecord.STEP_6;

        MovingAverage.getVariableMovingAverage(al, 0, MACD_TimePeriods_High, MovingAverage.METHOD_VOLUME_ADJUSTED,
                GDM, stepRes1, stepRes2, getOHLCPriority(), stepEMA26);
        MovingAverage.getVariableMovingAverage(al, 0, MACD_TimePeriods_Low, MovingAverage.METHOD_VOLUME_ADJUSTED,
                GDM, stepRes3, stepRes4, getOHLCPriority(), stepEMA12);

        int tmpBeginIndex = loopBegin;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((cr.getStepValue(stepEMA12) == Indicator.NULL) || Double.isNaN(cr.getStepValue(stepEMA12)) ||
                    (cr.getStepValue(stepEMA26) == Indicator.NULL) || Double.isNaN(cr.getStepValue(stepEMA26))) {
                tmpBeginIndex = i + 1;
            } else {
                break;
            }
        }
        for (int i = tmpBeginIndex - 1; i >= 0; i--) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = tmpBeginIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                double val = cr.getStepValue(stepEMA12) - cr.getStepValue(stepEMA26);
                if (!Double.isNaN(val) && !Double.isInfinite(val)) {
                    aPoint.setIndicatorValue(val);
                } else {
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
            }
        }

        //System.out.println("**** MACD Volume Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        //parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + signalTimePeriods;
        return Language.getString("IND_MACD_VOLUME") + " " + parent;
    }

    public String getShortName() {
        return Language.getString("IND_MACD_VOLUME");
    }

    public MACDHistogram getHistogram() {
        return histogram;
    }

    public void setHistogram(MACDHistogram histogram) {
        this.histogram = histogram;
    }

    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color signalColor) {
        this.signalColor = signalColor;
    }

    public MovingAverage getSignalLine() {
        return signalLine;
    }

    public void setSignalLine(MovingAverage signalLine) {
        this.signalLine = signalLine;
    }

    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(byte signalStyle) {
        this.signalStyle = signalStyle;
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int signalTimePeriods) {
        this.signalTimePeriods = signalTimePeriods;
    }

    public int getMACD_TimePeriods_High() {
        return MACD_TimePeriods_High;
    }

    public void setMACD_TimePeriods_High(int MACD_TimePeriods_High) {
        this.MACD_TimePeriods_High = MACD_TimePeriods_High;
    }

    public int getMACD_TimePeriods_Low() {
        return MACD_TimePeriods_Low;
    }

    public void setMACD_TimePeriods_Low(int MACD_TimePeriods_Low) {
        this.MACD_TimePeriods_Low = MACD_TimePeriods_Low;
    }
}
