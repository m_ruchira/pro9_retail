package com.isi.csvr.chart;

import com.isi.csvr.TWFileFilter;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonResizeable;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Changed By: Charith Nidarsha
 * Date: May 6, 2005
 * Time: 4:18:06 PM
 */
public class DataWindow extends JInternalFrame implements NonResizeable, InternalFrameListener, ActionListener, ListSelectionListener, MouseListener {

    public static final int MODE_HISTORY = 0;
    public static final int MODE_INTRADAY = 1;
    private final int WIDTH1 = 230;
    private final int WIDTH2 = 410;
    private final int DATA_ROW_HEIGHT = 20;
    private String strEmpty = "-";
    private JPanel pnlDataWindow;
    private JPanel panelRows;  //pnale which holds the data rows
    private JPanel pnlToolBar; //panel which holds the top tool bar
    private JPanel pnlTable;
    private JPanel pnlAll;
    private GraphLabel lblSymbol;
    private GraphLabel lblDate;
    private GraphLabel lblValue;
    private GraphLabel lblDateLabel;
    private GraphLabel lblOpen;
    private GraphLabel lblHigh;
    private GraphLabel lblLow;
    private GraphLabel lblClose;
    private GraphLabel lblVolume;
    private GraphLabel lblTurnover;
    private JButton btnSave;
    private JButton btnPrint;
    private JButton btnDataMode;
    private JButton btnDataModeImaginary;
    private JButton btnEnableDragMode;
    private JButton btnEnableSnapToDate;
    private ArrayList<DataRow> indicatorRows;
    private SimpleDateFormat monthFormat = new SimpleDateFormat("MMM yyyy");
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd-HH:mm");
    private DecimalFormat priceFormat = new DecimalFormat("#,###.00");
    private DecimalFormat quantityFormat = new DecimalFormat("#,###");
    private String DATA_WINDOW_MODE = "DATA_WINDOW";
    private String DATA_TABLE_MODE = "DATA_TABLE";
    private ChartFrame chartFrame = null;
    private boolean windowMode = true;
    private boolean dragMode = false;
    private CardLayout cards;
    private int HEIGHT1 = 235;
    private String[] widths1 = new String[]{"100%"};
    private String[] heights1 = new String[]{"23", "100%"};
    private FlexGridLayout flexGridLayout1 = new FlexGridLayout(widths1, heights1, 0, 2);
    private int HEIGHT2 = 400;
    private String[] widths2 = new String[]{"100%"};
    private String[] heights2 = new String[]{"23", "100%"};
    private FlexGridLayout flexGridLayout2 = new FlexGridLayout(widths2, heights2, 0, 2);
    private Table table = null;
    private GraphFrame activeGraph = null;
    private String symbol;
    private Font labelFont = new Font("Tahoma", Font.PLAIN, 12);
    private boolean snapToDate = false;

    public DataWindow(ChartFrame cf) {
        super(Language.getString("DATA_WINDOW"));
        setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif")));
        this.chartFrame = cf;
        this.activeGraph = chartFrame.getActiveGraph();
        this.priceFormat = activeGraph.graph.GDMgr.getNumberFormat();
        this.addInternalFrameListener(this);
        createUI();
        indicatorRows = new ArrayList<DataRow>();
    }

    private void createUI() {

        setResizable(false);
        setIconifiable(false);
        setClosable(true);
        setMaximizable(false);
        setLocation(10, 10);
        //putClientProperty("JInternalFrame.isPalette", Boolean.TRUE);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPreferredSize(new Dimension(WIDTH1, HEIGHT1));

        pnlDataWindow = new JPanel();
        pnlDataWindow.setLayout(flexGridLayout1);

        lblSymbol = new GraphLabel(Color.black);
        lblDate = new GraphLabel(Color.black);
        lblDateLabel = new GraphLabel(Language.getString("DATE"), Color.black);
        lblValue = new GraphLabel(Color.black);
        lblOpen = new GraphLabel(Color.black);
        lblHigh = new GraphLabel(Color.black);
        lblLow = new GraphLabel(Color.black);
        lblClose = new GraphLabel(Color.black);
        lblVolume = new GraphLabel(Color.black);
        lblTurnover = new GraphLabel(Color.black);

        lblSymbol.setFont(labelFont);
        lblDate.setFont(labelFont);
        lblDateLabel.setFont(labelFont);
        lblValue.setFont(labelFont);
        lblOpen.setFont(labelFont);
        lblHigh.setFont(labelFont);
        lblLow.setFont(labelFont);
        lblClose.setFont(labelFont);
        lblVolume.setFont(labelFont);
        lblTurnover.setFont(labelFont);

        GraphLabel lblStrSymbol = new GraphLabel(Language.getString("SYMBOL"), Color.BLACK);
        lblStrSymbol.setFont(labelFont);
        GraphLabel lblStrVal = new GraphLabel(Language.getString("VALUE"), Color.black);
        lblStrVal.setFont(labelFont);
        GraphLabel lblStrOpen = new GraphLabel(Language.getString("OPEN"), Color.black);
        lblStrOpen.setFont(labelFont);
        GraphLabel lblStrHigh = new GraphLabel(Language.getString("HIGH"), Color.black);
        lblStrHigh.setFont(labelFont);

        GraphLabel lblStrLow = new GraphLabel(Language.getString("LOW"), Color.black);
        lblStrLow.setFont(labelFont);
        GraphLabel lblStrClose = new GraphLabel(Language.getString("CLOSE"), Color.black);
        lblStrClose.setFont(labelFont);
        GraphLabel lblStrVol = new GraphLabel(Language.getString("VOLUME"), Color.black);
        lblStrVol.setFont(labelFont);
        GraphLabel lblStrTurnover = new GraphLabel(Language.getString("GRAPH_TURNOVER"), Color.black);
        lblStrTurnover.setFont(labelFont);

        panelRows = new JPanel();
        panelRows.setLayout(new ColumnLayout());
        panelRows.setBackground(Color.WHITE);

        DataRow symbolRow = new DataRow(lblStrSymbol, lblSymbol);
        panelRows.add(symbolRow);

        lblDateLabel.setText(Language.getString("DATE"));
        DataRow dateTimeRow = new DataRow(lblDateLabel, lblDate);
        panelRows.add(dateTimeRow);

        DataRow valueRow = new DataRow(lblStrVal, lblValue);
        panelRows.add(valueRow);

        DataRow openRow = new DataRow(lblStrOpen, lblOpen);
        panelRows.add(openRow);

        DataRow highRow = new DataRow(lblStrHigh, lblHigh);
        panelRows.add(highRow);

        DataRow lowRow = new DataRow(lblStrLow, lblLow);
        panelRows.add(lowRow);

        DataRow closeRow = new DataRow(lblStrClose, lblClose);
        panelRows.add(closeRow);

        DataRow volumeRow = new DataRow(lblStrVol, lblVolume);
        panelRows.add(volumeRow);

        DataRow turnoverRow = new DataRow(lblStrTurnover, lblTurnover);
        panelRows.add(turnoverRow);

        pnlToolBar = new JPanel(new FlowLayout(FlowLayout.LEADING, 3, 0));

        initButtons();      //enable snap to date

        pnlToolBar.add(btnSave);
        pnlToolBar.add(btnPrint);
        pnlToolBar.add(btnDataMode);
        pnlToolBar.add(btnEnableDragMode);
        pnlToolBar.add(btnEnableSnapToDate);

        pnlTable = new JPanel(new BorderLayout(0, 4));

        pnlAll = new JPanel();
        cards = new CardLayout();
        pnlAll.setLayout(cards);
        pnlAll.add(DATA_WINDOW_MODE, panelRows);
        pnlAll.add(DATA_TABLE_MODE, pnlTable);

        pnlDataWindow.add(pnlToolBar);
        pnlDataWindow.add(pnlAll);

        //datawindow Row Colors
        for (int i = 0; i < panelRows.getComponents().length; i++) {
            if (i % 2 == 0) {
                panelRows.getComponent(i).setBackground(Theme.getColor("GRAPH_DATAWINDOW_EVEN_ROW_BG_COLOR"));
            } else {
                panelRows.getComponent(i).setBackground(Theme.getColor("GRAPH_DATAWINDOW_ODD_ROW_BG_COLOR"));
            }
        }
        add(pnlDataWindow);

        GUISettings.applyOrientation(this);
        addActionListeners();
        pack();
        clear();
    }

    public void initButtons() {

        btnSave = new JButton();
        btnPrint = new JButton();
        btnDataMode = new JButton();
        btnEnableDragMode = new JButton();
        btnEnableSnapToDate = new JButton();
        btnDataModeImaginary = new JButton();

        btnSave.setPreferredSize(new Dimension(24, 24));
        btnPrint.setPreferredSize(new Dimension(24, 24));
        btnDataMode.setPreferredSize(new Dimension(24, 24));
        btnEnableDragMode.setPreferredSize(new Dimension(24, 24));
        btnEnableSnapToDate.setPreferredSize(new Dimension(24, 24));

        btnSave.setRolloverEnabled(true);
        btnPrint.setRolloverEnabled(true);
        btnDataMode.setRolloverEnabled(true);
        btnEnableDragMode.setRolloverEnabled(true);
        btnEnableSnapToDate.setRolloverEnabled(true);

        Border borderEmpty = BorderFactory.createEmptyBorder();
        btnSave.setBorder(borderEmpty);
        btnPrint.setBorder(borderEmpty);
        btnDataMode.setBorder(borderEmpty);
        btnEnableDragMode.setBorder(borderEmpty);
        btnEnableSnapToDate.setBorder(borderEmpty);

        updateToolTips();
        refreshButtons();
    }

    public void addActionListeners() {
        btnSave.addActionListener(this);
        btnPrint.addActionListener(this);
        btnDataMode.addActionListener(this);
        btnDataModeImaginary.addActionListener(this);
        btnEnableDragMode.addActionListener(this);
        btnEnableSnapToDate.addActionListener(this);
    }

    public void updateToolTips() {
        if (windowMode) {
            btnDataMode.setToolTipText(Language.getString("SHOW_TABLE"));
        } else {
            btnDataMode.setToolTipText(Language.getString("SHOW_WINDOW"));
        }

        if (dragMode) {
            btnEnableDragMode.setToolTipText(Language.getString("GRAPH_DISABLE_ROW_HIGHLIGHT"));
        } else {
            btnEnableDragMode.setToolTipText(Language.getString("GRAPH_ENABLE_ROW_HIGHLIGHT"));
        }

        if (snapToDate) {
            btnEnableSnapToDate.setToolTipText(Language.getString("DISABLE_SNAP_TO_DATE"));
        } else {
            btnEnableSnapToDate.setToolTipText(Language.getString("ENABLE_SNAP_TO_DATE"));
        }
    }

    public void setData(String symbol, int mode, long interval, long date, double value, double open, double high, double low, double close, long volume, double turnover) {
        lblSymbol.setText(symbol);
        boolean isTimeValid = date != Long.MIN_VALUE;
        if (mode == MODE_HISTORY) {
            SimpleDateFormat sdtf = (interval == StockGraph.MONTHLY) ? monthFormat : dateFormat;
            lblDate.setText(isTimeValid ? sdtf.format(new Date(date)) : strEmpty);
            lblDateLabel.setText(Language.getString("DATE"));
        } else {
            lblDate.setText(isTimeValid ? timeFormat.format(new Date(date)) : strEmpty);
            lblDateLabel.setText(Language.getString("TIME"));
        }
        lblValue.setText((value == Float.MIN_VALUE || Double.isInfinite(value) || Double.isNaN(value)) ? strEmpty : priceFormat.format(value));
        lblOpen.setText(open == Float.MIN_VALUE ? strEmpty : priceFormat.format(open));
        lblHigh.setText(high == Float.MIN_VALUE ? strEmpty : priceFormat.format(high));
        lblLow.setText(low == Float.MIN_VALUE ? strEmpty : priceFormat.format(low));
        lblClose.setText(close == Float.MIN_VALUE ? strEmpty : priceFormat.format(close));
        lblVolume.setText(volume == Long.MIN_VALUE ? strEmpty : priceFormat.format(volume));
        lblTurnover.setText((turnover == Long.MIN_VALUE || turnover <= 0) ? strEmpty : priceFormat.format(turnover));
    }

    public void clear() {
        lblSymbol.setText(strEmpty);
        lblDate.setText(strEmpty);
        lblValue.setText(strEmpty);
        lblOpen.setText(strEmpty);
        lblHigh.setText(strEmpty);
        lblLow.setText(strEmpty);
        lblClose.setText(strEmpty);
        lblVolume.setText(strEmpty);
        lblTurnover.setText(strEmpty);
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }

    public void internalFrameClosing(InternalFrameEvent e) {
    }

    public void internalFrameClosed(InternalFrameEvent e) {

        snapToDate = false;
        chartFrame.setDataWinVisible(false);
        chartFrame.refreshTopToolbar();

        JInternalFrame[] frames = chartFrame.getDesktop().getAllFrames();

        for (JInternalFrame frame : frames) {
            if (frame instanceof GraphFrame) {
                GraphFrame gf = (GraphFrame) frame;
                gf.graph.setCursor(Cursor.getDefaultCursor());
                dragMode = false;
                windowMode = true;
                chartFrame.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                gf.graph.GDMgr.verticalX = -1;
                ArrayList objects = gf.graph.GDMgr.getObjArray();
                ArrayList temp = (ArrayList) objects.clone();

                for (Object object : objects) {
                    AbstractObject oR = (AbstractObject) object;
                    if (oR.getObjType() == AbstractObject.INT_DATA_LINE_VERTI) {
                        temp.remove(object);
                    }
                }
                gf.graph.GDMgr.setObjArray(temp);
                gf.graph.repaint();
            }
        }

        chartFrame.setDataWindow(null);
    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void addIndicatorValues(GraphDataManager gdMgr, int timeIndex) {
        for (DataRow dataRow : indicatorRows) {
            panelRows.remove(dataRow);
            HEIGHT1 = HEIGHT1 - DATA_ROW_HEIGHT;
        }
        panelRows.doLayout();
        pnlDataWindow.doLayout();
        indicatorRows.clear();

        ArrayList indicators = gdMgr.getIndicators();
        for (Object indicatorObject : indicators) {
            ChartProperties indicator = (ChartProperties) indicatorObject;
            int index = gdMgr.getIndexForSourceCP(indicator);
            ChartPoint chartPoint = gdMgr.getChartPointForDataWindow(timeIndex, index);
            String value = (chartPoint != null) ? priceFormat.format(chartPoint.Open) : "-";
            GraphLabel lblStr = new GraphLabel(((Indicator) indicator).getTableColumnHeading(), indicator.getColor());
            lblStr.setFont(labelFont);
            GraphLabel lblVal = new GraphLabel(value, indicator.getColor());
            lblVal.setFont(labelFont);
            DataRow indicatorRow = new DataRow(lblStr, lblVal);
            indicatorRows.add(indicatorRow);
        }

        for (DataRow dataRow : indicatorRows) {
            panelRows.add(dataRow);
            HEIGHT1 = HEIGHT1 + DATA_ROW_HEIGHT;
        }
        //setting row colors
        for (int i = 0; i < panelRows.getComponents().length; i++) {
            if (i % 2 == 0) {
                panelRows.getComponent(i).setBackground(Theme.getColor("GRAPH_DATAWINDOW_EVEN_ROW_BG_COLOR"));
            } else {
                panelRows.getComponent(i).setBackground(Theme.getColor("GRAPH_DATAWINDOW_ODD_ROW_BG_COLOR"));
            }
        }
        setPreferredSize(new Dimension(WIDTH1, HEIGHT1));
        panelRows.doLayout();
        pnlDataWindow.doLayout();
        GUISettings.applyOrientation(this);
        pack();
    }

    public void actionPerformed(ActionEvent e) {

        /* refreshButtons();
                updateToolTips();
        */
        if (e.getSource() == btnDataMode) {

            table = null;
            windowMode = !windowMode;

            if (windowMode) {
                setTitle(Language.getString("DATA_WINDOW"));
                setPreferredSize(new Dimension(WIDTH1, HEIGHT1));
                setResizable(false);
                pnlDataWindow.setLayout(flexGridLayout1);
                cards.show(pnlAll, DATA_WINDOW_MODE);
                pnlAll.doLayout();
                pnlDataWindow.doLayout();
                chartFrame.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                setGraphCursors(false);
            } else {   //table mode
                dragMode = false;
                Component[] comps = pnlTable.getComponents();
                for (Component comp : comps) {
                    if (table != null && comp == table) {
                        pnlTable.remove(table);
                    }
                }
                //table = null;
                setPreferredSize(new Dimension(WIDTH2, HEIGHT2));
                setResizable(true);
                pnlDataWindow.setLayout(flexGridLayout2);
                chartFrame.getActiveGraph().prepareGraphTable();
                table = chartFrame.getActiveGraph().getGraphDataTable();
                //table.getTable().getSelectionModel().removeListSelectionListener(this);


                applyTableProperties();
                this.setTitle(getTitleString());
                addSelectionListener();
                //table.getTable().getSelectionModel().addListSelectionListener(this);
                //table.getTable().addMouseListener(this);

                chartFrame.getActiveGraph().graph.GDMgr.verticalX = -1;
                chartFrame.getActiveGraph().graph.repaint();

                pnlTable.add(table);
                cards.show(pnlAll, DATA_TABLE_MODE);
                pnlAll.doLayout();
                pnlTable.doLayout();
                pnlDataWindow.doLayout();
                setResizable(true);

            }
        } else if (e.getSource() == btnSave) {
            if (windowMode) {
                exportWindowModeData();
            } else {
                chartFrame.getActiveGraph().exportGraphData();
            }

        } else if (e.getSource() == btnPrint) {
            if (!windowMode && table != null) {
                //String title = ((JInternalFrame) table.getModel().getViewSettings().getParent()).getTitle();
                String title = this.getTitleString();
                SharedMethods.printTable(table.getTable(), PrintManager.TABLE_TYPE_DEFAULT, title);
            }
        } else if (e.getSource() == btnDataModeImaginary) {
            Component[] comps = pnlTable.getComponents();
            for (Component comp : comps) {
                if (table != null && comp == table) {
                    pnlTable.remove(table);
                }
            }
            table = null;
            setPreferredSize(new Dimension(WIDTH2, HEIGHT2));
            pnlDataWindow.setLayout(flexGridLayout2);
            chartFrame.getActiveGraph().prepareGraphTable();
            table = chartFrame.getActiveGraph().getGraphDataTable();
            applyTableProperties();
            this.setTitle(getTitleString());
            addSelectionListener();
            //table.getTable().getSelectionModel().addListSelectionListener(this);

            pnlTable.add(table);
            cards.show(pnlAll, DATA_TABLE_MODE);
            pnlAll.doLayout();
            pnlTable.doLayout();
            pnlDataWindow.doLayout();
        } else if (e.getSource() == btnEnableDragMode) {
            dragMode = !dragMode;
            if (dragMode) {
                chartFrame.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_DATA_LINE_VERTI;

            } else {
                chartFrame.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
            }
            setGraphCursors(dragMode);
            //removeSelectionListener();
        } else if (e.getSource() == btnEnableSnapToDate) {
            snapToDate = !snapToDate;
            if (!snapToDate) {
                StockGraph graph = chartFrame.getActiveGraph().graph;
                for (int i = 0; i < graph.GDMgr.getObjArray().size(); i++) {
                    AbstractObject obj = (AbstractObject) graph.GDMgr.getObjArray().get(i);
                    if (obj.getObjType() == (AbstractObject.INT_DATA_LINE_VERTI)) {
                        obj.setSelected(false);
                    }
                }
                GraphDataManager gdmr = chartFrame.getActiveGraph().graph.GDMgr;
                gdmr.verticalX = -1;
                graph.repaint();
            }
        }

        refreshButtons();
        updateToolTips();

        pack();
    }


    public void setGraphCursors(boolean dragmode) {
        if (dragmode) {
            Toolkit tk = Toolkit.getDefaultToolkit();
            Image img = tk.getImage("images/Theme" + Theme.getID() + "/charts/graph_obj_vertical_selection.gif");
            Cursor cur = tk.createCustomCursor(img, new Point(0, 0), "Ellipse");

            JInternalFrame[] frames = chartFrame.getDesktop().getAllFrames();

            for (JInternalFrame frame : frames) {
                if (frame instanceof GraphFrame) {
                    GraphFrame gf = (GraphFrame) frame;
                    gf.graph.setCursor(cur);
                }
            }
        } else {
            JInternalFrame[] frames = chartFrame.getDesktop().getAllFrames();

            for (JInternalFrame frame : frames) {
                if (frame instanceof GraphFrame) {
                    GraphFrame gf = (GraphFrame) frame;
                    gf.graph.setCursor(Cursor.getDefaultCursor());
                }
            }
        }
    }

    public void refreshButtons() {

        btnSave.setEnabled(!windowMode);
        btnPrint.setEnabled(!windowMode);
        btnEnableDragMode.setEnabled(!windowMode);
        btnEnableSnapToDate.setEnabled(!windowMode);

        btnSave.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_save.gif"));
        btnPrint.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_print.gif"));

        btnSave.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_save_act.gif"));
        btnPrint.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_tb_print_act.gif"));


        if (windowMode) {
            btnDataMode.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_data_mode.gif"));
            btnDataMode.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_data_mode_roll.gif"));
        } else {
            btnDataMode.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_window_mode.gif"));
            btnDataMode.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_window_mode_roll.gif"));
        }

        if (!dragMode) {
            btnEnableDragMode.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_vertical_selection.gif"));
            btnEnableDragMode.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_vertical_selection_roll.gif"));

        } else {
            btnEnableDragMode.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_vertical_selection_act.gif"));
            btnEnableDragMode.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_vertical_selection_act_roll.gif"));
        }

        if (!snapToDate) {
            btnEnableSnapToDate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_snap_to_date.gif"));
            btnEnableSnapToDate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_snap_to_date_roll.gif"));

        } else {
            btnEnableSnapToDate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_snap_to_date_act.gif"));
            btnEnableSnapToDate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_snap_to_date_act_roll.gif"));
        }
    }

    public void exportWindowModeData() {
        JFileChooser fileChooser = new JFileChooser();
        String[] extensionCSV = new String[]{"csv"};
        TWFileFilter filterCSV = new TWFileFilter("CSV files", extensionCSV);

        String[] extensionTXT = new String[]{"txt"};
        TWFileFilter filterTXT = new TWFileFilter("Text files", extensionTXT);

        fileChooser.setFileFilter(filterCSV);
        fileChooser.setFileFilter(filterTXT);

        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.showSaveDialog(this);

        File file = null;
        if ((file = fileChooser.getSelectedFile()) != null) {
            saveWindowModeData(file.getAbsolutePath(), fileChooser.getFileFilter());
        }
    }

    public void saveWindowModeData(String filePath, FileFilter filter) {

        if (filter.getDescription().equals("Text files") && filePath.indexOf(".") < 0) {
            filePath += ".txt";
        } else if (filter.getDescription().equals("CSV files") && filePath.indexOf(".") < 0) {
            filePath += ".csv";
        }

        FileOutputStream outStream = null;
        StringBuffer buffer = null;
        try {
            outStream = new FileOutputStream(filePath);
            buffer = new StringBuffer();

            JPanel pnlRows = getPanelRows();
            Component[] comp = pnlRows.getComponents();

            for (Component aComp : comp) {
                if (aComp instanceof DataRow) {

                    JLabel lblLeft = ((DataRow) aComp).getLblLeft();
                    JLabel lblRight = ((DataRow) aComp).getLblRight();

                    buffer.append(lblLeft.getText());

                    if (filePath.indexOf(".txt") > 0) {
                        buffer.append(" : ");
                        buffer.append(lblRight.getText());
                        buffer.append("\r\n");
                    } else if (filePath.indexOf(".csv") > 0) {
                        buffer.append(",");
                        buffer.append(lblRight.getText().replaceAll(",", ""));  //removes the , from 1,000
                        buffer.append("\n");
                    }
                }
            }

            outStream.write(buffer.toString().getBytes());
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyTableProperties() {
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setSortingEnabled();
        /*table.setAutoscrolls(true);
        table.getTable().setShowGrid(true);
        table.getTable().setGridColor(Color.BLUE);
        table.getTable().setSelectionBackground(Color.DARK_GRAY);*/

    }

    public void updateDataWindowTable(boolean currentMode, long interval, long date) {

        int rowNumber = getDesiredRow(date);
        table.getTable().setRowSelectionInterval(rowNumber, rowNumber);
        Rectangle r = table.getTable().getCellRect(rowNumber, 0, true);
        table.getTable().scrollRectToVisible(table.getTable().getCellRect(table.getTable().getRowCount() - 1, 0, true));
        table.getTable().scrollRectToVisible(r);
        table.scrollRectToVisible(table.getTable().getCellRect(rowNumber - 1, 0, true));

    }

    public void removeSelectionListener() {

        //ListSelectionListener[] listenrs = table.getTable().getListeners(ListSelectionListener.class);
        //System.out.println("======== BEFORE remove SelectionListener listenrs count :  " + listenrs.length);

        table.getTable().getSelectionModel().removeListSelectionListener(this);

        //listenrs = table.getTable().getListeners(ListSelectionListener.class);
        //System.out.println("======== AFTER remove SelectionListener listenrs count :  " + listenrs.length);

    }

    public void addSelectionListener() {

        /*ListSelectionListener[] listenrs = (ListSelectionListener[])table.getTable().getListeners(ListSelectionListener.class);
        System.out.println("========BEFORE add SelectionListener listenrs count :  " + listenrs.length);
        */
        table.getTable().getSelectionModel().addListSelectionListener(this);

        //ListSelectionListener[] listenrs = (ListSelectionListener[])table.getTable().getListeners(ListSelectionListener.class);
        //System.out.println("======== AFTER add SelectionListener listenrs count :  " + listenrs.length);
    }

    public int getDesiredRow(long longTime) {
        int rowNumber = 0;
        long time = 1;

        if (table != null) {
            int rows = table.getTable().getRowCount();
            for (int i = 0; i < rows; i++) {
                try {
                    time = (Long) table.getTable().getModel().getValueAt(i, 0);
                } catch (Exception ex) {
                    System.out.println("==== ERROR IN  getDesiredRow(long longTime) ===== ");
                    ex.printStackTrace();
                    time = 0;
                }
                if (time == longTime) {
                    return i;
                }
            }
        }
        return rowNumber;
    }

    public boolean isWindowMode() {
        return windowMode;
    }

    public void setWindowMode(boolean mode) {
        this.windowMode = mode;
    }

    public JButton getBtnDataMode() {
        return btnDataMode;
    }

    public JButton getBtnDataModeImaginary() {
        return btnDataModeImaginary;
    }

    public Table getTable() {
        return table;
    }

    public boolean isDragMode() {
        return dragMode;
    }

    public void setDragMode(boolean dragMode) {
        this.dragMode = dragMode;
    }

    public String getTitleString() {
        String baseGraph = chartFrame.getActiveGraph().graph.GDMgr.getBaseGraph();
        String title1 = StockGraph.extractSymbolFromStr(baseGraph);
        symbol = title1;
        String title2 = DataStore.getSharedInstance().getCompanyName(baseGraph);
        return (title2 + " (" + title1 + ")");
    }

    public JPanel getPanelRows() {
        return panelRows;
    }

    public String getCurrentSymbol() {
        return symbol;
    }

    public void valueChanged(ListSelectionEvent e) {
        try {
            StockGraph graph = chartFrame.getActiveGraph().graph;
            GraphDataManager gdmr = chartFrame.getActiveGraph().graph.GDMgr;
            table = chartFrame.getActiveGraph().getGraphDataTable();
            int rowIndex = table.getTable().getSelectedRow();
            if (rowIndex == -1) {
                return;//todo
            }
            long time = (Long) table.getTable().getModel().getValueAt(rowIndex, 0);
            float[] xArr = gdmr.convertXArrayTimeToIndex(new long[]{time});
            boolean hasDataline = false;

            for (int i = 0; i < graph.GDMgr.getObjArray().size(); i++) {
                AbstractObject obj = (AbstractObject) graph.GDMgr.getObjArray().get(i);
                if (obj.getObjType() == (AbstractObject.INT_DATA_LINE_VERTI)) {
                    hasDataline = true;
                    if (snapToDate) {
                        obj.setXArr(new long[]{time});
                        obj.setIndexArray(xArr);
                    } else if (obj.isSelected()) {
                        obj.setXArr(new long[]{time});
                        obj.setIndexArray(xArr);
                    }
                }
            }
            if (!hasDataline && snapToDate) {
                AbstractObject obj = AbstractObject.CreateObject(AbstractObject.INT_DATA_LINE_VERTI, new long[]{time}, new double[]{0}, xArr, null, (Rectangle) graph.panels.get(0), graph.panels, graph);
                graph.GDMgr.addToObjectArray(obj);
            }
            graph.repaint();

        } catch (Exception ex) {
            System.out.println("==== ERROR IN valueChanged(ListSelectionEvent e) ===== ");
            ex.printStackTrace();
        }
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println(" ======= mouse clicked ====== ");
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        // System.out.println(" ======= mouseReleased ====== ");
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isSnapToDate() {
        return snapToDate;
    }

    public void setSnapToDate(boolean snapToDate) {
        this.snapToDate = snapToDate;
    }

    class DataRow extends JPanel {

        JLabel lblLeft;
        JLabel lblRight;

        public DataRow(JLabel lblLeft, JLabel lblRight) {
            setBackground(Theme.getColor("GRAPH_DATAWINDOW_BG_COLOR"));
            //String[] widths = {"10", "90", "100"};
            String[] widths = {"10", "90", "10", "120"};
            String[] heights = {"20"};
            setLayout(new FlexGridLayout(widths, heights));

            add(new JLabel());
            add(lblLeft);
            add(new JLabel());
            add(lblRight);
            setPreferredSize(new Dimension(WIDTH1, DATA_ROW_HEIGHT));
        }

        public JLabel getLblLeft() {
            return lblLeft;
        }

        public void setLblLeft(JLabel lblLeft) {
            this.lblLeft = lblLeft;
        }

        public JLabel getLblRight() {
            return lblRight;
        }

        public void setLblRight(JLabel lblRight) {
            this.lblRight = lblRight;
        }
    }
}
