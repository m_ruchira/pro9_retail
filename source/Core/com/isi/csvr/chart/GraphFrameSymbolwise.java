package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import javax.swing.*;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 27, 2008
 * Time: 4:06:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class GraphFrameSymbolwise extends GraphFrame implements LinkedWindowListener {

    ViewSetting defaultSettings;
    String symbol;
    String settings/* woekspace settings */;
    String id/* workspace id */;
    boolean forcedMode; /*true - intraday, history otherwise*/
    private ViewSettingsManager g_oViewSettings;
    private boolean showInProCharts;

    public GraphFrameSymbolwise(ChartFrame parent, ViewSetting defaultSettings, String symbol, String settings, String id, boolean forcedMode, boolean useDefTemplate, boolean showInProChart) {
        super(parent);
        //  this.defaultSettings = defaultSettings;
        this.symbol = symbol;
        this.settings = settings;
        this.id = id;
        this.forcedMode = forcedMode;
        this.setUsingDefaultTemplate(useDefTemplate);
        this.showInProCharts = showInProChart;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        inintUI();
    }

    public void inintUI() {

        this.setDetached(true);

        Client.getInstance().oTopDesktop.add(this);
        this.setResizable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setClosable(true);
        this.setLinkGroupsEnabled(true);

        try {
            this.setSelected(true);
        } catch (Exception ex) {
        }

        if (id == null) { // creating a new chart
            defaultSettings = g_oViewSettings.getSymbolView("OUTER_CHART").getObject();
            if (defaultSettings != null) {
                defaultSettings.setParent(this);
                this.setViewSettings(defaultSettings);
                this.setSize(defaultSettings.getSize());
                //  GUISettings.setLocationRelativeTo(this, Client.getInstance().oTopDesktop);
                defaultSettings.setID("" + System.currentTimeMillis());
                defaultSettings.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }

        } else { // loading from workspace
            defaultSettings = g_oViewSettings.getChartView(ViewSettingsManager.OUTER_CHART + "|" + id);
            defaultSettings.setParent(this);
            this.setViewSettings(defaultSettings);
            defaultSettings.setID(id);
            String lnkgroup = defaultSettings.getProperty(ViewConstants.VC_LINKED_GROUP);
            if (lnkgroup != null && !lnkgroup.isEmpty()) {
                setLinkedGroupID(lnkgroup);
            }
            this.applySettings();
            if (settings != null) {
                String path = defaultSettings.getProperty(ViewConstants.VC_CHART_LAYOUT);
                if (path.split("/").length > 1) {
                    path = path.split("/")[path.split("/").length - 1];
                }
//                GraphStream.setBasicChartProperties(settings, graph);
                LayoutFactory.LoadLayout(
//                        new File(Settings.getAbsolutepath() + defaultSettings.getProperty(ViewConstants.VC_CHART_LAYOUT)), null, true, this, true);
                        new File(settings + "//" + path), null, true, this, true);
            }
            //graph.applySettings();
        }


        if (id == null) { // not loading from a workspace. must have a symbol
            this.isSendOHLCRequest = false;
            if (symbol == null) {
                symbol = Client.getInstance().getSelectedSymbol();
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        Client.getInstance().oTopDesktop.remove(this);
                        SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                    if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        Client.getInstance().oTopDesktop.remove(this);
                        return;
                    }
                    this.popupGraphData(symbol, forcedMode);
                } else {
                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
                    Client.getInstance().oTopDesktop.remove(this);
                    return;
                }
            } else {
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeExpiryMesage((SharedMethods.getExchangeFromKey(symbol)));
                        Client.getInstance().oTopDesktop.remove(this);
                        return;
                    }
                    if (ExchangeStore.isInactive((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage((SharedMethods.getExchangeFromKey(symbol)));
                        Client.getInstance().oTopDesktop.remove(this);
                        return;
                    }
                    /* Symbol selected from the main board */
                    this.popupGraphData(symbol, forcedMode);
                } else {
                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
                    return;
                }

            }


            this.setClientGraphMode(false);

        }
        if (symbol != null && !symbol.isEmpty()) {
            defaultSettings.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, symbol);
        }
        defaultSettings.putProperty(ViewConstants.VC_LINKED, false);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //    defaultSettings = null;
        if (!defaultSettings.isLocationValid()) {
            this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
            defaultSettings.setLocation(this.getLocation());
        } else {
            this.setLocation(defaultSettings.getLocation());
        }
        this.show();
        this.updateUI();
        this.setLayer(GUISettings.TOP_LAYER);
        /*if (ChartSettings.getSettings().isOpenInProChart() && id == null) { //TODO
            this.toggleDetach();
        }*/
        if (showInProCharts && id == null) { //TODO
            this.toggleDetach();
        }
    }

    public void symbolChanged(String sKey) {
        if (sKey != null && !sKey.isEmpty()) {
            symbol = sKey;
            defaultSettings.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, symbol);
            if (ExchangeStore.isExpired((SharedMethods.getExchangeFromKey(symbol)))) {
                SharedMethods.showExchangeExpiryMesage((SharedMethods.getExchangeFromKey(symbol)));
                return;
            }
            if (ExchangeStore.isInactive((SharedMethods.getExchangeFromKey(symbol)))) {
                SharedMethods.showExchangeInactiveMesage((SharedMethods.getExchangeFromKey(symbol)));
                return;
            }
            /* Symbol selected from the main board */
            //  this.isSendOHLCRequest = false;
            try {
                TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                        new GraphFrame[]{this});
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            TemplateFactory.LoadTemplate(this, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                    symbol, this.isGraphDetached());
            //this.popupGraphData(symbol);

            //this.setGraphMode(mode, false);
        }
        this.updateUI();

    }
}
