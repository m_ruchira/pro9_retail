package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class ChartProperties implements Serializable {

    public final static BasicStroke BS_1pt0_solid = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
    public final static Color WARNING_COLOR = new Color(200, 40, 40);
    private Color warningColor = WARNING_COLOR;
    private Color preWarningColor = WARNING_COLOR;
    public final static Color NEUTRAL_COLOR = new Color(200, 190, 60);
    private Color neutralColor = NEUTRAL_COLOR;
    public final static Color HILIGHT_COLOR = new Color(200, 190, 60);
    private static final long serialVersionUID = 1072600473330203801L;
    public int pnlID;
    public String groupID = "";
    protected String longName;
    protected boolean isUsingUserDefault = false;
    protected boolean isIndicator = false;
    // is used to check if indicator is a market indicator
    protected boolean isMarketIndicator = false;
    // temporary patch for first candle always green issue - Mevan @ 2009-12-10
    protected boolean isFirstCandleGreen = true;
    private ArrayList<IntraDayOHLC> dataSource;   //to keep Graph Data
    private String Symbol;
    private String displaySymbol;
    private byte ID;
    transient private WindowPanel rect;
    private Color color;
    private Color preColor;
    private boolean useSameColor = false;
    private byte chartStyle;
    private byte OHLCPriority;
    transient private BasicStroke pen;
    private float penWidth;
    private byte penStyle;
    private SplitPointTable spTable = null;
    private DynamicArray daSplits = null;
    private int recordSize;
    private int columns;
    private boolean selected = false;
    private boolean hidden = false;
    private boolean isSignalParent = false;
    private boolean isSignalIndicator = false;
    private byte signalSourceID;
    private boolean savedAndOpened;
    private boolean userSaved = false;

    public ChartProperties(ArrayList<IntraDayOHLC> data, String symbl, byte anID, Color c, WindowPanel r) {

        dataSource = data;
        Symbol = symbl;
        ID = anID;
        rect = r;

        color = c;
        warningColor = c;
        neutralColor = c;

        if (anID == GraphDataManager.ID_BASE) {
            OHLCPriority = GraphDataManager.INNER_Close;
            chartStyle = GraphDataManager.baseGraphStyle;
        } else {
            //OHLCPriority = 0
            chartStyle = StockGraph.INT_GRAPH_STYLE_LINE;
        }
        pen = PropertyDialogFactory.getBasicStroke(GraphDataManager.STYLE_SOLID, 1f);
        recordSize = 0;
        penWidth = 1;
        penStyle = GraphDataManager.STYLE_SOLID;
        if (anID <= GraphDataManager.ID_COMPARE) {
            daSplits = new DynamicArray();
            daSplits.setComparator(new SplitPointComparator());
        }
    }

    public boolean IsMarketIndicator() {
        return isMarketIndicator;
    }

    public void setMarketIndicator(boolean value) {
        isMarketIndicator = value;
    }

    public void assignValuesFrom(ChartProperties cp) {
        this.dataSource = cp.dataSource;
        this.Symbol = cp.Symbol;
        this.ID = cp.ID;
        this.rect = cp.rect;

        this.color = cp.color;
        this.warningColor = cp.warningColor;
        this.neutralColor = cp.neutralColor;
        this.useSameColor = cp.useSameColor;
        this.chartStyle = cp.chartStyle;
        this.OHLCPriority = cp.OHLCPriority;
        this.penWidth = cp.penWidth;
        this.penStyle = cp.penStyle;
        this.pen = cp.pen;

        this.recordSize = cp.recordSize;
        this.columns = cp.columns;
        this.selected = cp.selected;
        this.hidden = cp.hidden;
        this.userSaved = cp.userSaved;
        this.isUsingUserDefault = cp.isUsingUserDefault;
        this.isSignalIndicator = cp.isSignalIndicator;
    }

    public void assignDefaultValues() {
        this.color = Color.red;
        //this.warningColor = WARNING_COLOR;
        this.warningColor = Color.red;//red or WARNING_COLOR?
        this.penWidth = 1;
        setPenWidth(1);
        setPenStyle(GraphDataManager.STYLE_SOLID);
        this.penStyle = GraphDataManager.STYLE_SOLID;
        this.useSameColor = false;
        //this.OHLCPriority =
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        this.OHLCPriority = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/OHLCPriority"));
        this.chartStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/ChartStyle"));
        this.userSaved = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/IsUserSaved"));
        this.useSameColor = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/UseSameColor"));

        if (this.getID() == GraphDataManager.ID_BASE) {
            if (isLayout) {
                String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpColor").split(",");
                this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));

                if (!useSameColor) {
                    saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownColor").split(",");
                    this.warningColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
                } else {
                    this.warningColor = this.color;
                }
            } else {
                if (userSaved) {
                    String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpColor").split(",");
                    this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));

                    if (!useSameColor) {
                        saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownColor").split(",");
                        this.warningColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
                    } else {
                        this.warningColor = this.color;
                    }
                } else {
                    this.color = Theme.getColor("GRAPH_UP_COLOR");
                    this.warningColor = Theme.getColor("GRAPH_DOWN_COLOR");
                }
            }


        } else if (this.getID() == GraphDataManager.ID_VOLUME || this.getID() == GraphDataManager.ID_TURNOVER) { //if volume graph or turnover
            this.userSaved = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/IsUserSaved"));
            this.useSameColor = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/UseSameColor"));
            this.chartStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/ChartStyle"));

            if (isLayout) {
                String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpColor").split(",");
                this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));

                if (!useSameColor) {
                    saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownColor").split(",");
                    this.warningColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
                } else {
                    this.warningColor = this.color;
                }
            } else {
                if (userSaved) {
                    String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpColor").split(",");
                    this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));

                    if (!useSameColor) {
                        saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownColor").split(",");
                        this.warningColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
                    } else {
                        this.warningColor = this.color;
                    }
                } else {
                    if (this.getID() == GraphDataManager.ID_VOLUME) {
                        this.color = Theme.getColor("GRAPH_VOLUME_UP_COLOR");
                        this.warningColor = Theme.getColor("GRAPH_VOLUME_DOWN_COLOR");
                    } else if (this.getID() == GraphDataManager.ID_TURNOVER) {
                        this.color = Theme.getColor("GRAPH_TURNOVER_UP_COLOR");
                        this.warningColor = Theme.getColor("GRAPH_TURNOVER_DOWN_COLOR");
                    }
                }
            }


        } else {
            String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpColor").split(",");
            this.color = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));

            if (!useSameColor) {
                saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownColor").split(",");
                this.warningColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
            } else {
                this.warningColor = this.color;
            }
        }

        setPenStyle(Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/LineStyle")));
        setPenWidth(Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/LineThickness")));
        this.longName = TemplateFactory.loadProperty(xpath, document, preExpression + "/LongName");
        this.hidden = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/IsHidden"));
        this.groupID = (TemplateFactory.loadProperty(xpath, document, preExpression + "/GroupID"));

    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        TemplateFactory.saveProperty(chart, document, "OHLCPriority", Byte.toString(this.OHLCPriority));
        TemplateFactory.saveProperty(chart, document, "ChartStyle", Byte.toString(this.chartStyle));
        TemplateFactory.saveProperty(chart, document, "IsUserSaved", Boolean.toString(this.userSaved));
        String strColor = this.color.getRed() + "," + this.color.getGreen() + "," + this.color.getBlue();
        TemplateFactory.saveProperty(chart, document, "UpColor", strColor);
        strColor = this.warningColor.getRed() + "," + this.warningColor.getGreen() + "," + this.warningColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "DownColor", strColor);
        TemplateFactory.saveProperty(chart, document, "UseSameColor", Boolean.toString(this.useSameColor));
        TemplateFactory.saveProperty(chart, document, "LineStyle", Byte.toString(this.penStyle));
        TemplateFactory.saveProperty(chart, document, "LineThickness", Float.toString(this.penWidth));
        TemplateFactory.saveProperty(chart, document, "LongName", this.longName);
        TemplateFactory.saveProperty(chart, document, "IsHidden", Boolean.toString(this.hidden));
        TemplateFactory.saveProperty(chart, document, "GroupID", this.groupID);
    }

    protected void modifyTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        TemplateFactory.modifyProperty(chart, "OHLCPriority", Byte.toString(this.OHLCPriority));
        TemplateFactory.modifyProperty(chart, "ChartStyle", Byte.toString(this.chartStyle));
        String strColor = this.color.getRed() + "," + this.color.getGreen() + "," + this.color.getBlue();
        TemplateFactory.modifyProperty(chart, "UpColor", strColor);
        strColor = this.warningColor.getRed() + "," + this.warningColor.getGreen() + "," + this.warningColor.getBlue();
        TemplateFactory.modifyProperty(chart, "DownColor", strColor);
        TemplateFactory.modifyProperty(chart, "UseSameColor", Boolean.toString(this.useSameColor));
        TemplateFactory.modifyProperty(chart, "LineStyle", Byte.toString(this.penStyle));
        TemplateFactory.modifyProperty(chart, "LineThickness", Float.toString(this.penWidth));
        TemplateFactory.modifyProperty(chart, "IsUserSaved", Boolean.toString(this.isUserSaved()));
        TemplateFactory.modifyProperty(chart, "IsHidden", Boolean.toString(this.hidden));
        TemplateFactory.modifyProperty(chart, "GroupID", this.groupID);
    }

    public String getDisplaySymbol() {
        return displaySymbol;
    }

    public void setDisplaySymbol(String displaySymbol) {
        this.displaySymbol = displaySymbol;
    }

    public ArrayList<IntraDayOHLC> getDataSource() {
        return dataSource;
    }

    public void setDataSource(ArrayList<IntraDayOHLC> ds) {
        dataSource = ds;
    }

    public String getSymbol() {
        return Symbol;
    }

    public void setSymbol(String smbl) {
        Symbol = smbl;
    }

    public byte getID() {
        return ID;
    }

    public Color getWarningColor() {
        return warningColor;
    }

    public void setWarningColor(Color c) {
        warningColor = c;
    }

    public Color getNeutralColor() {
        return neutralColor;
    }

    public void setNeutralColor(Color c) {
        neutralColor = c;
    }

    //color
    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    //pen
    public BasicStroke getPen() {
        return pen;
    }

    private void setPen(BasicStroke p) {
        pen = p;
    }

    //penWidth
    public float getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(float pw) {
        penWidth = pw;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    //penStyle
    public byte getPenStyle() {
        return penStyle;
    }

    public void setPenStyle(Byte PS) {
        penStyle = PS.byteValue();
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    public void setPenStyle(byte ps) {
        penStyle = ps;
        setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
    }

    //chartstyle
    public byte getChartStyle() {
        return chartStyle;
    }

    public void setChartStyle(byte s) {
        chartStyle = s;
    }

    //OhlcPriority
    public byte getOHLCPriority() {
        return OHLCPriority;
    }

    public void setOHLCPriority(byte p) {
        OHLCPriority = p;
    }

    //recordSize
    public int getRecordSize() {
        return recordSize;
    }

    public void setRecordSize(int size) {
        recordSize = size;
    }

    //columns
    public int getColumns() {
        return columns;
    }

    public void setColumns(int c) {
        columns = c;
    }

    //selected
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean sel) {
        selected = sel;
    }

    //hidden
    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean h) {
        hidden = h;
    }

    //rect
    public WindowPanel getRect() {
        return rect;
    }

    public void setRect(WindowPanel r) {
        rect = r;
    }

    //useSameColor
    public boolean isUsingSameColor() {
        return useSameColor;
    }

    public void setUseSameColor(boolean use) {
        useSameColor = use;
    }

    public Object clone() throws CloneNotSupportedException {
        Object copy = super.clone();
        return copy;
    }

    public String getShortName() {
        if (Symbol == null) {
            return Language.getString("CHART");
        }
        return Symbol;
    }

    public SplitPointTable getSpTable() {
        return spTable;
    }

    public void setSpTable(SplitPointTable spTable) {
        this.spTable = spTable;
    }

    public boolean hasNewSplitPoints(String symbol) {
        if (getID() > GraphDataManager.ID_COMPARE) {
            return false;
        } else {
            boolean result = (spTable.getRecords().length != daSplits.size());
//            if (result) {
//                reCalcSplitFactors(symbol);
//            }
            return result;
        }
    }

    //changed by charithn for unadjusted graph
    public void reCalcSplitFactors(String symbol, boolean isCurrentMode) {

        daSplits.clear();
        ChartSplitPoints[] csps = spTable.getRecords();
        float prevFactor = 1f;
        for (int i = csps.length - 1; i >= 0; i--) {
            float currFactor = 1f;
            ChartSplitPoints point = csps[i];
            if ((point.group == 1)) {
//                currFactor = 1f / point.value;
                currFactor = point.value;
                //System.out.println("type : "+point.type + " value : "+point.value);
//                System.out.println("SplitDate " + point.date + " adj:" + ChartInterface.getTimeZoneAdjustment(symbol, point.date));
            }
            prevFactor = prevFactor * currFactor;
            long tmpTime = point.date + ChartInterface.getTimeZoneAdjustment(symbol, point.date, isCurrentMode);
            daSplits.insert(new SplitPointRecord(tmpTime, point.value, point.group, point.type, point.sequence)); //prevFactor
        }

        /* daSplits.clear();
        ChartSplitPoints[] csps = spTable.getRecords();
        float[] factorArray = new float[csps.length];
        for (int i = factorArray.length - 1; i >= 0; i--) {
            factorArray[i] = 1;
        }

        float total = 0;
        for (int i = csps.length - 1; i >= 0; i--) {

            ChartSplitPoints point = csps[i];
            if ((point.group == 1) && (isAdjustTypeApplicable(adjustTypes, point.type))) {
                calculateFromEndToBegin(factorArray, i, point.value, point.type);
                total += point.value;
            }
            }
        for (int j = csps.length - 1; j >= 0; j--) {

            ChartSplitPoints point = csps[j];
            long tmpTime = point.date + ChartInterface.getTimeZoneAdjustment(symbol, point.date, isCurrentMode);
            daSplits.insert(new SplitPointRecord(tmpTime, factorArray[j], point.group, point.type));
        }*/
    }

    public boolean isAdjustTypeApplicable(boolean[] adjustTypes, int type) {
        if (type == ChartSplitPoints.TYPE_STOCK_DIVIDEND) {
            return adjustTypes[0];
        } else if (type == ChartSplitPoints.TYPE_CASH_DIVIDEND) {
            return adjustTypes[1];
        } else if (type == ChartSplitPoints.TYPE_RIGHT_ISSUE) {
            return adjustTypes[2];
        } else if (type == ChartSplitPoints.TYPE_STOCK_SPLIT) {
            return adjustTypes[3];
        }
        return false;
    }

    public void calculateFromEndToBegin(float[] factorArray, int endIndex, float factor, int type) {

        for (int i = endIndex; i >= 0; i--) {
            if (type == ChartSplitPoints.TYPE_CASH_DIVIDEND) {
                factorArray[i] = factorArray[i] + factor;
            } else {
                factorArray[i] = factorArray[i] * factor;
            }
        }
    }

    public float getSplitFactor(long time) {
        SplitPointRecord spr = new SplitPointRecord(time, 2f, 0, -1, 0);
        int searchIndex = daSplits.indexOfContainingValue(spr);
        if (searchIndex < 0) {
            searchIndex = -searchIndex - 1;
        } else {
            searchIndex = searchIndex + 1;
        }
        if (daSplits.size() > searchIndex) {
            spr = (SplitPointRecord) daSplits.get(searchIndex);
            return spr.factor;
        } else {
            return 1f;
        }
    }

    //added by charithn
    public ArrayList<SplitPointRecord> getSplitPointRecrod(long time) {
        ArrayList<SplitPointRecord> records = new ArrayList<SplitPointRecord>();
        for (int i = 0; i < daSplits.size(); i++) {
            SplitPointRecord point = (SplitPointRecord) daSplits.get(i);
            if (point.group == 1 && time == point.time) {
                records.add(point);
            }
        }
        if (records.size() == 1) {
            return records;
        } else {
            //return sortRecords(records);
            return (records);
        }
    }

    public ArrayList<SplitPointRecord> sortRecords(ArrayList<SplitPointRecord> records) {

        ArrayList<SplitPointRecord> result = new ArrayList<SplitPointRecord>();
        for (int i = 0; i < records.size(); i++) {
            SplitPointRecord splitPointRecord = records.get(i);
            if (splitPointRecord.type != ChartSplitPoints.TYPE_CASH_DIVIDEND) {
                result.add(splitPointRecord);
            }
        }
        for (int i = 0; i < records.size(); i++) {
            SplitPointRecord splitPointRecord = records.get(i);
            if (splitPointRecord.type == ChartSplitPoints.TYPE_CASH_DIVIDEND) {
                result.add(splitPointRecord);
            }
        }
        return result;
    }

    public int getSplitType(long time) {
        SplitPointRecord spr = new SplitPointRecord(time, 2f, 0, -1, 0);
        int searchIndex = daSplits.indexOfContainingValue(spr);

        /* for (int i = 0; i < daSplits.size(); i++) {
           spr = (SplitPointRecord) daSplits.get(i);
           if (spr.time == time && spr.group == 1) {
               //System.out.println("split");
               return spr.type;
           }
       }
       return -1;*/

        if (searchIndex < 0) {
            searchIndex = -searchIndex - 1;
        } else {
            searchIndex = searchIndex + 1;
        }
        if (daSplits.size() > searchIndex) {
            spr = (SplitPointRecord) daSplits.get(searchIndex);

            return spr.type;
        } else {
            return -1;
        }
    }

    public float getExactSplitFactor(long time, boolean splitAndAnnOnSameDate) {
        SplitPointRecord spr = new SplitPointRecord(time, 2f, 0, -1, 0);
        int searchIndex = daSplits.indexOfContainingValue(spr);
        if (searchIndex < 0) {
            searchIndex = -searchIndex - 1;
        } else {
            searchIndex = searchIndex + 1;
        }
        if (daSplits.size() > searchIndex) {
            if (splitAndAnnOnSameDate) {
                return (spTable.getRecords()[searchIndex + 1]).value;
            } else {
                return (spTable.getRecords()[searchIndex]).value;
            }
        } else {
            return 1f;
        }
    }

    public DynamicArray getDaSplits() {
        return daSplits;
    }

//    public ArrayList<Double> getHorizontalLines() {
//        return horizontalLines;
//    }

//    public void setHorizontalLines(ArrayList<Double> horizontalLines) {
//        this.horizontalLines = horizontalLines;
//    }

    public String toString() {
        return getShortName();
    }

    public boolean isSignalIndicator() {
        return isSignalIndicator;
    }

    public void setSignalIndicator(boolean isSignalIndicator) {
        this.isSignalIndicator = isSignalIndicator;
    }

    public byte getSignalSourceID() {
        return signalSourceID;
    }


    public void setSignalSourceID(byte signalSourceID) {
        this.signalSourceID = signalSourceID;
    }


    public boolean isSignalParent() {
        return isSignalParent;
    }

    public void setSignalParent(boolean signalParent) {
        isSignalParent = signalParent;
    }

    public void setSignalColor(Color signalColor) {
    }

    public void setSignalStyle(byte signalStyle) {
    }


    public boolean isSavedAndOpened() {
        return savedAndOpened;
    }

    public void setSavedAndOpened(boolean savedAndOpened) {
        this.savedAndOpened = savedAndOpened;
    }

    public boolean isUserSaved() {
        return userSaved;
    }

    public void setUserSaved(boolean saved) {
        userSaved = saved;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String grID) {
        groupID = grID;
    }

    public boolean isUsingUserDefault() {
        return isUsingUserDefault;
    }

    public void setUsingUserDefault(boolean userDefault) {
        isUsingUserDefault = userDefault;
    }

    public ArrayList<ChartSplitPoints> getSplitPointsForInterval(long splitDate, long interval) {
        ArrayList<ChartSplitPoints> alSplits = new ArrayList<ChartSplitPoints>();
        long timeBegin = splitDate - 1;
        long timeEnd = timeBegin + interval * 1000L;
        ChartSplitPoints bPoint = new ChartSplitPoints();
        ChartSplitPoints ePoint = new ChartSplitPoints();
        bPoint.date = timeBegin;
        ePoint.date = timeEnd;
        int searchIndex1 = -spTable.indexOfChartSplitPoint(bPoint) - 1;
        int searchIndex2 = -spTable.indexOfChartSplitPoint(ePoint) - 1;
        if (searchIndex1 < searchIndex2) {
            ChartSplitPoints[] csps = spTable.getRecords();
            for (int i = searchIndex1; i < searchIndex2; i++) {
                if (csps[i].group == 0) continue; // to avoid announcements
                alSplits.add(csps[i]);
            }
        }
        return alSplits;
    }

    public double getStrategyValue(GraphDataManagerIF GDM, long time, int pos) {
        ChartPoint srcPoint = GDM.readChartPoint(time, GDM.getIndexForSourceCP(this));
        if (srcPoint == null) {
            return 0;
        } else {
            if (this.getID() <= GraphDataManager.ID_COMPARE) {
                if ((this.chartStyle == StockGraph.INT_GRAPH_STYLE_BAR) || (this.chartStyle == StockGraph.INT_GRAPH_STYLE_LINE)) {
                    return srcPoint.getValue(this.getOHLCPriority());
                } else {
                    switch (pos) {
                        case -1:
                            return srcPoint.Low;
                        case 1:
                            return srcPoint.High;
                        default:
                            return (srcPoint.Low + srcPoint.High) / 2f;
                    }
                }
            } else {
                return srcPoint.getValue(GraphDataManager.INDICATOR);
            }
        }
    }

}


