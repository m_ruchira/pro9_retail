package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 1:18:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestSystemData {

    private long systemID = 0;
    private String systemName = "New System";
    private String systemDescription = "System Description ";
    private int generalPositions = 5;

    private String buyGrammar = "";
    private int buyQuantitiy = 100;
    private int buyType = BackTestingConstatns.BACKTEST_ENTRY_TYPE_DEFAULT_SIZE;

    private String sellGrammar = "";

    public BackTestSystemData(long systemID, String systemName, String systemDescription, int generalPositions, String buyGrammar, int buyQuantitiy, int buyType, String sellGrammar) {
        this.systemID = systemID;
        this.systemName = systemName;
        this.systemDescription = systemDescription;
        this.generalPositions = generalPositions;
        this.buyGrammar = buyGrammar;
        this.buyQuantitiy = buyQuantitiy;
        this.buyType = buyType;
        this.sellGrammar = sellGrammar;
    }

    public BackTestSystemData(String name) {
        this.systemName = name;
    }

    public BackTestSystemData(long id) {
        this.systemID = id;
    }

    public long getSystemID() {
        return systemID;
    }

    public void setSystemID(long systemID) {
        this.systemID = systemID;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getSystemDescription() {
        return systemDescription;
    }

    public void setSystemDescription(String systemDescription) {
        this.systemDescription = systemDescription;
    }

    public int getGeneralPositions() {
        return generalPositions;
    }

    public void setGeneralPositions(int generalPositions) {
        this.generalPositions = generalPositions;
    }

    public String getBuyGrammar() {
        return buyGrammar;
    }

    public void setBuyGrammar(String buyGrammar) {
        this.buyGrammar = buyGrammar;
    }

    public int getBuyQuantitiy() {
        return buyQuantitiy;
    }

    public void setBuyQuantitiy(int buyQuantitiy) {
        this.buyQuantitiy = buyQuantitiy;
    }

    public int getBuyType() {
        return buyType;
    }

    public void setBuyType(int buyType) {
        this.buyType = buyType;
    }

    public String getSellGrammar() {
        return sellGrammar;
    }

    public void setSellGrammar(String sellGrammar) {
        this.sellGrammar = sellGrammar;
    }

    public String getBuySellClassName(int logicType) {    //TODO : which way to decide
        //return "_BTI_" + systemID + "_" + logicType;
        // return BackTestingConstatns.BACK_TEST_CLASS_PATH + "_BTI_" + systemID + "_" + logicType;
        return "_BTI_" + systemID + "_" + logicType;
    }

    public void assignValuesFrom(BackTestSystemData system) {
        this.systemName = system.systemName;
        this.systemDescription = system.systemDescription;
        this.generalPositions = system.generalPositions;
        this.buyGrammar = system.buyGrammar;
        this.buyQuantitiy = system.buyQuantitiy;
        this.buyType = system.buyType;
        this.sellGrammar = system.sellGrammar;
    }


}
