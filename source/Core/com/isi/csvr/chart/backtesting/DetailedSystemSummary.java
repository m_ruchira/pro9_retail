package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 30, 2008
 * Time: 10:23:00 AM
 * To change this template use File | Settings | File Templates.
 */
public class DetailedSystemSummary {

    private int id;
    private String systemName = "";
    private String avgNetProfit;
    private String avgGain;
    private String bestProfit;
    private String worstProfit;
    private String avgTrades;
    private String avgTradesPL;
    private String avgPL;


    public DetailedSystemSummary(int id, String systemName, String avgNetProfit, String avgGain, String bestProfit, String worstProfit, String avgTrades, String avgTradesPL, String avgPL) {
        this.id = id;
        this.systemName = systemName;
        this.avgNetProfit = avgNetProfit;
        this.avgGain = avgGain;
        this.bestProfit = bestProfit;
        this.worstProfit = worstProfit;
        this.avgTrades = avgTrades;
        this.avgTradesPL = avgTradesPL;
        this.avgPL = avgPL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public String getAvgNetProfit() {
        return avgNetProfit;
    }

    public void setAvgNetProfit(String avgNetProfit) {
        this.avgNetProfit = avgNetProfit;
    }

    public String getAvgGain() {
        return avgGain;
    }

    public void setAvgGain(String avgGain) {
        this.avgGain = avgGain;
    }

    public String getBestProfit() {
        return bestProfit;
    }

    public void setBestProfit(String bestProfit) {
        this.bestProfit = bestProfit;
    }

    public String getWorstProfit() {
        return worstProfit;
    }

    public void setWorstProfit(String worstProfit) {
        this.worstProfit = worstProfit;
    }

    public String getAvgTrades() {
        return avgTrades;
    }

    public void setAvgTrades(String avgTrades) {
        this.avgTrades = avgTrades;
    }

    public String getAvgTradesPL() {
        return avgTradesPL;
    }

    public void setAvgTradesPL(String avgTradesPL) {
        this.avgTradesPL = avgTradesPL;
    }

    public String getAvgPL() {
        return avgPL;
    }

    public void setAvgPL(String avgPL) {
        this.avgPL = avgPL;
    }
}
