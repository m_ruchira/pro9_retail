package com.isi.csvr.chart.backtesting;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 30, 2008
 * Time: 10:31:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class DetailedSecuritySummary {

    private int id;
    private String securityName;
    private String symbol;
    private String netProfit;
    private String avgGain;
    private String trades;
    private String tradesPL;
    private String avgPL;

    public DetailedSecuritySummary(int id, String securityName, String symbol, String netProfit, String avgGain, String trades, String tradesPL, String avgPL) {
        this.id = id;
        this.securityName = securityName;
        this.symbol = symbol;
        this.netProfit = netProfit;
        this.avgGain = avgGain;
        this.trades = trades;
        this.tradesPL = tradesPL;
        this.avgPL = avgPL;
    }

    public String getName() {
        return securityName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSecurityName() {
        String exch = SharedMethods.getExchangeFromKey(securityName);
        Stock st = DataStore.getSharedInstance().getStockObject(exch, SharedMethods.getSymbolFromKey(securityName), SharedMethods.getInstrumentTypeFromKey(securityName));
        return st.getShortDescription();
    }

    public void setSecurityName(String securityName) {
        this.securityName = securityName;
    }

    public String getSymbol() {
        return SharedMethods.getSymbolFromKey(securityName);
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(String netProfit) {
        this.netProfit = netProfit;
    }

    public String getAvgGain() {
        return avgGain;
    }

    public void setAvgGain(String avgGain) {
        this.avgGain = avgGain;
    }

    public String getTrades() {
        return trades;
    }

    public void setTrades(String trades) {
        this.trades = trades;
    }

    public String getTradesPL() {
        return tradesPL;
    }

    public void setTradesPL(String tradesPL) {
        this.tradesPL = tradesPL;
    }

    public String getAvgPL() {
        return avgPL;
    }

    public void setAvgPL(String avgPL) {
        this.avgPL = avgPL;
    }
}
