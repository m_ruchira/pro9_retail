package com.isi.csvr.chart.backtesting;

import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 26, 2008
 * Time: 2:56:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleBarChart extends JPanel {

    private final float BAR_WIDTH_FACTOR = .3f;
    private final float[] PriceVolPitch = {1f, 2f, 4f, 5f, 8f};
    private final float ALPHA_TRANSPARENT = .2f;
    private double[] values;
    private int yAxisWidth = 80;
    private int xAxisHeight = 25;
    private int chartWidth = 450;
    private int chartHeight = 250; //these are the effective width and height of the chart
    private int legendHeight = 30;
    //colors
    private Color gridColor = Theme.getColor("BAR_CHART_GRID_COLOR");
    private Color chartBackColor = Theme.getColor("BAR_CHART_BACK_COLOR");
    private Color chartAxisFontColor = Theme.getColor("BAR_CHART_AXIS_FONT_COLOR");
    private Color upColor = Theme.getColor("BAR_CHART_UP_COLOR");
    private Color downColor = Theme.getColor("BAR_CHART_DOWN_COLOR");
    private double minY = Double.MAX_VALUE;
    private double maxY = -Double.MIN_VALUE;
    private double xFactor;
    private double yFactor;
    private double bestXPitch = 10;
    private double bestYPitch = 100;
    private double xStart = 1;
    private double yStart = 1;
    private double adj = 10;
    private String legend;
    private TWDecimalFormat formatter = null;
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("###,##0.00");

    public SimpleBarChart(double[] barValues, String str) {
        super();
        this.values = barValues;
        this.legend = str;
    }

    public SimpleBarChart() {

    }

    public SimpleBarChart(int[] array) {

    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Simple Bar Chart");
        frame.setSize(800, 450);
        frame.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"90%"}));
        frame.add(new SimpleBarChart(RandomNumberGenerator.genreateDoubleArray(10, 90, 15, 2), "Systems Comparison"));
        //frame.add(chart);
        frame.setLocation(200, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void calculatePixelValues() {

        minY = Double.MAX_VALUE;
        maxY = -Double.MIN_VALUE;
        chartWidth = Math.max(this.getWidth() - 2 * yAxisWidth, 100);
        chartHeight = Math.max((this.getHeight() - xAxisHeight - legendHeight), 80);

        for (double value : values) {
            minY = Math.min(minY, value);
            maxY = Math.max(maxY, value);
        }

        adj = (maxY - minY) * 0.2f;
        minY = minY - adj;
        maxY = maxY + adj;

        minY = (minY > 0) ? 0 : minY;

        yFactor = chartHeight / ((maxY - minY));
        bestYPitch = getBestYInterval(maxY - minY, chartHeight);
        yStart = 0; //hardcoded to zero since y basis must start from zero always.
        if (bestYPitch < 1) {
            formatter = new TWDecimalFormat("###,##0.00");
        } else {
            formatter = new TWDecimalFormat("###,###");
        }
    }

    public void paint(Graphics g) {
        super.paint(g);

        calculatePixelValues();

        g.setColor(this.getBackground());
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(chartBackColor);
        g.fillRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

        drawLegend(g);

        //drawing the chart outline of the rectangle
        g.setColor(Color.BLACK);
        g.drawRect(yAxisWidth, legendHeight, chartWidth, chartHeight);


        drawBars(g);
        drawXAxisLabels(g);
        drawYAxisLabels(g);
        drawPercentages(g);


    }

    private void drawBars(Graphics g) {

        int number = values.length + 1;
        int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
        int barWd = (int) (HGAP * 2.0 * BAR_WIDTH_FACTOR);
        int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
        int zeroPos = (int) getPixelForYValue(0);

        Graphics2D g2 = (Graphics2D) g;

        for (double value : values) {

            if (value > 0) {
                GradientPaint greentowhite = new GradientPaint(xStart, 0, upColor, xStart + barWd, 0, Theme.getColor("BAR_CHART_GRADIENT_UP_COLOR"));
                g2.setPaint(greentowhite);

                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                g.fillRect(xStart, barTop, barWd, ht);

                //drawing the % string
                g.setFont(new Font("Arial", Font.BOLD, 10));
                g.setColor(chartAxisFontColor);
                String strVal = pctFormatter.format(value) + "%";
                int labelLength = g.getFontMetrics().stringWidth(strVal);
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), barTop - 5);
            } else {
                GradientPaint redtowhite = new GradientPaint(xStart, 0, downColor, xStart + barWd, 0, Theme.getColor("BAR_CHART_GRADIENT_DOWN_COLOR"));
                g2.setPaint(redtowhite);

                int ht = (int) ((0 - value) * yFactor);
                g.fillRect(xStart, zeroPos, barWd, ht);

                //drawing the % string
                g.setFont(new Font("Arial", Font.BOLD, 10));
                g.setColor(chartAxisFontColor);
                String strVal = pctFormatter.format(value) + "%";
                int labelLength = g.getFontMetrics().stringWidth(strVal);
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), zeroPos + ht + 10);
            }
            xStart = xStart + HGAP;
        }
    }

    private void drawPercentages(Graphics g) {

        int number = values.length + 1;
        int HGAP = chartWidth / number;  //Horizontal gap between two horizonal lines
        int barWd = (int) (HGAP * 2.0 * BAR_WIDTH_FACTOR);
        int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
        int zeroPos = (int) getPixelForYValue(0);

        Graphics2D g2 = (Graphics2D) g;
        g.setFont(new Font("Arial", Font.BOLD, 10));
        g.setColor(chartAxisFontColor);

        for (double value : values) {

            String strVal = pctFormatter.format(value) + "%";
            int labelLength = g.getFontMetrics().stringWidth(strVal);

            if (value > 0) {
                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                //drawing the % string
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), barTop - 5);
            } else {
                int ht = (int) ((0 - value) * yFactor);
                //drawing the % string
                g.drawString(strVal, xStart + ((barWd - labelLength) / 2), zeroPos + ht + 10);
            }
            xStart = xStart + HGAP;
        }
    }

    private void drawYAxisLabels(Graphics g) {

        float labelLength = 0;
        g.setColor(this.getForeground());
        double currVal = yStart;
        float x = yAxisWidth - 5;
        float yAdj = 0;

        Graphics2D g2D = (Graphics2D) g;

        while (currVal <= maxY) {
            float y = getPixelForYValue(currVal);
            g.setColor(gridColor);
            g2D.setStroke(new BasicStroke(.4f));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);
            String strVal = formatter.format(currVal);
            labelLength = g.getFontMetrics().stringWidth(strVal);
            g.setColor(Color.BLACK);
            g.drawString(strVal, (int) (x - labelLength), (int) (y + yAdj));
            g.drawLine((int) yAxisWidth, (int) y, (int) yAxisWidth - 3, (int) y);
            currVal += bestYPitch;
        }

        //drawing the negative lines and values
        currVal = 0;
        while (currVal >= minY) {
            float y = getPixelForYValue(currVal);
            g.setColor(gridColor);
            g2D.setStroke(new BasicStroke(.4f));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);
            String strVal = formatter.format(currVal);
            labelLength = g.getFontMetrics().stringWidth(strVal);
            g.drawString(strVal, (int) (x - labelLength), (int) (y + yAdj));
            g.drawLine((int) yAxisWidth, (int) y, (int) yAxisWidth - 3, (int) y);
            currVal -= bestYPitch;
        }
    }

    public void drawXAxisLabels(Graphics g) {

        int number = values.length + 1;
        int width = chartWidth / number;
        int xStart = yAxisWidth + width;

        for (int i = 0; i < number - 1; i++) {
            g.setColor(chartAxisFontColor);
            g.drawLine(xStart, legendHeight + chartHeight, xStart, legendHeight + chartHeight + 8);
            g.drawString(String.valueOf(i + 1), xStart - 2, (legendHeight + chartHeight + 20));

            g.setColor(gridColor);
            g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight));
            g.drawLine(xStart, (legendHeight + chartHeight), xStart, (legendHeight + chartHeight + 4));
            xStart += width;
        }
    }

    public void drawLegend(Graphics g) {
        g.setColor(Color.BLUE);
        g.drawString(legend, yAxisWidth, legendHeight - 15);
    }

    private double getBestYInterval(double val, float pixLen) {

        double HALF_INCH = 72 / 3d;
        val = val / pixLen * HALF_INCH;

        float pitch = PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            float Multiplier = ((float) j / 1000000000);
            for (float aPriceVolPitch : PriceVolPitch) {
                if (val >= (double) Multiplier * aPriceVolPitch) {
                    pitch = (Multiplier * aPriceVolPitch);
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10f) {
                return pitch;
            }
        }
        return pitch;
    }

    private float getPixelForYValue(double value) {
        return chartHeight + legendHeight - (float) ((value - minY) * yFactor);
    }

    private double getStart(double min, double bestPitch) {
        return bestPitch * Math.round(min / bestPitch);
    }
}
