package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 9:46:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class OrderDetailRecord {

    private int barID;
    private String date;
    private int number;
    private String action;
    private String transactionType;
    private int quantity;
    private String price;
    private String position;
    private String source;

    public OrderDetailRecord(int barID, String date, int number, String action, String transactionType, int quantity, String price, String position, String source) {
        this.barID = barID;
        this.date = date;
        this.number = number;
        this.action = action;
        this.transactionType = transactionType;
        this.quantity = quantity;
        this.price = price;
        this.position = position;
        this.source = source;
    }

    public int getBarID() {
        return barID;
    }

    public void setBarID(int barID) {
        this.barID = barID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}



