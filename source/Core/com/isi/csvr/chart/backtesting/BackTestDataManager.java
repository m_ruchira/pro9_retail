package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.ChartPoint;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManagerIF;
import com.isi.csvr.scanner.Scans.ScanRecord;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 9:50:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestDataManager implements GraphDataManagerIF {

    private final int Buy = 0;
    /**
     * ***********BackTestLogic Types ********************
     */


    private int currentBackTestLogic = Buy; //todo get set


    /* ************BackTestLogic Types *********************/
    private final int Sell = 1;
    private final int SellShort = 2;
    private final int BuyToCover = 3;
    private final int LastBar = 99999;
    private ArrayList alRecords = null;
    private BackTestResult result = null;


    // *********************#region IGraphDataManager  Memembers ******************************/


    //todo Dynamic array

    public ChartPoint getIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {
        TransactionPoint tp = new TransactionPoint(timeIndex, currentBackTestLogic);
        int searchIndex = Collections.binarySearch(alRecords, new ScanRecord(timeIndex));//alRecords.indexOfContainingValue(new ScanRecord(timeIndex)); //todo need dynamic array for binary search?
        tp.setCurrentRecord((ScanRecord) alRecords.get(searchIndex));
        if (alRecords.size() > searchIndex + 1) {
            tp.setNextRecord((ScanRecord) alRecords.get(searchIndex + 1));
        }
        ArrayList transactions = result.getTransactions();
        searchIndex = Collections.binarySearch(transactions, tp);//transactions.indexOfContainingValue(tp);
        if (searchIndex >= 0) {
            transactions.add(searchIndex, tp);
        } else {
            transactions.add(-searchIndex - 1, tp);
        }
        //result.Transactions.Add(tp);
        return tp;
    }

    public void removeIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {

    }

    public int getClosestIndexFortheTime(long t) {
        //List<TransactionPoint> transactions = result.Transactions;
        int searchIndex = Collections.binarySearch(alRecords, new ScanRecord(t));//alRecords.indexOfContainingValue (new ScanRecord(t)); //transactions.BinarySearch(new TransactionPoint(t));
        if (searchIndex >= 0) {
            return searchIndex;
        } else {
            return Math.min(-searchIndex - 1, alRecords.size() - 1);
        }
    }

    public ChartPoint readChartPoint(long timeIndex, int graphIndex) {
        ArrayList transactions = result.getTransactions();
        int searchIndex = Collections.binarySearch(transactions, new TransactionPoint(timeIndex, currentBackTestLogic));//transactions.indexOfContainingValue(new TransactionPoint(timeIndex, currentBackTestLogic));
        if (searchIndex >= 0) {
            return (ScanRecord) transactions.get(searchIndex);
        }
        return null;
    }

    public int getIndexForSourceCP(ChartProperties aCp) {
        return 0;
    }


    public ArrayList<ScanRecord> getAlRecords() {
        return alRecords;
    }

    public void setAlRecords(ArrayList<ScanRecord> value) {
        alRecords = value;
    }

    public int getCurrentBackTestLogic() {
        return currentBackTestLogic;
    }

    public void setCurrentBackTestLogic(int currentBackTestLogic) {
        this.currentBackTestLogic = currentBackTestLogic;
    }

    public BackTestResult getResult() {
        return result;
    }

    public void setResult(BackTestResult result) {
        this.result = result;
    }
}
