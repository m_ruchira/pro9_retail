package com.isi.csvr.chart.backtesting;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 26, 2008
 * Time: 9:29:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class OrderDetailsTableModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList<OrderDetailRecord> records;

    public OrderDetailsTableModel(ArrayList<OrderDetailRecord> recs) {
        this.records = recs;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        OrderDetailRecord record = records.get(rowIndex);

        switch (columnIndex) {

            case -1:
                return record;
            case 0:
                return String.valueOf(record.getBarID());

            case 1:
                return record.getDate();

            case 2:
                return String.valueOf(record.getNumber());

            case 3:
                return String.valueOf(record.getAction());

            case 4:
                return String.valueOf(record.getTransactionType());

            case 5:
                return String.valueOf(record.getQuantity());

            case 6:
                return String.valueOf(record.getPrice());

            case 7:
                return record.getPosition();

            case 8:
                return record.getSource();

            default:
                return "";
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return records.size();
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }
}
