package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.ToolTipDataRow;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 25, 2008
 * Time: 10:47:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestResultPanel extends JPanel {

    private BackTestResult result;
    private TWTabbedPane resultsTabbedPane;
    private SimpleChart chart = new SimpleChart();
    private BackTestSimulationData simData;

    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,##0.00");
    private TWDecimalFormat pctgFormatter = new TWDecimalFormat("##0.00");
    private TWDateFormat dateFormatter = new TWDateFormat("dd MMM yyyy");
    private TWDateFormat formatter = new TWDateFormat("dd MMM yyyy HH:mm:ss");

    private Table equityTable;
    private Table orderDetailTable;

    public BackTestResultPanel(BackTestResult res, BackTestSimulationData simData) {
        super();
        this.result = res;
        this.simData = simData;
        createUI();
    }

    public BackTestResultPanel() {
        super();
        createUI();
    }

    private void createUI() {

        resultsTabbedPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);

        resultsTabbedPane.addTab(Language.getString("BT_GENERAL_IND"), createGeneralIndicesPanel());
        resultsTabbedPane.addTab(Language.getString("BT_ORDERS"), createOrdersPanel());
        resultsTabbedPane.addTab(Language.getString("BT_EQUITY"), createEquityPanel());
        resultsTabbedPane.setSelectedIndex(0);

        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 5));
        this.add(resultsTabbedPane);
    }

    private JScrollPane createGeneralIndicesPanel() {

        //JPanel pnlGeneral = new JPanel(new FlexGridLayout(new String[]{"2%", "96%", "2%"}, new String[]{"95", "750"}, 0, 2));
        JPanel pnlGeneral = new JPanel(new FlexGridLayout(new String[]{"99%"}, new String[]{"95", "750"}, 0, 2));
        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"50%", "1%", "49%"}, new String[]{"100%"}, 0, 2));
        pnlBottom.add(createLeftGeneralPanel());
        pnlBottom.add(new JPanel());
        pnlBottom.add(createRightGeneralPanel());

        pnlGeneral.add(createTopPanel());
        pnlGeneral.add(pnlBottom);

        return new JScrollPane(pnlGeneral, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    }

    private JPanel createTopPanel() {

        //creating the top panel
        JPanel pnlTop = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 2));
        String key = result.getSymbol();
        Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), SharedMethods.getInstrumentTypeFromKey(key));

        CustomPanel cusTop = new CustomPanel(st.getShortDescription(), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusTop.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "20", "20", "20"}, 5, 0));
        cusTop.add(new JLabel());

        JLabel lbl3 = new JLabel(Language.getString("BT_SIM_LONG") + simData.getSimulationName());
        lbl3.setFont(new Font("Arial", Font.BOLD, 11));
        lbl3.setForeground(Theme.getColor("BT_TABLE_PANEL_FONT_COLOR"));
        cusTop.add(lbl3);

        //current date time
        String displayTime = formatter.format(System.currentTimeMillis());

        JLabel lbl1 = new JLabel(Language.getString("BT_SIM_DATE") + displayTime);
        lbl1.setFont(new Font("Arial", Font.BOLD, 11));
        lbl1.setForeground(Theme.getColor("BT_TABLE_PANEL_FONT_COLOR"));
        cusTop.add(lbl1);

        String text = "";
        JLabel lbl2 = new JLabel();
        int count = result.getEquityDetailRecords().size();
        if (count > 0) {
            int days = (int) ((result.getEquityDetailRecords().get(count - 1).getTime() - result.getEquityDetailRecords().get(0).getTime()) / 86400000L);
            text = count + " " + Language.getString("BT_BARS") + " " + result.getEquityDetailRecords().get(0).getDate() +
                    " " + Language.getString("BT_TO") + " " + result.getEquityDetailRecords().get(count - 1).getDate() + Language.getString("BT_OVER") + " " + days + " " + Language.getString("BT_DAYS");
        } else {
            text = Language.getString("BT_NO_HISTORY");
            lbl2.setForeground(Color.RED);
        }

        lbl2.setText(text);
        lbl2.setFont(new Font("Arial", Font.BOLD, 11));
        lbl2.setForeground(Theme.getColor("BT_TABLE_PANEL_FONT_COLOR"));
        cusTop.add(lbl2);

        pnlTop.add(cusTop);

        return pnlTop;

    }

    private JPanel createRightGeneralPanel() {

        JPanel pnlRight = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"80", "200", "125", "100", "100", "80"}, 0, 5));

        /******************** performacne indices **************************************/
        ArrayList<ToolTipDataRow> rowsPerfIndices = new ArrayList<ToolTipDataRow>(3);

        //calcuations
        double dBHProfit = getBHProfit();
        double profit = result.getTotalEquity() - simData.getInitialEquity();
        double BHIndex = (dBHProfit == -Double.MAX_VALUE) ? 0 : (profit - dBHProfit) * 100 / dBHProfit;
        double PLIndex = profit * 100 / (profit + Math.abs(result.getUnprofitableTrades() * result.getAverageLoss()));
        TWDecimalFormat formatter = new TWDecimalFormat("###,###,###.00");

        rowsPerfIndices.add(new ToolTipDataRow(Language.getString("BT_BUY_HOLD"), formatter.format(BHIndex) + "%"));
        rowsPerfIndices.add(new ToolTipDataRow(Language.getString("BT_PRO_LOSS"), formatter.format(PLIndex) + "%"));

        double HiOpDD = 0d;
        double HiClDD = 0d;
        double minEquity = Double.MIN_VALUE;
        double highCash = 0;
        double lowCash = Double.MIN_VALUE;
        double highStocks = 0;
        double openLoEq = simData.getInitialEquity();
        double closedLoEq = simData.getInitialEquity();
        int count = result.getEquityDetailRecords().size();
        if (count > 0) {

            for (EquityDetailRecord er : result.getEquityDetailRecords()) {
                minEquity = Math.min(minEquity, er.getTotal());
                highCash = Math.max(highCash, er.getCash());
                lowCash = Math.min(lowCash, er.getCash());
                highStocks = Math.max(highStocks, er.getPortfolio());
                if (er.getQuantity() == 0) {
                    closedLoEq = Math.min(closedLoEq, er.getTotal());
                } else {
                    openLoEq = Math.min(openLoEq, er.getTotal());
                }
            }
            //RRIndex
            double RRIndex = (result.getTotalEquity() - simData.getInitialEquity()) * 100 / (result.getTotalEquity() - minEquity);
            rowsPerfIndices.add(new ToolTipDataRow(Language.getString("BT_REW_RISK"), formatter.format(RRIndex) + "%"));

            /********************account variabtion **********************/

            //Account Variation Labels
            HiOpDD = openLoEq - simData.getInitialEquity();
            HiClDD = closedLoEq - simData.getInitialEquity();

        } else {
            rowsPerfIndices.add(new ToolTipDataRow(Language.getString("BT_REW_RISK"), "N/A"));
        }

        CustomPanel cusPerformanceInd = new CustomPanel(Language.getString("BT_PER_IND"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusPerformanceInd.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusPerformanceInd.add(new JLabel());
        cusPerformanceInd.add(new TablePanel(rowsPerfIndices));
        pnlRight.add(cusPerformanceInd);
        /******************** performacne indices **************************************/


        //*****************************accounting***********************************/
        ArrayList<ToolTipDataRow> rowsAccounting = new ArrayList<ToolTipDataRow>(8);
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_INI_EQUITY"), formatter.format(simData.getInitialEquity())));
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_TR_PROFIT"), formatter.format(result.getProfitableTrades() * result.getAverageProfit())));
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_TR_LOSS"), formatter.format((result.getUnprofitableTrades() * result.getAverageLoss()))));

        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_COM"), "0"));
        double interest = 0;
        for (EquityDetailRecord er : result.getEquityDetailRecords()) {
            interest += er.getInterest();
        }

        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_INTREST_CREDIT"), formatter.format(interest)));
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_INTR_CHARGED"), "0"));
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_FIN_EQUITY"), formatter.format(result.getTotalEquity())));
        rowsAccounting.add(new ToolTipDataRow(Language.getString("BT_OPEN_POS"), result.getOpenPositions() + ""));

        CustomPanel cusAccounting = new CustomPanel(Language.getString("BT_ACC"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusAccounting.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusAccounting.add(new JLabel());
        cusAccounting.add(new TablePanel(rowsAccounting));
        pnlRight.add(cusAccounting);
        //*****************************end of accounting***********************************/

        //***************************account variation******************************************/
        ArrayList<ToolTipDataRow> rowsAccVariation = new ArrayList<ToolTipDataRow>(5);
        rowsAccVariation.add(new ToolTipDataRow(Language.getString("BT_HIGH_AC_BAL"), formatter.format(highCash)));
        rowsAccVariation.add(new ToolTipDataRow(Language.getString("BT_LOW_AC_BALANCE"), formatter.format(highCash)));
        rowsAccVariation.add(new ToolTipDataRow(Language.getString("BT_HOGH_PORT"), formatter.format(highStocks)));
        rowsAccVariation.add(new ToolTipDataRow(Language.getString("BT_HIGHEST_DOWN"), formatter.format(HiOpDD)));
        rowsAccVariation.add(new ToolTipDataRow(Language.getString("BT_HIGHES_CLOSE"), formatter.format(HiClDD)));

        CustomPanel cusAccountVar = new CustomPanel(Language.getString("BT_ACC_VAR"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusAccountVar.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusAccountVar.add(new JLabel());
        cusAccountVar.add(new TablePanel(rowsAccVariation));
        pnlRight.add(cusAccountVar);
        //***************************end of account variation******************************************/

        //************************profitable timing**********************************************/
        ArrayList<ToolTipDataRow> rowsProfTiming = new ArrayList<ToolTipDataRow>(4);

        int totalP = 0, hiP = 0, loP = Integer.MAX_VALUE, totalL = 0, hiL = 0, loL = Integer.MAX_VALUE;
        for (BackTestingPosition pos : result.getPositions()) {
            int period = pos.BuyPos - pos.SellPos;
            if (pos.BuyPrice > pos.SellPrice) { //Profitable
                totalP += period;
                hiP = Math.max(hiP, period);
                loP = Math.min(loP, period);
            } else { //Unprofitable
                totalL += period;
                hiL = Math.max(hiL, period);
                loL = Math.min(loL, period);
            }
        }

        int avgP = (result.getProfitableTrades() != 0) ? totalP / result.getProfitableTrades() : 0;

        int avgL = (result.getUnprofitableTrades() != 0) ? totalL / result.getUnprofitableTrades() : 0;
        rowsProfTiming.add(new ToolTipDataRow(Language.getString("BT_AVG_TRADE_LEN"), avgP + ""));
        rowsProfTiming.add(new ToolTipDataRow(Language.getString("BT_LONG_LEN"), hiP + ""));
        rowsProfTiming.add(new ToolTipDataRow(Language.getString("BT_SHORT_LEN"), loP + ""));
        rowsProfTiming.add(new ToolTipDataRow(Language.getString("BT_TOTAL_TRADE"), totalP + ""));

        CustomPanel cusProfTime = new CustomPanel(Language.getString("BT_PROFIT_TIME"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusProfTime.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusProfTime.add(new JLabel());
        cusProfTime.add(new TablePanel(rowsProfTiming));
        pnlRight.add(cusProfTime);
        /************************************end of profitable timeing *****************************/


        /************************************Un profitable timeing *****************************/
        ArrayList<ToolTipDataRow> rowsUnProfTiming = new ArrayList<ToolTipDataRow>(4);
        rowsUnProfTiming.add(new ToolTipDataRow(Language.getString("BT_AVG_TRADE_LEN"), avgL + ""));
        rowsUnProfTiming.add(new ToolTipDataRow(Language.getString("BT_LONG_LEN"), hiL + ""));
        rowsUnProfTiming.add(new ToolTipDataRow(Language.getString("BT_SHORT_LEN"), loL + ""));
        rowsUnProfTiming.add(new ToolTipDataRow(Language.getString("BT_TOTAL_TRADE"), totalL + ""));

        CustomPanel cusUnProfTime = new CustomPanel(Language.getString("BT_UNPRFOT_TIME"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusUnProfTime.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusUnProfTime.add(new JLabel());
        cusUnProfTime.add(new TablePanel(rowsProfTiming));
        pnlRight.add(cusUnProfTime);
        /************************************end of Un profitable timeing *****************************/

        //*************************out of market timing *************************/
        ArrayList<ToolTipDataRow> rowOOMTiming = new ArrayList<ToolTipDataRow>(3);

        boolean isOOM = false;
        int oomCount = 0, oomTotal = 0, oomHi = 0, lastInMktbar = 0;
        for (int i = 0; i < result.getEquityDetailRecords().size(); i++) {
            EquityDetailRecord er = result.getEquityDetailRecords().get(i);
            if (er.getQuantity() > 0) {
                if (isOOM) { // just came into the market
                    isOOM = false;
                    int period = i - lastInMktbar;
                    oomTotal += period;
                    oomHi = Math.max(oomHi, period);
                }
                lastInMktbar = i + 1;
            } else {
                if (!isOOM) {
                    oomCount++;
                    isOOM = true;
                }
            }
        }
        if (isOOM) {
            int period = result.getEquityDetailRecords().size() - lastInMktbar;
            oomTotal += period;
            oomHi = Math.max(oomHi, period);
        }
        int oomAvg = (oomCount > 0) ? oomTotal / oomCount : 0;

        rowOOMTiming.add(new ToolTipDataRow(Language.getString("BT_AVERAGE"), oomAvg + ""));
        rowOOMTiming.add(new ToolTipDataRow(Language.getString("BT_LONGEST"), oomHi + ""));
        rowOOMTiming.add(new ToolTipDataRow(Language.getString("BT_TOTAL"), oomTotal + ""));

        CustomPanel cusoutMarket = new CustomPanel(Language.getString("BT_OUT_TIME"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusoutMarket.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusoutMarket.add(new JLabel());
        cusoutMarket.add(new TablePanel(rowOOMTiming));
        pnlRight.add(cusoutMarket);
        //*************************end of un profitable timing *****************************/

        return pnlRight;
    }

    private JPanel createLeftGeneralPanel() {

        JPanel pnlLeft = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100", "80", "180", "180", "100"}, 0, 5));

        /*******************performance table****************************************/
        ArrayList<ToolTipDataRow> performaneRows = new ArrayList<ToolTipDataRow>(3);
        double dProfit = result.getTotalEquity() - simData.getInitialEquity();
        performaneRows.add(new ToolTipDataRow(Language.getString("BT_PROFIT"), priceFormatter.format(dProfit)));

        double dPerformance = 100 * dProfit / simData.getInitialEquity();
        performaneRows.add(new ToolTipDataRow(Language.getString("BT_PER"), pctgFormatter.format(dPerformance) + "%"));

        double dBHProfit = getBHProfit();
        double BHPerformance;
        if (dBHProfit == 0) {
            BHPerformance = 0d;
        } else {
            BHPerformance = 100 * dBHProfit / result.getInitialEquity();
        }
        performaneRows.add(new ToolTipDataRow(Language.getString("BT_BUY_AND_HOLD"), priceFormatter.format(dBHProfit) + ""));
        performaneRows.add(new ToolTipDataRow(Language.getString("BT_BUY_AND_HOLD_PER"), priceFormatter.format(BHPerformance) + ""));

        CustomPanel cusPerformance = new CustomPanel(Language.getString("BT_PER"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusPerformance.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusPerformance.add(new JLabel());
        cusPerformance.add(new TablePanel(performaneRows));
        pnlLeft.add(cusPerformance);
        /*******************end of performance table****************************************/


        /*******************Trade Summary****************************************/
        ArrayList<ToolTipDataRow> rowsTrade = new ArrayList<ToolTipDataRow>(3);

        rowsTrade.add(new ToolTipDataRow(Language.getString("BT_TOTAL_TRADES"), result.getTradeCount() + ""));
        rowsTrade.add(new ToolTipDataRow(Language.getString("BT_TRADE_EFF"), "N/A"));
        double avgPoverL = result.getAverageProfit() / result.getAverageLoss();

        TWDecimalFormat formatter = new TWDecimalFormat("########0.00");
        rowsTrade.add(new ToolTipDataRow(Language.getString("BT_APL"), (result.getAverageLoss() == 0) ? "N/A" : formatter.format(avgPoverL)));

        CustomPanel cusSummary = new CustomPanel(Language.getString("BT_TRADE_SUM"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusSummary.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusSummary.add(new JLabel());
        cusSummary.add(new TablePanel(rowsTrade));
        pnlLeft.add(cusSummary);
        /*******************end of Trade Summary****************************************/

        /*********************************** profitable trades ************************************/
        ArrayList<ToolTipDataRow> rowsProfitableTrades = new ArrayList<ToolTipDataRow>(7);
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_TOTAL"), result.getProfitableTrades() + ""));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_LONG"), result.getProfitableTrades() + ""));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_SHORT"), "0"));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_AVG_PROFIT"), "0.00"));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_HIGH_PROFIT"), "0.00"));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_LOW_PROFIT"), "0.00"));
        rowsProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_CONS"), result.getMostConsecutivePTs() + ""));

        CustomPanel cusTrades = new CustomPanel(Language.getString("BT_PROFIT_TRADES"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusTrades.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusTrades.add(new JLabel());
        cusTrades.add(new TablePanel(rowsProfitableTrades));
        pnlLeft.add(cusTrades);
        /*********************************** profitable trades ************************************/


        /*******************unprofitable trades****************************************/
        ArrayList<ToolTipDataRow> rowsUnProfitableTrades = new ArrayList<ToolTipDataRow>(7);
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_MAX_POS"), result.getUnprofitableTrades() + ""));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_LONG"), result.getUnprofitableTrades() + ""));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_SHORT"), "0"));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_AVG_LOSS"), "0.00"));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_HIGH_LOSS"), "0.00"));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_LOW_LOSS"), "0.00"));
        rowsUnProfitableTrades.add(new ToolTipDataRow(Language.getString("BT_CONS"), result.getMostConsecutiveUTs() + ""));

        CustomPanel cusUnTrades = new CustomPanel(Language.getString("BT_UNPOFIT_TRADES"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusUnTrades.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusUnTrades.add(new JLabel());
        cusUnTrades.add(new TablePanel(rowsUnProfitableTrades));
        pnlLeft.add(cusUnTrades);
        /*******************end of  unprofitable trades****************************************/

        /*******************excursions ****************************************/
        ArrayList<ToolTipDataRow> rowExcuriosns = new ArrayList<ToolTipDataRow>(4);
        rowExcuriosns.add(new ToolTipDataRow(Language.getString("BT_LONG_FAV"), "N/A"));
        rowExcuriosns.add(new ToolTipDataRow(Language.getString("BT_SHORT_FAV"), "N/A"));
        rowExcuriosns.add(new ToolTipDataRow(Language.getString("BT_LONG_ADVERSE"), "N/A"));
        rowExcuriosns.add(new ToolTipDataRow(Language.getString("BT_SHORT_ADVERSE"), "N/A"));

        CustomPanel cusPos = new CustomPanel(Language.getString("BT_MAX_POS"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusPos.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusPos.add(new JLabel());
        cusPos.add(new TablePanel(rowExcuriosns));
        pnlLeft.add(cusPos);
        /*******************end of  excursions ****************************************/

        return pnlLeft;
    }

    private double getBHProfit() {

        int count = result.getEquityDetailRecords().size();
        double BHProfit = -Double.MAX_VALUE;
        if (count > 0) {
            int actualStocks = (int) Math.floor(result.getInitialEquity() / result.getEquityDetailRecords().get(0).getPrice());
            double cashInHand = result.getInitialEquity() - actualStocks * result.getEquityDetailRecords().get(0).getPrice();
            int days = (int) ((result.getEquityDetailRecords().get(count - 1).getTime() - result.getEquityDetailRecords().get(0).getTime()) / 86400000L);
            cashInHand = cashInHand * Math.pow(simData.getDailyInterestRate(), days);
            double finalEquity = result.getEquityDetailRecords().get(count - 1).getPrice() * actualStocks + cashInHand;
            return BHProfit = (finalEquity - result.getInitialEquity());

        } else {
            return 0;
        }
    }

    private JPanel createOrdersPanel() {

        /*************************************************/
        int total = 0, executed = 0, cancelled = 0;
        for (TransactionPoint txnPt : result.getTransactions()) {
            switch (txnPt.getTransactionResult()) {
                case BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED:
                case BackTestingConstatns.TRANSACTION_RESULT_PARTIALLY_EXECUTED:
                    total++;
                    executed++;
                    break;
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_CASH:
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_STOCKS:
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_POSITION_LIMIT:
                    total++;
                    cancelled++;
                    break;
                case BackTestingConstatns.TRANSACTION_RESULT_HOLD_TILL_NEXTBAR:
                    total++;
                    break;
            }
        }
        /*************************************************/

        ArrayList<ToolTipDataRow> rows8 = new ArrayList<ToolTipDataRow>(4);
        rows8.add(new ToolTipDataRow(Language.getString("BT_TOTAL"), total + ""));
        rows8.add(new ToolTipDataRow(Language.getString("BT_CONSIDERED"), total + ""));
        rows8.add(new ToolTipDataRow(Language.getString("BT_EXECUTED"), executed + ""));
        rows8.add(new ToolTipDataRow(Language.getString("BT_CANCELLED"), cancelled + ""));

        JPanel orderPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "72"}, 0, 2));
        JPanel pnlUp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 2));

        CustomPanel cusOrderDetails = new CustomPanel(Language.getString("BT_ORDER_DETAILS"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusOrderDetails.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusOrderDetails.add(new JLabel());
        cusOrderDetails.add(createOrderDetailTable());

        pnlUp.add(cusOrderDetails);

        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 2));

        CustomPanel cusOrderSummary = new CustomPanel(Language.getString("BT_ORDER_SUMMARY"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusOrderSummary.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusOrderSummary.add(new JLabel());
        cusOrderSummary.add(new TablePanel2(rows8));

        pnlBottom.add(cusOrderSummary);

        orderPanel.add(pnlUp);
        orderPanel.add(pnlBottom);

        return orderPanel;
    }

    private JPanel createEquityPanel() {

        createSimplaeChart();
        JPanel pnlEquity = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 2));
        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"50%", "50%"}, 0, 5));

        CustomPanel cusEquity = new CustomPanel(Language.getString("BT_EQ_O_TIME"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusEquity.setLayout(new FlexGridLayout(new String[]{"5%", "90%", "5%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusEquity.add(new JLabel());
        cusEquity.add(new JLabel());
        cusEquity.add(new JLabel());
        cusEquity.add(new JLabel());
        cusEquity.add(chart);
        cusEquity.add(new JLabel());

        pnlBottom.add(cusEquity);

        CustomPanel cusEquityDetails = new CustomPanel(Language.getString("BT_EQUITY_DETAILS"), CustomPanel.TITLE_ALIGNMENT_CENNTER);
        cusEquityDetails.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{String.valueOf(CustomPanel.TITLE_BORDER_HEIGHT + 5), "95%"}, 5, 0));
        cusEquityDetails.add(new JLabel());
        cusEquityDetails.add(createEquityTable());

        pnlBottom.add(cusEquityDetails);

        pnlEquity.add(pnlBottom);

        return pnlEquity;
    }

    private void createSimplaeChart() {

        ChartSeries series1 = new ChartSeries(Language.getString("BT_PORTFOLIO"), result.getEquityDetailRecords().size(), Theme.getColor("SIMPLE_CHART_PORTFOLIO_COLOR"));
        ChartSeries series2 = new ChartSeries(Language.getString("BT_CASH"), result.getEquityDetailRecords().size(), Theme.getColor("SIMPLE_CHART_CASH_COLOR"));

        for (int i = 0; i < result.getEquityDetailRecords().size(); i++) {
            EquityDetailRecord er = result.getEquityDetailRecords().get(i);
            series1.XData[i] = er.getBarID();
            series1.YData[i] = er.getTotal();
            series2.XData[i] = er.getBarID();
            series2.YData[i] = er.getCash();
        }
        chart.addSeries(series1);
        chart.addSeries(series2);
    }

    private Table createOrderDetailTable() {

        //*************************Orders Tab*****************************
        int count = 1;
        ArrayList<OrderDetailRecord> OrdDetailRecordsArr = new ArrayList<OrderDetailRecord>();
        for (TransactionPoint txnPt : (ArrayList<TransactionPoint>) result.getTransactions()) {

            if (txnPt.getTransactionResult() == BackTestingConstatns.TRANSACTION_RESULT_NONE) continue;
            //signal
            String pos = ((txnPt.getTransactionType() == BackTestingConstatns.LOGIC_TYPE_SELL) && (txnPt.getTransactionResult() == BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED)) ? txnPt.getPositionString() : "";
            OrdDetailRecordsArr.add(new OrderDetailRecord(txnPt.getSignalPos(), dateFormatter.format(txnPt.getSignalDate()),
                    count, Language.getString("BT_CONSIDER"), txnPt.getTXNType(), txnPt.getTransactionSize(), "", pos, txnPt.getSource()));

            //execution
            // this checks a buy or sell signal on last bar
            if ((txnPt.getTransactionType() != BackTestingConstatns.LOGIC_TYPE_LAST_BAR) && (txnPt.getNextRecord() == null))
                continue;

            switch (txnPt.getTransactionResult()) {
                case BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED:
                case BackTestingConstatns.TRANSACTION_RESULT_PARTIALLY_EXECUTED:
                    OrdDetailRecordsArr.add(new OrderDetailRecord(txnPt.getExecutionPos(), dateFormatter.format(txnPt.getExecutionDate()),
                            count, Language.getString("BT_EXCEC"), txnPt.getTXNType(), txnPt.getTransactionSize(), priceFormatter.format(txnPt.getPrice()), txnPt.getPositionString(), txnPt.getSource()));

                    break;
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_CASH:
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_STOCKS:
                case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_POSITION_LIMIT:
                    OrdDetailRecordsArr.add(new OrderDetailRecord(txnPt.getExecutionPos(), dateFormatter.format(txnPt.getExecutionDate()),
                            count, txnPt.getResult(), txnPt.getTXNType(), 1000, "", "", txnPt.getSource()));//todo

                    break;
            }
            count++;
        }

        ViewSetting settings = null;
        try {
            settings = ViewSettingsManager.getSummaryView("ORDER_DETAIL_COLS");
            GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("ORDER_DETAIL_COLS"));

            if (settings == null)
                throw (new Exception("View not found"));

            orderDetailTable = new Table();
            OrderDetailsTableModel model = new OrderDetailsTableModel(OrdDetailRecordsArr);
            model.setViewSettings(settings);
            orderDetailTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            orderDetailTable.setModel(model);
            model.setTable(orderDetailTable, new OrderDetailsTableRenderer(model.getRendIDs()));
            ((SmartTable) orderDetailTable.getTable()).adjustColumnWidthsToFit(40);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("**************** order details table *********************");
        }

        return orderDetailTable;
    }

    private Table createEquityTable() {

        ViewSetting settings = null;
        try {
            settings = ViewSettingsManager.getSummaryView("EQUITY_DETAIL_COLS");
            GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("EQUITY_DETAIL_COLS"));

            if (settings == null)
                throw (new Exception("View not found"));

            equityTable = new Table();
            EquityDetailsTableModel model = new EquityDetailsTableModel(result.getEquityDetailRecords());
            model.setViewSettings(settings);
            equityTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            equityTable.setModel(model);
            model.setTable(equityTable, new EquityDetailsTableRenederer(model.getRendIDs()));
            ((SmartTable) equityTable.getTable()).adjustColumnWidthsToFit(40);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("**************** equity details table *********************");
        }

        return equityTable;
    }
}