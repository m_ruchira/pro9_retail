package com.isi.csvr.chart.backtesting;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 23, 2008
 * Time: 12:27:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingSymbolRenderer extends TWBasicTableRenderer {

    static Border selectedBorder;
    static Border unselectedBorder;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private Color foreground = null;
    private Color background = null;
    private String g_sNA = "NA";
    private int g_iCenterAlign;
    private int[] g_asRendIDs;
    private TWDecimalFormat oQuantityFormat;
    private Double floatValue;
    private TWDecimalFormat oPriceFormat;
    private int g_iNumberAlign;
    private int g_iStringAlign;
    private long longValue;
    private ImageIcon chartImage;
    private String chartToolTip;
    private TWDateFormat g_oDateTimeFormatHM;
    private Date date;
    private JCheckBox checkBoxRenderer;

    public BackTestingSymbolRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        g_iCenterAlign = JLabel.CENTER;
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        g_iNumberAlign = JLabel.RIGHT;
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;

        try {
            chartImage = new ImageIcon("images/Theme" + Theme.getID() + "/scanChart.gif");
        } catch (Exception e) {
            chartImage = null;
        }
        chartToolTip = Language.getString("SYMBOL_PANEL_CHART");
        checkBoxRenderer = new JCheckBox();
        checkBoxRenderer.setOpaque(true);
        reload();
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            selectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oFG1);
        } catch (Exception e) {
            g_oSelectedFG = Color.green;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
    }

    public void propertyChanged(int property) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));

        date = new Date();
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;

            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;

            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }
        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        lblRenderer.setIcon(null);
        lblRenderer.setToolTipText("");

        switch (iRendID) {

            case 0: // Symbol
                lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;
            case 1: //Exchange
                lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;
            case 2: //Company
                lblRenderer.setText((String) value);
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;

            default:
                lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }
}
