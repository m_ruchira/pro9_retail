package com.isi.csvr.chart.backtesting;

import com.isi.csvr.scanner.Scans.ScanRecord;
import com.isi.csvr.shared.Language;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 25, 2008
 * Time: 8:46:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class TransactionPoint extends ScanRecord implements Comparator {

    private ScanRecord currentRecord = null;
    private ScanRecord nextRecord = null;

    private int transactionType = BackTestingConstatns.LOGIC_TYPE_BUY;
    private int transactionResult = BackTestingConstatns.TRANSACTION_RESULT_NONE;

    private int transactionSize = 0;
    private int signalPos = 0;
    private int executionPos = 0;

    private String positionString = "";

    private int stocksInHand;
    private double cashInHand;

    public TransactionPoint(long time, int trnType) {
        super();
        this.Time = time;
        this.transactionType = trnType;
    }

    public void setTransactionDetail(int size, int stocks, double cash, int txnResult, int signalPos, int execPos, String posString) {
        this.transactionSize = size;
        this.stocksInHand = stocks;
        this.cashInHand = cash;
        this.transactionResult = txnResult;
        this.signalPos = signalPos;
        this.executionPos = execPos;
        this.positionString = posString;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public int getTransactionSize() {
        return transactionSize;
    }

    public void setTransactionSize(int transactionSize) {
        this.transactionSize = transactionSize;
    }

    public int getSignalPos() {
        return signalPos;
    }

    public void setSignalPos(int signalPos) {
        this.signalPos = signalPos;
    }

    public int getExecutionPos() {
        return executionPos;
    }

    public void setExecutionPos(int executionPos) {
        this.executionPos = executionPos;
    }

    public String getPositionString() {
        return positionString;
    }

    public void setPositionString(String positionString) {
        this.positionString = positionString;
    }

    public long getSignalDate() {    //TODO :Whats better to be decide
        return currentRecord.getTime() * 60000L;
    }

    public long getExecutionDate() {  //TODO :Whats better to be decide

        if (nextRecord != null) {
            return nextRecord.getTime() * 60000L;
        } else {
            return currentRecord.getTime() * 60000L;
        }
    }

    public int getStocksInHand() {
        return stocksInHand;
    }

    public void setStocksInHand(int stocksInHand) {
        this.stocksInHand = stocksInHand;
    }

    public double getCashInHand() {
        return cashInHand;
    }

    public void setCashInHand(double cashInHand) {
        this.cashInHand = cashInHand;
    }

    public double getPrice() {
        if (nextRecord != null) {
            return nextRecord.getOpen();
        } else {
            return currentRecord.getClose();
        }
    }

    public String getSource() {
        if (this.transactionType == BackTestingConstatns.LOGIC_TYPE_LAST_BAR) {
            return Language.getString("BT_LAST_BAR");
        } else {
            return Language.getString("BT_SIGNAL");
        }
    }

    public String getTXNType() {

        switch (this.transactionType) {

            case BackTestingConstatns.LOGIC_TYPE_BUY:
                return Language.getString("BT_BUY2");

            case BackTestingConstatns.LOGIC_TYPE_SELL:
                return Language.getString("BT_SELL2");

            case BackTestingConstatns.LOGIC_TYPE_SELL_SHORT:
                return Language.getString("BT_SELL_SHORT");

            case BackTestingConstatns.LOGIC_TYPE_BUY_TOCOVER:
                return Language.getString("BT_BUY_TO_COVER");

            default:
                return Language.getString("BT_SELL_LAST");
        }
    }

    public String getResult() {

        switch (this.transactionResult) {

            case BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED:
            case BackTestingConstatns.TRANSACTION_RESULT_PARTIALLY_EXECUTED:
                return Language.getString("BT_EXECUTED");

            case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_CASH:
                return Language.getString("BT_OUT_OF_CASH");

            case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_STOCKS:
                return Language.getString("BT_OUT_OF_STOCK");

            case BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_POSITION_LIMIT:
                return Language.getString("BT_OUT_OF_POS_LIMIT");

            case BackTestingConstatns.TRANSACTION_RESULT_HOLD_TILL_NEXTBAR:
            default:
                return "";
        }
    }

    public ScanRecord getCurrentRecord() {
        return currentRecord;
    }

    public void setCurrentRecord(ScanRecord currentRecord) {
        this.currentRecord = currentRecord;
    }

    public ScanRecord getNextRecord() {
        return nextRecord;
    }

    public void setNextRecord(ScanRecord nextRecord) {
        this.nextRecord = nextRecord;
    }


    public int compare(Object x, Object y) {
        long t1 = ((ScanRecord) x).Time;
        long t2 = ((ScanRecord) y).Time;
        return (t1 > t2) ? 1 : (t1 < t2) ? -1 : 0;
    }

    public int getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(int transactionResult) {
        this.transactionResult = transactionResult;
    }
}
