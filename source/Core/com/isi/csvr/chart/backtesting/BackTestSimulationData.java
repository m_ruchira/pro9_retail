package com.isi.csvr.chart.backtesting;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Nov 19, 2008
 * Time: 10:29:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestSimulationData {

    private String simulationName;
    private long simulationID = System.currentTimeMillis();
    private int defaultSize = 100;
    private int defaultType = BackTestingConstatns.BACKTEST_ENTRY_TYPE_DEFAULT_SIZE;
    private long[] systems;
    private double initialEquity = 10000;
    private double dailyInterestRate = 1.00008;
    private String[] securities;
    private ArrayList<BackTestResult> results = new ArrayList<BackTestResult>();

    public BackTestSimulationData(long simID, String simName) {
        this.simulationID = simID;
        this.simulationName = simName;
    }

    public BackTestSimulationData(long simulationID, String simulationName, int defaultSize, int defaultType, double initialEquity, double dailyInterestRate, long[] systems,
                                  String[] securities) {
        this.simulationName = simulationName;
        this.simulationID = simulationID;
        this.defaultSize = defaultSize;
        this.defaultType = defaultType;
        this.systems = systems;
        this.initialEquity = initialEquity;
        this.dailyInterestRate = dailyInterestRate;
        this.securities = securities;
    }

    public String getSimulationName() {
        return simulationName;
    }

    public void setSimulationName(String simulationName) {
        this.simulationName = simulationName;
    }

    public long getSimulationID() {
        return simulationID;
    }

    public void setSimulationID(long simulationID) {
        this.simulationID = simulationID;
    }

    public int getDefaultSize() {
        return defaultSize;
    }

    public void setDefaultSize(int defaultSize) {
        this.defaultSize = defaultSize;
    }

    public int getDefaultType() {
        return defaultType;
    }

    public void setDefaultType(int defaultType) {
        this.defaultType = defaultType;
    }

    public long[] getSystems() {
        return systems;
    }

    public void setSystems(long[] systems) {
        this.systems = systems;
    }

    public double getInitialEquity() {
        return initialEquity;
    }

    public void setInitialEquity(double initialEquity) {
        this.initialEquity = initialEquity;
    }

    public double getDailyInterestRate() {
        return dailyInterestRate;
    }

    public void setDailyInterestRate(double dailyInterestRate) {
        this.dailyInterestRate = dailyInterestRate;
    }

    public String[] getSecurities() {
        return securities;
    }

    public void setSecurities(String[] securities) {
        this.securities = securities;
    }

    public ArrayList<BackTestResult> getResults() {
        return results;
    }

    public void setResults(ArrayList<BackTestResult> results) {
        this.results = results;
    }

    public void assignValuesFrom(BackTestSimulationData sim) {
        this.simulationName = sim.simulationName;
        this.defaultSize = sim.getDefaultSize();
        this.defaultType = sim.getDefaultType();
        this.systems = sim.systems;
        this.initialEquity = sim.initialEquity;
        this.dailyInterestRate = sim.dailyInterestRate;
        this.securities = sim.securities;
    }


}
