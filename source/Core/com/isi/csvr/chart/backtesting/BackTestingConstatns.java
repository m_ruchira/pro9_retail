package com.isi.csvr.chart.backtesting;

import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 1:30:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingConstatns {

    //backtest entry types
    protected static final int BACKTEST_ENTRY_TYPE_DEFAULT_SIZE = 0;
    protected static final int BACKTEST_ENTRY_TYPE_NUMBER_OF_UNITS = 1;
    protected static final int BACKTEST_ENTRY_TYPE_TRANSACTION_COST = 2;
    protected static final int BACKTEST_ENTRY_TYPE_PERCENTAGE_OF_AVAULABLE_EQUITY = 3;

    //backtest logic types
    protected static final int LOGIC_TYPE_BUY = 0;
    protected static final int LOGIC_TYPE_SELL = 1;
    protected static final int LOGIC_TYPE_SELL_SHORT = 2;
    protected static final int LOGIC_TYPE_BUY_TOCOVER = 3;
    protected static final int LOGIC_TYPE_LAST_BAR = 9999;

    //transaction result types
    protected static final int TRANSACTION_RESULT_NONE = 0; // not a transaction
    protected static final int TRANSACTION_RESULT_EXECEUTED = 1;
    protected static final int TRANSACTION_RESULT_PARTIALLY_EXECUTED = 2;  //not enough cash
    protected static final int TRANSACTION_RESULT_CANCELLED_POSITION_LIMIT = 3;
    protected static final int TRANSACTION_RESULT_CANCELLED_NO_CASH = 4;
    protected static final int TRANSACTION_RESULT_CANCELLED_NO_STOCKS = 5;
    protected static final int TRANSACTION_RESULT_HOLD_TILL_NEXTBAR = 6;

    //file paths
    protected static final String BACK_TEST_SIMULATIONS_FILE_PATH = "./Charts/sim.xml";
    protected static final String BACK_TEST_SIMULATIONS_TEMP_FILE_PATH = "./Charts/sim_temp.xml";
    protected static final String BACK_TEST_SYSTEMS_FILE_PATH = "./Charts/sys.xml";
    protected static final String BACK_TEST_SYSTEMS_TEMP_FILE_PATH = "./Charts/sys_temp.xml";

    protected static final String BACK_TEST_CLASS_PATH = Settings.getAbsolutepath() + "CustomIndicators/";

    protected static final String BT_ENCRYPTION_PATH = "./Charts/bt_aes.dll";

}
