package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 23, 2008
 * Time: 8:52:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class Key {

    private String symbol;
    private String exchange;
    private int instrumentType;
    private String company;

    public Key(String symbol, String exchange, int insType, String company) {
        this.symbol = symbol;
        this.exchange = exchange;
        this.instrumentType = insType;
        this.company = company;
    }

    public Key(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public String getCompany() {
        return company;
    }

    public int getInstrumentType() {
        return instrumentType;
    }


    public String toString() {

        return this.symbol + " (" + this.company + ")";
    }
}
