/*
package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.options.AESFileEncryptor;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

*/
/**
 * Created by IntelliJ IDEA.
 * User: Charith Nidarsha
 * Date: Nov 19, 2008
 * Time: 11:20:49 AM
 * To change this template use File | Settings | File Templates.
 */
/*
public class BackTestingXmlService {

    //systems related tags
    public static final String ROOT_ELEMENT_SYSTEMS = "BackTestingSystems";
    public static final String ROOT_ELEMENT_SYSTEM = "System";

    public static final String SYSTEM_ID = "SystemID";
    public static final String SYSTEM_NAME = "SystemName";
    public static final String SYSTEM_DESCRIPTION = "SystemDescription";
    public static final String GENERAL_POSITIONS = "GeneralPositions";
    public static final String BUY_GRAMMAR = "BuyGrammar";
    public static final String BUY_QUANTITY = "BuyQuantity";
    public static final String BUY_TYPE = "BuyType";
    public static final String SELL_GRAMMAR = "SellGrammar";

    //simulation related tags
    public static final String ROOT_ELEMENT_SIMULATIONS = "Simulations";
    public static final String ROOT_ELEMENT_SIMULATION = "Simulation";

    public static final String SIMULATION_NAME = "SimulationName";
    public static final String SIMULATION_ID = "SimulationID";
    public static final String DEFAULT_TYPE = "DefaultType";
    public static final String DEFAULT_SIZE = "DefaultSize";
    public static final String INITIAL_EQUITY = "InitialEquity";
    public static final String DAILY_INTREST = "DailyIntrestRate";
    public static final String SECURITIES = "Securities";
    public static final String SYSTEMS = "Systems";

    private static boolean isEncrypted = false;

    public static synchronized void serializeBackSysetms(ArrayList<BackTestSystemData> systems) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_SYSTEMS);
            doc.appendChild(root);

            String strText = "";

            for (BackTestSystemData system : systems) {

                Element eleSystem = doc.createElement(ROOT_ELEMENT_SYSTEM);
                root.appendChild(eleSystem);

                Element eleID = doc.createElement(SYSTEM_ID);
                strText = String.valueOf(system.getSystemID());
                Text text = doc.createTextNode(strText);
                eleID.appendChild(text);
                eleSystem.appendChild(eleID);

                Element eleName = doc.createElement(SYSTEM_NAME);
                strText = system.getSystemName();
                text = doc.createTextNode(strText);
                eleName.appendChild(text);
                eleSystem.appendChild(eleName);

                Element eleDes = doc.createElement(SYSTEM_DESCRIPTION);
                CDATASection cdata = doc.createCDATASection(system.getSystemDescription());
                eleDes.appendChild(cdata);
                eleSystem.appendChild(eleDes);

                Element elePos = doc.createElement(GENERAL_POSITIONS);
                strText = String.valueOf(system.getGeneralPositions());
                text = doc.createTextNode(strText);
                elePos.appendChild(text);
                eleSystem.appendChild(elePos);

                Element eleBuyGram = doc.createElement(BUY_GRAMMAR);
                cdata = doc.createCDATASection(system.getBuyGrammar());
                eleBuyGram.appendChild(cdata);
                eleSystem.appendChild(eleBuyGram);

                Element eleQty = doc.createElement(BUY_QUANTITY);
                strText = String.valueOf(system.getBuyQuantitiy());
                text = doc.createTextNode(strText);
                eleQty.appendChild(text);
                eleSystem.appendChild(eleQty);

                Element eleType = doc.createElement(BUY_TYPE);
                strText = String.valueOf(system.getBuyType());
                text = doc.createTextNode(strText);
                eleType.appendChild(text);
                eleSystem.appendChild(eleType);

                Element eleSellGram = doc.createElement(SELL_GRAMMAR);
                cdata = doc.createCDATASection(system.getSellGrammar());
                eleSellGram.appendChild(cdata);
                eleSystem.appendChild(eleSellGram);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            if (isEncrypted) {
                // Encrypt
                FileInputStream inStream = new FileInputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);
                encrypter.encryptFile(inStream, new FileOutputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH));
                inStream.close();
                deleteTempFile(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== BackTestingXmlService class- serializeBackSysetms() ======");
        }
    }

    public static synchronized ArrayList<BackTestSystemData> desirializeSystems() {

        ArrayList<BackTestSystemData> systems = new ArrayList<BackTestSystemData>();

        File file = null;
        if (isEncrypted) {
            file = new File(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH);
        } else {
            file = new File(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
        }
        if (!file.exists()) {
            return systems;
        }

        try {
            if (isEncrypted) {
                // Decrypt
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);

                FileOutputStream stream = new FileOutputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                encrypter.decryptFile(new FileInputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH), stream);

                stream.close();
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            //Document dom = docBuilder.parse(tempPath);

            NodeList list = dom.getElementsByTagName(ROOT_ELEMENT_SYSTEM);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlID = elem.getElementsByTagName(SYSTEM_ID);
                NodeList nlName = elem.getElementsByTagName(SYSTEM_NAME);
                NodeList nlDes = elem.getElementsByTagName(SYSTEM_DESCRIPTION);
                NodeList nlPos = elem.getElementsByTagName(GENERAL_POSITIONS);
                NodeList nlBuyGram = elem.getElementsByTagName(BUY_GRAMMAR);
                NodeList nlBuyQt = elem.getElementsByTagName(BUY_QUANTITY);
                NodeList nlBuyType = elem.getElementsByTagName(BUY_TYPE);
                NodeList nlSellGram = elem.getElementsByTagName(SELL_GRAMMAR);

                long id = Long.parseLong(nlID.item(0).getTextContent());
                String name = nlName.item(0).getTextContent();
                String des = nlDes.item(0).getTextContent();
                int pos = Integer.parseInt(nlPos.item(0).getTextContent());
                String buyGram = nlBuyGram.item(0).getTextContent();
                int buyQt = Integer.parseInt(nlBuyQt.item(0).getTextContent());
                int buyType = Integer.parseInt(nlBuyType.item(0).getTextContent());
                String SellGram = nlSellGram.item(0).getTextContent();
                BackTestSystemData item = new BackTestSystemData(id, name, des, pos, buyGram, buyQt, buyType, SellGram);
                systems.add(item);
            }
            if (isEncrypted) {
                deleteTempFile(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            }
        }

        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("=== XMLSerializationService class-deserializeCustomDataItems() ======");
        }

        return systems;
    }

    public static void updateSystemBySystemID(BackTestSystemData sys, boolean shouldDelete) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData system = systems.get(i);

            if (system.getSystemID() == sys.getSystemID()) {
                if (shouldDelete) {
                    systems.remove(i);
                } else {
                    system.assignValuesFrom(sys);
                }
                break;
            }
        }

        try {
            serializeBackSysetms(systems);
        }

        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update systems **************");
        }

    }

    public static void updateSimulationBySimulationID(BackTestSimulationData sim, boolean shouldDelete) {

        ArrayList<BackTestSimulationData> sims = deserializeSimulations();

        for (int i = 0; i < sims.size(); i++) {
            BackTestSimulationData s = sims.get(i);

            if (s.getSimulationID() == sim.getSimulationID()) {
                if (shouldDelete) {
                    sims.remove(i);
                } else {
                    s.assignValuesFrom(sim);
                }
                break;
            }
        }

        try {
            serializeSimulations(sims);
        }

        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update simulations **************");
        }

    }

    public static void deleteSystemBySystemID(BackTestSystemData sys) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData system = systems.get(i);

            if (system.getSystemID() == sys.getSystemID()) {
                system.assignValuesFrom(sys);
                break;
            }
        }

        try {
            serializeBackSysetms(systems);
        }

        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update systems **************");
        }

    }

    private static synchronized void deleteTempFile(String filePath) {

        File f1 = new File(filePath);

        try {
            if (f1.exists()) {
                //f1.delete();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("failure deletion of tempory file..");
        }
    }

    public static synchronized void serializeSimulations(ArrayList<BackTestSimulationData> simulations) {
        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_SIMULATIONS);
            doc.appendChild(root);

            //add general settings to xml file
            for (BackTestSimulationData item : simulations) {
                Element eleBTSimulation = doc.createElement(ROOT_ELEMENT_SIMULATION);
                root.appendChild(eleBTSimulation);

                //ID
                Element eleID = doc.createElement(SIMULATION_ID);
                Text textID = doc.createTextNode(Long.toString(item.getSimulationID()));
                eleBTSimulation.appendChild(eleID);
                eleID.appendChild(textID);

                //NAME
                Element eleName = doc.createElement(SIMULATION_NAME);
                Text textName = doc.createTextNode(item.getSimulationName());
                eleBTSimulation.appendChild(eleName);
                eleName.appendChild(textName);

                Element eleType = doc.createElement(DEFAULT_TYPE);
                Text textType = doc.createTextNode(String.valueOf(item.getDefaultType()));
                eleBTSimulation.appendChild(eleType);
                eleType.appendChild(textType);

                Element eleSize = doc.createElement(DEFAULT_SIZE);
                Text textSize = doc.createTextNode(String.valueOf(item.getDefaultSize()));
                eleBTSimulation.appendChild(eleSize);
                eleSize.appendChild(textSize);

                Element eleQt = doc.createElement(INITIAL_EQUITY);
                Text textQt = doc.createTextNode(String.valueOf(item.getInitialEquity()));
                eleBTSimulation.appendChild(eleQt);
                eleQt.appendChild(textQt);

                Element eleIntr = doc.createElement(DAILY_INTREST);
                Text textint = doc.createTextNode(String.valueOf(item.getDailyInterestRate()));
                eleBTSimulation.appendChild(eleIntr);
                eleIntr.appendChild(textint);

                Element eleSec = doc.createElement(SECURITIES);

                String secs = "";
                for (int i = 0; i < item.getSecurities().length; i++) {
                    secs += item.getSecurities()[i] + ",";
                }
                secs = secs.substring(0, secs.length() - 1);
                CDATASection cDSec = doc.createCDATASection(secs);
                eleSec.appendChild(cDSec);
                eleBTSimulation.appendChild(eleSec);

                Element eleSys = doc.createElement(SYSTEMS);

                String syss = "";
                for (int i = 0; i < item.getSystems().length; i++) {
                    syss += item.getSystems()[i] + ",";
                }
                syss = syss.substring(0, syss.length() - 1);
                CDATASection cDSys = doc.createCDATASection(syss);
                eleSys.appendChild(cDSys);
                eleBTSimulation.appendChild(eleSys);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            if (isEncrypted) {
                // Encrypt
                FileInputStream inStream = new FileInputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);

                encrypter.encryptFile(inStream, new FileOutputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH));
                inStream.close();
                deleteTempFile(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== XMLSerializationService class-serializeBackTestingDataObjects() ======");
        }
    }

    public static synchronized ArrayList<BackTestSimulationData> deserializeSimulations() {

        ArrayList<BackTestSimulationData> bTDataItems = new ArrayList<BackTestSimulationData>();

        File file = null;
        if(isEncrypted){
             file = new File(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH);
        }else{
             file = new File(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
        }
        if (!file.exists()) {
            return bTDataItems;
        }
        try {
            if (isEncrypted) {
                // Decrypt
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);
                FileOutputStream stream = new FileOutputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
                encrypter.decryptFile(new FileInputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH), stream);
                stream.close();
            }

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
            Document dom = docBuilder.parse(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);

            NodeList list = dom.getElementsByTagName(ROOT_ELEMENT_SIMULATION);
            int count = list.getLength();
            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlID = elem.getElementsByTagName(SIMULATION_ID);
                NodeList nlName = elem.getElementsByTagName(SIMULATION_NAME);
                NodeList nlType = elem.getElementsByTagName(DEFAULT_TYPE);
                NodeList nlSize = elem.getElementsByTagName(DEFAULT_SIZE);
                NodeList nlEqu = elem.getElementsByTagName(INITIAL_EQUITY);
                NodeList nlIntr = elem.getElementsByTagName(DAILY_INTREST);
                NodeList nlSys = elem.getElementsByTagName(SYSTEMS);
                NodeList nlSec = elem.getElementsByTagName(SECURITIES);

                long lSimID = Long.parseLong(nlID.item(0).getTextContent());
                String sName = nlName.item(0).getTextContent();
                int type = Integer.parseInt(nlType.item(0).getTextContent());
                int size = Integer.parseInt(nlSize.item(0).getTextContent());
                double eqt = Double.parseDouble(nlEqu.item(0).getTextContent());
                double intr = Double.parseDouble(nlIntr.item(0).getTextContent());
                intr = 1.00008;//todo
                String secs = nlSec.item(0).getTextContent();
                String sys = nlSys.item(0).getTextContent();

                StringTokenizer tokenizer = new StringTokenizer(sys, ",");
                long[] ids = new long[tokenizer.countTokens()];
                int c = 0;
                while (tokenizer.hasMoreTokens()) {
                    ids[c] = Long.parseLong(String.valueOf(tokenizer.nextToken()));
                    c++;
                }

                StringTokenizer tokenizer2 = new StringTokenizer(secs, ",");
                String[] secus = new String[tokenizer2.countTokens()];
                c = 0;
                while (tokenizer2.hasMoreTokens()) {
                    secus[c] = String.valueOf(tokenizer2.nextElement());
                    c++;
                }

                BackTestSimulationData item = new BackTestSimulationData(lSimID, sName, size, type, eqt, intr, ids, secus);
                bTDataItems.add(item);
            }
            if (isEncrypted) {
                deleteTempFile(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            }
        }

        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("=== XMLSerializationService class-deserializeAnalysTechRecords() ======");
        }

        return bTDataItems;

    }

    public static ArrayList<BackTestSystemData> getSystemsForSimulation(BackTestSimulationData sim) {

        ArrayList<BackTestSimulationData> sims = BackTestingXmlService.deserializeSimulations();
        long[] systemsIds = null;
        for (int i = 0; i < sims.size(); i++) {
            if (sims.get(i).getSimulationID() == sim.getSimulationID()) {
                systemsIds = sims.get(i).getSystems();
                break;
            }
        }

        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        if (systemsIds == null || (systemsIds.length <= 0)) {
            return null;
        }

        ArrayList<BackTestSystemData> result = new ArrayList<BackTestSystemData>();

        for (int j = 0; j < systemsIds.length; j++) {
            for (int i = 0; i < systems.size(); i++) {
                if (systems.get(i).getSystemID() == systemsIds[j]) {
                    //systemsIds = sims.get(i).getSystems();
                    result.add(systems.get(i));
                }
            }
        }

        return result;
    }

    public static long getSystemIDBySystemName(String systemName) {
        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        for (int i = 0; i < systems.size(); i++) {
            if (systemName.equals(systems.get(i).getSystemName())) {
                return systems.get(i).getSystemID();
            }
        }

        return -99999;
    }

    public static long[] getSystemIDSForSystemNames(String[] names) {

        long[] result = new long[names.length];
        for (int i = 0; i < names.length; i++) {
            result[i] = getSystemIDBySystemName(names[i]);
        }

        return result;
    }

    public static BackTestSystemData getBackTestSystemByID(long sysID) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData backTestSystemData = systems.get(i);

            if (backTestSystemData.getSystemID() == sysID) {
                return backTestSystemData;
            }

        }

        return null;
    }

}



*/


package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.options.AESFileEncryptor;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Charith Nidarsha
 * Date: Nov 19, 2008
 * Time: 11:20:49 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingXmlService {

    //systems related tags
    public static final String ROOT_ELEMENT_SYSTEMS = "BackTestingSystems";
    public static final String ROOT_ELEMENT_SYSTEM = "System";

    public static final String SYSTEM_ID = "SystemID";
    public static final String SYSTEM_NAME = "SystemName";
    public static final String SYSTEM_DESCRIPTION = "SystemDescription";
    public static final String GENERAL_POSITIONS = "GeneralPositions";
    public static final String BUY_GRAMMAR = "BuyGrammar";
    public static final String BUY_QUANTITY = "BuyQuantity";
    public static final String BUY_TYPE = "BuyType";
    public static final String SELL_GRAMMAR = "SellGrammar";

    //simulation related tags
    public static final String ROOT_ELEMENT_SIMULATIONS = "Simulations";
    public static final String ROOT_ELEMENT_SIMULATION = "Simulation";

    public static final String SIMULATION_NAME = "SimulationName";
    public static final String SIMULATION_ID = "SimulationID";
    public static final String DEFAULT_TYPE = "DefaultType";
    public static final String DEFAULT_SIZE = "DefaultSize";
    public static final String INITIAL_EQUITY = "InitialEquity";
    public static final String DAILY_INTREST = "DailyIntrestRate";
    public static final String SECURITIES = "Securities";
    public static final String SYSTEMS = "Systems";

    private static boolean isEncrypted = false;

    private static ArrayList<BackTestSystemData> backTestSystems = null;
    private static ArrayList<BackTestSimulationData> backTestSims = null;

    public static synchronized void serializeBackSysetms(ArrayList<BackTestSystemData> systems) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            backTestSystems = systems;

            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_SYSTEMS);
            doc.appendChild(root);

            String strText = "";

            for (BackTestSystemData system : systems) {

                Element eleSystem = doc.createElement(ROOT_ELEMENT_SYSTEM);
                root.appendChild(eleSystem);

                Element eleID = doc.createElement(SYSTEM_ID);
                strText = String.valueOf(system.getSystemID());
                Text text = doc.createTextNode(strText);
                eleID.appendChild(text);
                eleSystem.appendChild(eleID);

                Element eleName = doc.createElement(SYSTEM_NAME);
                strText = system.getSystemName();
                text = doc.createTextNode(strText);
                eleName.appendChild(text);
                eleSystem.appendChild(eleName);

                Element eleDes = doc.createElement(SYSTEM_DESCRIPTION);
                CDATASection cdata = doc.createCDATASection(system.getSystemDescription());
                eleDes.appendChild(cdata);
                eleSystem.appendChild(eleDes);

                Element elePos = doc.createElement(GENERAL_POSITIONS);
                strText = String.valueOf(system.getGeneralPositions());
                text = doc.createTextNode(strText);
                elePos.appendChild(text);
                eleSystem.appendChild(elePos);

                Element eleBuyGram = doc.createElement(BUY_GRAMMAR);
                cdata = doc.createCDATASection(system.getBuyGrammar());
                eleBuyGram.appendChild(cdata);
                eleSystem.appendChild(eleBuyGram);

                Element eleQty = doc.createElement(BUY_QUANTITY);
                strText = String.valueOf(system.getBuyQuantitiy());
                text = doc.createTextNode(strText);
                eleQty.appendChild(text);
                eleSystem.appendChild(eleQty);

                Element eleType = doc.createElement(BUY_TYPE);
                strText = String.valueOf(system.getBuyType());
                text = doc.createTextNode(strText);
                eleType.appendChild(text);
                eleSystem.appendChild(eleType);

                Element eleSellGram = doc.createElement(SELL_GRAMMAR);
                cdata = doc.createCDATASection(system.getSellGrammar());
                eleSellGram.appendChild(cdata);
                eleSystem.appendChild(eleSellGram);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            if (isEncrypted) {
                // Encrypt
                FileInputStream inStream = new FileInputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);
                encrypter.encryptFile(inStream, new FileOutputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH));
                inStream.close();
                deleteTempFile(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== BackTestingXmlService class- serializeBackSysetms() ======");
        }
    }

    public static synchronized ArrayList<BackTestSystemData> desirializeSystems() {

        if (backTestSystems == null) {
            System.out.println("############## SYSTEMS DESERIALIZATION ########### ");

            ArrayList<BackTestSystemData> systems = new ArrayList<BackTestSystemData>();

            File file = null;
            if (isEncrypted) {
                file = new File(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH);
            } else {
                file = new File(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
            }
            if (!file.exists()) {
                return systems;
            }

            try {
                if (isEncrypted) {
                    // Decrypt
                    AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                    encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);

                    FileOutputStream stream = new FileOutputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                    encrypter.decryptFile(new FileInputStream(BackTestingConstatns.BACK_TEST_SYSTEMS_FILE_PATH), stream);

                    stream.close();
                }

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

                Document dom = docBuilder.parse(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                //Document dom = docBuilder.parse(tempPath);

                NodeList list = dom.getElementsByTagName(ROOT_ELEMENT_SYSTEM);
                int count = list.getLength();

                for (int i = 0; i < count; i++) {

                    Element elem = (Element) list.item(i);
                    NodeList nlID = elem.getElementsByTagName(SYSTEM_ID);
                    NodeList nlName = elem.getElementsByTagName(SYSTEM_NAME);
                    NodeList nlDes = elem.getElementsByTagName(SYSTEM_DESCRIPTION);
                    NodeList nlPos = elem.getElementsByTagName(GENERAL_POSITIONS);
                    NodeList nlBuyGram = elem.getElementsByTagName(BUY_GRAMMAR);
                    NodeList nlBuyQt = elem.getElementsByTagName(BUY_QUANTITY);
                    NodeList nlBuyType = elem.getElementsByTagName(BUY_TYPE);
                    NodeList nlSellGram = elem.getElementsByTagName(SELL_GRAMMAR);

                    long id = Long.parseLong(nlID.item(0).getTextContent());
                    String name = nlName.item(0).getTextContent();
                    String des = nlDes.item(0).getTextContent();
                    int pos = Integer.parseInt(nlPos.item(0).getTextContent());
                    String buyGram = nlBuyGram.item(0).getTextContent();
                    int buyQt = Integer.parseInt(nlBuyQt.item(0).getTextContent());
                    int buyType = Integer.parseInt(nlBuyType.item(0).getTextContent());
                    String SellGram = nlSellGram.item(0).getTextContent();
                    BackTestSystemData item = new BackTestSystemData(id, name, des, pos, buyGram, buyQt, buyType, SellGram);
                    systems.add(item);
                }
                if (isEncrypted) {
                    deleteTempFile(BackTestingConstatns.BACK_TEST_SYSTEMS_TEMP_FILE_PATH);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("=== XMLSerializationService class-deserializeCustomDataItems() ======");
            }

            backTestSystems = systems;
        }
        return backTestSystems;
    }

    public static void updateSystemBySystemID(BackTestSystemData sys, boolean shouldDelete) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData system = systems.get(i);

            if (system.getSystemID() == sys.getSystemID()) {
                if (shouldDelete) {
                    systems.remove(i);
                } else {
                    system.assignValuesFrom(sys);
                }
                break;
            }
        }

        try {
            serializeBackSysetms(systems);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update systems **************");
        }

    }

    public static void updateSimulationBySimulationID(BackTestSimulationData sim, boolean shouldDelete) {

        ArrayList<BackTestSimulationData> sims = deserializeSimulations();

        for (int i = 0; i < sims.size(); i++) {
            BackTestSimulationData s = sims.get(i);

            if (s.getSimulationID() == sim.getSimulationID()) {
                if (shouldDelete) {
                    sims.remove(i);
                } else {
                    s.assignValuesFrom(sim);
                }
                break;
            }
        }

        try {
            serializeSimulations(sims);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update simulations **************");
        }

    }

    public static void deleteSystemBySystemID(BackTestSystemData sys) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData system = systems.get(i);

            if (system.getSystemID() == sys.getSystemID()) {
                system.assignValuesFrom(sys);
                break;
            }
        }

        try {
            serializeBackSysetms(systems);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("******************* faulure in update systems **************");
        }

    }

    private static synchronized void deleteTempFile(String filePath) {

        File f1 = new File(filePath);

        try {
            if (f1.exists()) {
                //f1.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("failure deletion of tempory file..");
        }
    }

    public static synchronized void serializeSimulations(ArrayList<BackTestSimulationData> simulations) {
        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            backTestSims = simulations;
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_SIMULATIONS);
            doc.appendChild(root);

            //add general settings to xml file
            for (BackTestSimulationData item : simulations) {
                Element eleBTSimulation = doc.createElement(ROOT_ELEMENT_SIMULATION);
                root.appendChild(eleBTSimulation);

                //ID
                Element eleID = doc.createElement(SIMULATION_ID);
                Text textID = doc.createTextNode(Long.toString(item.getSimulationID()));
                eleBTSimulation.appendChild(eleID);
                eleID.appendChild(textID);

                //NAME
                Element eleName = doc.createElement(SIMULATION_NAME);
                Text textName = doc.createTextNode(item.getSimulationName());
                eleBTSimulation.appendChild(eleName);
                eleName.appendChild(textName);

                Element eleType = doc.createElement(DEFAULT_TYPE);
                Text textType = doc.createTextNode(String.valueOf(item.getDefaultType()));
                eleBTSimulation.appendChild(eleType);
                eleType.appendChild(textType);

                Element eleSize = doc.createElement(DEFAULT_SIZE);
                Text textSize = doc.createTextNode(String.valueOf(item.getDefaultSize()));
                eleBTSimulation.appendChild(eleSize);
                eleSize.appendChild(textSize);

                Element eleQt = doc.createElement(INITIAL_EQUITY);
                Text textQt = doc.createTextNode(String.valueOf(item.getInitialEquity()));
                eleBTSimulation.appendChild(eleQt);
                eleQt.appendChild(textQt);

                Element eleIntr = doc.createElement(DAILY_INTREST);
                Text textint = doc.createTextNode(String.valueOf(item.getDailyInterestRate()));
                eleBTSimulation.appendChild(eleIntr);
                eleIntr.appendChild(textint);

                Element eleSec = doc.createElement(SECURITIES);

                String secs = "";
                for (int i = 0; i < item.getSecurities().length; i++) {
                    secs += item.getSecurities()[i] + ",";
                }
                secs = secs.substring(0, secs.length() - 1);
                CDATASection cDSec = doc.createCDATASection(secs);
                eleSec.appendChild(cDSec);
                eleBTSimulation.appendChild(eleSec);

                Element eleSys = doc.createElement(SYSTEMS);

                String syss = "";
                for (int i = 0; i < item.getSystems().length; i++) {
                    syss += item.getSystems()[i] + ",";
                }
                syss = syss.substring(0, syss.length() - 1);
                CDATASection cDSys = doc.createCDATASection(syss);
                eleSys.appendChild(cDSys);
                eleBTSimulation.appendChild(eleSys);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            if (isEncrypted) {
                // Encrypt
                FileInputStream inStream = new FileInputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
                AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);

                encrypter.encryptFile(inStream, new FileOutputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH));
                inStream.close();
                deleteTempFile(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== XMLSerializationService class-serializeBackTestingDataObjects() ======");
        }
    }

    public static synchronized ArrayList<BackTestSimulationData> deserializeSimulations() {

        if (backTestSims == null) {
            ArrayList<BackTestSimulationData> bTDataItems = new ArrayList<BackTestSimulationData>();

            File file = null;
            if (isEncrypted) {
                file = new File(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH);
            } else {
                file = new File(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
            }
            if (!file.exists()) {
                return bTDataItems;
            }
            try {
                if (isEncrypted) {
                    // Decrypt
                    AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
                    encrypter.constructKeys(BackTestingConstatns.BT_ENCRYPTION_PATH);
                    FileOutputStream stream = new FileOutputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
                    encrypter.decryptFile(new FileInputStream(BackTestingConstatns.BACK_TEST_SIMULATIONS_FILE_PATH), stream);
                    stream.close();
                }

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
                Document dom = docBuilder.parse(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);

                NodeList list = dom.getElementsByTagName(ROOT_ELEMENT_SIMULATION);
                int count = list.getLength();
                for (int i = 0; i < count; i++) {

                    Element elem = (Element) list.item(i);
                    NodeList nlID = elem.getElementsByTagName(SIMULATION_ID);
                    NodeList nlName = elem.getElementsByTagName(SIMULATION_NAME);
                    NodeList nlType = elem.getElementsByTagName(DEFAULT_TYPE);
                    NodeList nlSize = elem.getElementsByTagName(DEFAULT_SIZE);
                    NodeList nlEqu = elem.getElementsByTagName(INITIAL_EQUITY);
                    NodeList nlIntr = elem.getElementsByTagName(DAILY_INTREST);
                    NodeList nlSys = elem.getElementsByTagName(SYSTEMS);
                    NodeList nlSec = elem.getElementsByTagName(SECURITIES);

                    long lSimID = Long.parseLong(nlID.item(0).getTextContent());
                    String sName = nlName.item(0).getTextContent();
                    int type = Integer.parseInt(nlType.item(0).getTextContent());
                    int size = Integer.parseInt(nlSize.item(0).getTextContent());
                    double eqt = Double.parseDouble(nlEqu.item(0).getTextContent());
                    double intr = Double.parseDouble(nlIntr.item(0).getTextContent());
                    intr = 1.00008;//todo
                    String secs = nlSec.item(0).getTextContent();
                    String sys = nlSys.item(0).getTextContent();

                    StringTokenizer tokenizer = new StringTokenizer(sys, ",");
                    long[] ids = new long[tokenizer.countTokens()];
                    int c = 0;
                    while (tokenizer.hasMoreTokens()) {
                        ids[c] = Long.parseLong(String.valueOf(tokenizer.nextToken()));
                        c++;
                    }

                    StringTokenizer tokenizer2 = new StringTokenizer(secs, ",");
                    String[] secus = new String[tokenizer2.countTokens()];
                    c = 0;
                    while (tokenizer2.hasMoreTokens()) {
                        secus[c] = String.valueOf(tokenizer2.nextElement());
                        c++;
                    }

                    BackTestSimulationData item = new BackTestSimulationData(lSimID, sName, size, type, eqt, intr, ids, secus);
                    bTDataItems.add(item);
                }
                if (isEncrypted) {
                    deleteTempFile(BackTestingConstatns.BACK_TEST_SIMULATIONS_TEMP_FILE_PATH);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("=== XMLSerializationService class-deserializeAnalysTechRecords() ======");
            }

            backTestSims = bTDataItems;
        }

        return backTestSims;

    }

    public static ArrayList<BackTestSystemData> getSystemsForSimulation(BackTestSimulationData sim) {

        ArrayList<BackTestSimulationData> sims = BackTestingXmlService.deserializeSimulations();
        long[] systemsIds = null;
        for (int i = 0; i < sims.size(); i++) {
            if (sims.get(i).getSimulationID() == sim.getSimulationID()) {
                systemsIds = sims.get(i).getSystems();
                break;
            }
        }

        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        if (systemsIds == null || (systemsIds.length <= 0)) {
            return null;
        }

        ArrayList<BackTestSystemData> result = new ArrayList<BackTestSystemData>();

        for (int j = 0; j < systemsIds.length; j++) {
            for (int i = 0; i < systems.size(); i++) {
                if (systems.get(i).getSystemID() == systemsIds[j]) {
                    //systemsIds = sims.get(i).getSystems();
                    result.add(systems.get(i));
                }
            }
        }

        return result;
    }

    public static long getSystemIDBySystemName(String systemName) {
        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        for (int i = 0; i < systems.size(); i++) {
            if (systemName.equals(systems.get(i).getSystemName())) {
                return systems.get(i).getSystemID();
            }
        }

        return -99999;
    }

    public static long[] getSystemIDSForSystemNames(String[] names) {

        long[] result = new long[names.length];
        for (int i = 0; i < names.length; i++) {
            result[i] = getSystemIDBySystemName(names[i]);
        }

        return result;
    }

    public static BackTestSystemData getBackTestSystemByID(long sysID) {

        ArrayList<BackTestSystemData> systems = desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData backTestSystemData = systems.get(i);

            if (backTestSystemData.getSystemID() == sysID) {
                return backTestSystemData;
            }

        }

        return null;
    }

}



