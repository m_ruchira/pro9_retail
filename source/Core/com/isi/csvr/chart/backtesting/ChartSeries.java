package com.isi.csvr.chart.backtesting;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 14, 2008
 * Time: 2:32:43 PM
 * To change this template use File | Settings | File Templates.
 */

public class ChartSeries {

    public String name;
    public Color color;
    public int count;
    public double[] XData;
    public double[] YData;
    public Point2D.Float[] points;

    public ChartSeries(String name, int count, Color color) {
        this.name = name;
        this.count = count;
        this.color = color;
        this.XData = new double[count];
        this.YData = new double[count];
    }

    public ChartSeries(String name, int count, Color color, double[] xdata, double[] ydata) {
        this.name = name;
        this.count = count;
        this.color = color;
        this.XData = xdata;
        this.YData = ydata;
    }
}