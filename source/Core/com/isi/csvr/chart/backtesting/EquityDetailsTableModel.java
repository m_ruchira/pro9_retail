package com.isi.csvr.chart.backtesting;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 26, 2008
 * Time: 9:30:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class EquityDetailsTableModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList<EquityDetailRecord> detailRecords;

    public EquityDetailsTableModel(ArrayList<EquityDetailRecord> recs) {
        this.detailRecords = recs;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        EquityDetailRecord detailRecord = detailRecords.get(rowIndex);

        switch (columnIndex) {

            case -1:
                return detailRecord;
            case 0:
                return String.valueOf(detailRecord.getBarID());

            case 1:
                return detailRecord.getDate();

            case 2:
                return String.valueOf(detailRecord.getCash());

            case 3:
                return String.valueOf(detailRecord.getPortfolio());

            case 4:
                return String.valueOf(detailRecord.getInterest());

            case 5:
                return String.valueOf(detailRecord.getTotal());

            default:
                return "";
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return detailRecords.size();
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }

}
