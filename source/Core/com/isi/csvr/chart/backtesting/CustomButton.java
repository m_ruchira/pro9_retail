package com.isi.csvr.chart.backtesting;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Dec 2, 2008
 * Time: 5:11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomButton extends JButton {

    public CustomButton(String text) {
        super(text);
    }

    public Insets getInsets() {
        return new Insets(0, 0, 0, 0);
    }
}
