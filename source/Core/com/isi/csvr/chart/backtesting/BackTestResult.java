package com.isi.csvr.chart.backtesting;

import com.isi.csvr.scanner.Scans.ScanRecord;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 18, 2008
 * Time: 9:48:04 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestResult implements Comparable {

    private BackTestSystemData systemData = null;
    private long systemID;

    private String symbol = null;
    private double dailyRate = 0;
    private double cachInHand = 0;
    private int stocksInHand = 0;
    private double lastPrice = 0;
    private double totalEquity = 0;
    private double initialEquity = 0;
    private int tradeCount = 0;
    private int profitableTrades = 0;
    private int unprofitableTrades = 0;
    private double averageProfit = 0;
    private double averageLoss = 0;
    private double highestProfit = 0;
    private double highestLoss = 0;
    private double lowestProfit = 0;
    private double lowestLoss = 0;
    private int mostConsecutivePTs = 0;
    private int mostConsecutiveUTs = 0;
    private int openPositions = 0;

    private ArrayList records = null;
    private ArrayList<TransactionPoint> transactions = new ArrayList<TransactionPoint>();
    private ArrayList<EquityDetailRecord> equityDetailRecords = new ArrayList<EquityDetailRecord>();
    private ArrayList<BackTestingPosition> positions = new ArrayList<BackTestingPosition>();


    public BackTestResult(BackTestSystemData system, String symbol, double equity, double rate) {
        this.systemData = system;
        this.symbol = symbol;
        this.initialEquity = equity;
        this.dailyRate = rate;
        this.systemID = system.getSystemID();
    }

    public BackTestSystemData getSystemData() {
        return systemData;
    }

    public void setBackTestSystemData(BackTestSystemData val) {
        this.systemID = val.getSystemID();
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getDailyRate() {
        return dailyRate;
    }

    public void setDailyRate(double dailyRate) {
        this.dailyRate = dailyRate;
    }

    public double getCachInHand() {
        return cachInHand;
    }

    public void setCachInHand(double cachInHand) {
        this.cachInHand = cachInHand;
    }

    public int getStocksInHand() {
        return stocksInHand;
    }

    public void setStocksInHand(int stocksInHand) {
        this.stocksInHand = stocksInHand;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public double getTotalEquity() {
        return totalEquity;
    }

    public void setTotalEquity(double totalEquity) {
        this.totalEquity = totalEquity;
    }

    public double getInitialEquity() {
        return initialEquity;
    }

    public void setInitialEquity(double initialEquity) {
        this.initialEquity = initialEquity;
    }

    public int getTradeCount() {
        return tradeCount;
    }

    public void setTradeCount(int tradeCount) {
        this.tradeCount = tradeCount;
    }

    public int getProfitableTrades() {
        return profitableTrades;
    }

    public void setProfitableTrades(int profitableTrades) {
        this.profitableTrades = profitableTrades;
    }

    public int getUnprofitableTrades() {
        return unprofitableTrades;
    }

    public void setUnprofitableTrades(int unprofitableTrades) {
        this.unprofitableTrades = unprofitableTrades;
    }

    public double getAverageProfit() {
        return averageProfit;
    }

    public void setAverageProfit(double averageProfit) {
        this.averageProfit = averageProfit;
    }

    public double getAverageLoss() {
        return averageLoss;
    }

    public void setAverageLoss(double averageLoss) {
        this.averageLoss = averageLoss;
    }

    public double getHighestProfit() {
        return highestProfit;
    }

    public void setHighestProfit(double highestProfit) {
        this.highestProfit = highestProfit;
    }

    public double getHighestLoss() {
        return highestLoss;
    }

    public void setHighestLoss(double highestLoss) {
        this.highestLoss = highestLoss;
    }

    public double getLowestProfit() {
        return lowestProfit;
    }

    public void setLowestProfit(double lowestProfit) {
        this.lowestProfit = lowestProfit;
    }

    public double getLowestLoss() {
        return lowestLoss;
    }

    public void setLowestLoss(double lowestLoss) {
        this.lowestLoss = lowestLoss;
    }

    public int getMostConsecutivePTs() {
        return mostConsecutivePTs;
    }

    public void setMostConsecutivePTs(int mostConsecutivePTs) {
        this.mostConsecutivePTs = mostConsecutivePTs;
    }

    public int getMostConsecutiveUTs() {
        return mostConsecutiveUTs;
    }

    public void setMostConsecutiveUTs(int mostConsecutiveUTs) {
        this.mostConsecutiveUTs = mostConsecutiveUTs;
    }

    public int getOpenPositions() {
        return openPositions;
    }

    public void setOpenPositions(int openPositions) {
        this.openPositions = openPositions;
    }

    public ArrayList getRecords() {
        return records;
    }

    public void setRecords(ArrayList<ScanRecord> records) {
        this.records = records;
    }

    public void processResults() {  //TODO: completing the logic here..
        tradeCount = 0;
        profitableTrades = 0;
        unprofitableTrades = 0;
        double profitSum = 0;
        double lossSum = 0;
        highestProfit = 0;
        highestLoss = 0;
        mostConsecutivePTs = 0;
        mostConsecutiveUTs = 0;
        lowestProfit = Double.MAX_VALUE;
        lowestLoss = -Double.MAX_VALUE;
        int consecP = 0, consecL = 0;
        if (positions != null) {
            for (BackTestingPosition pos : positions) {
                tradeCount++;
                double currPL = (pos.SellPrice - pos.BuyPrice) * pos.Quantity;
                if (pos.BuyPrice < pos.SellPrice) {
                    profitableTrades++;
                    profitSum += currPL;
                    highestProfit = Math.max(highestProfit, currPL);
                    lowestProfit = Math.min(lowestProfit, currPL);
                    consecP++;
                    consecL = 0;
                    mostConsecutivePTs = Math.max(mostConsecutivePTs, consecP);
                } else {
                    unprofitableTrades++;
                    lossSum += currPL;
                    highestLoss = Math.min(highestLoss, currPL);
                    lowestLoss = Math.max(lowestLoss, currPL);
                    consecL++;
                    consecP = 0;
                    mostConsecutiveUTs = Math.max(mostConsecutiveUTs, consecL);
                }
            }
        }
        if (lowestProfit == Double.MAX_VALUE) lowestProfit = 0;
        if (lowestLoss == -Double.MAX_VALUE) lowestLoss = 0;
        averageProfit = (profitableTrades > 0) ? profitSum / profitableTrades : 0;
        averageLoss = (unprofitableTrades > 0) ? lossSum / unprofitableTrades : 0;
        extractEquityRecords();
    }

    public void extractEquityRecords() { //TODO: completing the logic here..
        //extract all bars from records
        for (int i = 0; i < records.size(); i++) {
            ScanRecord scr = (ScanRecord) records.get(i);
            equityDetailRecords.add(new EquityDetailRecord(i + 1, scr.Time * 60000L, scr.Close));
        }
        //insert txn details

        int tsncount = transactions.size();
        for (int i = 0; i < tsncount; i++) {
            TransactionPoint tp = (TransactionPoint) transactions.get(i);
            if ((tp.getTransactionResult() == BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED) || (tp.getTransactionResult() == BackTestingConstatns.TRANSACTION_RESULT_PARTIALLY_EXECUTED)) {
                long time = 0;
                switch (tp.getTransactionType()) {
                    case BackTestingConstatns.LOGIC_TYPE_BUY:
                        time = tp.getNextRecord().Time;
                        break;
                    case BackTestingConstatns.LOGIC_TYPE_SELL:
                        time = tp.getNextRecord().Time;
                        break;
                    case BackTestingConstatns.LOGIC_TYPE_LAST_BAR:
                        time = tp.getCurrentRecord().Time;
                        break;
                }
                if (time != 0) {
                    int searchIndex = Collections.binarySearch(records, new ScanRecord(time));
                    if (searchIndex >= 0) {
                        equityDetailRecords.get(searchIndex).setValues(tp.getStocksInHand(), tp.getCashInHand());
                    }
                }
            }
        }
        //calculate all equity fields
        if (equityDetailRecords.size() > 0) {
            equityDetailRecords.get(0).setValues(0, initialEquity, 0); //qty, cash, interest
            double interest, cash, preCash = initialEquity;
            int preQty = 0;
            int days = 1;
            long preTime = equityDetailRecords.get(0).getTime();
            for (int i = 1; i < equityDetailRecords.size(); i++) {
                EquityDetailRecord er = equityDetailRecords.get(i);
                days = (int) Math.round((er.getTime() - preTime) / 86400000d);
                if (er.getCash() == -Double.MAX_VALUE) { // non txn point
                    cash = preCash * Math.pow(getDailyRate(), days);
                    er.setValues(preQty, cash, cash - preCash); //qty, cash, interest
                } else { // txn point
                    if (preCash > er.getCash()) { //buy
                        if (days > 1) {
                            interest = preCash * Math.pow(getDailyRate(), days - 1) - preCash;
                            interest = interest + er.getCash() - er.getCash() / getDailyRate();
                        } else {
                            interest = er.getCash() - er.getCash() / getDailyRate();
                        }
                    } else { //sell
                        interest = preCash * Math.pow(getDailyRate(), days) - preCash;
                    }
                    er.setValues(er.getQuantity(), er.getCash(), interest); //qty, cash, interest
                    preQty = er.getQuantity();
                }
                preTime = er.getTime();
                preCash = er.getCash();
            }
        }
    }

    public int compareTo(Object obj) {

        BackTestResult result = (BackTestResult) obj;
        int sys = this.systemData.getSystemName().compareTo(result.systemData.getSystemName());

        if (sys == 0) {
            return this.symbol.compareTo(result.symbol);
        } else {
            return sys;
        }

    }

    public ArrayList<TransactionPoint> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<TransactionPoint> tr) {
        this.transactions = tr;
    }

    public ArrayList<BackTestingPosition> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<BackTestingPosition> positions) {
        this.positions = positions;
    }

    public long getSystemID() {
        return systemID;
    }

    public void setSystemID(long systemID) {
        this.systemID = systemID;
    }

    public ArrayList<EquityDetailRecord> getEquityDetailRecords() {
        return equityDetailRecords;
    }

    public void setEquityDetailRecords(ArrayList<EquityDetailRecord> equityDetailRecords) {
        this.equityDetailRecords = equityDetailRecords;
    }


}