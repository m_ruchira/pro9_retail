package com.isi.csvr.chart.backtesting;

import com.isi.csvr.Client;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.chart.*;
import com.isi.csvr.chart.customindicators.*;
import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.Scans.ScanRecord;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.FlexGridLayout;
import com.mubasher.formulagen.TWCompiler;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 11, 2008
 * Time: 7:20:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingWindow extends InternalFrame implements NonResizeable, Themeable, ActionListener, KeyListener, WatchlistListener, ExchangeListener, FocusListener {

    //private final int EVALUATOR_WINDOW_WIDTH = 800;
    //private final int EVALUATOR_WINDOW_HEIGHT = 900;

    private static BackTestingWindow testingWindow = null;
    private final int EVALUATOR_WINDOW_WIDTH = 1272;
    private final int EVALUATOR_WINDOW_HEIGHT = 553;
    private final String BUTTON_HEIGHT = "20";
    public Hashtable<String, ChartSymbolNode> mainStore;
    public Hashtable<String, ChartSymbolNode> filteredStore;
    //systems tabbed panel
    private TWTabbedPane systemsTabbedPane;
    //systems - left panel components
    private TWButton btnNewSystem;
    private TWButton btnDelete;
    //systems - left panel layouts
    private FlexGridLayout finalLayOutSystems = new FlexGridLayout(new String[]{"25", "200", "100%"}, new String[]{"100%"}, 0, 0);
    private FlexGridLayout rightPanelLayOutSystems = new FlexGridLayout(new String[]{"100%"}, new String[]{"92%", "8%"}, 0, 0);
    private FlexGridLayout leftSystemsPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"7%", "93%"}, 0, 0);
    private FlexGridLayout leftUpperPanelLayOut = new FlexGridLayout(new String[]{"105", "70"}, new String[]{"20", "2"}, 5, 0);
    //general panel components
    //private FlexGridLayout generalPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"16%", "50%", "16%"}, 0, 10);
    private FlexGridLayout generalPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "150", "20", "20"}, 0, 6);
    private TWTextField txtSystemName = new TWTextField();
    private TWTextArea txtDescription = new TWTextArea();
    private SpinnerModel model = new SpinnerNumberModel(5, 1, 1000, 1);
    private JSpinner spnPositionLimit = new JSpinner(model);
    //buy order panel components
    private FlexGridLayout buyPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"70%", "10%", "15%"}, 0, 5);
    private JTextPane txtBuyGrammar;
    private StyledDocument buyDoc;
    private TWButton btnTestBuyGrammar = new TWButton(Language.getString("BT_TEST_GRAMMAR"));
    private TWButton btnfunctionsBuy = new TWButton("Functions");
    private TWComboBox cmbType = new TWComboBox(new String[]{"Default size", "Number of Units", "Transaction Cost", "% Of Available Equity"});
    private TWTextField txtQuantity = new TWTextField();
    //sell order panel components
    private FlexGridLayout sellPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"70%", "30%"}, 0, 5);
    private JTextPane txtSellGrammar;
    private StyledDocument sellDoc;
    private TWButton btnTestSellGrammar = new TWButton(Language.getString("BT_TEST_GRAMMAR"));
    private TWButton btnfunctionsSell = new TWButton(Language.getString("BT_FUNCTIONS"));
    //simulations tabbed panel
    private TWTabbedPane simulationsTabbedPane;
    private TWTabbedPane propertiesTabbedPane;
    //simulations - left panel components
    private MyButton btnNewSimulations;
    private TWButton btnDeleteSim;
    //simulations - left panel layouts
    private FlexGridLayout leftSimulationsPanelLayOut = new FlexGridLayout(new String[]{"100%"}, new String[]{"7%", "93%"}, 0, 0);
    private FlexGridLayout leftSimulationsUpperPanelLayOut = new FlexGridLayout(new String[]{"110", "67"}, new String[]{"20", "2", "20"}, 5, 0);
    //simulations - system panel components
    private JList lstLeft;
    private JList lstRight;
    private TWButton btnleftToRightSystems = new TWButton(" > ");
    private TWButton btnRightToLeftSystems = new TWButton(" < ");
    private TWButton btnRightToLeftSystemsAll = new TWButton("<<");
    //simulations -settings panel components
    private TWTextField txtSimulationName;
    private TWTextField txtInitialEquity;
    private TWComboBox cmbSize = new TWComboBox(new String[]{Language.getString("BT_NO_OF_UNITS"), Language.getString("BT_COST"), Language.getString("BT_AVAILABLE_EQUITY")});
    private TWTextField txtSize;
    //simulations - securities panel components
    private TWButton btnleftToRightSecurities = new TWButton(" > ");
    private TWButton btnRightToLeftSecurities = new TWButton(" < ");
    private TWButton btnRightToLeftSecuritiesAll = new TWButton("<<");
    private ChartNavigationTree symbolTree;
    private ChartSearchTreeModel treeModel;
    private JTree systemsTree;
    private JTree simulationTree;
    private TWButton btnSave;
    private TWButton btnCancel;
    private TWButton btnSaveSimulation;
    private TWButton btnStart;
    private DefaultMutableTreeNode sysRoot;
    private DefaultMutableTreeNode simRoot;
    private IndicatorParser parser;
    private boolean isUpdated = false;
    private BackTestSystemData previousSystem = null;
    private BackTestSystemData currentSystem = null;
    private Table symbolTable;
    private BackTestingSymbolTableModel symbolModel;
    private ArrayList<Key> selectedSymbols = null;
    private FunctionDialog dialog1;
    private FunctionDialog dialog2;
    private JPanel container;
    //security tree
    private JPanel pnlSecurities;
    private Hashtable<String, ChartExchangeNode> watchListReference;
    private ChartExchangeNode rootNode;

    private TWTextField searchText;
    private TWButton searchButton;
    private JPanel pnlSecurityTreePanel;
    private JPanel treeSearchCompPanel;
    private JScrollPane secutiryScrollPane;

    private VerticalTabButton btnTestSys;
    private VerticalTabButton btnTestSim;

    private VerticalTabButton btnSystems;
    private VerticalTabButton btnSimulations;

    private JPanel pnlSummary;
    private JPanel pnlSystems;
    private TWTabbedPane systemsTabs;

    private String ID_SYSTEM = "system";
    private String ID_SIMULATION = "Sim";
    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,##0.00");
    private TWDecimalFormat tradeFormatter = new TWDecimalFormat("###,###,##0.0");
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("#0.00");
    private TWDecimalFormat prcFormatter = new TWDecimalFormat("########0.00");

    private CustomButton[][] btnArray;

    private BackTestingWindow() throws HeadlessException {
        super();
        createUI();
    }

    public static BackTestingWindow getSharedInstance() {
        if (testingWindow == null) {
            testingWindow = new BackTestingWindow();
        }

        return testingWindow;
    }

    public static String getSourcePath(String className, String source) {
        String srcPath = (BackTestingConstatns.BACK_TEST_CLASS_PATH) + className + ".java";
        try {
            File file = new File(srcPath);
            if (file.exists()) file.delete();
            file.createNewFile();
            BufferedWriter bwriter = new BufferedWriter(new FileWriter(file));
            bwriter.write(source);
            bwriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return srcPath;
    }

    public void showWindow() {
        Client.getInstance().getDesktop().add(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());
        Client.getInstance().getDesktop().setLayer(this, GUISettings.TOP_LAYER);
        setVisible(true);
    }

    private void createUI() {

        JPanel rightPanelSystems = new JPanel();
        rightPanelSystems.setLayout(rightPanelLayOutSystems);

        rightPanelSystems.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        rightPanelSystems.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        systemsTabbedPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);

        systemsTabbedPane.addTab(Language.getString("BT_GENERAL"), createGenralPanel());
        systemsTabbedPane.addTab(Language.getString("BT_BUY"), createBuyOrderPanel());
        systemsTabbedPane.addTab(Language.getString("BT_SELL"), createSellOrderPanel());
        systemsTabbedPane.setSelectedIndex(0);

        JPanel pnlBottmSystems = createSystemsBottomPanel();
        rightPanelSystems.add(systemsTabbedPane);
        rightPanelSystems.add(pnlBottmSystems);

        JPanel leftPanelSystems = createLeftSystemsPanel();
        JPanel systemsTabPanel = new JPanel();
        systemsTabPanel.setLayout(finalLayOutSystems);
        JPanel pnlAll = new JPanel(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"100%"}, 0, 0));
        JPanel pnlSystemButtonContainer = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"5", "106", "106"}, 0, 0));
        pnlSystemButtonContainer.setBackground(Theme.getColor("VERTICAL_PANEL_BACKGROUND"));

        btnTestSys = new VerticalTabButton(Language.getString("BT_SYSTEMS"), true);
        btnTestSim = new VerticalTabButton(Language.getString("BT_SIMULATIONS"), false);

        pnlSystemButtonContainer.add(new JPanel());
        pnlSystemButtonContainer.add(btnTestSys);
        pnlSystemButtonContainer.add(btnTestSim);

        pnlAll.add(new JPanel());
        pnlAll.add(pnlSystemButtonContainer);
        pnlAll.add(new JPanel());
        systemsTabPanel.add(pnlAll);
        systemsTabPanel.add(leftPanelSystems);
        systemsTabPanel.add(rightPanelSystems);

        /* ================ start of simulatinos ===================== */
        JPanel leftPanelSimulations = createLeftSimulationsPanel();
        simulationsTabbedPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);
        simulationsTabbedPane.addTab(Language.getString("PROPERTIES"), createPropertiesPanel());
        simulationsTabbedPane.setSelectedIndex(0);

        JPanel rightPanelSimulations = new JPanel();
        rightPanelSimulations.setLayout(rightPanelLayOutSystems);

        rightPanelSimulations.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        rightPanelSimulations.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        JPanel pnlBottmSimulations = createSimulationsBottomPanel();
        rightPanelSimulations.add(simulationsTabbedPane);
        rightPanelSimulations.add(pnlBottmSimulations);

        JPanel simulationsTabPanel = new JPanel();
        simulationsTabPanel.setLayout(finalLayOutSystems);

        JPanel pnlAll2 = new JPanel(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"100%"}, 0, 0));
        JPanel pnlSimButtonContainer = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"5", "106", "106"}, 0, 0));
        pnlSimButtonContainer.setBackground(Theme.getColor("VERTICAL_PANEL_BACKGROUND"));

        btnSystems = new VerticalTabButton(Language.getString("BT_SYSTEMS"), false);
        btnSimulations = new VerticalTabButton(Language.getString("BT_SIMULATIONS"), true);

        pnlSimButtonContainer.add(new JPanel());
        pnlSimButtonContainer.add(btnSystems);
        pnlSimButtonContainer.add(btnSimulations);

        pnlAll2.add(new JPanel());
        pnlAll2.add(pnlSimButtonContainer);
        pnlAll2.add(new JPanel());

        simulationsTabPanel.add(pnlAll2);
        simulationsTabPanel.add(leftPanelSimulations);
        simulationsTabPanel.add(rightPanelSimulations);
        /* ================ end of simulatinos ===================== */

        TWTabbedPane allTabbedPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);

        allTabbedPane.addTab(Language.getString("BT_SYSTEMS"), systemsTabPanel);
        allTabbedPane.addTab(Language.getString("BT_SIMULATIONS"), simulationsTabPanel);
        allTabbedPane.setSelectedIndex(0);

        container = new JPanel(new CardLayout());

        container.add(systemsTabPanel, ID_SYSTEM);
        container.add(simulationsTabPanel, ID_SIMULATION);

        this.add(container);

        this.setSize(EVALUATOR_WINDOW_WIDTH, EVALUATOR_WINDOW_HEIGHT);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("BT_TITLE"));
        this.setClosable(true);
        this.setResizable(true);
        this.setIconifiable(true);
        this.setMaximizable(true);
        this.setLocation(-1000, -1000); // invalidate the location so that it can be centered
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        this.setMinimumSize(new Dimension(900, 450));

        try {
            //this.setMaximum(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        addActionListeners();
    }

    public void applyTheme() {
        systemsTree.setBackground(new JPanel().getBackground());
        simulationTree.setBackground(new JPanel().getBackground());
    }

    private JPanel createSystemsBottomPanel() {

        JPanel pnlBottmSystems = new JPanel(new FlexGridLayout(new String[]{"125", "240", "85", "85"}, new String[]{"20"}, 5, 5));
        pnlBottmSystems.add(new JLabel(""));
        pnlBottmSystems.add(new JLabel(""));
        btnSave = new TWButton(Language.getString("CO_SAVE"));
        btnCancel = new TWButton(Language.getString("CANCEL"));
        pnlBottmSystems.add(btnSave);
        pnlBottmSystems.add(btnCancel);
        pnlBottmSystems.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        return pnlBottmSystems;
    }

    private JPanel createSimulationsBottomPanel() {

        JPanel pnlBottmSimulations = new JPanel(new FlexGridLayout(new String[]{"360", "90", "90"}, new String[]{"20"}, 5, 5));
        pnlBottmSimulations.add(new JLabel(""));
        btnSaveSimulation = new TWButton(Language.getString("CO_SAVE"));
        btnStart = new TWButton(Language.getString("BT_START"));
        pnlBottmSimulations.add(btnSaveSimulation);
        pnlBottmSimulations.add(btnStart);
        pnlBottmSimulations.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        return pnlBottmSimulations;
    }

    private JPanel createLeftSystemsPanel() {

        JPanel pnlLeftSystems = new JPanel(leftSystemsPanelLayOut);

        btnNewSystem = new TWButton(Language.getString("BT_NEW_SYSTEM"));
        btnDelete = new TWButton(Language.getString("DELETE"));
        btnDelete.setEnabled(false);

        JPanel pnlLeftUpper = new JPanel(leftUpperPanelLayOut);
        pnlLeftUpper.add(btnNewSystem);
        pnlLeftUpper.add(btnDelete);
        pnlLeftUpper.add(new JLabel(""));

        JPanel pnlAvaliableSystems = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        pnlAvaliableSystems.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_AVAILABLE_SYSTEMS"), 0, 0, pnlAvaliableSystems.getFont().deriveFont(Font.BOLD)));

        createSystemsTree();
        JScrollPane paneSystems = new JScrollPane(systemsTree);
        paneSystems.setBackground(Color.BLUE);
        pnlAvaliableSystems.add(paneSystems);
        pnlLeftSystems.add(pnlLeftUpper);
        pnlLeftSystems.add(pnlAvaliableSystems);

        pnlLeftSystems.setBorder(BorderFactory.createEmptyBorder(5, 5, 1, 5));
        return pnlLeftSystems;
    }

    private JPanel createLeftSimulationsPanel() {

        JPanel pnlLeftSimulations = new JPanel(leftSimulationsPanelLayOut);

        btnNewSimulations = new MyButton(Language.getString("BT_NEW_SIM"));
        btnDeleteSim = new TWButton(Language.getString("DELETE"));
        btnDeleteSim.setEnabled(false);
        JPanel pnlLeftUpper = new JPanel(leftSimulationsUpperPanelLayOut);
        pnlLeftUpper.add(btnNewSimulations);
        pnlLeftUpper.add(btnDeleteSim);

        JPanel pnlPreviosSimulations = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        pnlPreviosSimulations.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));
        pnlPreviosSimulations.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_PREVIOS_SYM"), 0, 0, pnlPreviosSimulations.getFont().deriveFont(Font.BOLD)));

        createSimulationTree();
        JScrollPane paneSimulations = new JScrollPane(simulationTree);
        pnlPreviosSimulations.add(paneSimulations);

        pnlLeftSimulations.add(pnlLeftUpper);
        pnlLeftSimulations.add(pnlPreviosSimulations);

        pnlLeftSimulations.setBorder(BorderFactory.createEmptyBorder(5, 5, 1, 5));
        return pnlLeftSimulations;
    }

    //create the general panel in the systems tab
    private JPanel createGenralPanel() {

        /*JPanel pnlGeneral = new JPanel(generalPanelLayOut);
        pnlGeneral.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel pnlSystemName = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"20"}));
        pnlSystemName.add(new JLabel());
        pnlSystemName.add(txtSystemName);
        pnlSystemName.add(new JLabel());
        pnlSystemName.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BY_SYS_NAME"), 0, 0, pnlSystemName.getFont().deriveFont(Font.BOLD)));

        JPanel pnlSystemDes = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"95%"}));
        pnlSystemDes.add(new JLabel());
        pnlSystemDes.add(txtDescription);
        pnlSystemDes.add(new JLabel());
        pnlSystemDes.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CUS_INDEX_DESCRIPTION"), 0, 0, pnlSystemDes.getFont().deriveFont(Font.BOLD)));

        JPanel pnlPositionLimit = new JPanel(new FlexGridLayout(new String[]{"2%", "240", "50"}, new String[]{"20"}));
        pnlPositionLimit.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_POS_LIMIT"), 0, 0, pnlPositionLimit.getFont().deriveFont(Font.BOLD)));
        pnlPositionLimit.add(new JLabel());
        pnlPositionLimit.add(new JLabel(Language.getString("BT_LIMIT_POSITIONS")));
        pnlPositionLimit.add(spnPositionLimit);

        pnlGeneral.add(pnlSystemName);
        pnlGeneral.add(pnlSystemDes);
        pnlGeneral.add(pnlPositionLimit);*/

        JPanel pnlGeneral = new JPanel(generalPanelLayOut);

        JPanel pnlSystemName = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"20"}));
        pnlSystemName.add(new JLabel());
        pnlSystemName.add(txtSystemName);
        pnlSystemName.add(new JLabel());
        pnlSystemName.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BY_SYS_NAME"), 0, 0, pnlSystemName.getFont().deriveFont(Font.BOLD)));

        JPanel pnlSystemDes = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"95%"}));
        pnlSystemDes.add(new JLabel());
        pnlSystemDes.add(txtDescription);
        pnlSystemDes.add(new JLabel());
        pnlSystemDes.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CUS_INDEX_DESCRIPTION"), 0, 0, pnlSystemDes.getFont().deriveFont(Font.BOLD)));

        JPanel pnlPositionLimit = new JPanel(new FlexGridLayout(new String[]{"2%", "240", "50"}, new String[]{"20"}));
        pnlPositionLimit.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_POS_LIMIT"), 0, 0, pnlPositionLimit.getFont().deriveFont(Font.BOLD)));
        pnlPositionLimit.add(new JLabel());
        pnlPositionLimit.add(new JLabel(Language.getString("BT_LIMIT_POSITIONS")));
        pnlPositionLimit.add(spnPositionLimit);

        JLabel lblName = new JLabel(Language.getString("BY_SYS_NAME"));
        lblName.setFont(new Font("Arial", Font.BOLD, 12));

        JLabel lblDes = new JLabel(Language.getString("CUS_INDEX_DESCRIPTION"));
        lblDes.setFont(new Font("Arial", Font.BOLD, 12));

        JLabel lblPos = new JLabel(Language.getString("BT_POS_LIMIT"));
        lblPos.setFont(new Font("Arial", Font.BOLD, 12));

        JPanel pnl1 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        pnl1.add(new JPanel());
        pnl1.add(lblName);
        pnl1.add(new JPanel());

        pnlGeneral.add(pnl1);

        JPanel pnl2 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        pnl2.add(new JPanel());
        pnl2.add(txtSystemName);
        pnl2.add(new JPanel());
        pnlGeneral.add(pnl2);

        JPanel pnl3 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        pnl3.add(new JPanel());
        pnl3.add(lblDes);
        pnl3.add(new JPanel());

        pnlGeneral.add(pnl3);

        JPanel pnl4 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        pnl4.add(new JPanel());
        pnl4.add(txtDescription);
        pnl4.add(new JPanel());

        pnlGeneral.add(pnl4);

        JPanel pnl5 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        pnl5.add(new JPanel());
        pnl5.add(lblPos);
        pnl5.add(new JPanel());

        pnlGeneral.add(pnl5);

        JPanel pnl6 = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
        JPanel pnlTemp = new JPanel(new FlexGridLayout(new String[]{"230", "50"}, new String[]{"100%"}, 0, 0));

        pnlTemp.add(new JLabel(Language.getString("BT_LIMIT_POSITIONS")));
        pnlTemp.add(spnPositionLimit);
        pnl6.add(new JPanel());
        pnl6.add(pnlTemp);
        pnl6.add(new JPanel());

        pnlGeneral.add(pnl6);

        JPanel result = new JPanel(new FlexGridLayout(new String[]{"5", "100%", "5"}, new String[]{"100%"}, 0, 0));
        result.add(new JPanel());
        result.add(pnlGeneral);
        result.add(new JPanel());

        JPanel finalPanel = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"3%", "65%", "32%"}));
        result.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(result);
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());

        return finalPanel;
    }

    private JPanel createBuyOrderPanel() {

        /* txtBuyGrammar = new JTextPane();
       dialog1 = new FunctionDialog(txtBuyGrammar);

       buyDoc = txtBuyGrammar.getStyledDocument();
       txtBuyGrammar.setFont(new TWFont("Courier New", Font.PLAIN, 12));
       txtBuyGrammar.addKeyListener(this);
       setDocumentStyles(txtBuyGrammar, buyDoc);

       JPanel pnlBuyOrder = new JPanel(buyPanelLayOut);
       pnlBuyOrder.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

       JPanel pnlBuyStmt = new JPanel(new FlexGridLayout(new String[]{"2%", "96%", "2%"}, new String[]{"95%"}));
       pnlBuyStmt.add(new JLabel());
       JScrollPane pane = new JScrollPane(txtBuyGrammar);
       pnlBuyStmt.add(pane);
       pnlBuyStmt.add(new JLabel());
       pnlBuyStmt.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_BUY_STMT"), 0, 0, pnlBuyStmt.getFont().deriveFont(Font.BOLD)));

       JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"20", "120", "90"}, new String[]{"20"}, 5, 5));
       pnlButtons.add(new JLabel());
       pnlButtons.add(btnTestBuyGrammar);
       pnlButtons.add(btnfunctionsBuy);

       JPanel pnlEntitySize = new JPanel(new FlexGridLayout(new String[]{"20", "60", "140", "20", "60", "100"}, new String[]{"20"}));
       pnlEntitySize.add(new JLabel());
       pnlEntitySize.add(new JLabel(Language.getString("DEPTH_CALC_TYPE")));
       pnlEntitySize.add(cmbType);
       pnlEntitySize.add(new JLabel(""));
       pnlEntitySize.add(new JLabel(Language.getString("QUANTITY")));
       txtQuantity.setText("100");
       pnlEntitySize.add(txtQuantity);
       pnlEntitySize.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_ENTITY_SIZE"), 0, 0, pnlEntitySize.getFont().deriveFont(Font.BOLD)));

       pnlBuyOrder.add(pnlBuyStmt);
       pnlBuyOrder.add(pnlButtons);
       pnlBuyOrder.add(pnlEntitySize);

       return pnlBuyOrder;*/


        txtBuyGrammar = new JTextPane();
        dialog1 = new FunctionDialog(txtBuyGrammar);

        buyDoc = txtBuyGrammar.getStyledDocument();
        txtBuyGrammar.setFont(new TWFont("Courier New", Font.PLAIN, 12));
        txtBuyGrammar.addKeyListener(this);
        setDocumentStyles(txtBuyGrammar, buyDoc);

        JPanel pnlTop = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"330", "10", "65"}, 0, 0));

        JPanel pnlBuyOrder = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        pnlBuyOrder.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel pnlBuyStmt = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"95%"}));
        pnlBuyStmt.add(new JLabel());
        JScrollPane pane = new JScrollPane(txtBuyGrammar);
        pnlBuyStmt.add(pane);
        pnlBuyStmt.add(new JLabel());
        pnlBuyStmt.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_BUY_STMT"), 0, 0, pnlBuyStmt.getFont().deriveFont(Font.BOLD)));

        JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"120", "6", "90"}, new String[]{"20"}, 0, 10));

        pnlButtons.add(btnTestBuyGrammar);
        pnlButtons.add(new JLabel());
        pnlButtons.add(btnfunctionsBuy);

        JPanel pnlEntitySize = new JPanel(new FlexGridLayout(new String[]{"40", "140", "20", "60", "100"}, new String[]{"20"}));
        //pnlEntitySize.add(new JLabel());
        pnlEntitySize.add(new JLabel(Language.getString("DEPTH_CALC_TYPE")));
        pnlEntitySize.add(cmbType);
        pnlEntitySize.add(new JLabel(""));
        pnlEntitySize.add(new JLabel(Language.getString("QUANTITY")));
        txtQuantity.setText("100");
        pnlEntitySize.add(txtQuantity);
        //pnlEntitySize.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_ENTITY_SIZE"), 0, 0, pnlEntitySize.getFont().deriveFont(Font.BOLD)));

        JPanel pnlEntity = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20"}, 0, 5));

        JLabel lblEntity = new JLabel(Language.getString("BT_ENTITY_SIZE"));
        lblEntity.setFont(new Font("Arial", Font.BOLD, 12));
        pnlEntity.add(lblEntity);
        pnlEntity.add(pnlEntitySize);

        JPanel pnlBottom = new JPanel(new FlexGridLayout(new String[]{"1%", "99%"}, new String[]{"100%"}, 0, 0));
        pnlBottom.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

        pnlBottom.add(new JPanel());
        pnlBottom.add(pnlEntity);

        JLabel lblBuy = new JLabel(Language.getString("BT_BUY_STMT"));
        lblBuy.setFont(new Font("Arial", Font.BOLD, 12));

        JPanel pnlFirst = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "245", "30"}, 0, 0));
        pnlFirst.add(lblBuy);
        pnlFirst.add(pane);
        pnlFirst.add(pnlButtons);

        JPanel pnlSecond = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"100%"}, 0, 0));

        pnlSecond.add(new JPanel());
        pnlSecond.add(pnlFirst);
        pnlSecond.add(new JPanel());
        pnlSecond.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

        pnlTop.add(pnlSecond);
        pnlTop.add(new JPanel());
        pnlTop.add(pnlBottom);


        pnlBuyOrder.add(pnlTop);

        JPanel finalPanel = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"2%", "96%", "2%"}));
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(pnlBuyOrder);
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());

        return finalPanel;
    }

    private JPanel createSellOrderPanel() {

        /* txtSellGrammar = new JTextPane();
       dialog2 = new FunctionDialog(txtSellGrammar);
       sellDoc = txtSellGrammar.getStyledDocument();
       txtSellGrammar.setFont(new TWFont("Courier New", Font.PLAIN, 12));
       txtSellGrammar.addKeyListener(this);
       setDocumentStyles(txtSellGrammar, sellDoc);

       JPanel pnlSellOrder = new JPanel(sellPanelLayOut);
       pnlSellOrder.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

       JPanel pnlSellStmt = new JPanel(new FlexGridLayout(new String[]{"2%", "96%", "2%"}, new String[]{"95%"}));
       JScrollPane pane = new JScrollPane(txtSellGrammar);
       pnlSellStmt.add(new JLabel());
       pnlSellStmt.add(pane);
       pnlSellStmt.add(new JLabel());

       pnlSellStmt.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SELL_STMT"), 0, 0, pnlSellStmt.getFont().deriveFont(Font.BOLD)));

       JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"20", "120", "90"}, new String[]{"20"}, 5, 5));
       pnlButtons.add(new JLabel());
       pnlButtons.add(btnTestSellGrammar);
       pnlButtons.add(btnfunctionsSell);

       pnlSellOrder.add(pnlSellStmt);
       pnlSellOrder.add(pnlButtons);

       return pnlSellOrder;*/

        txtSellGrammar = new JTextPane();
        dialog2 = new FunctionDialog(txtSellGrammar);

        sellDoc = txtSellGrammar.getStyledDocument();
        txtSellGrammar.setFont(new TWFont("Courier New", Font.PLAIN, 12));
        txtSellGrammar.addKeyListener(this);
        setDocumentStyles(txtSellGrammar, sellDoc);


        JPanel pnlBuyOrder = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "280", "40"}, 0, 0));

        JScrollPane pane = new JScrollPane(txtSellGrammar);

        JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"120", "6", "90"}, new String[]{"20"}, 0, 10));

        pnlButtons.add(btnTestSellGrammar);
        pnlButtons.add(new JLabel());
        pnlButtons.add(btnfunctionsSell);

        JLabel lbl = new JLabel(Language.getString("BT_SELL_STMT"));
        lbl.setFont(new Font("Arial", Font.BOLD, 12));
        pnlBuyOrder.add(lbl);
        pnlBuyOrder.add(pane);
        pnlBuyOrder.add(pnlButtons);

        JPanel finalPanel = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"2%", "96%", "2%"}));


        JPanel pnl = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"80%"}));
        pnl.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        pnl.add(new JPanel());
        pnl.add(pnlBuyOrder);
        pnl.add(new JPanel());

        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(pnl);
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());
        finalPanel.add(new JPanel());

        return finalPanel;
    }

    //create properties tab panel of the simulations tab
    private JPanel createPropertiesPanel() {

        JPanel pnlPropertes = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"2%", "95%"}));

        propertiesTabbedPane = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);
        propertiesTabbedPane.addTab(Language.getString("SETTINGS"), createSettingsPanel());
        propertiesTabbedPane.addTab(Language.getString("BT_SYSTEMS"), createSystemsPanel());
        propertiesTabbedPane.addTab(Language.getString("BT_SECURITIES"), createSecuritiesPanel());
        propertiesTabbedPane.setSelectedIndex(0);

        pnlPropertes.add(new JLabel(""));
        pnlPropertes.add(propertiesTabbedPane);

        return pnlPropertes;
    }

    private JPanel createSystemsPanel() {

        JPanel pnlSystems = new JPanel(new FlexGridLayout(new String[]{"3%", "42%", "10%", "42%", "3%"}, new String[]{"80%"}));
        pnlSystems.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SELECT_SYSTEMS"), 0, 0, pnlSystems.getFont().deriveFont(Font.BOLD)));

        lstLeft = new JList();
        lstRight = new JList();
        createDefaultJLists();
        applyJlistProperites(lstLeft);
        applyJlistProperites(lstRight);

        JPanel pnlCenter = new JPanel(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"100%"}, 0, 5));
        JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20", "20", "20"}, 5, 7));

        pnlButtons.add(new JLabel(""));
        pnlButtons.add(new JLabel(""));
        pnlButtons.add(btnleftToRightSystems);
        pnlButtons.add(btnRightToLeftSystems);
        pnlButtons.add(btnRightToLeftSystemsAll);
        pnlButtons.add(new JLabel(""));
        pnlButtons.add(new JLabel(""));

        pnlCenter.add(new JLabel(""));
        pnlCenter.add(pnlButtons);
        pnlCenter.add(new JLabel(""));

        pnlSystems.add(new JLabel(""));
        pnlSystems.add(new JScrollPane(lstLeft));
        pnlSystems.add(pnlButtons);
        pnlSystems.add(new JScrollPane(lstRight));
        pnlSystems.add(new JLabel(""));

        return pnlSystems;
    }

    private JPanel createSecuritiesPanel() {

        pnlSecurities = new JPanel(new FlexGridLayout(new String[]{"2%", "35%", "12%", "49%", "2%"}, new String[]{"100%"}));
        pnlSecurities.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SELECT_SECS"), 0, 0, pnlSecurities.getFont().deriveFont(Font.BOLD)));

        JPanel pnlSymbols = new JPanel();
        pnlSymbols.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));
        createSymbolTree();
        loadTree();
        secutiryScrollPane = new JScrollPane(symbolTree);
        pnlSecurityTreePanel = new JPanel();
        //pnlSecurityTreePanel.setBackground(Color.BLACK);
        pnlSecurityTreePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%"}, 3, 3));
        treeSearchCompPanel = new JPanel();
        treeSearchCompPanel.setLayout(new FlexGridLayout(new String[]{"100%", "18"}, new String[]{"100%"}, 3, 3));
        treeSearchCompPanel.add(searchText);
        treeSearchCompPanel.add(searchButton);
        pnlSecurityTreePanel.add(treeSearchCompPanel);
        pnlSecurityTreePanel.add(secutiryScrollPane);

        JPanel pnlSymbolTable = new JPanel();
        pnlSymbolTable.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        JPanel pnlCenter = new JPanel(new FlexGridLayout(new String[]{"8%", "84%", "8%"}, new String[]{"100%"}, 0, 5));
        JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "20", "20", "20", "20"}, 5, 7));

        pnlButtons.add(new JLabel(""));
        pnlButtons.add(new JLabel(""));
        pnlButtons.add(btnleftToRightSecurities);
        pnlButtons.add(btnRightToLeftSecurities);
        pnlButtons.add(btnRightToLeftSecuritiesAll);
        pnlButtons.add(new JLabel(""));
        pnlButtons.add(new JLabel(""));

        pnlCenter.add(new JLabel(""));
        pnlCenter.add(pnlButtons);
        pnlCenter.add(new JLabel(""));

        addSelectedSymbolsTable();
        pnlSecurities.add(new JLabel(""));
        pnlSecurities.add(pnlSecurityTreePanel);
        pnlSecurities.add(pnlCenter);
        pnlSecurities.add(symbolTable);
        pnlSecurities.add(new JLabel(""));


        return pnlSecurities;
    }

    private void applyJlistProperites(JList list) {

        list.setSelectionBackground(Color.GRAY);
        list.setSelectionForeground(Color.BLACK);
        //list.setFont(new TWFont("Courier New", Font.PLAIN, 14));
        list.setFont(new TWFont("Arial Unicode MS", Font.PLAIN, 13));
        list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    private JPanel createSettingsPanel() {

        /*JPanel pnlSettings = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"18%", "27%"}, 0, 5));

        JPanel pnlSimulationName = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"20"}, 0, 5));
        pnlSimulationName.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SIM_NAME"), 0, 0, pnlSimulationName.getFont().deriveFont(Font.BOLD)));

        txtSimulationName = new TWTextField();

        pnlSimulationName.add(new JLabel());
        pnlSimulationName.add(txtSimulationName);
        pnlSimulationName.add(new JLabel());

        JPanel pnlParameters = new JPanel(new FlexGridLayout(new String[]{"80", "140", "140"}, new String[]{"20", "20"}, 10, 5));
        pnlParameters.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_TRADING_PARAMS"), 0, 0, pnlParameters.getFont().deriveFont(Font.BOLD)));

        JLabel lblInitialEquity = new JLabel(Language.getString("BT_INITIAL_EQUITY"));
        txtInitialEquity = new TWTextField();

        JLabel lblSize = new JLabel(Language.getString("BT_DEFAULT_SIZE"));
        txtSize = new TWTextField();

        pnlParameters.add(lblInitialEquity);
        pnlParameters.add(txtInitialEquity);
        pnlParameters.add(new JLabel(""));
        pnlParameters.add(lblSize);
        pnlParameters.add(cmbSize);
        pnlParameters.add(txtSize);

        pnlSettings.add(pnlSimulationName);
        pnlSettings.add(pnlParameters);

        return pnlSettings;*/

        JPanel pnlSettings = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20", "60"}, 0, 5));

        JPanel pnlSimulationName = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"20"}, 0, 5));

        JLabel lbl = new JLabel(Language.getString("BT_SIM_NAME"));
        lbl.setFont(new Font("Arial", Font.BOLD, 12));

        JLabel lbl2 = new JLabel(Language.getString("BT_TRADING_PARAMS"));
        lbl2.setFont(new Font("Arial", Font.BOLD, 12));

        txtSimulationName = new TWTextField();

        pnlSimulationName.add(new JLabel());
        pnlSimulationName.add(txtSimulationName);
        pnlSimulationName.add(new JLabel());

        JPanel pnlParameters = new JPanel(new FlexGridLayout(new String[]{"80", "140", "140"}, new String[]{"20", "20"}, 5, 5));
        //pnlParameters.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_TRADING_PARAMS"), 0, 0, pnlParameters.getFont().deriveFont(Font.BOLD)));

        JLabel lblInitialEquity = new JLabel(Language.getString("BT_INITIAL_EQUITY"));
        txtInitialEquity = new TWTextField();

        JLabel lblSize = new JLabel(Language.getString("BT_DEFAULT_SIZE"));
        txtSize = new TWTextField();

        pnlParameters.add(lblInitialEquity);
        pnlParameters.add(txtInitialEquity);
        pnlParameters.add(new JPanel());
        pnlParameters.add(lblSize);
        pnlParameters.add(cmbSize);
        pnlParameters.add(txtSize);

        pnlSettings.add(lbl);
        pnlSettings.add(txtSimulationName);
        pnlSettings.add(lbl2);
        pnlSettings.add(pnlParameters);

        JPanel pnl = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"100%"}, 0, 5));
        pnl.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

        pnl.add(new JPanel());
        pnl.add(pnlSettings);
        pnl.add(new JPanel());

        JPanel pnlFinal = new JPanel(new FlexGridLayout(new String[]{"1%", "98%", "1%"}, new String[]{"40%"}, 0, 5));
        pnlFinal.add(new JPanel());
        pnlFinal.add(pnl);
        pnlFinal.add(new JPanel());

        return pnlFinal;
    }

    public void createSystemsTree() {

        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();

        sysRoot = new DefaultMutableTreeNode(Language.getString("BT_SYSTEMS"), true);
        for (int i = 0; i < systems.size(); i++) {
            DefaultMutableTreeNode systemNode = new DefaultMutableTreeNode(systems.get(i), false);
            systemNode.setUserObject(systems.get(i));
            sysRoot.add(systemNode);
        }

        systemsTree = new JTree(sysRoot);
        BackTestingTreeRenderer btTreeRendere = new BackTestingTreeRenderer();
        systemsTree.setCellRenderer(btTreeRendere);
        systemsTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        systemsTree.putClientProperty("JTree.lineStyle", "Vertical");
        systemsTree.setBackground(new JPanel().getBackground());
        addSystemsTreeMouseListener();

    }

    private void addSystemToTree(BackTestSystemData system) {
        DefaultMutableTreeNode systemNode = new DefaultMutableTreeNode(system);
        systemNode.setUserObject(system);
        sysRoot.add(systemNode);
    }

    private void addSystemsTreeMouseListener() {
        systemsTree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                TreePath path = systemsTree.getPathForLocation(ev.getX(), ev.getY());
                if (path != null) {
                    systemsTree.setSelectionPath(path);
                    try {
                        /*if (previousSystem != null && isDirty(previousSystem)) {
                            int option = JOptionPane.showConfirmDialog(testingWindow, "Do you want to save the system?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (option != 1) {
                                BackTestingXmlService.updateSystemBySystemID(previousSystem, false);
                            }
                        }*/
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) systemsTree.getLastSelectedPathComponent();
                        if (node.getUserObject() instanceof BackTestSystemData) {
                            BackTestSystemData system = (BackTestSystemData) node.getUserObject();
                            loadSystemData(system);
                            previousSystem = extractSystemData();

                            btnDelete.setEnabled(true);
                        } else {
                            btnDelete.setEnabled(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public BackTestSystemData extractSystemData() {

        long id = System.currentTimeMillis();
        String name = txtSystemName.getText().trim();
        String des = txtDescription.getText();
        int pos = Integer.parseInt(String.valueOf(spnPositionLimit.getValue()));
        int buyType = cmbType.getSelectedIndex();
        int quantity = Integer.parseInt(txtQuantity.getText().trim());
        String buyGrammar = txtBuyGrammar.getText();
        String sellGrammar = txtSellGrammar.getText();


        return new BackTestSystemData(id, name, des, pos, buyGrammar, quantity, buyType, sellGrammar);
    }

    public void addActionListeners() {
        btnfunctionsBuy.addActionListener(this);
        btnfunctionsSell.addActionListener(this);
        btnNewSystem.addActionListener(this);
        btnfunctionsSell.addActionListener(this);
        btnfunctionsSell.addActionListener(this);
        btnCancel.addActionListener(this);
        btnDelete.addActionListener(this);
        btnTestBuyGrammar.addActionListener(this);
        btnTestSellGrammar.addActionListener(this);
        btnRightToLeftSystems.addActionListener(this);
        btnRightToLeftSystemsAll.addActionListener(this);
        btnleftToRightSystems.addActionListener(this);
        btnleftToRightSecurities.addActionListener(this);
        btnRightToLeftSecurities.addActionListener(this);
        btnRightToLeftSecuritiesAll.addActionListener(this);
        btnDeleteSim.addActionListener(this);
        btnNewSimulations.addActionListener(this);
        btnSave.addActionListener(this);
        btnSaveSimulation.addActionListener(this);
        btnStart.addActionListener(this);
        btnTestSys.addActionListener(this);
        btnTestSim.addActionListener(this);
        btnSystems.addActionListener(this);
        btnSimulations.addActionListener(this);

    }

    public void actionPerformed(ActionEvent e) {

        Object obj = e.getSource();
        if (obj.equals(btnCancel)) {
            this.setVisible(false);
        } else if (obj.equals(btnfunctionsBuy)) {
            dialog1.showWindow();
            markSyntax(txtBuyGrammar, buyDoc);
        } else if (obj.equals(btnfunctionsSell)) {
            dialog2.showWindow();
            markSyntax(txtSellGrammar, sellDoc);
        } else if (obj.equals(btnNewSystem)) {
            addNewSystem();
        } else if (obj.equals(btnSave)) {
            SaveSystemData();
        } else if (obj.equals(btnDelete)) {
            deleteSystem();
        } else if (obj.equals(btnTestBuyGrammar)) {
            if (isValidGrammar(txtBuyGrammar.getText())) {
                JOptionPane.showMessageDialog(this, Language.getString("BT_BUY_SUCCSS"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, Language.getString("BT_BUY_FAIL"), Language.getString("BT_ERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else if (obj.equals(btnTestSellGrammar)) {
            if (isValidGrammar(txtSellGrammar.getText())) {
                JOptionPane.showMessageDialog(this, Language.getString("BT_SELL_SUCCSS"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, Language.getString("BT_SELL_FAIL"), Language.getString("BT_ERROR"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else if (obj.equals(btnleftToRightSystems)) {
            swapSystemsFromLeftToRight(lstLeft, lstRight, false);
        } else if (obj.equals(btnRightToLeftSystems)) {
            swapSystemsFromLeftToRight(lstRight, lstLeft, false);
        } else if (obj.equals(btnRightToLeftSystemsAll)) {
            swapSystemsFromLeftToRight(lstLeft, lstRight, true);
        } else if (obj.equals(btnleftToRightSecurities)) {
            swapSecurities(true, false);
        } else if (obj.equals(btnRightToLeftSecurities)) {
            swapSecurities(false, false);
        } else if (obj.equals(btnRightToLeftSecuritiesAll)) {
            swapSecurities(false, true);
        } else if (obj.equals(btnDeleteSim)) {
            deleteSimulation();
        } else if (obj.equals(btnNewSimulations)) {
            addNewSimulation();
        } else if (obj.equals(btnSaveSimulation)) {
            saveSimulationData();
        } else if (obj.equals(btnStart)) {
            doSimulation();
        } else if (obj.equals(searchButton)) {
            searchTree(searchText.getText());
        } else if (obj.equals(btnTestSys) || obj.equals(btnSystems)) {

            CardLayout layout = (CardLayout) container.getLayout();
            layout.show(container, ID_SYSTEM);

        } else if (obj.equals(btnTestSim) || obj.equals(btnSimulations)) {

            CardLayout layout = (CardLayout) container.getLayout();
            layout.show(container, ID_SIMULATION);
        }
    }

    private void addNewSystem() {
        resetSystemData();
    }

    private void resetSystemData() {

        systemsTree.setSelectionRow(-1);
        systemsTabbedPane.setSelectedIndex(0);

        txtSystemName.setText(Language.getString("BT_NEW_SYSTEM"));
        txtDescription.setText("");
        spnPositionLimit.setValue(5);
        txtSellGrammar.setText("");
        txtBuyGrammar.setText("");
        txtQuantity.setText("1000");
        cmbType.setSelectedIndex(0);
    }

    private void loadSystemData(BackTestSystemData system) {

        if (system != null) {
            txtSystemName.setText(system.getSystemName());
            txtDescription.setText(system.getSystemDescription());
            spnPositionLimit.setValue(system.getGeneralPositions());
            txtQuantity.setText(String.valueOf(system.getBuyQuantitiy()));
            txtBuyGrammar.setText(system.getBuyGrammar());
            txtSellGrammar.setText(system.getSellGrammar());
            cmbType.setSelectedIndex(system.getBuyType());

            markSyntax(txtBuyGrammar, buyDoc);
            markSyntax(txtSellGrammar, sellDoc);
        }
    }

    public void deleteSystem() {

        int result = JOptionPane.showConfirmDialog(this, Language.getString("BT_CONFIM_DELETE_SYS"), Language.getString("BT_SYS_DELETION"), JOptionPane.YES_NO_OPTION);
        if (result == 1) { //No option
            return;
        }
        if (systemsTree != null && systemsTree.getSelectionRows() != null && systemsTree.getSelectionPath() != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) systemsTree.getLastSelectedPathComponent();
            BackTestSystemData system = (BackTestSystemData) node.getUserObject();
            BackTestingXmlService.updateSystemBySystemID(system, true);

            sysRoot.remove(node);

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    systemsTree.repaint();
                    systemsTree.updateUI();
                }
            });
            JOptionPane.showMessageDialog(this, Language.getString("BT_SYS_DELETE"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void SaveSystemData() {

        if (!isValidSystem()) {
            return;
        }

        long id = System.currentTimeMillis();
        String name = txtSystemName.getText().trim();
        String des = txtDescription.getText().trim();
        int pos = Integer.parseInt(String.valueOf(spnPositionLimit.getValue()));
        int buyType = cmbType.getSelectedIndex();
        int quantity = Integer.parseInt(txtQuantity.getText().trim());
        String buyGrammar = txtBuyGrammar.getText();
        String sellGrammar = txtSellGrammar.getText();

        //updating an existing simulation
        if (systemsTree != null && systemsTree.getSelectionRows() != null && systemsTree.getSelectionPath() != null) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) systemsTree.getLastSelectedPathComponent();
            if (node.getUserObject() instanceof BackTestSystemData) {
                BackTestSystemData system = (BackTestSystemData) node.getUserObject();
                final BackTestSystemData sys = new BackTestSystemData(system.getSystemID(), name, des, pos, buyGrammar, quantity, buyType, sellGrammar);

                if (isNameExistsInOtherSystems(system, txtSystemName.getText().trim())) {
                    JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_NAME2"), Language.getString("BT_INVALID_NAME"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
                new Thread("backtest grammar generating Thread - system updating") {
                    public void run() {
                        try {
                            String buyClassName = "_BTI_" + sys.getSystemID() + "_" + BackTestingConstatns.LOGIC_TYPE_BUY;
                            generateGrammar(sys.getBuyGrammar(), buyClassName);

                            String sellClassName = "_BTI_" + sys.getSystemID() + "_" + BackTestingConstatns.LOGIC_TYPE_SELL;
                            generateGrammar(sys.getSellGrammar(), sellClassName);

                        } catch (Exception e) {
                            e.printStackTrace();
                            System.out.println("*********** EXCEPTION IN BACKTEST GRAMMAR GENERATING *******");
                            return;
                        }
                    }
                }.start();

                BackTestingXmlService.updateSystemBySystemID(sys, false);
                node.setUserObject(sys);
                JOptionPane.showMessageDialog(this, Language.getString("BT_SYS_UPDATE"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            //adding a new system
            if (isSystemNameExists(txtSystemName.getText().trim())) {
                JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_NAME2"), Language.getString("BT_INVALID_NAME"), JOptionPane.ERROR_MESSAGE);
                return;
            }

            final BackTestSystemData system = extractSystemData();
            ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
            systems.add(system);
            BackTestingXmlService.serializeBackSysetms(systems);

            new Thread("backtest grammar generating Thread-system adding") {
                public void run() {
                    try {
                        String buyClassName = "_BTI_" + system.getSystemID() + "_" + BackTestingConstatns.LOGIC_TYPE_BUY;
                        generateGrammar(system.getBuyGrammar(), buyClassName);

                        String sellClassName = "_BTI_" + system.getSystemID() + "_" + BackTestingConstatns.LOGIC_TYPE_SELL;
                        generateGrammar(system.getSellGrammar(), sellClassName);

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("*********** EXCEPTION IN BACKTEST GRAMMAR GENERATING *******");
                        return;
                    }
                }
            }.start();

            addSystemToTree(system);
            JOptionPane.showMessageDialog(this, Language.getString("BT_ADD_SYS"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                systemsTree.repaint();
                systemsTree.updateUI();
            }
        });
    }

    private boolean isDirty(BackTestSystemData system) {

        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();

        for (int i = 0; i < systems.size(); i++) {
            BackTestSystemData sys = systems.get(i);

            if (sys.getSystemID() == system.getSystemID()) {
                system.assignValuesFrom(sys);
                break;
            }
        }


        if (!txtSystemName.getText().trim().equals(system.getSystemName())) {
            return true;
        } else if (!txtDescription.getText().trim().equals(system.getSystemDescription().trim())) {
            return true;
        } else if (!txtQuantity.getText().trim().equals(String.valueOf(system.getBuyQuantitiy()))) {
            return true;
        } else if (!String.valueOf(spnPositionLimit.getValue()).equals(String.valueOf(system.getGeneralPositions()))) {
            return true;
        } else if (!txtSellGrammar.getText().trim().equals(system.getSellGrammar().trim())) {
            return true;
        } else if (!txtBuyGrammar.getText().trim().equals(system.getBuyGrammar().trim())) {
            return true;
        } else if (cmbType.getSelectedIndex() != system.getBuyType()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSystemNameExists(String systemName) {
        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        for (int i = 0; i < systems.size(); i++) {
            if (systemName.equalsIgnoreCase(systems.get(i).getSystemName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isNameExistsInOtherSystems(BackTestSystemData system, String systemName) {
        ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
        for (int i = 0; i < systems.size(); i++) {
            if (system.getSystemID() == systems.get(i).getSystemID()) {
                continue;
            }
            if (systemName.equalsIgnoreCase(systems.get(i).getSystemName())) {
                return true;
            }
        }
        return false;
    }

    private boolean isValidSystem() {

        boolean invalidQuantity = false;

        try {
            int quantity = Integer.parseInt(txtQuantity.getText());
        } catch (Exception e) {
            invalidQuantity = true;
        }

        if (txtSystemName.getText().trim().equals("") || txtSystemName.getText().trim().length() <= 0) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_NAME"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (txtDescription.getText().trim().equals("") || txtDescription.getText().trim().length() <= 0) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_DES"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (invalidQuantity) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_QT"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!isValidGrammar(txtBuyGrammar.getText())) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_INVALID_BUY"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!isValidGrammar(txtSellGrammar.getText())) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_INVALID_SELL"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else {
            return true;
        }
    }

    /* ==================== custom indicator and grammar related =================================*/
    private boolean isValidGrammar(String grammar) {

        String indicatorName = "New Custom Indicator";
        boolean valid = true;
        IndicatorConfigInfo info = new IndicatorConfigInfo(
                "", grammar, "_SE_" + System.currentTimeMillis(), "true",
                indicatorName.substring(0, Math.min(8, indicatorName.length())),
                indicatorName, false, Color.BLUE, Color.BLUE, (byte) StockGraph.INT_GRAPH_STYLE_LINE, false,
                GraphDataManager.STYLE_SOLID, 1.0f);

        String source = parseAndGenerateCode(info);
        int result = compileCode(source, info.ClassName);
        //System.out.println("*************** result ************* " + result);
        if (result != 0) {
            //System.out.println("********* COMPILE ERROR ************");
            valid = false;
        }

        String srcPath = BackTestingConstatns.BACK_TEST_CLASS_PATH + info.ClassName + ".java";
        String indPath = BackTestingConstatns.BACK_TEST_CLASS_PATH + info.ClassName + ".ind";
        File file = new File(srcPath);
        File file2 = new File(indPath);
        if (file.exists()) {
            file.delete();
        }
        if (file2.exists()) {
            file2.delete();
        }
        return valid;
    }

    private void generateGrammar(String grammar, String className) {

        String indicatorName = "New Custom Indicator";
        IndicatorConfigInfo info = new IndicatorConfigInfo(
                "", grammar, className, "true",
                indicatorName.substring(0, Math.min(8, indicatorName.length())),
                indicatorName, false, Color.BLUE, Color.BLUE, (byte) StockGraph.INT_GRAPH_STYLE_LINE, false,
                GraphDataManager.STYLE_SOLID, 1.0f);

        String source = parseAndGenerateCode(info);
        int result = compileCode(source, info.ClassName);
        //System.out.println("*************** result ************* " + result);
        if (result != 0) {
            //System.out.println("********* COMPILE ERROR ************");
        }

        String srcPath = BackTestingConstatns.BACK_TEST_CLASS_PATH + info.ClassName + ".java";
        File file = new File(srcPath);
        if (file.exists()) {
            file.delete();
        }
    }

    public int compileCode(String source, String className) {
        int result = -1;
        try {
            result = TWCompiler.compile(getSourcePath(className, source), BackTestingConstatns.BACK_TEST_CLASS_PATH);

            if (result == 0) {
                File classFile = new File(BackTestingConstatns.BACK_TEST_CLASS_PATH + className + ".class");
                File fileRenamed = new File(BackTestingConstatns.BACK_TEST_CLASS_PATH + className + ".ind");
                if (fileRenamed.exists()) fileRenamed.delete();
                classFile.renameTo(fileRenamed);
                Encoder.encode(fileRenamed);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String parseAndGenerateCode(IndicatorConfigInfo info) {

        boolean testFailed = false;
        if (parser == null) {
            parser = new IndicatorParser(Settings.CHART_DATA_PATH + "/CustomIndicator.cgt");
        }
        CIProgram program = parser.parse(info.Grammer);
        String code = "code";
        if (program != null) {
            try {
                code = IndicatorFactory.GenerateCode(info, program);
                info.IsStrategy = program.isStrategy;
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("*************** PARSER ERROR *************");
            }
        } else {
            testFailed = true;
            code = "sd";
        }
        if (!testFailed) {
            //System.out.println("************* PARSER SUCCESS **************");
        }
        return code;
    }

    private boolean arrayContains(String[] syntaxKeyWords, String word) {
        boolean result = false;
        for (String syntaxKeyWord : syntaxKeyWords) {
            if (syntaxKeyWord.equals(word)) return true;
        }
        return result;
    }

    public void setDocumentStyles(JTextPane textPane, StyledDocument styleDoc) {
        Style style = textPane.addStyle("PINK", null);

        StyleConstants.setForeground(style, new Color(255, 0, 255));
        StyleConstants.setBold(style, false);
        style = textPane.addStyle("BLUE", null);
        StyleConstants.setBold(style, true);
        StyleConstants.setForeground(style, new Color(0, 0, 255));
        style = textPane.addStyle("BLACK", null);
        StyleConstants.setBold(style, false);
        StyleConstants.setForeground(style, new Color(0, 0, 0));
        style = textPane.addStyle("VIOLET", null);
        StyleConstants.setBold(style, true);
        StyleConstants.setForeground(style, new Color(128, 0, 128));
        style = textPane.addStyle("GREEN", null);
        StyleConstants.setBold(style, false);
        StyleConstants.setForeground(style, new Color(0, 180, 0));
        markSyntax(textPane, styleDoc);
    }

    public void markSyntax(JTextPane textPane, StyledDocument styleDoc) {
        //First initialize all
        String fullText = textPane.getText();
        int caretPos = textPane.getCaretPosition();
        fullText = fullText.replace("\r", ""); //remove all carriage return characters from the context
        textPane.setText(fullText);
        textPane.setCaretPosition(caretPos);
        styleDoc.setCharacterAttributes(0, textPane.getText().length(), textPane.getStyle("BLACK"), true);

        int charAt = 0;
        String[] lines = textPane.getText().split("\n");
        for (int i = 0; i < lines.length; i++) {
            StringTokenizer st = new StringTokenizer(lines[i], " \t();,+-/><=*", true);
            while (st.hasMoreTokens()) {
                String word = st.nextToken();
                charAt = textPane.getText().indexOf(word, charAt);
                if (arrayContains(IndicatorConstants.keyWords, word)) { // Check for keywords
                    styleDoc.setCharacterAttributes(charAt, word.length(), textPane.getStyle("BLUE"), true);
                } else if (arrayContains(IndicatorConstants.literals, word)) { //check for literals
                    styleDoc.setCharacterAttributes(charAt, word.length(), textPane.getStyle("PINK"), true);
                } else if (arrayContains(IndicatorConstants.indicators, word)) { //check for indicators
                    styleDoc.setCharacterAttributes(charAt, word.length(), textPane.getStyle("VIOLET"), true);
                } else { //others
                    styleDoc.setCharacterAttributes(charAt, word.length(), textPane.getStyle("BLACK"), true);
                }
                charAt += word.length();
            }
        }
        markComments(textPane, styleDoc);
    }

    private void markComments(JTextPane textPane, StyledDocument styleDoc) {
        String[] lines = textPane.getText().split("\n");
        boolean blockComment = false;
        int preCharCount = 0;
        for (String line : lines) {
            int startIndex = 0;
            if (blockComment) {
                int tmpIndex = line.indexOf("*/", startIndex);
                if (tmpIndex >= 0) {
                    int endIndex = tmpIndex + 1;
                    startIndex = endIndex;
                    // paint the half comment
                    styleDoc.setCharacterAttributes(preCharCount, endIndex + 1, textPane.getStyle("GREEN"), true);
                    blockComment = false;
                } else {
                    // paint the comment
                    styleDoc.setCharacterAttributes(preCharCount, line.length(), textPane.getStyle("GREEN"), true);
                    preCharCount += line.length() + 1;
                    continue;
                }
            }
            while (startIndex < line.length() - 1) {
                int blockCmntIndex = line.indexOf("/*", startIndex);
                int lineCmntIndex = line.indexOf("//", startIndex);
                if ((lineCmntIndex >= 0) || (blockCmntIndex >= 0)) {
                    int index;
                    if ((lineCmntIndex >= 0) && (blockCmntIndex >= 0)) {
                        index = Math.min(lineCmntIndex, blockCmntIndex);
                        blockComment = lineCmntIndex > blockCmntIndex;
                    } else if (lineCmntIndex >= 0) {
                        index = lineCmntIndex;
                    } else { // Block comment
                        index = blockCmntIndex;
                        blockComment = true;
                    }
                    int endIndex = line.length() - 1;
                    if (blockComment) {
                        int tmpIndex = line.indexOf("*/", startIndex);
                        if (tmpIndex > index) {
                            endIndex = tmpIndex + 1;
                            startIndex = endIndex;
                            blockComment = false;
                        } else {
                            startIndex = line.length();
                        }
                    } else {
                        startIndex = line.length();
                    }
                    // paint the comment
                    styleDoc.setCharacterAttributes(preCharCount + index, endIndex - index + 1,
                            textPane.getStyle("GREEN"), true);
                } else {
                    startIndex = line.length();
                }
            }
            preCharCount += line.length() + 1;
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(searchText)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (!searchText.getText().equals("")) {
                    searchTree(searchText.getText());
                    e.consume();
                    searchText.requestFocus(true);
                } else {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                if (searchText.getText().equals("")) {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                rootNode.clear();
                loadTree();
                e.consume();
                setInitialText();
            }
        } else {
            searchText.requestFocus();
            try {
                if (!Character.isWhitespace(e.getKeyChar()) && (!(e.getKeyChar() == KeyEvent.VK_ESCAPE))) {
                    searchText.getDocument().insertString(searchText.getCaretPosition(), "" + e.getKeyChar(), null);
                }
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setInitialText() {
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.setSelectionStart(0);
        searchText.setSelectionEnd(searchText.getText().length());
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT ||
                e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN) {
            return;
        }
        if (txtBuyGrammar.hasFocus()) {
            markSyntax(txtBuyGrammar, buyDoc);
        } else if (txtSellGrammar.hasFocus()) {
            markSyntax(txtSellGrammar, sellDoc);
        }
    }
    /* ==================== end of custom indicator and grammar related =================================*/


    /* =================================== simulation related ===========================================*/

    private void createSimulationTree() {

        ArrayList<BackTestSimulationData> alresultBsimData = BackTestingXmlService.deserializeSimulations();
        simRoot = new DefaultMutableTreeNode(Language.getString("BT_SIMULATIONS"));

        for (BackTestSimulationData item : alresultBsimData) {
            DefaultMutableTreeNode child1 = new DefaultMutableTreeNode(item);
            simRoot.add(child1);
        }

        simulationTree = new JTree(simRoot);
        BackTestingTreeRenderer btTreerendere = new BackTestingTreeRenderer();
        simulationTree.setCellRenderer(btTreerendere);
        simulationTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        simulationTree.setBackground(new JPanel().getBackground());
        addSimulationsTreeMouseListener();

    }

    private void addSimulationsTreeMouseListener() {
        simulationTree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                TreePath path = simulationTree.getPathForLocation(ev.getX(), ev.getY());
                if (path != null) {
                    simulationTree.setSelectionPath(path);
                    try {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode) simulationTree.getLastSelectedPathComponent();

                        if (node.getUserObject() instanceof BackTestSimulationData) {
                            BackTestSimulationData sim = (BackTestSimulationData) node.getUserObject();
                            loadSimulationData(sim);
                            btnDeleteSim.setEnabled(true);
                        } else {
                            btnDeleteSim.setEnabled(false);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void loadSimulationData(BackTestSimulationData data) {

        if (data != null) {
            txtSimulationName.setText(data.getSimulationName());
            txtInitialEquity.setText(prcFormatter.format(data.getInitialEquity()));
            cmbSize.setSelectedIndex(data.getDefaultType());
            txtSize.setText(String.valueOf(data.getDefaultSize()));
            loadJListsForSimulation(data);
            loadSymbolTableForSimulation(data);
        }
    }

    private void loadJListsForSimulation(BackTestSimulationData data) {

        try {
            ArrayList<BackTestSystemData> sims = BackTestingXmlService.getSystemsForSimulation(data);
            ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();

            //filling the relevant systems for a simulation
            String[] str = null;
            if (sims != null) {
                str = new String[sims.size()];
                for (int i = 0; i < sims.size(); i++) {
                    str[i] = sims.get(i).getSystemName();
                }
                lstRight.setListData(str);
                lstRight.repaint();
                lstRight.updateUI();

                String[] strResult = new String[systems.size() - sims.size()];
                int count = 0;
                for (int j = 0; j < systems.size(); j++) {
                    if (!containsName(str, systems.get(j).getSystemName())) {
                        strResult[count] = systems.get(j).getSystemName();
                        count++;
                    }
                }
                lstLeft.setListData(strResult);
                lstLeft.repaint();
                lstLeft.updateUI();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("***************************** exception in loading JLists from value changed event ************************* ");
        }
    }

    private void loadSymbolTableForSimulation(BackTestSimulationData sim) {

        selectedSymbols.clear();

        String[] keys = sim.getSecurities();
        if (keys != null) {
            for (int i = 0; i < keys.length; i++) {
                Stock st = DataStore.getSharedInstance().getStockObject(keys[i]);
                if (st != null) {
                    selectedSymbols.add(new Key(st.getSymbol(), st.getExchange(), st.getInstrumentType(), st.getShortDescription()));
                }
            }
            ((SmartTable) symbolTable.getTable()).adjustColumnWidthsToFit(40);
            symbolModel.getTable().updateUI();
        }
    }

    private boolean containsName(String[] array, String name) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(name)) {
                return true;
            }
        }
        return false;
    }

    private long[] getSelectedSystemIDs() {

        int count = lstRight.getModel().getSize();
        long result[] = new long[count];

        for (int i = 0; i < count; i++) {
            String systemName = (String) lstRight.getModel().getElementAt(i);
            result[i] = BackTestingXmlService.getSystemIDBySystemName(systemName);
        }
        return result;
    }

    private String[] getSelectedSecurities() {

        int count = symbolTable.getModel().getRowCount();
        String result[] = new String[count];

        for (int i = 0; i < count; i++) {

            Key key = (Key) symbolTable.getModel().getValueAt(i, -1);
            result[i] = key.getExchange() + "~" + key.getSymbol() + "~" + key.getInstrumentType();
        }

        return result;
    }

    private boolean isValidSimulation() {

        boolean invalidEquity = false;
        boolean invalidSize = false;

        try {
            double quantity = Double.parseDouble(txtInitialEquity.getText());
        } catch (Exception e) {
            invalidEquity = true;
        }
        try {
            int quantity = Integer.parseInt(txtSize.getText());
        } catch (Exception e) {
            invalidSize = true;
        }

        if (txtSimulationName.getText().trim().equals("") || txtSimulationName.getText().trim().length() <= 0) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_NAM_SIM"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (invalidEquity) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_EQ"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (invalidSize) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_VALID_SIZE"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (lstRight.getModel().getSize() == 0) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_ONE_SYS"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (symbolTable.getModel().getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, Language.getString("BT_ONE_SEC"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
            return false;
        } else {
            return true;
        }
    }

    private void saveSimulationData() {

        if (!isValidSimulation()) {
            return;
        }

        String name = txtSimulationName.getText().trim();
        int type = cmbSize.getSelectedIndex();
        int size = Integer.parseInt(txtSize.getText().trim());
        double equity = Double.parseDouble(txtInitialEquity.getText().trim());

        //updating an existing system
        if (simulationTree != null && simulationTree.getSelectionRows() != null && simulationTree.getSelectionPath() != null) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) simulationTree.getLastSelectedPathComponent();

            if (node.getUserObject() instanceof BackTestSimulationData) {
                BackTestSimulationData system = (BackTestSimulationData) node.getUserObject();
                BackTestSimulationData sim = new BackTestSimulationData(system.getSimulationID(), name, size, type, equity, 1.00008, getSelectedSystemIDs(), getSelectedSecurities());

                if (isNameExistsInOtherSimulations(system, txtSimulationName.getText().trim())) {
                    JOptionPane.showMessageDialog(this, Language.getString("BT_SIM_NAME_EXISTS"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }

                BackTestingXmlService.updateSimulationBySimulationID(sim, false);
                node.setUserObject(sim);
                JOptionPane.showMessageDialog(this, Language.getString("BT_SIM_UPDATE"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            //adding a new system
            if (isSimulationNameExists(txtSimulationName.getText().trim())) {
                JOptionPane.showMessageDialog(this, Language.getString("BT_SIM_NAME_EXISTS"), Language.getString("BT_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            //BackTestSystemData system = new BackTestSystemData(id, name, des, pos, buyGrammar, quantity, buyType, sellGrammar);
            BackTestSimulationData sim = extractSimulationData();
            ArrayList<BackTestSimulationData> sims = BackTestingXmlService.deserializeSimulations();
            sims.add(sim);
            BackTestingXmlService.serializeSimulations(sims);
            addSimulationToTree(sim);
            JOptionPane.showMessageDialog(this, Language.getString("BT_ADD_SIM"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                simulationTree.repaint();
                simulationTree.updateUI();
            }
        });
    }

    private void addNewSimulation() {
        resetSimulationData();
    }

    private void resetSimulationData() {

        simulationTree.setSelectionRow(-1);
        simulationsTabbedPane.setSelectedIndex(0);
        propertiesTabbedPane.setSelectedIndex(0);

        txtSimulationName.setText(Language.getString("BT_NEW_SIM"));
        txtInitialEquity.setText("10000");
        txtSize.setText("100");
        cmbSize.setSelectedIndex(0);
        createDefaultJLists();
        selectedSymbols.clear();
        btnDeleteSim.setEnabled(false);

    }

    private void addSelectedSymbolsTable() {

        ViewSetting settings = null;
        try {
            settings = ViewSettingsManager.getSummaryView("SIMPLE_TABLE_COLS");
            GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("SIMPLE_TABLE_COLS"));

            if (settings == null)
                throw (new Exception("View not found"));

            symbolTable = new Table();
            selectedSymbols = new ArrayList<Key>();
            symbolModel = new BackTestingSymbolTableModel(selectedSymbols);
            symbolModel.setViewSettings(settings);

            symbolTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            symbolTable.setModel(symbolModel);
            symbolModel.setTable(symbolTable, new BackTestingSymbolRenderer(symbolModel.getRendIDs()));
            symbolTable.getPopup().setTMTavailable(false);
            ((SmartTable) symbolTable.getTable()).adjustColumnWidthsToFit(40);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteSimulation() {

        int result = JOptionPane.showConfirmDialog(this, Language.getString("BT_CONFIM_DELETE_SIM"), Language.getString("BT_SIM_DELETION"), JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.NO_OPTION || result == JOptionPane.CANCEL_OPTION) { //No option
            return;
        }
        if (simulationTree != null && simulationTree.getSelectionRows() != null && simulationTree.getSelectionPath() != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) simulationTree.getLastSelectedPathComponent();
            BackTestSimulationData sim = (BackTestSimulationData) node.getUserObject();
            BackTestingXmlService.updateSimulationBySimulationID(sim, true);

            simRoot.remove(node);

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    simulationTree.repaint();
                    simulationTree.updateUI();
                }
            });
            JOptionPane.showMessageDialog(this, Language.getString("BT_SIM_DELETE"), Language.getString("BT_SUCCESS"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private boolean isSimulationNameExists(String simName) {
        ArrayList<BackTestSimulationData> sims = BackTestingXmlService.deserializeSimulations();
        for (int i = 0; i < sims.size(); i++) {
            if (simName.equalsIgnoreCase(sims.get(i).getSimulationName())) {
                return true;
            }
        }
        return false;
    }

    public void swapSecurities(boolean fromLeftToRight, boolean removeAll) {

        if (removeAll) {
            selectedSymbols.clear();
            symbolModel.updateGUI();
            symbolTable.getTable().updateUI();
        } else if (fromLeftToRight) {

            if (symbolTree != null && symbolTree.getSelectionRows() != null && symbolTree.getSelectionPath() != null) {

                if (symbolTree.getLastSelectedPathComponent() instanceof ChartSymbolNode) {
                    ChartSymbolNode node = (ChartSymbolNode) symbolTree.getLastSelectedPathComponent();

                    Key key = new Key(node.getSymbol(), node.getExchange(), node.getInstrumentType(), node.getShortDescription());
                    if (!isSymbolAlreadyExists(key)) {
                        selectedSymbols.add(key);
                        ((SmartTable) symbolTable.getTable()).adjustColumnWidthsToFit(40);
                        symbolModel.getTable().updateUI();
                    }
                }

            }
        } else {
            Key key = (Key) symbolTable.getTable().getModel().getValueAt(symbolTable.getTable().getSelectedRow(), -1);
            if (key != null) {
                selectedSymbols.remove(key);
                symbolModel.updateGUI();
                ((SmartTable) symbolTable.getTable()).adjustColumnWidthsToFit(40); //TODO:comment this line
                symbolTable.getTable().updateUI();

            }
        }

    }

    private boolean isSymbolAlreadyExists(Key key) {

        for (int i = 0; i < selectedSymbols.size(); i++) {
            Key k = selectedSymbols.get(i);
            if (k.getSymbol().equals(key.getSymbol()) && k.getExchange().equals(key.getExchange())
                    && k.getInstrumentType() == key.getInstrumentType()) {
                return true;
            }
        }

        return false;
    }

    private boolean isNameExistsInOtherSimulations(BackTestSimulationData sim, String simName) {
        ArrayList<BackTestSimulationData> sims = BackTestingXmlService.deserializeSimulations();
        for (int i = 0; i < sims.size(); i++) {
            if (sim.getSimulationID() == sims.get(i).getSimulationID()) {
                continue;
            }
            if (simName.equalsIgnoreCase(sims.get(i).getSimulationName())) {
                return true;
            }
        }
        return false;
    }

    private void addSimulationToTree(BackTestSimulationData sim) {
        DefaultMutableTreeNode simNode = new DefaultMutableTreeNode(sim);
        simNode.setUserObject(sim);
        simRoot.add(simNode);
    }

    public BackTestSimulationData extractSimulationData() {

        long id = System.currentTimeMillis();
        String name = txtSimulationName.getText().trim();
        int type = cmbSize.getSelectedIndex();
        int size = Integer.parseInt(txtSize.getText().trim());
        double equity = Double.parseDouble(txtInitialEquity.getText().trim());

        return new BackTestSimulationData(id, name, size, type, equity, 1.00008, getSelectedSystemIDs(), getSelectedSecurities());
    }

    private void swapSystemsFromLeftToRight(JList lstLeft, JList lstRight, boolean moveAll) {

        try {
            if (moveAll) {
                ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();
                String[] array = new String[systems.size()];
                for (int i = 0; i < systems.size(); i++) {
                    array[i] = systems.get(i).getSystemName();
                }

                lstLeft.setListData(array);
                lstLeft.updateUI();
                lstLeft.repaint();

                String[] str = new String[0];
                lstRight.setListData(str);
                lstRight.updateUI();
                lstRight.repaint();

                return;
            }

            //selected items from left side
            Object[] objLeft = (Object[]) lstLeft.getSelectedValues();
            String[] strLeft = new String[objLeft.length];
            for (int i = 0; i < objLeft.length; i++) {
                strLeft[i] = (String) objLeft[i];
            }

            int size = lstLeft.getModel().getSize();
            String[] strLeftAll = new String[size];
            for (int i = 0; i < size; i++) {
                strLeftAll[i] = (String) lstLeft.getModel().getElementAt(i);
            }
            String[] strLeftNew = new String[size - strLeft.length];
            int count = 0;
            for (int i = 0; i < strLeftAll.length; i++) {
                boolean contains = false;
                for (int j = 0; j < strLeft.length; j++) {
                    if (strLeftAll[i].equals(strLeft[j])) {
                        contains = true;
                    }
                }
                if (!contains) {
                    strLeftNew[count] = strLeftAll[i];
                    count++;
                }
            }

            lstLeft.setListData(strLeftNew);
            lstLeft.updateUI();
            lstLeft.repaint();

            size = lstRight.getModel().getSize();
            String[] strCurrent = new String[size];
            String[] strRight = new String[lstRight.getModel().getSize() + strLeft.length];

            for (int i = 0; i < size; i++) {
                strCurrent[i] = (String) lstRight.getModel().getElementAt(i);
                strRight[i] = (String) lstRight.getModel().getElementAt(i);
            }

            for (int i = size; i < strRight.length; i++) {
                strRight[i] = strLeft[i - size];
            }

            lstRight.setListData(strRight);
            lstRight.updateUI();
            lstRight.repaint();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("****************** exception in swapping systems *************");
        }
    }

    private void createDefaultJLists() {
        try {
            ArrayList<BackTestSystemData> systems = BackTestingXmlService.desirializeSystems();

            //filling the relevant systems for a simulation
            String[] str = null;
            if (systems != null) {
                str = new String[systems.size()];
                for (int i = 0; i < systems.size(); i++) {
                    str[i] = systems.get(i).getSystemName();
                }
                lstRight.setListData(new String[]{});
                lstRight.repaint();
                lstRight.updateUI();

                lstLeft.setListData(str);
                lstLeft.repaint();
                lstLeft.updateUI();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("***************************** exception in loading JLists from value changed event ************************* ");
        }
    }
    /* =================================== end of  simulation related ===========================================*/

    private void createSymbolTree() {
        rootNode = new ChartExchangeNode("Root", 0, "root");
        watchListReference = new Hashtable<String, ChartExchangeNode>();
        mainStore = new Hashtable<String, ChartSymbolNode>();
        filteredStore = new Hashtable<String, ChartSymbolNode>();
        searchButton = new TWButton(">>");
        searchButton.addActionListener(this);
        searchText = new TWTextField();
        searchText.addKeyListener(this);
        searchText.addFocusListener(this);
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.requestFocus(false);

        treeModel = new ChartSearchTreeModel(rootNode);
        symbolTree = new ChartNavigationTree(treeModel);
        symbolTree.setOpaque(true);
        symbolTree.addKeyListener(this);
        symbolTree.putClientProperty("JTree.lineStyle", "Vertical");
        symbolTree.setToggleClickCount(1);
        symbolTree.setRootVisible(false);
        symbolTree.setBackground(new JPanel().getBackground());
        symbolTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        ChartSymbolTreeRenderer renderer = new ChartSymbolTreeRenderer();
        symbolTree.setCellRenderer(renderer);

    }

    public void loadTree() {
        mainStore.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        ChartExchangeNode exgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        for (Market subMarket : exg.getSubMarkets()) {
                            Hashtable<String, ChartSymbolNode> marketHash = new Hashtable<String, ChartSymbolNode>();
                            String subName = subMarket.getDescription();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                            }
                            ChartExchangeNode tempvector = new ChartExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                            tempvector.setChildrenStore(marketHash);
                            exgVector.addNode(tempvector);
                        }
                        rootNode.addNode(exgVector);

                    } else {
                        Hashtable<String, ChartSymbolNode> exgHash = new Hashtable<String, ChartSymbolNode>();
                        Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                        while (exchageSymbols.hasMoreElements()) {
                            String symbol = (String) exchageSymbols.nextElement();
                            ChartSymbolNode sto = new ChartSymbolNode();
                            sto.setExchange(exg.getSymbol());
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                            sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                            sto.setType(ChartConstants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                sto.setShortDescription(st.getShortDescription());
                                sto.setOpenVal(st.getTodaysOpen());
                                sto.setCloseVal(st.getTodaysClose());
                                sto.setHighVal(st.getHigh());
                                sto.setLowVal(st.getLow());
                                sto.setDescription(st.getLongDescription());
                                st = null;
                            } catch (Exception e) {
                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                        }
                        ChartExchangeNode tempExgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setChildrenStore(exgHash);
                        rootNode.addNode(tempExgVector);
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                watchListReference.clear();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        Hashtable<String, ChartSymbolNode> watchListHash = new Hashtable<String, ChartSymbolNode>();
                        String[] symbolArray = watchlist.getSymbols();
                        for (int i = 0; i < symbolArray.length; i++) {
                            String key = symbolArray[i];
                            ChartSymbolNode sto = new ChartSymbolNode();
                            sto.setExchange(SharedMethods.getExchangeFromKey(key));
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                            sto.setType(ChartConstants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(key);
                                sto.setShortDescription(st.getShortDescription());
                                sto.setOpenVal(st.getTodaysOpen());
                                sto.setCloseVal(st.getTodaysClose());
                                sto.setHighVal(st.getHigh());
                                sto.setLowVal(st.getLow());
                                sto.setDescription(st.getLongDescription());
                                st = null;
                            } catch (Exception e) {
                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(key, sto);
                            watchListHash.put(key, sto);
                        }
                        ChartExchangeNode tempWatchListVector = new ChartExchangeNode(watchlist.getCaption(), -1, "");
                        tempWatchListVector.setChildrenStore(watchListHash);
                        watchListReference.put(watchlist.getId(), tempWatchListVector);
                        rootNode.addNode(tempWatchListVector);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            GUISettings.applyOrientation(symbolTree);
            symbolTree.updateUI();
            symbolTree.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void watchlistAdded(String listID) {
        rootNode.clear();
        loadTree();
    }

    /**
     * ************************REGION - watchlist & exchange listner methods *********************
     */
    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        rootNode.clear();
        loadTree();
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void subMarketStatsChanged(int oldStatus, Exchange exchange, Market subMarket) {
    }

    public void subMarketMustInitialize(Exchange exchange, Market market) {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {

        try {
            for (int i = 0; i < rootNode.getSymbolStore().size(); i++) {
                ChartExchangeNode ex = (ChartExchangeNode) rootNode.getSymbolStore().get(i);
                if (ex.getExchangeSymbol().equals(exchange.getSymbol())) {
                    ex.setMarketStatus(exchange.getMarketStatus());
                    updateTree();
                    symbolTree.repaint();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateTree() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                symbolTree.updateUI();
            }
        });
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        fireSymbolAddedEvent(key, listID);
        symbolTree.repaint();

    }

    public void symbolRemoved(String key, String listID) {
        fireSymbolRemovedEvent(key, listID);
        symbolTree.repaint();
    }

    public void watchlistRenamed(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRemoved(String listID) {
        rootNode.clear();
        loadTree();
    }

    /**
     * ************************watchlist & exchange listner methods *********************
     */
    public void exchangeAdded(Exchange exchange) {

    }

    public void fireSymbolRemovedEvent(String key, String id) {
        try {
            ChartSymbolNode sto = new ChartSymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(ChartConstants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).removeNode(sto);
            symbolTree.updateUI();
            symbolTree.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireSymbolAddedEvent(String key, String id) {
        try {
            if ((watchListReference.get(id)) == null) {
                return;
            }
            ChartSymbolNode sto = new ChartSymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(ChartConstants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).addNode(sto);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    symbolTree.updateUI();
                    symbolTree.repaint();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ************************#end of Region --watchlist & exchange listner methods *********************
     */
    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(searchText)) {
            if (searchText.getText().equals(Language.getString("SIDE_BAR_SEARCH"))) {
                searchText.setText("");
            }

        }
    }

    public void focusLost(FocusEvent e) {

    }

    public void searchTree(String criteria) {
        if ((!criteria.equals(""))) {
            Enumeration mainStoreenum = mainStore.elements();
            filteredStore.clear();
            while (mainStoreenum.hasMoreElements()) {
                ChartSymbolNode sto = (ChartSymbolNode) mainStoreenum.nextElement();
                if ((sto.getSymbol().contains(criteria.toUpperCase())) || (sto.getDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (sto.getDescription().contains(criteria))) {
                    filteredStore.put(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()), sto);
                }
            }
            rootNode.clear();
            try {

                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
                while (exchanges.hasMoreElements()) {
                    Exchange exg = (Exchange) exchanges.nextElement();
                    boolean ismatched = false;
                    if (exg.isDefault()) {
                        if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                            ChartExchangeNode exgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            for (Market subMarket : exg.getSubMarkets()) {
                                Hashtable<String, ChartSymbolNode> marketHash = new Hashtable<String, ChartSymbolNode>();
                                String subName = subMarket.getDescription();
                                Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                                String[] symbolList = subSymbols.getSymbols();
                                for (int i = 0; i < symbolList.length; i++) {
                                    ChartSymbolNode sto = new ChartSymbolNode();
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(subName);
                                    sto.setSymbol(SharedMethods.getSymbolFromExchangeKey(symbolList[i]));
                                    sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                    sto.setType(ChartConstants.symbolType);
                                    try {
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i]));
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                    }
                                    if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                        ismatched = true;
                                        marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i])), sto);
                                    }
                                }
                                if (ismatched) {
                                    ChartExchangeNode tempvector = new ChartExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                                    tempvector.setChildrenStore(marketHash);
                                    exgVector.addNode(tempvector);
                                }
                            }
                            if (ismatched) {
                                rootNode.addNode(exgVector);
                            }

                        } else {
                            Hashtable<String, ChartSymbolNode> exgHash = new Hashtable<String, ChartSymbolNode>();
                            Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                            while (exchageSymbols.hasMoreElements()) {
                                String symbol = (String) exchageSymbols.nextElement();
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(null);
                                sto.setSymbol(SharedMethods.getSymbolFromExchangeKey(symbol));
                                sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                    ismatched = true;
                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }

                            }
                            if (ismatched) {
                                ChartExchangeNode tempExgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setChildrenStore(exgHash);
                                rootNode.addNode(tempExgVector);
                            }
                        }
                    }

                }
                try {
                    Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                    for (WatchListStore watchlist : watchlists) {
                        if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                            boolean isWatchListMatched = false;
                            Hashtable<String, ChartSymbolNode> watchListHash = new Hashtable<String, ChartSymbolNode>();
                            String[] symbolArray = watchlist.getSymbols();
                            for (int i = 0; i < symbolArray.length; i++) {
                                String key = symbolArray[i];
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(SharedMethods.getExchangeFromKey(key));
                                sto.setMarket(null);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(key);
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                    isWatchListMatched = true;
                                    watchListHash.put(key, sto);
                                }
                            }

                            if (isWatchListMatched) {
                                ChartExchangeNode tempWatchListVector = new ChartExchangeNode(watchlist.getCaption(), -1, "");
                                tempWatchListVector.setChildrenStore(watchListHash);
                                rootNode.addNode(tempWatchListVector);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                symbolTree.repaint();
                symbolTree.updateUI();
                for (int i = 0; i < symbolTree.getRowCount(); i++) {
                    symbolTree.expandRow(i);
                }
                //ToolTipManager.sharedInstance().registerComponent(symbolTree);
                GUISettings.applyOrientation(pnlSecurityTreePanel);
                searchText.requestFocus(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rootNode.clear();
            loadTree();
            symbolTree.updateUI();
            symbolTree.repaint();
            secutiryScrollPane.updateUI();
            pnlSecurityTreePanel.updateUI();
            this.updateUI();
            searchText.requestFocus(false);
        }

    }

    /* *************************** Region - Simulation **************************************/
    public BackTestSimulationData getSelectedSimulation() {
        if (!isValidSimulation()) {
            return null;
        }

        String name = txtSimulationName.getText().trim();
        int type = cmbSize.getSelectedIndex();
        int size = Integer.parseInt(txtSize.getText().trim());
        double equity = Double.parseDouble(txtInitialEquity.getText().trim());

        //updating an existing system
        if (simulationTree != null && simulationTree.getSelectionRows() != null && simulationTree.getSelectionPath() != null) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) simulationTree.getLastSelectedPathComponent();
            BackTestSimulationData system = (BackTestSimulationData) node.getUserObject();
            return new BackTestSimulationData(system.getSimulationID(), name, size, type, equity, 1.00008, getSelectedSystemIDs(), getSelectedSecurities());
        } else {
            return null;
        }
    }

    public void doSimulation() {

        final BackTestSimulationData simulationData = getSelectedSimulation();

        if (simulationData != null) {

            //removes the existing tabbed pane
            if (simulationsTabbedPane.getTabComponentAt(1) != null) {
                simulationsTabbedPane.removeTab(1);
            }

            final WorkInProgressIndicator indicator = new WorkInProgressIndicator(WorkInProgressIndicator.SIMULATION_IN_PROGRESS);
            indicator.setVisible(true);

            indicator.setVisible(true);
            new Thread("Simulation Loading Thread") {
                public void run() {
                    try {
                        long entryTime = System.currentTimeMillis();
                        startSimulation(simulationData);
                        processResults(simulationData);
                        displayResults(simulationData);
                        System.gc();

                        System.out.println("***************** BACKTESTING SIMULATION TIME ************** " + (System.currentTimeMillis() - entryTime) / 1000d);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    indicator.dispose();
                }
            }.start();
        }
    }
    /* *************************** end of Region - Simulation ******************************/

    private void InitializeScanner() { //TODO: complete
        ScanManager scMger = new ScanManager();
        //scMger.TimeMode = ChartTimeMode.History;
        //scMger.Interval = (int)HistoryInterval.DAILY;
    }

    public void startSimulation(BackTestSimulationData simulationData) throws Exception {
        InitializeScanner();
        ArrayList<BackTestResult> results = simulationData.getResults();
        results.clear();
        BackTestDataManager GDM = new BackTestDataManager();
        for (int j = 0; j < simulationData.getSecurities().length; j++) {
            String cmpny = simulationData.getSecurities()[j];
            ArrayList<ScanRecord> records = OHLCStore.getInstance().addScanHistoryRequest(cmpny).getList();
            for (int i = 0; i < simulationData.getSystems().length; i++) {
                long[] btSystemsArr = simulationData.getSystems();
                BackTestSystemData system = BackTestingXmlService.getBackTestSystemByID(btSystemsArr[i]);
                if (system == null) continue;
                BackTestResult result = new BackTestResult(system, cmpny, simulationData.getInitialEquity(), simulationData.getDailyInterestRate());
                GDM.setAlRecords(new ArrayList(records));
                GDM.setResult(result);
                result.setRecords(records);
                scan(simulationData, GDM);
                results.add(result);
            }
        }
    }

    private int getDefaultTradedAmount(BackTestSimulationData simulationData, BackTestSystemData system, TransactionPoint transaction, double cashInHand) {
        switch (system.getBuyType()) {
            case BackTestingConstatns.BACKTEST_ENTRY_TYPE_NUMBER_OF_UNITS:
                return system.getBuyQuantitiy();
            case BackTestingConstatns.BACKTEST_ENTRY_TYPE_PERCENTAGE_OF_AVAULABLE_EQUITY:
                if (transaction.getNextRecord() == null) return 0;
                return (int) Math.floor(cashInHand * system.getBuyQuantitiy() / transaction.getNextRecord().Open / 100f);
            case BackTestingConstatns.BACKTEST_ENTRY_TYPE_TRANSACTION_COST:
                if (transaction.getNextRecord() == null) return 0;
                return (int) Math.floor(system.getBuyQuantitiy() / transaction.getNextRecord().Open);
            case BackTestingConstatns.BACKTEST_ENTRY_TYPE_DEFAULT_SIZE:
            default:
                switch (simulationData.getDefaultType()) {
                    case BackTestingConstatns.BACKTEST_ENTRY_TYPE_PERCENTAGE_OF_AVAULABLE_EQUITY:
                        if (transaction.getNextRecord() == null) return 0;
                        return (int) Math.floor(cashInHand * simulationData.getDefaultSize() / transaction.getNextRecord().Open / 100f);
                    case BackTestingConstatns.BACKTEST_ENTRY_TYPE_TRANSACTION_COST:
                        if (transaction.getNextRecord() == null) return 0;
                        return (int) Math.floor(simulationData.getDefaultSize() / transaction.getNextRecord().Open);
                    case BackTestingConstatns.BACKTEST_ENTRY_TYPE_NUMBER_OF_UNITS:
                    default:
                        return simulationData.getDefaultSize();

                }
        }
    }

    public void scan(BackTestSimulationData simulationData, BackTestDataManager GDM) throws Exception {

        long entryTime = System.currentTimeMillis();

        scanBuySellSignal(GDM, BackTestingConstatns.LOGIC_TYPE_BUY);
        scanBuySellSignal(GDM, BackTestingConstatns.LOGIC_TYPE_SELL);

        addLastTransaction(GDM);
        //System.out.println("@@@@@@@@ End Scanning after: " + (System.currentTimeMillis() - entryTime));
    }

    private void scanBuySellSignal(BackTestDataManager GDM, int logic) throws Exception {
        GDM.setCurrentBackTestLogic(logic);
        BackTestSystemData system = GDM.getResult().getSystemData();
        ChartProperties cp = new ChartProperties(null, "", GraphDataManager.ID_BASE, Color.black, null);
        Class indClass = Class.forName(system.getBuySellClassName(logic), false, new IndicatorLoader());
        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
        ChartProperties indicator = (ChartProperties) constructor.newInstance(null, cp.getSymbol(), Color.red, null);

        if (indicator == null) throw new Exception("Indicator creation error - " + logic);
        ((IndicatorBase) indicator).setPlotID(0);
        indicator.setOHLCPriority(GraphDataManager.INNER_Close);
        ((Indicator) indicator).insertIndicatorToGraphStore(GDM.getAlRecords(), GDM, logic);
    }

    private void addLastTransaction(BackTestDataManager GDM) {
        long lastTime = 0;
        int cnt = GDM.getResult().getTransactions().size();
        if (cnt > 0) {
            lastTime = ((TransactionPoint) (GDM.getResult().getTransactions().get(cnt - 1))).Time;

        }
        cnt = GDM.getAlRecords().size();
        if ((cnt > 0) && (lastTime != ((ScanRecord) GDM.getAlRecords().get(cnt - 1)).Time)) {
            TransactionPoint tp = new TransactionPoint(((ScanRecord) GDM.getAlRecords().get(cnt - 1)).Time, BackTestingConstatns.LOGIC_TYPE_LAST_BAR);
            tp.setCurrentRecord((ScanRecord) GDM.getAlRecords().get(cnt - 1));
            GDM.getResult().getTransactions().add(tp);
        }
    }

    /* ***********************     results processing and displaying ********************************************* */
    private void processResults(BackTestSimulationData simulationData) {
        ArrayList<BackTestResult> btSimResultArr = simulationData.getResults();

        for (BackTestResult btSimResult : btSimResultArr) {

            double cashInHand = simulationData.getInitialEquity();
            int stocksInHand = 0;
            int positions = 0;
            int maxPositions = btSimResult.getSystemData().getGeneralPositions();
            boolean mustSellAtLastBar = true;
            boolean isDynamicAmount = ((simulationData.getDefaultType() == BackTestingConstatns.BACKTEST_ENTRY_TYPE_DEFAULT_SIZE) && ((int) btSimResult.getSystemData().getBuyType() > 1))
                    || ((int) simulationData.getDefaultType() > 1);
            int stocksTraded = 100;
            if (!isDynamicAmount) {
                stocksTraded = getDefaultTradedAmount(simulationData, btSimResult.getSystemData(), null, cashInHand);
            }
            btSimResult.setPositions(new ArrayList<BackTestingPosition>());
            ArrayList<BackTestingPosition> simultaneousPositions = new ArrayList<BackTestingPosition>();
            long preTime = 0;
            int days;
            if ((btSimResult.getRecords() != null) && (btSimResult.getRecords().size() > 0)) {
                preTime = ((ScanRecord) (btSimResult.getRecords().get(0))).getTime();
            }

            int transactionCount = btSimResult.getTransactions().size();
            ArrayList trsnArray = btSimResult.getTransactions();
            for (int i = 0; i < transactionCount; i++) {
                TransactionPoint tPoint = (TransactionPoint) trsnArray.get(i);
                ////cannot sell if no stocksInHand - cannot buy if no cashInHand
                //if (((transaction.TransactionType == BackTestLogicType.Sell) || (transaction.TransactionType == BackTestLogicType.LastBar)) && (stocksInHand == 0)) continue;
                //if ((transaction.TransactionType == BackTestLogicType.Buy) && (cashInHand == 0)) continue;

                if (isDynamicAmount && (tPoint.getTransactionType() == BackTestingConstatns.LOGIC_TYPE_BUY) && (positions < maxPositions)) {
                    stocksTraded = getDefaultTradedAmount(simulationData, btSimResult.getSystemData(), tPoint, cashInHand);
                }
                int signalPos = Collections.binarySearch(btSimResult.getRecords(), new ScanRecord(tPoint.getCurrentRecord().Time));//btSimResult.getRecords().indexOfContainingValue(new ScanRecord(tPoint.getCurrentRecord().Time));

                int execPos = (tPoint.getNextRecord() == null) ? signalPos : Collections.binarySearch(btSimResult.getRecords(), new ScanRecord(tPoint.getNextRecord().Time));//btSimResult.getRecords().indexOfContainingValue(new ScanRecord(tPoint.getNextRecord().Time));
                int txnResult = BackTestingConstatns.TRANSACTION_RESULT_NONE;
                int actualStocksTraded = 0;
                String posStr = "";
                switch (tPoint.getTransactionType()) {
                    case BackTestingConstatns.LOGIC_TYPE_BUY:
                        if (tPoint.getNextRecord() == null) {
                            txnResult = BackTestingConstatns.TRANSACTION_RESULT_HOLD_TILL_NEXTBAR;
                        } else if (positions < maxPositions) {
                            days = Math.max((int) Math.round((tPoint.getNextRecord().Time - preTime) / 86400000d) - 1, 0);
                            cashInHand = cashInHand * Math.pow(simulationData.getDailyInterestRate(), days); // add interest for days excluding txn day
                            actualStocksTraded = (int) Math.min(stocksTraded, Math.floor(cashInHand / tPoint.getNextRecord().Open));
                            txnResult = (stocksTraded == actualStocksTraded) ? BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED : (actualStocksTraded == 0) ? BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_CASH : BackTestingConstatns.TRANSACTION_RESULT_PARTIALLY_EXECUTED;
                            stocksInHand += actualStocksTraded;
                            cashInHand -= actualStocksTraded * tPoint.getNextRecord().Open;
                            positions++;
                            cashInHand = simulationData.getDailyInterestRate() * cashInHand; // add interest for txn day
                            preTime = tPoint.getNextRecord().Time;
                            BackTestingPosition pos = new BackTestingPosition(btSimResult.getPositions().size() + 1, execPos, tPoint.getNextRecord().Open, actualStocksTraded);
                            simultaneousPositions.add(pos);
                            btSimResult.getPositions().add(pos);
                        } else {
                            txnResult = BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_POSITION_LIMIT;
                        }
                        tPoint.setTransactionDetail(actualStocksTraded, stocksInHand, cashInHand, txnResult, signalPos, execPos, Integer.toString(btSimResult.getPositions().size()));
                        break;
                    case BackTestingConstatns.LOGIC_TYPE_SELL:
                        if (tPoint.getNextRecord() == null) {
                            txnResult = BackTestingConstatns.TRANSACTION_RESULT_HOLD_TILL_NEXTBAR;
                        } else {
                            txnResult = (stocksInHand > 0) ? BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED : BackTestingConstatns.TRANSACTION_RESULT_CANCELLED_NO_STOCKS;
                            if (stocksInHand > 0) {
                                days = Math.max((int) Math.round((tPoint.getNextRecord().Time - preTime) / 86400000d), 0);
                                cashInHand = cashInHand * Math.pow(simulationData.getDailyInterestRate(), days);
                                actualStocksTraded = stocksInHand;
                                cashInHand += actualStocksTraded * tPoint.getNextRecord().Open;
                                stocksInHand = 0;
                                positions = 0;
                                preTime = tPoint.getNextRecord().Time;
                                for (BackTestingPosition pos : simultaneousPositions) {
                                    pos.setSellData(execPos, tPoint.getNextRecord().Open);
                                    posStr += ("".equals(posStr)) ? Integer.toString(pos.Count) : ", " + pos.Count;
                                }
                                simultaneousPositions.clear();
                            }
                        }
                        tPoint.setTransactionDetail(actualStocksTraded, stocksInHand, cashInHand, txnResult, signalPos, execPos, posStr);
                        break;
                    case BackTestingConstatns.LOGIC_TYPE_LAST_BAR:
                        for (BackTestingPosition pos : simultaneousPositions) {
                            pos.setSellData(signalPos, tPoint.getCurrentRecord().Close);
                            posStr += ("".equals(posStr)) ? Integer.toString(pos.Count) : ", " + pos.Count;
                        }
                        days = Math.max((int) Math.round((tPoint.getCurrentRecord().Time - preTime) / 86400000d), 0);
                        cashInHand = cashInHand * Math.pow(simulationData.getDailyInterestRate(), days);
                        if ((stocksInHand > 0) && mustSellAtLastBar) {
                            actualStocksTraded = stocksInHand;
                            cashInHand += actualStocksTraded * tPoint.getCurrentRecord().Close;
                            stocksInHand = 0;
                            positions = 0;
                            preTime = tPoint.getCurrentRecord().Time;
                            simultaneousPositions.clear();
                            tPoint.setTransactionDetail(actualStocksTraded, stocksInHand, cashInHand, BackTestingConstatns.TRANSACTION_RESULT_EXECEUTED, signalPos, execPos, posStr);
                        }
                        btSimResult.setLastPrice(tPoint.getCurrentRecord().Close);
                        break;
                }
            }
            btSimResult.setOpenPositions(positions);
            btSimResult.setCachInHand(cashInHand);
            btSimResult.setStocksInHand(stocksInHand);
            btSimResult.setTotalEquity(cashInHand + stocksInHand * btSimResult.getLastPrice());
            btSimResult.processResults();
        }
    }

    public void displayResults(BackTestSimulationData simData) {

        simulationsTabbedPane.addTab(Language.getString("BT_RESULTS"), createResultsTabbedPane(simData));
    }

    private JPanel createResultsTabbedPane(BackTestSimulationData simData) {

        JPanel resultsTabPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

        ArrayList<BackTestSystemData> systems = BackTestingXmlService.getSystemsForSimulation(simData);

        systemsTabs = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);
        if (systems.size() > 1) {
            systemsTabs.addTab(Language.getString("BT_COMPARE"), createSystemsComparisonPanel(simData));
        }

        btnArray = new CustomButton[simData.getSecurities().length][simData.getSystems().length];
        for (int i = 0; i < systems.size(); i++) {

            BackTestSystemData system = systems.get(i);
            long systemId = system.getSystemID();
            systemsTabs.addTab(system.getSystemName(), createSystemWiseTabPanel(system, simData, systemId, i));
        }

        systemsTabs.setSelectedIndex(0);
        resultsTabPanel.add(systemsTabs);

        return resultsTabPanel;
    }

    private BackTestResult getResultBySystemSecutiry(BackTestSimulationData simData, long SystemId, String secSymbol) {
        ArrayList<BackTestResult> simResultsArr = simData.getResults();
        for (BackTestResult btResult : simResultsArr) {
            if (SystemId == btResult.getSystemID() && secSymbol.equals(btResult.getSymbol())) {
                return btResult;
            }
        }
        return null;

    }

    private JPanel createSystemWiseTabPanel(BackTestSystemData sysData, BackTestSimulationData simData, long SystemId, int columnID) {

        final String[] securities = simData.getSecurities();

        String[] heights = new String[securities.length + 1];
        for (int i = 0; i < heights.length; i++) {
            heights[i] = "22";
        }

        ArrayList<CustomButton> btnList = new ArrayList<CustomButton>();
        final CustomButtonGroup group = new CustomButtonGroup(btnList);

        JPanel pnlSummary = new JPanel(new FlexGridLayout(new String[]{"100%"}, heights, 4, 4)); //TODO::
        final JPanel pnlDetailsymbolSummary = new JPanel(new CardLayout());

        //pnlSummary.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        CustomButton btnSummary = new CustomButton(Language.getString("BT_SUMMARY"));
        btnSummary.setBackground(new JButton().getBackground());
        btnSummary.setForeground(Theme.getColor("BT_SELECTED_LABEL_FOREGROUND"));
        group.add(btnSummary);
        if (simData.getSecurities().length > 1) {
            pnlSummary.add(btnSummary);
            final String SUMMARY_ID = "SUMMARY" + SystemId;
            pnlDetailsymbolSummary.add(createSecurityComparisonPanel(simData, SystemId), SUMMARY_ID);
            btnSummary.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    CardLayout layout = (CardLayout) pnlDetailsymbolSummary.getLayout();
                    layout.show(pnlDetailsymbolSummary, SUMMARY_ID);
                }
            });
        }


        FontMetrics metrics = btnSummary.getFontMetrics(btnSummary.getFont());
        int maxStringWidth = metrics.stringWidth(btnSummary.getText());
        for (int j = 0; j < securities.length; j++) {
            String[] str = securities[j].split("~", 3);
            maxStringWidth = Math.max(maxStringWidth, metrics.stringWidth(str[1]));
        }
        for (int j = 0; j < securities.length; j++) {
            String[] str = securities[j].split("~", 3);

            CustomButton btnSYmbol = new CustomButton(str[1]);
            btnSYmbol.setBackground(new Color(232, 232, 232));
            btnSYmbol.setForeground(Theme.getColor("BT_NOT_SELECTED_LABEL_FOREGROUND"));
            btnArray[j][columnID] = btnSYmbol;
            group.add(btnSYmbol);
            pnlSummary.add(btnSYmbol);
            BackTestResult btResult = getResultBySystemSecutiry(simData, SystemId, securities[j]);
            if (btResult != null) { //if no result corroponding to system secutiry combination
                BackTestResultPanel pnlRsultsForSymbol = new BackTestResultPanel(btResult, simData);
                pnlDetailsymbolSummary.add(pnlRsultsForSymbol, securities[j]);
            } else {
                JOptionPane.showMessageDialog(this, "no Results found");
                return new JPanel();
            }


            final int j1 = j;
            btnSYmbol.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    CardLayout layout = (CardLayout) pnlDetailsymbolSummary.getLayout();
                    layout.show(pnlDetailsymbolSummary, securities[j1]);
                }
            });
        }

        JPanel pnlSystems = new JPanel(new FlexGridLayout(new String[]{String.valueOf(maxStringWidth + 30), "100%"}, new String[]{"100%"}, 5, 0));
        pnlSystems.add(new JScrollPane(pnlSummary));  //summary panel
        pnlSystems.add(pnlDetailsymbolSummary);  //left symbol wise details panel

        return pnlSystems;
    }

    private JPanel createSecurityComparisonPanel(BackTestSimulationData simulationData, long systemID) {

        JPanel pnlFinal = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"60%", "40%"}, 0, 0));

        JPanel pnlBarChart = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        pnlBarChart.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SEC_COMPARE"), 0, 0, pnlBarChart.getFont().deriveFont(Font.BOLD)));

        JPanel pnlSecurityDetails = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        pnlSecurityDetails.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SEC_COMPARE"), 0, 0, pnlSecurityDetails.getFont().deriveFont(Font.BOLD)));


        ArrayList<DetailedSecuritySummary> secSummaries = new ArrayList<DetailedSecuritySummary>();

        /********************** rows *************************/

        ArrayList<BackTestResult> results = simulationData.getResults();

        int secCount = simulationData.getSecurities().length;
        int sysCount = simulationData.getSystems().length;

        ArrayList<BackTestResult> systemResults = new ArrayList<BackTestResult>();

        for (BackTestResult result1 : results) {
            if (result1.getSystemID() == systemID) {
                systemResults.add(result1);
            }
        }

        double[] yArr = new double[secCount];
        if (secCount > 1) {
            for (int i = 0; i < systemResults.size(); i++) {
                BackTestResult result = systemResults.get(i);

                double profit = result.getTotalEquity() - simulationData.getInitialEquity();
                double profitPercent = 100 * profit / simulationData.getInitialEquity();
                int smblID = (i + 1);
                //int seriesID = (int) Math.ceil((i + 1.0f) / sysCount);
                int seriesID = i + 1;
                //System.out.println("*********** series id ********" + seriesID);

                //if ((i + 1) % sysCount == 0) {
                yArr[i] = profitPercent;
                String strValue = priceFormatter.format(profitPercent) + "%";
                //Detailed Sexcurity Comparison

                String ApL = "";
                if (result.getAverageLoss() == 0) {
                    ApL = "N/A";
                } else {
                    double avgPoverL = (double) Math.abs(result.getAverageProfit() / result.getAverageLoss());
                    ApL = priceFormatter.format(avgPoverL);
                }

                DetailedSecuritySummary summary = new DetailedSecuritySummary(seriesID, result.getSymbol(), result.getSymbol(), priceFormatter.format(profit),
                        strValue, String.valueOf(result.getTradeCount()), String.valueOf(result.getProfitableTrades()), ApL);

                secSummaries.add(summary);

            }
            pnlBarChart.add(new SimpleBarChart(yArr, "")); //drawing the bar chart
        }


        /* double[] yArr = new double[secCount];
                if (secCount > 1) {
                    for (int i = 0; i < results.size(); i++) {
                        BackTestResult result = results.get(i);

                        double profit = result.getTotalEquity() - simulationData.getInitialEquity();
                        double profitPercent = 100 * profit / simulationData.getInitialEquity();
                        int smblID = (i + 1);
                        int seriesID = (int) Math.ceil((i + 1.0f) / sysCount);
                        //System.out.println("*********** series id ********" + seriesID);

                        if ((i + 1) % sysCount == 0) {
                            yArr[seriesID - 1] = profitPercent;
                            String strValue = priceFormatter.format(profitPercent) + "%";
                            //Detailed Sexcurity Comparison

                            String ApL = "";
                            if (result.getAverageLoss() == 0) {
                                ApL = "N/A";
                            } else {
                                double avgPoverL = (double) Math.abs(result.getAverageProfit() / result.getAverageLoss());
                                ApL = priceFormatter.format(avgPoverL);
                            }

                            DetailedSecuritySummary summary = new DetailedSecuritySummary(seriesID, result.getSymbol(), result.getSymbol(), priceFormatter.format(profit),
                                    strValue, String.valueOf(result.getTradeCount()), String.valueOf(result.getProfitableTrades()), ApL);

                            secSummaries.add(summary);
                        }

                    }
                    pnlBarChart.add(new SimpleBarChart(yArr, "Securities Performance Comparison")); //drawing the bar chart
                    System.out.println("********** Y ARRAY ********** " + yArr[0]);
                }
        */

        /****************** secirity compare ********************************/
        ViewSetting settings = null;
        try {

            settings = ViewSettingsManager.getSummaryView("DETAIL_SECURITY_SUMMARY_COLS");
            GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("DETAIL_SECURITY_SUMMARY_COLS"));

            if (settings == null)
                throw (new Exception("View not found"));

            final Table secCompareTable = new Table();
            final DetailSecurityTableModel model = new DetailSecurityTableModel(secSummaries);
            model.setViewSettings(settings);
            secCompareTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            secCompareTable.setModel(model);
            model.setTable(secCompareTable, new DetailSecurityTableRenderer(model.getRendIDs()));
            ((SmartTable) secCompareTable.getTable()).adjustColumnWidthsToFit(40);
            secCompareTable.updateUI();

            secCompareTable.getTable().addMouseListener(new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
                    try {
                        if (e.getClickCount() > 1) {

                            int row = secCompareTable.getTable().getSelectedRow();
                            int column = systemsTabs.getSelectedIndex() - 1;

                            CustomButton btn = btnArray[row][column];
                            if (btn != null) {
                                btn.doClick();
                            }
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            /****************** end of secirity compare ********************************/

            pnlSecurityDetails.add(secCompareTable);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("**************** security details table *********************");
        }

        pnlFinal.add(pnlBarChart);
        pnlFinal.add(pnlSecurityDetails);

        return pnlFinal;
    }

    private JPanel createSystemsComparisonPanel(BackTestSimulationData simulationData) {


        /*JPanel pnlComparison = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"60%", "40%"}, 0, 0));

  JPanel pnlBarChart = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
  pnlBarChart.setBorder(BorderFactory.createTitledBorder(null, "Systems Performance Comparison", 0, 0, pnlBarChart.getFont().deriveFont(Font.BOLD)));


  // **************** create system comparison chart ***************//*
        ArrayList<BackTestResult> results = simulationData.getResults();
        ArrayList<DetailedSystemSummary> systemSummaries = new ArrayList<DetailedSystemSummary>();
        int sysCount = simulationData.getSystems().length;
        int symblCount = simulationData.getSecurities().length;
        double[] yArr = new double[sysCount];
        //tab system summary
        if (sysCount > 1) {

            double profitPercent = 0, bestP = -Double.MAX_VALUE, worstP = Double.MAX_VALUE;
            int tradeSum = 0, tradeSumP = 0, tradeSumU = 0;

            for (int i = 0; i < results.size(); i++) {
                BackTestResult result = results.get(i);
                double profit = result.getTotalEquity() - simulationData.getInitialEquity();
                profitPercent += 100 * profit / simulationData.getInitialEquity();
                int seriesID = (int) Math.ceil((i + 1.0f) / symblCount);
                bestP = Math.max(bestP, profit);
                worstP = Math.min(worstP, profit);
                tradeSum = tradeSum + result.getTradeCount();
                tradeSumP = tradeSumP + result.getProfitableTrades();
                tradeSumU = tradeSumU + result.getUnprofitableTrades();

                if ((i + 1) % symblCount == 0) {
                    //Systems Performance Summary Chart

                    double avgProfitPercent = profitPercent / symblCount;
                    yArr[seriesID - 1] = avgProfitPercent;
                    String strValue = priceFormatter.format(avgProfitPercent) + "%";
                    //Detailed System Comparison
                    double avgProfit = avgProfitPercent * simulationData.getInitialEquity() / 100;
                    double avgTrades = (double) tradeSum / symblCount;
                    double avgTradesP = (double) tradeSumP / symblCount;
                    double avgTradesU = (double) tradeSumU / symblCount;

                    String ApL = "";
                    if (result.getAverageLoss() == 0) {
                        ApL = "N/A";
                    } else {
                        double avgPoverL = (double) Math.abs(result.getAverageProfit() / result.getAverageLoss());
                        ApL = priceFormatter.format(avgPoverL);
                    }

                    DetailedSystemSummary detSysSummary = new DetailedSystemSummary(seriesID, result.getSystemData().getSystemName(),
                            priceFormatter.format(avgProfit), strValue,
                            priceFormatter.format(bestP), priceFormatter.format(worstP), tradeFormatter.format(avgTrades),
                            tradeFormatter.format(avgTradesP) + "/" + priceFormatter.format(avgTradesU), ApL);

                    systemSummaries.add(detSysSummary);
                    profitPercent = 0;
                    bestP = -Double.MAX_VALUE;
                    worstP = Double.MAX_VALUE;
                    tradeSum = 0;
                    tradeSumP = 0;
                    tradeSumU = 0;
                }
            }
        }
        pnlBarChart.add(new SimpleBarChart(yArr, "Systems Performance Comparison"));
        /*************** end of create system comparison chart ***************/


        JPanel pnlComparison = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"60%", "40%"}, 0, 0));
        JPanel pnlBarChart = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

        /*************** create system comparison chart ***************/
        ArrayList<BackTestResult> results = simulationData.getResults();

        int sysCount = simulationData.getSystems().length;
        int symblCount = simulationData.getSecurities().length;
        double[] yArr = new double[sysCount];

        ArrayList<DetailedSystemSummary> systemSummaries = new ArrayList<DetailedSystemSummary>(sysCount);

        long[] systemIDs = simulationData.getSystems();
        //tab system summary
        if (sysCount > 1) {

            for (int j = 0; j < systemIDs.length; j++) {

                double profitPercent = 0, bestP = -Double.MAX_VALUE, worstP = Double.MAX_VALUE;
                int tradeSum = 0, tradeSumP = 0, tradeSumU = 0;
                String ApL = "N/A";

                for (int i = 0; i < results.size(); i++) {

                    BackTestResult result = results.get(i);

                    if (systemIDs[j] == result.getSystemID()) {

                        double profit = result.getTotalEquity() - simulationData.getInitialEquity();
                        profitPercent += 100 * profit / simulationData.getInitialEquity();
                        //int seriesID = (int) Math.ceil((i + 1.0f) / symblCount);
                        bestP = Math.max(bestP, profit);
                        worstP = Math.min(worstP, profit);
                        tradeSum = tradeSum + result.getTradeCount();
                        tradeSumP = tradeSumP + result.getProfitableTrades();
                        tradeSumU = tradeSumU + result.getUnprofitableTrades();
                    }

                    if (result.getAverageLoss() != 0) {
                        double avgPoverL = Math.abs(result.getAverageProfit() / result.getAverageLoss());
                        ApL = priceFormatter.format(avgPoverL);
                    }
                }

                double avgProfitPercent = profitPercent / symblCount;
                String strValue = priceFormatter.format(avgProfitPercent) + "%";
                //Detailed System Comparison
                double avgProfit = avgProfitPercent * simulationData.getInitialEquity() / 100;
                double avgTrades = (double) tradeSum / symblCount;
                double avgTradesP = (double) tradeSumP / symblCount;
                double avgTradesU = (double) tradeSumU / symblCount;

                String systemName = BackTestingXmlService.getBackTestSystemByID(systemIDs[j]).getSystemName();
                DetailedSystemSummary detSysSummary = new DetailedSystemSummary((j + 1), systemName,
                        priceFormatter.format(avgProfit), strValue,
                        priceFormatter.format(bestP), priceFormatter.format(worstP), tradeFormatter.format(avgTrades),
                        tradeFormatter.format(avgTradesP) + "/" + priceFormatter.format(avgTradesU), ApL);

                systemSummaries.add(detSysSummary);
                yArr[j] = avgProfitPercent;//chart y array
            }
            pnlBarChart.add(new SimpleBarChart(yArr, ""));
        }
        /*************** end of create system comparison chart ***************/


        JPanel pnlSystemDetails = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

        ViewSetting settings = null;
        try {

            settings = ViewSettingsManager.getSummaryView("DETAIL_SYSTEM_SUMMARY_COLS");
            GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("DETAIL_SYSTEM_SUMMARY_COLS"));

            if (settings == null)
                throw (new Exception("View not found"));

            final Table systemCompareTable = new Table();
            final DetailedSystemModel model = new DetailedSystemModel(systemSummaries);
            model.setViewSettings(settings);
            systemCompareTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            systemCompareTable.setModel(model);
            model.setTable(systemCompareTable, new DetailedSystemRenderer(model.getRendIDs()));
            ((SmartTable) systemCompareTable.getTable()).adjustColumnWidthsToFit(40);

            systemCompareTable.updateUI();


            /************* add mouse listenet to table *************/
            systemCompareTable.getTable().addMouseListener(new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
                    try {
                        if (e.getClickCount() > 1) {

                            int row = systemCompareTable.getTable().getSelectedRow();
                            DetailedSystemSummary summary = (DetailedSystemSummary) model.getValueAt(row, -1);
                            int index = systemsTabs.getIndexForText(summary.getSystemName());
                            if (systemsTabs.getTabCount() > 1) {
                                systemsTabs.setSelectedIndex(index);
                                systemsTabs.updateUI();
                            } else {
                                systemsTabs.setSelectedIndex(row);
                            }
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });

            /************* end of add mouse listenet to table *************/
            pnlSystemDetails.add(systemCompareTable);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("**************** equity details table *********************");
        }

        pnlSystemDetails.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_DETAIL_SYS_COMAPRE"), 0, 0, pnlSystemDetails.getFont().deriveFont(Font.BOLD)));

        JPanel pnlUpper = new JPanel(new FlexGridLayout(new String[]{"5%", "90%", "5%"}, new String[]{"100%"}, 0, 0));
        pnlUpper.setBorder(BorderFactory.createTitledBorder(null, Language.getString("BT_SYS_COMPARE"), 0, 0, pnlBarChart.getFont().deriveFont(Font.BOLD)));
        pnlUpper.add(new JPanel());
        pnlUpper.add(pnlBarChart);
        pnlUpper.add(new JPanel());

        pnlComparison.add(pnlUpper);
        pnlComparison.add(pnlSystemDetails);

        return pnlComparison;

    }

    class NodeCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            return ((ChartExchangeNode) o1).getName().compareTo(((ChartExchangeNode) o2).getName());
        }
    }
    /* ***********************  end of results processing and  displaying ********************************************* */

    public class MyButton extends TWButton {

        public MyButton(String text) {
            super(text);
        }

        public Insets getInsets() {
            return new Insets(0, 0, 0, 0);
        }
    }

}
