package com.isi.csvr.chart.backtesting;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: May 29, 2008
 * Time: 12:33:51 PM
 * To change this template use File | Settings | File Templates.
 */


// Copyright (c) 2000 Integrated Systems International (ISI)
public class BackTestingTreeRenderer implements TreeCellRenderer {
    public static final DefaultTreeCellRenderer DEFAULT_RENDERER = new DefaultTreeCellRenderer() {

        public void paint(Graphics g) {
//            ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            super.paint(g);
        }
    };
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oUnSelectedFG;
    private static Color g_oUnSelectedBG;
    private static ImageIcon g_oLeafIcon = null;
    private static ImageIcon g_oLeafIcon_sub = null;
    private static ImageIcon g_oNewIcon = null;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;

    public BackTestingTreeRenderer() {
        reload();
    }

    public static void reload() {
        try {
            g_oSelectedFG = Theme.getColor("MENU_FGCOLOR");
            g_oSelectedBG = Theme.getColor("SCAN_TREE_SELECTED_COLOR");
            g_oUnSelectedFG = Theme.getColor("LABEL_FGCOLOR");
            g_oUnSelectedBG = Theme.getColor("BACKGROUND_COLOR");
            g_oLeafIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeaf.gif");
            g_oNewIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeafNew.gif");
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            g_oLeafIcon_sub = new ImageIcon("images/theme" + Theme.getID() + "/symboltreeLeaf.gif");

        } catch (Exception e) {
            g_oSelectedFG = Color.white;
            g_oSelectedBG = Color.black;
            g_oUnSelectedFG = Color.white;
            g_oUnSelectedBG = Color.black;
        }
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        Component renderer = DEFAULT_RENDERER.getTreeCellRendererComponent(tree, value, isSelected, expanded, leaf, row, hasFocus);

        JLabel lblRenderer = (JLabel) renderer;
        lblRenderer.setOpaque(true);

        Color foreground, background;
        Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
        if (leaf) {
            if (userObject instanceof BackTestSystemData) {
                BackTestSystemData btSystemData = (BackTestSystemData) userObject;
                lblRenderer.setText(btSystemData.getSystemName());
                lblRenderer.setIcon(g_oLeafIcon_sub);
                //    lblRenderer.setIcon(g_oExpandedIcon);

            } else if (userObject instanceof BackTestSimulationData) {
                BackTestSimulationData btsimulationData = (BackTestSimulationData) userObject;
                lblRenderer.setText(btsimulationData.getSimulationName());
                lblRenderer.setIcon(g_oLeafIcon_sub);

                //    lblRenderer.setIcon(g_oExpandedIcon);
            }
//            else if (((DefaultMutableTreeNode) value).getUserObject() instanceof BTS) {
//                lblRenderer.setIcon(g_oNewIcon);
//            } else {
//                lblRenderer.setIcon(g_oLeafIcon);
//            }
        } else if (expanded) {

            lblRenderer.setIcon(g_oExpandedIcon);

        } else {
            if (userObject instanceof BackTestSystemData) {
                BackTestSystemData btSystemData = (BackTestSystemData) userObject;
                lblRenderer.setText(btSystemData.getSystemName());
//                lblRenderer.setIcon(g_oLeafIcon_sub);
                lblRenderer.setIcon(g_oClosedIcon);
            } else if (userObject instanceof BackTestSimulationData) {
                BackTestSimulationData btsimulationData = (BackTestSimulationData) userObject;
                lblRenderer.setText(btsimulationData.getSimulationName());
//                lblRenderer.setIcon(g_oLeafIcon_sub);
                lblRenderer.setIcon(g_oClosedIcon);
            }
            lblRenderer.setIcon(g_oClosedIcon);
        }

        if (isSelected) {
            foreground = g_oSelectedFG;
            background = g_oSelectedBG;
        } else {
            foreground = g_oUnSelectedFG;
            background = g_oUnSelectedBG;
        }


        //renderer.setForeground(foreground);
        renderer.setBackground(background);
        return renderer;
    }


}


