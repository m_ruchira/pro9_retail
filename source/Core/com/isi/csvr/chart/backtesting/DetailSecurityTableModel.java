package com.isi.csvr.chart.backtesting;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith  nidarsha
 * Date: Dec 1, 2008
 * Time: 10:19:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class DetailSecurityTableModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList<DetailedSecuritySummary> secSummaries;

    public DetailSecurityTableModel(ArrayList<DetailedSecuritySummary> secSummaries) {
        this.secSummaries = secSummaries;
    }

    public int getRowCount() {
        return secSummaries.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        DetailedSecuritySummary sysSummary = secSummaries.get(rowIndex);
        switch (columnIndex) {

            case -1:
                return sysSummary;

            case 0:
                //Stock st = Share
                return String.valueOf(sysSummary.getId());

            case 1:
                return sysSummary.getSecurityName();

            case 2:
                return sysSummary.getSymbol();
            case 3:
                return sysSummary.getNetProfit();

            case 4:
                return sysSummary.getAvgGain();

            case 5:
                return sysSummary.getTrades();

            case 6:
                return sysSummary.getTradesPL();

            case 7:
                return sysSummary.getAvgPL();

            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }
}
