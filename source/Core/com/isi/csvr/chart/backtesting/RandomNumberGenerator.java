package com.isi.csvr.chart.backtesting;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Dec 4, 2008
 * Time: 9:56:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class RandomNumberGenerator {

    /*
    lowerlimit-positive integer
    upper limit-positive integer
    size-size of the result integer array
     */
    public static int[] genreatePositiveIntArray(int lowerLimit, int upperLimit, int size) {

        if (lowerLimit > upperLimit || lowerLimit < 0 || upperLimit < 0) {
            try {
                throw new Exception("Invalid parameters");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        int[] resultArray = new int[size];


        for (int i = 0; i < size; i++) {

            int result = (int) ((Math.random() * Integer.MAX_VALUE) % (upperLimit - lowerLimit)) + lowerLimit;

            System.out.println("********** result ***** " + result);
        }


        return resultArray;
    }


    /*
   lowerlimit-negative integer
   upper limit-negative integer
   size-size of the result negative integer array
    */
    public static int[] genreateNegativeIntArray(int lowerLimit, int upperLimit, int size) {

        if (lowerLimit > upperLimit || lowerLimit > 0 || upperLimit > 0) {
            try {
                throw new Exception("Invalid parameters");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        int[] posArray = genreatePositiveIntArray(-lowerLimit, -upperLimit, size);
        int[] resultArray = new int[size];

        for (int i = 0; i < posArray.length; i++) {

            int result = (int) ((Math.random() * Integer.MAX_VALUE) % (upperLimit - lowerLimit)) + lowerLimit;

            System.out.println("********** result ***** " + (-result));

            resultArray[i] = -result;
        }


        return resultArray;
    }


    public static int[] generateNegativeToPositiveIntArray(int lowerLimit, int upperLimit, int size) {

        if (lowerLimit > upperLimit || lowerLimit > 0 || upperLimit < 0) {
            try {
                throw new Exception("Invalid parameters");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        int[] resultArray = new int[size];
        int split = 1111111111 % size;

        int[] posArray = genreatePositiveIntArray(0, upperLimit, split);
        int[] negArray = genreateNegativeIntArray(lowerLimit, 0, (size - split));


        while (!isFilled(resultArray)) {

            /*int index = 7777777 % size;
            long pos = System.currentTimeMillis() %2;
            if(pos == 0){
                Random  r = new Random();
                
            }
            resultArray[index] =*/

        }


        return resultArray;
    }

    private static boolean isFilled(int[] array) {

        for (int i = 0; i < array.length; i++) {
            try {
                int x = array[i];
            } catch (Exception e) {
                return false;
            }
        }

        return true;
    }

    /*
   lowerlimit-positive double
   upper limit-positive double
   size-size of the result double array
    */
    public static double[] genreateDoubleArray(double lowerLimit, double upperLimit, int size, int decimalPlaces) {

        if (lowerLimit > upperLimit) {
            try {
                throw new Exception("Invalid parameters");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        double[] resultArray = new double[size];

        int factor = (int) Math.pow(10, decimalPlaces);
        int lower = (int) (lowerLimit * factor);
        int upper = (int) (upperLimit * factor);


        for (int i = 0; i < size; i++) {

            int result = (int) ((Math.random() * Integer.MAX_VALUE) % (upper - lower)) + lower;

            System.out.println("********** result ***** " + (double) result / factor);
            resultArray[i] = (double) result / factor;

        }
        return resultArray;
    }


    public static void main(String[] args) {

        int[] intArray = RandomNumberGenerator.genreatePositiveIntArray(10, -90, 100);
        double[] doubleArray = RandomNumberGenerator.genreateDoubleArray(2.5, 5.5, 30, 4);
    }
}
