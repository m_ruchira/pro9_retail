package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 16, 2008
 * Time: 4:00:09 PM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.ChartInterface;
import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.text.BadLocationException;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 26-Mar-2008 Time: 15:42:44 To change this template use File | Settings
 * | File Templates.
 */

//SymbolNavigator
public class ChartSymbolNavigator extends InternalFrame implements ActionListener, KeyListener, Themeable, WatchlistListener,
        Runnable, FocusListener, NonTabbable, ExchangeListener, InternalFrameListener, ApplicationListener {
    public static ChartSymbolNavigator self;
    private final long PRICE_UPDATE_MINIMUM_WAIT = 0;
    public Hashtable<String, ChartSymbolNode> mainStore;
    public Hashtable<String, ChartSymbolNode> filteredStore;
    private JPanel mainPanel;
    private JScrollPane scrollPane;
    private JPanel upperPanel;
    private TWButton searchButton;
    private ToolBarButton optionBtn;
    private TWTextField searchText;
    private ChartExchangeNode rootNode;
    private ChartNavigationTree tree;
    private ChartSearchTreeModel treeModel;
    private ChartSymbolTreeRenderer renderer = new ChartSymbolTreeRenderer();
    private JPopupMenu symbolTreePopup;
    private String selectedSymbol = "";
    private String selectedExchange = "";
    private int selectedSymbolsIntType = -1;
    private ViewSetting oSetting;
    private boolean isReady = false;
    private boolean isActive = false;
    private Hashtable<String, ChartExchangeNode> watchListReference;
    private TWMenuItem redmenu;
    private TWMenuItem greenmenu;
    private TWMenuItem bluemenu;
    private TWMenuItem notlinkedmenu;
    private JPopupMenu linkGroupMenu;
    private String linkID = LinkStore.LINK_NONE;
    private long lastChartChangedTime = System.currentTimeMillis();
    private boolean isSpeedChanged = false;

    private ChartSymbolNavigator() {
        oSetting = ViewSettingsManager.getSummaryView("CHART_NAVIGATION_BAR");
        oSetting.setParent(this);
        this.setViewSetting(oSetting);
        rootNode = new ChartExchangeNode("Root", 0, "root");
        treeModel = new ChartSearchTreeModel(rootNode);
        mainStore = new Hashtable<String, ChartSymbolNode>();
        filteredStore = new Hashtable<String, ChartSymbolNode>();
        watchListReference = new Hashtable<String, ChartExchangeNode>();
        createUI();
        applyTheme();
        Theme.registerComponent(this);
        isActive = true;
        hideTitleBarMenu();
        setLinkGroupsEnabled(true);
        //this.setVisible(Boolean.parseBoolean(oSetting.getProperty(ViewConstants.VC_CHART_NAVIGAION_BAR_VISIBLE)));
        //ChartNavigationBar.getSharedInstance().controlSideBar();

        Application.getInstance().addApplicationListener(this);
    }

    public static ChartSymbolNavigator getSharedInstance() {
        if (self == null) {
            self = new ChartSymbolNavigator();
        }
        return self;
    }

    public boolean isNeedsToDisplay() {
        return oSetting.isVisible();
    }

    public Dimension getSavedSize() {
        return oSetting.getSize();
    }

    public void createUI() {
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        tree = new ChartNavigationTree(treeModel);
        addMouseListener();
//        addKeyListener();
        tree.setOpaque(false);
        tree.addKeyListener(this);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        tree.setToggleClickCount(1);


        tree.setCellRenderer(renderer);
        tree.setRootVisible(false);
        scrollPane = new JScrollPane(tree);
        searchButton = new TWButton("");
        optionBtn = new ToolBarButton("");
        optionBtn.setPreferredSize(new Dimension(20, 19));
        optionBtn.setToolTipText(Language.getString("SIDEBAR_CONF"));
        optionBtn.setEnabled(true);
        optionBtn.setVisible(true);
        optionBtn.setIcon(getIconFromString("configure"));
        optionBtn.setRollOverIcon(getIconFromString("configure-mouseover"));

//        optionBtn.addMouseListener(this);
        optionBtn.addActionListener(this);
        searchButton.setIcon(getIconFromString("search"));
        searchButton.setRolloverIcon(getIconFromString("search-mouseover"));
        searchButton.addActionListener(this);
        searchText = new TWTextField();
        searchText.addKeyListener(this);
        searchText.addFocusListener(this);
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.requestFocus(false);
        this.setSize(oSetting.getSize());
        this.setLocation(oSetting.getLocation());
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(mainPanel);
        this.setResizable(true);
        this.setClosable(true);
//        this.setMaximizable(true);
//        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString("SYMBOLS"));
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "100%"}, 3, 3));
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%", "18", "18"}, new String[]{"100%"}, 3, 3));
        upperPanel.add(searchText);
        upperPanel.add(searchButton);
        upperPanel.add(optionBtn);
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(this);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
//        Client.getInstance().getDesktop().add(this);
//        ChartFrame.getSharedInstance().getDesktop().add(this);
        loadTree();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        WatchListManager.getInstance().addWatchlistListener(this);
        this.hideTitleBarMenu();
        mainPanel.add(upperPanel);
        mainPanel.add(scrollPane);
        setBorder(BorderFactory.createEmptyBorder());
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        ChartNavigationBar.getSharedInstance().closeSideBar();
        ChartFrame.getSharedInstance().refreshTopToolbar();
    }

    public void internalFrameClosed(InternalFrameEvent e) {
        //ChartNavigationBar.getSharedInstance().closeSideBar();
    }

    public void doDefaultCloseAction() {

        this.setVisible(false);
        ChartFrame.getSharedInstance().isNavigationBarVisible = false;
        ChartFrame.getSharedInstance().refreshTopToolbar();
    }

    public void loadTree() {
        mainStore.clear();
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exg = (Exchange) exchanges.nextElement();
                if (exg.isDefault()) {
                    if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                        ChartExchangeNode exgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        for (Market subMarket : exg.getSubMarkets()) {
                            Hashtable<String, ChartSymbolNode> marketHash = new Hashtable<String, ChartSymbolNode>();
                            String subName = subMarket.getDescription();
                            Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                            String[] symbolList = subSymbols.getSymbols();
                            for (int i = 0; i < symbolList.length; i++) {
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(subName);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                                marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i])), sto);
                            }
                            ChartExchangeNode tempvector = new ChartExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                            tempvector.setChildrenStore(marketHash);
                            exgVector.addNode(tempvector);
                        }
                        rootNode.addNode(exgVector);

                    } else {
                        Hashtable<String, ChartSymbolNode> exgHash = new Hashtable<String, ChartSymbolNode>();
                        Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                        while (exchageSymbols.hasMoreElements()) {
                            String symbol = (String) exchageSymbols.nextElement();
                            ChartSymbolNode sto = new ChartSymbolNode();
                            sto.setExchange(exg.getSymbol());
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getExchangeFromKey(symbol));
                            sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                            sto.setType(ChartConstants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                sto.setShortDescription(st.getShortDescription());
                                sto.setOpenVal(st.getTodaysOpen());
                                sto.setCloseVal(st.getTodaysClose());
                                sto.setHighVal(st.getHigh());
                                sto.setLowVal(st.getLow());
                                sto.setDescription(st.getLongDescription());
                                st = null;
                            } catch (Exception e) {
                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                            exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                        }
                        ChartExchangeNode tempExgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                        tempExgVector.setChildrenStore(exgHash);
                        rootNode.addNode(tempExgVector);
                    }
                }

            }
            try {
                Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                watchListReference.clear();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        Hashtable<String, ChartSymbolNode> watchListHash = new Hashtable<String, ChartSymbolNode>();
                        String[] symbolArray = watchlist.getSymbols();
                        for (int i = 0; i < symbolArray.length; i++) {
                            String key = symbolArray[i];
                            ChartSymbolNode sto = new ChartSymbolNode();
                            sto.setExchange(SharedMethods.getExchangeFromKey(key));
                            sto.setMarket(null);
                            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                            sto.setType(ChartConstants.symbolType);
                            try {
                                Stock st = DataStore.getSharedInstance().getStockObject(key);
                                sto.setShortDescription(st.getShortDescription());
                                sto.setOpenVal(st.getTodaysOpen());
                                sto.setCloseVal(st.getTodaysClose());
                                sto.setHighVal(st.getHigh());
                                sto.setLowVal(st.getLow());
                                sto.setDescription(st.getLongDescription());
                                st = null;
                            } catch (Exception e) {
                                sto.setShortDescription("");
                                sto.setOpenVal(0.0);
                                sto.setCloseVal(0.0);
                                sto.setHighVal(0.0);
                                sto.setLowVal(0.0);
                                sto.setDescription("");
                            }
                            mainStore.put(key, sto);
                            watchListHash.put(key, sto);
                        }
                        ChartExchangeNode tempWatchListVector = new ChartExchangeNode(watchlist.getCaption(), -1, "");
                        tempWatchListVector.setChildrenStore(watchListHash);
                        watchListReference.put(watchlist.getId(), tempWatchListVector);
                        rootNode.addNode(tempWatchListVector);
                    }
                }
            } catch (Exception e) {

            }

//            addMouseListener();
            //ToolTipManager.sharedInstance().registerComponent(tree);
            //shashika
            /*searchText.requestFocus(false);
            GUISettings.applyOrientation(mainPanel);
            GUISettings.applyOrientation(tree);
            tree.updateUI();
            tree.repaint();*/
            ToolTipManager.sharedInstance().registerComponent(tree);
            searchText.requestFocus(false);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //To change body of implemented methods use File | Settings | File Templates.
                    GUISettings.applyOrientation(mainPanel);
                    GUISettings.applyOrientation(tree);
                    tree.updateUI();
                    tree.repaint();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void searchTree(String criteria) {
        if ((!criteria.equals(""))) {
            Enumeration mainStoreenum = mainStore.elements();
            filteredStore.clear();
            while (mainStoreenum.hasMoreElements()) {
                ChartSymbolNode sto = (ChartSymbolNode) mainStoreenum.nextElement();
                if ((sto.getSymbol().contains(criteria.toUpperCase())) || (sto.getDescription().contains(criteria.substring(0, 1).toUpperCase() + criteria.substring(1))) || (sto.getDescription().contains(criteria))) {
                    filteredStore.put(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()), sto);
                }
            }
            rootNode.clear();
            try {

                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
                while (exchanges.hasMoreElements()) {
                    Exchange exg = (Exchange) exchanges.nextElement();
                    boolean ismatched = false;
                    if (exg.isDefault()) {
                        if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                            ChartExchangeNode exgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                            for (Market subMarket : exg.getSubMarkets()) {
                                Hashtable<String, ChartSymbolNode> marketHash = new Hashtable<String, ChartSymbolNode>();
                                String subName = subMarket.getDescription();
                                Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), subMarket.getMarketID());
                                String[] symbolList = subSymbols.getSymbols();
                                for (int i = 0; i < symbolList.length; i++) {
                                    ChartSymbolNode sto = new ChartSymbolNode();
                                    sto.setExchange(exg.getSymbol());
                                    sto.setMarket(subName);
                                    sto.setSymbol(SharedMethods.getSymbolFromKey(symbolList[i]));
                                    sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                    sto.setType(ChartConstants.symbolType);
                                    try {
                                        Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromKey(symbolList[i]), SharedMethods.getInstrumentTypeFromKey(symbolList[i]));
                                        sto.setShortDescription(st.getShortDescription());
                                        sto.setOpenVal(st.getTodaysOpen());
                                        sto.setCloseVal(st.getTodaysClose());
                                        sto.setHighVal(st.getHigh());
                                        sto.setLowVal(st.getLow());
                                        sto.setDescription(st.getLongDescription());
                                        st = null;
                                    } catch (Exception e) {
                                        sto.setShortDescription("");
                                        sto.setOpenVal(0.0);
                                        sto.setCloseVal(0.0);
                                        sto.setHighVal(0.0);
                                        sto.setLowVal(0.0);
                                        sto.setDescription("");
                                    }
                                    if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                        ismatched = true;
                                        marketHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbolList[i]), SharedMethods.getInstrumentFromExchangeKey(symbolList[i])), sto);
                                    }
                                }
                                if (ismatched) {
                                    ChartExchangeNode tempvector = new ChartExchangeNode(subName, exg.getMarketStatus(), exg.getSymbol());
                                    tempvector.setChildrenStore(marketHash);
                                    exgVector.addNode(tempvector);
                                }
                            }
                            if (ismatched) {
                                rootNode.addNode(exgVector);
                            }

                        } else {
                            Hashtable<String, ChartSymbolNode> exgHash = new Hashtable<String, ChartSymbolNode>();
                            Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exg.getSymbol());
                            while (exchageSymbols.hasMoreElements()) {
                                String symbol = (String) exchageSymbols.nextElement();
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(exg.getSymbol());
                                sto.setMarket(null);
                                sto.setSymbol(SharedMethods.getSymbolFromExchangeKey(symbol));
                                sto.setInstrumentType(SharedMethods.getInstrumentFromExchangeKey(symbol));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                    ismatched = true;
                                    exgHash.put(SharedMethods.getKey(exg.getSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)), sto);
                                }

                            }
                            if (ismatched) {
                                ChartExchangeNode tempExgVector = new ChartExchangeNode(exg.getDescription(), exg.getMarketStatus(), exg.getSymbol());
                                tempExgVector.setChildrenStore(exgHash);
                                rootNode.addNode(tempExgVector);
                            }
                        }
                    }

                }
                try {
                    Collections.sort(rootNode.getSymbolStore(), new NodeCoparator());
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
                    WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                    for (WatchListStore watchlist : watchlists) {
                        if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                            boolean isWatchListMatched = false;
                            Hashtable<String, ChartSymbolNode> watchListHash = new Hashtable<String, ChartSymbolNode>();
                            String[] symbolArray = watchlist.getSymbols();
                            for (int i = 0; i < symbolArray.length; i++) {
                                String key = symbolArray[i];
                                ChartSymbolNode sto = new ChartSymbolNode();
                                sto.setExchange(SharedMethods.getExchangeFromKey(key));
                                sto.setMarket(null);
                                sto.setSymbol(SharedMethods.getSymbolFromKey(key));
                                sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
                                sto.setType(ChartConstants.symbolType);
                                try {
                                    Stock st = DataStore.getSharedInstance().getStockObject(key);
                                    sto.setShortDescription(st.getShortDescription());
                                    sto.setOpenVal(st.getTodaysOpen());
                                    sto.setCloseVal(st.getTodaysClose());
                                    sto.setHighVal(st.getHigh());
                                    sto.setLowVal(st.getLow());
                                    sto.setDescription(st.getLongDescription());
                                    st = null;
                                } catch (Exception e) {
                                    sto.setShortDescription("");
                                    sto.setOpenVal(0.0);
                                    sto.setCloseVal(0.0);
                                    sto.setHighVal(0.0);
                                    sto.setLowVal(0.0);
                                    sto.setDescription("");
                                }
                                if (filteredStore.containsKey(SharedMethods.getKey(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType()))) {
                                    isWatchListMatched = true;
                                    watchListHash.put(key, sto);
                                }
                            }

                            if (isWatchListMatched) {
                                ChartExchangeNode tempWatchListVector = new ChartExchangeNode(watchlist.getCaption(), -1, "");
                                tempWatchListVector.setChildrenStore(watchListHash);
                                rootNode.addNode(tempWatchListVector);
                            }
                        }
                    }
                } catch (Exception e) {

                }
                tree.repaint();
                tree.updateUI();
                for (int i = 0; i < tree.getRowCount(); i++) {
                    tree.expandRow(i);
                }
                ToolTipManager.sharedInstance().registerComponent(tree);
//                addMouseListener();
                GUISettings.applyOrientation(mainPanel);
                searchText.requestFocus(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rootNode.clear();
            loadTree();
            tree.updateUI();
            tree.repaint();
            scrollPane.updateUI();
            mainPanel.updateUI();
            this.updateUI();
            searchText.requestFocus(false);
        }

    }

    public void fireSymbolAddedEvent(String key, String id) {
        try {
            if ((watchListReference.get(id)) == null) {
                return;
            }
            ChartSymbolNode sto = new ChartSymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(ChartConstants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            (watchListReference.get(id)).addNode(sto);
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    tree.updateUI();
                    tree.repaint();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void fireSymbolRemovedEvent(String key, String id) {
        try {
            ChartSymbolNode sto = new ChartSymbolNode();
            sto.setExchange(SharedMethods.getExchangeFromKey(key));
            sto.setMarket(null);
            sto.setSymbol(SharedMethods.getSymbolFromKey(key));
            sto.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(key));
            sto.setType(ChartConstants.symbolType);
            try {
                Stock st = DataStore.getSharedInstance().getStockObject(key);
                sto.setShortDescription(st.getShortDescription());
                sto.setOpenVal(st.getTodaysOpen());
                sto.setCloseVal(st.getTodaysClose());
                sto.setHighVal(st.getHigh());
                sto.setLowVal(st.getLow());
                sto.setDescription(st.getLongDescription());
                st = null;
            } catch (Exception e) {
                sto.setShortDescription("");
                sto.setOpenVal(0.0);
                sto.setCloseVal(0.0);
                sto.setHighVal(0.0);
                sto.setLowVal(0.0);
                sto.setDescription("");
            }
            mainStore.put(key, sto);
            if (id != null) (watchListReference.get(id)).removeNode(sto);
//            tree.updateUI();
            SharedMethods.updateComponent(tree);
            tree.repaint();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(searchButton)) {
            searchTree(searchText.getText());
        } else if (e.getSource().equals(optionBtn)) {
            showOptionDialog();
        }
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(searchText)) {
            if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                if (!searchText.getText().equals("")) {
                    searchTree(searchText.getText());
                    e.consume();
                    searchText.requestFocus(true);
                } else {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
                if (searchText.getText().equals("")) {
                    rootNode.clear();
                    loadTree();
                    e.consume();
                    setInitialText();
                }

            } else if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                rootNode.clear();
                loadTree();
                e.consume();
                setInitialText();
            }
        } else {
            searchText.requestFocus();
            try {
                if (!Character.isWhitespace(e.getKeyChar()) && (!(e.getKeyChar() == KeyEvent.VK_ESCAPE))) {
                    searchText.getDocument().insertString(searchText.getCaretPosition(), "" + e.getKeyChar(), null);
                }
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setInitialText() {
        searchText.setText(Language.getString("SIDE_BAR_SEARCH"));
        searchText.setSelectionStart(0);
        searchText.setSelectionEnd(searchText.getText().length());
    }

    public void keyReleased(KeyEvent e) {
        /*if (isSpeedChanged && (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_DOWN)) {
            if (selectedSymbol != null && !selectedSymbol.isEmpty()) {

                String key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);

                if (linkID != null && linkID.equals(LinkStore.LINK_NONE)) {
                    if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                        ChangeBaseChart(key);
                    }
                } else {
                    if (linkID != null && ChartFrame.getSharedInstance().getDesktop().getAllFrames().length > 0) {
                        ArrayList<GraphFrame> frames = ChartLinkStore.getSharedInstance().getGraphFrames(linkID);
                        GraphFrame[] graphs = new GraphFrame[frames.size()];
                        for (int i = 0; i < frames.size(); i++) {
                            GraphFrame graphFrame = frames.get(i);
                            graphs[i] = graphFrame;
                        }
                        //long temp = System.currentTimeMillis();
                        if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                            changeBaseChartForMultipleTemplates(linkID, key, graphs);
                        }
                        //System.out.println("++++++++++ time taken ++++++++++++ " + (System.currentTimeMillis() - temp) / 1000.0);
                    } else {  //if there are no graphs when the side bar is linked, need to open the graph
                        if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                            ChangeBaseChart(key);
                        }
                    }
                }
                focusToThis();
            }
            System.out.println("key released");
        }*/
    }

    private JPopupMenu getPopupMenu1() {
        if (symbolTreePopup == null) {
            symbolTreePopup = Client.getInstance().getTablePopup();
        }
        return symbolTreePopup;
    }

    private void addMouseListener() {
        tree.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent ev) {
                selectedSymbol = "";
                selectedExchange = "";
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path != null) {
                    tree.setSelectionPath(path);
                    try {
                        ChartSymbolNode node = (ChartSymbolNode) path.getLastPathComponent();
                        ChartSymbolNode sto = (ChartSymbolNode) node;
                        selectedExchange = sto.getExchange();
                        selectedSymbol = sto.getSymbol();
                        selectedSymbolsIntType = sto.getInstrumentType();
                    } catch (Exception e) {

                    }
                }
            }

            public void mouseClicked(MouseEvent ev) {

                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path == null) {
                    return;
                }
                if (ev.getClickCount() >= 1) {
                    ChartSymbolNode node = (ChartSymbolNode) path.getLastPathComponent();
                    if (node != null && !ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof ChartSymbolNode) {
                            if ((ev.getModifiersEx() & ev.CTRL_DOWN_MASK) == ev.CTRL_DOWN_MASK) {
                                String key = null;
                                key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                                if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                    ChartFrame.getSharedInstance().addNewGraph(true, false, key);
                                }
                            } else if ((ev.getModifiers() & ev.BUTTON3_MASK) == ev.BUTTON3_MASK) { //right click

                            } else {
                                if (linkID != null && linkID.equals(LinkStore.LINK_NONE)) {
                                    String key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                                    if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                        ChangeBaseChart(key);
                                    }
                                } else {
                                    if (linkID != null && ChartFrame.getSharedInstance().getDesktop().getAllFrames().length > 0) {
                                        ArrayList<GraphFrame> frames = ChartLinkStore.getSharedInstance().getGraphFrames(linkID);
                                        //System.out.println("********** total graphs ********** " + frames.size());
                                        GraphFrame[] graphs = new GraphFrame[frames.size()];
                                        for (int i = 0; i < frames.size(); i++) {
                                            GraphFrame graphFrame = frames.get(i);
                                            graphs[i] = graphFrame;
                                        }
                                        String key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                                        if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                            changeBaseChartForMultipleTemplates(linkID, key, graphs);
                                        }
                                    } else {  //if there are no graphs when the side bar is linked, need to open the graph
                                        String key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
                                        if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                            ChangeBaseChart(key);
                                        }
                                    }
                                }
                            }
                            focusToThis();
                        }
                    }
                }
            }


            public void mouseReleased(MouseEvent ev) {
                try {
                    TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                    if (path == null) {
                        return;
                    }

                    //IF we're on a path, then we can check the node that its holding
                    ChartSymbolNode node = (ChartSymbolNode) path.getLastPathComponent();
                    if (node != null && ev.isPopupTrigger()) {    //&& node.isLeaf()
                        if (node instanceof ChartSymbolNode) {
                            // getPopupMenu1().show(tree, ev.getX(), ev.getY());
                        }

                    }
                } catch (Exception e) {

                }


            }
        });
    }

    public void ChangeBaseChart(String key) {
        GraphFrame gf = ChartFrame.getSharedInstance().getActiveGraph();
        if (gf != null) {
            try {
                TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), new GraphFrame[]{gf});
                TemplateFactory.LoadTemplate(gf, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), key, gf.isGraphDetached());
                gf.setGraphTitle();
                // mevan (right margin reset )
                gf.graph.GDMgr.setRightMarginDefault();
                //updating datawindow
                if (ChartFrame.getSharedInstance().getDataWindow() != null) {
                    DataWindow window = ChartFrame.getSharedInstance().getDataWindow();
                    if (!window.isWindowMode() && window.isVisible()) {
                        window.setSnapToDate(false);
                        window.setDragMode(false);
                        window.getBtnDataModeImaginary().doClick();
                    }
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } else {//if no graphs in the chart frame --> add a new graph
            ChartFrame.getSharedInstance().addNewGraph(true, false, key);
            // mevan (right margin reset )
            gf.graph.GDMgr.setRightMarginDefault();
        }

    }

    public boolean isValidSymbol(String symbol) {
        /**************validatign for inactive symbols ***********************************/

        if (!symbol.equals("")) {
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                return false;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                return false;
            }
        } else {
            return false;

        }
        /**************validatign for inactive symbols ***********************************/
        return true;
    }

    public String getSelectedKey() {
        return SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);
    }

    public void run() {
        while (isActive) {
            try {
                if (isReadyToFilter()) {
                    searchTree(searchText.getText());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void showSideBar() {
        if (Language.isLTR()) {
            showSideBar(true);
            searchText.requestFocus(true);
        } else {
            showSideBar(false);
        }
    }

    public void showSideBar(boolean left) {
        if (isVisible()) {
            setVisible(false);
            return;
        }

        Dimension desktopSize = getDesktopPane().getSize();
        setVisible(true);
        setSize(getWidth(), (int) desktopSize.getHeight());
        if (left) {
            setLocation(0, 0);
        } else {
            setLocation((int) desktopSize.getWidth() - getWidth(), 0);
        }
        searchText.requestFocus(true);
    }

    public void applyTheme() {
        searchButton.setIcon(getIconFromString("search"));
        searchButton.setRolloverIcon(getIconFromString("search-mouseover"));
        optionBtn.setIcon(getIconFromString("configure"));
        optionBtn.setRollOverIcon(getIconFromString("configure-mouseover"));

    }

    private boolean isReadyToFilter() {
        return this.isReady;
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void focusGained(FocusEvent e) {
        if (e.getSource().equals(searchText)) {
            if (searchText.getText().equals(Language.getString("SIDE_BAR_SEARCH"))) {
                searchText.setText("");
            }

        }
    }

    public void focusLost(FocusEvent e) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        rootNode.clear();
        loadTree();
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void subMarketStatsChanged(int oldStatus, Exchange exchange, Market subMarket) {
    }

    public void subMarketMustInitialize(Exchange exchange, Market market) {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {

        try {
            for (int i = 0; i < rootNode.getSymbolStore().size(); i++) {
                ChartExchangeNode ex = (ChartExchangeNode) rootNode.getSymbolStore().get(i);
                if (ex.getExchangeSymbol().equals(exchange.getSymbol())) {
                    ex.setMarketStatus(exchange.getMarketStatus());
                    updateTree();
                    tree.repaint();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateTree() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                tree.updateUI();
            }
        });


    }

    @Override
    public void updateUI() {
        super.updateUI();
        updateTree();
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        fireSymbolAddedEvent(key, listID);
        tree.repaint();

    }

    public void symbolRemoved(String key, String listID) {
        fireSymbolRemovedEvent(key, listID);
        tree.repaint();
    }

    public void watchlistAdded(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRenamed(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void watchlistRemoved(String listID) {
        rootNode.clear();
        loadTree();
    }

    public void setVisible(boolean value) {
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
        if (value == false) {
            ChartNavigationBar.getSharedInstance().setMainPainPreferedsize();
        }


    }

    public void workspaceWillLoad() {

    }

    public void workspaceLoaded() {

        boolean isVisible = Boolean.parseBoolean(oSetting.getProperty(ViewConstants.VC_CHART_NAVIGAION_BAR_VISIBLE));

        ChartFrame.getSharedInstance().setChartNavigationBarVisible(isVisible);
        ChartSymbolNavigator.getSharedInstance().setVisible(!isVisible); // this is a special case
        ChartNavigationBar.getSharedInstance().leftTriggerMainPan.setPreferredSize(new Dimension(170, 1));
        ChartNavigationBar.getSharedInstance().controlSideBar();

        String linkID = oSetting.getProperty(ViewConstants.VC_CHART_NAVIGAION_BAR_LINK_ID);
        setLinkedGroupID(linkID);
    }

    public void workspaceWillSave() {

        oSetting.putProperty(ViewConstants.VC_CHART_NAVIGAION_BAR_VISIBLE, this.isVisible());
        oSetting.putProperty(ViewConstants.VC_CHART_NAVIGAION_BAR_LINK_ID, this.linkID);
    }

    public void workspaceSaved() {

    }

    public String getTemplateFileForLinkID(String linkID) {

        //return Settings.getAbsolutepath() + "\\Charts\\temp\\" + "temp_link_" + linkID + ".xml";
        return Settings.getAbsolutepath() + "/Temp/" + "temp_link_" + linkID + ".mct";
    }

    public void changeBaseChartForMultipleTemplates(final String linkID, final String key, final GraphFrame[] frames) {
        //GraphFrame activeGragh = ChartFrame.getSharedInstance().getActiveGraph();
        try {
            String filePath = getTemplateFileForLinkID(linkID);
            long temp1 = System.currentTimeMillis();
            TemplateFactory.SaveMultipleTemplates(new File(filePath), frames);
            System.out.println("++++++++ SaveMultipleTemplates ++++++++++ " + (System.currentTimeMillis() - temp1) / 1000.0);

            temp1 = System.currentTimeMillis();
            for (int i = frames.length - 1; i >= 0; i--) {
                GraphFrame gf = frames[i];
                TemplateFactory.LoadMultipleTemplates(gf, new File(filePath), key, gf.isGraphDetached(), i);
            }
            System.out.println("++++++++ LoadMultipleTemplates ++++++++++ " + (System.currentTimeMillis() - temp1) / 1000.0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*if (activeGragh == null) {
            ChartFrame.getSharedInstance().addNewGraph(true, false, key);
        }*/
    }

    public void showLinkGroup() {

        if (linkGroupMenu == null) {
            createLinkGroupMenu();
        }

        SwingUtilities.updateComponentTreeUI(linkGroupMenu);
        Point point = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(point, this);
        GUISettings.showPopup(linkGroupMenu, this, (int) point.getX(), (int) point.getY());
    }

    public void setLinkedGroupID(String linkID) {
        super.setLinkedGroupID(linkID);
        this.linkID = linkID;
        ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(linkID);
    }

    private void createLinkGroupMenu() {
        linkGroupMenu = new JPopupMenu();

        redmenu = new TWMenuItem(Language.getString("RED"), "lnk_red.gif");
        redmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_RED);
            }
        });
        linkGroupMenu.add(redmenu);

        greenmenu = new TWMenuItem(Language.getString("GREEN"), "lnk_green.gif");
        greenmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_GREEN);
            }
        });
        linkGroupMenu.add(greenmenu);

        bluemenu = new TWMenuItem(Language.getString("BLUE"), "lnk_blue.gif");
        bluemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_BLUE);
            }
        });
        linkGroupMenu.add(bluemenu);

        notlinkedmenu = new TWMenuItem(Language.getString("NOT_LINK"), "lnk_notlink.gif");
        notlinkedmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_NONE);
            }
        });
        linkGroupMenu.add(notlinkedmenu);

        linkGroupMenu.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                /* if (oSettings != null) {
                    redmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_RED));
                    greenmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_GREEN));
                    bluemenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_BLUE));
                }*/
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });
        GUISettings.applyOrientation(linkGroupMenu);
    }

    public void focusToThis() {
        try {
            this.setSelected(true);
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
    }

    public void addKeyListener() {

        tree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                long timeNow = System.currentTimeMillis();
                //if (timeNow > lastChartChangedTime + PRICE_UPDATE_MINIMUM_WAIT) {
                if (true) {
                    isSpeedChanged = false;
                    TreePath path = e.getPath();
                    ChartSymbolNode node;
                    node = (ChartSymbolNode) path.getLastPathComponent();
                    ChartSymbolNode sto = (ChartSymbolNode) node;
                    selectedExchange = sto.getExchange();
                    selectedSymbol = sto.getSymbol();
                    selectedSymbolsIntType = sto.getInstrumentType();
                    if (selectedSymbol != null && !selectedSymbol.isEmpty()) {

                        String key = SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType);

                        if (linkID != null && linkID.equals(LinkStore.LINK_NONE)) {
                            if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                ChangeBaseChart(key);
                            }
                        } else {
                            if (linkID != null && ChartFrame.getSharedInstance().getDesktop().getAllFrames().length > 0) {
                                ArrayList<GraphFrame> frames = ChartLinkStore.getSharedInstance().getGraphFrames(linkID);
                                GraphFrame[] graphs = new GraphFrame[frames.size()];
                                for (int i = 0; i < frames.size(); i++) {
                                    GraphFrame graphFrame = frames.get(i);
                                    graphs[i] = graphFrame;
                                }
                                //long temp = System.currentTimeMillis();
                                if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                    changeBaseChartForMultipleTemplates(linkID, key, graphs);
                                }
                                //System.out.println("++++++++++ time taken ++++++++++++ " + (System.currentTimeMillis() - temp) / 1000.0);
                            } else {  //if there are no graphs when the side bar is linked, need to open the graph
                                if (isValidSymbol(key) && (!"".equals(selectedSymbol))) {
                                    ChangeBaseChart(key);
                                }
                            }
                        }
                        focusToThis();
                    }
                } else {
                    /*isSpeedChanged = true;
                    TreePath path = e.getPath();
                    ChartSymbolNode node;
                    node = (ChartSymbolNode) path.getLastPathComponent();
                    ChartSymbolNode sto = (ChartSymbolNode) node;
                    selectedExchange = sto.getExchange();
                    selectedSymbol = sto.getSymbol();
                    selectedSymbolsIntType = sto.getInstrumentType();*/
                }
                lastChartChangedTime = System.currentTimeMillis();
            }
        });
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    private void showOptionDialog() {
        ChartNavibarConfigDialog rUI = new ChartNavibarConfigDialog(ChartInterface.getChartFrameParentComponent(), Language.getString("CHART_NAVIBAR_OPTIONS_TITLE"), true);
    }

    class NodeCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            return ((ChartExchangeNode) o1).getName().compareTo(((ChartExchangeNode) o2).getName());
        }
    }
}


