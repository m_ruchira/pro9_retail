package com.isi.csvr.chart;

import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.SharedMethods;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class GraphStream implements Serializable {

    /**
     * JDK 1.1 serialVersionUID
     */
    private static final long serialVersionUID = 1072600473330203800L;

    public static String COMPARE_SEPERATOR = "|";
    transient private GraphFrame gFrame;
    transient private StockGraph graph;
    transient private GraphDataManager gdm;

    private ArrayList sources;
    private ArrayList objArray;
    private ArrayList staticObjArray;
    private ArrayList graphColors;

    private ArrayList panels;
    private float[] splitters;
    private float zoom;
    private float zoomBegin;

    private boolean detached;
    private boolean currentMode;
    private int period;
    private long interval;
    private boolean indexed;
    private boolean showVolume;
    private int legendType;
    private String[] saBase;
    private String[] saCompare;

    private int mainGrfIndex = 0;
    private int volGrfIndex = 1;

    public GraphStream() {
    }

    public static String getBasicChartString(GraphFrame gf) {
        if (gf != null) {
            StockGraph g = gf.graph;
            GraphDataManager gm = g.GDMgr;
            return "" + ViewConstants.VC_GRAPH_MODE + ViewConstants.VC_FIELD_DELIMETER +
                    g.isCurrentMode() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_BASE + ViewConstants.VC_FIELD_DELIMETER +
                    getBaseGraph(gm) + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_COMPARE + ViewConstants.VC_FIELD_DELIMETER +
                    getCompareGraphArr(gm) + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_PERIOD + ViewConstants.VC_FIELD_DELIMETER +
                    g.getPeriod() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_INTERVAL + ViewConstants.VC_FIELD_DELIMETER +
                    g.getInterval() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_STYLE + ViewConstants.VC_FIELD_DELIMETER +
                    g.getGraphStyle() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_VOLUME + ViewConstants.VC_FIELD_DELIMETER +
                    g.isShowingVolumeGrf() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_INDEXED + ViewConstants.VC_FIELD_DELIMETER +
                    gm.isIndexed() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_DESCRIPTION + ViewConstants.VC_FIELD_DELIMETER +
                    g.getLegendType() + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_DETACHED + ViewConstants.VC_FIELD_DELIMETER +
                    gf.isGraphDetached() + ViewConstants.VC_RECORD_DELIMETER +
//					ViewConstants.VC_GRAPH_BASE +  ViewConstants.VC_FIELD_DELIMETER +
//						getBaseGraph(gm) + ViewConstants.VC_RECORD_DELIMETER +
//					ViewConstants.VC_GRAPH_COMPARE +  ViewConstants.VC_FIELD_DELIMETER +
//						getCompareGraphArr(gm) + ViewConstants.VC_RECORD_DELIMETER +
                    ViewConstants.VC_GRAPH_DATA_MODE + ViewConstants.VC_FIELD_DELIMETER +
                    gf.isTableMode() + ViewConstants.VC_RECORD_DELIMETER;
        } else {
            return null;
        }
    }

    private static String getBaseGraph(GraphDataManager gm) {
        String result = gm.getBaseGraph();
        if (result != null) {
            return result;
        } else {
            return "";
        }
    }

    private static String getCompareGraphArr(GraphDataManager gm) {
        String[] cgArr = gm.getCompareGraphs();
        if ((cgArr != null) && (cgArr.length > 0)) {
            String result = cgArr[0];
            for (int i = 1; i < cgArr.length; i++) {
                result = result + COMPARE_SEPERATOR + cgArr[i];
            }
            return result;
        } else {
            return "";
        }
    }

    public static void setBasicChartProperties(String record, GraphFrame gf) {

        StringTokenizer fields = new StringTokenizer(record, ViewConstants.VC_RECORD_DELIMETER);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(ViewConstants.VC_FIELD_DELIMETER);

        String value;
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {

                    case ViewConstants.VC_GRAPH_BASE:
                        gf.setBaseSymbol(value);
                        break;
                    case ViewConstants.VC_GRAPH_COMPARE:
                        String[] sa = value.split("\\" + COMPARE_SEPERATOR);
                        if ((sa != null) && (sa.length > 0)) {
                            for (int i = 0; i < sa.length; i++) {
                                if (!sa[i].equals(""))
                                    gf.setComparisonSymbol(sa[i]);
                            }
                        }
                        break;
                    case ViewConstants.VC_GRAPH_MODE:
                        gf.setGraphMode(booleanValue(value, true), true, false); //not toggling mode -isRemoveOHLCRequest = false
                        break;
                    case ViewConstants.VC_GRAPH_PERIOD:
                        gf.graph.setPeriod(SharedMethods.intValue(value, gf.graph.ALL_HISTORY));
                        break;
                    case ViewConstants.VC_GRAPH_INTERVAL:
                        gf.graph.setInterval(SharedMethods.longValue(value, gf.graph.DAILY));
                        break;
                    case ViewConstants.VC_GRAPH_STYLE:
                        gf.graph.setGraphStyle(SharedMethods.intValue(value, gf.graph.INT_GRAPH_STYLE_LINE));
                        break;
                    case ViewConstants.VC_GRAPH_VOLUME:
                        gf.graph.setVolumeGraphflag(booleanValue(value, false));
                        break;
                    case ViewConstants.VC_GRAPH_INDEXED:
                        gf.graph.GDMgr.setIndexed(booleanValue(value, true));
                        break;
                    case ViewConstants.VC_GRAPH_DESCRIPTION:
                        gf.graph.setLegendType(SharedMethods.intValue(value, gf.graph.INT_LEGEND_SYMBOL));
                        break;
                    case ViewConstants.VC_GRAPH_DETACHED:
                        setDetached(gf, !booleanValue(value, true));
                        gf.toggleDetach();
                        break;
                    case ViewConstants.VC_GRAPH_DATA_MODE:
                        gf.setTableMode(!booleanValue(value, true));
                        gf.switchPanel();
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        fields = null;
        pair = null;
    }

    private static void setDetached(GraphFrame gf, boolean detached) {
        gf.setDetached(detached);
    }

    public static boolean booleanValue(String value, boolean bool) {
        try {
            return value.trim().equalsIgnoreCase("TRUE");
        } catch (Exception ex) {
            return bool;
        }
    }

    public void setGraph(GraphFrame gf) {
        this.gFrame = gf;
        this.graph = gf.graph;
        this.gdm = gf.graph.GDMgr;
    }

    public void extractFieldsFromGraph() {
        if (gFrame != null) {
            setPnlIDs(gdm.getSources());
            setPnlIDs(gdm.getObjArray());
            setPnlIDs(gdm.getStaticObjArray());
            sources = gdm.getSources();
            objArray = gdm.getObjArray();
            staticObjArray = gdm.getStaticObjArray();
            graphColors = gdm.graphColors;

            panels = graph.panels;
            mainGrfIndex = gdm.getIndexOfTheRect(graph.panels, graph.mainRect);
            volGrfIndex = gdm.getIndexOfTheRect(graph.panels, graph.volumeRect);
            splitters = graph.splitters;
            zoom = graph.zSlider.getZoom();
            zoomBegin = graph.zSlider.getBeginPos();

            currentMode = graph.isCurrentMode();
            period = graph.getPeriod();
            interval = graph.getInterval();
            indexed = gdm.isIndexed();
            showVolume = graph.isShowingVolumeGrf();
            legendType = graph.getLegendType();
            saBase = gFrame.g_oBaseSymbols.getSymbols();
            saCompare = gFrame.g_oComparedSymbols.getSymbols();
            detached = gFrame.isGraphDetached();
        }
    }

    public void assignValuesToGraph() {
        gdm.setSources(sources);
        gdm.setObjArray(objArray);
        gdm.setStaticObjArray(staticObjArray);
        gdm.graphColors = graphColors;

        graph.panels = panels;
        graph.mainRect = (WindowPanel) graph.panels.get(mainGrfIndex);
        if (volGrfIndex >= 0)
            graph.volumeRect = (WindowPanel) graph.panels.get(volGrfIndex);
        graph.splitters = splitters;
        graph.recalculateRectBounds();
        setPanels(sources);
        setPanels(objArray);
        setPanels(staticObjArray);

        graph.zSlider.setZoomAndPos(zoom, zoomBegin);

        graph.setCurrentMode(currentMode);
        gFrame.CurrentMode = currentMode;
        graph.setPeriod(period);
        graph.setInterval(interval);
        gdm.setIndexed(indexed);
        graph.setVolumeGraphflag(showVolume);
        graph.setLegendType(legendType);
        gFrame.g_oBaseSymbols.setSymbols(saBase);
        gFrame.g_oComparedSymbols.setSymbols(saCompare);
        gFrame.setDetached(false);
    }

    private void setPnlIDs(ArrayList al) {
        if (al == null) return;
        int ID = 0;
        for (int i = 0; i < al.size(); i++) {
            Object obj = al.get(i);
            if (obj instanceof ChartProperties) {
                ID = gdm.getIndexOfTheRect(graph.panels, ((ChartProperties) obj).getRect());
                ((ChartProperties) obj).pnlID = ID;
            } else if (obj instanceof AbstractObject) {
                ID = gdm.getIndexOfTheRect(graph.panels, ((AbstractObject) obj).getRect());
                ((AbstractObject) obj).pnlID = ID;
            }
        }
    }

    private void setPanels(ArrayList al) {
        if (al == null) return;
        WindowPanel r = null;
        for (int i = 0; i < al.size(); i++) {
            Object obj = al.get(i);
            if (obj instanceof ChartProperties) {
                int pnlID = ((ChartProperties) obj).pnlID;
                if (pnlID >= 0) {
                    r = (WindowPanel) graph.panels.get(pnlID);
                } else {
                    r = graph.volumeRect;
                }
                ((ChartProperties) obj).setRect(r);
                // ????????
                ((ChartProperties) obj).setPenWidth(((ChartProperties) obj).getPenWidth());
            } else if (obj instanceof AbstractObject) {
                int pnlID = ((AbstractObject) obj).pnlID;
                if (pnlID >= 0) {
                    r = (WindowPanel) graph.panels.get(pnlID);
                } else {
                    r = graph.volumeRect;
                }
                ((AbstractObject) obj).setRect(r);
                ((AbstractObject) obj).setPenWidth(((AbstractObject) obj).getPenWidth());
            }
        }
    }
}