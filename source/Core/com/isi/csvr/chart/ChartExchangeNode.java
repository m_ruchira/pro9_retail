package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 16, 2008
 * Time: 4:31:50 PM
 * To change this template use File | Settings | File Templates.
 */

//public class ExchangeNode {
public class ChartExchangeNode {
    private String name;
    private int marketStatus;
    private String status = null;
    private String exchangeSymbol;
    private Vector symbolStore;

    public ChartExchangeNode(String name, int marketStatus, String exgSymbol) {
        this.name = name;
        this.marketStatus = marketStatus;
        this.exchangeSymbol = exgSymbol;
        symbolStore = new Vector();
        setMarketStatus(marketStatus);
    }

    public ChartExchangeNode() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMarketStatusType() {
        return marketStatus;
    }

    public void setMarketStatusType(int marketStatus) {
        this.marketStatus = marketStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExchangeSymbol() {
        return exchangeSymbol;
    }

    public void setExchangeSymbol(String exchangeSymbol) {
        this.exchangeSymbol = exchangeSymbol;
    }

    public Vector getSymbolStore() {
        return symbolStore;
    }

    public void setChildrenStore(Hashtable store) {
        try {
            Enumeration symbols = store.elements();
            while (symbols.hasMoreElements()) {
                symbolStore.add((ChartSymbolNode) symbols.nextElement());
            }
            Collections.sort(symbolStore, new LeafCoparator());
        } catch (Exception e) {
        }
    }

    public void addNode(ChartExchangeNode node) {
        try {
            symbolStore.add(node);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeNode(ChartSymbolNode oNode) {
        try {
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((ChartSymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    symbolStore.remove(i);
                    break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void addNode(ChartSymbolNode oNode) {
        try {
            boolean include = false;
            for (int i = 0; i < symbolStore.size(); i++) {
                if (((ChartSymbolNode) symbolStore.get(i)).getSymbol().equals(oNode.getSymbol())) {
                    include = true;
                }

            }
            if (!include) {
                symbolStore.add(oNode);
                include = false;
            }
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void clear() {
        symbolStore.clear();
    }

    public Object getChild(int index) {
        return symbolStore.get(index);
    }

    public int getChildCount() {
        try {
            return symbolStore.size();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean isLeaf(Object object) {
        try {
            if (symbolStore.contains((ChartExchangeNode) object)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public int getIndex(Object object) {
        return symbolStore.indexOf((ChartExchangeNode) object);
    }

    public String toString() {
        return name;
    }

    public void setMarketStatus(int status) {
        this.marketStatus = status;
        if (status == Meta.MARKET_OPEN) {
            this.status = Language.getString("STATUS_OPEN");
        } else if (status == Meta.MARKET_CLOSE) {
            this.status = Language.getString("STATUS_CLOSE");
        } else if (status == Meta.MARKET_PREOPEN) {
            this.status = Language.getString("STATUS_PREOPEN");
        } else if (status == Meta.MARKET_PRECLOSE) {
            this.status = Language.getString("STATUS_PRECLOSE");
        } else {
            this.status = "";
        }
    }


    public boolean isExchangeType() {
        return true;
    }

    class LeafCoparator implements Comparator {
        public int compare(Object o1, Object o2) {
            if (ChartNavibarConfigDialog.isSymbolOnly() || ChartNavibarConfigDialog.isSymbolShortOnly()) {
                return ((ChartSymbolNode) o1).getSymbol().compareTo(((ChartSymbolNode) o2).getSymbol());
            }
            if (ChartNavibarConfigDialog.isShortOnly() || ChartNavibarConfigDialog.isShorSymboltOnly()) {
                return ((ChartSymbolNode) o1).getShortDescription().compareTo(((ChartSymbolNode) o2).getShortDescription());
            }
            if (ChartNavibarConfigDialog.isLongOnly() || ChartNavibarConfigDialog.isLongSymbolOnly()) {
                return ((ChartSymbolNode) o1).getDescription().compareTo(((ChartSymbolNode) o2).getDescription());
            }
            return ((ChartSymbolNode) o1).getShortDescription().compareTo(((ChartSymbolNode) o2).getShortDescription());

        }
    }
}
