package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 10, 2010
 * Time: 10:39:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class Alligator extends ChartProperties implements Indicator {

    public final static byte ALLIGATOR_JAW = 1;     //blue line (upper)
    public final static byte ALLIGATOR_TEETH = 0;   //red line (center)
    public byte alligatorType = ALLIGATOR_TEETH;
    public final static byte ALLIGATOR_LIPS = -1;   //green line (lower)
    private final static byte ALLIGATOR_JAW_TIME_PERIOD = 13;
    private final static byte ALLIGATOR_TEETH_TIME_PERIOD = 8;
    private final static byte ALLIGATOR_LIPS_TIME_PERIOD = 5;
    private final static byte ALLIGATOR_JAW_OFFSET = 8;
    private final static byte ALLIGATOR_TEETH_OFFSET = 5;
    private final static byte ALLIGATOR_LIPS_OFFSET = 3;
    private String tableColumnHeading;
    private ChartProperties innerSource;

    private Alligator upper = null, lower = null;

    public Alligator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_ALLIGATOR");
        this.alligatorType = ALLIGATOR_TEETH;
        isIndicator = true;

        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
        }
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof Alligator) {
            Alligator mp = (Alligator) cp;
            this.innerSource = mp.innerSource;
            this.alligatorType = mp.alligatorType;
            this.upper = mp.upper;
            this.lower = mp.lower;
        }

        /* if (this.upper != null) {
           this.upper.assignValuesFrom(cp);
           this.upper.setAlligatorType(Alligator.ALLIGATOR_JAW);
           this.upper.setSelected(false);
       }
       if (lower != null) {
           this.lower.assignValuesFrom(cp);
           this.lower.setAlligatorType(Alligator.ALLIGATOR_LIPS);
           this.lower.setSelected(false);
       } */
    }

    public boolean hasItsOwnScale() {
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        this.innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //To change body of implemented methods use File | Settings | File Templates.

        int timePeriods;
        int offset;

        switch (alligatorType) {
            case ALLIGATOR_JAW:
                timePeriods = ALLIGATOR_JAW_TIME_PERIOD;
                offset = ALLIGATOR_JAW_OFFSET;
                break;
            case ALLIGATOR_TEETH:
                timePeriods = ALLIGATOR_TEETH_TIME_PERIOD;
                offset = ALLIGATOR_TEETH_OFFSET;
                break;
            case ALLIGATOR_LIPS:
                timePeriods = ALLIGATOR_LIPS_TIME_PERIOD;
                offset = ALLIGATOR_LIPS_OFFSET;
                break;
            default:
                timePeriods = ALLIGATOR_TEETH_TIME_PERIOD;
                offset = ALLIGATOR_TEETH_OFFSET;
                break;
        }

        int loopBegin = timePeriods - 1;

        ChartRecord cr, crTemp;
        if (timePeriods > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
            return;
        }

        // steps involved
        byte stepMidPoint = ChartRecord.STEP_1;
        byte stepMA = ChartRecord.STEP_2;
        double currVal = 0;

        //setting step size 2 (reserved)
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (cr != null) {
                currVal = (cr.High + cr.Low) / 2f;
                cr.setStepValue(stepMidPoint, currVal);
            }
        }

        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_SIMPLE, stepMidPoint, stepMA);
        for (int i = (loopBegin + offset); i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crTemp = (ChartRecord) al.get(i - offset);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(crTemp.getStepValue(stepMA));
        }
        for (int i = 0; i < loopBegin + offset; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
    }

    public String getTableColumnHeading() {

        switch (alligatorType) {
            case ALLIGATOR_TEETH:
                return tableColumnHeading + " " + "(8,5)";
            case ALLIGATOR_JAW:
                return tableColumnHeading + " " + "(13,8)";
            case ALLIGATOR_LIPS:
                return tableColumnHeading + " " + "(5,3)";
        }
        return "";
    }

    public void setTableColumnHeading(String tableColumnHeading) {

    }

    public String getShortName() {
        return getTableColumnHeading();
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        //this.timePeriods -- loaded in the super class
        this.alligatorType = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/AlligatorType"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        //this.timePeriods -- saved in the super class
        TemplateFactory.saveProperty(chart, document, "AlligatorType", Byte.toString(this.alligatorType));
    }

    public Alligator getUpper() {
        return upper;
    }

    public void setUpper(ChartProperties upper) {
        if (upper instanceof Alligator) {
            this.upper = (Alligator) upper;
        }
    }

    public Alligator getLower() {
        return lower;
    }

    public void setLower(ChartProperties lower) {
        if (lower instanceof Alligator) {
            this.lower = (Alligator) lower;
        }
    }

    public byte getAlligatorType() {
        return alligatorType;
    }

    public void setAlligatorType(byte alligatorType) {
        this.alligatorType = alligatorType;
    }
}
