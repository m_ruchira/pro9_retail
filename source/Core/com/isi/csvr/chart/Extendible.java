package com.isi.csvr.chart;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:56:00 PM
 */
public interface Extendible {
    boolean isExtendedLeft();

    void setExtendedLeft(boolean extendedLeft);

    boolean isExtendedRight();

    void setExtendedRight(boolean extendedRight);
}
