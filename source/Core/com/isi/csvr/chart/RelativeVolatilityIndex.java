package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 28, 2006
 * Time: 11:49:21 AM
 */
public class RelativeVolatilityIndex extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_RVI;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public RelativeVolatilityIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_RVI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_RVI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods,
                                          byte step1, byte step2, byte step3, byte step4, byte step5, byte step6, byte step7, byte step8, byte destIndex) {

        ChartRecord cr, crOld;
        if (al.size() < t_im_ePeriods || t_im_ePeriods < 2) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        // steps involved
        byte stepUpHigh = step1;
        byte stepUpLow = step2;
        byte stepDownHigh = step3;
        byte stepDownLow = step4;
        byte stepWSUpHigh = step5;
        byte stepWSUpLow = step6;
        byte stepWSDownHigh = step7;
        byte stepWSDownLow = step8;

        StandardDeviation.getStandardDeviation(al, bIndex, 10, GraphDataManager.INNER_High, stepUpHigh);
        StandardDeviation.getStandardDeviation(al, bIndex, 10, GraphDataManager.INNER_Low, stepUpLow);

        // correct upHigh, upLow and generate downHigh, downLow
        for (int i = 9; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (cr.High < crOld.High) {
                cr.cloneStep(stepUpHigh, stepDownHigh);//set downHigh as stdv and make upHigh = 0
                cr.setValue(stepUpHigh, 0d);
            } else if (cr.High > crOld.High) {
                cr.setValue(stepDownHigh, 0d); //keep upHigh as stdv and make downHigh = 0
            } else {
                cr.setValue(stepUpHigh, 0d); // set both as 0
                cr.setValue(stepDownHigh, 0d);
            }
            if (cr.Low < crOld.Low) {
                cr.cloneStep(stepUpLow, stepDownLow);//set downLow as stdv and make upLow = 0
                cr.setValue(stepUpLow, 0d);
            } else if (cr.Low > crOld.Low) {
                cr.setValue(stepDownLow, 0d); //keep upLow as stdv and make downLow = 0
            } else {
                cr.setValue(stepUpLow, 0d); // set both as 0
                cr.setValue(stepDownLow, 0d);
            }
        }

        //Average the upHigh and upLow - Note that this is not simple moving average
        WildersSmoothing.getWildersSmoothing(al, 9, t_im_ePeriods, stepUpHigh, stepWSUpHigh);
        WildersSmoothing.getWildersSmoothing(al, 9, t_im_ePeriods, stepUpLow, stepWSUpLow);
        WildersSmoothing.getWildersSmoothing(al, 9, t_im_ePeriods, stepDownHigh, stepWSDownHigh);
        WildersSmoothing.getWildersSmoothing(al, 9, t_im_ePeriods, stepDownLow, stepWSDownLow);

        int loopBegin = 8 + bIndex + t_im_ePeriods;// { 10-1 + timePeriods-1 = 8+timePeriods
        //int adj = 9; // 10-1
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        double rvih, rvil;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            rvih = 100d * cr.getValue(stepWSUpHigh) / (cr.getValue(stepWSUpHigh) + cr.getValue(stepWSDownHigh));
            rvil = 100d * cr.getValue(stepWSUpLow) / (cr.getValue(stepWSUpLow) + cr.getValue(stepWSDownLow));
            cr.setValue(destIndex, (rvih + rvil) / 2d);
        }


    }

    public static int getAuxStepCount() {
        return 8;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof RelativeVolatilityIndex) {
            RelativeVolatilityIndex rvi = (RelativeVolatilityIndex) cp;
            this.timePeriods = rvi.timePeriods;
            this.innerSource = rvi.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_RVI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // This calculation includes 8 intemediate steps:
    // UpHigh, UpLow, DownHigh, DownLow + Wilders smoothing of those 4
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        if (al.size() < timePeriods || timePeriods < 2) return;

        long entryTime = System.currentTimeMillis();

        ChartRecord cr, crOld;
        //setting step size 8
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(8);
        }
        // steps involved
        byte stepUpHigh = ChartRecord.STEP_1;
        byte stepUpLow = ChartRecord.STEP_2;
        byte stepDownHigh = ChartRecord.STEP_3;
        byte stepDownLow = ChartRecord.STEP_4;
        byte stepWSUpHigh = ChartRecord.STEP_5;
        byte stepWSUpLow = ChartRecord.STEP_6;
        byte stepWSDownHigh = ChartRecord.STEP_7;
        byte stepWSDownLow = ChartRecord.STEP_8;

        StandardDeviation.getStandardDeviation(al, 0, 10, GraphDataManager.INNER_High, stepUpHigh);
        StandardDeviation.getStandardDeviation(al, 0, 10, GraphDataManager.INNER_Low, stepUpLow);

        // correct upHigh, upLow and generate downHigh, downLow
        for (int i = 9; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (cr.High < crOld.High) {
                cr.cloneStep(stepUpHigh, stepDownHigh);//set downHigh as stdv and make upHigh = 0
                cr.setStepValue(stepUpHigh, 0d);
            } else if (cr.High > crOld.High) {
                cr.setStepValue(stepDownHigh, 0d); //keep upHigh as stdv and make downHigh = 0
            } else {
                cr.setStepValue(stepUpHigh, 0d); // set both as 0
                cr.setStepValue(stepDownHigh, 0d);
            }
            if (cr.Low < crOld.Low) {
                cr.cloneStep(stepUpLow, stepDownLow);//set downLow as stdv and make upLow = 0
                cr.setStepValue(stepUpLow, 0d);
            } else if (cr.Low > crOld.Low) {
                cr.setStepValue(stepDownLow, 0d); //keep upLow as stdv and make downLow = 0
            } else {
                cr.setStepValue(stepUpLow, 0d); // set both as 0
                cr.setStepValue(stepDownLow, 0d);
            }
        }

        //Average the upHigh and upLow
        WildersSmoothing.getWildersSmoothing(al, 9, timePeriods, stepUpHigh, stepWSUpHigh);
        WildersSmoothing.getWildersSmoothing(al, 9, timePeriods, stepUpLow, stepWSUpLow);
        WildersSmoothing.getWildersSmoothing(al, 9, timePeriods, stepDownHigh, stepWSDownHigh);
        WildersSmoothing.getWildersSmoothing(al, 9, timePeriods, stepDownLow, stepWSDownLow);

        int loopBegin = 8 + timePeriods;// 10-1 + timePeriods-1 = 8+timePeriods
        //int adj = 9; // 10-1
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        double rvih, rvil;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            rvih = 100d * cr.getStepValue(stepWSUpHigh) / (cr.getStepValue(stepWSUpHigh) + cr.getStepValue(stepWSDownHigh));
            rvil = 100d * cr.getStepValue(stepWSUpLow) / (cr.getStepValue(stepWSUpLow) + cr.getStepValue(stepWSDownLow));
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue((rvih + rvil) / 2d);
            }
        }

        //System.out.println("**** RVI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public String getShortName() {
        return Language.getString("IND_RVI");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
