package com.isi.csvr.chart;

/**
 * User: Pramoda
 * Date: Apr 11, 2007
 * Time: 1:11:20 PM
 */
public interface GraphDataManagerIF {
    ChartPoint getIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID);

    void removeIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID);

    int getClosestIndexFortheTime(long t);

    int getIndexForSourceCP(ChartProperties cp);

    ChartPoint readChartPoint(long timeIndex, int graphIndex);
}
