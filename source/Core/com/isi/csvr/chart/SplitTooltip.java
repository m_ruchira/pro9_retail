package com.isi.csvr.chart;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Jan 9, 2008
 * Time: 5:11:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplitTooltip extends JPanel {

    ArrayList<ChartSplitPoints> alSplits = new ArrayList<ChartSplitPoints>();
    private int blockHeight = 50;
    private int blockWidth = 140;

    public void setTooltipData(ArrayList<ChartSplitPoints> alSplits) {
        this.alSplits = alSplits;
        this.setSize(150, blockHeight * alSplits.size());
    }


    public void paint(Graphics g) {
        super.paint(g);
        if (alSplits.size() > 0) {
            int yAdj = 13, xAdj = 10, xGap = 5, yGap = 5;
            for (int i = 0; i < alSplits.size(); i++) {
                ChartSplitPoints csps = alSplits.get(i);
                int y = i * blockHeight + yAdj;
                int y2 = y + 14, y3 = y + 27;
                // border
                g.setColor(Color.LIGHT_GRAY);
                g.drawRoundRect(xAdj - xGap, y - yGap, blockWidth, blockHeight - yAdj, xGap, yGap);
                //title
                g.setFont(StockGraph.font_BOLD_10);
                String strName = ChartSplitPoints.getSplitPointName(csps.type);
                int wdName = g.getFontMetrics().stringWidth(strName);
                g.setColor(this.getBackground());
                g.fillRect(xAdj, y - yGap, wdName + 10, 15);
                g.setColor(this.getForeground());
                g.drawString(strName, xAdj + 5, y);
                //Date label
                g.setFont(StockGraph.font_PLAIN_10);
                g.drawString("Date", xAdj + 5, y2);
                Date dt = new Date(csps.date);
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                g.drawString(" : " + formatter.format(dt), xAdj + 40, y2);
                //Value label
                g.drawString("Value", xAdj + 5, y3);
                g.drawString(" : " + Float.toString(csps.value), xAdj + 40, y3);
            }
        }
    }
}
