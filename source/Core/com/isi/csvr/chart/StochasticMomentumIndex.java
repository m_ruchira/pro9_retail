package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorStochasticMomentumIndexProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 5, 2006
 * Time: 1:25:50 PM
 */
public class StochasticMomentumIndex extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_SMO;
    //Fields
    private int timePeriods;
    private int smoothingPeriod;
    private int doubleSmoothingPeriod;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public StochasticMomentumIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_SMO") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_SMO");
        this.timePeriods = 5;
        smoothingPeriod = 3;
        doubleSmoothingPeriod = 3;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorStochasticMomentumIndexProperty idp = (IndicatorStochasticMomentumIndexProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTimePeriods(idp.getTimePeriods());
            this.setSmoothingPeriod(idp.getSmoothingPeriod());
            this.setDoubleSmoothingPeriod(idp.getDoubleSmoothingPeriod());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, int smoothingPeriod, int doubleSmoothingPeriod, byte step1, byte step2, byte step3, byte step4, byte step5, byte step6, byte destIndex) {
        byte stepMidDis = step1;
        byte stepMidSmooth1 = step2;
        byte stepMidSmooth2 = step3;

        byte stepDif = step4;
        byte stepDifSmooth1 = step5;
        byte stepDifSmooth2 = step6;

        ChartRecord cr, crPre;
        int startIndex = bIndex + t_im_ePeriods - 1;
        if (al.size() < startIndex || t_im_ePeriods == 0 || smoothingPeriod == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        double highestHigh = Double.MIN_VALUE;
        double lowestLow = Double.MAX_VALUE;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            highestHigh = Double.MIN_VALUE;
            lowestLow = Double.MAX_VALUE;
            if (i >= startIndex) {
                for (int j = 0; j < t_im_ePeriods; j++) {
                    crPre = (ChartRecord) al.get(i - j);
                    if (crPre.High > highestHigh)
                        highestHigh = crPre.High;
                    if (crPre.Low < lowestLow)
                        lowestLow = crPre.Low;
                }
                cr.setValue(stepMidDis, cr.Close - ((highestHigh + lowestLow) / 2));
                cr.setValue(stepDif, highestHigh - lowestLow);
            }
        }

        MovingAverage.getMovingAverage(al, startIndex + smoothingPeriod, smoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepMidDis, stepMidSmooth1);
        MovingAverage.getMovingAverage(al, startIndex + 2 * (smoothingPeriod), doubleSmoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepMidSmooth1, stepMidSmooth2);

        MovingAverage.getMovingAverage(al, startIndex + smoothingPeriod, smoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepDif, stepDifSmooth1);
        MovingAverage.getMovingAverage(al, startIndex + 2 * (smoothingPeriod), doubleSmoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepDifSmooth1, stepDifSmooth2);

        startIndex = startIndex + 2 * (smoothingPeriod);

        for (int i = startIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (Double.isNaN(cr.getValue(stepMidSmooth2)) || Double.isNaN(cr.getValue(stepDifSmooth2)))
                cr.setValue(destIndex, Indicator.NULL);
            else
                cr.setValue(destIndex, (cr.getValue(stepMidSmooth2) / (cr.getValue(stepDifSmooth2) / 2)) * 100);
        }
    }

    public static int getAuxStepCount() {
        return 6;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 5;
        this.smoothingPeriod = 3;
        this.doubleSmoothingPeriod = 3;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.smoothingPeriod = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SmoothingPeriods"));
        this.doubleSmoothingPeriod = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/DoubleSmoothingPeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "SmoothingPeriods", Integer.toString(this.smoothingPeriod));
        TemplateFactory.saveProperty(chart, document, "DoubleSmoothingPeriods", Integer.toString(this.doubleSmoothingPeriod));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof StochasticMomentumIndex) {
            StochasticMomentumIndex smo = (StochasticMomentumIndex) cp;
            this.timePeriods = smo.timePeriods;
            this.smoothingPeriod = smo.smoothingPeriod;
            this.doubleSmoothingPeriod = smo.doubleSmoothingPeriod;
            this.innerSource = smo.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_SMO") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public int getSmoothingPeriod() {
        return smoothingPeriod;
    }

    public void setSmoothingPeriod(int smoothingPeriod) {
        this.smoothingPeriod = smoothingPeriod;
    }

    public int getDoubleSmoothingPeriod() {
        return doubleSmoothingPeriod;
    }
    //############################################

    public void setDoubleSmoothingPeriod(int doubleSmoothingPeriod) {
        this.doubleSmoothingPeriod = doubleSmoothingPeriod;
    }

    // this includes 6 intermediate steps CminusMP, HminusL, EMAofC-MP, EMAofEMAofC-MP, EMAofH-L, EMAofEMAofH-L
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        if ((timePeriods >= al.size()) || (timePeriods < 2)) return;
        ChartRecord cr;
        //setting step size 6
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(6);
        }
        // steps involved
        byte stepCminusMP = ChartRecord.STEP_1;
        byte stepHminusL = ChartRecord.STEP_2;
        byte stepEMAofC = ChartRecord.STEP_3;
        byte stepEMAofEMAofC = ChartRecord.STEP_4;
        byte stepEMAofHL = ChartRecord.STEP_5;
        byte stepEMAofEMAofHL = ChartRecord.STEP_6;

        for (int i = timePeriods - 1; i < al.size(); i++) {
            double highestH = -Double.MAX_VALUE;
            double lowestL = Double.MAX_VALUE;
            for (int j = 0; j < timePeriods; j++) {
                cr = (ChartRecord) al.get(i - j);
                highestH = Math.max(highestH, cr.High);
                lowestL = Math.min(lowestL, cr.Low);
            }
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepCminusMP, cr.Close - (highestH + lowestL) / 2f);
            cr.setStepValue(stepHminusL, highestH - lowestL);
        }
        // C - MidPoint
        MovingAverage.getMovingAverage(al, timePeriods - 1, smoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepCminusMP, stepEMAofC); //Smooth once
        MovingAverage.getMovingAverage(al, timePeriods + smoothingPeriod - 2, doubleSmoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepEMAofC, stepEMAofEMAofC); //Smooth twice
        // H - L
        MovingAverage.getMovingAverage(al, timePeriods - 1, smoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepHminusL, stepEMAofHL); //Smooth once
        MovingAverage.getMovingAverage(al, timePeriods + smoothingPeriod - 2, doubleSmoothingPeriod, MovingAverage.METHOD_EXPONENTIAL, stepEMAofHL, stepEMAofEMAofHL); //Smooth twice

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (i >= (timePeriods + smoothingPeriod + doubleSmoothingPeriod - 3)) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    double value = 200 * cr.getStepValue(stepEMAofEMAofC) / cr.getStepValue(stepEMAofEMAofHL);
                    if (Double.isNaN(value) || Double.isInfinite(value)) value = 50d;
                    aPoint.setIndicatorValue(value);
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        //System.out.println("**** SMO Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_SMO");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
