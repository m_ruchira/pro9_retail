package com.isi.csvr.chart;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */


import javax.swing.*;
import java.awt.*;


/**
 * Creates a Beveled arrow icon to indicate the
 * column sort order
 * Implements the Icon interface and draws an arrow
 * using graphics to indicate the direction
 *
 * @version 1.0 02/26/99
 * @Author unknown (This is a downloaded class from the internet)
 */
public class ComboImage implements Icon {
    private static final int WIDTH = 20;
    private static final int HEIGHT = 20;
    private static final int GAP = 4;
    public static ComboImage disabledImage = new ComboImage(new Color(200, 200, 200), Color.LIGHT_GRAY);
    private Color color;
    private Color borderColor = Color.black;

    /**
     * Constructor
     */
    public ComboImage(Color color) {
        this.color = color;
    }

    /**
     * Constructor 2
     */
    public ComboImage(Color color, Color borderColor) {
        this.color = color;
        this.borderColor = borderColor;
    }

    public static ComboImage getDisabledImage() {
        return disabledImage;
    }

    /**
     * Paint the icon object. This draws the icon for the selected
     * direction
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        drawIcon(g, x, y);
    }

    /**
     * Returns the width of the icon
     */
    public int getIconWidth() {
        return this.WIDTH; //+5
    }

    /**
     * Returns the height of the icon
     */
    public int getIconHeight() {
        return this.HEIGHT;
    }

    /**
     * Draws the down arrow using graphics
     */
    private void drawIcon(Graphics g, int xo, int yo) {
        g.setColor(color);
        g.fillRect(xo + GAP, yo + GAP, this.WIDTH - 2 * GAP, this.HEIGHT - 2 * GAP);
        g.setColor(borderColor);
        g.drawRect(xo + GAP, yo + GAP, this.WIDTH - 2 * GAP, this.HEIGHT - 2 * GAP);
    }

    public void setColor(Color c) {
        this.color = c;
    }

}

