package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeThreeParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 7:55:31 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class KlingerOscillator extends ChartProperties implements Indicator, Serializable {

    private static final long serialVersionUID = UID_KLINGER_OSCILL;
    //Fields
    private int signalTimePeriods;
    private byte signalStyle;
    private Color signalColor;
    private ChartProperties innerSource;
    //private int innerSourceIndex;
    private MovingAverage signalLine;
    private String tableColumnHeading;

    public KlingerOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_KLINGER_OSCILL") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_KLINGER_OSCILL");
        signalTimePeriods = 13;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeThreeParameterProperty idp = (IndicatorTypeThreeParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setSignalTimePeriods(idp.getTimePeriods());
            this.setSignalColor(idp.getLineColor());
            this.setSignalStyle(idp.getLineStyle2());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, byte stepVolumeForce, byte stepEMA34, byte stepEMA55,
                                          byte destIndex) {
        double currVal, kvo, trend, preTrend, dirOfTrend, dm, cm, volumeForce, preDir, preDM;
        ChartRecord cr;

        if (al.size() < 56) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }
        long entryTime = System.currentTimeMillis();
        //setting step size 3
        /*for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }*/
        // steps involved
        /*byte stepVolumeForce = ChartRecord.STEP_1;
        byte stepEMA34 = ChartRecord.STEP_2;
        byte stepEMA55 = ChartRecord.STEP_3;*/

        cr = (ChartRecord) al.get(0);
        preTrend = cr.High + cr.Low + cr.Close;
        preDir = 1;
        preDM = cr.High - cr.Low;
        cm = preDM;

        for (int i = 1 + bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            trend = cr.High + cr.Low + cr.Close;
            dirOfTrend = (trend >= preTrend) ? 1f : -1f;
            dm = cr.High - cr.Low;
            cm = (dirOfTrend == preDir) ? cm + dm : preDM + dm;

            currVal = 2f * dm / cm - 1;
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 1;
            volumeForce = cr.Volume * dirOfTrend * 100f * Math.abs(currVal);
            cr.setStepValue(stepVolumeForce, volumeForce);

            preTrend = trend;
            preDir = dirOfTrend;
            preDM = dm;
        }
        MovingAverage.getMovingAverage(al, 1, 34, MovingAverage.METHOD_EXPONENTIAL, stepVolumeForce, stepEMA34);
        MovingAverage.getMovingAverage(al, 1, 55, MovingAverage.METHOD_EXPONENTIAL, stepVolumeForce, stepEMA55);

        for (int i = 55; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            kvo = cr.getStepValue(stepEMA34) - cr.getStepValue(stepEMA55);
            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint!=null){
                aPoint.setIndicatorValue(kvo);
            }*/
            cr.setValue(destIndex, kvo);
        }
        //System.out.println("**** Klinger Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.signalTimePeriods = 14;
        this.signalStyle = GraphDataManager.STYLE_DASH;
        this.signalColor = Color.DARK_GRAY;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof KlingerOscillator) {
            KlingerOscillator ma = (KlingerOscillator) cp;
            this.signalTimePeriods = ma.signalTimePeriods;
            this.signalStyle = ma.signalStyle;
            this.signalColor = ma.signalColor;
            this.innerSource = ma.innerSource;
            if ((this.signalLine != null) && (ma.signalLine != null)) {
                this.signalLine.assignValuesFrom(ma.signalLine);
            }
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }
    //############################################

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this calculation involves 3 intermediate steps
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal, kvo, trend, preTrend, dirOfTrend, dm, cm, volumeForce, preDir, preDM;
        ChartRecord cr;

        if (al.size() < 56) return;
        long entryTime = System.currentTimeMillis();
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepVolumeForce = ChartRecord.STEP_1;
        byte stepEMA34 = ChartRecord.STEP_2;
        byte stepEMA55 = ChartRecord.STEP_3;

        cr = (ChartRecord) al.get(0);
        preTrend = cr.High + cr.Low + cr.Close;
        preDir = 1;
        preDM = cr.High - cr.Low;
        cm = preDM;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            trend = cr.High + cr.Low + cr.Close;
            dirOfTrend = (trend >= preTrend) ? 1f : -1f;
            dm = cr.High - cr.Low;
            cm = (dirOfTrend == preDir) ? cm + dm : preDM + dm;

            currVal = 2f * dm / cm - 1;
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 1;
            volumeForce = cr.Volume * dirOfTrend * 100f * Math.abs(currVal);
            cr.setStepValue(stepVolumeForce, volumeForce);

            preTrend = trend;
            preDir = dirOfTrend;
            preDM = dm;
        }
        MovingAverage.getMovingAverage(al, 1, 34, MovingAverage.METHOD_EXPONENTIAL, stepVolumeForce, stepEMA34);
        MovingAverage.getMovingAverage(al, 1, 55, MovingAverage.METHOD_EXPONENTIAL, stepVolumeForce, stepEMA55);

        for (int i = 55; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            kvo = cr.getStepValue(stepEMA34) - cr.getStepValue(stepEMA55);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(kvo);
            }
        }
        //System.out.println("**** Klinger Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + signalTimePeriods;
        return Language.getString("IND_KLINGER_OSCILL") + " " + parent;
    }

    //signalTimePeriods
    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int tp) {
        signalTimePeriods = tp;
        if (signalLine != null)
            signalLine.setTimePeriods(tp);
    }

    //signalStyle;
    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(Byte ss) {
        setSignalStyle(ss.byteValue());
    }

    public void setSignalStyle(byte ss) {
        signalStyle = ss;
        if (signalLine != null)
            signalLine.setPenStyle(ss);
    }

    //signalColor;
    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color c) {
        signalColor = c;
        if (signalLine != null)
            signalLine.setColor(c);
    }

    //signalLine
    public MovingAverage getSignalLine() {
        return signalLine;
    }
    //record size - override
//	public void setRecordSize(int size){
//		super.setRecordSize(size);
//		if (signalLine!=null){
//			signalLine.setRecordSize(size);
//		}
//	}

    public void setSignalLine(MovingAverage ma) {
        signalLine = ma;
    }

    public String getShortName() {
        return Language.getString("IND_KLINGER_OSCILL");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}