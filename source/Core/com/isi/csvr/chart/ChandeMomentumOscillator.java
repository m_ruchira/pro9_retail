package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 5, 2006
 * Time: 10:35:28 AM
 */
public class ChandeMomentumOscillator extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_CMO;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private MovingAverage signalLine;
    private Color signalColor;
    private int signalTimePeriods;
    private byte signalStyle;
    private String tableColumnHeading;

    public ChandeMomentumOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_CMO") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_CMO");
        this.timePeriods = 20;
        signalTimePeriods = 3;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
            this.setTimePeriods(idp.getTimePeriods());

        }
    }

    public static void getChandeMomentumOscillator(ArrayList al, int bIndex, int timePeriods, byte srcIndex, byte destIndex) {

        long entryTime = System.currentTimeMillis();
        double upSum = 0f, downSum = 0f;

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            ChartRecord cR = (ChartRecord) al.get(i);
            double val = cR.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        int loopBegin = bIndex + timePeriods + 1;
        if ((loopBegin >= al.size()) || (loopBegin < bIndex + 3)) return;

        for (int i = bIndex + 1; i < loopBegin; i++) {
            ChartRecord cr = (ChartRecord) al.get(i);
            ChartRecord crPre = (ChartRecord) al.get(i - 1);
            double val = cr.getStepValue(srcIndex);
            double valPre = crPre.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) && (Indicator.NULL != val) &&
                    !Double.isNaN(valPre) && (Double.NEGATIVE_INFINITY != valPre) && (Double.POSITIVE_INFINITY != valPre) && (Indicator.NULL != valPre)) {
                double diff = val - valPre;
                if (diff >= 0f) {
                    upSum += diff;
                } else {
                    downSum += -diff;
                }
            }

            if (i == loopBegin - 1) { //insert the first point
                cr.setStepValue(destIndex, (upSum + downSum > 0f) ? 100f * (upSum - downSum) / (upSum + downSum) : 0f);
            } else { // remove any previous data before the start point
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            //inserting new value
            ChartRecord cr = (ChartRecord) al.get(i);
            ChartRecord crPre = (ChartRecord) al.get(i - 1);
            double val = cr.getStepValue(srcIndex);
            double valPre = crPre.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) && (Indicator.NULL != val) &&
                    !Double.isNaN(valPre) && (Double.NEGATIVE_INFINITY != valPre) && (Double.POSITIVE_INFINITY != valPre) && (Indicator.NULL != valPre)) {
                double diff = val - valPre;
                if (diff >= 0f) {
                    upSum += diff;
                } else {
                    downSum += -diff;
                }
            }
            //removing old value
            ChartRecord crOld = (ChartRecord) al.get(i - timePeriods);
            ChartRecord crPreOld = (ChartRecord) al.get(i - 1 - timePeriods);
            double valOld = crOld.getStepValue(srcIndex);
            double valPreOld = crPreOld.getStepValue(srcIndex);
            if (!Double.isNaN(valOld) && (Double.NEGATIVE_INFINITY != valOld) && (Double.POSITIVE_INFINITY != valOld) && (Indicator.NULL != valOld) &&
                    !Double.isNaN(valPreOld) && (Double.NEGATIVE_INFINITY != valPreOld) && (Double.POSITIVE_INFINITY != valPreOld) && (Indicator.NULL != valPreOld)) {
                double diffOld = valOld - valPreOld;
                if (diffOld >= 0f) {
                    upSum -= diffOld;
                } else {
                    downSum -= -diffOld;
                }
            }

            //double val = (upSum+downSum>0f)? 100f*(upSum-downSum)/(upSum+downSum): 0f;
            cr.setStepValue(destIndex, (upSum + downSum > 0f) ? 100f * (upSum - downSum) / (upSum + downSum) : 0f);
        }
        //System.out.println("**** get ChandeMomentumOscillator Calc time " + (entryTime - System.currentTimeMillis()));

    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte srcIndex, byte destIndex) {

        getChandeMomentumOscillator(al, bIndex, t_im_ePeriods, srcIndex, destIndex);
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 20;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ChandeMomentumOscillator) {
            ChandeMomentumOscillator cmo = (ChandeMomentumOscillator) cp;
            this.timePeriods = cmo.timePeriods;
            this.innerSource = cmo.innerSource;
            this.signalTimePeriods = cmo.signalTimePeriods;
            this.signalStyle = cmo.signalStyle;
            this.signalColor = cmo.signalColor;
            if ((this.signalLine != null) && (cmo.signalLine != null)) {
                this.signalLine.assignValuesFrom(cmo.signalLine);
            }
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_CMO") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public MovingAverage getSignalLine() {
        return signalLine;
    }

    public void setSignalLine(MovingAverage signalLine) {
        this.signalLine = signalLine;
    }

    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color signalColor) {
        this.signalColor = signalColor;
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int signalTimePeriods) {
        this.signalTimePeriods = signalTimePeriods;
    }
    //############################################

/*
    public static void getChandeMomentumOscillator(ArrayList al, int bIndex, int timePeriods, byte srcIndex, byte destIndex){

        long entryTime = System.currentTimeMillis();
        double upSum = 0f, downSum = 0f;

        int loopBegin = bIndex+timePeriods+1;
        if ((loopBegin>=al.size())||(loopBegin<bIndex+3)) return;

        for (int i=bIndex+1; i<loopBegin; i++) {
            ChartRecord cr = (ChartRecord)al.get(i);
            ChartRecord crPre = (ChartRecord)al.get(i-1);
            double diff = cr.getStepValue(srcIndex) - crPre.getStepValue(srcIndex);
            if (diff >= 0f) {
                upSum += diff;
            } else {
                downSum += -diff;
            }

            if (i==loopBegin-1){ //insert the first point
                cr.setStepValue(destIndex, (upSum+downSum>0f)? 100f*(upSum-downSum)/(upSum+downSum): 0f);
            }else{ // remove any previous data before the start point
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        for(int i=loopBegin; i<al.size(); i++){
            //inserting new value
            ChartRecord cr = (ChartRecord)al.get(i);
            ChartRecord crPre = (ChartRecord)al.get(i-1);
            double diff = cr.getStepValue(srcIndex) - crPre.getStepValue(srcIndex);
            if (diff >= 0f) {
                upSum += diff;
            } else {
                downSum += -diff;
            }
            //removing old value
            ChartRecord crOld = (ChartRecord)al.get(i-timePeriods);
            ChartRecord crPreOld = (ChartRecord)al.get(i-1-timePeriods);
            double diffOld = crOld.getStepValue(srcIndex) - crPreOld.getStepValue(srcIndex);
            if (diffOld >= 0f) {
                upSum -= diffOld;
            } else {
                downSum -= -diffOld;
            }

            double val = (upSum+downSum>0f)? 100f*(upSum-downSum)/(upSum+downSum): 0f;
            cr.setStepValue(destIndex, val);
        }
        System.out.println("**** get ChandeMomentumOscillator Calc time " + (entryTime - System.currentTimeMillis()));

    }
*/

    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(byte signalStyle) {
        this.signalStyle = signalStyle;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        double upSum = 0f, downSum = 0f;

        int loopBegin = timePeriods + 1;
        if ((loopBegin >= al.size()) || (loopBegin < 3)) return;

        for (int i = 1; i < loopBegin; i++) {
            ChartRecord cr = (ChartRecord) al.get(i);
            ChartRecord crPre = (ChartRecord) al.get(i - 1);
            double diff = cr.getValue(this.getOHLCPriority()) - crPre.getValue(this.getOHLCPriority());
            if (diff >= 0f) {
                upSum += diff;
            } else {
                downSum += -diff;
            }

            if (i == loopBegin - 1) { //insert the first point
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue((upSum + downSum > 0f) ? 100f * (upSum - downSum) / (upSum + downSum) : 0f);
            } else { // remove any previous data before the start point
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            //inserting new value
            ChartRecord cr = (ChartRecord) al.get(i);
            ChartRecord crPre = (ChartRecord) al.get(i - 1);
            double diff = cr.getValue(this.getOHLCPriority()) - crPre.getValue(this.getOHLCPriority());
            if (diff >= 0f) {
                upSum += diff;
            } else {
                downSum += -diff;
            }
            //removing old value
            ChartRecord crOld = (ChartRecord) al.get(i - timePeriods);
            ChartRecord crPreOld = (ChartRecord) al.get(i - 1 - timePeriods);
            double diffOld = crOld.getValue(this.getOHLCPriority()) - crPreOld.getValue(this.getOHLCPriority());
            if (diffOld >= 0f) {
                upSum -= diffOld;
            } else {
                downSum -= -diffOld;
            }

            double val = (upSum + downSum > 0f) ? 100f * (upSum - downSum) / (upSum + downSum) : 0f;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(val);
        }
        //System.out.println("**** ChandeMomentumOscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_CMO");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
