package com.isi.csvr.chart;

import java.awt.geom.Line2D;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 9, 2010
 * Time: 9:48:00 AM
 * To change this template use File | Settings | File Templates.
 */
public interface Alarmable {

    boolean isAlarmEnabled();

    void setAlarmEnabled(boolean enabled);

    String getAlarmMessage();

    void setAlarmMessage(String msg);

    boolean isIntesectingLine(Line2D priceLine);
}
