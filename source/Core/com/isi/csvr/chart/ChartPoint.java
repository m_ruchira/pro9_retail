package com.isi.csvr.chart;

/**
 * User: udaka
 * Date: Apr 30, 2006
 * Time: 1:51:36 AM
 */
public class ChartPoint {
    public double Open;
    public double High;
    public double Low;
    public double Close;
    public double Volume; // this can handle up to 16 digits without losing precision
    public double TurnOver; // this can handle up to 16 digits without losing precision

    public ChartPoint() {

    }

    public double getValue(byte OHLCVPriority) {
        switch (OHLCVPriority) {
            case GraphDataManager.INNER_Open: // = GraphDataManager.INDICATOR
                return this.Open;
            case GraphDataManager.INNER_High:
                return this.High;
            case GraphDataManager.INNER_Low:
                return this.Low;
            case GraphDataManager.INNER_Close:
                return this.Close;
            case GraphDataManager.INNER_Volume:
                return this.Volume;
            case GraphDataManager.INNER_TurnOver:
                return this.TurnOver;
        }
        return this.Open;
    }

    public void setValue(byte OHLCVPriority, double value) {
        switch (OHLCVPriority) {
            case GraphDataManager.INNER_Open: // = GraphDataManager.INDICATOR
                this.Open = value;
                break;
            case GraphDataManager.INNER_High:
                this.High = value;
                break;
            case GraphDataManager.INNER_Low:
                this.Low = value;
                break;
            case GraphDataManager.INNER_Close:
                this.Close = value;
                break;
            case GraphDataManager.INNER_Volume:
                this.Volume = value;
                break;
            case GraphDataManager.INNER_TurnOver:
                this.TurnOver = value;
                break;
        }
    }

    public void setIndicatorValue(double value) {
        this.Open = value;
    }

    public void setIndicatorValue(double value1, double value2) {
        this.Open = value1;
        this.Close = value2;

    }

    public void setIndicatorValue(double valueOpen, double valueHigh, double valueLow) {
        this.Open = valueOpen;
        this.High = valueHigh;
        this.Low = valueLow;


    }
}
