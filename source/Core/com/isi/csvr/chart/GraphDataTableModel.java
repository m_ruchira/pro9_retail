package com.isi.csvr.chart;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Sep 4, 2008
 * Time: 11:42:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class GraphDataTableModel extends CommonTable implements TableModel, CommonTableInterface, ListSelectionListener {

    TWDecimalFormat indicatorFormatter = null;
    private GraphFrame frame = null;
    private GraphDataManager gdm = null;
    private int noOfIndicators = 0;
    private String exchange = null;


    public GraphDataTableModel(GraphFrame gf) {
        this.frame = gf;
    }

    public void setStore(String key, GraphDataManager GDMR) {
        this.gdm = GDMR;
        this.exchange = SharedMethods.getExchangeFromKey(key);

    }

    public void setStore(GraphDataManager GDMR) {
        this.gdm = GDMR;

    }

    /*implemements the methods of the TableModel interface */

    public int getRowCount() {
        try {
            if (gdm == null) {
                return 0;
            } else {
                return gdm.getLastNotNullIndexOfBaseSymbolPoints() + 1;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public int getColumnCount() {
        /*if (getViewSettings() == null) {
            return 0;
        } else if (gdm == null) {
            return getViewSettings().getColumnHeadings().length;
        } else {
            if (noOfIndicators != gdm.getIndicators().size()) {
                int viewHeadingLength = getViewSettings().getColumnHeadings().length;
                String[] columnHeadings = new String[viewHeadingLength - noOfIndicators];

                for (int i = 0; i < columnHeadings.length; i++) {
                    columnHeadings[i] = getViewSettings().getColumnHeadings()[i];
                }
                getViewSettings().setColumnHeadings(columnHeadings);
                columnHeadings = null;

                noOfIndicators = gdm.getIndicators().size();
                viewHeadingLength = getViewSettings().getColumnHeadings().length;

                columnHeadings = new String[viewHeadingLength + noOfIndicators];

                //append column headings
                for (int j = 0; j < viewHeadingLength; j++) {
                    columnHeadings[j] = getViewSettings().getColumnHeadings()[j];
                }
                //append indicator headings
                for (int k = 0; k < noOfIndicators; k++) {
                    columnHeadings[viewHeadingLength + k] = ((Indicator) gdm.getIndicators().get(k)).getTableColumnHeading();
                }
                getViewSettings().setColumnHeadings(columnHeadings);
                columnHeadings = null;

                return (viewHeadingLength + noOfIndicators);
            } else {
                return getViewSettings().getColumnHeadings().length;
            }
        }*/


        if (getViewSettings() == null) {
            return 0;
        } else if (gdm == null) {
            return getViewSettings().getColumnHeadings().length;
        } else {
            //if (noOfIndicators != gdm.getIndicators().size()) {
            //if (true) {
            int viewHeadingLength = getViewSettings().getColumnHeadings().length;
            String[] colHeadings = new String[(viewHeadingLength - noOfIndicators)];
            for (int j = 0; j < (viewHeadingLength - noOfIndicators); j++) {
                colHeadings[j] = getViewSettings().getColumnHeadings()[j];
            }
            getViewSettings().setColumnHeadings(colHeadings);
            colHeadings = null;

            noOfIndicators = gdm.getIndicators().size();

            viewHeadingLength = getViewSettings().getColumnHeadings().length;
            colHeadings = new String[(viewHeadingLength + noOfIndicators)];
            for (int j = 0; j < viewHeadingLength; j++) {
                colHeadings[j] = getViewSettings().getColumnHeadings()[j];
            }
            for (int i = 0; i < noOfIndicators; i++) {
                colHeadings[viewHeadingLength + i] = ((Indicator) gdm.getIndicators().get(i)).getTableColumnHeading();
            }
            getViewSettings().setColumnHeadings(colHeadings);
            colHeadings = null;
            return (viewHeadingLength + noOfIndicators);
        }
    }

    public String getColumnName(int columnIndex) {

        String columnName = "";
        try {
            columnName = getViewSettings().getColumnHeadings()[columnIndex];
        } catch (Exception ex) { //array out of bound exception
            columnName = ((Indicator) gdm.getIndicators().get(columnIndex - 9)).getTableColumnHeading();
        }
        //System.out.println(" ===== column name ===== : " + columnName);
        return columnName;
    }

    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0 || columnIndex == 5) {
            return Long.class;
        } else if (columnIndex == 6) {
            return Double.class;
        } else {
            return Float.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            ChartRecord record = gdm.getChartRecordForTable(getRowCount() - rowIndex - 1, 0);
            if (record != null) {
                switch (columnIndex) {

                    case -1:
                        return gdm;
                    case -2:
                        return frame;
                    case 0:
                        return record.Time;
                    case 1:
                        return new Float(record.Open);
                    case 2:
                        return new Float(record.High);
                    case 3:
                        return new Float(record.Low);
                    case 4:
                        return new Float(record.Close);
                    case 5:
                        return (long) (record.Volume);
                    case 6:
                        return new Double(record.TurnOver);
                    case 7:
                        return getNetChg(getRowCount() - rowIndex - 1, (float) record.Close);
                    case 8:
                        return getPctChg(getRowCount() - 1 - rowIndex, (float) record.Close);
                    default:
                        /*ChartProperties cp = (ChartProperties) gdm.getIndicators().get(columnIndex - 9);
                        int index = gdm.getIndexForSourceCP(cp);
                        record = gdm.getChartRecordForTable(getRowCount() - rowIndex - 1, index);
                        if (record != null) {
                            return new Float(record.Open);
                        } else {
                            return 0;
                        }*/
                        ChartProperties cp = ((ChartProperties) gdm.getIndicators().get(columnIndex - 9));
                        int index = gdm.getIndexForSourceCP(cp);
                        record = gdm.getChartRecordForTable(getRowCount() - rowIndex - 1, index); //TODO

                        //int panelIndex = gdm.getIndexOfTheRect(frame.graph.panels, cp.getRect());
                        //double minMaxDiff = gdm.getMinMaxDifference(panelIndex);

                        if (record != null) {
                            //indicatorFormatter = getFormatter(minMaxDiff);
                            //return indicatorFormatter.format(record.Open);
                            return record.Open;
                        } else {
                            return Double.MAX_VALUE;
                        }
                }
            } else {
                return 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    private TWDecimalFormat getFormatter(double minMaxDiff) {

        TWDecimalFormat formatter;

        if (minMaxDiff < 0.000000009d) {
            formatter = new TWDecimalFormat("##0.0000000000");
        } else if (minMaxDiff < 0.00000009d) {
            formatter = new TWDecimalFormat("##0.000000000");
        } else if (minMaxDiff < 0.0000009d) {
            formatter = new TWDecimalFormat("##0.00000000");
        } else if (minMaxDiff < 0.000009d) {
            formatter = new TWDecimalFormat("##0.0000000");
        } else if (minMaxDiff < 0.00009d) {
            formatter = new TWDecimalFormat("##0.000000");
        } else if (minMaxDiff < 0.0009d) {
            formatter = new TWDecimalFormat("##0.00000");
        } else if (minMaxDiff < 0.009d) {
            formatter = new TWDecimalFormat("##0.0000");
        } else if (minMaxDiff < 0.09d) {
            formatter = new TWDecimalFormat("##0.000");
        } else if (minMaxDiff < 0.9d) {
            formatter = new TWDecimalFormat("##0.00");
        } else {
            formatter = new TWDecimalFormat("###,##0.00");
        }

        return formatter;
    }

    private float getNetChg(int row, float close) {
        float value;
        try {
            if (row == 0) { //period begin day
                Object[] preRecord = gdm.getPrePeriodRecord();
                if (preRecord[1] != null) {
                    ChartPoint preChartPoint = (ChartPoint) preRecord[1];
                    value = close - (float) preChartPoint.Close;
                } else {
                    value = close - 0;
                }
            } else {
                ChartPoint prevDay = gdm.getChartRecordForTable(row - 1, 0);
                value = close - (float) prevDay.Close;
                prevDay = null;

            }

            return value;
        } catch (Exception e) {
            return 0;
        }

    }

    private float getPctChg(int row, float close) {
        float value;
        try {
            if (row == 0) { //period begin day
                Object[] prePeriodRecord = gdm.getPrePeriodRecord();
                if (prePeriodRecord[1] != null) {
                    ChartPoint prePeriodDay = (ChartPoint) prePeriodRecord[1];
                    if (prePeriodDay.Close != 0) {
                        value = close - (float) prePeriodDay.Close;
                        value = (value * 100) / (float) prePeriodDay.Close;
                    } else {
                        value = 0;// actually Not a Number
                    }


                } else {
                    value = 0;

                }
            } else {
                ChartPoint prevDay = gdm.getChartRecordForTable(row - 1, 0);
                if (prevDay.Close != 0) {
                    value = close - (float) prevDay.Close;
                    value = (value * 100) / (float) prevDay.Close;
                } else {
                    value = 0; //actually Not a number
                }

                prevDay = null;

            }

            return value;
        } catch (Exception e) {
            return 0;
        }

    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }
    /* end of - implemements the methods of the TableModel interface */


    /* implemements the methods of the CommonTable abstract class*/

    public void setSymbol(String symbol) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    public void valueChanged(ListSelectionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        System.out.println("============== slection event in table model ======");
    }
}
