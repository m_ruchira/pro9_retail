package com.isi.csvr.chart;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Jan 11, 2008
 * Time: 2:26:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartSplitComparator implements Comparator, Serializable {
    public int compare(Object o1, Object o2) {
        ChartSplitPoints spr1 = (ChartSplitPoints) o1;
        ChartSplitPoints spr2 = (ChartSplitPoints) o2;

        if (spr1.date > spr2.date) {
            return 1;
        } else if (spr1.date < spr2.date) {
            return -1;
        } else {
            if (spr1.group != spr2.group) {
                return -1;
            } else {
                // this part is merely to be used by comparison points later
                // not for insertion
                /*if (spr1.type>spr2.type){
                    return 1;
                }else if (spr1.type<spr2.type){
                    return -1;
                }else{
                    return 1;
                }*/
                if (spr1.sequence < spr2.sequence)
                    return 1;
                else
                    return -1;
            }
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}