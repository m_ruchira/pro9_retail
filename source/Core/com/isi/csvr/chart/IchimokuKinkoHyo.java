package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorIchimokuKinkoHyoProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 7:48:05 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class IchimokuKinkoHyo extends ChartProperties implements Indicator, Serializable {
    public final static byte TENKAN_SEN = 0;
    public final static byte KIJUN_SEN = 1;
    public final static byte SENKOU_SPAN_A = 2;
    public final static byte SENKOU_SPAN_B = 3;
    public final static byte CHIKOU_SPAN = 4;
    public final static byte CLOSE = 5;
    private static final long serialVersionUID = UID_ICHIMOKU_KINKO_HYO;
    //Fields
    private int tenkanSenPeriods;
    private int kijunSenPeriods;
    private int senkouSpanPeriods;
    private int chikouSpanPeriods;
    private ChartProperties innerSource;
    private byte style;
    private long grpID;
    private Color upCloudColor;
    private Color downCloudColor;
    private String tableColumnHeading;

    public IchimokuKinkoHyo(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_ICHIMOKU_KINKO_HYO") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ICHIMOKU_KINKO_HYO");
        this.tenkanSenPeriods = 9;
        this.kijunSenPeriods = 26;
        this.senkouSpanPeriods = 52;
        this.chikouSpanPeriods = 26;
        style = IchimokuKinkoHyo.TENKAN_SEN;
        grpID = System.currentTimeMillis();
        upCloudColor = new Color(0, 255, 0);
        downCloudColor = Color.MAGENTA;
        isIndicator = true;

        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorIchimokuKinkoHyoProperty idp = (IndicatorIchimokuKinkoHyoProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTenkanSenPeriods(idp.getTenkanSenPeriods());
            this.setKijunSenPeriods(idp.getKijunSenPeriods());
            this.setSenkouSpanPeriods(idp.getSenkouSpanPeriods());
            this.setChikouSpanPeriods(idp.getChikouSpanPeriods());
            // this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, byte style,
                                          byte destIndex) {
      /*  ChartRecord cr, crNew;
        double high, low;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        int timePeriods = 26;
        switch (style) {
            case IchimokuKinkoHyo.TENKAN_SEN:
                timePeriods = tenkanSenPeriods;
                break;
            case IchimokuKinkoHyo.KIJUN_SEN:
                timePeriods = kijunSenPeriods;
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_A:
                timePeriods = Math.max(tenkanSenPeriods, kijunSenPeriods);
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_B:
                timePeriods = senkouSpanPeriods;
                break;
            case IchimokuKinkoHyo.CHIKOU_SPAN:
                timePeriods = chikouSpanPeriods;
                break;
        }

        switch (style) {
            case IchimokuKinkoHyo.TENKAN_SEN:
            case IchimokuKinkoHyo.KIJUN_SEN:
                for (int i = timePeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < timePeriods; j++) {
                        cr = (ChartRecord) al.get(i - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    cr = (ChartRecord) al.get(i);
                    *//*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint!=null){
                        aPoint.setIndicatorValue((low+high)/2f);
                    }*//*
                    cr.setValue(destIndex, (low + high) / 2f);
                }
                for (int i = 0; i < timePeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    cr.setValue(destIndex, Indicator.NULL);
                }
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_A:
                double tenkanSen, kijunSen;
                //TODO: check the logic for this calculation: why using chikouSpanPeriods ??
                for (int i = timePeriods + chikouSpanPeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < tenkanSenPeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    tenkanSen = (low + high) / 2f;

                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < kijunSenPeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    kijunSen = (low + high) / 2f;

                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue((tenkanSen + kijunSen) / 2f);
                    }
                }
                for (int i = 0; i < timePeriods + chikouSpanPeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_B:
                for (int i = timePeriods + chikouSpanPeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < timePeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue((low + high) / 2f);
                    }
                }
                for (int i = 0; i < timePeriods + chikouSpanPeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;
            case IchimokuKinkoHyo.CHIKOU_SPAN:
                for (int i = 0; i < timePeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                for (int i = 0; i <= al.size() - timePeriods; i++) {
                    crNew = (ChartRecord) al.get(i + timePeriods - 1);
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(crNew.Close);
                    }
                }
                break;
            case IchimokuKinkoHyo.CLOSE:
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(cr.Close);
                    }
                }
                break;
        }
        System.out.println("**** Ichimoku Kinko Hyo" + style + " Calc time " + (entryTime - System.currentTimeMillis()));
*/
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.tenkanSenPeriods = 9;
        this.kijunSenPeriods = 26;
        this.senkouSpanPeriods = 52;
        this.chikouSpanPeriods = 26;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.tenkanSenPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TenkanSenPeriods"));
        this.kijunSenPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/KijunSenPeriods"));
        this.senkouSpanPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SenkouSpanPeriods"));
        this.chikouSpanPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/ChikouSpanPeriods"));
        this.style = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/IchimokuStyle"));
        //this.groupID = Long.parseLong(TemplateFactory.loadProperty(xpath, document, preExpression + "/GroupID"));
        this.grpID = Long.parseLong(TemplateFactory.loadProperty(xpath, document, preExpression + "/GrpID"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/UpCloudColor").split(",");
        this.upCloudColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
        saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/DownCloudColor").split(",");
        this.downCloudColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TenkanSenPeriods", Integer.toString(this.tenkanSenPeriods));
        TemplateFactory.saveProperty(chart, document, "KijunSenPeriods", Integer.toString(this.kijunSenPeriods));
        TemplateFactory.saveProperty(chart, document, "SenkouSpanPeriods", Integer.toString(this.senkouSpanPeriods));
        TemplateFactory.saveProperty(chart, document, "ChikouSpanPeriods", Integer.toString(this.chikouSpanPeriods));
        TemplateFactory.saveProperty(chart, document, "IchimokuStyle", Byte.toString(this.style));
        //TemplateFactory.saveProperty(chart, document, "GroupID", Long.toString(this.groupID));
        TemplateFactory.saveProperty(chart, document, "GrpID", Long.toString(this.grpID));
        String strColor = this.upCloudColor.getRed() + "," + this.upCloudColor.getGreen() + "," + this.upCloudColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "UpCloudColor", strColor);
        strColor = this.downCloudColor.getRed() + "," + this.downCloudColor.getGreen() + "," + this.downCloudColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "DownCloudColor", strColor);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof IchimokuKinkoHyo) {
            IchimokuKinkoHyo ich = (IchimokuKinkoHyo) cp;
            this.tenkanSenPeriods = ich.tenkanSenPeriods;
            this.kijunSenPeriods = ich.kijunSenPeriods;
            this.senkouSpanPeriods = ich.senkouSpanPeriods;
            this.chikouSpanPeriods = ich.chikouSpanPeriods;
            this.style = ich.style;
            this.groupID = ich.groupID;
            this.upCloudColor = ich.upCloudColor;
            this.upCloudColor = ich.upCloudColor;
            this.innerSource = ich.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += " (" + tenkanSenPeriods + ", " + senkouSpanPeriods + ", " +
                kijunSenPeriods + ", " + chikouSpanPeriods + ")";
        return Language.getString("IND_ICHIMOKU_KINKO_HYO") + " " + getStyleString() + " " + parent;
    }

    private String getStyleString() {
        switch (style) {
            case IchimokuKinkoHyo.TENKAN_SEN:
                return "[" + Language.getString("TENKAN_SEN") + "]";
            case IchimokuKinkoHyo.KIJUN_SEN:
                return "[" + Language.getString("KIJUN_SEN") + "]";
            case IchimokuKinkoHyo.SENKOU_SPAN_A:
                return "[" + Language.getString("SENKOU_SPAN_A") + "]";
            case IchimokuKinkoHyo.SENKOU_SPAN_B:
                return "[" + Language.getString("SENKOU_SPAN_B") + "]";
            case IchimokuKinkoHyo.CHIKOU_SPAN:
                return "[" + Language.getString("CHIKOU_SPAN") + "]";
            default:
                return "[" + Language.getString("CLOSE") + "]";
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }
    //############################################

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        ChartRecord cr, crNew;
        double high, low;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        int timePeriods = 26;
        switch (style) {
            case IchimokuKinkoHyo.TENKAN_SEN:
                timePeriods = tenkanSenPeriods;
                break;
            case IchimokuKinkoHyo.KIJUN_SEN:
                timePeriods = kijunSenPeriods;
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_A:
                timePeriods = Math.max(tenkanSenPeriods, kijunSenPeriods);
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_B:
                timePeriods = senkouSpanPeriods;
                break;
            case IchimokuKinkoHyo.CHIKOU_SPAN:
                timePeriods = chikouSpanPeriods;
                break;
        }

        switch (style) {
            case IchimokuKinkoHyo.TENKAN_SEN:
            case IchimokuKinkoHyo.KIJUN_SEN:
                for (int i = timePeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < timePeriods; j++) {
                        cr = (ChartRecord) al.get(i - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue((low + high) / 2f);
                    }
                }
                for (int i = 0; i < timePeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_A:
                double tenkanSen, kijunSen;
                //TODO: check the logic for this calculation: why using chikouSpanPeriods ??
                for (int i = timePeriods + chikouSpanPeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < tenkanSenPeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    tenkanSen = (low + high) / 2f;

                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < kijunSenPeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    kijunSen = (low + high) / 2f;

                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue((tenkanSen + kijunSen) / 2f);
                    }
                }
                for (int i = 0; i < timePeriods + chikouSpanPeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;
            case IchimokuKinkoHyo.SENKOU_SPAN_B:
                for (int i = timePeriods + chikouSpanPeriods - 1; i < al.size(); i++) {
                    low = Double.MAX_VALUE;
                    high = Double.MIN_VALUE;
                    for (int j = 0; j < timePeriods; j++) {
                        cr = (ChartRecord) al.get(i - chikouSpanPeriods - j);
                        low = Math.min(low, cr.Low);
                        high = Math.max(high, cr.High);
                    }
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue((low + high) / 2f);
                    }
                }
                for (int i = 0; i < timePeriods + chikouSpanPeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;
            case IchimokuKinkoHyo.CHIKOU_SPAN:
                for (int i = 0; i < timePeriods - 1; i++) {
                    if (i >= al.size()) break;
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                for (int i = 0; i <= al.size() - timePeriods; i++) {
                    crNew = (ChartRecord) al.get(i + timePeriods - 1);
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(crNew.Close);
                    }
                }
                break;
            case IchimokuKinkoHyo.CLOSE:
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(cr.Close);
                    }
                }
                break;
        }
        //System.out.println("**** Ichimoku Kinko Hyo" + style + " Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_ICHIMOKU_KINKO_HYO");
    }

    //tenkanSenPeriods
    public int getTenkanSenPeriods() {
        return tenkanSenPeriods;
    }

    public void setTenkanSenPeriods(int tp) {
        tenkanSenPeriods = tp;
    }

    //chikouSpanPeriods
    public int getChikouSpanPeriods() {
        return chikouSpanPeriods;
    }

    public void setChikouSpanPeriods(int chikouSpanPeriods) {
        this.chikouSpanPeriods = chikouSpanPeriods;
    }

    //kijunSenPeriods
    public int getKijunSenPeriods() {
        return kijunSenPeriods;
    }

    public void setKijunSenPeriods(int kijunSenPeriods) {
        this.kijunSenPeriods = kijunSenPeriods;
    }

    //senkouSpanPeriods
    public int getSenkouSpanPeriods() {
        return senkouSpanPeriods;
    }

    public void setSenkouSpanPeriods(int senkouSpanPeriods) {
        this.senkouSpanPeriods = senkouSpanPeriods;
    }

    public byte getStyle() {
        return style;
    }

    public void setStyle(byte style) {
        this.style = style;
    }

    public long getGrpID() {
        return grpID;
    }

    public Color getUpCloudColor() {
        return upCloudColor;
    }

    public void setUpCloudColor(Color upCloudColor) {
        this.upCloudColor = upCloudColor;
    }

    public Color getDownCloudColor() {
        return downCloudColor;
    }

    public void setDownCloudColor(Color downCloudColor) {
        this.downCloudColor = downCloudColor;
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
