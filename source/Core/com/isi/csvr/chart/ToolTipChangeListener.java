package com.isi.csvr.chart;

import java.awt.*;
import java.util.ArrayList;

// Copyright (c) 2000 Home


public interface ToolTipChangeListener {

    /**
     * Event
     */
    public void ToolTipDataChanged(boolean bTipActive, byte bytMode, int dataRows,
                                   Color aColor, String sSymbol, String sDate, String sOpen, String sHigh,
                                   String sLow, String sClose, String sVolume, String sTurnOver, ArrayList<ToolTipDataRow> toolTipRows, boolean isMove, String aoTitle);
}