package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorRelativeMomentumIndexProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 10:14:03 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class RelativeMomentumIndex extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_RMI;
    //Fields
    private int timePeriods;
    private int momentumParam;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public RelativeMomentumIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_RMI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_RMI");
        this.timePeriods = 20;
        this.momentumParam = 5;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorRelativeMomentumIndexProperty idp = (IndicatorRelativeMomentumIndexProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTimePeriods(idp.getTimePeriods());
            this.setMomentumParam(idp.getMomentumParam());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, int momentomPeriod, byte step1, byte step2, byte step3, byte step4, byte destIndex) {
        byte stepUpChange = step1;
        byte stepDownChange = step2;
        byte stepUpChangeSmoothing = step3;
        byte stepDownChangeSmoothing = step4;
        ChartRecord cr, crPre4;
        int startIndex = bIndex + momentomPeriod + t_im_ePeriods - 1;
        if (al.size() < startIndex || t_im_ePeriods == 0 || momentomPeriod == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = bIndex + momentomPeriod; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crPre4 = (ChartRecord) al.get(i - momentomPeriod);
            if (cr.Close > crPre4.Close) {
                cr.setValue(stepUpChange, cr.Close - crPre4.Close);
                cr.setValue(stepDownChange, 0);
            } else if (cr.Close < crPre4.Close) {
                cr.setValue(stepUpChange, 0);
                cr.setValue(stepDownChange, crPre4.Close - cr.Close);
            } else {
                cr.setValue(stepUpChange, 0);
                cr.setValue(stepDownChange, 0);
            }
        }


        WildersSmoothing.getWildersSmoothing(al, bIndex + momentomPeriod, t_im_ePeriods, stepUpChange, stepUpChangeSmoothing);
        WildersSmoothing.getWildersSmoothing(al, bIndex + momentomPeriod, t_im_ePeriods, stepDownChange, stepDownChangeSmoothing);
        for (int i = 0; i < startIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = startIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, cr.getValue(stepUpChangeSmoothing) * 100 / (cr.getValue(stepUpChangeSmoothing) + cr.getValue(stepDownChangeSmoothing)));
        }
    }

    public static int getAuxStepCount() {
        return 4;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 20;
        this.momentumParam = 5;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.momentumParam = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/MomentumParam"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "MomentumParam", Integer.toString(this.momentumParam));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof RelativeMomentumIndex) {
            RelativeMomentumIndex rmi = (RelativeMomentumIndex) cp;
            this.timePeriods = rmi.timePeriods;
            this.momentumParam = rmi.momentumParam;
            this.innerSource = rmi.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_RMI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    /*
    I have used MFI = posFlow*100f/(posFlow+negFlow)
    which is correct and the best equation - if you follow the steps in metastock
    to arrive at this by first calculating "Money Ratio" it can lead to a division by zero
    when negative flow is zero - so it should not be used.
    */
    // this includes 4 intermediate steps: UpSum, DownSum, WSUpSum and WSDownSum
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal;

        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods + momentumParam - 1;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        ChartRecord cr, crOld;
        //setting step size 4
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(4);
        }
        // steps involved
        byte stepUpSum = ChartRecord.STEP_1;
        byte stepDownSum = ChartRecord.STEP_2;
        byte stepWSUpSum = ChartRecord.STEP_3;
        byte stepWSDownSum = ChartRecord.STEP_4;

        for (int i = momentumParam; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - momentumParam);
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(getOHLCPriority()) - crOld.getValue(getOHLCPriority());
            if (currVal >= 0) {
                cr.setStepValue(stepUpSum, currVal);
                cr.setStepValue(stepDownSum, 0d);
            } else {
                cr.setStepValue(stepUpSum, 0d);
                cr.setStepValue(stepDownSum, -currVal);
            }
        }

        WildersSmoothing.getWildersSmoothing(al, momentumParam, timePeriods, stepUpSum, stepWSUpSum);
        WildersSmoothing.getWildersSmoothing(al, momentumParam, timePeriods, stepDownSum, stepWSDownSum);

        double upSum;
        double downSum;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            upSum = cr.getStepValue(stepWSUpSum);
            downSum = cr.getStepValue(stepWSDownSum);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(upSum + downSum == 0 ? 0 : 100f * upSum / (upSum + downSum));  // check for division by 0
            }
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        //System.out.println("**** Relative Momentum Index Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_RMI");
    }

    public int getMomentumParam() {
        return momentumParam;
    }

    public void setMomentumParam(int momentumParam) {
        this.momentumParam = momentumParam;
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
