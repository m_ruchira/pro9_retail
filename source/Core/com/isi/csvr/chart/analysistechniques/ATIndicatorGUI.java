package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.chart.ChartFrame;
import com.isi.csvr.chart.IndicatorBuilder;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.table.Table;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 11, 2008
 * Time: 12:53:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class ATIndicatorGUI extends JPanel implements MouseListener {
    //object type


    int ATTYPE = ChartConstants.TYPE_INDICATOR;
    ArrayList<AnalysTechBase> aTDataStoreLocal = new ArrayList<AnalysTechBase>();
    private Table indTable;
    private AnalysisModel indModel;
    private String encryptedFilePath = ChartConstants.INDICATOR_ENCRYPTED_FILE; //should be according to type

    public ATIndicatorGUI(String encryptedFilePath, int aTType) {
        this.ATTYPE = aTType;

//        aTDataStoreLocal.

        //get the array list
        IATXmlSerializeService iATxmlService = new IATXmlSerializeService();
        try {
//            aTDataStoreLocal   = iATxmlService.deserializeAnalysTechRecords(ChartConstants.INDICATOR_ENCRYPTED_FILE);
            aTDataStoreLocal = iATxmlService.deserializeAnalysTechRecords(encryptedFilePath, aTType);
            Collections.sort(aTDataStoreLocal, new AnalysisTechComparator(aTType));

        } catch (Exception e) {
            e.printStackTrace();
        }

        indModel = new AnalysisModel(aTDataStoreLocal, ATTYPE);
        //view setting
        ViewSetting analysSetting = null;
        try {
            analysSetting = ViewSettingsManager.getSummaryView("ANALYS_TECH_COLS");
            if (analysSetting == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            // return; //ToDo - consier the return value
        }

        indModel.setViewSettings(analysSetting);
        indTable = new Table();
//        indTable.setSortingEnabled();
        indTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        indTable.setModel(indModel);
        indTable.getPopup().setTMTavailable(false);

        indModel.setInsertAnalysTable(indTable);
//        indTable.getPopup().setPrintTarget(indTable.getTable(), PrintManager.PRINT_JTABLE);

//        indTable.getPopup().enableUnsort(false);
//        indTable.getPopup().enableUnsort(false);
        indTable.getModel().updateGUI();
//        GUISettings.applyOrientation(indTable.getPopup());
        indTable.getTable().addMouseListener(this);
        //check whether these are need
        GUISettings.setColumnSettings(analysSetting, TWColumnSettings.getItem("ANALYS_TECH_COLS"));
        analysSetting.setTableNumber(1);
        analysSetting.setParent(this);
        this.setLayout(new BorderLayout());
        this.add(indTable, BorderLayout.CENTER);

    }


    public void mouseClicked(MouseEvent e) {


        if ((e.getModifiers() & e.BUTTON1_MASK) == e.BUTTON1_MASK) { //left click) {

            if (e.getClickCount() == 1) {

                int selRow = indTable.getTable().getSelectedRow();
                int selCol = indTable.getTable().getSelectedColumn();
                if (selCol == 3) { //Faviorite column
                    JCheckBox checkBoxRenderer;
                    //get checkBox renderer

                    checkBoxRenderer = (JCheckBox) indTable.getTable().getCellRenderer(selRow, selCol).
                            getTableCellRendererComponent(indTable.getTable(), indTable.getTable().getModel().getValueAt(selRow, selCol), false, false, selRow, selCol);
                    boolean preState = checkBoxRenderer.isSelected();
//                    checkBoxRenderer .setSelected(!preState);
                    AnalysTechBase atBase = (AnalysTechBase) aTDataStoreLocal.get(selCol);
//                    atBase.setFaviourite(!preState);
                    //  atBase.setFaviourite(false);

                } else if (selCol == 0) { //definiton
                    String section = "Averages.htm#TEMA";
                    if (selRow != -1) {
                        section = (String) indTable.getTable().getModel().getValueAt(selRow, -1); //id
                    }
                    NativeMethods.showInsertAnalysisHelp(section);
                } else if (selCol == 1) {

                    String syntax = "";
                    if (selRow != -1) {
                        syntax = (String) indTable.getTable().getModel().getValueAt(selRow, -2);
                    }

                    /*******************showm grammer in IndicatorBuilder****************************************/
                    IndicatorBuilder ciw = IndicatorBuilder.getInstance();
                    ciw.getIndicatorBuilderPanel().setIndicatorName("New Custom Indicator");
                    ciw.getIndicatorBuilderPanel().setEditingIndictor("New Custom Indicator");
                    ciw.getIndicatorBuilderPanel().setIndicatorFormulaText(syntax);
                    ciw.getIndicatorBuilderPanel().markSyntax();
                    ciw.showWindow();
                }


            } else if (e.getClickCount() > 1) {

                insertAnalysisTechniqueToGraph();

            }
        }

    }

    public void insertAnalysisTechniqueToGraph() {
        int selRow = indTable.getTable().getSelectedRow();
        if (selRow != -1) {
            String atName = (String) indTable.getTable().getModel().getValueAt(selRow, 2); //language.getstring Arabic or english
            switch (ATTYPE) {
                case ChartConstants.TYPE_INDICATOR:
                    ChartFrame.getSharedInstance().getActiveGraph().indicatorClicked(atName);
                    break;
                case ChartConstants.TYPE_PATTERN:
                    ChartFrame.getSharedInstance().getActiveGraph().insertPattern(atName);
                    break;
                case ChartConstants.TYPE_STRATEGY:
                    ChartFrame.getSharedInstance().getActiveGraph().insertStrategy(atName);
            }
        }
    }

    public void mousePressed(MouseEvent e) {

    }


    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public Table getIndTable() {
        return indTable;
    }

    public void setIndTable(Table indTable) {
        this.indTable = indTable;
    }

    public ArrayList<AnalysTechBase> getATDataStoreLocal() {
        return aTDataStoreLocal;
    }

    public void setATDataStoreLocal(ArrayList<AnalysTechBase> aTDataStoreLocal) {
        this.aTDataStoreLocal = aTDataStoreLocal;

    }

    public int getATTYPE() {
        return ATTYPE;
    }

    public void setATTYPE(int ATTYPE) {
        this.ATTYPE = ATTYPE;
    }


    public ArrayList<String> getFavouriteList() {
        ArrayList<String> faviouriteList = new ArrayList<String>();
        for (AnalysTechBase aTB : aTDataStoreLocal) {
            if (aTB.isFaviourite()) {
                if (ATTYPE == ChartConstants.TYPE_INDICATOR) {

                    faviouriteList.add(Language.getString(aTB.getName()));
                } else {
                    faviouriteList.add(aTB.getName());
                }
            }
        }

        return faviouriteList;
    }


//     private JPanel createStreategyPanel() {

    //        IATXmlSerializeService iATxmlService = new IATXmlSerializeService();
    //        //get the array list

    //        strategyDataStore = new ArrayList<AnalysTechBase>();

    //        //get indicator name list with ID :
//        try {
//            strategyDataStore = iATxmlService.deserializeAnalysTechRecords(ChartConstants.STRATEGY_ENCRYPTED_FILE);
//        } catch (Exception e) {
//            e.printStackTrace();
//
//        }
//        AnalysisModel strategyModel = new AnalysisModel(strategyDataStore);
//        //view setting
//        ViewSetting StrategySetting = null;
//        try {
//            // Read the default view in the view settings file
//            // get the default view
//            StrategySetting = ViewSettingsManager.getSummaryView("ANALYS_TECH_COLS");
//            if (StrategySetting == null)
//                throw (new Exception("View not found"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            // return; //ToDo - consier the return value
//        }
//
//
//        strategyModel.setViewSettings(StrategySetting);
//        strategyTable = new Table();
//        strategyTable.setSortingEnabled();
//        strategyTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
//        strategyTable.setModel(strategyModel);
//        strategyTable.getPopup().setTMTavailable(false);
//
//        strategyModel.setInsertAnalysTable(strategyTable);
//        strategyTable.getPopup().setPrintTarget(strategyTable.getTable(), PrintManager.PRINT_JTABLE);
//
////        strategyTable.getPopup().enableUnsort(false);
//        strategyTable.getModel().updateGUI();
////        GUISettings.applyOrientation(strategyTable.getPopup());
//        strategyTable.getTable().addMouseListener(this);
//
//
//        //check whether these are need
//        GUISettings.setColumnSettings(StrategySetting, TWColumnSettings.getItem("ANALYS_TECH_COLS"));
//        StrategySetting.setTableNumber(1);
//        StrategySetting.setParent(this);
//
//        strategyPanel = new JPanel();
//        strategyPanel.setLayout(new BorderLayout());
//        strategyPanel.add(strategyTable, BorderLayout.CENTER);
//        return strategyPanel;
//    }
}
