package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.chart.ChartFrame;
import com.isi.csvr.chart.IndicatorBuilder;
import com.isi.csvr.chart.IndicatorDetailStore;
import com.isi.csvr.chart.options.CustomDataItem;
import com.isi.csvr.chart.options.XMLSerilaizationService;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.NonResizeable;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 4, 2008
 * Time: 2:11:41 PM
 * To change this template use File | Settings | File Templates.
 */

public class AnalysisTechWindow extends InternalFrame implements ActionListener, MouseListener, NonResizeable {// Themeable, WindowListener {

    private static AnalysisTechWindow analysisWindow = null;
    private final int ANALYSIS_WINDOW_WIDTH = 385;
    private final int ANALYSIS_WINDOW_HEIGHT = 350;
    private TWTabbedPane tbpAnaTechs;
    private JPanel finalPanel;

    //main panels
    private JPanel mediatePanel;
    private JPanel defPanel;
    private JPanel bottomPanel;
    //defintion panel
    private TWButton btnDefinition = new TWButton(Language.getString("DEFINITION"));
    private TWButton btnEditLang = new TWButton(Language.getString("EDIT_CUSTOM_IND_BUILDER"));
    //bottom panel
    private TWButton btnOk = new TWButton(Language.getString("OK"));
    private TWButton btnCanel = new TWButton(Language.getString("CANCEL"));
    private TWButton btnHelp = new TWButton(Language.getString("HELP"));

    public AnalysisTechWindow() throws HeadlessException {
        super();
        createUI();
    }

    public static AnalysisTechWindow getSharedInstance() {

        if (analysisWindow != null) {
            analysisWindow.dispose();
            analysisWindow = null;
        }
        analysisWindow = new AnalysisTechWindow();

        ChartFrame.getSharedInstance().getDesktop().setLayer(analysisWindow, GUISettings.TOP_LAYER + 5);
        return analysisWindow;
    }

    private void createUI() {

        tbpAnaTechs = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);
        finalPanel = new JPanel();
        mediatePanel = new JPanel();
        mediatePanel.setLayout(new FlexGridLayout(new String[]{"0", "100%", "0"}, new String[]{"100%", "5"}, 0, 0));//width ,height
//        mediatePanel.setLayout(new BorderLayout());//width ,height
        finalPanel.setLayout(new FlexGridLayout(new String[]{"2", "100%", "1"}, new String[]{"2", "100%", "5", "22", "5"}, 0, 0));//width ,height
//        finalPanel.setLayout(new BorderLayout());//width ,height
        this.setLayout(new BorderLayout());
        this.add(finalPanel, BorderLayout.CENTER);
//        finalPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
//        finalPanel.setBorder(BorderFactory.createEtchedBorder());
//        finalPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        mediatePanel.add(new JLabel(""));
        mediatePanel.add(tbpAnaTechs);
        mediatePanel.add(new JLabel(""));
//        mediatePanel.add(tbpAnaTechs,BorderLayout.NORTH);
//        mediatePanel.setPreferredSize(new Dimension(this.getPreferredSize().width,this.getPreferredSize().height - 25));
        tbpAnaTechs.addTab(Language.getString("INDICATORS"), new ATIndicatorGUI(ChartConstants.INDICATOR_ENCRYPTED_FILE, ChartConstants.TYPE_INDICATOR));
        if (ChartInterface.isSystemPatternsEnabled()) {
            tbpAnaTechs.addTab(Language.getString("PATTERNS"), new ATIndicatorGUI(ChartConstants.PATTERN_ENCRYPTED_FILE, ChartConstants.TYPE_PATTERN));
        }
        if (ChartInterface.isSystemStrategiesEnabled()) {
            tbpAnaTechs.addTab(Language.getString("STRATEGIES"), new ATIndicatorGUI(ChartConstants.STRATEGY_ENCRYPTED_FILE, ChartConstants.TYPE_STRATEGY));
        }
        tbpAnaTechs.addTab(Language.getString("CUSTOM_INDICATORS"), new CustomIndUI(ChartConstants.CUST_IND_ENCRYPTED_FILE, ChartConstants.TYPE_CUSTOM));
        tbpAnaTechs.setSelectedIndex(0);

        //buttons
        mediatePanel.add(new JLabel());
        mediatePanel.add(new JLabel(""));
        mediatePanel.add(new JLabel(""));
//        mediatePanel.add(new JLabel(""));
//        mediatePanel.add(createDefinitionpanel());
//        mediatePanel.add(new JLabel(""));
//        mediatePanel.add(createDefinitionpanel(),BorderLayout.SOUTH);
//        mediatePanel.add(new JLabel());
//        mediatePanel.add(new JLabel(""));
//        mediatePanel.add(new JLabel(""));
//        mediatePanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));
//        mediatePanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));
//        mediatePanel.setBorder(BorderFactory.createCompoundBorder());
        mediatePanel.setBorder(BorderFactory.createEtchedBorder());

        finalPanel.add(new JLabel(""));
        finalPanel.add(new JLabel(""));
        finalPanel.add(new JLabel(""));
        finalPanel.add(new JLabel(""));
        finalPanel.add(mediatePanel);
        finalPanel.add(new JLabel(""));
//        finalPanel.add(mediatePanel,BorderLayout.NORTH);
        finalPanel.add(new JLabel());
        finalPanel.add(new JLabel());
        finalPanel.add(new JLabel());

//        finalPanel.add(createBottomPanel(),BorderLayout.SOUTH);
        finalPanel.add(new JLabel());
        finalPanel.add(createBottomPanel());
        finalPanel.add(new JLabel());
        finalPanel.add(new JLabel());
        finalPanel.add(new JLabel());
        finalPanel.add(new JLabel());

        this.setResizable(true);
        this.setMinimumSize(new Dimension(200, 300));
//        this.setResizable(false);
        this.setSize(ANALYSIS_WINDOW_WIDTH, ANALYSIS_WINDOW_HEIGHT);
        this.setLocationRelativeTo(ChartFrame.getSharedInstance());
        this.setTitle(Language.getString("INSERT_ANALYSIS_TECHNIQUE"));

        //intanal frame -----------------------------------
        this.setClosable(true);
        ChartFrame.getSharedInstance().getDesktop().add(this);
        Theme.registerComponent(this);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        addActionListeners();

    }


    private JPanel createDefinitionpanel() {

        defPanel = new JPanel();
        defPanel.setLayout(new FlexGridLayout(new String[]{"3", "90", "3", "210", "100%"}, new String[]{"20"}, 0, 0));//width ,height
        btnDefinition.addActionListener(this);
        btnEditLang.addActionListener(this);
        defPanel.add(new JLabel());
        defPanel.add(btnDefinition);
        defPanel.add(new JLabel());
        defPanel.add(btnEditLang);
        defPanel.add(new JLabel());

        return defPanel;
    }

    private JPanel createBottomPanel() {
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlexGridLayout(new String[]{"100%", "80", "3", "80", "3", "80", "3"}, new String[]{"20"}, 0, 0));
        bottomPanel.add(new JLabel());
        btnOk.addActionListener(this);
        btnCanel.addActionListener(this);
        btnHelp.addActionListener(this);
        bottomPanel.add(btnOk);
        bottomPanel.add(new JLabel());
        bottomPanel.add(btnCanel);
        bottomPanel.add(new JLabel());
        bottomPanel.add(btnHelp);
        bottomPanel.add(new JLabel());

        return bottomPanel;
    }

    private void addActionListeners() {

    }


    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnEditLang) {
            /**************get the selected Analysis technique tab*********/
            Table selecTable = getselectedAnalysisTechTable();
            /*********************get syntax (gramer) relevent to analysis technique***************************************/
            int selectedRow = selecTable.getTable().getSelectedRow();
            String syntax = "";
            if (selectedRow != -1) {
                syntax = (String) selecTable.getTable().getModel().getValueAt(selectedRow, -2);
            }

            /*******************showm grammer in IndicatorBuilder****************************************/
            IndicatorBuilder ciw = IndicatorBuilder.getInstance();
            ciw.getIndicatorBuilderPanel().setIndicatorName("New Custom Indicator");
            ciw.getIndicatorBuilderPanel().setEditingIndictor("New Custom Indicator");
            ciw.getIndicatorBuilderPanel().setIndicatorFormulaText(syntax);
            ciw.getIndicatorBuilderPanel().markSyntax();
            ciw.showWindow();
        } else if (e.getSource() == btnDefinition) {

            /**************get the selected Analysis technique tab*********/
            Table selecTable = getselectedAnalysisTechTable();

            int selectedRow = selecTable.getTable().getSelectedRow();
            String section = "Averages.htm#TEMA";
            if (selectedRow != -1) {
                section = (String) selecTable.getTable().getModel().getValueAt(selectedRow, -1); //id
            }
            NativeMethods.showInsertAnalysisHelp(section);
        } else if (e.getSource() == btnOk) {
            //todo //serialize all the data

            IATXmlSerializeService iATxmlService = new IATXmlSerializeService();
            XMLSerilaizationService service = new XMLSerilaizationService();

            try {
                ATIndicatorGUI atIndGui = (ATIndicatorGUI) tbpAnaTechs.getComponentAt(0);
                //ChartFrame.getSharedInstance().setAlFavouiteIndicators(atIndGui.getFavouriteList());
                ArrayList<AnalysTechBase> indDataStore = atIndGui.getATDataStoreLocal();

                iATxmlService.serializeAnalysisTechRecords(indDataStore, ChartConstants.INDICATOR_ENCRYPTED_FILE, ChartConstants.ROOT_ELEMENT_INDICATOR);
                service.serializeCustomDataItems(getFavouriteList(indDataStore), ChartConstants.INDICATOR_FAVOURITE_FILE);

                ATIndicatorGUI atpatGui = (ATIndicatorGUI) tbpAnaTechs.getComponentAt(1);
                ArrayList<AnalysTechBase> patDataStore = atpatGui.getATDataStoreLocal();
                //ChartFrame.getSharedInstance().setAlFaviouritePatterns(atpatGui.getFavouriteList());
                iATxmlService.serializeAnalysisTechRecords(patDataStore, ChartConstants.PATTERN_ENCRYPTED_FILE, ChartConstants.ROOT_ELEMENT_PATTERN);
                service.serializeCustomDataItems(getFavouriteList(patDataStore), ChartConstants.PATTERN_FAVOURITE_FILE);

                ATIndicatorGUI atstratGui = (ATIndicatorGUI) tbpAnaTechs.getComponentAt(2);
                ArrayList<AnalysTechBase> stratDataStore = atstratGui.getATDataStoreLocal();
                //ChartFrame.getSharedInstance().setAlFavouriteStrategies(atstratGui.getFavouriteList());
                iATxmlService.serializeAnalysisTechRecords(stratDataStore, ChartConstants.STRATEGY_ENCRYPTED_FILE, ChartConstants.ROOT_ELEMENT_STRATEGY);
                service.serializeCustomDataItems(getFavouriteList(stratDataStore), ChartConstants.STRATEGY_FAVOURITE_FILE);

                //todo  custom indiator faviourtes and custom indicator source code
                CustomIndUI atCustIndGui = (CustomIndUI) tbpAnaTechs.getComponentAt(3);
                ArrayList<AnalysTechBase> custIndDataStore = atCustIndGui.getATDataStoreLocal();
                //ChartFrame.getSharedInstance().setAlFaviouriteCustIndicators(atCustIndGui.getFavouriteList());
                iATxmlService.serializeAnalysisTechRecords(custIndDataStore, ChartConstants.CUST_IND_ENCRYPTED_FILE, ChartConstants.ROOT_ELEMENT_CUSTOM_INDICATOR);
                service.serializeCustomDataItems(getFavouriteList(custIndDataStore), ChartConstants.CI_FAVOURITE_FILE);

                IndicatorDetailStore.alFaviouritePatterns = atpatGui.getFavouriteList();
                ChartFrame.getSharedInstance().refreshPatternsFavouritesMenu();

                IndicatorDetailStore.alFavouriteStrategies = atstratGui.getFavouriteList();
                ChartFrame.getSharedInstance().refreshStratergiesFavouriteMenu();

                IndicatorDetailStore.alFavouiteIndicators = atstratGui.getFavouriteList();
                ChartFrame.getSharedInstance().refreshIndicatorsFavouriteMenu();

                IndicatorDetailStore.alFaviouriteCustIndicators = atCustIndGui.getFavouriteList();
                ChartFrame.getSharedInstance().refreshIndicatorsFavouriteMenu();


            } catch (Exception ex) {
                ex.printStackTrace();
            }
            this.dispose();


        } else if (e.getSource() == btnCanel) {
            this.dispose();
        } else if (e.getSource() == btnHelp) {
            String section = "insert_analysis_technique.htm";
            NativeMethods.showInsertAnalysisHelp(section);
        }
    }

    private Table getselectedAnalysisTechTable() {
        int seleTabIndex = tbpAnaTechs.getSelectedIndex();
        Table selecTable;
        if (seleTabIndex == 3) {
            CustomIndUI atCustInd = (CustomIndUI) tbpAnaTechs.getComponentAt(3);
            selecTable = atCustInd.getIndTable();
        } else {
            ATIndicatorGUI atIndGui = (ATIndicatorGUI) tbpAnaTechs.getComponentAt(seleTabIndex);
            selecTable = atIndGui.getIndTable();

        }
        return selecTable;
    }

    public void mouseClicked(MouseEvent e) {


    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameClosing(InternalFrameEvent e) {

    }

    public void doDefaultCloseAction() {
        this.dispose();
    }

    public ArrayList<CustomDataItem> getFavouriteList(ArrayList<AnalysTechBase> store) {
        ArrayList<CustomDataItem> items = new ArrayList<CustomDataItem>();

        for (int i = 0; i < store.size(); i++) {
            AnalysTechBase analysTechBase = store.get(i);
            if (analysTechBase.isFaviourite()) {
                items.add(new CustomDataItem(analysTechBase.getName(), ""));
            }
        }

        return items;
    }
}





