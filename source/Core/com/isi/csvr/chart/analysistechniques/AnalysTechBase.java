package com.isi.csvr.chart.analysistechniques;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 9, 2008
 * Time: 10:32:35 AM
 * To change this template use File | Settings | File Templates.
 */

public class AnalysTechBase {

    private String sSyntax = "";
    private String sID = "";
    private String sName = "";
    private String sType = "";
    private boolean faviourite = true;


    public AnalysTechBase(String ID, String name, String type, String syntax, boolean bFaviourite) {
        this.sSyntax = syntax;
        this.sID = ID;
        this.sName = name;
        this.sType = type;
        this.faviourite = bFaviourite;
    }


    public String getID() {
        return sID;
    }

    public void setID(String sID) {
        this.sID = sID;
    }

    public String getSyntax() {

        return sSyntax;
    }

    public void setSyntax(String sName) {
        this.sSyntax = sName;
    }

    public String getName() {
        return sName;
    }

    public void setSName(String sName) {
        this.sName = sName;
    }

    public String getType() {
        return sType;
    }

    public void setSType(String sType) {
        this.sType = sType;
    }

    public boolean isFaviourite() {
        return faviourite;
    }

    public void setFaviourite(boolean faviourite) {
        this.faviourite = faviourite;
    }
}
