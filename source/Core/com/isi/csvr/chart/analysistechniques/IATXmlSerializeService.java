package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.chart.options.CustomDataItem;
import com.isi.csvr.chart.options.XMLSerilaizationService;
import com.isi.csvr.shared.Language;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 9, 2008
 * Time: 10:30:44 AM
 * To change this template use File | Settings | File Templates.
 */

public class IATXmlSerializeService {

    public static final String ANALYSISTECHNIQUE = "ANALYSISTECHNIQUE";
    public static final String ID = "ID";
    public static final String SYNTAX = "SYNTAX";
    public static final String NAME = "NAME";
    public static final String TYPE = "TYPE";
    public static final String FAVORITE = "FAVIOURITE";


    public IATXmlSerializeService() {
    }

    public static void main(String[] args) {
       /* IATXmlSerializeService xmltest = new IATXmlSerializeService();
        AnaTechDefaultEntries sF = new AnaTechDefaultEntries();
        ArrayList<AnalysTechBase> patArrlocal = sF.patArr;
        xmltest.serializeAnalysisTechRecords(patArrlocal, ChartConstants.STRATEGY_ENCRYPTED_FILE, ChartConstants.ROOT_ELEMENT_STRATEGY);
        ArrayList<AnalysTechBase> patArrOUtPut = xmltest.deserializeAnalysTechRecords(ChartConstants.STRATEGY_ENCRYPTED_FILE);


        for (AnalysTechBase item : patArrOUtPut) {

            System.out.println("ID: " + item.getID());
            System.out.println("name: " + item.getName());
            System.out.println("Type: " + item.getType());
            System.out.println("Syntax: " + item.getSyntax());
            System.out.println("-------------------------------------------------");
        }*/


    }

    public synchronized void serializeAnalysisTechRecords(ArrayList<AnalysTechBase> abDataItems, String filePath, String RootEliment) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(RootEliment);
            doc.appendChild(root);
            //add general settings to xml file
            for (AnalysTechBase item : abDataItems) {
                Element eleAnaTechnique = doc.createElement(ANALYSISTECHNIQUE);
                root.appendChild(eleAnaTechnique);

                //ID
                Element eleID = doc.createElement(ID);
                Text textID = doc.createTextNode(item.getID());
                eleAnaTechnique.appendChild(eleID);
                eleID.appendChild(textID);

                //NAME
                Element eleName = doc.createElement(NAME);
                Text textName = doc.createTextNode(item.getName());
                eleAnaTechnique.appendChild(eleName);
                eleName.appendChild(textName);

                //SYNTAX
                Element eleSyntax = doc.createElement(SYNTAX);
                String strSyntax = String.valueOf(item.getSyntax());
                Text textSyntax = doc.createTextNode(strSyntax);
                eleAnaTechnique.appendChild(eleSyntax);
                eleSyntax.appendChild(textSyntax);

                //TYPE
                Element eleType = doc.createElement(TYPE);
                Text textType = doc.createTextNode(item.getType());
                eleAnaTechnique.appendChild(eleType);
                eleType.appendChild(textType);

                //isFaviourite
                Element eleFaviourite = doc.createElement(FAVORITE);
                Text textFaviourite = doc.createTextNode(Boolean.toString(item.isFaviourite()));
                eleAnaTechnique.appendChild(eleFaviourite);
                eleFaviourite.appendChild(textFaviourite);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

//            FileWriter fw = new FileWriter(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);
            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            // Encrypt
//            FileInputStream inStream = new FileInputStream((ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH));
//            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
//            encrypter.constructKeys(ChartConstants.ENCRYPT_FILE_PATH);
//
//            encrypter.encryptFile(inStream, new FileOutputStream(filePath));
//            inStream.close();
            //deleteTempFile();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== XMLSerializationService class-serializeCustomDataItemObjects() ======");
        }

    }

    public synchronized ArrayList<AnalysTechBase> deserializeAnalysTechRecords(String filePath, int type) {

        ArrayList<AnalysTechBase> dataItems = new ArrayList<AnalysTechBase>();

        File file = new File(filePath);
        if (!file.exists()) {
            return dataItems;
        }
        try {
            // Decrypt
//            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
//            encrypter.constructKeys(ChartConstants.ENCRYPT_FILE_PATH);
//            FileOutputStream stream = new FileOutputStream(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);
//            encrypter.decryptFile(new FileInputStream(filePath), stream);
//
//            stream.close();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

//            Document dom = docBuilder.parse(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);
            Document dom = docBuilder.parse(file.toURI().toString());

            ArrayList<CustomDataItem> favItems = new XMLSerilaizationService().deserializeCustomDataItems(getFavFilePath(type));


            NodeList list = dom.getElementsByTagName(ANALYSISTECHNIQUE);
            int count = list.getLength();
            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i); //analys tech
                NodeList nlID = elem.getElementsByTagName(ID);
                NodeList nlName = elem.getElementsByTagName(NAME);
                NodeList nlSyntax = elem.getElementsByTagName(SYNTAX);
                NodeList nlType = elem.getElementsByTagName(TYPE);
                NodeList nlFaviourite = elem.getElementsByTagName(FAVORITE);

                String sID = nlID.item(0).getTextContent(); //there are only 1 name and value element within the CustomDataItem
                String sName = nlName.item(0).getTextContent();
                String sSyntax = nlSyntax.item(0).getTextContent();
                String sType = nlType.item(0).getTextContent();
                String sFaviourite = nlFaviourite.item(0).getTextContent();
                sSyntax = sSyntax.replaceAll("COMPANY_NAME", Language.getString("CHART_COMPANY"));

                //boolean bFaviourite = Boolean.parseBoolean(sFaviourite);
                boolean bFaviourite = false;
                for (int j = 0; j < favItems.size(); j++) {

                    if (sName.equals(favItems.get(j).getDataName())) {
                        bFaviourite = true;
                        break;
                    }
                }

                AnalysTechBase item = new AnalysTechBase(sID, sName, sType, sSyntax, bFaviourite);
                dataItems.add(item);

            }
            deleteTempFile();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("=== XMLSerializationService class-deserializeAnalysTechRecords() ======");
        }

        return dataItems;
    }

    private synchronized void deleteTempFile() {

        File f1 = new File(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);
        File f2 = new File(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);

        try {
            if (f1.exists()) {
                f1.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("failure deletion of tempory file..");
        }

        try {
            if (f2.exists()) {
                f2.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("failure deletion of tempory file in XMLSerialization service class-deleteTempFile()..");
        }
    }

    public ArrayList<String> getFaviouriteList(int Type) {


        String filePath = getFilePathByType(Type);
        ArrayList<String> faviouriteList = new ArrayList<String>();
        File file = new File(filePath);
        if (!file.exists()) {
            return faviouriteList;
        }
        try {
            // Decrypt
//            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
//            encrypter.constructKeys(ChartConstants.ENCRYPT_FILE_PATH);
//            FileOutputStream stream = new FileOutputStream(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);
//            encrypter.decryptFile(new FileInputStream(filePath), stream);
//
//            stream.close();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

//            Document dom = docBuilder.parse(ChartConstants.CHART_ANALYSIS_INDICATOR_TEMP_PATH);

            Document dom = docBuilder.parse(file.toURI().toString());

            NodeList list = dom.getElementsByTagName(ANALYSISTECHNIQUE);
            int count = list.getLength();
            for (int i = 0; i < count; i++) {
                Element elem = (Element) list.item(i); //analys tech
                NodeList nlName = elem.getElementsByTagName(NAME);

                NodeList nlFaviourite = elem.getElementsByTagName(FAVORITE);

                String sName = nlName.item(0).getTextContent();
                String sFaviourite = nlFaviourite.item(0).getTextContent();
                boolean bFaviourite = Boolean.parseBoolean(sFaviourite);
                if (bFaviourite) {

                    if (Type == ChartConstants.TYPE_INDICATOR) {
                        faviouriteList.add(Language.getString(sName));
                    } else {
                        faviouriteList.add(sName);
                    }
                }

            }
            deleteTempFile();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("=== XMLSerializationService class-deserializeAnalysTechRecords() ======");
        }

        return faviouriteList;
    }

    private String getFilePathByType(int Type) {
        switch (Type) {
            case ChartConstants.TYPE_INDICATOR:
                return ChartConstants.INDICATOR_ENCRYPTED_FILE;
            case ChartConstants.TYPE_STRATEGY:
                return ChartConstants.STRATEGY_ENCRYPTED_FILE;
            case ChartConstants.TYPE_PATTERN:
                return ChartConstants.PATTERN_ENCRYPTED_FILE;
            case ChartConstants.TYPE_CUSTOM:
                return ChartConstants.CUST_IND_ENCRYPTED_FILE;
            default:
                return "";
        }


    }

    private String getFavFilePath(int type) {
        switch (type) {
            case ChartConstants.TYPE_INDICATOR:
                return ChartConstants.INDICATOR_FAVOURITE_FILE;
            case ChartConstants.TYPE_STRATEGY:
                return ChartConstants.STRATEGY_FAVOURITE_FILE;
            case ChartConstants.TYPE_PATTERN:
                return ChartConstants.PATTERN_FAVOURITE_FILE;
            case ChartConstants.TYPE_CUSTOM:
                return ChartConstants.CI_FAVOURITE_FILE;
            default:
                return ChartConstants.INDICATOR_FAVOURITE_FILE;
        }
    }


}
