package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.chart.ChartFrame;
import com.isi.csvr.chart.IndicatorBuilder;
import com.isi.csvr.chart.customindicators.IndicatorConfigInfo;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 16, 2008
 * Time: 8:32:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomIndUI extends JPanel implements MouseListener {

    int ATTYPE = ChartConstants.TYPE_INDICATOR;
    ArrayList<AnalysTechBase> aTDataStoreLocal = new ArrayList<AnalysTechBase>();
    private Table indTable;
    private AnalysisModel indModel;
    private String encryptedFilePath = ChartConstants.INDICATOR_ENCRYPTED_FILE; //should be according to type

    public CustomIndUI(String encryptedFilePath, int aTType) {

        this.ATTYPE = aTType;

        /***********get from file ******************************************************************************
         * this is to get the faviourite list from the file
         */

        this.ATTYPE = aTType;
        IATXmlSerializeService iATxmlService = new IATXmlSerializeService();
        ArrayList<AnalysTechBase> tempDAtaStoreCustomInd = new ArrayList<AnalysTechBase>();
        try {
            tempDAtaStoreCustomInd = iATxmlService.deserializeAnalysTechRecords(encryptedFilePath, aTType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /***********got from file ************************************************************************************/

        try {
//            aTDataStoreLocal   = iATxmlService.deserializeAnalysTechRecords(ChartConstants.INDICATOR_ENCRYPTED_FILE);
            ArrayList<String> custInNames = ChartFrame.getSharedInstance().getActiveGraph().getCustomIndicatorNames();
            AnalysTechBase atBase;
            for (int i = 0; i < custInNames.size(); i++) {
                IndicatorConfigInfo ici = IndicatorFactory.getIndicatoInfoForName(custInNames.get(i));

                String custIndName = custInNames.get(i);
                atBase = new AnalysTechBase(" ", custIndName, "Custom", ici.Grammer, false);
                /****check the faviourite property**/
                AnalysTechBase fileATBase = null;
                boolean isInFile = false;
                for (int j = 0; j < tempDAtaStoreCustomInd.size(); j++) {
                    fileATBase = tempDAtaStoreCustomInd.get(j);
                    if ((fileATBase.getName()).equals(custIndName)) {
                        isInFile = true;
                        break;
                    }
                }
                /****check the faviourite property**/
                if (isInFile) {
                    atBase.setFaviourite(fileATBase.isFaviourite());
                }
                aTDataStoreLocal.add(atBase);
                atBase = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(aTDataStoreLocal, new AnalysisTechComparator(aTType));

        indModel = new AnalysisModel(aTDataStoreLocal, ATTYPE);
        //view setting
        ViewSetting analysSetting = null;
        try {
            analysSetting = ViewSettingsManager.getSummaryView("ANALYS_TECH_COLS");
            if (analysSetting == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            // return; //ToDo - consier the return value
        }

        indModel.setViewSettings(analysSetting);
        indTable = new Table();
//        indTable.setSortingEnabled();
        indTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        indTable.setModel(indModel);
        indTable.getPopup().setTMTavailable(false);

        indModel.setInsertAnalysTable(indTable);
//        indTable.getPopup().setPrintTarget(indTable.getTable(), PrintManager.PRINT_JTABLE);

//        indTable.getPopup().enableUnsort(false);
//        indTable.getPopup().enableUnsort(false);
        indTable.getModel().updateGUI();
//        GUISettings.applyOrientation(indTable.getPopup());
        indTable.getTable().addMouseListener(this);
        //check whether these are need
        GUISettings.setColumnSettings(analysSetting, TWColumnSettings.getItem("ANALYS_TECH_COLS"));
        analysSetting.setTableNumber(1);
        analysSetting.setParent(this);
        this.setLayout(new BorderLayout());
        this.add(indTable, BorderLayout.CENTER);


    }


    public void mouseClicked(MouseEvent e) {

        if ((e.getModifiers() & e.BUTTON1_MASK) == e.BUTTON1_MASK) { //left click) {

            int selRow = indTable.getTable().getSelectedRow();
            int selCol = indTable.getTable().getSelectedColumn();

            if (e.getClickCount() == 1) {


                if (selCol == 3) { //Faviorite column
                    JCheckBox checkBoxRenderer;
                    //get checkBox renderer

                    checkBoxRenderer = (JCheckBox) indTable.getTable().getCellRenderer(selRow, selCol).
                            getTableCellRendererComponent(indTable.getTable(), indTable.getTable().getModel().getValueAt(selRow, selCol), false, false, selRow, selCol);
                    boolean preState = checkBoxRenderer.isSelected();
//                    checkBoxRenderer .setSelected(!preState);
                    AnalysTechBase atBase = (AnalysTechBase) aTDataStoreLocal.get(selCol);
//                    atBase.setFaviourite(!preState);
                    //  atBase.setFaviourite(false);

                } else if (selCol == 0) { //definiton
                    //String section = "Not_Found.htm";
//                    if (selRow != -1) {
//                        section = (String) indTable.getTable().getModel().getValueAt(selRow, -1); //id
//                    }
                    // NativeMethods.showInsertAnalysisHelp(section);
                } else if (selCol == 1) {

                    String syntax = "", name = "";
                    if (selRow != -1) {
                        syntax = (String) indTable.getTable().getModel().getValueAt(selRow, -2);
                        name = (String) indTable.getTable().getModel().getValueAt(selRow, 2);
                    }

                    /*******************showm grammer in IndicatorBuilder****************************************/
                    IndicatorBuilder ciw = IndicatorBuilder.getInstance();
                    //ciw.getIndicatorBuilderPanel().setIndicatorName("New Custom Indicator");
                    ciw.getIndicatorBuilderPanel().setIndicatorName(name);
                    ciw.getIndicatorBuilderPanel().setIndicatorFormulaText(syntax);
                    ciw.getIndicatorBuilderPanel().getTxtIndicatorName().setEditable(true);
                    ciw.getIndicatorBuilderPanel().setEditing(true);
                    ciw.getIndicatorBuilderPanel().setEditingIndictor(name);
                    ciw.getIndicatorBuilderPanel().markSyntax();
                    ciw.showWindow();
                }


            } else if (e.getClickCount() > 1 && selCol == 2) {
                insertCustIndicatorToGraph();
            }
        }
    }

    public void insertCustIndicatorToGraph() {
        int selectedRow = indTable.getTable().getSelectedRow();
        if (selectedRow != -1) {
            String custIndName = (String) indTable.getTable().getModel().getValueAt(selectedRow, 2); //language.getstring Arabic or english
            ChartFrame.getSharedInstance().getActiveGraph().insertCustomIndicator(custIndName);
        }
    }

    public void mousePressed(MouseEvent e) {

    }


    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public Table getIndTable() {
        return indTable;
    }

    public void setIndTable(Table indTable) {
        this.indTable = indTable;
    }

    public ArrayList<AnalysTechBase> getATDataStoreLocal() {
        return aTDataStoreLocal;
    }

    public void setATDataStoreLocal(ArrayList<AnalysTechBase> aTDataStoreLocal) {
        this.aTDataStoreLocal = aTDataStoreLocal;

    }

    public int getATTYPE() {
        return ATTYPE;
    }

    public void setATTYPE(int ATTYPE) {
        this.ATTYPE = ATTYPE;
    }

    public ArrayList<String> getFavouriteList() {
        ArrayList<String> faviouriteList = new ArrayList<String>();
        for (AnalysTechBase aTB : aTDataStoreLocal) {
            if (aTB.isFaviourite()) {
                faviouriteList.add(aTB.getName());
            }
        }
        return faviouriteList;
    }

}
