package com.isi.csvr.chart;

import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * <p>Title: TW International</p>
 * <p>Description: Customised Label for StockGraph </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class GraphLabel extends JLabel implements Serializable {

    GraphLabel() {
        this("");
    }

    GraphLabel(Color color) {
        this("");
        super.setForeground(color);
    }

    GraphLabel(String text) {
        super(text);
    }

    GraphLabel(String text, Color color) {
        super(text);
        super.setForeground(color);
    }

    GraphLabel(String text, boolean isBold) {
        super(text);
        if (isBold)
            this.setFont(new TWFont(getFont().getName(), Font.BOLD, getFont().getSize()));
    }

    public void paint(Graphics g) {
        Graphics2D gg = (Graphics2D) g;
        gg.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        super.paint(g);
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
    }

    private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
        ois.defaultReadObject();
    }

}