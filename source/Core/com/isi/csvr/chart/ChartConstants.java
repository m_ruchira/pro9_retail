package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 16, 2008
 * Time: 5:38:28 PM
 * To change this template use File | Settings | File Templates.
 */

public class ChartConstants {
    public static final int symbolType = 1;
    public static final int indexType = 2;


    //Later Move all Chartconstantt these in to this Common ChartConstant File
    public static final int TYPE_INDICATOR = 0;
    public static final int TYPE_PATTERN = 1;
    public static final int TYPE_STRATEGY = 2;
    public static final int TYPE_CUSTOM = 3;
    public static String NUM_FORMAT_LONG = "###,###,##0";


}