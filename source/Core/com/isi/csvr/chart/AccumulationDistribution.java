package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 25, 2005 Time: 10:57:50 AM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class AccumulationDistribution extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_AccumDistrib;
    //Fields
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public AccumulationDistribution(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("ND_ACCUM_DISTRI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ACCUM_DISTRI");
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorDefaultProperty idp = (IndicatorDefaultProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());
        }
    }

    public static void getAccumulationDistribution(ArrayList al, byte stepAccDist) {
        double accmVal = 0f, currVal;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);
            accmVal += currVal;
            cr.setStepValue(stepAccDist, accmVal);
        }
//        System.out.println("**** Accumulation/Distribution Step " + (entryTime - System.currentTimeMillis()));
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          byte destIndex) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr;
        double accmVal = 0f, currVal;
        for (int i = 0; i < Math.min(bIndex, al.size()); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);
            accmVal += currVal;
            cr.setValue(destIndex, accmVal);
        }

        //System.out.println("**** Accumulation Distribution Step Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof AccumulationDistribution) {
            AccumulationDistribution acds = (AccumulationDistribution) cp;
            this.innerSource = acds.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        return Language.getString("ND_ACCUM_DISTRI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }
    //############################################

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double accmVal = 0f, currVal;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High == cr.Low) ? 0 : (2 * cr.Close - cr.High - cr.Low) * cr.Volume / (cr.High - cr.Low);
            accmVal += currVal;
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(accmVal);
            }
        }
        System.out.println("**** Accumulation/Distribution " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("ND_ACCUM_DISTRI");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}

