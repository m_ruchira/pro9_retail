package com.isi.csvr.chart.options;


import com.isi.csvr.ChartInterface;
import com.isi.csvr.chart.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 20, 2008
 * Time: 12:56:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChartOptionsWindow extends JDialog implements ActionListener, Themeable, WindowListener {

    private static ChartOptionsWindow optWindowChart = null;
    final int CUSTOMIZE_BOTTOM_TOOL_TAB = 4;
    private final int OPTIONS_WINDOW_WIDTH = 560;
    private final int OPTIONS_WINDOW_HEIGHT = 500;
    private final String GENERAL_SETTINGS_CARD = "GENERAL";
    private final String CHART_THEME_CARD = "CHART_THEME";
    private final String CHART_SETTINGS_CARD = "CHART_SETTINGS";
    private final String SET_DEFAULT_CARD = "DEFAULT";
    //jspinner model parameters
    private final int MAX_LIMIT = 100;
    private final int MIN_LIMIT = 1;
    private final int INITIAL_VAL = 25;
    private final int STEP = 1;
    private final int CHART_OBJECT_ID = 2;
    private final int CHART_TOOL_TYPE = 3;
    //miscellaneous components
    JRadioButton rbSymbolMisc;
    JRadioButton rbSymbolShortName;
    JRadioButton rbSymbolLongName;
    JRadioButton rbSymbolWithShortName;
    JRadioButton rbSymbolWithLongName;
    private TWButton btnSave;
    private TWButton btnCancel;
    //general settings  components
    private JRadioButton rbContinueAdding;
    private JRadioButton rbShiftKey;
    private JRadioButton rbSingleObject;
    private JCheckBox chkConfirmDeletingIndicator;
    private JCheckBox chkShowToolTip;
    private JCheckBox chkFreeStyle;
    private JCheckBox chkShwLastInd;
    private JCheckBox chkConfirmDeletingChart;
    private JLabel lblDefault;
    private JComboBox cmbImageType;
    //chart theme components
    private JLabel lblBgL;
    private CustomColorLabel clblBackgroundL;
    private JLabel lblBgR;
    private CustomColorLabel clblBackgroundR;
    private JLabel lblAxis;
    private CustomColorLabel clblAxis;
    private JLabel lblFont;
    private CustomColorLabel clblFont;
    private JLabel lblAxisBg;
    private CustomColorLabel clblAxisBgInner;
    private JLabel lblAxisBgOuter;
    private CustomColorLabel clblAxisBgOuter;
    private JLabel lblGrid;
    private CustomColorLabel clblGrid;
    private JLabel lblYearSeperator;
    private CustomColorLabel clblYearSeperator;
    private JLabel lblTitleBg;
    private CustomColorLabel clblTitleBgL;
    private JLabel lblTitleBgR;
    private CustomColorLabel clblTitleBgR;
    private JLabel lblTitleFg;
    private CustomColorLabel clblTitleFg;
    private JLabel lblSelected;
    private CustomColorLabel clblSelected;
    private JLabel lblToolTipBg;
    private CustomColorLabel clblToolTipBg;
    private JLabel lblToolTipFg;
    private CustomColorLabel clblToolTipFg;
    private JLabel lblToolTipBorder;
    private CustomColorLabel clblToolTipBorder;
    private JLabel lblCroosHairs;
    private CustomColorLabel clblCrossHairs;
    private JLabel lblDragZoom;
    private CustomColorLabel clblDragZoom;
    private JLabel lblTitle;
    private JLabel lblAxisCol;
    private JLabel lblToolTip;
    private JLabel lblLegendc;
    private CustomFontLabel clblTitleFont;
    private CustomFontLabel clblAxisFont;
    private CustomFontLabel clblToolTipFont;
    private CustomFontLabel clblLegendFont;
    private TWButton btnPreview;
    private JRadioButton rbThemeColors;
    private JRadioButton rbDefault;
    private JRadioButton rbCustomColors;
    //chart settings pannel
    private JCheckBox chkMode;
    private JCheckBox chkPeriod;
    private JCheckBox chkInterval;
    private JRadioButton rbSymbol;
    private JRadioButton rbShortName;
    private JRadioButton rbCompanyName;
    private JRadioButton rbLeft;
    private JRadioButton rbRight;
    private JRadioButton rbBoth;
    private JLabel lblIntraDay;
    private JLabel lblHistory;
    private JSpinner spIntraDay;
    private JSpinner spHistory;
    private JRadioButton rbTileNormal;
    private JRadioButton rbTabbed;
    private JRadioButton rbTileHor;
    private JRadioButton rbTileVer;
    //set defaults components
    private JComboBox cmbHistoryPeriods;
    private JComboBox cmbHistoryIntervals;
    private JComboBox cmbIntraDayPeriods;
    private JComboBox cmbIntraDayIntervals;
    private JRadioButton rbIntraDay;
    private JRadioButton rbHistory;
    private JCheckBox chkShowCurrentPriceLine;
    private JCheckBox chkShowPreviousCloseLine;
    private JCheckBox chkShowOrderLine;
    private JCheckBox chkShowBidAsk;
    private JCheckBox chkShowMinMax;
    private JCheckBox chkShowVWap;
    private JCheckBox chkAlwaysOpenInProChart;
    private JComboBox cmbUpBase;
    private JComboBox cmbDownBase;
    private JCheckBox chkSameColorBase;
    private JCheckBox chkUseThemeBase;
    private JComboBox cmbPriceFieldBase;
    private JComboBox cmbChartStyleBase;
    private JComboBox cmbLineStyleBase;
    private JComboBox cmbThicknessBase;
    private JComboBox cmbUpVolume;
    private JComboBox cmbDownVolume;
    private JCheckBox chkSameColorVolume;
    private JCheckBox chkUseThemeVolume;
    private JComboBox cmbPriceFieldVolume;
    private JComboBox cmbChartStyleVolume;
    private JComboBox cmbLineStyleVolume;
    private JComboBox cmbThicknessVolume;
    private JComboBox cmbUpTurnover;
    private JComboBox cmbDownTurnover;
    private JCheckBox chkSameColorTurnover;
    private JCheckBox chkUseThemeTurnover;
    private JComboBox cmbPriceFieldTurnover;
    private JComboBox cmbChartStyleTurnover;
    private JComboBox cmbLineStyleTurnover;
    private JComboBox cmbThicknessTurnover;
    private JLabel lblHistoryPeriod;
    private JLabel lblHistoryInterval;
    private JLabel lblIntraDayPeriod;
    private JLabel lblIntraDayInterval;
    private JPanel bottomPanel;
    private JPanel finalPanel;
    private JPanel generalSettingsPanel;
    private JPanel chartThemePanel;
    private JPanel chartSettingsPanel;
    private JPanel setDefaultsPanel;
    private JPanel custBtmToolPnl;
    private JPanel graphCololsPanel;
    private TWTabbedPane tabbedPane;
    private String borderTitle = Language.getString("CO_GENERAL_SETTINGS");
    private Color upColorBase = ChartSettings.getSettings().getCP().getColor();
    private Color downColorBase = ChartSettings.getSettings().getCP().getWarningColor();
    private Color upColorVolume = ChartSettings.getSettings().getVolumeCP().getColor();
    private Color downColorVolume = ChartSettings.getSettings().getVolumeCP().getWarningColor();
    private Color upColorTurnover = ChartSettings.getSettings().getTurnoverCP().getColor();
    private Color downColorTurnover = ChartSettings.getSettings().getTurnoverCP().getWarningColor();
    private int[] periodSettings = null;
    private long[] intervalSettings = null;
    private TWTabbedPane tabPaneChartProperties;
    private long[] customInterval = new long[]{-1L};
    private long[] intraDayIntervals = {StockGraph.EVERYMINUTE, StockGraph.EVERY5MINUTES, StockGraph.EVERY10MINUTES,
            StockGraph.EVERY15MINUTES, StockGraph.EVERY30MINUTES, StockGraph.EVERY60MINUTES, customInterval[0]};
    private long[] historyIntervals = {StockGraph.DAILY, StockGraph.WEEKLY, StockGraph.MONTHLY, customInterval[0]};
    private int[] intradayPeriods = {StockGraph.ONE_DAY, StockGraph.SIX_DAYS, StockGraph.ALL_HISTORY,
            StockGraph.CUSTOM_PERIODS};
    private int[] historyPeriods = {StockGraph.ONE_MONTH, StockGraph.THREE_MONTHS, StockGraph.SIX_MONTHS,
            StockGraph.YTD, StockGraph.ONE_YEAR, StockGraph.TWO_YEARS, StockGraph.THREE_YEARS,
            StockGraph.FIVE_YEARS, StockGraph.TEN_YEARS, StockGraph.ALL_HISTORY,
            StockGraph.CUSTOM_PERIODS};
    private int selecttedTab = 0;

    private Vector<Object> vdrawingelementsBackup = null;
    private Vector<Object> vSeldrawingelementsBackup = null;

    public ChartOptionsWindow(int selctedTab) throws HeadlessException {
        //super(Client.getInstance().getWindow());
        super(ChartInterface.getChartFrameParentComponent());
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));

        this.selecttedTab = selctedTab;
        createUI();
    }

    public static ChartOptionsWindow getSharedInstance(int selctedTab) {

        if (optWindowChart != null) {
            optWindowChart.dispose();
            optWindowChart = null; //TODO: this is not good. should remove this ways in the future. to fix the bug in detached mode
        }

        //if (optWindowChart == null) {
        optWindowChart = new ChartOptionsWindow(selctedTab);

        //}

        optWindowChart.setModal(true);

        optWindowChart.loadDefaultChartSettings();
        optWindowChart.loadOptionData(ChartOptions.getCurrentOptions());
        optWindowChart.loadChartTheme(ChartTheme.getCustomThemeDataToApply());

        return optWindowChart;
    }

    public static ChartOptionsWindow getTempInstance() {
        if (optWindowChart == null) {
            optWindowChart = new ChartOptionsWindow(0);
        }
        return optWindowChart;
    }

    public void destroyChartOptionsWindow() {
        optWindowChart = null;
    }

    private void createUI() {

        if (Language.getLanguageTag().equals("FR")) {
            tabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "dt");
        } else {
            tabbedPane = new TWTabbedPane();
        }
        tabbedPane.addTab(Language.getString("CO_CHART_THEME"), createChartThemePanel());
        tabbedPane.addTab(Language.getString("CO_CHART_SETTINGS"), createChartSettingsPanel());
        tabbedPane.addTab(Language.getString("CO_SET_DEFAULTS"), createSetDefaultsPanel());
        tabbedPane.addTab(Language.getString("CO_CUS_BTB"), createCustomizeBtmToolsBarPanel()); //Todo lanauge tags
        tabbedPane.addTab(Language.getString("CO_GENERAL_SETTINGS"), createGeneralSettingsPanel());
        tabbedPane.setSelectedIndex(selecttedTab);
        createBottomPanel();

        finalPanel = new JPanel();
        finalPanel.setLayout(new BorderLayout());
        finalPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        finalPanel.add(tabbedPane, BorderLayout.CENTER);
        finalPanel.add(bottomPanel, BorderLayout.SOUTH);
        GUISettings.applyOrientation(finalPanel);

        /* ======= main frame settings  ======= */
        this.add(finalPanel);
        this.setResizable(false);
        this.setSize(OPTIONS_WINDOW_WIDTH, OPTIONS_WINDOW_HEIGHT);
        this.setModal(true);
        this.setLocationRelativeTo(ChartFrame.getSharedInstance());
        this.setTitle(Language.getString("CHART_OPTIONS"));
        GUISettings.applyOrientation(this);
        addActionListeners();

    }

    public void setSelctedTab(int tabnum) {
        tabbedPane.setSelectedIndex(tabnum);
    }

    /* ========== general settings panel ========= */
    private JPanel createGeneralSettingsPanel() {

        generalSettingsPanel = new JPanel();
        generalSettingsPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"5", "120", "10", "150", "100%"}, 0, 0));
        generalSettingsPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        JPanel pnlLineStudies = new JPanel();
        pnlLineStudies.setLayout(new GridLayout(2, 1, 0, 1));
        pnlLineStudies.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_ADD_OBJECTS"), 0, 0, pnlLineStudies.getFont().deriveFont(Font.BOLD)));

        rbContinueAdding = new JRadioButton(Language.getString("CO_ALWAYS_CONTINUE"));
        rbShiftKey = new JRadioButton(Language.getString("CO_CONTINUE_ADDING"));
        rbSingleObject = new JRadioButton(Language.getString("CO_ALWAYS_ADD_SINGLE"));

        ButtonGroup grp3 = new ButtonGroup();
        grp3.add(rbContinueAdding);
        grp3.add(rbShiftKey);
        grp3.add(rbSingleObject);
        pnlLineStudies.add(rbSingleObject);
        pnlLineStudies.add(rbShiftKey);
        generalSettingsPanel.add(new JLabel(""));

        // now this is the default setting. no need to include in to the option dialog

        /*generalSettingsPanel.add(pnlLineStudies);
        generalSettingsPanel.add(new JLabel(""));
        */
        JPanel pnlDeleteChartElements = new JPanel();
        pnlDeleteChartElements.setLayout(new GridLayout(4, 1, 0, 8));
        pnlDeleteChartElements.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_DELETING_ELEMENTS"), 0, 0, pnlDeleteChartElements.getFont().deriveFont(Font.BOLD)));

        chkConfirmDeletingIndicator = new JCheckBox(Language.getString("CO_CONFIRM_DELETE"));
        chkConfirmDeletingChart = new JCheckBox(Language.getString("CO_ALWAYS_CONFIRM"));
        chkShowToolTip = new JCheckBox(Language.getString("CO_SHOW_TOOLTIP"));
        chkFreeStyle = new JCheckBox(Language.getString("CO_FREE_STYLE"));
        chkShwLastInd = new JCheckBox(Language.getString("CO_SHOW_LAST_IND_VALUE"));
        pnlDeleteChartElements.add(chkConfirmDeletingIndicator);
        pnlDeleteChartElements.add(chkConfirmDeletingChart);
        pnlDeleteChartElements.add(chkShowToolTip);
        pnlDeleteChartElements.add(chkShwLastInd);
        //pnlDeleteChartElements.add(chkFreeStyle); //TODO:charithn need to uncomment in future

        generalSettingsPanel.add(pnlDeleteChartElements);
        generalSettingsPanel.add(new JLabel(""));

        JPanel pnlSymbolOnTree = new JPanel();
//        pnlSymbolOnTree.setLayout(new GridLayout(5, 1, 0, 5));
//        pnlSymbolOnTree.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_SYMBOL_ON_TREE"), 0, 0, pnlSymbolOnTree.getFont().deriveFont(Font.BOLD)));
//        pnlSymbolOnTree.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_SYMBOL_ON_TREE")));

        rbSymbolMisc = new JRadioButton(Language.getString("SYMBOL"));
        rbSymbolShortName = new JRadioButton(Language.getString("CO_SYMBOL_SHORT"));
        rbSymbolLongName = new JRadioButton(Language.getString("CO_SYMBOL_LONG"));
        rbSymbolWithShortName = new JRadioButton(Language.getString("CO_SYMBOL_WITH_SHORT"));
        rbSymbolWithLongName = new JRadioButton(Language.getString("CO_SYMBOL_WITH_LONG"));

        ButtonGroup misGroup = new ButtonGroup();
        misGroup.add(rbSymbolMisc);
        misGroup.add(rbSymbolShortName);
        misGroup.add(rbSymbolLongName);
        misGroup.add(rbSymbolWithShortName);
        misGroup.add(rbSymbolWithLongName);

//        pnlSymbolOnTree.add(rbSymbolMisc);
//        pnlSymbolOnTree.add(rbSymbolShortName);
//        pnlSymbolOnTree.add(rbSymbolLongName);
//        pnlSymbolOnTree.add(rbSymbolWithShortName);
//        pnlSymbolOnTree.add(rbSymbolWithLongName);

        generalSettingsPanel.add(pnlSymbolOnTree);

        return generalSettingsPanel;
    }

    /* ========== Chart Theme settings panel ======= */
    private JPanel createChartThemePanel() {

        chartThemePanel = new JPanel();
        chartThemePanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));
        chartThemePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "10", "210", "15", "90", "100%"}, 0, 0));

        JPanel pnlRadioButton = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 5));
        btnPreview = new TWButton(Language.getString("CO_PREIVIEW"));
        btnPreview.setToolTipText(Language.getString("CO_TOOLTIP_APPLY"));
        rbThemeColors = new JRadioButton(Language.getString("CO_RESET"));
        rbThemeColors.setToolTipText(Language.getString("CO_TOOLTIP_RESET"));
        rbDefault = new JRadioButton(Language.getString("CO_DEFAULT"));
        rbCustomColors = new JRadioButton(Language.getString("CO_CUSTOM_COLORS"));
        rbDefault.setToolTipText(Language.getString("CO_TOOLTIP_DEFAULT"));
        rbCustomColors.setToolTipText(Language.getString("CO_TOOLTIP_CUSTOM_COLORS"));

        ButtonGroup colorsGroup = new ButtonGroup();
        colorsGroup.add(rbThemeColors);
        colorsGroup.add(rbDefault);
        colorsGroup.add(rbCustomColors);

        pnlRadioButton.add(rbThemeColors);
        pnlRadioButton.add(rbDefault);
        pnlRadioButton.add(rbCustomColors);
        pnlRadioButton.add(btnPreview);

        chartThemePanel.add(pnlRadioButton);
        chartThemePanel.add(new Label(""));

        graphCololsPanel = new JPanel(new GridLayout(10, 4, 6, 6));
        graphCololsPanel.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_COLORS"), 0, 0, graphCololsPanel.getFont().deriveFont(Font.BOLD)));
//        graphCololsPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_CHART_COLORS")));

        lblBgL = new JLabel(Language.getString("CO_GRAPH_LEFT"), JLabel.LEADING);
        clblBackgroundL = new CustomColorLabel(new Color(255, 255, 255));
        lblBgR = new JLabel(Language.getString("CO_GRAPH_RIGHT"), JLabel.LEADING);
        clblBackgroundR = new CustomColorLabel(new Color(255, 255, 255));

        graphCololsPanel.add(lblBgL);
        graphCololsPanel.add(clblBackgroundL);
        graphCololsPanel.add(lblBgR);
        graphCololsPanel.add(clblBackgroundR);

        lblAxis = new JLabel(Language.getString("CO_AXIS_COLOR"), JLabel.LEADING);
        clblAxis = new CustomColorLabel(new Color(0, 0, 0));
        lblFont = new JLabel(Language.getString("CO_FONT_COLOR"), JLabel.LEADING);
        clblFont = new CustomColorLabel(new Color(0, 0, 0));

        graphCololsPanel.add(lblAxis);
        graphCololsPanel.add(clblAxis);
        graphCololsPanel.add(lblFont);
        graphCololsPanel.add(clblFont);

        lblAxisBg = new JLabel(Language.getString("CO_AXIS_INNER"), JLabel.LEADING);
        clblAxisBgInner = new CustomColorLabel(new Color(234, 237, 239));
        lblAxisBgOuter = new JLabel(Language.getString("CO_AXIS_OUTER"), JLabel.LEADING);
        clblAxisBgOuter = new CustomColorLabel(new Color(234, 237, 239));

        graphCololsPanel.add(lblAxisBg);
        graphCololsPanel.add(clblAxisBgInner);
        graphCololsPanel.add(lblAxisBgOuter);
        graphCololsPanel.add(clblAxisBgOuter);

        lblGrid = new JLabel(Language.getString("CO_AXIS_GRID"), JLabel.LEADING);
        clblGrid = new CustomColorLabel(new Color(213, 219, 225));
        lblYearSeperator = new JLabel(Language.getString("CO_YEAR_SEP"), JLabel.LEADING);
        clblYearSeperator = new CustomColorLabel(new Color(155, 204, 236));

        graphCololsPanel.add(lblGrid);
        graphCololsPanel.add(clblGrid);
        graphCololsPanel.add(lblYearSeperator);
        graphCololsPanel.add(clblYearSeperator);

        lblTitleBg = new JLabel(Language.getString("CO_TITLE_BG_LEFT"), JLabel.LEADING);
        clblTitleBgL = new CustomColorLabel(new Color(255, 0, 0));
        lblTitleBgR = new JLabel(Language.getString("CO_TITLE_BG_RIGHT"), JLabel.LEADING);
        clblTitleBgR = new CustomColorLabel(new Color(192, 192, 192));

        graphCololsPanel.add(lblTitleBg);
        graphCololsPanel.add(clblTitleBgL);
        graphCololsPanel.add(lblTitleBgR);
        graphCololsPanel.add(clblTitleBgR);

        lblTitleFg = new JLabel(Language.getString("CO_TITLE_FG"), JLabel.LEADING);
        clblTitleFg = new CustomColorLabel(new Color(0, 0, 0));
        lblSelected = new JLabel(Language.getString("CO_SELECTED_COLOR"), JLabel.LEADING);
        clblSelected = new CustomColorLabel(new Color(119, 136, 153));

        graphCololsPanel.add(lblTitleFg);
        graphCololsPanel.add(clblTitleFg);
        graphCololsPanel.add(lblSelected);
        graphCololsPanel.add(clblSelected);

        lblToolTipBg = new JLabel(Language.getString("CO_TOOLTIP_BG"), JLabel.LEADING);
        clblToolTipBg = new CustomColorLabel(new Color(215, 224, 230));
        lblToolTipFg = new JLabel(Language.getString("CO_TOOLTIP_FG"), JLabel.LEADING);
        clblToolTipFg = new CustomColorLabel(new Color(0, 0, 0));
        lblToolTipBorder = new JLabel(Language.getString("CO_TOOLTIP_BORDER"), JLabel.LEADING);
        clblToolTipBorder = new CustomColorLabel(new Color(0, 0, 0));

        graphCololsPanel.add(lblToolTipBg);
        graphCololsPanel.add(clblToolTipBg);
        graphCololsPanel.add(lblToolTipFg);
        graphCololsPanel.add(clblToolTipFg);
        graphCololsPanel.add(lblToolTipBorder);
        graphCololsPanel.add(clblToolTipBorder);

        lblCroosHairs = new JLabel(Language.getString("CO_CROSSHAIRS"), JLabel.LEADING);
        clblCrossHairs = new CustomColorLabel(new Color(176, 224, 255));
        lblDragZoom = new JLabel(Language.getString("CO_DRAG_ZOOM"), JLabel.LEADING);
        clblDragZoom = new CustomColorLabel(new Color(218, 112, 214));

        graphCololsPanel.add(lblCroosHairs);
        graphCololsPanel.add(clblCrossHairs);
        graphCololsPanel.add(lblDragZoom);
        graphCololsPanel.add(clblDragZoom);

        graphCololsPanel.add(new JLabel());
        graphCololsPanel.add(new JLabel());
        graphCololsPanel.add(new JLabel());
        graphCololsPanel.add(new JLabel());
        chartThemePanel.add(graphCololsPanel);
        chartThemePanel.add(new Label(""));

        JPanel pnlFonts = new JPanel(new GridLayout(3, 4, 8, 6));
        pnlFonts.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_FONTS"), 0, 0, pnlFonts.getFont().deriveFont(Font.BOLD)));
//        pnlFonts.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_CHART_FONTS")));

        lblTitle = new JLabel(Language.getString("CO_TIT_FONT"), JLabel.LEADING);
        lblAxisCol = new JLabel(Language.getString("CO_AXIS_FONT"), JLabel.LEADING);
        lblToolTip = new JLabel(Language.getString("CO_TOOLTIP_FONT"), JLabel.LEADING);
        lblLegendc = new JLabel(Language.getString("CO_LEG_FONT"), JLabel.LEADING);

        ThemeData data = ChartTheme.getCustomThemeDataToApply();
        //Font font = new Font("Arial", 0, 12);
        clblTitleFont = new CustomFontLabel(data.TitleFont);
        clblAxisFont = new CustomFontLabel(data.AxisFont);
        clblToolTipFont = new CustomFontLabel(data.ToolTipFont);
        clblLegendFont = new CustomFontLabel(data.LegendFont);

        pnlFonts.add(lblTitle);
        pnlFonts.add(clblTitleFont);
        pnlFonts.add(lblAxisCol);
        pnlFonts.add(clblAxisFont);
        pnlFonts.add(lblToolTip);
        pnlFonts.add(clblToolTipFont);
        pnlFonts.add(lblLegendc);
        pnlFonts.add(clblLegendFont);
        pnlFonts.add(new JLabel(""));
        pnlFonts.add(new JLabel(""));

        chartThemePanel.add(pnlFonts);
        chartThemePanel.add(new Label(""));

        return chartThemePanel;

    }

    /* ========== Chart settings panel ======= */
    private JPanel createChartSettingsPanel() {

        chartSettingsPanel = new JPanel();
        if (Language.getLanguageTag().equals("FR")) {
            chartSettingsPanel.setLayout(new FlexGridLayout(new String[]{"100%"},
                    new String[]{"5", "50", "10", "50", "10", "50", "10", "80", "10", "60", "10", "60", "100%"}, 0, 0));
        } else {
            chartSettingsPanel.setLayout(new FlexGridLayout(new String[]{"100%"},
                    new String[]{"5", "50", "10", "50", "10", "50", "10", "50", "10", "60", "10", "60", "100%"}, 0, 0));
        }

        chartSettingsPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        JPanel pnlChartTitleContent = new JPanel();
        pnlChartTitleContent.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_BCTC"), 0, 0, pnlChartTitleContent.getFont().deriveFont(Font.BOLD)));
//        pnlChartTitleContent.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_BCTC")));
        pnlChartTitleContent.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 0));

        chkMode = new JCheckBox(Language.getString("CO_MODE"));
        chkPeriod = new JCheckBox(Language.getString("CO_PERIOD"));
        chkInterval = new JCheckBox(Language.getString("CO_INTERVAL"));

        pnlChartTitleContent.add(chkMode);
        pnlChartTitleContent.add(chkPeriod);
        pnlChartTitleContent.add(chkInterval);

        chartSettingsPanel.add(new JLabel(""));
        chartSettingsPanel.add(pnlChartTitleContent);
        chartSettingsPanel.add(new JLabel(""));

        JPanel pnlLegendStyle = new JPanel();
        pnlLegendStyle.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_LEGEND_STYLE"), 0, 0, pnlLegendStyle.getFont().deriveFont(Font.BOLD)));
//        pnlLegendStyle.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_LEGEND_STYLE")));
        pnlLegendStyle.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 0));
        ButtonGroup bgLegendStyle = new ButtonGroup();

        rbSymbol = new JRadioButton(Language.getString("SYMBOL"));
        rbShortName = new JRadioButton(Language.getString("CO_SHORT"));
        rbCompanyName = new JRadioButton(Language.getString("CO_COMPANY_NAME"));

        bgLegendStyle.add(rbSymbol);
        bgLegendStyle.add(rbShortName);
        bgLegendStyle.add(rbCompanyName);

        pnlLegendStyle.add(rbSymbol);
        //pnlLegendStyle.add(rbShortName);
        pnlLegendStyle.add(rbCompanyName);

        chartSettingsPanel.add(pnlLegendStyle);
        chartSettingsPanel.add(new JLabel(""));

        JPanel pnlYaxisPosition = new JPanel();
        pnlYaxisPosition.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_DEFAULTY"), 0, 0, pnlYaxisPosition.getFont().deriveFont(Font.BOLD)));
//        pnlYaxisPosition.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_DEFAULTY")));
        pnlYaxisPosition.setLayout(new FlowLayout(FlowLayout.LEADING, 10, 0));

        ButtonGroup bgYAxis = new ButtonGroup();

        rbLeft = new JRadioButton(Language.getString("CO_LEFT"));
        rbRight = new JRadioButton(Language.getString("CO_RIGHT"));
        rbBoth = new JRadioButton(Language.getString("CO_BOTH"));

        bgYAxis.add(rbLeft);
        bgYAxis.add(rbRight);
        bgYAxis.add(rbBoth);

        pnlYaxisPosition.add(rbLeft);
        pnlYaxisPosition.add(rbRight);
        pnlYaxisPosition.add(rbBoth);

        chartSettingsPanel.add(pnlYaxisPosition);
        chartSettingsPanel.add(new JLabel(""));

        JPanel pnlRightMargin = new JPanel();
        pnlRightMargin.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_RIGHT_MARGIN"), 0, 0, pnlRightMargin.getFont().deriveFont(Font.BOLD)));
//        pnlRightMargin.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_RIGHT_MARGIN")));

        lblIntraDay = new JLabel(Language.getString("CO_NO_INTRA"));
        lblHistory = new JLabel(Language.getString("CO_NO_HISTORY"));

        SpinnerModel smIntraDay = new SpinnerNumberModel(INITIAL_VAL, MIN_LIMIT, MAX_LIMIT, STEP);
        spIntraDay = new JSpinner(smIntraDay);

        SpinnerModel smHistory = new SpinnerNumberModel(INITIAL_VAL, MIN_LIMIT, MAX_LIMIT, STEP);
        spHistory = new JSpinner(smHistory);

        if (Language.getLanguageTag().equals("FR")) {
            pnlRightMargin.setLayout(new FlexGridLayout(new String[]{"50%", "10%"}, new String[]{"20", "10", "20"}, 15, 0));
            pnlRightMargin.add(lblIntraDay);
            pnlRightMargin.add(spIntraDay);
            pnlRightMargin.add(new JPanel());
            pnlRightMargin.add(new JPanel());
            pnlRightMargin.add(lblHistory);
            pnlRightMargin.add(spHistory);
        } else {
            pnlRightMargin.setLayout(new FlowLayout(FlowLayout.LEADING, 25, 0));
            pnlRightMargin.add(lblIntraDay);
            pnlRightMargin.add(spIntraDay);
            pnlRightMargin.add(lblHistory);
            pnlRightMargin.add(spHistory);
        }


        chartSettingsPanel.add(pnlRightMargin);
        chartSettingsPanel.add(new JLabel(""));

        JPanel pnlImageType = new JPanel();

        pnlImageType.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_SAVE_IMAGE"), 0, 0, pnlImageType.getFont().deriveFont(Font.BOLD)));
        pnlImageType.setLayout(new FlowLayout(FlowLayout.LEADING, 15, 10));

        lblDefault = new JLabel(Language.getString("CO_DEFAULT_TYPE"));
        cmbImageType = new JComboBox(ChartConstants.IMAGE_EXTENSIONS);

        pnlImageType.add(lblDefault);
        pnlImageType.add(cmbImageType);

        chartSettingsPanel.add(pnlImageType);
        chartSettingsPanel.add(new JLabel(""));

        JPanel pnlTileType = new JPanel();

        pnlTileType.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_TILE_MODE"), 0, 0, pnlImageType.getFont().deriveFont(Font.BOLD)));
        pnlTileType.setLayout(new FlowLayout(FlowLayout.LEADING, 15, 10));

        rbTileNormal = new JRadioButton(Language.getString("TILE"));
        rbTabbed = new JRadioButton(Language.getString("TABBED"));
        rbTileHor = new JRadioButton(Language.getString("CO_TILE_HOR"));
        rbTileVer = new JRadioButton(Language.getString("CO_TILE_VER"));
        ButtonGroup grp = new ButtonGroup();
        grp.add(rbTileNormal);
        grp.add(rbTabbed);
        grp.add(rbTileHor);
        grp.add(rbTileVer);

        pnlTileType.add(rbTileNormal);
        pnlTileType.add(rbTabbed);
        pnlTileType.add(rbTileHor);
        pnlTileType.add(rbTileVer);

        //chartSettingsPanel.add(pnlTileType); //TODO: charithn- if required in future.
        return chartSettingsPanel;

    }

    /* ========== set defaults panel ========= */
    private JPanel createSetDefaultsPanel() {

        setDefaultsPanel = new JPanel();
        setDefaultsPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"130", "5", "150", "5", "100", "100%"}, 0, 0));
        setDefaultsPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

        JPanel pnlGeneral = new JPanel();
        pnlGeneral.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_GENERAL"), 0, 0, pnlGeneral.getFont().deriveFont(Font.BOLD)));
        pnlGeneral.setLayout(null);

        JPanel pnlHistory = new JPanel();
        pnlHistory.setLayout(null);
        pnlHistory.setBounds(260, 20, 265, 100);
        pnlHistory.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));

        rbIntraDay = new JRadioButton(Language.getString("CO_INTRADAY"));
        rbHistory = new JRadioButton(Language.getString("CO_HISTORY"));
        ButtonGroup grp = new ButtonGroup();
        grp.add(rbIntraDay);
        grp.add(rbHistory);

        int x = 25;
        int y = 15;
        int width = 50;
        int height = 22;
        int vGap = 10;
        int comboWidth = 90;

        lblHistoryPeriod = new JLabel(Language.getString("CO_PERIOD2"), JLabel.LEADING);
        lblHistoryInterval = new JLabel(Language.getString("CO_INTERVAL2"), JLabel.LEADING);

        String[] saHistoryPeriods = GraphFrame.getHistoryPeriods();
        String[] saHistoryIntervals = GraphFrame.getHistoryIntervals();
        String[] saIntraDayPeriods = GraphFrame.getCurrentPeriods();
        String[] saIntraDayIntervals = GraphFrame.getCurrentIntervals();

        cmbHistoryPeriods = new JComboBox(saHistoryPeriods);
        cmbHistoryIntervals = new JComboBox(saHistoryIntervals);
        cmbIntraDayPeriods = new JComboBox(saIntraDayPeriods);
        cmbIntraDayIntervals = new JComboBox(saIntraDayIntervals);

        if (Language.isLTR()) {
            rbHistory.setBounds(x, y, width + 30, height);
            lblHistoryPeriod.setBounds(x - 10, y + height, width, height);
            cmbHistoryPeriods.setBounds(x + width, y + height, comboWidth, height);
            lblHistoryInterval.setBounds(x - 10, y + 2 * height + vGap, width, height);
            cmbHistoryIntervals.setBounds(x + width, y + 2 * height + vGap, comboWidth, height);
        } else {
            rbHistory.setBounds(x + 150, y, width + 30, height);
            lblHistoryPeriod.setBounds(x - 10 + 150, y + height, width + 20, height);
            cmbHistoryPeriods.setBounds(x + 50, y + height, comboWidth, height);
            lblHistoryInterval.setBounds(x - 10 + 150, y + 2 * height + vGap, width + 20, height);
            cmbHistoryIntervals.setBounds(x + 50, y + 2 * height + vGap, comboWidth, height);
        }
        pnlHistory.add(rbHistory);
        pnlHistory.add(lblHistoryPeriod);
        pnlHistory.add(cmbHistoryPeriods);
        pnlHistory.add(lblHistoryInterval);
        pnlHistory.add(cmbHistoryIntervals);

        JPanel pnlIntraDay = new JPanel();
        pnlIntraDay.setLayout(null);
        pnlIntraDay.setBounds(20, 20, 220, 100);
        pnlIntraDay.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));

        lblIntraDayPeriod = new JLabel(Language.getString("CO_PERIOD2"), JLabel.LEADING);
        lblIntraDayInterval = new JLabel(Language.getString("CO_INTERVAL2"), JLabel.LEADING);

        if (Language.isLTR()) {
            rbIntraDay.setBounds(x, y, width + 30, height);
            lblIntraDayPeriod.setBounds(x - 10, y + height, width, height);
            cmbIntraDayPeriods.setBounds(x + width, y + height, comboWidth + 20, height);
            lblIntraDayInterval.setBounds(x - 10, y + 2 * height + vGap, width, height);
            cmbIntraDayIntervals.setBounds(x + width, y + 2 * height + vGap, comboWidth + 20, height);
        } else {
            rbIntraDay.setBounds(x + 100, y, width + 30, height);
            lblIntraDayPeriod.setBounds(x - 10 + 100, y + height, width + 20, height);
            cmbIntraDayPeriods.setBounds(x - 20, y + height, comboWidth + 20, height);
            lblIntraDayInterval.setBounds(x - 10 + 100, y + 2 * height + vGap, width + 20, height);
            cmbIntraDayIntervals.setBounds(x - 20, y + 2 * height + vGap, comboWidth + 20, height);
        }

        pnlIntraDay.add(rbIntraDay);
        pnlIntraDay.add(lblIntraDayPeriod);
        pnlIntraDay.add(cmbIntraDayPeriods);
        pnlIntraDay.add(lblIntraDayInterval);
        pnlIntraDay.add(cmbIntraDayIntervals);

        pnlGeneral.add(pnlIntraDay);
        pnlGeneral.add(pnlHistory);

        setDefaultsPanel.add(pnlGeneral);
        setDefaultsPanel.add(new JLabel(""));

        /*======================= base chart properties ================= */

        JPanel pnlBaseChart = new JPanel();
        pnlBaseChart.setLayout(null);
        //pnlBaseChart.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_BCP")));
        JLabel lblPriceBase = new JLabel(Language.getString("CO_PF"), JLabel.LEADING);
        cmbPriceFieldBase = UIHelper.getPriorityCombo();
        JLabel lblChartStyleBase = new JLabel(Language.getString("CO_CS"), JLabel.LEADING);


        JLabel lblStyleBase = new JLabel(Language.getString("CO_LS"), JLabel.LEADING);
        cmbLineStyleBase = UIHelper.getPenStyleCombo();
        JLabel lblThicknessBase = new JLabel(Language.getString("CO_LT"), JLabel.LEADING);
        cmbThicknessBase = UIHelper.getPenWidthCombo();

        JComponent[] ca = {lblStyleBase, cmbLineStyleBase, lblThicknessBase, cmbThicknessBase};
        cmbChartStyleBase = UIHelper.getGraphStyleCombo(false, ca);

        int m = 20;
        int n = 10;
        int hGap = 20;

        width = 90;
        comboWidth = 110;

        lblPriceBase.setBounds(m, n, width, height);
        cmbPriceFieldBase.setBounds(m, n + height, comboWidth - 10, height);
        lblChartStyleBase.setBounds(m, n + 2 * height + 2, width, height);
        cmbChartStyleBase.setBounds(m, n + 3 * height + 2, comboWidth - 10, height);

        lblStyleBase.setBounds(m + hGap + width, n, width, height);
        cmbLineStyleBase.setBounds(m + hGap + width, n + height, comboWidth, height);
        lblThicknessBase.setBounds(m + hGap + width, n + 2 * height + 2, width, height);
        cmbThicknessBase.setBounds(m + hGap + width, n + 3 * height + 2, comboWidth, height);

        JPanel pnlChartColorsBase = new JPanel();
        pnlChartColorsBase.setLayout(null);
        pnlChartColorsBase.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_COLOR"), 0, 0, pnlChartColorsBase.getFont().deriveFont(Font.BOLD)));
        pnlChartColorsBase.setBounds(m + 2 * width + 55, n - 5, 270, 100);

        JLabel lblUp = new JLabel(Language.getString("CO_UP"), JLabel.LEADING);
        JLabel lblDown = new JLabel(Language.getString("CO_DOWN"), JLabel.LEADING);
        cmbUpBase = UIHelper.getColorCombo();
        cmbDownBase = UIHelper.getColorCombo();
        chkSameColorBase = new JCheckBox(Language.getString("CO_USE_SAME"));
        chkUseThemeBase = new JCheckBox(Language.getString("CO_USE_THEME"));

        int p = 20;
        int q = 15;
        int ww = 80;
        int hh = 20;
        int v = 10;
        comboWidth = 100;
        if (Language.isLTR()) {
            lblUp.setBounds(p, q, ww + 30, hh);
            //cmbUpBase.setBounds(p + ww, q, comboWidth, hh);
            cmbUpBase.setBounds(p + ww + 30, q, comboWidth, hh);
            lblDown.setBounds(p, q + hh + v, ww + 30, hh);
            //cmbDownBase.setBounds(p + ww, q + hh + v, comboWidth, hh);
            cmbDownBase.setBounds(p + ww + 30, q + hh + v, comboWidth, hh);
            chkSameColorBase.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeBase.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);
        } else {
            lblUp.setBounds(p + 150, q, ww, hh);
            cmbUpBase.setBounds(p + ww - 20, q, comboWidth, hh);
            lblDown.setBounds(p + 150, q + hh + v, ww, hh);
            cmbDownBase.setBounds(p + ww - 20, q + hh + v, comboWidth, hh);
            chkSameColorBase.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeBase.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);

        }

        pnlChartColorsBase.add(lblUp);
        pnlChartColorsBase.add(cmbUpBase);
        pnlChartColorsBase.add(lblDown);
        pnlChartColorsBase.add(cmbDownBase);
        pnlChartColorsBase.add(chkSameColorBase);
        pnlChartColorsBase.add(chkUseThemeBase);

        pnlBaseChart.add(lblPriceBase);
        pnlBaseChart.add(cmbPriceFieldBase);
        pnlBaseChart.add(lblChartStyleBase);
        pnlBaseChart.add(cmbChartStyleBase);

        pnlBaseChart.add(lblStyleBase);
        pnlBaseChart.add(cmbLineStyleBase);
        pnlBaseChart.add(lblThicknessBase);
        pnlBaseChart.add(cmbThicknessBase);

        pnlBaseChart.add(pnlChartColorsBase);
        /* ===================== end of base chart ============================================ */

        /* ===================== volume chart ============================================ */

        JPanel pnlVolumeChart = new JPanel();
        pnlVolumeChart.setLayout(null);
        //pnlVolumeChart.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_BCP")));
        JLabel lblPriceVolume = new JLabel(Language.getString("CO_PF"), JLabel.LEADING);
        cmbPriceFieldVolume = UIHelper.getPriorityCombo();
        JLabel lblChartStyleVolume = new JLabel(Language.getString("CO_CS"), JLabel.LEADING);
        JLabel lblStyleVolume = new JLabel(Language.getString("CO_LS"), JLabel.LEADING);
        cmbLineStyleVolume = UIHelper.getPenStyleCombo();
        JLabel lblThicknessVolume = new JLabel(Language.getString("CO_LT"), JLabel.LEADING);
        cmbThicknessVolume = UIHelper.getPenWidthCombo();
        ca = new JComponent[]{lblStyleVolume, cmbLineStyleVolume, lblThicknessVolume, cmbThicknessVolume};
        cmbChartStyleVolume = UIHelper.getGraphStyleCombo(true, ca);


        width = 90;
        comboWidth = 110;

        lblPriceVolume.setBounds(m, n, width, height);
        cmbPriceFieldVolume.setBounds(m, n + height, comboWidth - 10, height);
        lblChartStyleVolume.setBounds(m, n + 2 * height + 2, width, height);
        cmbChartStyleVolume.setBounds(m, n + 3 * height + 2, comboWidth - 10, height);

        lblStyleVolume.setBounds(m + hGap + width, n, width, height);
        cmbLineStyleVolume.setBounds(m + hGap + width, n + height, comboWidth, height);
        lblThicknessVolume.setBounds(m + hGap + width, n + 2 * height + 2, width, height);
        cmbThicknessVolume.setBounds(m + hGap + width, n + 3 * height + 2, comboWidth, height);


        JPanel pnlChartColorsVolume = new JPanel();
        pnlChartColorsVolume.setLayout(null);
        pnlChartColorsVolume.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_COLOR"), 0, 0, pnlChartColorsVolume.getFont().deriveFont(Font.BOLD)));
        pnlChartColorsVolume.setBounds(m + 2 * width + 55, n - 5, 270, 100);

        JLabel lblUpVolume = new JLabel(Language.getString("CO_UP"), JLabel.LEADING);
        JLabel lblDownVolume = new JLabel(Language.getString("CO_DOWN"), JLabel.LEADING);
        cmbUpVolume = UIHelper.getColorCombo();
        cmbDownVolume = UIHelper.getColorCombo();
        chkSameColorVolume = new JCheckBox(Language.getString("CO_USE_SAME"));
        chkUseThemeVolume = new JCheckBox(Language.getString("CO_USE_THEME"));

        comboWidth = 100;

        if (Language.isLTR()) {
            lblUpVolume.setBounds(p, q, ww + 30, hh);
            cmbUpVolume.setBounds(p + ww + 30, q, comboWidth, hh);
            lblDownVolume.setBounds(p, q + hh + v, ww + 30, hh);
            cmbDownVolume.setBounds(p + ww + 30, q + hh + v, comboWidth, hh);
            chkSameColorVolume.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeVolume.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);
        } else {
            lblUpVolume.setBounds(p + 150, q, ww, hh);
            cmbUpVolume.setBounds(p + ww - 20, q, comboWidth, hh);
            lblDownVolume.setBounds(p + 150, q + hh + v, ww, hh);
            cmbDownVolume.setBounds(p + ww - 20, q + hh + v, comboWidth, hh);
            chkSameColorVolume.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeVolume.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);
        }

        pnlChartColorsVolume.add(lblUpVolume);
        pnlChartColorsVolume.add(cmbUpVolume);
        pnlChartColorsVolume.add(lblDownVolume);
        pnlChartColorsVolume.add(cmbDownVolume);
        pnlChartColorsVolume.add(chkSameColorVolume);
        pnlChartColorsVolume.add(chkUseThemeVolume);

        pnlVolumeChart.add(lblPriceVolume);
        pnlVolumeChart.add(cmbPriceFieldVolume);
        pnlVolumeChart.add(lblChartStyleVolume);
        pnlVolumeChart.add(cmbChartStyleVolume);

        pnlVolumeChart.add(lblStyleVolume);
        pnlVolumeChart.add(cmbLineStyleVolume);
        pnlVolumeChart.add(lblThicknessVolume);
        pnlVolumeChart.add(cmbThicknessVolume);

        pnlVolumeChart.add(pnlChartColorsVolume);

        //these properties should be disabled
        lblPriceVolume.setEnabled(false);
        cmbPriceFieldVolume.setEnabled(false);
        //lblChartStyleVolume.setEnabled(true);
        cmbChartStyleVolume.setEnabled(true);
        lblStyleVolume.setEnabled(false);
        cmbLineStyleVolume.setEnabled(false);
        lblThicknessVolume.setEnabled(false);
        cmbThicknessVolume.setEnabled(false);

        /* ============================== end of volume chart ================================= */

        /* ===================== turnover chart ============================================ */
        JPanel pnlTurnoverChart = new JPanel();
        pnlTurnoverChart.setLayout(null);
        //pnlVolumeChart.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_BCP")));
        JLabel lblPriceTurnover = new JLabel(Language.getString("CO_PF"), JLabel.LEADING);
        cmbPriceFieldTurnover = UIHelper.getPriorityCombo();
        JLabel lblChartStyleTurnover = new JLabel(Language.getString("CO_CS"), JLabel.LEADING);
        JLabel lblStyleTurnover = new JLabel(Language.getString("CO_LS"), JLabel.LEADING);
        cmbLineStyleTurnover = UIHelper.getPenStyleCombo();
        JLabel lblThicknessTurnover = new JLabel(Language.getString("CO_LT"), JLabel.LEADING);
        cmbThicknessTurnover = UIHelper.getPenWidthCombo();
        ca = new JComponent[]{lblStyleTurnover, cmbLineStyleTurnover, lblThicknessTurnover, cmbThicknessTurnover};
        cmbChartStyleTurnover = UIHelper.getGraphStyleCombo(true, ca);

        width = 90;
        comboWidth = 110;

        lblPriceTurnover.setBounds(m, n, width, height);
        cmbPriceFieldTurnover.setBounds(m, n + height, comboWidth - 10, height);
        lblChartStyleTurnover.setBounds(m, n + 2 * height + 2, width, height);
        cmbChartStyleTurnover.setBounds(m, n + 3 * height + 2, comboWidth - 10, height);

        lblStyleTurnover.setBounds(m + hGap + width, n, width, height);
        cmbLineStyleTurnover.setBounds(m + hGap + width, n + height, comboWidth, height);
        lblThicknessTurnover.setBounds(m + hGap + width, n + 2 * height + 2, width, height);
        cmbThicknessTurnover.setBounds(m + hGap + width, n + 3 * height + 2, comboWidth, height);


        JPanel pnlChartColorsTurnover = new JPanel();
        pnlChartColorsTurnover.setLayout(null);
        pnlChartColorsTurnover.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_COLOR"), 0, 0, pnlChartColorsVolume.getFont().deriveFont(Font.BOLD)));
        pnlChartColorsTurnover.setBounds(m + 2 * width + 55, n - 5, 270, 100);

        JLabel lblUpTurnover = new JLabel(Language.getString("CO_UP"), JLabel.LEADING);
        JLabel lblDownTurnover = new JLabel(Language.getString("CO_DOWN"), JLabel.LEADING);
        cmbUpTurnover = UIHelper.getColorCombo();
        cmbDownTurnover = UIHelper.getColorCombo();
        chkSameColorTurnover = new JCheckBox(Language.getString("CO_USE_SAME"));
        chkUseThemeTurnover = new JCheckBox(Language.getString("CO_USE_THEME"));

        comboWidth = 100;

        if (Language.isLTR()) {
            lblUpTurnover.setBounds(p, q, ww + 30, hh);
            cmbUpTurnover.setBounds(p + ww + 30, q, comboWidth, hh);
            lblDownTurnover.setBounds(p, q + hh + v, ww + 30, hh);
            cmbDownTurnover.setBounds(p + ww + 30, q + hh + v, comboWidth, hh);
            chkSameColorTurnover.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeTurnover.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);
        } else {
            lblUpTurnover.setBounds(p + 150, q, ww, hh);
            cmbUpTurnover.setBounds(p + ww - 20, q, comboWidth, hh);
            lblDownTurnover.setBounds(p + 150, q + hh + v, ww, hh);
            cmbDownTurnover.setBounds(p + ww - 20, q + hh + v, comboWidth, hh);
            chkSameColorTurnover.setBounds(p - 2, q + 2 * hh + v + 3, ww + 40, hh);
            chkUseThemeTurnover.setBounds(p - 2 + ww + 40, q + 2 * hh + v + 3, ww + 45, hh);
        }

        pnlChartColorsTurnover.add(lblUpTurnover);
        pnlChartColorsTurnover.add(cmbUpTurnover);
        pnlChartColorsTurnover.add(lblDownTurnover);
        pnlChartColorsTurnover.add(cmbDownTurnover);
        pnlChartColorsTurnover.add(chkSameColorTurnover);
        pnlChartColorsTurnover.add(chkUseThemeTurnover);

        pnlTurnoverChart.add(lblPriceTurnover);
        pnlTurnoverChart.add(cmbPriceFieldTurnover);
        pnlTurnoverChart.add(lblChartStyleTurnover);
        pnlTurnoverChart.add(cmbChartStyleTurnover);

        pnlTurnoverChart.add(lblStyleTurnover);
        pnlTurnoverChart.add(cmbLineStyleTurnover);
        pnlTurnoverChart.add(lblThicknessTurnover);
        pnlTurnoverChart.add(cmbThicknessTurnover);

        pnlTurnoverChart.add(pnlChartColorsTurnover);

        //these properties should be disabled
        lblPriceTurnover.setEnabled(false);
        cmbPriceFieldTurnover.setEnabled(false);
        //lblChartStyleTurnover.setEnabled(true);
        cmbChartStyleTurnover.setEnabled(true);
        lblStyleTurnover.setEnabled(false);
        cmbLineStyleTurnover.setEnabled(false);
        lblThicknessTurnover.setEnabled(false);
        cmbThicknessTurnover.setEnabled(false);
        /* ============================== end of turnover chart ================================= */


        JPanel pnlChartProperties = new JPanel();
        pnlChartProperties.setLayout(new BorderLayout());
        tabPaneChartProperties = new TWTabbedPane(TWTabbedPane.LAYOUT_POLICY.ScrollTabLayout);
        tabPaneChartProperties.addTab(Language.getString("CO_BASE_CP"), pnlBaseChart);
        tabPaneChartProperties.addTab(Language.getString("CO_VOLUME_CP"), pnlVolumeChart);
        tabPaneChartProperties.addTab(Language.getString("CO_TURNOVER_CP"), pnlTurnoverChart);

        tabPaneChartProperties.selectTab(0);
        pnlChartProperties.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_CHART_PROPERTIES"), 0, 0, pnlChartProperties.getFont().deriveFont(Font.BOLD)));

        pnlChartProperties.add(tabPaneChartProperties, BorderLayout.CENTER);
        setDefaultsPanel.add(pnlChartProperties);
        setDefaultsPanel.add(new JLabel(""));

        /* ============================== end of  chart properties ================================= */

        JPanel pnlOther = new JPanel();
        pnlOther.setLayout(null);
        pnlOther.setBorder(BorderFactory.createTitledBorder(null, Language.getString("CO_OTHER"), 0, 0, pnlOther.getFont().deriveFont(Font.BOLD)));
        int xx = 15;
        int yy = 25;
        int w = 200;
        int h = 14;
        int hG = 40;
        int vG = 10;

        chkShowCurrentPriceLine = new JCheckBox(Language.getString("CO_SHOW_CPL"));
        chkShowCurrentPriceLine.setBounds(xx, yy, w, h);
        pnlOther.add(chkShowCurrentPriceLine);

        chkShowOrderLine = new JCheckBox(Language.getString("CO_SHOW_OL"));
        chkShowOrderLine.setBounds(xx, yy + h + vG, w, h);
        pnlOther.add(chkShowOrderLine);

        chkShowBidAsk = new JCheckBox(Language.getString("CO_SHOW_BAT"));
        chkShowBidAsk.setBounds(xx, yy + (2 * h) + 2 * vG, w, h);
        pnlOther.add(chkShowBidAsk);

        chkShowMinMax = new JCheckBox(Language.getString("CO_SHOW_MINMAX"));
        chkShowMinMax.setBounds(xx + w + hG, yy, w, h);
        pnlOther.add(chkShowMinMax);

        chkShowVWap = new JCheckBox(Language.getString("CO_SHOW_VWAP"));
        //chkShowVWap.setBounds(xx + w + hG, yy + 2 * h + 2 * vG, w, h);
        chkShowVWap.setBounds(xx + w + hG, yy + h + vG, w, h);

        if (ChartInterface.isVWAPEnabled()) pnlOther.add(chkShowVWap);

        chkAlwaysOpenInProChart = new JCheckBox(Language.getString("CO_OPEN_IN_PRO"));
        //chkAlwaysOpenInProChart.setBounds(xx + w + hG, yy + h + vG, w, h);
        // pnlOther.add(chkAlwaysOpenInProChart);


        chkShowPreviousCloseLine = new JCheckBox(Language.getString("CO_SHOW_PCL"));
        chkShowPreviousCloseLine.setBounds(xx + w + hG, yy + 2 * h + 2 * vG, w, h);
        pnlOther.add(chkShowPreviousCloseLine);

        setDefaultsPanel.add(pnlOther);
        setDefaultsPanel.add(new JLabel(""));

        return setDefaultsPanel;

    }

    /* ======= customize bottom tool bar ======= */
    private JPanel createCustomizeBtmToolsBarPanel() {

        try {
            custBtmToolPnl = new JPanel();
            custBtmToolPnl.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"5", "22", "3", "100%", "5"}, 0, 0));

            //select tool pnl
            JPanel selectToolPnl = new JPanel();

            selectToolPnl.setLayout(new FlexGridLayout(new String[]{"5", "200", "100%", "200", "5"}, new String[]{"22"}, 0, 0));

            Object drawingelements[][] = ChartObjectElementList.getDrawingElements();

            final Vector<Object> vdrawingelements = new Vector<Object>();
            final JList listSrc = new JList(vdrawingelements);
            for (int i = 0; i < drawingelements.length; i++) {
                vdrawingelements.add(drawingelements[i]);

            }
            /*******tool types ***************/

            //keep a string array and get from that

            ArrayList<TWComboItem> listToolTypes = new ArrayList<TWComboItem>();
            listToolTypes.add(new TWComboItem(ChartObjectElementList.DRAWING_TOOLS, Language.getString("CO_DRAWING_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.FIBONACI_TOOLS, Language.getString("CO_FIB_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.CHANNEL_TOOLS, Language.getString("CO_CHANNEL_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.TERM_TOOLS, Language.getString("CO_TERM_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.GANN_TOOLS, Language.getString("CO_GAN_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.ZOOM_TOOLS, Language.getString("CO_ZOOM_TOOLS")));
            listToolTypes.add(new TWComboItem(ChartObjectElementList.ALL_TOOLS, Language.getString("CO_ALL_TOOLS")));

            /*******tool types ***************/


            //hash table  = object id - > boolean
            if (!ChartFrame.getSharedInstance().bottomToolBar.isChartOptionDirty) {
                ChartFrame.getSharedInstance().bottomToolBar.fillObjectStateTable();
                ChartFrame.getSharedInstance().bottomToolBar.isChartOptionDirty = true;
            }

            //hash table to temporary keep newly added objects
            vdrawingelementsBackup = new Vector<Object>();
            vSeldrawingelementsBackup = new Vector<Object>();

            //fill hashtable //this should be done @ the construction of chart frame

            final Vector<Object> vSeldrawingelements = new Vector<Object>();
            final JList listDest = new JList(vSeldrawingelements);
            final TWComboBox cmbTool = new TWComboBox(new TWComboModel(listToolTypes));
            cmbTool.addActionListener(new ActionListener() {
                                          public void actionPerformed(ActionEvent e) {
                                              TWComboItem cmbitem = (TWComboItem) cmbTool.getSelectedItem();
                                              int selToolSet = Integer.parseInt(cmbitem.getId());
                                              Hashtable hashToolSet = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets;

                                              vdrawingelements.clear();
                                              vSeldrawingelements.clear();

                                              if (selToolSet == ChartObjectElementList.ALL_TOOLS) {

                                                  Enumeration enm = hashToolSet.keys();
                                                  while (enm.hasMoreElements()) {
                                                      Object key = enm.nextElement(); //tool type
                                                      Hashtable hashtoolType = (Hashtable) hashToolSet.get(key);
                                                      //corroponding hashtable & corrosponding elements
                                                      Object[][] elementsToolType = ChartObjectElementList.getToolset((Integer) key);
                                                      for (int i = 0; i < elementsToolType.length; i++) {
                                                          Object[] objArr = elementsToolType[i];
                                                          if (objArr != null) {
                                                              if ((Boolean) hashtoolType.get(objArr[CHART_OBJECT_ID])) {
                                                                  vSeldrawingelements.add(objArr);
                                                              } else {
                                                                  vdrawingelements.add(objArr);
                                                              }
                                                          }
                                                      }
                                                  }
                                              } else {
                                                  Object[][] elements = ChartObjectElementList.getToolset(selToolSet);
                                                  //get selected list & unselcted list
                                                  Hashtable hashobjectList = (Hashtable) hashToolSet.get(selToolSet);
                                                  //enumeration
                                                  for (int i = 0; i < elements.length; i++) {
                                                      Object[] objArr = elements[i];
                                                      if (objArr != null) {
                                                          if ((Boolean) hashobjectList.get(objArr[CHART_OBJECT_ID])) {
                                                              vSeldrawingelements.add(objArr);
                                                          } else {
                                                              vdrawingelements.add(objArr);
                                                          }
                                                      }
                                                  }
                                              }
                                              listSrc.updateUI();
                                              listDest.updateUI();

                                          }
                                      }

            );

            cmbTool.setSelectedIndex(ChartObjectElementList.ALL_TOOLS);

            //******store the initial selected and unselected objects***********//
            for (int i = 0; i < vdrawingelements.size(); i++) {
                vdrawingelementsBackup.add(vdrawingelements.get(i));

            }
            for (int i = 0; i < vSeldrawingelements.size(); i++) {
                vSeldrawingelementsBackup.add(vSeldrawingelements.get(i));

            }

            selectToolPnl.add(new JLabel(""));
            selectToolPnl.add(cmbTool);
            selectToolPnl.add(new JLabel(""));
            //cmbTool value chage
            selectToolPnl.add(new JLabel(""));
            selectToolPnl.add(new JLabel("")

            );


            /******************************list panel*****************************/
            JPanel listPnl = new JPanel(new BorderLayout());

            /********************** drawing elements ***************************/
            JPanel srcListPnl = new JPanel(new FlexGridLayout(new String[]{"3", "200", "3"}, new String[]{"100%"}, 0, 0));
            listSrc.addMouseListener(new MouseAdapter() {
                                         public void mouseClicked(MouseEvent e) {
                                             if ((e.getClickCount() > 1) && (listSrc.getSelectedIndex() >= 0)
                                                     && (vdrawingelements.size() > listSrc.getSelectedIndex())) {
                                                 Object obj = vdrawingelements.remove(listSrc.getSelectedIndex());
                                                 vSeldrawingelements.add(obj);
                                                 Object[] object = (Object[]) obj;
                                                 //update corrsopondign hastable
                                                 //TODO: do a better way usig objects
                                                 int selToolSet = Integer.parseInt((((TWComboItem) cmbTool.getSelectedItem()).getId()));
                                                 if (obj != null) {
                                                     if (selToolSet == ChartObjectElementList.ALL_TOOLS) {
                                                         //get corroponding hashtable
                                                         Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                                                         hash.put((Integer) object[CHART_OBJECT_ID], true); //2 = object ID
                                                     } else {

                                                         Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get(selToolSet); //selected index of combo
                                                         hash.put((Integer) object[CHART_OBJECT_ID], true); //2 = object ID
                                                     }
                                                 }
                                                 listSrc.updateUI();
                                                 listDest.updateUI();
                                             }
                                         }
                                     }

            );

            //listSrc.setBackground(Color.WHITE);
            listSrc.setBackground(Theme.getColor("CUSTOMIZE_BTB_BACKGROUND"));
            listSrc.setForeground(Theme.getColor("CUSTOMIZE_BTB_FONT_COLOR"));
            ListCellRenderer renderer = new ImageListRenderer();
            listSrc.setFont(new Font("Arial", Font.PLAIN, 12));
            listSrc.setCellRenderer(renderer);
            JScrollPane srcScrollPane = new JScrollPane(listSrc);
            srcScrollPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.GRAY, Color.GRAY));
            srcListPnl.add(new JLabel(""));
            srcListPnl.add(srcScrollPane);
            srcListPnl.add(new JLabel(""));
            srcListPnl.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_UNSELCTED_TOOLS"))); //Todo: language tag
            listPnl.add(srcListPnl, BorderLayout.WEST);

            /*********************add remove buttions *******************************/
            JPanel addRemovePanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"150", "43", "100%"}, 0, 0));
            addRemovePanel.add(new JLabel(""));

            JPanel buttonPnl = new JPanel(new FlexGridLayout(new String[]{"6", "90", "7"}, new String[]{"20", "3", "20"}, 0, 0));
            TWButton btnAdd = new TWButton(Language.getString("CO_ADD_CUS"));
            btnAdd.addActionListener(new ActionListener() {
                                         public void actionPerformed(ActionEvent e) {
                                             Object[] selectedObjects = listSrc.getSelectedValues();
                                             for (int i = 0; i < selectedObjects.length; i++) {
                                                 //if ((listSrc.getSelectedIndex() >= 0) && (vdrawingelements.size() > listSrc.getSelectedIndex())) {
                                                 if (selectedObjects[i] != null) {
                                                     Object obj = vdrawingelements.remove(selectedObjects[i]);
                                                     Object[] object = (Object[]) selectedObjects[i];  //set names for them

                                                     int selToolSet = Integer.parseInt((((TWComboItem) cmbTool.getSelectedItem()).getId()));
                                                     if (obj != null) {
                                                         if (selToolSet == ChartObjectElementList.ALL_TOOLS) {
                                                             //get corroponding hashtable
                                                             Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                                                             hash.put((Integer) object[CHART_OBJECT_ID], true); //2 = object ID
                                                         } else {
                                                             Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get(selToolSet); //selected index of combo
                                                             hash.put((Integer) object[CHART_OBJECT_ID], true); //2 = object ID
                                                         }
                                                     }

                                                     vSeldrawingelements.add(selectedObjects[i]);

                                                 }
                                             }
                                             listSrc.clearSelection();
                                             listDest.updateUI();
                                             listSrc.updateUI();
                                         }
                                     }

            );

            TWButton btnRemove = new TWButton(Language.getString("CO_REMOVE_CUS"));
            btnRemove.addActionListener(new ActionListener() {
                                            public void actionPerformed(ActionEvent e) {
                                                Object[] selectedObjects = listDest.getSelectedValues();
                                                //if ((listDest.getSelectedIndex() >= 0) && (vSeldrawingelements.size() > listDest.getSelectedIndex())) {
                                                for (int i = 0; i < selectedObjects.length; i++) {
                                                    if (selectedObjects[i] != null) {
                                                        Object obj = vSeldrawingelements.remove(selectedObjects[i]);
                                                        Object[] object = (Object[]) selectedObjects[i];

                                                        int selToolSet = Integer.parseInt((((TWComboItem) cmbTool.getSelectedItem()).getId()));
                                                        if (obj != null) {
                                                            if (selToolSet == ChartObjectElementList.ALL_TOOLS) { //todo actually this condition can be remove
                                                                //get corroponding hashtable
                                                                Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                                                                hash.put((Integer) object[CHART_OBJECT_ID], false); //2 = object ID
                                                            } else {

                                                                Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get(selToolSet); //selected index of combo
                                                                hash.put((Integer) object[CHART_OBJECT_ID], false); //2 = object ID
                                                            }

                                                        }
                                                        vdrawingelements.add(selectedObjects[i]);
                                                    }
                                                }
                                                listDest.clearSelection();
                                                listDest.updateUI();
                                                listSrc.updateUI();
                                            }
                                        }
            );

            buttonPnl.add(new JLabel(""));
            buttonPnl.add(btnAdd);
            buttonPnl.add(new JLabel(""));

            buttonPnl.add(new JLabel(""));
            buttonPnl.add(new JLabel(""));
            buttonPnl.add(new JLabel(""));

            buttonPnl.add(new JLabel(""));
            buttonPnl.add(btnRemove);
            buttonPnl.add(new JLabel(""));

            addRemovePanel.add(buttonPnl);
            addRemovePanel.add(new JLabel(""));

            listPnl.add(addRemovePanel, BorderLayout.CENTER);

            /*********************add remove buttions *******************************/


            //destination  list
            JPanel destListPnl = new JPanel(new FlexGridLayout(new String[]{"3", "200", "3"}, new String[]{"100%"}, 0, 0));

            listDest.setBackground(Theme.getColor("CUSTOMIZE_BTB_BACKGROUND"));
            listDest.setForeground(Theme.getColor("CUSTOMIZE_BTB_FONT_COLOR"));
            listDest.setFont(new Font("Arial", Font.PLAIN, 12));
            listDest.setCellRenderer(renderer);
            listDest.addMouseListener(new MouseAdapter() {
                                          public void mouseClicked(MouseEvent e) {
                                              if ((e.getClickCount() > 1) && (listDest.getSelectedIndex() >= 0)
                                                      && (vSeldrawingelements.size() > listDest.getSelectedIndex())) {
                                                  Object obj = vSeldrawingelements.remove(listDest.getSelectedIndex());
                                                  vdrawingelements.add(obj);
                                                  Object[] object = (Object[]) obj;
                                                  //update the hashtable
                                                  int selToolSet = Integer.parseInt((((TWComboItem) cmbTool.getSelectedItem()).getId()));
                                                  if (obj != null) {
                                                      if (selToolSet == ChartObjectElementList.ALL_TOOLS) { //todo actually this condition can be remove
                                                          //get corroponding hashtable
                                                          Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                                                          hash.put((Integer) object[CHART_OBJECT_ID], false); //2 = object ID
                                                      } else {

                                                          Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get(selToolSet); //selected index of combo
                                                          hash.put((Integer) object[CHART_OBJECT_ID], false); //2 = object ID
                                                      }
                                                  }

                                                  listSrc.updateUI();
                                                  listDest.updateUI();
                                              }
                                          }
                                      }
            );

            JScrollPane listDestScrollPane = new JScrollPane(listDest);
            listDestScrollPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.GRAY, Color.GRAY));
            destListPnl.add(new JLabel(""));
            destListPnl.add(listDestScrollPane);
            destListPnl.add(new JLabel(""));
            destListPnl.setBorder(BorderFactory.createTitledBorder(Language.getString("CO_SELCTED_TOOLS")));
            listPnl.add(destListPnl, BorderLayout.EAST);


            /******************************list panel*****************************/

            custBtmToolPnl.add(new JPanel());
            custBtmToolPnl.add(selectToolPnl);
            custBtmToolPnl.add(new JLabel(""));
            custBtmToolPnl.add(listPnl);
            custBtmToolPnl.add(new JLabel(""));

            custBtmToolPnl.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Color.GRAY, Color.GRAY));

            return custBtmToolPnl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return custBtmToolPnl;
    }

    /* ======= bottom panel whcih holds ok and cancel buttons ======= */
    private void createBottomPanel() {

        bottomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        btnSave = new TWButton(Language.getString("CO_SAVE"));
        btnCancel = new TWButton(Language.getString("CANCEL"));
        bottomPanel.add(btnSave);
        bottomPanel.add(btnCancel);
    }

    private void addActionListeners() {

        btnCancel.addActionListener(this);
        btnSave.addActionListener(this);

        rbDefault.addActionListener(this);
        rbCustomColors.addActionListener(this);
        rbThemeColors.addActionListener(this);
        btnPreview.addActionListener(this);

        this.addWindowListener(this);

    }

    private void addListeners() {

        rbIntraDay.addActionListener(this);
        rbHistory.addActionListener(this);

        chkSameColorBase.addActionListener(this);
        cmbUpBase.addActionListener(this);
        cmbDownBase.addActionListener(this);
        chkUseThemeBase.addActionListener(this);

        cmbHistoryIntervals.addActionListener(this);
        cmbHistoryPeriods.addActionListener(this);
        cmbIntraDayIntervals.addActionListener(this);
        cmbIntraDayPeriods.addActionListener(this);

        cmbUpVolume.addActionListener(this);
        cmbDownVolume.addActionListener(this);
        chkUseThemeVolume.addActionListener(this);
        chkSameColorVolume.addActionListener(this);

        cmbUpTurnover.addActionListener(this);
        cmbDownTurnover.addActionListener(this);
        chkUseThemeTurnover.addActionListener(this);
        chkSameColorTurnover.addActionListener(this);

        cmbLineStyleVolume.addActionListener(this);
        cmbChartStyleVolume.addActionListener(this);

        cmbLineStyleTurnover.addActionListener(this);
        cmbChartStyleTurnover.addActionListener(this);

    }

    private void removeListeners() {

        rbIntraDay.removeActionListener(this);
        rbHistory.removeActionListener(this);
        chkSameColorBase.removeActionListener(this);

        cmbUpBase.removeActionListener(this);
        cmbDownBase.removeActionListener(this);
        chkUseThemeBase.removeActionListener(this);
        chkSameColorBase.removeActionListener(this);

        cmbHistoryIntervals.removeActionListener(this);
        cmbHistoryPeriods.removeActionListener(this);
        cmbIntraDayIntervals.removeActionListener(this);
        cmbIntraDayPeriods.removeActionListener(this);

        cmbUpVolume.removeActionListener(this);
        cmbDownVolume.removeActionListener(this);
        chkUseThemeVolume.removeActionListener(this);
        chkSameColorVolume.removeActionListener(this);

        cmbUpTurnover.removeActionListener(this);
        cmbDownTurnover.removeActionListener(this);
        chkUseThemeTurnover.removeActionListener(this);
        chkSameColorTurnover.removeActionListener(this);

    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btnCancel) {
            ChartFrame.getSharedInstance().applyCustomColors(ChartTheme.getCustomThemeDataToApply());
            loadOptionData(ChartOptions.getCurrentOptions());
            loadDefaultChartSettings();

            //*************************if cancelled restore to the starting objects***************//
            for (int i = 0; i < vdrawingelementsBackup.size(); i++) {
                Object obj = vdrawingelementsBackup.get(i);
                Object[] object = (Object[]) obj;
                Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                hash.put((Integer) object[CHART_OBJECT_ID], false);
            }

            for (int i = 0; i < vSeldrawingelementsBackup.size(); i++) {
                Object obj = vSeldrawingelementsBackup.get(i);
                Object[] object = (Object[]) obj;
                Hashtable hash = ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets.get((Integer) object[CHART_TOOL_TYPE]); //selected index of combo
                hash.put((Integer) object[CHART_OBJECT_ID], true);
            }
            //*********************************************************************************//

            this.dispose();
            this.destroyChartOptionsWindow();
        } else if (e.getSource() == rbDefault) {
            borderTitle = Language.getString("CO_CHART_THEME");
            enableDisableThemeColrPanel(false);
            loadChartTheme(ChartTheme.applyFontFieldsFromCustomTheme(new ThemeData()));
            btnPreview.setEnabled(true);
        } else if (e.getSource() == rbThemeColors) {
            loadChartTheme(ChartTheme.applyFontFieldsFromCustomTheme(ChartTheme.getThemeDataForCurrentProTheme()));
            enableDisableThemeColrPanel(false);
            borderTitle = Language.getString("CO_CHART_THEME");
        } else if (e.getSource() == rbCustomColors) {
            loadChartTheme(ChartTheme.getCurrentCustomThemeData());
            enableDisableThemeColrPanel(true);
            borderTitle = Language.getString("CO_CHART_THEME");
        } else if (e.getSource() == btnPreview) {
            borderTitle = Language.getString("CO_CHART_THEME");
            ChartFrame.getSharedInstance().applyCustomColors(getExisitingColors());
        } else if (e.getSource() == btnSave) {
            //chart options saving
            OptionData tempData = ChartOptions.getCurrentOptions();
            ChartOptions.saveOptionData(extractOptionData());
            //chart theme saving
            ThemeData data = extractTheme();
            if (rbCustomColors.isSelected()) {
                ChartTheme.saveCustomizedThemeData(data);
            } else {
                ThemeData finalData = ChartTheme.updateFontFieldsInCustomTheme(data);
                ChartTheme.saveCustomizedThemeData(finalData);
            }
            ChartFrame.getSharedInstance().applyCustomColors(ChartTheme.getCustomThemeDataToApply());
            saveDefaultSettings();
            loadDefaultChartSettings();
            this.dispose();
            this.destroyChartOptionsWindow();

            if (isDirsty(tempData)) {
                JOptionPane.showMessageDialog(ChartInterface.getChartFrameParentComponent(), Language.getString("CHART_OPTION_INFO_MSG"), Language.getString("INFORMATION"), JOptionPane.INFORMATION_MESSAGE);
            }


            //bootom tool bar settings
            ChartFrame.getSharedInstance().bottomToolBar.reDolayoutToolBar(); //pass later
            BottomToolBarData.saveToolBarData(ChartFrame.getSharedInstance().bottomToolBar.hashtoolsets);

            // combo box of history mode
        } else if (e.getSource() == cmbHistoryPeriods) {

            JComboBox cmb = (JComboBox) e.getSource();
            String itemString = (String) cmb.getSelectedItem();
            //check whether its the custom periods selected
            if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                CustomDialog dlg = null;

                //dlg = CustomDialogs.getCustomTimePeriodsDialog(Calendar.DAY_OF_YEAR, 60, false);
                dlg = getCustomTimePeriodsDialog(ChartSettings.getSettings().getCustomPeriod(), ChartSettings.getSettings().getCustomRange(), false);

                dlg.setModal(true);
                dlg.setLocationRelativeTo(this);
                dlg.pack();
                dlg.setResizable(false);
                dlg.setVisible(true);

                if (dlg.result != null) {
                    //copies the results returned from the cutom period dialog box
                    periodSettings = (int[]) dlg.result;
                } else {
                    cmbHistoryPeriods.setSelectedIndex(0);
                    return;
                }
            } else {
                //clear the period setting results in combobox menu changed event.
                //if "other periods" is not selected.
                periodSettings = null;
            }
        } else if (e.getSource() == cmbHistoryIntervals) {

            JComboBox cmb = (JComboBox) e.getSource();
            String itemString = (String) cmb.getSelectedItem();

            //check whether its the custom interval selected
            if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                CustomDialog dlg = null;

                //dlg = getCustomTimePeriodsDialog(ChartSettings.getSettings().getCustomPeriod(), ChartSettings.getSettings().getCustomRange(), false);   //false since its not in intraday moed
                dlg = getCustomIntervalsDialog(ChartSettings.getSettings().getCustomIntervalType(), ChartSettings.getSettings().getCustomIntervalFactor(), false);

                dlg.setModal(true);
                dlg.setLocationRelativeTo(this);
                dlg.pack();
                dlg.setResizable(false);
                dlg.setVisible(true);
                if (dlg.result != null) {
                    intervalSettings = (long[]) dlg.result;
                } else {
                    cmbHistoryIntervals.setSelectedIndex(0);
                    return;
                }
            } else {
                //clear the interval setting results in combobox menu changed event
                intervalSettings = null;
            }
        } else if (e.getSource() == cmbIntraDayPeriods) {

            JComboBox cmb = (JComboBox) e.getSource();
            String itemString = (String) cmb.getSelectedItem();
            //check whether its the custom periods selected
            if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                CustomDialog dlg = null;

                //dlg = CustomDialogs.getCustomTimePeriodsDialog(Calendar.DAY_OF_YEAR, 60, true);
                dlg = getCustomTimePeriodsDialog(ChartSettings.getSettings().getCustomPeriod(), ChartSettings.getSettings().getCustomRange(), true);

                dlg.setModal(true);
                dlg.setLocationRelativeTo(this);
                dlg.pack();
                dlg.setResizable(false);
                dlg.setVisible(true);

                if (dlg.result != null) {
                    //copies the results returned from the cutom interval dialog box
                    periodSettings = (int[]) dlg.result;
                } else {
                    cmbIntraDayPeriods.setSelectedIndex(0);
                    return;
                }
            } else {
                //clear the cuctom period settings
                periodSettings = null;
            }
        } else if (e.getSource() == cmbIntraDayIntervals) {

            JComboBox cmb = (JComboBox) e.getSource();
            String itemString = (String) cmb.getSelectedItem();

            //check whether its the custom periods selected
            if (itemString.equals(Language.getString("OTHER_PERIODS"))) {
                CustomDialog dlg = null;

                //dlg = getCustomIntervalsDialog(1L, 3L, true);   //true since its  in intraday moed
                dlg = getCustomIntervalsDialog(ChartSettings.getSettings().getCustomIntervalType(), ChartSettings.getSettings().getCustomIntervalFactor(), true);

                dlg.setModal(true);
                dlg.setLocationRelativeTo(this);
                dlg.pack();
                dlg.setResizable(false);
                dlg.setVisible(true);
                if (dlg.result != null) {
                    intervalSettings = (long[]) dlg.result;
                } else {
                    cmbIntraDayIntervals.setSelectedIndex(0);
                    return;
                }
            } else {
                //clear the custom interval setting results in combobox menu changed event
                intervalSettings = null;
            }
        } else if (e.getSource() == cmbUpBase) {
            upColorBase = (Color) ((ComboItem) cmbUpBase.getSelectedItem()).getValue();
        } else if (e.getSource() == cmbDownBase) {
            downColorBase = (Color) ((ComboItem) cmbDownBase.getSelectedItem()).getValue();
        } else if (e.getSource() == chkSameColorBase) {
            if (chkSameColorBase.isSelected()) {
                cmbDownBase.setEnabled(false);
            } else {
                cmbUpBase.setEnabled(true);
                cmbDownBase.setEnabled(true);
            }
        } else if (e.getSource() == chkUseThemeBase) {
            if (chkUseThemeBase.isSelected()) {
                cmbUpBase.setEnabled(false);
                cmbDownBase.setEnabled(false);
                chkSameColorBase.setEnabled(false);
            } else {
                cmbUpBase.setEnabled(true);
                cmbDownBase.setEnabled(true);
                chkSameColorBase.setEnabled(true);
            }
        } else if (e.getSource() == rbHistory) {

            cmbIntraDayIntervals.setEnabled(false);
            cmbIntraDayPeriods.setEnabled(false);
            cmbHistoryPeriods.setEnabled(true);
            cmbHistoryIntervals.setEnabled(true);

        } else if (e.getSource() == rbIntraDay) {

            cmbIntraDayIntervals.setEnabled(true);
            cmbIntraDayPeriods.setEnabled(true);
            cmbHistoryPeriods.setEnabled(false);
            cmbHistoryIntervals.setEnabled(false);

        }

        //volume chart specific
        else if (e.getSource() == cmbUpVolume) {
            upColorVolume = (Color) ((ComboItem) cmbUpVolume.getSelectedItem()).getValue();
        } else if (e.getSource() == cmbDownVolume) {
            downColorVolume = (Color) ((ComboItem) cmbDownVolume.getSelectedItem()).getValue();
        } else if (e.getSource() == chkSameColorVolume) {
            if (chkSameColorVolume.isSelected()) {
                cmbDownVolume.setEnabled(false);
            } else {
                cmbUpVolume.setEnabled(true);
                cmbDownVolume.setEnabled(true);
            }
        } else if (e.getSource() == chkUseThemeVolume) {
            if (chkUseThemeVolume.isSelected()) {
                cmbUpVolume.setEnabled(false);
                cmbDownVolume.setEnabled(false);
                chkSameColorVolume.setEnabled(false);
            } else {
                cmbUpVolume.setEnabled(true);
                cmbDownVolume.setEnabled(true);
                chkSameColorVolume.setEnabled(true);
            }
        } else if (e.getSource() == cmbChartStyleVolume) {
            if (cmbChartStyleVolume.getSelectedIndex() == 0) { // line style
                cmbLineStyleVolume.setEnabled(true);
                cmbThicknessVolume.setEnabled(true);
            } else {
                cmbLineStyleVolume.setEnabled(false);
                cmbThicknessVolume.setEnabled(false);
            }
        }

        //turonver chart specific
        else if (e.getSource() == cmbUpTurnover) {
            upColorTurnover = (Color) ((ComboItem) cmbUpTurnover.getSelectedItem()).getValue();
        } else if (e.getSource() == cmbDownTurnover) {
            downColorTurnover = (Color) ((ComboItem) cmbDownTurnover.getSelectedItem()).getValue();
        } else if (e.getSource() == chkSameColorTurnover) {
            if (chkSameColorTurnover.isSelected()) {
                cmbDownTurnover.setEnabled(false);
            } else {
                cmbUpTurnover.setEnabled(true);
                cmbDownTurnover.setEnabled(true);
            }
        } else if (e.getSource() == chkUseThemeTurnover) {
            if (chkUseThemeTurnover.isSelected()) {
                cmbUpTurnover.setEnabled(false);
                cmbDownTurnover.setEnabled(false);
                chkSameColorTurnover.setEnabled(false);
            } else {
                cmbUpTurnover.setEnabled(true);
                cmbDownTurnover.setEnabled(true);
                chkSameColorTurnover.setEnabled(true);
            }
        } else if (e.getSource() == cmbChartStyleTurnover) {
            if (cmbChartStyleTurnover.getSelectedIndex() == 0) { // line style
                cmbLineStyleTurnover.setEnabled(true);
                cmbThicknessTurnover.setEnabled(true);
            } else {
                cmbLineStyleTurnover.setEnabled(false);
                cmbThicknessTurnover.setEnabled(false);
            }
        }
    }

    public boolean isDirsty(OptionData data) {

        if (ChartOptions.getCurrentOptions().misc_symbol_Name != data.misc_symbol_Name) {
            return true;
        }
        return false;
    }

    public void enableDisableThemeColrPanel(boolean isEnable) {
        Component[] compArr = ((JPanel) graphCololsPanel).getComponents();
        for (int i = 0; i < compArr.length; i++) {
            compArr[i].setEnabled(isEnable);
        }

    }

    private ThemeData getExisitingColors() {
        ThemeData data = new ThemeData();
        data.GraphAreaLeft = clblBackgroundL.getBackground();
        data.GraphAreaRight = clblBackgroundR.getBackground();
        data.AxisColor = clblAxis.getBackground();
        data.FontColor = clblFont.getBackground();
        data.CrosshairsColor = clblCrossHairs.getBackground();
        data.GridColor = clblGrid.getBackground();
        data.AxisBGInner = clblAxisBgInner.getBackground();
        data.AxisBGOuter = clblAxisBgOuter.getBackground();
        data.YearSeperator = clblYearSeperator.getBackground();
        data.TitleBGLeft = clblTitleBgL.getBackground();
        data.TitleBGRight = clblTitleBgR.getBackground();
        data.TitleForeground = clblTitleFg.getBackground();
        data.SelectedColor = clblSelected.getBackground();
        data.TooltipBackground = clblToolTipBg.getBackground();
        data.TooltipForeground = clblToolTipFg.getBackground();
        data.TooltipBorderColor = clblToolTipBorder.getBackground();
        data.DragZoomColor = clblDragZoom.getBackground();
        data.AxisFont = clblAxisFont.getFont();
        data.TitleFont = clblTitleFont.getFont();
        data.LegendFont = clblLegendFont.getFont();
        data.ToolTipFont = clblToolTipFont.getFont();

        return data;

    }

    public void saveDefaultSettings() {

        int intPeriod, intPeriodIntraday, intPeriodHistory;
        long longInterval, longIntervalIntraday, longIntervalHistory;
        boolean isCurrentMode = rbIntraDay.isSelected();
        if (isCurrentMode) {//intraday
            intPeriod = intradayPeriods[cmbIntraDayPeriods.getSelectedIndex()];
            longInterval = intraDayIntervals[cmbIntraDayIntervals.getSelectedIndex()];
        } else {//history
            intPeriod = historyPeriods[cmbHistoryPeriods.getSelectedIndex()];
            longInterval = historyIntervals[cmbHistoryIntervals.getSelectedIndex()];
        }

        intPeriodHistory = historyPeriods[cmbHistoryPeriods.getSelectedIndex()];
        longIntervalHistory = historyIntervals[cmbHistoryIntervals.getSelectedIndex()];
        intPeriodIntraday = intradayPeriods[cmbIntraDayPeriods.getSelectedIndex()];
        longIntervalIntraday = intraDayIntervals[cmbIntraDayIntervals.getSelectedIndex()];

        boolean objShowBidAskTags = chkShowBidAsk.isSelected();
        boolean objShowLastLine = chkShowCurrentPriceLine.isSelected();
        boolean objShowPrvCloseLine = chkShowPreviousCloseLine.isSelected();
        boolean objShowOrderLines = chkShowOrderLine.isSelected();
        boolean objShowMinMaxLines = chkShowMinMax.isSelected();
        boolean objOpenInProCharts = chkAlwaysOpenInProChart.isSelected();
        boolean objShowVWAP = chkShowVWap.isSelected();

        //added by charithn
        int customPeriods;
        int customRange;

        //if user sets his custom values, get it from there. else use the already saved values..
        if (periodSettings != null && periodSettings.length == 2) {
            customPeriods = periodSettings[0];
            customRange = periodSettings[1];

        } else {
            customPeriods = ChartSettings.getSettings().getCustomPeriod();
            customRange = ChartSettings.getSettings().getCustomRange();
        }

        long customIntervalType;
        long customIntervalFactor;

        if (intervalSettings != null) {
            customIntervalType = intervalSettings[0];
            customIntervalFactor = intervalSettings[1];
            longInterval = customIntervalType * customIntervalFactor;

        } else {
            customIntervalFactor = ChartSettings.getSettings().getCustomIntervalFactor();
            customIntervalType = ChartSettings.getSettings().getCustomIntervalType();
        }

        //saves the selected values in the xml file
        ChartSettings cs = ChartSettings.getSettings();
        ChartProperties cp = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_BASE, Color.BLACK, null);
        cp.setOHLCPriority((byte) cmbPriceFieldBase.getSelectedIndex());
        cp.setPenStyle((byte) cmbLineStyleBase.getSelectedIndex());
        Object obj = ((ComboItem) cmbThicknessBase.getSelectedItem()).getValue();
        cp.setPenWidth(Float.parseFloat((String) obj));
        cp.setChartStyle((byte) cmbChartStyleBase.getSelectedIndex());
        cp.setUseSameColor(chkSameColorBase.isSelected());

        cp.setColor(upColorBase);
        if (chkSameColorBase.isSelected()) {
            cp.setWarningColor(upColorBase);
        } else {
            cp.setWarningColor(downColorBase);

        }

        if (!chkUseThemeBase.isSelected()) {
            cp.setUserSaved(true);
        } else {
            cp.setUserSaved(false);
        }

        cs.setProperties(isCurrentMode, intPeriod, longInterval,
                objShowLastLine, objShowPrvCloseLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

        //saving intraday and hidtory defaults
        cs.setIntraHistoryProperties(isCurrentMode, intPeriodIntraday, longIntervalIntraday, intPeriodHistory, longIntervalHistory,
                objShowLastLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

        //save volume graph properties
        cs = ChartSettings.getSettings();
        cp = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_VOLUME, Color.BLACK, null);
        cp.setPenStyle((byte) cmbLineStyleVolume.getSelectedIndex());
        Object ob = ((ComboItem) cmbThicknessVolume.getSelectedItem()).getValue();
        cp.setPenWidth(Float.parseFloat((String) ob));
        cp.setChartStyle((byte) cmbChartStyleVolume.getSelectedIndex());
        cp.setUseSameColor(chkSameColorVolume.isSelected());

        cp.setColor(upColorVolume);
        if (chkSameColorVolume.isSelected()) {
            cp.setWarningColor(upColorVolume);
        } else {
            cp.setWarningColor(downColorVolume);
        }

        if (!chkUseThemeVolume.isSelected()) {
            cp.setUserSaved(true);
        } else {
            cp.setUserSaved(false);
        }

        cp.setOHLCPriority(GraphDataManager.INNER_Close);

        cs.setProperties(isCurrentMode, intPeriod, longInterval,
                objShowLastLine, objShowPrvCloseLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

        //saving intraday and hidtory defaults
        cs.setIntraHistoryProperties(isCurrentMode, intPeriodIntraday, longIntervalIntraday, intPeriodHistory, longIntervalHistory,
                objShowLastLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

        //save turnover graph properties
        cs = ChartSettings.getSettings();
        cp = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_TURNOVER, Color.BLACK, null);
        cp.setPenStyle((byte) cmbLineStyleTurnover.getSelectedIndex());
        ob = ((ComboItem) cmbThicknessTurnover.getSelectedItem()).getValue();
        cp.setPenWidth(Float.parseFloat((String) ob));
        cp.setChartStyle((byte) cmbChartStyleTurnover.getSelectedIndex());
        cp.setUseSameColor(chkSameColorTurnover.isSelected());

        cp.setColor(upColorTurnover);
        if (chkSameColorTurnover.isSelected()) {
            cp.setWarningColor(upColorTurnover);
        } else {
            cp.setWarningColor(downColorTurnover);
        }

        if (!chkUseThemeTurnover.isSelected()) {
            cp.setUserSaved(true);
        } else {
            cp.setUserSaved(false);
        }

        cp.setOHLCPriority(GraphDataManager.INNER_Close);

        cs.setProperties(isCurrentMode, intPeriod, longInterval,
                objShowLastLine, objShowPrvCloseLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

        //saving intraday and hidtory defaults
        cs.setIntraHistoryProperties(isCurrentMode, intPeriodIntraday, longIntervalIntraday, intPeriodHistory, longIntervalHistory,
                objShowLastLine, objShowOrderLines, objShowBidAskTags,
                objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                customIntervalType, customIntervalFactor, objShowVWAP);

    }

    private int getEquivalentWidth(float pw) {
        if (pw <= 1.1f) {
            return 0;
        } else if (pw <= 1.6f) {
            return 1;
        } else if (pw <= 2.1f) {
            return 2;
        } else if (pw <= 3.1f) {
            return 3;
        } else return 4;
    }

    private void loadDefaultChartSettings() {

        try {
            removeListeners();

            ChartSettings cs = ChartSettings.getSettings();
            ChartProperties cpBase = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_BASE, Color.BLACK, null);

            chkUseThemeBase.setSelected(!cpBase.isUserSaved());
            cmbUpBase.setEnabled(cpBase.isUserSaved());
            cmbDownBase.setEnabled(cpBase.isUserSaved());
            chkSameColorBase.setSelected(cpBase.isUsingSameColor());
            chkSameColorBase.setEnabled(cpBase.isUserSaved());
            cmbPriceFieldBase.setSelectedIndex(cpBase.getOHLCPriority());
            cmbLineStyleBase.setSelectedIndex(cpBase.getPenStyle());
            cmbChartStyleBase.setSelectedIndex(cpBase.getChartStyle());

            int equiW = getEquivalentWidth(cpBase.getPenWidth());
            cmbThicknessBase.setSelectedIndex(equiW);

            boolean customColorUp = true;
            boolean customColorDown = true;

            //if (cpBase.isUserSaved()) {
            if (true) {
                for (int i = 0; i < UIHelper.colors.length; i++) {

                    Color up = cpBase.getColor();
                    Color down = cpBase.getWarningColor();
                    if (UIHelper.colors[i].equals(up)) {
                        cmbUpBase.setSelectedIndex(i);
                        customColorUp = false;
                    }
                    if (UIHelper.colors[i].equals(down)) {
                        cmbDownBase.setSelectedIndex(i);
                        customColorDown = false;
                    }
                }

                Color oldUpColor = ChartSettings.getSettings().getCP().getColor();
                Color oldDownColor = ChartSettings.getSettings().getCP().getWarningColor();

                if (customColorUp) {
                    cmbUpBase.removeItemAt(cmbUpBase.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldUpColor, new ComboImage(oldUpColor), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbUpBase.addItem(ci);
                    cmbUpBase.setSelectedIndex(cmbUpBase.getItemCount() - 1);
                }

                if (customColorDown) {
                    cmbDownBase.removeItemAt(cmbDownBase.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldDownColor, new ComboImage(oldDownColor), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbDownBase.addItem(ci);
                    cmbDownBase.setSelectedIndex(cmbDownBase.getItemCount() - 1);
                }
                if (cpBase.isUsingSameColor()) {
                    cmbDownBase.setSelectedIndex(cmbUpBase.getSelectedIndex());
                    cmbDownBase.setEnabled(false);
                }
            }

            rbIntraDay.setSelected(cs.isCurrentMode());
            cmbIntraDayIntervals.setEnabled(cs.isCurrentMode());
            cmbIntraDayPeriods.setEnabled(cs.isCurrentMode());

            rbHistory.setSelected(!cs.isCurrentMode());
            cmbHistoryIntervals.setEnabled(!cs.isCurrentMode());
            cmbHistoryPeriods.setEnabled(!cs.isCurrentMode());

            if (!cs.isCurrentMode()) {
                for (int i = 0; i < historyPeriods.length; i++) {
                    if (historyPeriods[i] == cs.getPeriod()) {
                        cmbHistoryPeriods.setSelectedIndex(i);
                    }
                }
                boolean customInter = true;
                for (int j = 0; j < historyIntervals.length; j++) {
                    if (historyIntervals[j] == cs.getInterval()) {
                        cmbHistoryIntervals.setSelectedIndex(j);
                        customInter = false;
                    }
                }
                if (customInter) {
                    cmbHistoryIntervals.setSelectedIndex(historyIntervals.length - 1);
                }
            } else {
                for (int i = 0; i < intradayPeriods.length; i++) {
                    if (intradayPeriods[i] == cs.getPeriod()) {
                        cmbIntraDayPeriods.setSelectedIndex(i);
                    }
                }
                boolean customInter = true;
                for (int j = 0; j < intraDayIntervals.length; j++) {
                    if (intraDayIntervals[j] == cs.getInterval()) {
                        cmbIntraDayIntervals.setSelectedIndex(j);
                        customInter = false;
                    }
                }
                if (customInter) {
                    cmbIntraDayIntervals.setSelectedIndex(intraDayIntervals.length - 1);
                }
            }

            chkShowBidAsk.setSelected(cs.isShowBidAskTags());
            chkShowCurrentPriceLine.setSelected(cs.isShowLastPriceLine());
            chkShowPreviousCloseLine.setSelected(cs.isShowPreviousCloseLine());
            chkShowOrderLine.setSelected(cs.isShowOrderLines());
            chkShowMinMax.setSelected(cs.isShowMinMaxLines());
            chkAlwaysOpenInProChart.setSelected(cs.isOpenInProChart());

            chkShowVWap.setSelected(cs.isShowVWAP());

            /* if (cs.isCurrentMode()) { //VWAP is diabled in intraday mode
                chkShowVWap.setSelected(false);
                chkShowVWap.setEnabled(false);
            } else {
                chkShowVWap.setSelected(cs.isShowVWAP());
            }*/

            /* ================== load volume chart properties  ========== */

            ChartSettings cs2 = ChartSettings.getSettings();
            ChartProperties cpVolume = cs2.getDefaultCP(null, "", GraphDataManager.ID_VOLUME, Color.BLACK, null);

            customColorUp = true;
            customColorDown = true;

            chkUseThemeVolume.setSelected(!cpVolume.isUserSaved());
            cmbUpVolume.setEnabled(cpVolume.isUserSaved());
            cmbDownVolume.setEnabled(cpVolume.isUserSaved());
            chkSameColorVolume.setSelected(cpVolume.isUsingSameColor());
            chkSameColorVolume.setEnabled(cpVolume.isUserSaved());
            cmbChartStyleVolume.setSelectedIndex(cpVolume.getChartStyle());
            if (cpVolume.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {
                cmbLineStyleVolume.setEnabled(true);
                cmbLineStyleVolume.setSelectedIndex(cpVolume.getPenStyle());

                cmbThicknessVolume.setEnabled(true);
                int width = getEquivalentWidth(cpVolume.getPenWidth());
                cmbThicknessVolume.setSelectedIndex(width);
            } else {
                cmbLineStyleVolume.setEnabled(false);
                cmbThicknessVolume.setEnabled(false);
            }

            //if (cpVolume.isUserSaved()) {
            if (true) {
                for (int i = 0; i < UIHelper.colors.length; i++) {

                    Color up = cpVolume.getColor();
                    Color down = cpVolume.getWarningColor();
                    if (UIHelper.colors[i].equals(up)) {
                        cmbUpVolume.setSelectedIndex(i);
                        customColorUp = false;
                    }
                    if (UIHelper.colors[i].equals(down)) {
                        cmbDownVolume.setSelectedIndex(i);
                        customColorDown = false;
                    }
                }

                Color oldUpColor2 = ChartSettings.getSettings().getVolumeCP().getColor();
                Color oldDownColor2 = ChartSettings.getSettings().getVolumeCP().getWarningColor();

                if (customColorUp) {
                    cmbUpVolume.removeItemAt(cmbUpVolume.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldUpColor2, new ComboImage(oldUpColor2), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbUpVolume.addItem(ci);
                    cmbUpVolume.setSelectedIndex(cmbUpVolume.getItemCount() - 1);
                }

                if (customColorDown) {
                    cmbDownVolume.removeItemAt(cmbDownVolume.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldDownColor2, new ComboImage(oldDownColor2), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbDownVolume.addItem(ci);
                    cmbDownVolume.setSelectedIndex(cmbDownVolume.getItemCount() - 1);
                }
                if (cpVolume.isUsingSameColor()) {
                    cmbDownVolume.setSelectedIndex(cmbUpVolume.getSelectedIndex());
                    cmbDownVolume.setEnabled(false);
                }
            }

/* ================== load turnover chart properties  ========== */

            ChartSettings cs3 = ChartSettings.getSettings();
            ChartProperties cpTurnover = cs3.getDefaultCP(null, "", GraphDataManager.ID_TURNOVER, Color.BLACK, null);

            customColorUp = true;
            customColorDown = true;

            chkUseThemeTurnover.setSelected(!cpTurnover.isUserSaved());
            cmbUpTurnover.setEnabled(cpTurnover.isUserSaved());
            cmbDownTurnover.setEnabled(cpTurnover.isUserSaved());
            chkSameColorTurnover.setSelected(cpTurnover.isUsingSameColor());
            chkSameColorTurnover.setEnabled(cpTurnover.isUserSaved());
            cmbChartStyleTurnover.setSelectedIndex(cpTurnover.getChartStyle());
            if (cpTurnover.getChartStyle() == StockGraph.INT_GRAPH_STYLE_LINE) {
                cmbLineStyleTurnover.setEnabled(true);
                cmbLineStyleTurnover.setSelectedIndex(cpTurnover.getPenStyle());

                cmbThicknessTurnover.setEnabled(true);
                int width = getEquivalentWidth(cpTurnover.getPenWidth());
                cmbThicknessTurnover.setSelectedIndex(width);
            } else {
                cmbLineStyleTurnover.setEnabled(false);
                cmbThicknessTurnover.setEnabled(false);
            }

            //if (cpVolume.isUserSaved()) {
            if (true) {
                for (int i = 0; i < UIHelper.colors.length; i++) {

                    Color up = cpTurnover.getColor();
                    Color down = cpTurnover.getWarningColor();
                    if (UIHelper.colors[i].equals(up)) {
                        cmbUpTurnover.setSelectedIndex(i);
                        customColorUp = false;
                    }
                    if (UIHelper.colors[i].equals(down)) {
                        cmbDownTurnover.setSelectedIndex(i);
                        customColorDown = false;
                    }
                }

                Color oldUpColor2 = ChartSettings.getSettings().getTurnoverCP().getColor();
                Color oldDownColor2 = ChartSettings.getSettings().getTurnoverCP().getWarningColor();

                if (customColorUp) {
                    cmbUpTurnover.removeItemAt(cmbUpTurnover.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldUpColor2, new ComboImage(oldUpColor2), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbUpTurnover.addItem(ci);
                    cmbUpTurnover.setSelectedIndex(cmbUpTurnover.getItemCount() - 1);
                }

                if (customColorDown) {
                    cmbDownTurnover.removeItemAt(cmbDownTurnover.getItemCount() - 1);
                    ComboItem ci = new ComboItem(Language.getString("CUSTOM"), oldDownColor2, new ComboImage(oldDownColor2), ComboItem.ITEM_CUSTOM_COLOR);
                    cmbDownTurnover.addItem(ci);
                    cmbDownTurnover.setSelectedIndex(cmbDownTurnover.getItemCount() - 1);
                }
                if (cpTurnover.isUsingSameColor()) {
                    cmbDownTurnover.setSelectedIndex(cmbUpTurnover.getSelectedIndex());
                    cmbDownTurnover.setEnabled(false);
                }
            }

            tabPaneChartProperties.setSelectedIndex(0);
            addListeners();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ThemeData extractTheme() {

        ThemeData data = new ThemeData();
        data.GraphAreaLeft = clblBackgroundL.getBackground();
        data.GraphAreaRight = clblBackgroundR.getBackground();
        data.AxisColor = clblAxis.getBackground();
        data.FontColor = clblFont.getBackground();
        data.CrosshairsColor = clblCrossHairs.getBackground();
        data.GridColor = clblGrid.getBackground();
        data.AxisBGInner = clblAxisBgInner.getBackground();
        data.AxisBGOuter = clblAxisBgOuter.getBackground();
        data.YearSeperator = clblYearSeperator.getBackground();
        data.TitleBGLeft = clblTitleBgL.getBackground();
        data.TitleBGRight = clblTitleBgR.getBackground();
        data.TitleForeground = clblTitleFg.getBackground();
        data.SelectedColor = clblSelected.getBackground();
        data.TooltipBackground = clblToolTipBg.getBackground();
        data.TooltipForeground = clblToolTipFg.getBackground();
        data.TooltipBorderColor = clblToolTipBorder.getBackground();
        data.DragZoomColor = clblDragZoom.getBackground();
        data.AxisFont = clblAxisFont.getFont();
        data.LegendFont = clblLegendFont.getFont();
        data.TitleFont = clblTitleFont.getFont();
        data.ToolTipFont = clblToolTipFont.getFont();

        return data;
    }

    public OptionData extractOptionData() {

        OptionData data = new OptionData();

        /* ====== extract chart settings here ====== */

        //chart main titile
        data.chart_main_title[0] = chkMode.isSelected();
        data.chart_main_title[1] = chkPeriod.isSelected();
        data.chart_main_title[2] = chkInterval.isSelected();

        //chart legend style
        if (rbSymbol.isSelected()) {
            data.chart_legend_style = ChartOptions.LegendStyle.SYMBOL;
        } else if (rbCompanyName.isSelected()) {
            data.chart_legend_style = ChartOptions.LegendStyle.COMPANY_NAME;
        }

        //Y-Axis position
        if (rbLeft.isSelected()) {
            data.chart_y_axis_poition = ChartOptions.YAxisPosition.LEFT_ONLY;
        } else if (rbRight.isSelected()) {
            data.chart_y_axis_poition = ChartOptions.YAxisPosition.RIGHT_ONLY;
        } else if (rbBoth.isSelected()) {
            data.chart_y_axis_poition = ChartOptions.YAxisPosition.BOTH;
        }

        byte mode = ChartFrame.TILE_NORMALLY;
        //window tile mode
        if (rbTileNormal.isSelected()) {
            data.chart_window_tile = ChartOptions.WindowTileMode.TILE_NORMAL;
        } else if (rbTabbed.isSelected()) {
            data.chart_window_tile = ChartOptions.WindowTileMode.TABBED;
            mode = ChartFrame.TABBED;
        } else if (rbTileHor.isSelected()) {
            data.chart_window_tile = ChartOptions.WindowTileMode.TILE_HORIZONTAL;
            mode = ChartFrame.TILE_HORIZONTALLY;
        } else if (rbTileVer.isSelected()) {
            data.chart_window_tile = ChartOptions.WindowTileMode.TILE_VERTICAL;
            mode = ChartFrame.TILE_VERTICALLY;
        }
        //ChartFrame.getSharedInstance().setCurrentArrangeMode(mode);
        //ChartFrame.getSharedInstance().tileWindows();


        //Right margin
        data.chart_right_margin_history = (Integer) spHistory.getValue();
        data.chart_right_margin_intraday = (Integer) spIntraDay.getValue();

        /* ====== extract save settings here ====== */

        data.save_image_type = ChartOptions.ImageType.valueOf(cmbImageType.getSelectedItem().toString());

        //symbol name on tree view

        if (rbSymbolMisc.isSelected()) {
            data.misc_symbol_Name = ChartOptions.SymbolNameOptions.SYMBOL_ONLY;
        } else if (rbSymbolShortName.isSelected()) {
            data.misc_symbol_Name = ChartOptions.SymbolNameOptions.SHORT_NAME;
        } else if (rbSymbolLongName.isSelected()) {
            data.misc_symbol_Name = ChartOptions.SymbolNameOptions.LONG_NAME;
        } else if (rbSymbolWithShortName.isSelected()) {
            data.misc_symbol_Name = ChartOptions.SymbolNameOptions.SYMBOL_AND_SHORT_NAME;
        } else if (rbSymbolWithLongName.isSelected()) {
            data.misc_symbol_Name = ChartOptions.SymbolNameOptions.SYMBOL_AND_LONG_NAME;
        }

        //add line studies
        if (rbShiftKey.isSelected()) {
            data.gen_add_studies = ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT;
        } else if (rbContinueAdding.isSelected()) {
            data.gen_add_studies = ChartOptions.AddStudiesOption.ALWAYS_CONTINUE;
        } else if (rbSingleObject.isSelected()) {
            data.gen_add_studies = ChartOptions.AddStudiesOption.ALWAYS_SINGLE;
        }

        //deleting chart elements
        data.gen_confirm_del_indicator = chkConfirmDeletingIndicator.isSelected();
        data.gen_confirm_del_chart = chkConfirmDeletingChart.isSelected();
        data.gen_show_tool_tip = chkShowToolTip.isSelected();
        data.gen_is_free_style = chkFreeStyle.isSelected();
        data.gen_is_shw_last_ind_val = chkShwLastInd.isSelected();

        if (rbCustomColors.isSelected()) {
            data.chart_color_preferences = ChartOptions.ChartColorPreferences.CUSTOM_COLORS;
        } else if (rbDefault.isSelected()) {
            data.chart_color_preferences = ChartOptions.ChartColorPreferences.DEFAULT_COLORS;
        } else if (rbThemeColors.isSelected()) {
            data.chart_color_preferences = ChartOptions.ChartColorPreferences.THEME_COLORS;
        }
        return data;

    }

    public void loadOptionData(OptionData data) {

        /* ====== load chart settings here ====== */

        //chart main title
        chkMode.setSelected(data.chart_main_title[0]);
        chkPeriod.setSelected(data.chart_main_title[1]);
        chkInterval.setSelected(data.chart_main_title[2]);

        //legend style
        if (data.chart_legend_style == ChartOptions.LegendStyle.COMPANY_NAME) {
            rbCompanyName.setSelected(true);
        } else if (data.chart_legend_style == ChartOptions.LegendStyle.SYMBOL) {
            rbSymbol.setSelected(true);
        }

        //Y-Axis position
        if (data.chart_y_axis_poition == ChartOptions.YAxisPosition.LEFT_ONLY) {
            rbLeft.setSelected(true);
        } else if (data.chart_y_axis_poition == ChartOptions.YAxisPosition.RIGHT_ONLY) {
            rbRight.setSelected(true);
        } else if (data.chart_y_axis_poition == ChartOptions.YAxisPosition.BOTH) {
            rbBoth.setSelected(true);
        }

        //Window tile mode
        if (data.chart_window_tile == ChartOptions.WindowTileMode.TILE_NORMAL) {
            rbTileNormal.setSelected(true);
        } else if (data.chart_window_tile == ChartOptions.WindowTileMode.TABBED) {
            rbTabbed.setSelected(true);
        } else if (data.chart_window_tile == ChartOptions.WindowTileMode.TILE_HORIZONTAL) {
            rbTileHor.setSelected(true);
        } else if (data.chart_window_tile == ChartOptions.WindowTileMode.TILE_VERTICAL) {
            rbTileVer.setSelected(true);
        }

        //right margin
        spHistory.setValue(data.chart_right_margin_history);
        spIntraDay.setValue(data.chart_right_margin_intraday);

        /* ====== load save settings here ====== */
        for (int i = 0; i < ChartConstants.IMAGE_EXTENSIONS.length; i++) {
            if (data.save_image_type.toString() == (String) cmbImageType.getItemAt(i)) {
                cmbImageType.setSelectedIndex(i);
                break;
            }
        }

        /* ====== general settings here ====== */
        //symbol name on tree view
        if (data.misc_symbol_Name == ChartOptions.SymbolNameOptions.SYMBOL_ONLY) {
            rbSymbolMisc.setSelected(true);
        } else if (data.misc_symbol_Name == ChartOptions.SymbolNameOptions.SHORT_NAME) {
            rbSymbolShortName.setSelected(true);
        } else if (data.misc_symbol_Name == ChartOptions.SymbolNameOptions.LONG_NAME) {
            rbSymbolLongName.setSelected(true);
        } else if (data.misc_symbol_Name == ChartOptions.SymbolNameOptions.SYMBOL_AND_SHORT_NAME) {
            rbSymbolWithShortName.setSelected(true);
        } else if (data.misc_symbol_Name == ChartOptions.SymbolNameOptions.SYMBOL_AND_LONG_NAME) {
            rbSymbolWithLongName.setSelected(true);
        }

        //add line studies
        if (data.gen_add_studies == ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT) {
            rbShiftKey.setSelected(true);
        } else if (data.gen_add_studies == ChartOptions.AddStudiesOption.ALWAYS_CONTINUE) {
            rbContinueAdding.setSelected(true);
        } else if (data.gen_add_studies == ChartOptions.AddStudiesOption.ALWAYS_SINGLE) {
            rbSingleObject.setSelected(true);
        }

        if (data.chart_color_preferences == ChartOptions.ChartColorPreferences.CUSTOM_COLORS) {
            rbCustomColors.setSelected(true);
        } else if (data.chart_color_preferences == ChartOptions.ChartColorPreferences.DEFAULT_COLORS) {
            rbDefault.setSelected(true);
        } else if (data.chart_color_preferences == ChartOptions.ChartColorPreferences.THEME_COLORS) {
            rbThemeColors.setSelected(true);
        }

        //deleting chart elements
        chkConfirmDeletingIndicator.setSelected(data.gen_confirm_del_indicator);
        chkShowToolTip.setSelected(data.gen_show_tool_tip);
        chkConfirmDeletingChart.setSelected(data.gen_confirm_del_chart);
        chkFreeStyle.setSelected(data.gen_is_free_style);
        chkShwLastInd.setSelected(data.gen_is_shw_last_ind_val);

        if ((data.chart_color_preferences != ChartOptions.ChartColorPreferences.CUSTOM_COLORS)) {
            enableDisableThemeColrPanel(false);
        }
    }

    private String getLabelText(Color color) {

        int rin = color.getRed();
        int gin = color.getGreen();
        int bin = color.getBlue();

        return (rin + ", " + gin + ", " + bin);

    }

    private void setLabelColors(CustomColorLabel lbl, Color c) {

        lbl.setBackground(c);
        lbl.setForeground(getInverseColor(c));
        lbl.setText(getLabelText(c));
    }

    private void setLabelFont(CustomFontLabel lbl, Font font) {

        String fontName = font.getName();
        int fontSize = font.getSize();
        int style = font.getStyle();

        String fontStyle = "";

        switch (style) {
            case 0:
                fontStyle = "Plain";
                break;
            case 1:
                fontStyle = "Bold";
                break;
            case 2:
                fontStyle = "Italic";
                break;
        }

        lbl.setFont(font);
        String text = fontName + ", " + fontSize + ", " + fontStyle;
        lbl.setText(text);
        lbl.setToolTipText(text);
    }

    private Color getInverseColor(Color color) {

        int rin = color.getRed();
        int gin = color.getGreen();
        int bin = color.getBlue();
        int rout = 255 - rin;
        int gout = 255 - gin;
        int bout = 255 - bin;

        return new Color(rout, gout, bout);
    }

    public void loadChartTheme(ThemeData themedata) {

        setLabelColors(clblBackgroundL, themedata.GraphAreaLeft);
        setLabelColors(clblBackgroundR, themedata.GraphAreaRight);
        setLabelColors(clblAxis, themedata.AxisColor);
        setLabelColors(clblFont, themedata.FontColor);
        setLabelColors(clblAxisBgInner, themedata.AxisBGInner);
        setLabelColors(clblAxisBgOuter, themedata.AxisBGOuter);
        setLabelColors(clblYearSeperator, themedata.YearSeperator);
        setLabelColors(clblGrid, themedata.GridColor);
        setLabelColors(clblTitleBgL, themedata.TitleBGLeft);
        setLabelColors(clblTitleBgR, themedata.TitleBGRight);
        setLabelColors(clblTitleFg, themedata.TitleForeground);
        setLabelColors(clblSelected, themedata.SelectedColor);
        setLabelColors(clblToolTipBg, themedata.TooltipBackground);
        setLabelColors(clblToolTipFg, themedata.TooltipForeground);
        setLabelColors(clblToolTipBorder, themedata.TooltipBorderColor);
        setLabelColors(clblCrossHairs, themedata.CrosshairsColor);
        setLabelColors(clblDragZoom, themedata.DragZoomColor);

        //font
        setLabelFont(clblAxisFont, themedata.AxisFont);
        setLabelFont(clblTitleFont, themedata.TitleFont);
        setLabelFont(clblLegendFont, themedata.LegendFont);
        setLabelFont(clblToolTipFont, themedata.ToolTipFont);
    }

    public void applyTheme() {
        this.setBackground(Theme.getColor("BACKGROUND"));
    }

    public void windowOpened(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowClosing(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        ChartFrame.getSharedInstance().applyCustomColors(ChartTheme.getCustomThemeDataToApply());
        loadOptionData(ChartOptions.getCurrentOptions());
        loadDefaultChartSettings();
        destroyChartOptionsWindow();
    }

    public void windowClosed(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public CustomDialog getCustomTimePeriodsDialog(int period, int range, boolean isIntraday) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SET_CUSTOM_TIME_PERIODS"));
        final int[] periodSettings = new int[2];

        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(285, 40));
        String[] PanelWidths = {"10", "90", "10", "75", "10", "75", "10"};
        String[] PanelHeights = {"10", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        pnl.setLayout(flexGridLayout);

        JLabel lblCustomPeriods = new JLabel(Language.getString("CUSTOM_TIME_PERIOD"));
        lblCustomPeriods.setBounds(new Rectangle(10, 10, 250, 20));
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblCustomPeriods);
        pnl.add(new JLabel());

        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(range, 1, Integer.MAX_VALUE, 1);
        final JSpinner rangeField = new JSpinner(spinnerNumberModel);
        rangeField.setBounds(new Rectangle(10, 35, 110, 30));
        pnl.add(rangeField);

        final JComboBox periodType = new JComboBox();
        if (isIntraday) {
            periodType.addItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_HOURS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_DAYS"));
        } else {
            periodType.addItem(Language.getString("CUSTOM_PERIOD_DAYS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_YEARS"));
        }
        periodType.setBounds(new Rectangle(130, 35, 110, 30));
        pnl.add(new JLabel());
        pnl.add(periodType);
        pnl.add(new JLabel());


        switch (period) {
            case Calendar.MINUTE:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
                break;
            case Calendar.HOUR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_HOURS"));
                break;
            case Calendar.DAY_OF_YEAR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
                break;
            case Calendar.MONTH:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
                break;
            case Calendar.YEAR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_YEARS"));
                break;
            default:
                if (isIntraday) {
                    periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
                } else {
                    periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
                }
                break;
        }

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedItem = (String) periodType.getSelectedItem();
                if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MINUTES"))) {
                    periodSettings[0] = Calendar.MINUTE;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_HOURS"))) {
                    periodSettings[0] = Calendar.HOUR;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_DAYS"))) {
                    periodSettings[0] = Calendar.DAY_OF_YEAR;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MONTHS"))) {
                    periodSettings[0] = Calendar.MONTH;
                } else {
                    periodSettings[0] = Calendar.YEAR;
                }
                try {
                    periodSettings[1] = ((SpinnerNumberModel) rangeField.getModel()).getNumber().intValue();
                    cdlg.result = periodSettings;
                } catch (NumberFormatException e1) {
                    cdlg.result = null;
                }
            }
        });

        GUISettings.applyOrientation(cdlg);
        return cdlg;
    }

    public CustomDialog getCustomIntervalsDialog(long intervalType, long factor, boolean isIntraday) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SET_CUSTOM_INTERVALS"));
        final long[] periodSettings = new long[2];

        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(285, 40));
        String[] PanelWidths = {"5", "100", "5", "75", "10", "75", "10"};
        String[] PanelHeights = {"10", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        pnl.setLayout(flexGridLayout);

        JLabel lblCustomPeriods = new JLabel(Language.getString("CUSTOM_INTERVALS"));
        lblCustomPeriods.setBounds(new Rectangle(10, 10, 250, 20));
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblCustomPeriods);
        pnl.add(new JLabel());
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(factor, 1, Long.MAX_VALUE, 1);
        final JSpinner rangeField = new JSpinner(spinnerNumberModel);
        rangeField.setBounds(new Rectangle(10, 35, 110, 30));
        pnl.add(rangeField);

        final JComboBox periodType = new JComboBox();
        if (isIntraday) {
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_MINUTES"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_HOURS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_DAYS"));
            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_MONTHS"));
            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_YEARS"));
        } else {
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_DAYS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_MONTHS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_YEARS"));
        }
        periodType.setBounds(new Rectangle(130, 35, 110, 30));
        pnl.add(new JLabel());
        pnl.add(periodType);
        pnl.add(new JLabel());


        if (intervalType == 60L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
        } else if (intervalType == 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_HOURS"));
        } else if (intervalType == 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
        } else if (intervalType == 7 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
        } else if (intervalType == 30 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
        } else if (intervalType == 365 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_YEARS"));
        } else {
            if (isIntraday) {
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
            } else {
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
            }
        }

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedItem = (String) periodType.getSelectedItem();
                if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MINUTES"))) {
                    periodSettings[0] = 60L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_HOURS"))) {
                    periodSettings[0] = 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_DAYS"))) {
                    periodSettings[0] = 24 * 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_INTERVAL_WEEKS"))) {
                    periodSettings[0] = 7 * 24 * 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MONTHS"))) {
                    periodSettings[0] = 30 * 24 * 3600L;
                } else {
                    periodSettings[0] = 365 * 24 * 3600L;
                }
                try {
                    periodSettings[1] = ((SpinnerNumberModel) rangeField.getModel()).getNumber().intValue();
                    cdlg.result = periodSettings;
                    ChartSettings.getSettings().setCustomIntervalType(periodSettings[0]);
                } catch (NumberFormatException e1) {
                    cdlg.result = null;
                }
            }
        });
        GUISettings.applyOrientation(cdlg);
        return cdlg;
    }

    public class GradientButton extends JButton {
        private Color color1;
        private Color color2;

        public GradientButton() {
            this(Color.WHITE, Color.LIGHT_GRAY);
        }

        public GradientButton(Color c1, Color c2) {
            this.color1 = c1;
            this.color2 = c2;
            setContentAreaFilled(false);
        }

        public void setColor1(Color c1) {
            this.color1 = c1;
            repaint();
        }

        public void setColor2(Color c2) {
            this.color2 = c2;
            repaint();
        }

        public void updateButtonColors(Color c1, Color c2) {
            this.color1 = c1;
            this.color2 = c2;
            repaint();
        }

        // Overloaded in order to paint the background
        protected void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int w = getWidth();
            int h = getHeight();
            GradientPaint gradient = new GradientPaint(0, 0, color1, w, 0, color2, false);
            g2.setPaint(gradient);
            g2.fillRect(0, 0, w, h);
            super.paintComponent(g);
        }
    }

    public class CustomColorLabel extends JLabel implements MouseMotionListener, MouseListener {

        ActionListener okActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("OK Button");
                setLabelVisuals(colorChooser.getColor());
            }
        };
        ActionListener cancelActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("Cancel Button");
            }
        };
        private JColorChooser colorChooser = new JColorChooser(Color.BLUE);

        public CustomColorLabel(Color col) {

            super("", JLabel.CENTER);
            this.addMouseMotionListener(this);
            this.addMouseListener(this);
            this.setOpaque(true);
            this.setBackground(col);
            this.setLabelText(col);
            this.setForeground(getInverseColor(col));
            this.setPreferredSize(new Dimension(50, 10));
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        }

        public void mouseDragged(MouseEvent e) {

        }

        public void mouseMoved(MouseEvent e) {

        }

        private void setLabelVisuals(Color color) {

            this.setBackground(color);
            this.setForeground(getInverseColor(color));
            setLabelText(color);
            this.updateUI();
        }

        private void setLabelText(Color color) {

            int rin = color.getRed();
            int gin = color.getGreen();
            int bin = color.getBlue();

            String text = rin + ", " + gin + ", " + bin;
            this.setText(text);
        }

        private Color getInverseColor(Color color) {

            int rin = color.getRed();
            int gin = color.getGreen();
            int bin = color.getBlue();
            int rout = 255 - rin;
            int gout = 255 - gin;
            int bout = 255 - bin;

            return new Color(rout, gout, bout);
        }

        public void mouseClicked(MouseEvent e) {

            if (isEnabled()) {
                JDialog colorDialog = JColorChooser.createDialog(this, "pick color", true, colorChooser, okActionListener, cancelActionListener);
                colorDialog.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
                int x = (int) this.getLocationOnScreen().getX() + 40;
                int y = (int) this.getLocationOnScreen().getY() + 20;

                colorDialog.setLocationRelativeTo(ChartOptionsWindow.getTempInstance());
                colorDialog.setVisible(true);
            }
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        public void mouseExited(MouseEvent e) {
        }
    }

    public class CustomFontLabel extends JLabel implements MouseMotionListener, MouseListener {

        ActionListener okActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("OK Button");
            }
        };
        ActionListener cancelActionListener = new ActionListener() {

            public void actionPerformed(ActionEvent actionEvent) {
                //System.out.println("Cancel Button");
            }
        };
        private CustomFontChooser customFontChooser = new CustomFontChooser(this, true);
        private Font font;

        public CustomFontLabel(Font font) {

            super("", JLabel.CENTER);
            this.addMouseMotionListener(this);
            this.addMouseListener(this);
            this.setOpaque(true);
            this.setPreferredSize(new Dimension(50, 10));
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            //this.setLabelText(font);
            this.setToolTipText(this.getText());
            this.font = font;
            //customFontChooser.setLocationRelativeTo(ChartOptionsWindow.getTempInstance());

        }

        public CustomFontLabel() {

            super("", JLabel.CENTER);
            this.addMouseMotionListener(this);
            this.addMouseListener(this);
            this.setOpaque(true);
            this.setPreferredSize(new Dimension(50, 10));
            this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            this.setToolTipText(this.getText());
        }

        public void mouseDragged(MouseEvent e) {

        }

        public void mouseMoved(MouseEvent e) {

        }

        private void setLabelText(Font font) {

            String fontName = font.getName();
            int fontSize = font.getSize();
            int style = font.getStyle();

            String fontStyle = "";

            switch (style) {
                case 0:
                    fontStyle = "Plain";
                    break;
                case 1:
                    fontStyle = "Bold";
                    break;
                case 2:
                    fontStyle = "Italic";
                    break;
            }

            String text = fontName + ", " + fontSize + ", " + fontStyle;
            this.setText(text);
            this.setToolTipText(text);
            this.setFont(new Font(fontName, style, fontSize));
        }

        public void mouseClicked(MouseEvent e) {

            String text = this.getText();
            String[] delims = text.split(",");
            String style = delims[2].trim();
            int s = 0;
            if (style.equals("Plain")) {
                s = Font.PLAIN;
            } else if (style.equals("Bold")) {
                s = Font.BOLD;
            } else if (style.equals("Italic")) {
                s = Font.ITALIC;
            }
            Font f = new Font(delims[0], s, Integer.parseInt(delims[1].trim()));
            customFontChooser.setLocationRelativeTo(ChartOptionsWindow.getTempInstance());
            //Font font = customFontChooser.showDialog(Language.getString("CO_SELECT_FONT"), new Font("Arial", 1, 12));
            Font fontText = customFontChooser.showDialog(Language.getString("CO_SELECT_FONT"), f);
            setLabelText(fontText);
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        }

        public void mouseExited(MouseEvent e) {
        }
    }

    class CustomDialog extends JDialog implements ActionListener {
        public TWButton btnOK = new TWButton(Language.getString("OK"));
        public TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        public Object result;
        private boolean ModalResult = false;
        private JPanel bottomPanel = new JPanel();
        private JPanel centerPanel = new JPanel();

        public CustomDialog() throws HeadlessException {
            super(optWindowChart);
            setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
            initiallizeUI();
            this.setSize(new Dimension(320, 250));

        }

        private void initiallizeUI() {
            btnOK.addActionListener(this);
            btnCancel.addActionListener(this);
            String[] PanelWidths = {"100%", "75", "10", "75", "10"};
            String[] PanelHeights = {"20"};
            FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
            bottomPanel.setLayout(flexGridLayout);
            bottomPanel.setPreferredSize(new Dimension(0, 30));
            bottomPanel.add(new JLabel());
            bottomPanel.add(btnOK);
            bottomPanel.add(new JLabel());
            bottomPanel.add(btnCancel);
            bottomPanel.add(new JLabel());
            this.getContentPane().setLayout(new BorderLayout());
            this.getContentPane().add(centerPanel, BorderLayout.CENTER);
            this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
            ModalResult = false;
        }

        public void actionPerformed(ActionEvent e) {
            Object obj = e.getSource();
            ModalResult = false;
            if (obj == btnOK) {
                ModalResult = true;
                this.setVisible(false);
            } else if (obj == btnCancel) {
                this.setVisible(false);
            }
        }

        public JPanel getPanel() {
            return centerPanel;
        }

        public boolean getModalResult() {
            return ModalResult;
        }

    }

}