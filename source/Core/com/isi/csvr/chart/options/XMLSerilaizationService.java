package com.isi.csvr.chart.options;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;


/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 25, 2008
 * Time: 11:07:06 AM
 * To change this template use File | Settings | File Templates.
 */


public class XMLSerilaizationService {

    //XML element tags
    public static final String ROOT_ELEMENT_PROCHART = "ProChart";
    //    public static final String ROOT_ELEMENT_THEME = "CustomDataItems";
    public static final String ROOT_ELEMENT_THEME = "CustomDataItems";


    public static final String CUSTOM_DATA_ITEM = "CustomDataItem";
    public static final String CHART_SETTINGS = "ChartSettings";
    public static final String GENERAL_SETTINGS = "GeneralSettings";
    public static final String OTHER_SETTINGS = "OtherSettnigs";

    public static final String MAIN_TITILE = "MainTitle";
    public static final String LEGEND_STYLE = "LegendStyle";
    public static final String Y_AXIS = "Y-AxisPosition";
    public static final String RIGHT_MARGIN_HISTORY = "RightMarginHistory";
    public static final String RIGHT_MARGIN_INTRADAY = "RightMarginIntraDay";
    public static final String ADD_STUDIES = "AddStudies";
    public static final String CONFIRM_CHART_DLETE = "ConfirmChartDelete";
    public static final String CONFIRM_INDICATOR_DELETE = "ConfirmIndicatorDelete";
    public static final String SHOW_TOOLTIP = "ShowTooltip";
    public static final String IS_FREE_STYLE = "IsFreeStyle";
    public static final String IS_SHOW_LAST_IND_VAL = "IsShowLastIndVal";
    public static final String SYMBOL_NAME = "SymbolNameType";
    public static final String IMG_TYPE = "ImageType";
    public static final String WINDOW_TILE_MODE = "TileMode";

    public static final String MODE = "MODE";
    public static final String PERIOD = "PERIOD";
    public static final String INTERVAL = "INTERVAL";

    public static final String CHART_COLOR_PREFERENCE = "ColorPreference";

    public XMLSerilaizationService() {
        //serializeOptionDataObject(new OptionData(), ChartOptions.CHART_OPTION_DATA_PATH);
    }

    public synchronized OptionData deserializeOptionData(String filePath) {

        OptionData data = new OptionData();

        /* File file = new File(filePath);
        if (!file.exists()) {
            return data;
        }*/
        File file = new File(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH);
        if (!file.exists()) {
            return data;
        }
        try {
            // Decrypt
            //AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
            //encrypter.constructKeys(ChartConstants.ENCRYPTION_KEY_FILE_PATH);

            //FileOutputStream stream = new FileOutputStream(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH);
            //encrypter.decryptFile(new FileInputStream(filePath), stream);

            //stream.close();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(new File(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH).toURI().toString());

            NodeList list = dom.getElementsByTagName(GENERAL_SETTINGS);
            NodeList generalSettingList = list.item(0).getChildNodes();

            for (int i = 0; i < generalSettingList.getLength(); i++) {

                Element elem = (Element) generalSettingList.item(i);

                if (elem.getTagName().equals(ADD_STUDIES)) {
                    data.gen_add_studies = ChartOptions.AddStudiesOption.valueOf(elem.getTextContent());
                } else if (elem.getTagName().equals(CONFIRM_CHART_DLETE)) {
                    data.gen_confirm_del_chart = Boolean.parseBoolean(elem.getTextContent());
                } else if (elem.getTagName().equals(CONFIRM_INDICATOR_DELETE)) {
                    data.gen_confirm_del_indicator = Boolean.parseBoolean(elem.getTextContent());
                } else if (elem.getTagName().equals(SYMBOL_NAME)) {
                    data.misc_symbol_Name = ChartOptions.SymbolNameOptions.valueOf(elem.getTextContent());
                } else if (elem.getTagName().equals(SHOW_TOOLTIP)) {
                    data.gen_show_tool_tip = Boolean.parseBoolean(elem.getTextContent());
                } else if (elem.getTagName().equals(IS_FREE_STYLE)) {
                    data.gen_is_free_style = Boolean.parseBoolean(elem.getTextContent());
                } else if (elem.getTagName().equals(IS_SHOW_LAST_IND_VAL)) {
                    data.gen_is_shw_last_ind_val = Boolean.parseBoolean(elem.getTextContent());
                }
            }

            list = dom.getElementsByTagName(CHART_SETTINGS);
            NodeList chartSettingsList = list.item(0).getChildNodes();

            for (int i = 0; i < chartSettingsList.getLength(); i++) {

                Element elem = (Element) chartSettingsList.item(i);

                if (elem.getTagName().equals(MAIN_TITILE)) {
                    data.chart_main_title[0] = Boolean.parseBoolean(elem.getAttribute(MODE));
                    data.chart_main_title[1] = Boolean.parseBoolean(elem.getAttribute(PERIOD));
                    data.chart_main_title[2] = Boolean.parseBoolean(elem.getAttribute(INTERVAL));
                } else if (elem.getTagName().equals(LEGEND_STYLE)) {
                    data.chart_legend_style = ChartOptions.LegendStyle.valueOf(elem.getTextContent());
                } else if (elem.getTagName().equals(Y_AXIS)) {
                    data.chart_y_axis_poition = ChartOptions.YAxisPosition.valueOf(elem.getTextContent());
                } else if (elem.getTagName().equals(RIGHT_MARGIN_HISTORY)) {
                    data.chart_right_margin_history = Integer.parseInt(elem.getTextContent());
                } else if (elem.getTagName().equals(RIGHT_MARGIN_INTRADAY)) {
                    data.chart_right_margin_intraday = Integer.parseInt(elem.getTextContent());
                } else if (elem.getTagName().equals(IMG_TYPE)) {
                    data.save_image_type = ChartOptions.ImageType.valueOf(elem.getTextContent());
                } else if (elem.getTagName().equals(WINDOW_TILE_MODE)) {
                    data.chart_window_tile = ChartOptions.WindowTileMode.valueOf(elem.getTextContent());
                }
            }

            list = dom.getElementsByTagName(OTHER_SETTINGS);
            NodeList misSettingsList = list.item(0).getChildNodes();

            for (int i = 0; i < misSettingsList.getLength(); i++) {

                Element elem = (Element) misSettingsList.item(i);
                if (elem.getTagName().equals(CHART_COLOR_PREFERENCE)) {
                    data.chart_color_preferences = ChartOptions.ChartColorPreferences.valueOf(elem.getTextContent());
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("==== deserializeOptionDataObject() =====");
        }

        //deleteTempFile();/TODO:charithn
        //data.printProperties();

        return data;
    }

    public synchronized void serializeOptionData(OptionData optData, String filePath) {

        try {
            //creating an empty XML document
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_PROCHART);
            doc.appendChild(root);

            //add general settings to xml file
            Element generalSettings = doc.createElement(GENERAL_SETTINGS);
            root.appendChild(generalSettings);

            Element generalAdd = doc.createElement(ADD_STUDIES);
            String strAdd = optData.gen_add_studies.toString();
            Text text = doc.createTextNode(strAdd);
            generalSettings.appendChild(generalAdd);
            generalAdd.appendChild(text);

            Element generalConDele = doc.createElement(CONFIRM_CHART_DLETE);
            String strCDel = String.valueOf(optData.gen_confirm_del_chart);
            text = doc.createTextNode(strCDel);
            generalSettings.appendChild(generalConDele);
            generalConDele.appendChild(text);

            Element generalConInDel = doc.createElement(CONFIRM_INDICATOR_DELETE);
            String strCIndDel = String.valueOf(optData.gen_confirm_del_indicator);
            text = doc.createTextNode(strCIndDel);
            generalSettings.appendChild(generalConInDel);
            generalConInDel.appendChild(text);

            Element generalTooltip = doc.createElement(SHOW_TOOLTIP);
            String strToolTip = String.valueOf(optData.gen_show_tool_tip);
            text = doc.createTextNode(strToolTip);
            generalSettings.appendChild(generalTooltip);
            generalTooltip.appendChild(text);

            Element generalFreeStyle = doc.createElement(IS_FREE_STYLE);
            String strFreeStyle = String.valueOf(optData.gen_is_free_style);
            text = doc.createTextNode(strFreeStyle);
            generalSettings.appendChild(generalFreeStyle);
            generalFreeStyle.appendChild(text);

            Element generalShwLastIndVal = doc.createElement(IS_SHOW_LAST_IND_VAL);
            String strShwLastIndVal = String.valueOf(optData.gen_is_shw_last_ind_val);
            text = doc.createTextNode(strShwLastIndVal);
            generalSettings.appendChild(generalShwLastIndVal);
            generalShwLastIndVal.appendChild(text);

            Element sym = doc.createElement(SYMBOL_NAME);
            String strSym = String.valueOf(optData.misc_symbol_Name);
            text = doc.createTextNode(strSym);
            generalSettings.appendChild(sym);
            sym.appendChild(text);

            //add chart settings to xml file
            Element chartSettings = doc.createElement(CHART_SETTINGS);
            root.appendChild(chartSettings);

            Element chartMainTitle = doc.createElement(MAIN_TITILE);

            chartMainTitle.setAttribute(MODE, String.valueOf(optData.chart_main_title[0]));
            chartMainTitle.setAttribute(PERIOD, String.valueOf(optData.chart_main_title[1]));
            chartMainTitle.setAttribute(INTERVAL, String.valueOf(optData.chart_main_title[2]));

            chartSettings.appendChild(chartMainTitle);

            Element legendStyle = doc.createElement(LEGEND_STYLE);
            String legengSty = String.valueOf(optData.chart_legend_style);
            text = doc.createTextNode(legengSty);
            legendStyle.appendChild(text);
            chartSettings.appendChild(legendStyle);

            Element yAxis = doc.createElement(Y_AXIS);
            String strYaxis = optData.chart_y_axis_poition.toString();
            text = doc.createTextNode(strYaxis);
            yAxis.appendChild(text);
            chartSettings.appendChild(yAxis);

            Element rMargin = doc.createElement(RIGHT_MARGIN_HISTORY);
            String strRMargin = String.valueOf(optData.chart_right_margin_history);
            text = doc.createTextNode(strRMargin);
            rMargin.appendChild(text);
            chartSettings.appendChild(rMargin);

            Element rMarginI = doc.createElement(RIGHT_MARGIN_INTRADAY);
            String strRMarginI = String.valueOf(optData.chart_right_margin_intraday);
            text = doc.createTextNode(strRMarginI);
            rMarginI.appendChild(text);
            chartSettings.appendChild(rMarginI);

            Element imgType = doc.createElement(IMG_TYPE);
            String img = optData.save_image_type.toString();
            text = doc.createTextNode(img);
            imgType.appendChild(text);
            chartSettings.appendChild(imgType);

            Element tileType = doc.createElement(WINDOW_TILE_MODE);
            String tile = optData.chart_window_tile.toString();
            text = doc.createTextNode(tile);
            tileType.appendChild(text);
            chartSettings.appendChild(tileType);

            //add other settings to xml file
            Element misSettings = doc.createElement(OTHER_SETTINGS);
            root.appendChild(misSettings);

            Element color = doc.createElement(CHART_COLOR_PREFERENCE);
            String strCol = optData.chart_color_preferences.toString();
            text = doc.createTextNode(strCol);
            color.appendChild(text);
            misSettings.appendChild(color);

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "true");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            // Encrypt
            /*
            FileInputStream inStream = new FileInputStream(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH);
            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
            encrypter.constructKeys(ChartConstants.ENCRYPTION_KEY_FILE_PATH);
            encrypter.encryptFile(inStream, new FileOutputStream(filePath));
            inStream.close();
            deleteTempFile();
            */

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=====XMLSerializationService-serializeOptionDataObject()====");
        }
    }

    public synchronized void serializeCustomDataItems(ArrayList<CustomDataItem> dataItems, String filePath) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_THEME);
            doc.appendChild(root);

            //add general settings to xml file

            for (CustomDataItem item : dataItems) {
                Element ele = doc.createElement(CUSTOM_DATA_ITEM);
                root.appendChild(ele);

                Element eleName = doc.createElement("Name");
                String str = item.getDataName();
                Text text = doc.createTextNode(str);
                ele.appendChild(eleName);
                eleName.appendChild(text);

                Element eleVal = doc.createElement("Value");
                String strVal = String.valueOf(item.getDataValue());

                text = doc.createTextNode(strVal);
                ele.appendChild(eleVal);
                eleVal.appendChild(text);
            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "true");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(filePath);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            // Encrypt
            /*
            FileInputStream inStream = new FileInputStream((ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH));
            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
            encrypter.constructKeys(ChartConstants.ENCRYPTION_KEY_FILE_PATH);
            encrypter.encryptFile(inStream, new FileOutputStream(filePath));
            inStream.close();
            deleteTempFile();
            */

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== XMLSerializationService class-serializeCustomDataItemObjects() ======");
        }

    }

    public synchronized ArrayList<CustomDataItem> deserializeCustomDataItems(String filePath) {

        ArrayList<CustomDataItem> dataItems = new ArrayList<CustomDataItem>();

        File file = new File(filePath);
        if (!file.exists()) {
            return dataItems;
        }
        try {
            // Decrypt
            /*
            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
            encrypter.constructKeys(ChartConstants.ENCRYPTION_KEY_FILE_PATH);
            */

            //FileOutputStream stream = new FileOutputStream(ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH);
            //encrypter.decryptFile(new FileInputStream(filePath), stream);

            //stream.close();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            //Document dom = docBuilder.parse(new File(ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH).toURI().toString());
            Document dom = docBuilder.parse(new File(filePath).toURI().toString());

            NodeList list = dom.getElementsByTagName(CUSTOM_DATA_ITEM);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlName = elem.getElementsByTagName("Name");
                NodeList nlVale = elem.getElementsByTagName("Value");

                String name = nlName.item(0).getTextContent();  //there are only 1 name and value element within the CustomDataItem
                String value = nlVale.item(0).getTextContent(); //there are only 1 name and value element within the CustomDataItem
                CustomDataItem item = new CustomDataItem(name, value);
                dataItems.add(item);
            }
            //deleteTempFile();//TODO:CHARITH
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("=== XMLSerializationService class-deserializeCustomDataItems() ======");
        }

        return dataItems;
    }

    private synchronized void deleteTempFile() {

        File f1 = new File(ChartConstants.CHART_OPTIONS_TEMP_FILE_PATH);
        File f2 = new File(ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH);

        try {
            if (f1.exists()) {
                f1.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("failure deletion of tempory file..");
        }

        try {
            if (f2.exists()) {
                f2.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("failure deletion of tempory file in XMLSerialization service class-deleteTempFile()..");
        }
    }

    /*public static void main(String[] args) throws Exception {

        OptionData data = new OptionData();

        data.chart_legend_style = ChartOptions.LegendStyle.COMPANY_NAME;
        data.chart_right_margin_history = 565;
        data.chart_right_margin_intraday = 222;

        data.gen_add_studies = ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT;
        data.gen_confirm_del_chart = false;
        data.gen_confirm_del_indicator = true;
        data.misc_symbol_Name = ChartOptions.SymbolNameOptions.SYMBOL_AND_LONG_NAME;

        data.save_image_type = ChartOptions.ImageType.bmp;

        data.chart_color_preferences = ChartOptions.ChartColorPreferences.CUSTOM_COLORS;

        XMLSerilaizationService xmlService = new XMLSerilaizationService();

        //xmlService.serializeOptionData(data, ChartConstants.CHART_OPTION_FILE_PATH);
        xmlService.deserializeOptionData(ChartConstants.CHART_OPTION_FILE_PATH);

        ThemeData t = new ThemeData();

        //xmlService.serializeCustomDataItemObjects(t.getFieldsList(), CHART_OPTION_DATA_PATH);
        //xmlService.deserializeCustomDataItems(ChartConstants.CHART_OPTION_DATA_PATH);
    }*/

}
