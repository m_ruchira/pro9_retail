package com.isi.csvr.chart.options;

import com.isi.csvr.shared.TWFont;

import java.awt.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 1, 2008
 * Time: 12:47:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ThemeData {

    //String constatns
    public static final String BACKGROUND_LEFT = "GraphAreaLeft";
    public static final String BACKGROUND_RIGHT = "GraphAreaRight";
    public static final String CROSSHAIR_COLOR = "CrossHairsColor";
    public static final String DRAGZOOM_COLOR = "DragZoomColor";
    public static final String AXIS_COLOR = "AxisColor";
    public static final String FONT_COLOR = "FontColor";
    public static final String AXIS_BG_INNER = "AxisBGInner";
    public static final String AXIS_BG_OUTER = "AxisBGOuter";
    public static final String GRID_COLOR = "GridColor";
    public static final String YEAR_SEPERATOR = "YearSeperator";
    public static final String TITLE_BG_LEFT = "TitleBGLeft";
    public static final String TITLE_BG_RIGHT = "TitleBGRight";
    public static final String TITILE_FOREGROUNG_COLOR = "TitleForground";
    public static final String SELECTED_COLOR = "SelectedColor";
    public static final String TOOLTIP_BACKGROUND_COLOR = "ToolTipBackground";
    public static final String TOOLTIP_FOREGROUND_COLOR = "ToolTipForeground";
    public static final String TOOLTIP_BORDER_COLOR = "ToolTipBorder";
    public static final String ANNOUNCEMNET_FLAG = "AnnoncementFlag";
    public static final String SPLIT_ADJ_FLAG = "SplitAdjFlag";
    public static final String AXIS_FONT = "AxisFont";
    public static final String LEGEND_FONT = "LegendFont";
    public static final String TITLE_FONT = "TitleFont";
    public static final String TOOL_TIP_FONT = "ToolTipFont";
    //basic color settings in chart theme settings in chart options
    public Color GraphAreaLeft = new Color(232, 237, 245);
    public Color GraphAreaRight = Color.WHITE;
    public Color AxisColor = new Color(153, 153, 255);
    public Color FontColor = new Color(0, 0, 0);
    public Color AxisBGInner = new Color(190, 190, 220);
    public Color AxisBGOuter = Color.WHITE;
    public Color GridColor = new Color(200, 200, 210);
    public Color YearSeperator = new Color(255, 128, 0);
    public Color TitleBGLeft = new Color(190, 190, 220);
    public Color TitleBGRight = new Color(250, 150, 150);
    public Color TitleForeground = Color.BLACK;
    public Color SelectedColor = new Color(255, 0, 255);
    public Color TooltipBackground = new Color(235, 235, 255);
    public Color TooltipForeground = Color.BLACK;
    public Color TooltipBorderColor = Color.BLUE;
    public Color CrosshairsColor = new Color(100, 100, 100);
    public Color DragZoomColor = new Color(0, 200, 200);
    public Color AnnouncementFlag = Color.BLUE;
    public Color SplitAdjFlag = new Color(255, 0, 255);
    public Color PanelBackColor = Color.DARK_GRAY;
    public Color TitleInfoColor = Color.BLUE;
    public Color DataWindowBGColor = Color.WHITE;
    //basic font settings in chart theme settings in chart options
    public Font TitleFont = new Font("Tahoma", Font.PLAIN, 11);
    public Font AxisFont = new Font("Arial", Font.PLAIN, 9);
    public Font LegendFont = new Font("Tahoma", Font.PLAIN, 11);
    public Font ToolTipFont = new Font("Tahoma", Font.BOLD, 11);

    public ThemeData() {
    }

    public static ArrayList<CustomDataItem> getCustomDataItems(ThemeData data) {

        ArrayList<CustomDataItem> items = new ArrayList<CustomDataItem>();

        items.add(new CustomDataItem(BACKGROUND_LEFT, data.GraphAreaLeft.getRGB()));
        items.add(new CustomDataItem(BACKGROUND_RIGHT, data.GraphAreaRight.getRGB()));
        items.add(new CustomDataItem(CROSSHAIR_COLOR, data.CrosshairsColor.getRGB()));
        items.add(new CustomDataItem(DRAGZOOM_COLOR, data.DragZoomColor.getRGB()));

        items.add(new CustomDataItem(AXIS_COLOR, data.AxisColor.getRGB()));
        items.add(new CustomDataItem(FONT_COLOR, data.FontColor.getRGB()));
        items.add(new CustomDataItem(AXIS_BG_INNER, data.AxisBGInner.getRGB()));
        items.add(new CustomDataItem(AXIS_BG_OUTER, data.AxisBGOuter.getRGB()));

        items.add(new CustomDataItem(GRID_COLOR, data.GridColor.getRGB()));
        items.add(new CustomDataItem(YEAR_SEPERATOR, data.YearSeperator.getRGB()));
        items.add(new CustomDataItem(TITLE_BG_LEFT, data.TitleBGLeft.getRGB()));
        items.add(new CustomDataItem(TITLE_BG_RIGHT, data.TitleBGRight.getRGB()));

        items.add(new CustomDataItem(TITILE_FOREGROUNG_COLOR, data.TitleForeground.getRGB()));
        items.add(new CustomDataItem(SELECTED_COLOR, data.SelectedColor.getRGB()));
        items.add(new CustomDataItem(TOOLTIP_BACKGROUND_COLOR, data.TooltipBackground.getRGB()));
        items.add(new CustomDataItem(TOOLTIP_FOREGROUND_COLOR, data.TooltipForeground.getRGB()));
        items.add(new CustomDataItem(TOOLTIP_BORDER_COLOR, data.TooltipBorderColor.getRGB()));

        items.add(new CustomDataItem(AXIS_FONT, convertFontToString(data.AxisFont)));
        items.add(new CustomDataItem(LEGEND_FONT, convertFontToString(data.LegendFont)));
        items.add(new CustomDataItem(TITLE_FONT, convertFontToString(data.TitleFont)));
        items.add(new CustomDataItem(TOOL_TIP_FONT, convertFontToString(data.ToolTipFont)));

        return items;
    }

    public static ThemeData getCustomThemeData(ArrayList<CustomDataItem> items) {

        ThemeData data = new ThemeData();

        for (CustomDataItem item : items) {

            if (item.getDataName().equals(BACKGROUND_LEFT)) {
                data.GraphAreaLeft = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(BACKGROUND_RIGHT)) {
                data.GraphAreaRight = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(CROSSHAIR_COLOR)) {
                data.CrosshairsColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(DRAGZOOM_COLOR)) {
                data.DragZoomColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(AXIS_COLOR)) {
                data.AxisColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(FONT_COLOR)) {
                data.FontColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(AXIS_BG_INNER)) {
                data.AxisBGInner = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(AXIS_BG_OUTER)) {
                data.AxisBGOuter = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(GRID_COLOR)) {
                data.GridColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(YEAR_SEPERATOR)) {
                data.YearSeperator = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TITLE_BG_LEFT)) {
                data.TitleBGLeft = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TITLE_BG_RIGHT)) {
                data.TitleBGRight = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TITILE_FOREGROUNG_COLOR)) {
                data.TitleForeground = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(SELECTED_COLOR)) {
                data.SelectedColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TOOLTIP_BACKGROUND_COLOR)) {
                data.TooltipBackground = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TOOLTIP_FOREGROUND_COLOR)) {
                data.TooltipForeground = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(TOOLTIP_BORDER_COLOR)) {
                data.TooltipBorderColor = new Color(Integer.parseInt(item.getDataValue().toString()));
            } else if (item.getDataName().equals(AXIS_FONT)) {
                data.AxisFont = getFontFromString(item.getDataValue().toString());
            } else if (item.getDataName().equals(LEGEND_FONT)) {
                data.LegendFont = getFontFromString(item.getDataValue().toString());
            } else if (item.getDataName().equals(TITLE_FONT)) {
                data.TitleFont = getFontFromString(item.getDataValue().toString());
            } else if (item.getDataName().equals(TOOL_TIP_FONT)) {
                data.ToolTipFont = getFontFromString(item.getDataValue().toString());
            }
        }

        return data;
    }

    //saves the font string as "Arial,8,1" - "fontName,FontSize,fontStyle"
    private static String convertFontToString(Font font) {
        return (font.getName() + "," + font.getSize() + "," + font.getStyle());
    }

    private static TWFont getFontFromString(String strFont) {
        StringTokenizer tokenizer = new StringTokenizer(strFont, ","); //tokenize from the " , "

        String name = tokenizer.nextToken();
        int size = Integer.parseInt(tokenizer.nextToken());
        int style = Integer.parseInt(tokenizer.nextToken());

        return new TWFont(name, style, size);
    }

}
