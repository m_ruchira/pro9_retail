// Copyright (c) 2000 Home

/**
 * A Swing-based dialog class.
 * <P>
 * @author Uditha Nagahawatta
 */

package com.isi.csvr.chart.options;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.StringTokenizer;

/**
 * FontChooser
 *
 * @author Janos Szatmary
 */

public class CustomFontChooser extends javax.swing.JDialog implements ActionListener {

    //java.awt.Frame parent;
    public boolean ok = false;
    String[] styleList = new String[]{Language.getString("PLAIN"),
            Language.getString("BOLD"), Language.getString("ITALIC")};
    String[] sizeList = new String[]
            {"8", "9", "10", "11", "12",};
    String currentFont = null;
    int currentStyle = -1;
    int currentSize = -1;
    // Variables declaration - do not modify
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField jFont;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList jFontList;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField jStyle;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList jStyleList;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField jSize;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList jSizeList;
    private javax.swing.JPanel jPanel1;
    //private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel jSample;
    private javax.swing.JPanel jButtons;
    private TWButton jOk;
    private TWButton jCancel;
    private javax.swing.JLabel jLabel6;

    public CustomFontChooser(Component parent, boolean modal) {
        super((Window) SwingUtilities.getAncestorOfClass(Window.class, parent), Dialog.ModalityType.APPLICATION_MODAL);
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        //this.parent = parent;
        initComponents();
//        Image im = Toolkit.getDefaultToolkit().getImage("images\\Common\\ClientServer.gif");
//        setIconImage(im);
        setListValues(jFontList,
                GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames());
        setListValues(jStyleList, styleList);
        setListValues(jSizeList, sizeList);
        setCurrentFont(jSample.getFont());
        setResizable(false);
        pack();
    }

    CustomFontChooser(java.awt.Frame parent, boolean modal, Font font) {
        this(parent, modal);
        setCurrentFont(font);
    }

    private void setListValues(JList list, String[] values) {
        if (list.getModel() instanceof DefaultListModel) {
            DefaultListModel model = (DefaultListModel)
                    list.getModel();
            model.removeAllElements();
            for (int i = 0; i < values.length; i++) {
                model.addElement(values[i]);
            }
        }
    }

    private void setSampleFont() {
        if (currentFont != null && currentStyle >= 0 && currentSize > 0) {
            jSample.setFont(new TWFont(currentFont, currentStyle, currentSize));
            //jSample.setText(Language.getString("FONT_CHOOSER_TEXT"));
            //jSample.updateUI();
        }
    }

    private String styleToString(int style) {
        String str = "";
        if ((style & Font.BOLD) == Font.BOLD) {
            if (str.length() > 0) {
                str += ",";
            }
            str += styleList[1];//"Bold";
        }
        if ((style & Font.ITALIC) == Font.ITALIC) {
            if (str.length() > 0) {
                str += ",";
            }
            str += styleList[2];//"Italic";
        }
        if (str.length() <= 0 && (style & Font.PLAIN) == Font.PLAIN) {
            str = styleList[0];//"Plain";
        }
        return str;
    }

    public Font getCurrentFont() {
        return jSample.getFont();
    }

    public void setCurrentFont(Font font) {
        if (font == null) {
            font = jSample.getFont();
        }

        jFont.setText(font.getName());
        jFontActionPerformed(null);

        jStyle.setText(styleToString(font.getStyle()));
        jStyleActionPerformed(null);

        jSize.setText(Integer.toString(font.getSize()));
        jSizeActionPerformed(null);
    }

    public Font showDialog(String title, Font font) {
        //CustomFontChooser dialog = new CustomFontChooser(parent, true, font);

        /*Point p1 = parent.getLocation();
        Dimension d1 = parent.getSize();
        Dimension d2 = dialog.getSize();*/

        setCurrentFont(font);

        GUISettings.setLocationRelativeTo(this, Client.getInstance().getDesktop());

        if (title != null) {
            setTitle(title);
        }
        setVisible(true);

        Font newfont = null;
        if (ok) {
            newfont = getCurrentFont();
        }

        dispose();
        return newfont;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this
     * method is
     * always regenerated by the FormEditor.
     */
    private void initComponents() {
        jPanel3 = new javax.swing.JPanel();
        jFont = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jFontList = new javax.swing.JList();
        jPanel4 = new javax.swing.JPanel();
        jStyle = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jStyleList = new javax.swing.JList();
        jPanel5 = new javax.swing.JPanel();
        jSize = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jSizeList = new javax.swing.JList();
        jPanel1 = new javax.swing.JPanel();
        //jScrollPane4= new javax.swing.JScrollPane();
        jSample = new JLabel(); //javax.swing.JTextArea();
        jButtons = new javax.swing.JPanel();
        jOk = new TWButton();
        jCancel = new TWButton();
        jLabel6 = new javax.swing.JLabel();

        getContentPane().setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints1;
        setTitle(Language.getString("SELECT_FONT"));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel3.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints2;
        jPanel3.setBorder(new javax.swing.border.TitledBorder(
                new javax.swing.border.EtchedBorder(), Language.getString("FONT")));

        jFont.setColumns(24);
        jFont.setEnabled(false);
        //jFont.setActionCommand("F");
        //jFont.addActionListener(this);
        /*java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jFontActionPerformed(evt);
            }
        });*/
        gridBagConstraints2 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints2.gridwidth = 0;
        gridBagConstraints2.fill =
                java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints2.insets = new java.awt.Insets(0, 3,
                0, 3);
        gridBagConstraints2.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints2.weightx = 1.0;
        jPanel3.add(jFont, gridBagConstraints2);


        jFontList.setModel(new DefaultListModel());
        jFontList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        //jFontList.setActionCommand("FL");
        jFontList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jFontListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jFontList);

        gridBagConstraints2 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints2.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints2.insets = new java.awt.Insets(3, 3,
                3, 3);
        gridBagConstraints2.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints2.weightx = 1.0;
        gridBagConstraints2.weighty = 1.0;
        jPanel3.add(jScrollPane1, gridBagConstraints2);


        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints1.insets = new java.awt.Insets(5, 5,
                0, 0);
        gridBagConstraints1.weightx = 0.5;
        gridBagConstraints1.weighty = 1.0;
        getContentPane().add(jPanel3, gridBagConstraints1);


        jPanel4.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints3;
        jPanel4.setBorder(new javax.swing.border.TitledBorder(
                new javax.swing.border.EtchedBorder(), Language.getString("STYLE")));

        jStyle.setPreferredSize(new Dimension(100, 20));// .setColumns(18);
        jStyle.setEnabled(false);
        //jStyle.setBackground(Color.red);
        /*jStyle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jStyleActionPerformed(evt);
            }
        });*/
        gridBagConstraints3 = new java.awt.GridBagConstraints();
        gridBagConstraints3.gridwidth = 0;
        gridBagConstraints3.fill =
                java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.insets = new java.awt.Insets(0, 3,
                0, 3);
        gridBagConstraints3.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints3.weightx = 1.0;
        jPanel4.add(jStyle, gridBagConstraints3);


        jStyleList.setModel(new DefaultListModel());
        jStyleList.setVisibleRowCount(4);
        jStyleList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jStyleListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jStyleList);

        gridBagConstraints3 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints3.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints3.insets = new java.awt.Insets(3, 3,
                3, 3);
        gridBagConstraints3.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints3.weightx = 0.5;
        gridBagConstraints3.weighty = 1.0;
        jPanel4.add(jScrollPane2, gridBagConstraints3);


        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints1.insets = new java.awt.Insets(5, 5,
                0, 0);
        gridBagConstraints1.weightx = 0.375;
        gridBagConstraints1.weighty = 1.0;
        getContentPane().add(jPanel4, gridBagConstraints1);


        jPanel5.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints4;
        jPanel5.setBorder(new javax.swing.border.TitledBorder(
                new javax.swing.border.EtchedBorder(), Language.getString("SIZE")));

        jSize.setColumns(6);
        jSize.setEnabled(false);
        /*jSize.addActionListener(new
        java.awt.event.ActionListener() {
        public void actionPerformed
        (java.awt.event.ActionEvent evt) {
        jSizeActionPerformed(evt);
        }
        }
        );*/
        gridBagConstraints4 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints4.gridwidth = 0;
        gridBagConstraints4.fill =
                java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints4.insets = new java.awt.Insets(0, 3,
                0, 3);
        gridBagConstraints4.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints4.weightx = 1.0;
        jPanel5.add(jSize, gridBagConstraints4);


        jSizeList.setModel(new DefaultListModel());
        jSizeList.setVisibleRowCount(4);
        jSizeList.setSelectionMode
                (javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jSizeList.addListSelectionListener(new
                                                   javax.swing.event.ListSelectionListener() {
                                                       public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                                                           jSizeListValueChanged(evt);
                                                       }
                                                   });
        jScrollPane3.setViewportView(jSizeList);

        gridBagConstraints4 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints4.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints4.insets = new java.awt.Insets(3, 3,
                3, 3);
        gridBagConstraints4.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints4.weightx = 0.25;
        gridBagConstraints4.weighty = 1.0;
        jPanel5.add(jScrollPane3, gridBagConstraints4);


        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints1.insets = new java.awt.Insets(5, 5,
                0, 5);
        gridBagConstraints1.weightx = 0.125;
        gridBagConstraints1.weighty = 1.0;
        getContentPane().add(jPanel5, gridBagConstraints1);


        jPanel1.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints5;
        jPanel1.setBorder(new javax.swing.border.TitledBorder(
                new javax.swing.border.EtchedBorder(), Language.getString("SAMPLE")));

        //jSample.setWrapStyleWord(true);
        //jSample.setEditable(false);
        jSample.setPreferredSize(new Dimension(400, 50));

        //jSample.setColumns(20);
        //jSample.setRows(3);
        jSample.setText(Language.getString("FONT_CHOOSER_TEXT"));
        //jScrollPane4.setViewportView(jSample);

        gridBagConstraints5 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints5.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints5.insets = new java.awt.Insets(0, 3, 3, 3);
        gridBagConstraints5.weightx = 1.0;
        gridBagConstraints5.weighty = 1.0;
        jPanel1.add(jSample, gridBagConstraints5);


        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.fill =
                java.awt.GridBagConstraints.BOTH;
        gridBagConstraints1.insets = new java.awt.Insets(0, 5,
                0, 5);
        gridBagConstraints1.anchor =
                java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints1.weightx = 1.0;
        getContentPane().add(jPanel1, gridBagConstraints1);


        jButtons.setLayout(new java.awt.GridBagLayout());
        java.awt.GridBagConstraints gridBagConstraints6;

        //jOk.setMnemonic(KeyEvent.VK_O);
        jOk.setText(Language.getString("OK"));
        //jOk.setRequestFocusEnabled(true);
        jOk.addActionListener(this);
        jOk.setActionCommand("O");
        jOk.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    jOkActionPerformed();
            }
        });
        /*new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                jOkActionPerformed(evt);
            }
        });*/
        gridBagConstraints6 = new java.awt.GridBagConstraints
                ();
        gridBagConstraints6.insets = new java.awt.Insets(5, 5, 5, 0);
        gridBagConstraints6.anchor = java.awt.GridBagConstraints.WEST;
        jButtons.add(jOk, gridBagConstraints6);

        //jCancel.setMnemonic(KeyEvent.VK_C);
        jCancel.setText(Language.getString("CANCEL"));
        //jCancel.setRequestFocusEnabled(true);
        jCancel.addActionListener(this);
        jCancel.setActionCommand("C");
        jCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    jCancelActionPerformed();
            }
        });
        /*java.awt.event.ActionListener() {
            public void actionPerformed (java.awt.event.ActionEvent evt)
            {
                jCancelActionPerformed(evt);
            }
        });*/
        gridBagConstraints6 = new java.awt.GridBagConstraints();
        gridBagConstraints6.gridwidth = 0;
        gridBagConstraints6.insets = new java.awt.Insets(5, 5, 5, 5);
        gridBagConstraints6.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints6.weightx = 1.0;
        jButtons.add(jCancel, gridBagConstraints6);

        gridBagConstraints6 = new java.awt.GridBagConstraints();
        gridBagConstraints6.weightx = 1.0;
        jButtons.add(jLabel6, gridBagConstraints6);


        gridBagConstraints1 = new java.awt.GridBagConstraints();
        gridBagConstraints1.gridwidth = 0;
        gridBagConstraints1.anchor =
                java.awt.GridBagConstraints.SOUTHWEST;
        gridBagConstraints1.weightx = 1.0;
        getContentPane().add(jButtons, gridBagConstraints1);

        jCancel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke
                (KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        jOk.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke
                (KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    private void jCancelActionPerformed() {
        // Add your handling code here:
        setVisible(false);
    }

    private void jOkActionPerformed() {
        // Add your handling code here:
        ok = true;
        setVisible(false);
    }

    private void jSizeActionPerformed(java.awt.event.ActionEvent evt) {
        // Add your handling code here:
        int size = 0;
        try {
            size = Integer.parseInt(jSize.getText());
        } catch (Exception e) {
        }
        if (size > 0) {
            currentSize = size;
            setSampleFont();
        }
    }

    private void jStyleActionPerformed(java.awt.event.ActionEvent evt) {
        // Add your handling code here:
        StringTokenizer st = new StringTokenizer(jStyle.getText(), ",");
        int style = 0;
        while (st.hasMoreTokens()) {
            String str = st.nextToken().trim();
            if (str.equalsIgnoreCase(Language.getString("PLAIN"))) {
                style |= Font.PLAIN;
            } else if (str.equalsIgnoreCase(Language.getString("BOLD"))) {
                style |= Font.BOLD;
            } else if (str.equalsIgnoreCase(Language.getString("ITALIC"))) {
                style |= Font.ITALIC;
            }
        }
        //if (jStyle.getSelectionStart()
        if (style >= 0) {
            currentStyle = style;
            setSampleFont();
        }
    }

    private void jFontActionPerformed(java.awt.event.ActionEvent evt) {
        // Add your handling code here:
        DefaultListModel model = (DefaultListModel)
                jFontList.getModel();
        if (model.indexOf(jFont.getText()) >= 0) {
            currentFont = jFont.getText();
            setSampleFont();
        }
    }

    private void jStyleListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        // Add your handling code here:
        String str = new String();
        Object[] values = jStyleList.getSelectedValues();

        if (values.length > 0) {
            int i;
            for (i = 0; i < values.length; i++) {
                String s = (String) values[i];
                if (s.equalsIgnoreCase(Language.getString("PLAIN"))) {
                    str = styleList[0];//"Plain";
                    break;
                }
                if (str.length() > 0) {
                    str += ",";
                }
                str += (String) values[i];
            }
        } else {
            str = styleToString(currentStyle);
        }

        jStyle.setText(str);
        jStyleActionPerformed(null);
    }

    private void jSizeListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        // Add your handling code here:
        String str = (String) jSizeList.getSelectedValue();
        if (str == null || str.length() <= 0) {
            str = Integer.toString
                    (currentSize);
        }
        jSize.setText(str);
        jSizeActionPerformed(null);
    }

    private void jFontListValueChanged(javax.swing.event.ListSelectionEvent evt) {
        // Add your handling code here:
        String str = (String) jFontList.getSelectedValue();
        if (str == null || str.length() <= 0) {
            str = currentFont;
        }
        jFont.setText(str);
        jFontActionPerformed(null);
    }

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {
        setVisible(false);
    }

    public void actionPerformed(java.awt.event.ActionEvent evt) {
        if (evt.getActionCommand().equals("C"))
            jCancelActionPerformed();
        else if (evt.getActionCommand().equals("O"))
            jOkActionPerformed();
        else if (evt.getActionCommand().equals("F"))
            jFontActionPerformed(evt);
        //else if (evt.getActionCommand().equals("FA"))
        //    jFontListValueChanged(evt);
    }
    // End of variables declaration

}

