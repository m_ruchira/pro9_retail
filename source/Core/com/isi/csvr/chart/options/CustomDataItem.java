package com.isi.csvr.chart.options;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 1, 2008
 * Time: 12:13:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomDataItem {

    private String name;
    private Object value;


    public CustomDataItem(String name, Object val) {
        this.name = name;
        this.value = val;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getDataName() {
        return name;
    }

    public Object getDataValue() {
        return value;
    }


}
