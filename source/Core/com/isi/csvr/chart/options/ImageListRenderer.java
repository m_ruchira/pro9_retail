package com.isi.csvr.chart.options;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 9, 2009
 * Time: 10:43:21 AM
 * To change this template use File | Settings | File Templates.
 */


class ImageListRenderer implements ListCellRenderer {
    protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();

    public Component getListCellRendererComponent(JList list, Object value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        Font theFont = null;
        Color theForeground = null;
        Icon theIcon = null;
        String theText = null;

        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
                isSelected, cellHasFocus);

        if (value instanceof Object[]) {
            Object values[] = (Object[]) value;
            //theFont = (Font) values[0];
            //theForeground = (Color) values[1];
            theText = (String) values[0];
            theIcon = (Icon) values[1];
        } else {
            theFont = list.getFont();
            theForeground = list.getForeground();
            theText = "";
        }
        if (!isSelected) {
            // renderer.setForeground(theForeground);

        } else { //selected
            renderer.setBackground(Color.gray); //ToDo:  make this thmable
        }

        if (theIcon != null) {
            renderer.setIcon(theIcon);
        }
        renderer.setText(theText);
//    renderer.setFont(theFont);
        return renderer;
    }
}