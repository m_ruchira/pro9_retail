package com.isi.csvr.chart.options;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 25, 2008
 * Time: 11:06:54 AM
 * To change this template use File | Settings | File Templates.
 */

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.spec.AlgorithmParameterSpec;

public class AESFileEncryptor {

    private static String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static AESFileEncryptor encrypter;
    private Cipher ecipher;
    private Cipher dcipher;
    // Create an 8-byte initialization vector
    private byte[] iv = new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    // Buffer used to transport the bytes from one stream to another
    private byte[] buf = new byte[1024];

    private AESFileEncryptor() {

    }

    //returns a singleton object.
    public static AESFileEncryptor getSharedInstance() {

        if (encrypter == null) {
            encrypter = new AESFileEncryptor();


        }
        return encrypter;
    }

    public void constructKeys(String path) {
        try {

            File f = new File(path);
            //for the first time encryption key has not been saved...so should be saved first
            if (!f.exists()) {
                saveEncryptionKey(path);
            }

            //read the encryption key saved in the file system.
            SecretKey key = readEncryptionKey(path);

            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            try {
                ecipher = Cipher.getInstance(ALGORITHM);
                dcipher = Cipher.getInstance(ALGORITHM);

                // CBC requires an initialization vector
                ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
                dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //serialize the encryption key and saved in the file system..
    private void saveEncryptionKey(String filePath) {

        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            kgen.init(128);
            SecretKey key = kgen.generateKey();

            FileOutputStream outStream = new FileOutputStream(new File(filePath));
            ObjectOutputStream objStream = new ObjectOutputStream(outStream);

            objStream.writeObject(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //desirialize the encryption key and returns the saved encryption key.
    private SecretKey readEncryptionKey(String filePath) {

        try {
            FileInputStream inStream = new FileInputStream(new File(filePath));
            ObjectInputStream objInStream = new ObjectInputStream(inStream);

            SecretKey sk = (SecretKey) objInStream.readObject();
            return sk;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //encrypts an input file stream to a specified  output file stream
    public void encryptFile(InputStream in, OutputStream out) {
        try {
            // Bytes written to out will be encrypted
            out = new CipherOutputStream(out, ecipher);

            // Read in the cleartext bytes and write to out to encrypt
            int numRead = 0;
            while ((numRead = in.read(buf)) >= 0) {
                out.write(buf, 0, numRead);
            }
            out.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    //decrypts the encrypted filestream and returns the original data file..
    public void decryptFile(InputStream in, OutputStream out) {
        try {
            // Bytes read from in will be decrypted
            in = new CipherInputStream(in, dcipher);

            // Read in the decrypted bytes and write the cleartext to out
            int numRead = 0;
            while ((numRead = in.read(buf)) >= 0) {
                out.write(buf, 0, numRead);
            }
            out.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    /*  public static void main(String args[]) {
        try {

            AESFileEncryptor encrypter = AESFileEncryptor.getSharedInstance();
            // Encrypt
            encrypter.encryptFile(new FileInputStream("DESTest.txt"), new FileOutputStream("Encrypted.txt"));
            // Decrypt
            encrypter.decryptFile(new FileInputStream("Encrypted.txt"), new FileOutputStream("Decrypted.txt"));

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
