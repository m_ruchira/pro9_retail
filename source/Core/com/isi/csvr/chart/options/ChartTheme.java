package com.isi.csvr.chart.options;

import com.isi.csvr.theme.Theme;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 1, 2008
 * Time: 12:09:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChartTheme {

    private static XMLSerilaizationService service = new XMLSerilaizationService();

    private static ThemeData customData = null;

    //returns the theme data specific to the current theme
    public static ThemeData getThemeDataForCurrentProTheme() {

        ThemeData data = new ThemeData();

        try {
            data.GraphAreaLeft = Theme.getColor("GRAPH_AREA_LEFT_COLOR");
            data.GraphAreaRight = Theme.getColor("GRAPH_AREA_RIGHT_COLOR");
            data.AxisColor = Theme.getColor("GRAPH_AXIS_COLOR");
            data.FontColor = Theme.getColor("GRAPH_FONT_COLOR");
            data.CrosshairsColor = Theme.getColor("GRAPH_CROSS_COLOR");
            data.GridColor = Theme.getColor("GRAPH_GRID_COLOR");
            data.AxisBGInner = Theme.getColor("GRAPH_AXISBG_INNER_COLOR");
            data.AxisBGOuter = Theme.getColor("GRAPH_AXISBG_OUTER_COLOR");
            data.YearSeperator = Theme.getColor("GRAPH_YEAR_SEPERATOR_COLOR");
            data.TitleBGLeft = Theme.getColor("GRAPH_PNLTITLE_LEFT_COLOR");
            data.TitleBGRight = Theme.getColor("GRAPH_PNLTITLE_RIGHT_COLOR");
            data.TitleForeground = Theme.getColor("GRAPH_TITLE_FOREGROUND_COLOR");
            data.SelectedColor = Theme.getColor("GRAPH_SELECTED_COLOR");
            data.TooltipBackground = Theme.getColor("GRAPH_TOOLTIP_BG_COLOR");
            data.TooltipForeground = Theme.getColor("GRAPH_TOOLTIP_FG_COLOR");
            data.TooltipBorderColor = Theme.getColor("GRAPH_TOOLTIP_BORDER_COLOR");
            data.DragZoomColor = Theme.getColor("GRAPH_DRAGZOOM_COLOR");
            data.AnnouncementFlag = Theme.getColor("GRAPH_ANNOUNCEMENT_COLOR");
            data.SplitAdjFlag = Theme.getColor("GRAPH_SPLIT_COLOR");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return data;
    }

    //this is the method what should access from outside..
    public static ThemeData getCustomThemeDataToApply() {

        OptionData current = ChartOptions.getCurrentOptions();
        ThemeData data = getThemeDataForCurrentProTheme();

        /*if (current.chart_color_preferences == ChartOptions.ChartColorPreferences.THEME_COLORS) {
            return data;
        } else if (current.chart_color_preferences == ChartOptions.ChartColorPreferences.CUSTOM_COLORS) {
            return getCurrentCustomThemeData();
        } else { //default colors
            return new ThemeData();
        }*/

        if (current.chart_color_preferences == ChartOptions.ChartColorPreferences.THEME_COLORS) {
            data = applyFontFieldsFromCustomTheme(data);
            return data;
        } else if (current.chart_color_preferences == ChartOptions.ChartColorPreferences.CUSTOM_COLORS) {
            return applyFontFieldsFromCustomTheme((getCurrentCustomThemeData()));
        } else { //default colors
            return applyFontFieldsFromCustomTheme(new ThemeData());
        }
    }

    public static void saveCustomizedThemeData(ThemeData data) {

        if (data == null) {
            return;
        }

        ArrayList<CustomDataItem> customItems = ThemeData.getCustomDataItems(data);
        //service.serializeCustomDataItems(customItems, ChartConstants.CHART_CUSTOMIZED_THEME_FILE_PATH);   //TODO:CHARITHN
        service.serializeCustomDataItems(customItems, ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH);

        customData = data;
    }


    public static ThemeData getCurrentCustomThemeData() {

        if (customData == null) {
            //ArrayList<CustomDataItem> customItems = service.deserializeCustomDataItems(ChartConstants.CHART_CUSTOMIZED_THEME_FILE_PATH);//TODO:charithn
            ArrayList<CustomDataItem> customItems = service.deserializeCustomDataItems(ChartConstants.CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH);
            customData = ThemeData.getCustomThemeData(customItems);
        }
        return customData;
    }

    public static ThemeData updateFontFieldsInCustomTheme(ThemeData data) {
        ThemeData current = getCurrentCustomThemeData();
        current.AxisFont = data.AxisFont;
        current.LegendFont = data.LegendFont;
        current.ToolTipFont = data.ToolTipFont;
        current.TitleFont = data.TitleFont;

        return current;
    }

    public static ThemeData applyFontFieldsFromCustomTheme(ThemeData data) {
        ThemeData current = getCurrentCustomThemeData();
        data.AxisFont = current.AxisFont;
        data.LegendFont = current.LegendFont;
        data.ToolTipFont = current.ToolTipFont;
        data.TitleFont = current.TitleFont;

        return data;
    }
}
