package com.isi.csvr.chart;

import com.isi.csvr.linkedwindows.LinkStore;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 28, 2009
 * Time: 8:03:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartLinkStore {

    private static ChartLinkStore self;
    private Hashtable<String, ArrayList<GraphFrame>> store = new Hashtable<String, ArrayList<GraphFrame>>();
    private ArrayList<GraphFrame> redLinkGroup = new ArrayList<GraphFrame>();
    private ArrayList<GraphFrame> greenLinkGroup = new ArrayList<GraphFrame>();
    private ArrayList<GraphFrame> blueLinkGroup = new ArrayList<GraphFrame>();

    private ChartLinkStore() {

        store.put(LinkStore.LINK_RED, redLinkGroup);
        store.put(LinkStore.LINK_GREEN, greenLinkGroup);
        store.put(LinkStore.LINK_BLUE, blueLinkGroup);
    }

    public static ChartLinkStore getSharedInstance() {
        if (self == null) {
            self = new ChartLinkStore();
        }
        return self;
    }

    public void addToStore(GraphFrame gf, String id) {
        removeIfAlreadyExists(gf);//remove the old one before adding the new one

        if (id.equals(LinkStore.LINK_RED)) {
            redLinkGroup.add(gf);
        } else if (id.equals(LinkStore.LINK_GREEN)) {
            greenLinkGroup.add(gf);
        } else if (id.equals(LinkStore.LINK_BLUE)) {
            blueLinkGroup.add(gf);
        }
    }

    public void removeFromStore(GraphFrame gf, String id) {
        if (id.equals(LinkStore.LINK_RED)) {
            redLinkGroup.remove(gf);
        } else if (id.equals(LinkStore.LINK_GREEN)) {
            greenLinkGroup.remove(gf);
        } else if (id.equals(LinkStore.LINK_BLUE)) {
            blueLinkGroup.remove(gf);
        }
    }

    public ArrayList<GraphFrame> getGraphFrames(String key) {
        return store.get(key);
    }

    public void removeIfAlreadyExists(GraphFrame frame) {

        for (int i = 0; i < redLinkGroup.size(); i++) {
            if (frame == redLinkGroup.get(i)) {
                redLinkGroup.remove(frame);
                return;
            }
        }
        for (int i = 0; i < blueLinkGroup.size(); i++) {
            if (frame == blueLinkGroup.get(i)) {
                blueLinkGroup.remove(frame);
                return;
            }
        }
        for (int i = 0; i < greenLinkGroup.size(); i++) {
            if (frame == greenLinkGroup.get(i)) {
                greenLinkGroup.remove(frame);
                return;
            }
        }
    }

}
