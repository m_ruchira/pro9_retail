package com.isi.csvr.chart;

import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public interface Indicator {
    static final long UID_BollingerBand = 1072600473330203810L;
    static final long UID_LRI = 1072600473330203811L;
    static final long UID_MACD = 1072600473330203812L;
    static final long UID_Momentum = 1072600473330203813L;
    static final long UID_MovingAvg = 1072600473330203814L;
    static final long UID_RSI = 1072600473330203815L;
    static final long UID_StdDeviation = 1072600473330203816L;
    static final long UID_Volume = 1072600473330203817L;
    static final long UID_AccumDistrib = 1072600473330203818L;
    static final long UID_StochasticOscil = 1072600473330203819L;
    static final long UID_WilliamsR = 1072600473330203820L;
    static final long UID_AvgTrueRange = 1072600473330203821L;
    static final long UID_MassIndex = 1072600473330203822L;
    static final long UID_MedianPrice = 1072600473330203823L;
    static final long UID_MoneyFlowIndex = 1072600473330203824L;
    static final long UID_Trix = 1072600473330203825L;
    static final long UID_Volatility = 1072600473330203826L;
    static final long UID_ZigZag = 1072600473330203827L;
    static final long UID_RMI = 1072600473330203828L;
    static final long UID_AROON = 1072600473330203829L;
    static final long UID_CHAIKINS_OSCILL = 1072600473330203830L;
    static final long UID_CCI = 1072600473330203831L;
    static final long UID_CHAIKINS_MONEY_FLOW = 1072600473330203832L;
    static final long UID_EASE_OF_MOVEMENT = 1072600473330203833L;
    static final long UID_ICHIMOKU_KINKO_HYO = 1072600473330203834L;
    static final long UID_KLINGER_OSCILL = 1072600473330203835L;
    static final long UID_PARABOLIC_SAR = 1072600473330203836L;
    static final long UID_PRICE_VOLUME_TREND = 1072600473330203837L;
    static final long UID_VOLUME_OSCILL = 1072600473330203838L;
    static final long UID_WILLS_ACCU_DISTRI = 1072600473330203839L;
    static final long UID_ENVILOPES = 1072600473330203840L;
    static final long UID_PRICE_OSCILL = 1072600473330203841L;
    static final long UID_INTRADAY_MOMENTUM_INDEX = 1072600473330203842L;
    static final long UID_ON_BALANCE_VOLUME = 1072600473330203843L;
    static final long UID_ROC = 1072600473330203844L;
    static final long UID_WILDERS_SMOOTHING = 1072600473330203845L;
    static final long UID_PLUS_DI = 1072600473330203846L;
    static final long UID_MINUS_DI = 1072600473330203847L;
    static final long UID_DX = 1072600473330203848L;
    static final long UID_ADX = 1072600473330203849L;
    static final long UID_ADXR = 1072600473330203850L;
    static final long UID_QSTICK = 1072600473330203851L;
    static final long UID_VERT_HORI_FILTER = 1072600473330203852L;
    static final long UID_CMO = 1072600473330203853L;
    static final long UID_FO = 1072600473330203854L;
    static final long UID_WEIGHTED_CLOSE = 1072600473330203855L;
    static final long UID_TSF = 1072600473330203856L;
    static final long UID_VOLUME_ROC = 1072600473330203857L;
    static final long UID_SMO = 1072600473330203858L;
    static final long UID_PVI = 1072600473330203859L;
    static final long UID_PERFORMANCE = 1072600473330203860L;
    static final long UID_DPO = 1072600473330203861L;
    static final long UID_TYPICAL_PRICE = 1072600473330203862L;
    static final long UID_MFI = 1072600473330203863L;
    static final long UID_PRICE_CHANNEL = 1072600473330203864L;
    static final long UID_NVI = 1072600473330203865L;
    static final long UID_DEMA = 1072600473330203866L;
    static final long UID_RVI = 1072600473330203867L;
    static final long UID_SWING_INDEX = 1072600473330203868L;
    static final long UID_TEMA = 1072600473330203869L;
    static final long UID_STD_ERR = 1072600473330203870L;
    static final long UID_ZIGZAG_SLOPE = 1072600473330203871L;
    static final long UID_TURNOVER = 1072600473330203872L;
    static final long UID_NET_ORDERS = 1072600473330203873L;
    ;
    static final long UID_NET_TURNOVER = 1072600473330203874L;
    static final long UID_COPPOCK_CURVE = 1072600473330203884L;
    static final long UID_DISPARITY_INDEX = 1072600473330203885L;
    static final long UID_HULL_MOVING_AVG = 1072600473330203888L;
    static final long UID_KELTNER_CHANNEL = 1072600473330203890L;

    static final String FD = "|";
    static final double NULL = Double.NaN;//-9876.54321d; //Double.MAX_VALUE;  this is removed to avoid memory leaks

    boolean hasItsOwnScale();

    ChartProperties getInnerSource();

    void setInnerSource(ChartProperties cp);

    int getInnerSourceIndex(ArrayList Sources);

    void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int graphIndex);

    String getTableColumnHeading();

    void setTableColumnHeading(String tableColumnHeading);

    void setGroupID(String gID);
}
