package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:31:34 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class MassIndex extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_MassIndex;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public MassIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_MASS_INDEX") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MASS_INDEX");
        this.timePeriods = 25;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_i_mePeriods, byte stepHminusL, byte stepEMA, byte stepEMAofEMA,
                                          byte destIndex) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        //setting step size 3
       /* for (int i=0; i<al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }*/

        // steps involved
        /*byte stepHminusL = ChartRecord.STEP_1;
        byte stepEMA = ChartRecord.STEP_2;
        byte stepEMAofEMA = ChartRecord.STEP_3;*/

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepHminusL, cr.High - cr.Low);
        }

        MovingAverage.getMovingAverage(al, 0, 9, MovingAverage.METHOD_EXPONENTIAL, stepHminusL, stepEMA);
        MovingAverage.getMovingAverage(al, 8, 9, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);

        int loopBegin = 16 + bIndex + t_i_mePeriods;
        double currVal, sumVal = 0d;

        for (int i = 16 + bIndex; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA) / cr.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal + currVal;
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            if (i == loopBegin - 1) {
                /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint!=null){
                    aPoint.setIndicatorValue(sumVal);
                }*/
                cr.setValue(destIndex, sumVal);

            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - t_i_mePeriods);
            currVal = crOld.getStepValue(stepEMA) / crOld.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal - currVal;

            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA) / cr.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal + currVal;

            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint!=null){
                aPoint.setIndicatorValue(sumVal);
            }*/
            cr.setValue(destIndex, sumVal);

        }
        //System.out.println("**** Mass Index Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 25;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MassIndex) {
            MassIndex mi = (MassIndex) cp;
            this.timePeriods = mi.timePeriods;
            this.innerSource = mi.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_MASS_INDEX") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this needs 3 intermediate steps: H-L, EMA, EMAofEMA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepHminusL = ChartRecord.STEP_1;
        byte stepEMA = ChartRecord.STEP_2;
        byte stepEMAofEMA = ChartRecord.STEP_3;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepHminusL, cr.High - cr.Low);
        }

        MovingAverage.getMovingAverage(al, 0, 9, MovingAverage.METHOD_EXPONENTIAL, stepHminusL, stepEMA);
        MovingAverage.getMovingAverage(al, 8, 9, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);

        int loopBegin = 16 + timePeriods;
        double currVal, sumVal = 0d;
        for (int i = 16; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA) / cr.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal + currVal;
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            if (i == loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(sumVal);
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - timePeriods);
            currVal = crOld.getStepValue(stepEMA) / crOld.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal - currVal;

            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA) / cr.getStepValue(stepEMAofEMA);
            if (Double.isNaN(currVal) || Double.isInfinite(currVal)) currVal = 0;
            sumVal = sumVal + currVal;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(sumVal);
            }
        }
        //System.out.println("**** Mass Index Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_MASS_INDEX");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
