package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: May 13, 2008
 * Time: 1:12:22 PM
 * To change this template use File | Settings | File Templates.
 */

public class SmartZoomWindow extends JWindow implements ActionListener, DateSelectedListener, AWTEventListener, Themeable {

    protected static JLabel lblDate;
    private static SmartZoomWindow sZoomWindow;
    //private final int SMART_WINDOW_WIDTH = 600;
    private final int SMART_WINDOW_WIDTH = 470;
    //private final int SMART_WINDOW_HEIGHT = 170;
    private final int SMART_WINDOW_HEIGHT = 145;
    //private final int DRAG_CHART_PANEL_WIDTH = 570;
    private final int DRAG_CHART_PANEL_WIDTH = 440;
    //private final int DRAG_CHART_PANEL_HEIGHT = 125;
    private final int DRAG_CHART_PANEL_HEIGHT = 105;
    public DatePicker datePicker;
    private TimePeriodButton btn1d;
    private TimePeriodButton btn6d;
    private TimePeriodButton btn1m;
    private TimePeriodButton btn3m;
    private TimePeriodButton btn6m;
    private TimePeriodButton btnytd;
    private TimePeriodButton btn1y;
    private TimePeriodButton btn18m;
    private TimePeriodButton btn2y;
    private TimePeriodButton btn3y;
    private TimePeriodButton btn5y;
    private TimePeriodButton btn10y;
    private TimePeriodButton btnall;
    private TimePeriodButton btnDate;
    private JLabel lblEmptry;
    private JPanel bottomPanel;
    private DragChartPanel dragChartPanel;
    private GraphFrame graphFrame;
    private String lblDateText = "";
    private boolean isVisible = false;

    private SmartZoomWindow() {
        createUI();
    }

    public static SmartZoomWindow getSharedInstance() {

        /*if (sZoomWindow == null) {
            sZoomWindow = new SmartZoomWindow();
        }
        return sZoomWindow;*/

        if (sZoomWindow != null) {
            sZoomWindow = null;
        }
        return new SmartZoomWindow();
    }

    public static SmartZoomWindow getObject() {
        return sZoomWindow;
    }

    public static void destroyObject() {
        sZoomWindow = null;
    }

    public DragChartPanel getDragChartPanel() {
        return dragChartPanel;
    }

    public boolean isSmartZoomWindowVisible() {
        return isVisible;
    }

    public void setSmartZoomWindowVisible(boolean visible) {
        isVisible = visible;
    }

    public void setParameters(GraphFrame parent) {
        this.graphFrame = parent;
    }

    private void createUI() {

        //this.setUndecorated(true); // this removes the titlebar from a JFrame.
        this.setBackground(Color.blue);

        btn1d = new TimePeriodButton("1d");
        btn1d.addActionListener(this);
        btn1d.setActionCommand(String.valueOf(StockGraph.ONE_DAY));

        btn6d = new TimePeriodButton("6d");
        btn6d.addActionListener(this);
        btn6d.setActionCommand(String.valueOf(StockGraph.SIX_DAYS));

        btn1m = new TimePeriodButton("1m");
        btn1m.addActionListener(this);
        btn1m.setActionCommand(String.valueOf(StockGraph.ONE_MONTH));

        btn3m = new TimePeriodButton("3m");
        btn3m.setActionCommand(String.valueOf(StockGraph.THREE_MONTHS));
        btn3m.addActionListener(this);

        btn6m = new TimePeriodButton("6m");
        btn6m.addActionListener(this);
        btn6m.setActionCommand(String.valueOf(StockGraph.SIX_MONTHS));

        btnytd = new TimePeriodButton("YTD");
        btnytd.addActionListener(this);
        btnytd.setActionCommand(String.valueOf(StockGraph.YTD));

        btn1y = new TimePeriodButton("1Y");
        btn1y.addActionListener(this);
        btn1y.setActionCommand(String.valueOf(StockGraph.ONE_YEAR));

        btn18m = new TimePeriodButton("18m");
        btn18m.addActionListener(this);
        btn18m.setActionCommand(String.valueOf(StockGraph.EIGHTEEN_MONTHS));

        btn2y = new TimePeriodButton("2Y");
        btn2y.addActionListener(this);
        btn2y.setActionCommand(String.valueOf(StockGraph.TWO_YEARS));

        btn3y = new TimePeriodButton("3Y");
        btn3y.addActionListener(this);
        btn3y.setActionCommand(String.valueOf(StockGraph.THREE_YEARS));

        btn5y = new TimePeriodButton("5Y");
        btn5y.addActionListener(this);
        btn5y.setActionCommand(String.valueOf(StockGraph.FIVE_YEARS));

        btn10y = new TimePeriodButton("10Y");
        btn10y.addActionListener(this);
        btn10y.setActionCommand(String.valueOf(StockGraph.TEN_YEARS));

        btnall = new TimePeriodButton("all");
        btnall.addActionListener(this);
        btnall.setActionCommand(String.valueOf(StockGraph.ALL_HISTORY));

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);

        lblDateText = date + "/" + month + "/" + year;
        lblDate = new JLabel(lblDateText);
        lblDate.setFont(new Font("Tahoma", Font.BOLD, 11));

        btnDate = new TimePeriodButton(" ");
        btnDate.setMargin(new Insets(0, 0, 0, 0));
        btnDate.setBorder(BorderFactory.createEmptyBorder());
        btnDate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/smartZoomCalendar.gif"));
        btnDate.addActionListener(this);

        lblEmptry = new JLabel("");
        String[] widths = new String[]{"25", "25", "25", "25", "25", "25", "25", "25", "25", "25", "25", "25", "25", "5", "70", "25"};
        String[] heights = new String[]{"20"};

        FlexGridLayout layout = new FlexGridLayout(widths, heights, 2, 3, true, true);

        bottomPanel = new JPanel(layout);

        bottomPanel.add(btn1d);
        bottomPanel.add(btn6d);
        bottomPanel.add(btn1m);
        bottomPanel.add(btn3m);
        bottomPanel.add(btn6m);
        bottomPanel.add(btnytd);
        bottomPanel.add(btn1y);
        bottomPanel.add(btn18m);
        bottomPanel.add(btn2y);
        bottomPanel.add(btn3y);
        bottomPanel.add(btn5y);
        bottomPanel.add(btn10y);
        bottomPanel.add(btnall);
        bottomPanel.add(lblEmptry);
        bottomPanel.add(lblDate);
        bottomPanel.add(btnDate);

        datePicker = new DatePicker(this, true);
        datePicker.getCalendar().addDateSelectedListener(this);
        datePicker.getCalendar().setSelfDispose(false);

        dragChartPanel = new DragChartPanel();
        dragChartPanel.setPreferredSize(new Dimension(DRAG_CHART_PANEL_WIDTH, DRAG_CHART_PANEL_HEIGHT));

        JPanel contentPanel = new JPanel(new BorderLayout(0, 2));
        Color color = new Color(153, 153, 255);
        contentPanel.setBackground(color);
        contentPanel.add(dragChartPanel, BorderLayout.NORTH);
        contentPanel.add(bottomPanel, BorderLayout.SOUTH);
        contentPanel.setBorder(BorderFactory.createLineBorder(color, 5));

        this.setLayout(new BorderLayout());
        this.add(contentPanel, BorderLayout.CENTER);
        this.setSize(SMART_WINDOW_WIDTH, SMART_WINDOW_HEIGHT);

        //set location should done here also to solve the paint issue in smart window
        int x, y;
        if (Language.isLTR()) {
            x = (int) (BottomToolBar.getBtnSmartZoom().getLocationOnScreen().getX() - this.getWidth());
            y = (int) (BottomToolBar.getBtnSmartZoom().getLocationOnScreen().getY() - this.getHeight());
        } else {
            x = (int) BottomToolBar.getBtnSmartZoom().getLocationOnScreen().getX();
            y = (int) (BottomToolBar.getBtnSmartZoom().getLocationOnScreen().getY() - this.getHeight());
        }

        this.setLocation(x, y);
        GUISettings.applyOrientation(this);
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == btnDate) {
            if (!datePicker.isVisible()) {

                String[] delims = lblDate.getText().trim().split("/", 3);
                int date = Integer.parseInt(delims[0]);
                int month = Integer.parseInt(delims[1]) - 1; // 0 based index
                int year = Integer.parseInt(delims[2]);

                datePicker.setDate(year, month, date);
                int x = 0, y = 0;
                if (Language.isLTR()) {
                    x = (int) this.getLocation().getX() + (dragChartPanel.getWidth() - 20);
                    y = (int) (this.getLocation().getY()) - (datePicker.getHeight() - dragChartPanel.getHeight()) + 20;
                } else {
                    x = (int) this.getLocation().getX();
                    y = (int) (this.getLocation().getY()) - (datePicker.getHeight() - dragChartPanel.getHeight()) + 20;
                }
                datePicker.setLocation(x, y);
                datePicker.show();
                this.setVisible(true);
            } else {
                datePicker.setVisible(false);
            }
        }

        if (e.getSource() != btnDate) {
            JButton btn = (JButton) e.getSource();
            int period = Integer.parseInt(btn.getActionCommand());
            dragChartPanel.setGraph(graphFrame);
            /* dragChartPanel.setDataArray(graphFrame.graph, graphFrame.graph.GDMgr.getBaseCP(), period,
           graphFrame.graph.getInterval(), graphFrame.graph.zSlider.getBeginPos(),
           graphFrame.graph.zSlider.getZoom(), 0L);*/

            StockGraph graph = graphFrame.graph;
            ChartProperties cp = graphFrame.graph.GDMgr.getBaseCP();
            long interval = graphFrame.graph.getInterval();
            float beginPos = graphFrame.graph.zSlider.getBeginPos();
            float zoom = graphFrame.graph.zSlider.getZoom();

            dragChartPanel.setDataArray(graph, cp, period, interval, beginPos, zoom, 0L);
            dragChartPanel.onSmartZoom(true, true, period);
        }
        ChartFrame.getSharedInstance().refreshToolBars();//for update period and time
    }

    public void initButons(boolean isIntraDay) {

        btn1d.setEnabled(isIntraDay);
        btn6d.setEnabled(isIntraDay);

        btn1m.setEnabled(!isIntraDay);
        btn3m.setEnabled(!isIntraDay);
        btn6m.setEnabled(!isIntraDay);
        btnytd.setEnabled(!isIntraDay);
        btn1y.setEnabled(!isIntraDay);
        btn18m.setEnabled(!isIntraDay);
        btn2y.setEnabled(!isIntraDay);
        btn3y.setEnabled(!isIntraDay);
        btn5y.setEnabled(!isIntraDay);
        btn10y.setEnabled(!isIntraDay);

        //all fistory button should be enabled for both modes.
        btnall.setEnabled(true);
    }

    //this event fires when the date is selected from a tare picker controler.
    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {

        try {
            Calendar cal = Calendar.getInstance();
            cal.set(iYear, iMonth, iDay);
            datePicker.hide();
            int period = StockGraph.CUSTOM_PERIODS;
            long begin = cal.getTimeInMillis();

            dragChartPanel.setGraph(graphFrame);
            dragChartPanel.setDataArray(graphFrame.graph, graphFrame.graph.GDMgr.getBaseCP(),
                    period, graphFrame.graph.getInterval(), graphFrame.graph.zSlider.getBeginPos(),
                    graphFrame.graph.zSlider.getZoom(), begin);

            dragChartPanel.onSmartZoom(true, false, period);

            lblDateText = String.valueOf(iDay) + "/" + String.valueOf(iMonth + 1) + "/" + String.valueOf(iYear);
            lblDate.setText(lblDateText);
            this.setVisible(true);
            setAWTListenerEffected(true);
        } catch (Exception e) {
            e.printStackTrace();
            setAWTListenerEffected(true);
        }
    }

    public Object[] getSelectedObjects() {
        return new Object[0];
    }

    //this event fires when the mouse motined or component rezised
    public void eventDispatched(AWTEvent event) {

        try {
            //System.out.println("======== AWTEventListner calling in Smart Zoomer ========");
            Object obj = event.getSource();

            /*  if (event.getID() == MouseEvent.MOUSE_PRESSED && !(obj instanceof DragChartPanel || obj instanceof JScrollBar || obj instanceof javax.swing.plaf.metal.MetalScrollButton ||
                      obj instanceof SmartZoomWindow || obj instanceof TimePeriodButton || obj instanceof javax.swing.plaf.basic.BasicComboPopup ||
                      obj.getClass().toString().contains("com.isi.csvr.calendar.CalLabel") || obj instanceof DatePicker || obj.getClass().toString().contains("com.isi.csvr.DetachableRootPane$2") ||
                      obj instanceof javax.swing.plaf.metal.MetalComboBoxButton || obj.getClass().toString().contains("javax.swing.plaf.basic.BasicComboPopup$1"))) {
            */
            //event.getID() == 501 when the mouse is clicked within the application.
            if (clickedOutSideOnZoomerWindow(event)) {
                if (obj != BottomToolBar.getBtnSmartZoom()) {
                    if (this.isSmartZoomWindowVisible()) {
                        this.setVisible(false);
                        this.setSmartZoomWindowVisible(false);
                        this.setAWTListenerEffected(false);
                        //System.out.println("======== AWTEventListner removed from Smart Zoomer ========");
                    }
                }
            }
            //set to visible false when pro frame is moved..
            //event.getID() == 100 when the pro app is moved ny the mouse
            if (event.getID() == ComponentEvent.COMPONENT_MOVED && (obj == Client.getInstance().getFrame())) {

                if (this.isSmartZoomWindowVisible()) {
                    this.setVisible(false);
                    this.setSmartZoomWindowVisible(false);
                    this.setAWTListenerEffected(false);
                    //System.out.println("======== AWTEventListner removed from Smart Zoomer ========");
                }
            }

            //event.getID() == 203 for window minimize event
            if ((event.getID() == WindowEvent.WINDOW_ICONIFIED || event.getID() == WindowEvent.WINDOW_CLOSED) && (obj == Client.getInstance().getFrame())) {

                if (this.isSmartZoomWindowVisible()) {
                    this.setVisible(false);
                    this.setSmartZoomWindowVisible(false);
                    this.setAWTListenerEffected(false);
                    //System.out.println("======== AWTEventListner removed from Smart Zoomer ========");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean clickedOutSideOnZoomerWindow(AWTEvent event) {

        Object obj = event.getSource();
        Container c = SwingUtilities.getAncestorOfClass(DatePicker.class, (Component) event.getSource());

        //event.getID() == 501 when the mouse is clicked within the application.
        if ((event.getID() == MouseEvent.MOUSE_PRESSED) && !(obj instanceof DragChartPanel || obj instanceof JScrollBar || obj instanceof javax.swing.plaf.metal.MetalScrollButton ||
                obj instanceof SmartZoomWindow || obj instanceof TimePeriodButton || obj instanceof javax.swing.plaf.basic.BasicComboPopup ||
                obj instanceof DatePicker || obj instanceof javax.swing.plaf.metal.MetalComboBoxButton || c != null)) {

            return true;
        } else {
            return false;
        }
    }


    //add or removes the AWTListener when required. this is to reduce the overhead of firing the method always.
    public void setAWTListenerEffected(boolean isSmartZoomWindowVisible) {

        try {
            if (isSmartZoomWindowVisible) {
                Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
                Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.WINDOW_EVENT_MASK);
                Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.COMPONENT_EVENT_MASK);
            } else {
                try {
                    Toolkit.getDefaultToolkit().removeAWTEventListener(this);
                    //Toolkit.getDefaultToolkit().removeAWTEventListener(this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            setVisible(isSmartZoomWindowVisible);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void applyTheme() {
        this.setBackground(Theme.getColor("BGCOLOR"));
    }

    //CUSTOM BUTTON TO USE IN THE PERIOD SELECTION BUTTONS
    public class TimePeriodButton extends JButton {

        private TimePeriodButton(String text) {
            super(text);
            setFont(new Font("Tahoma", Font.BOLD, 9));
        }

        public Insets getInsets() {
            return new Insets(0, 0, 0, 0);
        }
    }
}
