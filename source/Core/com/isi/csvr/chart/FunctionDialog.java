package com.isi.csvr.chart;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 16, 2009
 * Time: 12:22:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class FunctionDialog extends JDialog implements ActionListener, MouseListener, Themeable {

    private JList lstCategories;
    private JList lstFunctions;
    private JLabel lblFunction;
    private TWButton btnOK;
    private TWButton btnCancel;

    private Object[] saCategories = {
            "All",
            "Analysis Tools",
            "Data Arrays",
            "Indicators",
            "Logical",
            "Math",
            "Operators",
            "Constants",
            "Plot Functions"
    };

    private Object[] AnalysisTools = {
            new PasteFunction("Highest", "highest(<Data Array>)", 12),
            new PasteFunction("Highest Bars Ago", "highestbars(<Data Array>)", 12),
            new PasteFunction("Highest High Value", "hhv(<Data Array>, <Periods>)", 24),
            new PasteFunction("Highest High Value Bars Ago", "hhvbars(<Data Array>, <Periods>)", 24),
            new PasteFunction("Highest Since", "highestsince(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Highest Since Bars Ago", "highestsincebars(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Lowest", "lowest(<Data Array>)", 12),
            new PasteFunction("Lowest Bars Ago", "lowestbars(<Data Array>)", 12),
            new PasteFunction("Lowest High Value", "llv(<Data Array>, <Periods>)", 24),
            new PasteFunction("Lowest High Value Bars Ago", "llvbars(<Data Array>, <Periods>)", 24),
            new PasteFunction("Lowest Since", "lowestsince(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Lowest Since Bars Ago", "lowestsincebars(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Reference", "ref(<Data Array>, <Count>)", 22) // also included in Data Arrays
    };

    private Object[] DataArrays = {
            new PasteFunction("Close", "Close"),
            new PasteFunction("High", "High"),
            new PasteFunction("Low", "Low"),
            new PasteFunction("Open", "Open"),
            new PasteFunction("Volume", "Volume"),
            new PasteFunction("Reference", "ref(<Data Array>, <Count>)", 22), // also included in Analysis Tools
            new PasteFunction("Yesterdays Close", "ref(Close, -1)"),
            new PasteFunction("Yesterdays High", "ref(High, -1)"),
            new PasteFunction("Yesterdays Low", "ref(Low, -1)"),
            new PasteFunction("Yesterdays Open", "ref(Open, -1)"),
            new PasteFunction("Yesterdays Volume", "ref(Volume, -1)")
    };

    private Object[] Indicators = {
            /*new PasteFunction("Accumulation Distribution", "AccumulationDistribution()", 0), //done
       new PasteFunction("Aroon", "Aroon(<Time Periods>)", 0),
       new PasteFunction("Average True Range", "AverageTrueRange(<Time Periods>)", 0),  //done
       new PasteFunction("Bollinger Bands", "BollingerBands(<Data Array>, <Time Periods>, <Method> ,<Deviations>, <BandType(-1,0,1)>)", 0),  //done TODO: which band type
       new PasteFunction("Chaikin A/D Oscillator", "ChaikinADOscillator()", 0), //done
       new PasteFunction("ChaikinMoney Flow", "ChaikinMoneyFlow(<Time Periods>)", 0),   //done
       new PasteFunction("ChandeMomentum Oscillator", "ChandeMomentumOscillator(<Data Array>,<Time Periods>)", 0), //done
       new PasteFunction("Commodity Channel Index", "CommodityChannelIndex(<Time Periods>)", 0),  //done
       new PasteFunction("DEMA", "DEMA(<Data Array>,<Time Periods>)", 0),    //done
       new PasteFunction("Detrended Price Oscillator", "DetrendedPriceOscillator(<Data Array>,<Time Periods>)", 0), //done
       new PasteFunction("Ease Of Movement", "EaseOfMovement()", 0),
       new PasteFunction("Envelope", "Envelope(<Data Array>, <Time Periods>, <Method> ,<Percentage>)", 0),
       new PasteFunction("Forcast Oscillator", "ForcastOscillator(<Time Periods>, <Data Array> ,<Time Periods>)", 0),
       new PasteFunction("Ichimoku Kinko Hyo", "IchimokuKinkoHyo()", 0),
       new PasteFunction("Intraday Momentum Index", "IntradayMomentumIndex(<Time Periods>)", 0), //done
       new PasteFunction("Klinger Oscillator", "KlingerOscillator()", 0),//done
       new PasteFunction("Linear Regression", "LinearRegression(<Data Array>, <Time Periods>)", 29),//done
       new PasteFunction("MACD", "MACD(<Data Array>)", 0),   //done
       new PasteFunction("MACD Volume", "MACDVolume(<Data Array>,<Time Periods>)", 0),
       new PasteFunction("Market Facilitation Index", "MarketFacilitationIndex()", 0),
       new PasteFunction("Mass Index", "MassIndex(<Time Periods>)", 0),//done
       new PasteFunction("Median Price", "MedianPrice()", 0), //done
       new PasteFunction("Momentum", "Momentum(<Data Array>,<Time Periods>)", 0),//done
       new PasteFunction("Money Flow Index", "MoneyFlowIndex(<Time Periods>)", 0),//done
       new PasteFunction("Moving Average", "MovingAverage(<Data Array>, <Time Periods>, <Method>)", 39),//done
       new PasteFunction("Negative Volume Index", "NegativeVolumeIndex()", 0),//done
       new PasteFunction("On Balance Volume", "OnBalanceVolume(<Time Periods>)", 0),
       new PasteFunction("Parabolic SAR", "ParabolicSAR(<Deviations>,<Deviations>)", 0),//done
       new PasteFunction("Performance", "Performance(<Data Array>)", 0),
       new PasteFunction("Positive Volume Index", "PositiveVolumeIndex(<Data Array>)", 0),
       new PasteFunction("Price Channel", "PriceChannel(<Time Periods>)", 0),
       new PasteFunction("Price Oscillator", "PriceOscillator(<Time Periods>,<Time Periods>,<Data Array>,<Method>,<Method>)", 0),
       new PasteFunction("PriceROC", "PriceROC(<Time Periods>,<Data Array>,<Method>)", 0),
       new PasteFunction("Price Volume Trend", "PriceVolumeTrend(<Data Array>)", 0),
       new PasteFunction("Qstick", "Qstick(<Time Periods>)", 0),//done
       new PasteFunction("Relative Momentum Index", "RelativeMomentumIndex(<Data Array>,<Time Periods>,<Time Periods>)", 0),
       new PasteFunction("Relative Strength Index", "RelativeStrengthIndex(<Data Array>,<Time Periods>)", 0),//done
       new PasteFunction("Relative Volatility Index", "RelativeVolatilityIndex(<Time Periods>)", 0),
       new PasteFunction("Standard Deviation", "StandardDeviation(<Data Array>, <Time Periods>, <Deviations>)", 43), //done
       new PasteFunction("Standard Error", "StandardError(<Time Periods>,<Data Array>)", 0),
       new PasteFunction("StochasticMomentumIndex", "StochasticMomentumIndex(<Time Periods>,<Time Periods>,<Time Periods>,<Time Periods>)", 0), //done
       new PasteFunction("StochasticOscillator", "StochasticOscillator(<Time Periods>,<Time Periods>)", 0), //done
       new PasteFunction("TEMA", "TEMA(<Data Array>,<Time Periods>)", 0),//done
       new PasteFunction("Time Series Forecast", "TimeSeriesForecast(<Data Array>,<Time Periods>)", 0), //done
       new PasteFunction("TRIX", "TRIX(<Data Array>, <Time Periods>)", 0),//done
       new PasteFunction("Typical Price", "TypicalPrice()", 0),   //done
       new PasteFunction("Vertical Horizontal Filter", "VerticalHorizontalFilter(<Time Periods>,<Data Array>)", 0),
       new PasteFunction("Volatility (Chaikin's)", "Volatility(<Time Periods>,<Rate of Change>)", 0),
       new PasteFunction("Volume Oscillator", "VolumeOscillator(<ShortTerm Time Periods>,<LongTerm Time Periods>,<Method>,<Method>)", 0),
       new PasteFunction("Volume ROC", "VolumeROC(<Time Periods>)", 0),  //done
       new PasteFunction("Weighted Close", "WeightedClose()", 0), //done
       new PasteFunction("Wilders Smoothing", "WildersSmoothing(<Data Array>,<Time Periods>)", 0),//done
       new PasteFunction("Williams % R", "WilliamsR(<Time Periods>)", 0), //done
       new PasteFunction("Wills Accumulation Distribution", "WillsAccumDistribution()", 0),
       new PasteFunction("ZigZag", "ZigZag(<Data Array>,<Amount>,<Method>)", 0) //done*/


            new PasteFunction("Accumulation Distribution", "AccumulationDistribution()", 0), //done
            new PasteFunction("Average True Range", "AverageTrueRange(<Time Periods>)", 0),  //done
            new PasteFunction("Bollinger Bands", "BollingerBands(<Data Array>, <Time Periods>, <Method> ,<Deviations>, <BandType(-1,0,1)>)", 0),  //done TODO: which band type
            new PasteFunction("Chaikin A/D Oscillator", "ChaikinADOscillator()", 0), //done
            new PasteFunction("ChaikinMoney Flow", "ChaikinMoneyFlow(<Time Periods>)", 0),   //done
            new PasteFunction("ChandeMomentum Oscillator", "ChandeMomentumOscillator(<Data Array>,<Time Periods>)", 0), //done
            new PasteFunction("Commodity Channel Index", "CommodityChannelIndex(<Time Periods>)", 0),  //done
            new PasteFunction("DEMA", "DEMA(<Data Array>,<Time Periods>)", 0),    //done
            new PasteFunction("Detrended Price Oscillator", "DetrendedPriceOscillator(<Data Array>,<Time Periods>)", 0), //done
            new PasteFunction("Intraday Momentum Index", "IntradayMomentumIndex(<Time Periods>)", 0), //done
            new PasteFunction("Klinger Oscillator", "KlingerOscillator()", 0),//done
            new PasteFunction("Linear Regression", "LinearRegression(<Data Array>, <Time Periods>)", 29),//done
            new PasteFunction("MACD", "MACD(<Data Array>)", 0),   //done
            new PasteFunction("Mass Index", "MassIndex(<Time Periods>)", 0),//done
            new PasteFunction("Median Price", "MedianPrice()", 0), //done
            new PasteFunction("Momentum", "Momentum(<Data Array>,<Time Periods>)", 0),//done
            new PasteFunction("Money Flow Index", "MoneyFlowIndex(<Time Periods>)", 0),//done
            new PasteFunction("Moving Average", "MovingAverage(<Data Array>, <Time Periods>, <Method>)", 39),//done
            new PasteFunction("Negative Volume Index", "NegativeVolumeIndex()", 0),//done
            new PasteFunction("Parabolic SAR", "ParabolicSAR(<Deviations>,<Deviations>)", 0),//done
            new PasteFunction("Price Oscillator", "PriceOscillator(<DataArray>,<ShortTerm Time Periods>,<LongTerm Time Periods>,<Method>)", 0),//done
            new PasteFunction("PriceROC", "PriceROC(<Data Array>,<Time Periods>,<Method>)", 0),//done
            new PasteFunction("Qstick", "Qstick(<Time Periods>)", 0),//done
            new PasteFunction("Relative Strength Index", "RelativeStrengthIndex(<Data Array>,<Time Periods>)", 0),//done
            new PasteFunction("Relative Volatility Index", "RelativeVolatilityIndex(<Time Periods>)", 0),//done
            new PasteFunction("Relative Momentum Index", "RelativeMomentumIndex(<Time Periods>,<Time Periods>)", 0),//done
            new PasteFunction("Standard Deviation", "StandardDeviation(<Data Array>, <Time Periods>, <Deviations>)", 43), //done
            new PasteFunction("StochasticMomentumIndex", "StochasticMomentumIndex(<Time Periods>, <Smoothing Periods>,<DoubleSmoothing Periods>)", 0), //done
            new PasteFunction("StochasticOscillator", "StochasticOscillator(<Time Periods>,<Time Periods>)", 0), //done
            new PasteFunction("TEMA", "TEMA(<Data Array>,<Time Periods>)", 0),//done
            new PasteFunction("TimeSeriesForecast", "TimeSeriesForecast(<Data Array>,<Time Periods>)", 0), //done
            new PasteFunction("TRIX", "TRIX(<Data Array>, <Time Periods>)", 0),//done
            new PasteFunction("TypicalPrice", "TypicalPrice()", 0),   //done
            new PasteFunction("VerticalHorizontalFilter", "VerticalHorizontalFilter(<Data Array>,<Time Periods>)", 0),
            new PasteFunction("VolumeROC", "VolumeROC(<Time Periods>)", 0),  //done
            new PasteFunction("VolumeOscillator", "VolumeOscillator(<ShortTerm Time Periods>,<LongTerm Time Periods>,<Method>)", 0),//done
            new PasteFunction("Weighted Close", "WeightedClose()", 0), //done
            new PasteFunction("WildersSmoothing", "WildersSmoothing(<Data Array>,<Time Periods>)", 0),//done
            new PasteFunction("Williams % R", "WilliamsR(<Time Periods>)", 0), //done
            new PasteFunction("Wills Accumulation Distribution", "WillsAccumDistribution()", 0),
            new PasteFunction("ZigZag", "ZigZag(<Data Array>,<Amount>,<Method>)", 0), //done
            new PasteFunction("CoppockCurve", "CoppockCurve(<Data Array>,<CopCurve Period>)"), //done

            /*
            new PasteFunction("MACD Volume", "MACDVolume(<Data Array>,<Time Periods>)", 0), //edited
            new PasteFunction("Market Facilitation Index", "MarketFacilitationIndex()", 0),
            new PasteFunction("Ease Of Movement", "EaseOfMovement()", 0),  //TODO:
            new PasteFunction("Envelope", "Envelope(<Data Array>, <Time Periods>, <Method> ,<Percentage>)", 0),
            new PasteFunction("Forcast Oscillator", "ForcastOscillator(<Time Periods>, <Data Array> ,<Time Periods>)", 0),
            new PasteFunction("Ichimoku Kinko Hyo", "IchimokuKinkoHyo()", 0),
            new PasteFunction("On Balance Volume", "OnBalanceVolume(<Time Periods>)", 0),    //TODO:
            new PasteFunction("StandardError", "StandardError(<Time Periods>,<Data Array>)", 0), //TODO:
            new PasteFunction("Performance", "Performance(<Data Array>)", 0),   //TODO:
            new PasteFunction("Positive Volume Index", "PositiveVolumeIndex(<Data Array>)", 0),
            new PasteFunction("Price Channel", "PriceChannel(<Time Periods>)", 0),   //TODO:
            new PasteFunction("Price Volume Trend", "PriceVolumeTrend(<Data Array>)", 0),
            new PasteFunction("Aroon", "Aroon(<Time Periods>)", 0),
            new PasteFunction("Volatility (Chaikin's)", "Volatility(<Time Periods>,<Rate of Change>)", 0),
            */
    };

    private Object[] Logical = {
            new PasteFunction("And", "and"),
            new PasteFunction("Equal", "=="),
            new PasteFunction("Greater Than", ">"),
            new PasteFunction("Greater Than or Equal", ">="),
            new PasteFunction("Less Than", "<"),
            new PasteFunction("Less Than or Equal", "<="),
            new PasteFunction("Not Equal", "<>"),
            new PasteFunction("Or", "or")
    };

    private Object[] Math = {
            new PasteFunction("Absolute Value", "abs()", 1),
            new PasteFunction("Arc Cosine", "acos()", 1),
            new PasteFunction("Arc Sine", "asin()", 1),
            new PasteFunction("Arc Tangent", "atan()", 1),
            new PasteFunction("Arc Tangent 2", "atan2()", 1),
            new PasteFunction("Ceiling", "ceiling()", 1),
            new PasteFunction("Cosine", "cos()", 1),
            new PasteFunction("Cumulative Summation", "cum()", 1),
            new PasteFunction("Exponent", "exp()", 1),
            new PasteFunction("Floor", "floor()", 1),
            new PasteFunction("Fractional Part", "frac()", 1),
            new PasteFunction("Hyperbolic Cosine", "cosh()", 1),
            new PasteFunction("Hyperbolic Sine", "sinh()", 1),
            new PasteFunction("Hyperbolic Tangent", "tanh()", 1),
            new PasteFunction("Integral Part", "trunc()", 1),
            new PasteFunction("Logarithm (Base 10)", "log()", 1),
            new PasteFunction("Logarithm (Natural)", "ln()", 1),
            new PasteFunction("Maximum", "max()", 1),
            new PasteFunction("Minimum", "min()", 1),
            new PasteFunction("Power", "pow()", 1),
            new PasteFunction("Precision", "prec(<Data Array>, <Precision>)", 26),
            new PasteFunction("Round", "round()", 1),
            new PasteFunction("Sign", "sign()", 1),
            new PasteFunction("Sine", "sin()", 1),
            new PasteFunction("Square", "sqr()", 1),
            new PasteFunction("Square Root", "sqrt()", 1),
            new PasteFunction("Summation", "sum(<Data Array>, <Periods>)", 24),
            new PasteFunction("Tangent", "tan()", 1)
    };

    private Object[] Operators = {
            new PasteFunction("Add", "+"),
            new PasteFunction("Divide", "/"),
            new PasteFunction("Multiply", "*"),
            new PasteFunction("Substract", "-")
    };

    private Object[] Constants = {
            new PasteFunction("Boolean: False", "false"),
            new PasteFunction("Boolean: True", "true"),
            new PasteFunction("Calculation Method: Percent", "Percent"),
            new PasteFunction("Calculation Method: Price", "Price"),
            new PasteFunction("Color: Black", "Black"),
            new PasteFunction("Color: Blue", "Blue"),
            new PasteFunction("Color: Brown", "Brown"),
            new PasteFunction("Color: Cyan", "Cyan"),
            new PasteFunction("Color: Gray", "Gray"),
            new PasteFunction("Color: Green", "Green"),
            new PasteFunction("Color: Lime", "Lime"),
            new PasteFunction("Color: Magenta", "Magenta"),
            new PasteFunction("Color: Maroon", "Maroon"),
            new PasteFunction("Color: Navy", "Navy"),
            new PasteFunction("Color: Olive", "Olive"),
            new PasteFunction("Color: Orange", "Orange"),
            new PasteFunction("Color: Pink", "Pink"),
            new PasteFunction("Color: Purple", "Purple"),
            new PasteFunction("Color: Red", "Red"),
            new PasteFunction("Color: RGB (Red-Green-Blue)", "RGB(<Red>, <Green>, <Blue>)", 23),
            new PasteFunction("Color: Silver", "Silver"),
            new PasteFunction("Color: White", "White"),
            new PasteFunction("Color: Yellow", "Yellow"),
            new PasteFunction("Label Position: Above", "Above"),
            new PasteFunction("Label Position: Below", "Below"),
            new PasteFunction("Label Position: Hidden", "Hidden"),
            new PasteFunction("Line Style: Solid", "Solid"),
            new PasteFunction("Line Style: Dot", "Dot"),
            new PasteFunction("Line Style: Dash", "Dash"),
            new PasteFunction("Line Style: DashDot", "DashDot"),
            new PasteFunction("Line Style: DashDotDot", "DashDotDot"),
            new PasteFunction("Moving Average Calculation Method: Exponential", "Exponential"),
            new PasteFunction("Moving Average Calculation Method: Simple", "Simple"),
            new PasteFunction("Moving Average Calculation Method: Time Series", "TimeSeries"),
            new PasteFunction("Moving Average Calculation Method: Triangular", "Triangular"),
            new PasteFunction("Moving Average Calculation Method: Variable", "Variable"),
            new PasteFunction("Moving Average Calculation Method: Volume Adjusted", "VolumeAdjusted"),
            new PasteFunction("Moving Average Calculation Method: Weighted", "Weighted"),
            new PasteFunction("Coppock Curve Period: Monthly", "Monthly"),
            new PasteFunction("Coppock Curve Period: Daily", "Daily"),
            new PasteFunction("Symbol: Buy Arrow", "BuyArrow"),
            new PasteFunction("Symbol: Sell Arrow", "SellArrow"),
            new PasteFunction("Symbol: Thumbs Up", "ThumbsUp"),
            new PasteFunction("Symbol: Thumbs Down", "ThumbsDown"),
            new PasteFunction("Symbol: Happy Face", "HappyFace"),
            new PasteFunction("Symbol: Sad Face", "SadFace"),
            new PasteFunction("Symbol: Flag", "Flag"),
            new PasteFunction("Symbol: Bomb", "Bomb"),
            new PasteFunction("Symbol: Circle", "Circle"),
            new PasteFunction("Symbol: Diamond", "Diamond"),
            new PasteFunction("Symbol Position: Above", "Above"),
            new PasteFunction("Symbol Position: Middle", "Middle"),
            new PasteFunction("Symbol Position: Center", "Center"),
            new PasteFunction("Symbol Position: Below", "Below")
    };
    private Object[] PlotFunctions = {
            new PasteFunction("Draw Symbol (basic)", "drawsymbol (<Boolean Expression>, <Symbol>)"),
            new PasteFunction("Draw Symbol (with color)", "drawsymbol (<Boolean Expression>, <Symbol>, <Color>)"),
            new PasteFunction("Draw Symbol (with position)", "drawsymbol (<Boolean Expression>, <Symbol>, <Color>, <Symbol Position>)"),
            new PasteFunction("Mark Candlestick Pattern (basic)", "markpattern (<Data Array>)"),
            new PasteFunction("Mark Candlestick Pattern (with No. of bars)", "markpattern (<Data Array>, <No. of Bars>)"),
            new PasteFunction("Mark Candlestick Pattern (with color)", "markpattern (<Data Array>, <No. of Bars>, <Color>)"),
            new PasteFunction("Mark Candlestick Pattern (with border color)", "markpattern (<Data Array>, <No. of Bars>, <Color>, <Color>)"),
            new PasteFunction("Mark Candlestick Pattern (with label position)", "markpattern (<Data Array>, <No. of Bars>, <Color>, <Color>, <Label Position>)"),
            new PasteFunction("Plot (basic)", "plot <Data Array>"),
            new PasteFunction("Plot (with color)", "plot (<Data Array>, <Color>)"),
            new PasteFunction("Plot (with line style)", "plot (<Data Array>, <Color>, <Line Style>)"),
            new PasteFunction("Plot (with line thickness)", "plot (<Data Array>, <Color>, <Line Style>, <Line Thickness>)"),
            new PasteFunction("Plot (all params)", "plot (<Data Array>, <Up Color>, <Down Color>, <Line Style>, <Line Thickness>)"),
    };

    private Object[] All = {
            new PasteFunction("Absolute Value", "abs()", 1),
            new PasteFunction("Add", "+"),
            new PasteFunction("And", "and"),
            new PasteFunction("Arc Cosine", "acos()", 1),
            new PasteFunction("Arc Sine", "asin()", 1),
            new PasteFunction("Arc Tangent", "atan()", 1),
            new PasteFunction("Arc Tangent 2", "atan2()", 1),
            new PasteFunction("Boolean: False", "false"),
            new PasteFunction("Boolean: True", "true"),
            new PasteFunction("Calculation Method: Percent", "Percent"),
            new PasteFunction("Calculation Method: Price", "Price"),
            new PasteFunction("Ceiling", "ceiling()", 1),
            new PasteFunction("Close", "Close"),
            new PasteFunction("Color: Black", "Black"),
            new PasteFunction("Color: Blue", "Blue"),
            new PasteFunction("Color: Brown", "Brown"),
            new PasteFunction("Color: Cyan", "Cyan"),
            new PasteFunction("Color: Gray", "Gray"),
            new PasteFunction("Color: Green", "Green"),
            new PasteFunction("Color: Lime", "Lime"),
            new PasteFunction("Color: Magenta", "Magenta"),
            new PasteFunction("Color: Maroon", "Maroon"),
            new PasteFunction("Color: Navy", "Navy"),
            new PasteFunction("Color: Olive", "Olive"),
            new PasteFunction("Color: Orange", "Orange"),
            new PasteFunction("Color: Pink", "Pink"),
            new PasteFunction("Color: Purple", "Purple"),
            new PasteFunction("Color: Red", "Red"),
            new PasteFunction("Color: RGB (Red-Green-Blue)", "RGB(<Red>, <Green>, <Blue>)", 23),
            new PasteFunction("Color: Silver", "Silver"),
            new PasteFunction("Color: White", "White"),
            new PasteFunction("Color: Yellow", "Yellow"),
            new PasteFunction("Cosine", "cos()", 1),
            new PasteFunction("Cumulative Summation", "cum()", 1),
            new PasteFunction("Divide", "/"),
            new PasteFunction("Draw Symbol (basic)", "drawsymbol (<Boolean Expression>, <Symbol>)"),
            new PasteFunction("Draw Symbol (with color)", "drawsymbol (<Boolean Expression>, <Symbol>, <Color>)"),
            new PasteFunction("Draw Symbol (with position)", "drawsymbol (<Boolean Expression>, <Symbol>, <Color>, <Symbol Position>)"),
            new PasteFunction("Equal", "=="),
            new PasteFunction("Exponent", "exp()", 1),
            new PasteFunction("Floor", "floor()", 1),
            new PasteFunction("Fractional Part", "frac()", 1),
            new PasteFunction("Greater Than", ">"),
            new PasteFunction("Greater Than or Equal", ">="),
            new PasteFunction("High", "High"),
            new PasteFunction("Highest", "highest(<Data Array>)", 12),
            new PasteFunction("Highest Bars Ago", "highestbars(<Data Array>)", 12),
            new PasteFunction("Highest High Value", "hhv(<Data Array>, <Periods>)", 24),
            new PasteFunction("Highest High Value Bars Ago", "hhvbars(<Data Array>, <Periods>)", 24),
            new PasteFunction("Highest Since", "highestsince(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Highest Since Bars Ago", "highestsincebars(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Hyperbolic Cosine", "cosh()", 1),
            new PasteFunction("Hyperbolic Sine", "sinh()", 1),
            new PasteFunction("Hyperbolic Tangent", "tanh()", 1),
            new PasteFunction("Integral Part", "trunc()", 1),
            new PasteFunction("Label Position: Above", "Above"),
            new PasteFunction("Label Position: Below", "Below"),
            new PasteFunction("Label Position: Hidden", "Hidden"),
            new PasteFunction("Less Than", "<"),
            new PasteFunction("Less Than or Equal", "<="),
            new PasteFunction("Line Style: Solid", "Solid"),
            new PasteFunction("Line Style: Dot", "Dot"),
            new PasteFunction("Line Style: Dash", "Dash"),
            new PasteFunction("Line Style: DashDot", "DashDot"),
            new PasteFunction("Line Style: DashDotDot", "DashDotDot"),
            new PasteFunction("Linear Regression", "LinearRegression(<Data Array>, <Time Periods>)", 29),
            new PasteFunction("Logarithm (Base 10)", "log()", 1),
            new PasteFunction("Logarithm (Natural)", "ln()", 1),
            new PasteFunction("Low", "Low"),
            new PasteFunction("Lowest", "lowest(<Data Array>)", 12),
            new PasteFunction("Lowest Bars Ago", "lowestbars(<Data Array>)", 12),
            new PasteFunction("Lowest High Value", "llv(<Data Array>, <Periods>)", 24),
            new PasteFunction("Lowest High Value Bars Ago", "llvbars(<Data Array>, <Periods>)", 24),
            new PasteFunction("Lowest Since", "lowestsince(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Lowest Since Bars Ago", "lowestsincebars(<Nth>, <Boolean Expression>, <Data Array>)", 42),
            new PasteFunction("Mark Candlestick Pattern (basic)", "markpattern (<Data Array>)"),
            new PasteFunction("Mark Candlestick Pattern (with No. of bars)", "markpattern (<Data Array>, <No. of Bars>)"),
            new PasteFunction("Mark Candlestick Pattern (with color)", "markpattern (<Data Array>, <No. of Bars>, <Color>)"),
            new PasteFunction("Mark Candlestick Pattern (with border color)", "markpattern (<Data Array>, <No. of Bars>, <Color>, <Color>)"),
            new PasteFunction("Mark Candlestick Pattern (with label position)", "markpattern (<Data Array>, <No. of Bars>, <Color>, <Color>, <Label Position>)"),
            new PasteFunction("Maximum", "max()", 1),
            new PasteFunction("Minimum", "min()", 1),
            new PasteFunction("Moving Average", "MovingAverage(<Data Array>, <Time Periods>, <Method>)", 39),
            new PasteFunction("Moving Average Calculation Method: Exponential", "Exponential"),
            new PasteFunction("Moving Average Calculation Method: Simple", "Simple"),
            new PasteFunction("Moving Average Calculation Method: Time Series", "TimeSeries"),
            new PasteFunction("Moving Average Calculation Method: Triangular", "Triangular"),
            new PasteFunction("Moving Average Calculation Method: Variable", "Variable"),
            new PasteFunction("Moving Average Calculation Method: Volume Adjusted", "VolumeAdjusted"),
            new PasteFunction("Moving Average Calculation Method: Weighted", "Weighted"),
            new PasteFunction("Coppock Curve Period: Monthly", "Monthly"),
            new PasteFunction("Coppock Curve Period: Daily", "Daily"),
            new PasteFunction("Multiply", "*"),
            new PasteFunction("Not Equal", "<>"),
            new PasteFunction("Open", "Open"),
            new PasteFunction("Or", "or"),
            new PasteFunction("Plot (basic)", "plot <Data Array>"),
            new PasteFunction("Plot (with color)", "plot (<Data Array>, <Color>)"),
            new PasteFunction("Plot (with line style)", "plot (<Data Array>, <Color>, <Line Style>)"),
            new PasteFunction("Plot (with line thickness)", "plot (<Data Array>, <Color>, <Line Style>, <Line Thickness>)"),
            new PasteFunction("Plot (all params)", "plot (<Data Array>, <Up Color>, <Down Color>, <Line Style>, <Line Thickness>)"),
            new PasteFunction("Power", "pow()", 1),
            new PasteFunction("Precision", "prec(<Data Array>, <Precision>)", 26),
            new PasteFunction("Reference", "ref(<Data Array>, <Count>)", 22),
            new PasteFunction("Round", "round()", 1),
            new PasteFunction("Sign", "sign()", 1),
            new PasteFunction("Sine", "sin()", 1),
            new PasteFunction("Square", "sqr()", 1),
            new PasteFunction("Square Root", "sqrt()", 1),
            new PasteFunction("Standard Deviation", "StandardDeviation(<Data Array>, <Time Periods>, <Deviations>)", 43),
            new PasteFunction("Substract", "-"),
            new PasteFunction("Summation", "sum(<Data Array>, <Periods>)", 24),
            new PasteFunction("Symbol: Buy Arrow", "BuyArrow"),
            new PasteFunction("Symbol: Sell Arrow", "SellArrow"),
            new PasteFunction("Symbol: Thumbs Up", "ThumbsUp"),
            new PasteFunction("Symbol: Thumbs Down", "ThumbsDown"),
            new PasteFunction("Symbol: Happy Face", "HappyFace"),
            new PasteFunction("Symbol: Sad Face", "SadFace"),
            new PasteFunction("Symbol: Flag", "Flag"),
            new PasteFunction("Symbol: Bomb", "Bomb"),
            new PasteFunction("Symbol: Circle", "Circle"),
            new PasteFunction("Symbol: Diamond", "Diamond"),
            new PasteFunction("Symbol Position: Above", "Above"),
            new PasteFunction("Symbol Position: Middle", "Middle"),
            new PasteFunction("Symbol Position: Center", "Center"),
            new PasteFunction("Symbol Position: Below", "Below"),
            new PasteFunction("Tangent", "tan()", 1),
            new PasteFunction("Volume", "Volume"),
            new PasteFunction("Yesterdays Close", "ref(Close, -1)"),
            new PasteFunction("Yesterdays High", "ref(High, -1)"),
            new PasteFunction("Yesterdays Low", "ref(Low, -1)"),
            new PasteFunction("Yesterdays Open", "ref(Open, -1)"),
            new PasteFunction("Yesterdays Volume", "ref(Volume, -1)")
    };

    private Object[][] categories;
    private IndicatorBuilderBasePanel indBuilderPanel;

    public FunctionDialog(IndicatorBuilderBasePanel builderPanel) {
        super(Client.getInstance().getFrame(), true);
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        this.indBuilderPanel = builderPanel;
        createUI();
        categories = new Object[][]{All, AnalysisTools, DataArrays, Indicators, Logical, Math, Operators, Constants, PlotFunctions};
    }

    public void createUI() {
        setTitle(Language.getString("CUSTOM_INDICATOR_FUNCTIONS_WINDOW_TITLE"));
        setSize(400, 300);
        //sha
        /*
        setIconifiable(true);
        setClosable(true);
        setMaximizable(true);*/

        setResizable(false);
        getContentPane().setLayout(new BorderLayout());
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        lstCategories = new JList(saCategories);
        JScrollPane scrollPane1 = new JScrollPane(lstCategories);
        Border categoriesBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CUSTOM_INDICATOR_CATEGORY"));
        scrollPane1.setBorder(categoriesBorder);
        lstCategories.setSelectedIndex(0);
        lstCategories.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                lstFunctions.setListData(categories[lstCategories.getSelectedIndex()]);
                lstFunctions.updateUI();
                lstFunctions.setSelectedIndex(0);
            }
        });
        if (Language.isLTR()) {
            getContentPane().add(scrollPane1, BorderLayout.WEST);
        } else {
            getContentPane().add(scrollPane1, BorderLayout.EAST);
        }

        lstFunctions = new JList(All);
        JScrollPane scrollPane2 = new JScrollPane(lstFunctions);
        Border functionsBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CUSTOM_INDICATOR_FUNCTION"));
        scrollPane2.setBorder(functionsBorder);
        getContentPane().add(scrollPane2, BorderLayout.CENTER);
        lstFunctions.addMouseListener(this);
        lstFunctions.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                lblFunction.setText(((PasteFunction) categories[lstCategories.getSelectedIndex()]
                        [lstFunctions.getSelectedIndex()]).value);
            }
        });

        JPanel southPanel = new JPanel(new BorderLayout());
        lblFunction = new JLabel("");
        Border functionLabelBorder = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CUSTOM_INDICATOR_PASTE_FORMULA"));
        lblFunction.setBorder(functionLabelBorder);
        southPanel.add(lblFunction, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();

        buttonPanel.setPreferredSize(new Dimension(400, 30));
        String[] PanelWidths_eastPanel = {"203", "85", "85", "17"};
        String[] PanelHeights_eastPanel = {"25"};
        FlexGridLayout flexGridLayout_eastPanel = new FlexGridLayout(PanelWidths_eastPanel, PanelHeights_eastPanel, 2, 1);
        buttonPanel.setLayout(flexGridLayout_eastPanel);
        buttonPanel.add(new JLabel());
        btnOK = new TWButton(Language.getString("OK"));
        btnOK.addActionListener(this);
        buttonPanel.add(btnOK);
        btnCancel = new TWButton(Language.getString("CANCEL"));
        buttonPanel.add(btnCancel);
        buttonPanel.add(new JLabel());
        btnCancel.addActionListener(this);
        southPanel.add(buttonPanel, BorderLayout.SOUTH);
        getContentPane().add(southPanel, BorderLayout.SOUTH);


        Theme.registerComponent(this);
        applyTheme();
        GUISettings.applyOrientation(this);
    }

    public void showWindow() {
//            Client.getInstance().getDesktop().add(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());
//            Client.getInstance().getDesktop().setLayer(this, GUISettings.INTERNAL_DIALOG_LAYER+1);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == btnOK) {
            setVisible(false);
            pasteFunction();
        } else if (source == btnCancel) {
            setVisible(false);
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            setVisible(false);
            pasteFunction();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    private void pasteFunction() {
        PasteFunction selectedFunction = (PasteFunction) categories[lstCategories.getSelectedIndex()]
                [lstFunctions.getSelectedIndex()];
        getToolkit().getSystemClipboard().setContents(new StringSelection(selectedFunction.value),
                selectedFunction);
        indBuilderPanel.getIndicatorTextPane().paste();
        indBuilderPanel.enableButtons();
        indBuilderPanel.markSyntax();
        indBuilderPanel.dirty = true;
        indBuilderPanel.getIndicatorTextPane().requestFocus();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    class PasteFunction implements ClipboardOwner {

        public String name;
        public String value;
        public int adjustment;

        public PasteFunction(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public PasteFunction(String name, String value, int adjustment) {
            this.name = name;
            this.value = value;
            this.adjustment = adjustment;
        }

        public String toString() {
            return this.name;
        }

        public void lostOwnership(Clipboard clipboard, Transferable contents) {
        }
    }
}
