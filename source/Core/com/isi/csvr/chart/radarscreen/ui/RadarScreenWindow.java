package com.isi.csvr.chart.radarscreen.ui;

import com.isi.csvr.Client;
import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.chart.radarscreen.engine.RadarConstants;
import com.isi.csvr.chart.radarscreen.engine.RadarResult;
import com.isi.csvr.chart.radarscreen.engine.RadarScreenUpdator;
import com.isi.csvr.chart.radarscreen.engine.RadarSettings;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.ColumnGroup;
import com.isi.csvr.util.GroupableTableHeader;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 8, 2009
 * Time: 2:23:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class RadarScreenWindow extends InternalFrame implements Themeable, TWTabbedPaleListener, ActionListener {

    private static final int WINDOW_WIDTH = 820;
    private static final int WINDOW_HEIGHT = 500;
    public static int FIXED_COLUMNS = 6;
    private static RadarScreenWindow window = null;
    private final String AVG_BUY = Language.getString("RADAR_AVG_BUY");
    private final String AVG_SELL = Language.getString("RADAR_AVG_SELL");
    private final String PCT_PROFIT = Language.getString("RADAR_PROFIT");
    private final ArrayList<String> exchangeNames = new ArrayList<String>();
    public long referenceTime = -1;
    private Color topPanelBgColor;
    private JPanel pnlUp;
    private JPanel pnlDown;
    private JPanel rightPanel;
    private TWTabbedPane tabPane;
    private JPanel tabPanel;
    private JPanel topPanel;
    private JPanel tablePanel;
    private TWComboBox cmbExchanges;
    private TWButton btnSettings;
    private JButton btnHelp;
    private ViewSetting oSetting = null;
    private RadarTableModel oModel;
    private Table radarTable = new Table();
    private SortButtonRenderer renderer;
    private Hashtable<String, RadarResult> resultStore = new Hashtable<String, RadarResult>();
    private String selectedExchange = "TDWL";
    private byte selectedInterval = RadarSettings.TIME_INTERVAL_60_MIN;
    private byte selectedTabIndex = 0;
    private TWDateFormat format = new TWDateFormat("yyyy MMM dd-HH:mm");
    private byte[] indexArray = null;
    private RadarScreenUpdator updatorThread = null;

    private RadarScreenWindow() {
        super(false);

        createUI();

        this.setSize(new Dimension((int) oSetting.getSize().getWidth(), (int) oSetting.getSize().getHeight()));
        this.setTitle(oSetting.getCaption());
        this.setLocation(oSetting.getLocation());
        this.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        this.setLayer(GUISettings.TOP_LAYER);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setIconifiable(true);
        this.setClosable(true);
        this.setResizable(true);
        this.setMaximizable(true);
        this.hideTitleBarMenu();
        this.addInternalFrameListener(this);
        this.applySettings();

        Client.getInstance().getDesktop().add(this);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        Application.getInstance().addApplicationListener(this);
    }

    public static RadarScreenWindow getSharedInstance() {
        if (window == null) {
            window = new RadarScreenWindow();
        }
        return window;
    }

    private void createUI() {

        topPanel = new JPanel();
        tabPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "dt");
        boolean[] intervals = RadarSettings.getIntervalSettings();
        int length = RadarSettings.getSeletedIntervalCount();
        indexArray = new byte[length];
        int count = 0;

        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i]) {
                tabPane.addTab(RadarSettings.INTERVAL_NAMES[i], new JPanel());
                indexArray[count] = (byte) i;
                count++;
            }
        }

        cmbExchanges = new TWComboBox();

        count = 0;
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = exchanges.nextElement();
            if (exchange.isDefault()) {
                cmbExchanges.addItem(new TWComboItem(exchangeNames.size(), exchange.getDescription()));
                exchangeNames.add(exchangeNames.size(), exchange.getSymbol());
                if (count == 0) {
                    selectedExchange = exchange.getSymbol();
                    count = -1;
                }
            }
        }

        topPanelBgColor = Theme.getOptionalColor("TAB_" + "DT_" + "BGCOLOR");

        rightPanel = new JPanel(new FlexGridLayout(new String[]{"140", "100", "30"}, new String[]{"22"}, 5, 4));
        rightPanel.setBackground(topPanelBgColor);
        topPanel.setLayout(new FlexGridLayout(new String[]{"100%", "280"}, new String[]{"30"}, 0, 0));
        tabPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30"}, 0, 0));
        tabPanel.add(tabPane);

        JPanel pnlTemp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"4", "20", "6"}, 0, 0));

        topPanel.setBackground(topPanelBgColor);
        pnlUp = new JPanel();
        pnlUp.setBackground(topPanelBgColor);
        pnlTemp.add(pnlUp);
        pnlTemp.add(tabPanel);
        pnlDown = new JPanel();
        pnlDown.setBackground(topPanelBgColor);
        pnlTemp.add(pnlDown);

        topPanel.add(pnlTemp);

        btnSettings = new TWButton(Language.getString("RADAR_SETTINGS"));
        btnSettings.addActionListener(this);
        btnHelp = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/charts/indicator_help.gif"));
        btnHelp.setContentAreaFilled(false);
        btnHelp.setBorder(BorderFactory.createEmptyBorder());
        btnHelp.addActionListener(this);
        rightPanel.add(cmbExchanges);
        rightPanel.add(btnSettings);
        rightPanel.add(btnHelp);

        topPanel.add(rightPanel);
        createTablePanel();
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 0, 0));
        this.add(topPanel);
        this.add(tablePanel);

        //load selected saved properties from workspace
        if (oSetting != null && oSetting.getProperty(ViewConstants.SELECTED_EXCHANGE) != null) {
            selectedExchange = oSetting.getProperty(ViewConstants.SELECTED_EXCHANGE);
        }
        if (oSetting != null && oSetting.getProperty(ViewConstants.SELECTED_EXCHANGE_INDEX) != null) {
            cmbExchanges.setSelectedIndex(Integer.parseInt(oSetting.getProperty(ViewConstants.SELECTED_EXCHANGE_INDEX)));
        }

        selectedInterval = indexArray[0];
        tabPane.setSelectedIndex(0);
        //load selected saved properties from workspace
        if (oSetting != null && oSetting.getProperty(ViewConstants.SELECTED_INTERVAL) != null) {
            selectedInterval = Byte.parseByte(oSetting.getProperty(ViewConstants.SELECTED_INTERVAL));
        }
        if (oSetting != null && oSetting.getProperty(ViewConstants.SELECTED_TAB_INDEX) != null) {
            tabPane.setSelectedIndex(Integer.parseInt(oSetting.getProperty(ViewConstants.SELECTED_TAB_INDEX)));
            selectedTabIndex = (byte) tabPane.getSelectedIndex();
        }

        cmbExchanges.addActionListener(this);
        tabPane.addTabPanelListener(this);
    }

    private void createTablePanel() {

        try {
            oModel = new RadarTableModel(resultStore);
            oSetting = ViewSettingsManager.getSummaryView("RADAR_SCREEN_VIEW");
            //GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("RADAR_SCREEN_TABLE_COLS"));

            int count = RadarSettings.getSelectedStrategyData().size();
            FIXED_COLUMNS = oSetting.getColumnHeadings().length;
            oSetting.temp = true;

            String[] tableCols = new String[FIXED_COLUMNS + count * RadarConstants.NO_OF_SUB_COLUMNS];
            String renderingIDs = oSetting.getRenderingIDs();
            String resBigInt = "";

            //BigInteger lCols = BigInteger.valueOf(0);

            for (int i = 0; i < tableCols.length; i++) {

                //lCols = lCols.add(pwr(i));
                if (i < FIXED_COLUMNS) {
                    tableCols[i] = oSetting.getColumnHeadings()[i];
                } else {
                    if (i % RadarConstants.NO_OF_SUB_COLUMNS == 0) {
                        tableCols[i] = AVG_BUY;
                    } else if (i % RadarConstants.NO_OF_SUB_COLUMNS == 1) {
                        tableCols[i] = AVG_SELL;
                    } else if (i % RadarConstants.NO_OF_SUB_COLUMNS == 2) {
                        tableCols[i] = PCT_PROFIT;
                    }
                    renderingIDs += String.valueOf(2);
                }
            }

            for (int i = tableCols.length - 1; i >= 0; i--) {
                if (i >= FIXED_COLUMNS) {
                    resBigInt += "1";
                } else {
                    resBigInt += "0";
                }
            }

            //oSetting.setColumns((lCols));
            //resBigInt = "111000000";
            oSetting.setFixedColumns(new BigInteger(resBigInt, 2));
            oSetting.setRenderingIDs(renderingIDs);
            oSetting.setColumnHeadings(tableCols);
            oSetting.setParent(this);
            this.setViewSetting(oSetting);
            oModel.setViewSettings(oSetting);

            radarTable.setSortingEnabled();
            radarTable.setG_iLastSortedCol(FIXED_COLUMNS + 2);
            radarTable.setGroupableHeaderModel(oModel);
            oModel.setTable(radarTable);
            this.setTable(radarTable);

            radarTable.getTable().setDefaultRenderer(Object.class, new RadarTableRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
            radarTable.getTable().setDefaultRenderer(Number.class, new RadarTableRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
            radarTable.getTable().setDefaultRenderer(Boolean.class, new RadarTableRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
            radarTable.getTable().setDefaultRenderer(Long.class, new RadarTableRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
            radarTable.getTable().setDefaultRenderer(Double.class, new RadarTableRenderer(oModel.getViewSettings().getColumnHeadings(), oModel.getRendIDs()));
            radarTable.getTable().getTableHeader().setReorderingAllowed(false);
            //radarTable.getPopup().enableUnsort(false);
            Theme.registerComponent(radarTable);

            TableColumnModel model = radarTable.getTable().getColumnModel();

            GroupableTableHeader header = (GroupableTableHeader) (radarTable.getTable().getTableHeader());
            count = RadarSettings.getSelectedStrategyData().size();

            for (int i = 0; i < count; i++) {
                ColumnGroup group = new ColumnGroup(RadarSettings.getSelectedStrategyData().get(i).getShortName());
                for (int j = 0; j < RadarConstants.NO_OF_SUB_COLUMNS; j++) {
                    group.add(model.getColumn(FIXED_COLUMNS + ((i * RadarConstants.NO_OF_SUB_COLUMNS) + j % RadarConstants.NO_OF_SUB_COLUMNS)));
                }
                header.addGroup(group);
            }
            renderer = new SortButtonRenderer(true);
            renderer.applyTheme();
            TableColumnModel colmodel = radarTable.getTable().getColumnModel();

            for (int i = 0; i < oModel.getColumnCount(); i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }
            radarTable.createGroupableTable();
            radarTable.setWindowType(ViewSettingsManager.RADAR_WATCH_LIST);
            radarTable.getTable().getTableHeader().setDefaultRenderer(renderer);
            radarTable.getTable().getTableHeader().updateUI();
            radarTable.getTable().getTableHeader().repaint();
            radarTable.getModel().updateGUI();

            //((SmartTable) radarTable.getTable()).adjustColumnWidthsToFit(40);
            tablePanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            tablePanel.add(radarTable);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tabStareChanged(TWTabEvent event) {

        int index = ((TWTabbedPane) event.getSource()).getSelectedIndex();

        if (selectedTabIndex != index) {
            resultStore.clear();
            selectedInterval = indexArray[index];
            updatorThread.forceStart();
            selectedTabIndex = (byte) index;
            oSetting.putProperty(ViewConstants.SELECTED_TAB_INDEX, selectedTabIndex);
            oSetting.putProperty(ViewConstants.SELECTED_INTERVAL, selectedInterval);
        }
        System.out.println("**** selected interval ****** " + RadarSettings.INTERVAL_NAMES[selectedInterval]);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(btnSettings)) {
            radarTable.saveColumnPositions(oSetting);
            RadarSettingsWindow.getSharedInstance().setVisible(true);
        } else if (e.getSource().equals(cmbExchanges)) {
            int index = cmbExchanges.getSelectedIndex();
            selectedExchange = exchangeNames.get(index);
            resultStore.clear();
            updatorThread.forceStart();
            oSetting.putProperty(ViewConstants.SELECTED_EXCHANGE, selectedExchange);
            oSetting.putProperty(ViewConstants.SELECTED_EXCHANGE_INDEX, cmbExchanges.getSelectedIndex());
        } else if (e.getSource() == btnHelp) {
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_RADAR_SCREEN"));
        }
    }

    public void updateTabs() {

        tabPane.removeTabPaneListener();
        int count = tabPane.getTabCount();
        for (int i = 0; i < count; i++) {
            tabPane.removeTab(0);
        }
        boolean[] intervals = RadarSettings.getIntervalSettings();
        indexArray = new byte[RadarSettings.getSeletedIntervalCount()];
        int index = 0;
        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i]) {
                tabPane.addTab(RadarSettings.INTERVAL_NAMES[i], new JPanel());
                indexArray[index] = (byte) i;
                index++;
            }
        }
        tabPane.setSelectedIndex(0);
        selectedTabIndex = 0;
        tabPane.addTabPanelListener(this);
        selectedInterval = indexArray[0];
        tabPane.updateUI();
        tabPane.repaint();
    }

    public void updateTable() {

        int count = RadarSettings.getSelectedStrategyData().size();
        //FIXED_COLUMNS = oSetting.getColumnHeadings().length;

        String[] tableCols = new String[FIXED_COLUMNS + count * RadarConstants.NO_OF_SUB_COLUMNS];
        String renderingIDs = oSetting.getRenderingIDs();
        BigInteger lCols = BigInteger.valueOf(0);

        for (int i = 0; i < tableCols.length; i++) {
            lCols = lCols.add(pwr(i));
            if (i < FIXED_COLUMNS) {
                tableCols[i] = oSetting.getColumnHeadings()[i];
            } else {
                if (i % RadarConstants.NO_OF_SUB_COLUMNS == 0) {
                    tableCols[i] = AVG_BUY;
                } else if (i % RadarConstants.NO_OF_SUB_COLUMNS == 1) {
                    tableCols[i] = AVG_SELL;
                } else if (i % RadarConstants.NO_OF_SUB_COLUMNS == 2) {
                    tableCols[i] = PCT_PROFIT;
                }
                renderingIDs += String.valueOf(2);
            }
        }

        String resBigInt = "";
        for (int i = tableCols.length - 1; i >= 0; i--) {
            if (i >= FIXED_COLUMNS) {
                resBigInt += "1";
            } else {
                resBigInt += "0";
            }
        }
        BigInteger savedCols = oSetting.getColumns();
        BigInteger savedFixedCols = savedCols.and(new BigInteger("63")); // take only first 6 bits
        lCols = lCols.xor(new BigInteger("63")); // make first 6 bits 0
        lCols = lCols.or(savedFixedCols); // replace first 6 bits with saved cols
        oSetting.setColumns(lCols);
        oSetting.setFixedColumns(new BigInteger(resBigInt, 2)); //toDO: important
        oSetting.setRenderingIDs(renderingIDs);
        oSetting.setColumnHeadings(tableCols);
        radarTable.getTable().createDefaultColumnsFromModel();
        radarTable.setColumnIdentifiers();

        TableColumnModel model = radarTable.getTable().getColumnModel();
        GroupableTableHeader header = (GroupableTableHeader) (radarTable.getTable().getTableHeader());
        count = RadarSettings.getSelectedStrategyData().size();
        for (int i = 0; i < count; i++) {
            ColumnGroup group = new ColumnGroup(RadarSettings.getSelectedStrategyData().get(i).getShortName());
            for (int j = 0; j < RadarConstants.NO_OF_SUB_COLUMNS; j++) {
                group.add(model.getColumn(FIXED_COLUMNS + ((i * RadarConstants.NO_OF_SUB_COLUMNS) + j % RadarConstants.NO_OF_SUB_COLUMNS)));
            }
            header.addGroup(group);
        }
        TableColumnModel colmodel = radarTable.getTable().getColumnModel();

        for (int i = 0; i < oModel.getColumnCount(); i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }

        radarTable.getTable().setTableHeader(header);
        radarTable.getTable().updateUI();
        oModel.adjustColumns(radarTable.getTable(), oSetting.getColumns());
        radarTable.updateUI();

        //applySettings();
        radarTable.getModel().applySettings();
        GUISettings.applyOrientation(radarTable);
        //((SmartTable) radarTable.getTable()).adjustColumnWidthsToFit(40);
    }

    public void saveColumnPositions() {
        try {
            if (oSetting.getColumnSettings() == null) {
                oSetting.restColumnSettings();
            }
            for (int i = 0; i < oSetting.getColumnHeadings().length; i++) {
                oSetting.getColumnSettings()[i][2] =
                        radarTable.getTable().convertColumnIndexToView(i);
                TableColumn oColumn = radarTable.getTable().getColumn("" + i);
                oSetting.getColumnSettings()[i][1] = oColumn.getWidth();
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }

    public void showWindow() {
        this.setVisible(true);

        try {
            if (this.isIcon())
                this.setIcon(false);
            this.setSelected(true);
        } catch (PropertyVetoException e) {

        }

        if (updatorThread == null) {
            updatorThread = new RadarScreenUpdator();
            updatorThread.start();
        }
        //System.out.println("++++++++++++++++++ " + (System.currentTimeMillis() - TIME) / 1000.0);
    }

    //gettters and setters
    public RadarScreenUpdator getUpdatorThread() {
        return updatorThread;
    }

    public long getReferenceTime() {
        if (referenceTime == -1) {
            long beginTime = ExchangeStore.getSharedInstance().getExchange(selectedExchange).getMarketDateTime();
            referenceTime = (beginTime - RadarSettings.getMaxmSelectedTimeInterval());
            System.out.println("exchange referece time to process results :" + format.format(referenceTime));
        }
        return referenceTime;
    }

    public Table getResultsTable() {
        return radarTable;
    }

    public String getSelectedExchange() {
        return selectedExchange;
    }

    public Hashtable<String, RadarResult> getResultStore() {
        return resultStore;
    }

    public byte getSelectedInterval() {
        return indexArray[tabPane.getSelectedIndex()];
    }

    public void applyTheme() {
        renderer.applyTheme();

        topPanelBgColor = Theme.getOptionalColor("TAB_" + "DT_" + "BGCOLOR");

        topPanel.setBackground(topPanelBgColor);
        pnlUp.setBackground(topPanelBgColor);
        pnlDown.setBackground(topPanelBgColor);
        rightPanel.setBackground(topPanelBgColor);

    }

    public void snapshotProcessingEnded(Exchange exchange) {

        if (exchange.getSymbol().equals(selectedExchange)) {
            updatorThread.forceStart();
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {

        updatorThread.setActive(false);
        updatorThread = null;
    }

}
