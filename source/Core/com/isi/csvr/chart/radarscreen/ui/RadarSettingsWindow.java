package com.isi.csvr.chart.radarscreen.ui;

import com.isi.csvr.Client;
import com.isi.csvr.chart.radarscreen.engine.RadarScreenUpdator;
import com.isi.csvr.chart.radarscreen.engine.RadarSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.TWComboBox;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 8, 2009
 * Time: 10:47:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarSettingsWindow extends JDialog implements ActionListener, Themeable, WindowListener {

    private static final int WINDOW_WIDTH = 320;
    private static final int WINDOW_HEIGHT = 490;
    private static RadarSettingsWindow window = null;
    private SettingsPanel pnlIntervalSettings;
    private SettingsPanel pnlStrategies;
    private TWButton btnApply;
    private TWButton btnCancel;
    private TWComboBox cmbRefreshIntervals;

    private RadarSettingsWindow() {

        super(Client.getInstance().getFrame());
        createUI();

        this.setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setResizable(false);
        this.setTitle(Language.getString("RADAR_SETTINGS"));
//        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        this.addWindowListener(this);

        setModal(true);
    }

    public static RadarSettingsWindow getSharedInstance() {
        if (window == null) {
            window = new RadarSettingsWindow();
        }
        return window;
    }

    private void createUI() {

        JPanel pnlFinal = new JPanel();
        //pnlFinal.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "2", "20", "170", "25", "170", "30"}, 0, 0));
        pnlFinal.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "185", "25", "180", "30"}, 0, 0));

        Font labelFont = new Font("Tahoma", Font.BOLD, 12);

        pnlIntervalSettings = new SettingsPanel(SettingsPanel.TYPE_INTERVAL);
        JScrollPane paneIntervals = new JScrollPane(pnlIntervalSettings, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        pnlStrategies = new SettingsPanel(SettingsPanel.TYPE_STRATEGIES);
        JScrollPane paneStrategies = new JScrollPane(pnlStrategies, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JPanel pnlResults = new JPanel(new FlexGridLayout(new String[]{"6", "100", "75", "90"}, new String[]{"20"}, 5, 0));

        pnlResults.add(new JPanel());
        JLabel lblReults = new JLabel(Language.getString("RADAR_REFRESH_INTERVAL"));
        lblReults.setFont(labelFont);
        pnlResults.add(lblReults);
        pnlResults.add(new JPanel());
        cmbRefreshIntervals = new TWComboBox(RadarSettings.REFRESH_INTERVAL_NAMES);

        pnlResults.add(cmbRefreshIntervals);

        //JPanel pnlInterval = new JPanel(new FlowLayout(FlowLayout.TRAILING, 16, 0));
        JPanel pnlInterval = new JPanel(new FlexGridLayout(new String[]{"120"}, new String[]{"25"}));
        JLabel lblInterval = new JLabel(Language.getString("RADAR_TIME_INTERVALS"));
        lblInterval.setFont(labelFont);
        pnlInterval.add(lblInterval);

        //JPanel pnlStrat = new JPanel(new FlowLayout(FlowLayout.TRAILING, 16, 0));
        JPanel pnlStrat = new JPanel(new FlexGridLayout(new String[]{"120"}, new String[]{"25"}));
        JLabel lblStrat = new JLabel(Language.getString("RADAR_STRATEGIES"));
        lblStrat.setFont(labelFont);
        pnlStrat.add(lblStrat);

        JPanel pnlButtons = new JPanel(new FlexGridLayout(new String[]{"135", "70", "70"}, new String[]{"22"}, 5, 5));
        pnlButtons.add(new JPanel());
        btnApply = new TWButton(Language.getString("RADAR_APPLY"));
        btnCancel = new TWButton(Language.getString("RADAR_CANCEL"));
        btnApply.addActionListener(this);
        btnCancel.addActionListener(this);
        pnlButtons.add(btnApply);
        pnlButtons.add(btnCancel);

        //pnlFinal.add(pnlResults);
        //pnlFinal.add(new JSeparator(JSeparator.HORIZONTAL));
        pnlFinal.add(pnlInterval);
        pnlFinal.add(paneIntervals);
        pnlFinal.add(pnlStrat);
        pnlFinal.add(paneStrategies);
        pnlFinal.add(pnlButtons);

        pnlFinal.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        GUISettings.applyOrientation(pnlInterval);
        GUISettings.applyOrientation(pnlStrat); //Dont know why
        this.add(pnlFinal);
    }

    public SettingsPanel getPnlIntervalSettings() {
        return pnlIntervalSettings;
    }

    public SettingsPanel getPnlStrategies() {
        return pnlStrategies;
    }

    public void showWindow() {
        this.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(btnApply)) {
            if (RadarSettings.getSeletedIntervalCount() < RadarSettings.MIN_INTERVALS ||
                    RadarSettings.getSelectedStrategyData().size() < RadarSettings.MIN_STRATEGIES) {
                JOptionPane.showMessageDialog(this, Language.getString("RADAR_SETTINGS_ERROR"), Language.getString("RADAR_ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            System.out.println("apply");

            RadarSettings.saveRadarSettings(RadarSettings.getIntervalSettings(), RadarSettings.getStrategyData(), RadarSettings.REFRESH_INTERVALS[cmbRefreshIntervals.getSelectedIndex()]);
            this.setVisible(false);

            RadarScreenWindow.getSharedInstance().getResultStore().clear();
            RadarScreenWindow.getSharedInstance().updateTabs();
            RadarScreenWindow.getSharedInstance().updateTable();
            RadarScreenUpdator thread = RadarScreenWindow.getSharedInstance().getUpdatorThread();

            thread.forceStart();

        } else if (e.getSource().equals(btnCancel)) {
            System.out.println("cancel");
            this.setVisible(false);
            window = null;
            RadarSettings.loadRadarSettings();
        }
    }

    public void windowClosing(WindowEvent e) {
        window = null;
        RadarSettings.loadRadarSettings();
    }

    public void windowClosed(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowOpened(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applyTheme() {
        window = null;
    }
}
