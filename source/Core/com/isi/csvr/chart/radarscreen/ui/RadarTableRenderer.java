package com.isi.csvr.chart.radarscreen.ui;

import com.isi.csvr.ImageBorder;
import com.isi.csvr.chart.radarscreen.engine.RadarConstants;
import com.isi.csvr.chart.radarscreen.engine.RadarResult;
import com.isi.csvr.chart.radarscreen.engine.StrategyResultInfo;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Jul 11, 2009
 * Time: 7:38:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarTableRenderer extends TWBasicTableRenderer {

    static ImageBorder imageBorder;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color upColor;
    private static Color downColor;
    private Color foreground = null;
    private Color background = null;
    private String g_sNA = "N/A";
    private int g_iCenterAlign;
    private int[] g_asRendIDs;
    private int g_iNumberAlign;
    private int g_iStringAlign;
    private TWDecimalFormat formatter = new TWDecimalFormat("#0.00");
    private VariationImage variationImage = null;
    private double doubleValue;
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00");
    public RadarTableRenderer(String[] cols, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        g_iCenterAlign = JLabel.CENTER;
        g_iNumberAlign = JLabel.RIGHT;
        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
        } else {
            g_iStringAlign = JLabel.RIGHT;
        }
        variationImage = new VariationImage();
        reload();
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            upColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            downColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            imageBorder = new ImageBorder(1, 0, 1, 0, g_oSelectedBG);
        } catch (Exception e) {
            g_oSelectedFG = Color.green;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {

    }

    public void propertyChanged(int property) {

    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);
        //lblRenderer.setFont(new Font("Arial", Font.BOLD, 11));
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;

            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;

            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }
        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = 0;
        try {
            iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        } catch (Exception e) {
            iRendID = 2;
        }
        lblRenderer.setIcon(null);
        lblRenderer.setToolTipText("");
        variationImage.setHeight(table.getRowHeight());
        switch (iRendID) {

            case 0: //symbol
            case 1: //stock name
                lblRenderer.setText(String.valueOf(value));
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;

            case 3:
            case 4:
                double doubleVal = Double.parseDouble(String.valueOf(value));
                if (Double.isNaN(doubleVal)) {
                    lblRenderer.setText("0.00");
                } else {
                    lblRenderer.setText(formatter.format(Double.parseDouble(String.valueOf(value))));
                }

                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                if (toDoubleValue(value) > 0) {
                    lblRenderer.setForeground(upColor);
                } else if (toDoubleValue(value) < 0) {
                    lblRenderer.setForeground(downColor);
                }
                break;

            case 9://%cash map
                doubleValue = Double.parseDouble(String.valueOf(value));
                variationImage.setType(VariationImage.TYPE_CASH_MAP);
                variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                lblRenderer.setForeground(Color.black);
                if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                    variationImage.setValue(0);
                    lblRenderer.setText("");
                } else {
                    variationImage.setValue(doubleValue);
                    lblRenderer.setIcon(variationImage);
                    lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                }
                lblRenderer.setBackground(Color.BLACK);
                if (isSelected) {
                    lblRenderer.setBorder(imageBorder);
                } else {
                    lblRenderer.setBorder(null);
                }
                break;

            //double values
            default:
                if ((column - RadarScreenWindow.FIXED_COLUMNS) % RadarConstants.NO_OF_SUB_COLUMNS == 2) {

                    RadarResult result = (RadarResult) table.getValueAt(row, -1);
                    Hashtable results = result.getStrategyResults();
                    int index = (column - RadarScreenWindow.FIXED_COLUMNS) / RadarConstants.NO_OF_SUB_COLUMNS;
                    StrategyResultInfo info = (StrategyResultInfo) results.get(new Integer(index));
                    if (info != null && info.getProfitType() == RadarConstants.PROFIT_TYPE_REALIZED) {
                        if (!isSelected) {
                            lblRenderer.setBackground(Theme.getColor("RADAR_TABLE_REALIZED_CELL_COLOR"));
                        }
                        lblRenderer.setToolTipText(Language.getString("RADAR_REALIZED"));
                    } else if (info != null && info.getProfitType() == RadarConstants.PROFIT_TYPE_UNREALIZED) {
                        if (!isSelected) {
                            lblRenderer.setBackground(Theme.getColor("RADAR_TABLE_UNREALIZED_CELL_COLOR"));
                        }
                        lblRenderer.setToolTipText(Language.getString("RADAR_UNREALIZED"));
                    }

                    //foreground color
                    if (info != null && Double.compare(info.getProfitRatio(), Double.NaN) != 0) {
                        String strText = formatter.format(info.getProfitRatio());
                        if (strText.equals("-0.00")) strText = "0.00";
                        if (!isSelected && Double.parseDouble(strText) > 0) {
                            lblRenderer.setForeground(Theme.getColor("RADAR_TABLE_REALIZED_CELL_FONT_COLOR"));
                        } else if (!isSelected && Double.parseDouble(strText) < 0) {
                            lblRenderer.setForeground(Theme.getColor("RADAR_TABLE_UNREALIZED_CELL_FONT_COLOR"));
                        } else if (!isSelected && Double.parseDouble(strText) == 0) {
                            lblRenderer.setForeground(Theme.getColor("RADAR_TABLE_ZERO_FONT_COLOR"));
                        }
                    }
                }

                if (Double.compare(RadarConstants.MINIMUM_PROFIT_RATIO, Double.parseDouble(String.valueOf(value))) == 0 ||
                        Double.compare(Double.NaN, Double.parseDouble(String.valueOf(value))) == 0) {
                    //lblRenderer.setText(g_sNA);
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                } else {
                    String txtValue = formatter.format(Double.parseDouble(String.valueOf(value)));
                    if (txtValue.equals("-0.00")) txtValue = "0.00";
                    lblRenderer.setText(txtValue);
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                }
        }

        return lblRenderer;
    }

    private double toDoubleValue(Object oValue) {
        try {
            return (Double) oValue;
        } catch (Exception e) {
            return Double.MAX_VALUE;
        }
    }


}

