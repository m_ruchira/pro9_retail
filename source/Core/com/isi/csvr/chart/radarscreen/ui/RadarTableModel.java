package com.isi.csvr.chart.radarscreen.ui;

import com.isi.csvr.chart.radarscreen.engine.RadarConstants;
import com.isi.csvr.chart.radarscreen.engine.RadarResult;
import com.isi.csvr.chart.radarscreen.engine.StrategyResultInfo;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.Collection;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 9, 2009
 * Time: 7:42:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class RadarTableModel extends CommonTable implements TableModel, CommonTableInterface {

    private Hashtable<String, RadarResult> resultStore;

    public RadarTableModel(Hashtable<String, RadarResult> store) {
        this.resultStore = store;
    }

    public int getRowCount() {
        /*if (RadarScreenWindow.getSharedInstance().getUpdatorThread().reforce) {
            return 0;
        }*/
        //System.out.println("resultStore.size()"+resultStore.size());
        return resultStore.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        Collection<RadarResult> r = resultStore.values();
        Object[] f = r.toArray();
        RadarResult result = (RadarResult) f[rowIndex];
        String key = result.getSymbol();
        Stock stock = DataStore.getSharedInstance().getStockObject(key);

        switch (columnIndex) {

            case -1:
                return result;
            case 0:
                return String.valueOf(stock.getSymbol());

            case 1:
                return String.valueOf(stock.getShortDescription());

            case 2:
                //return new Double(stock.getLastTradedPrice());
                return new Double(stock.getLastTradeValue());

            case 3:
                return new Double(stock.getChange());

            case 4:
                return new Double(stock.getPctRange());

            case 5:
                double cashIn = stock.getCashInTurnover();
                double cashOut = stock.getCashOutTurnover();
                double total = cashIn + cashOut;
                if (total != 0) return new Double(cashIn / total);
                return 0;

            default:
                Hashtable results = result.getStrategyResults();
                StrategyResultInfo info = (StrategyResultInfo) results.get(new Integer((columnIndex - RadarScreenWindow.FIXED_COLUMNS) / RadarConstants.NO_OF_SUB_COLUMNS));
                if (info == null) {
                    //return new Double(RadarConstants.MINIMUM_PROFIT_RATIO);
                    return new Double(Double.NaN);
                }

                if ((columnIndex - RadarScreenWindow.FIXED_COLUMNS) % RadarConstants.NO_OF_SUB_COLUMNS == 0) {
                    return new Double(info.getBuyAverage());
                } else if ((columnIndex - RadarScreenWindow.FIXED_COLUMNS) % RadarConstants.NO_OF_SUB_COLUMNS == 1) {
                    return new Double(info.getSellAverage());
                } else if ((columnIndex - RadarScreenWindow.FIXED_COLUMNS) % RadarConstants.NO_OF_SUB_COLUMNS == 2) {
                    return new Double(info.getProfitRatio());
                }
                return new Double(Double.NaN);
        }
    }

    public void setSymbol(String symbol) {

    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {

        switch (iCol) {
            case 0:
            case 1:
                return String.class;
            default:
                return Double.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void getColumnIndexToName() {

    }

}
