package com.isi.csvr.chart.radarscreen.ui;

import com.isi.csvr.chart.radarscreen.engine.RadarSettings;
import com.isi.csvr.chart.radarscreen.engine.StrategyData;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 8, 2009
 * Time: 10:32:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class SettingsPanel extends JPanel implements Themeable {

    public static byte TYPE_INTERVAL = 0;
    //private ArrayList<SettingsLineItem> itemList = new ArrayList<SettingsLineItem>();
    private int type = TYPE_INTERVAL;
    public static byte TYPE_STRATEGIES = 1;

    public SettingsPanel(int type) {
        super();
        this.type = type;
        createUI();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    private void createUI() {

        if (type == TYPE_INTERVAL) {

            final boolean[] intervals = RadarSettings.getIntervalSettings();
            if (intervals.length != RadarSettings.INTERVAL_NAMES.length) {
                JOptionPane.showMessageDialog(this, "data mismatch");
            }
            this.removeAll();
            //String[] widths = new String[]{"25", "5", "100%"};
            String[] widths = new String[]{"100%"};
            String[] heights = new String[RadarSettings.INTERVAL_NAMES.length];
            for (int i = 0; i < heights.length; i++) {
                heights[i] = "25";
            }
            this.setLayout(new FlexGridLayout(widths, heights, 0, 0));

            final ArrayList<JCheckBox> chkintervals = new ArrayList<JCheckBox>();

            for (int i = 0; i < RadarSettings.INTERVAL_NAMES.length; i++) {
                //final JCheckBox chkItem = new JCheckBox("", intervals[i]);
                final JCheckBox chkItem = new JCheckBox(RadarSettings.INTERVAL_NAMES[i], intervals[i]);
                final int iTemp = i;
                final int count = getNoOfIntervals(intervals);

                // commented on 2009-12-4 for a suggestion - mevan
                /*
                if (count == RadarSettings.MAX_TIME_INTERVALS && !chkItem.isSelected()) {
                    chkItem.setEnabled(false);
                } else {
                    chkItem.setEnabled(true);
                }
                */
                chkItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        intervals[iTemp] = chkItem.isSelected();
                        if (getNoOfIntervals(intervals) == RadarSettings.MAX_TIME_INTERVALS) {
                            //controlChckboxes(chkintervals, false);
                        } else {
                            controlChckboxes(chkintervals, true);
                        }
                    }
                });
                JLabel lblName = new JLabel(RadarSettings.INTERVAL_NAMES[i]);
                this.add(chkItem);
                //this.add(new JLabel());
                //this.add(lblName);
                lblName.setBorder(BorderFactory.createLineBorder(Color.RED));
                chkintervals.add(chkItem);
            }
        } else if (type == TYPE_STRATEGIES) {

            final ArrayList<StrategyData> strategies = RadarSettings.getStrategyData();
            final ArrayList<JCheckBox> chkstrategies = new ArrayList<JCheckBox>();

            //String[] widths = new String[]{"25", "5", "100%"};
            String[] widths = new String[]{"100%"};
            String[] heights = new String[strategies.size()];
            for (int i = 0; i < heights.length; i++) {
                heights[i] = "25";
            }
            this.setLayout(new FlexGridLayout(widths, heights, 0, 0));

            int count = getNoOfStrategies(strategies);
            for (int i = 0; i < strategies.size(); i++) {
                final StrategyData data = strategies.get(i);
                //final JCheckBox chkItem = new JCheckBox("", data.isSelected());
                final JCheckBox chkItem = new JCheckBox(data.getName(), data.isSelected());
                if (count == RadarSettings.MAX_STRATEGIES && !chkItem.isSelected()) {
                    chkItem.setEnabled(false);
                } else {
                    chkItem.setEnabled(true);
                }
                chkItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        data.setSelected(chkItem.isSelected());
                        if (getNoOfStrategies(strategies) == RadarSettings.MAX_STRATEGIES) {
                            controlChckboxes(chkstrategies, false);
                        } else {
                            controlChckboxes(chkstrategies, true);
                        }
                    }
                });
                JLabel lblName = new JLabel(data.getName());
                this.add(chkItem);
                //this.add(new JLabel());
                //this.add(lblName);

                chkstrategies.add(chkItem);
            }
        }
    }

    public int getNoOfIntervals(boolean[] intervals) {
        int count = 0;
        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i]) {
                count++;
            }
        }
        return count;
    }

    public int getNoOfStrategies(ArrayList<StrategyData> strategies) {
        int count = 0;
        for (int i = 0; i < strategies.size(); i++) {
            StrategyData data = strategies.get(i);
            if (data.isSelected()) {
                count++;
            }
        }
        return count;
    }

    public void controlChckboxes(ArrayList<JCheckBox> chkBoxes, boolean status) {
        for (int i = 0; i < chkBoxes.size(); i++) {
            if (!chkBoxes.get(i).isSelected()) {
                chkBoxes.get(i).setEnabled(status);
            }
        }
    }

    public void applyTheme() {
        this.updateUI();
    }


}
