package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDateFormat;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 7:02:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class SymbolLoader {

    private static Hashtable<Exchange, ArrayList<String>> symbols = new Hashtable<Exchange, ArrayList<String>>();

    private static TWDateFormat formatter = new TWDateFormat("yyyy MM dd - HH:mm:ss ");

    public static ArrayList<String> getSymbolList(Exchange exchange) {

        if (symbols.containsKey(exchange)) {
            return symbols.get(exchange);
        } else {
            ArrayList<String> symbolNames = new ArrayList<String>();
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                try {
                    Exchange ex = exchanges.nextElement();
                    if (ex == exchange && ex.isDefault()) {
                        String exhangeSymbol = ex.getSymbol();
                        Enumeration e = DataStore.getSharedInstance().getExchangeSymbolStore(exhangeSymbol).keys();
                        while (e.hasMoreElements()) {
                            String symbol = (String) e.nextElement();
                            //System.out.println("symbol: " + symbol);
                            Stock stock = DataStore.getSharedInstance().getExchangeSymbolStore(exhangeSymbol).get(symbol);
                            String key = stock.getKey();
                            //System.out.println("key: " + key);
                            if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                symbolNames.add(key);
                            }
                        }
                        symbols.put(exchange, symbolNames);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return symbols.get(exchange);
    }

}
