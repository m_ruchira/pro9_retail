package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 8, 2009
 * Time: 3:35:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class StrategyData {

    private String uniqueID = "RS_" + System.currentTimeMillis();
    private boolean isSelected = false;
    private String grammar;
    private String name;
    private String shortName;

    public StrategyData(String uniqueID, String name, String shortName, String grammar, boolean selected) {
        this.uniqueID = uniqueID;
        isSelected = selected;
        this.name = name;
        this.shortName = shortName;
        this.grammar = grammar;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getGrammar() {
        return grammar;
    }

    public void setGrammar(String grammar) {
        this.grammar = grammar;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getBuySellClassName(int logicType) {
        return "RS_" + uniqueID + "_" + logicType;
    }

    public String getBuySellClassNameForLoad(int logicType) {
        return "RS_" + uniqueID + "_" + logicType + "_" + Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
    }

    public String getBuyGrammar(String text) {

        text = text.trim();
        int beginIndex = text.indexOf("sell");
        int endIndex = text.indexOf(";", beginIndex);

        String textToRemoved = text.substring(beginIndex, endIndex + 1);
        String result = text.replace(textToRemoved, "");

        return result;
    }

    public String getSellGrammar(String text) {

        text = text.trim();
        int beginIndex = text.indexOf("buy");
        int endIndex = text.indexOf(";", beginIndex);

        String textToRemoved = text.substring(beginIndex, endIndex + 1);
        String result = text.replace(textToRemoved, "");

        return result;
    }

    private String getBuySellGrammar(String text, int logic) {

        text = text.trim();
        int beginIndex = 0;

        if (logic == RadarConstants.LOGIC_TYPE_BUY) {
            beginIndex = text.indexOf("sell");
        } else if (logic == RadarConstants.LOGIC_TYPE_SELL) {
            beginIndex = text.indexOf("buy");
        }

        int endIndex = text.indexOf(";", beginIndex);
        String textToRemoved = text.substring(beginIndex, endIndex + 1);
        String result = text.replace(textToRemoved, "");

        return result;
    }

    public String getBuyGrammar() {
        return getBuySellGrammar(grammar, RadarConstants.LOGIC_TYPE_BUY);
    }

    public String getSellGrammar() {
        return getBuySellGrammar(grammar, RadarConstants.LOGIC_TYPE_SELL);
    }

    public String getBuyClassName() {
        return getBuySellClassName(RadarConstants.LOGIC_TYPE_BUY);
    }

    public String getSellClassName() {
        return getBuySellClassName(RadarConstants.LOGIC_TYPE_SELL);
    }
}
