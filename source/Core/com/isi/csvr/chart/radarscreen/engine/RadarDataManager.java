package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.chart.ChartPoint;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManagerIF;
import com.isi.csvr.chart.backtesting.TransactionPoint;
import com.isi.csvr.scanner.Scans.ScanRecord;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 7:35:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarDataManager implements GraphDataManagerIF {

    private ArrayList alRecords = null;
    private RadarResult result = null;
    private ArrayList<RadarResult> results = null;
    private int currentBackTestLogic = RadarConstants.LOGIC_TYPE_BUY;
    private RadarTransactionPoint lastpoint;

    private ArrayList transactions = new ArrayList(); // type of <TransactionPoint>


    public RadarDataManager() {
    }

    //calls only for transaction types
    public ChartPoint getIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {

        RadarTransactionPoint tp = new RadarTransactionPoint(timeIndex, currentBackTestLogic);
        int searchIndex = Collections.binarySearch(alRecords, new ScanRecord(timeIndex));
        tp.setCurrentRecord((ScanRecord) alRecords.get(searchIndex));
        if (alRecords.size() > searchIndex + 1) {
            tp.setNextRecord((ScanRecord) alRecords.get(searchIndex + 1));
        }
        //ArrayList transactions = result.getTransactions();
        searchIndex = Collections.binarySearch(transactions, tp);
        if (searchIndex >= 0) {
            transactions.add(searchIndex, tp);
            lastpoint = tp;
        } else {
            transactions.add(-searchIndex - 1, tp);
            lastpoint = tp;
        }
        return tp;
    }

    public ChartPoint readChartPoint(long timeIndex, int graphIndex) {
        //ArrayList transactions = result.getTransactions();
        int searchIndex = Collections.binarySearch(transactions, new TransactionPoint(timeIndex, currentBackTestLogic));
        if (searchIndex >= 0) {
            return (ScanRecord) transactions.get(searchIndex);
        }
        return null;
    }


    public void removeIndicatorPoint(long timeIndex, int graphIndex, byte indicatorID) {
    }

    public int getClosestIndexFortheTime(long t) {
        int searchIndex = Collections.binarySearch(alRecords, new ScanRecord(t));
        if (searchIndex >= 0) {
            return searchIndex;
        } else {
            return Math.min(-searchIndex - 1, alRecords.size() - 1);
        }
    }

    public int getIndexForSourceCP(ChartProperties cp) {
        return 0;
    }

    //getters and setters
    public ArrayList getAlRecords() {
        return alRecords;
    }

    public void setAlRecords(ArrayList alRecords) {
        this.alRecords = alRecords;
    }

    public RadarResult getResult() {
        return result;
    }

    public void setResult(RadarResult result) {
        this.result = result;
    }

    public int getCurrentBackTestLogic() {
        return currentBackTestLogic;
    }

    public void setCurrentBackTestLogic(int currentBackTestLogic) {
        this.currentBackTestLogic = currentBackTestLogic;
    }

    public RadarTransactionPoint getLastTransactionPoint() {
        return lastpoint;
    }

    public ArrayList<RadarResult> getResults() {
        return results;
    }

    public void setResults(ArrayList<RadarResult> results) {
        this.results = results;
    }

    public RadarTransactionPoint getLastpoint() {
        return lastpoint;
    }

    public ArrayList getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList transactions) {
        this.transactions = transactions;
    }
}
