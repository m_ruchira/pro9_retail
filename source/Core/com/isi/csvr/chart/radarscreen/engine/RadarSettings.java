package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.shared.Language;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 8, 2009
 * Time: 3:15:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class RadarSettings {

    public static final long[] TIME_INTERVALS = new long[]{5 * 60 * 1000, 10 * 60 * 1000, 15 * 60 * 1000, 30 * 60 * 1000,
            60 * 60 * 1000, 24 * 60 * 60 * 1000, 7 * 24 * 60 * 60 * 1000};
    public static final long INTERVAL_5_MIN = TIME_INTERVALS[0];


    /*public static final long[] TIME_INTERVALS = new long[]{1 * 24 * 3600 * 1000L, 10 * 24 * 3600 * 1000, 10 * 24 * 3600 * 1000, 15 * 60 * 1000, 30 * 24 * 3600 * 1000L,
            60 * 24 * 3600 * 1000L, 80 * 24 * 3600 * 1000L, 100 * 24 * 3600 * 1000L};
    */
    public static final long INTERVAL_10_MIN = TIME_INTERVALS[1];
    public static final long INTERVAL_15_MIN = TIME_INTERVALS[2];
    public static final long INTERVAL_30_MIN = TIME_INTERVALS[3];
    public static final long INTERVAL_60_MIN = TIME_INTERVALS[4];
    public static final long INTERVAL_1_DAY = TIME_INTERVALS[5];
    public static final long INTERVAL_1_WEEK = TIME_INTERVALS[6];
    public static final String[] INTERVAL_NAMES = new String[]{Language.getString("RADAR_5_MIN"), Language.getString("RADAR_10_MIN"), Language.getString("RADAR_15_MIN"),
            Language.getString("RADAR_30_MIN"), Language.getString("RADAR_60_MIN"), Language.getString("RADAR_1_DAY"), Language.getString("RADAR_1_WEEK")};
    public static final byte TIME_INTERVAL_5_MIN = 0;
    public static final byte TIME_INTERVAL_10_MIN = 1;
    public static final byte TIME_INTERVAL_15_MIN = 2;
    public static final byte TIME_INTERVAL_30_MIN = 3;
    public static final byte TIME_INTERVAL_60_MIN = 4;
    public static final byte TIME_INTERVAL_1_DAY = 5;
    public static final byte TIME_INTERVAL_1_WEEK = 6;
    public static final int MAX_TIME_INTERVALS = 3;
    public static final int MAX_STRATEGIES = 3;
    public static final int MIN_STRATEGIES = 1;
    public static final int MIN_INTERVALS = 1;
    public static final long REFRESH_INTERVAL_20S = 20 * 1000;
    private static long refreshInterval = REFRESH_INTERVAL_20S;
    public static final long REFRESH_INTERVAL_40S = 40 * 1000;
    public static final long REFRESH_INTERVAL_60S = 60 * 1000;
    public static final long REFRESH_INTERVAL_NONE = -1;
    public static final long[] REFRESH_INTERVALS = new long[]{REFRESH_INTERVAL_20S, REFRESH_INTERVAL_40S, REFRESH_INTERVAL_60S, REFRESH_INTERVAL_NONE};
    public static final String[] REFRESH_INTERVAL_NAMES = new String[]{Language.getString("RADAR_REFRESH_20S"), Language.getString("RADAR_REFRESH_40S"),
            Language.getString("RADAR_REFRESH_60S"), Language.getString("RADAR_REFRESH_NONE")};
    //private static final String FILE_PATH = Settings.getAbsolutepath() + "radar screen/settings.xml";
    private static final String FILE_PATH = "C:\\Documents and Settings\\charithn\\Application Data\\Mubasher\\Mubasher Pro 9\\radarscreen\\settings.xml";
    private static final String ROOT_ELEMENT_RADAR_SETTINGS = "Settings";
    private static final String ELEMENT_TIME_INTERVALS = "TimeIntervals";
    private static final String ELEMENT_STRATEGIES = "Strategies";
    private static final String ELEMENT_STRATEGY = "Strategy";
    private static final String ELEMENT_STRATEGY_ID = "StrategyID";
    private static final String ELEMENT_STRATEGY_SELECTED = "IsSelected";
    private static final String ELEMENT_STRATEGY_NAME = "StrategyName";
    private static final String ELEMENT_STRATEGY_SHORT_NAME = "ShortName";
    private static final String ELEMENT_STRATEGY_GRAMMAR = "Grammar";
    //public static boolean[] intervalSettings = new boolean[]{true, false, false, true, false, true, false};
    public static boolean[] intervalSettings = null;
    public static ArrayList<StrategyData> strategySettings = null;
    public static long maxmSelectedInterval = -1;
    private static long MILLIS_PER_MIN = 60 * 1000;

    public static void loadRadarSettings() {
        if (strategySettings == null) {
            strategySettings = new ArrayList<StrategyData>();
        } else {
            strategySettings.clear();
        }

        File file = new File(RadarConstants.RADAR_SETTINGS_FILE_PATH);
        //File file = new File(FILE_PATH);
        if (!file.exists()) {
            return;
        }
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = ROOT_ELEMENT_RADAR_SETTINGS + "/" + ELEMENT_STRATEGIES + "/" + ELEMENT_STRATEGY;

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(file.toURI().toString());
            NodeList list = (NodeList) xpath.evaluate(expression, dom, XPathConstants.NODESET);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlID = elem.getElementsByTagName(ELEMENT_STRATEGY_ID);
                NodeList nlName = elem.getElementsByTagName(ELEMENT_STRATEGY_NAME);
                NodeList nlShortName = elem.getElementsByTagName(ELEMENT_STRATEGY_SHORT_NAME);
                NodeList nlSelect = elem.getElementsByTagName(ELEMENT_STRATEGY_SELECTED);
                NodeList nlGrammar = elem.getElementsByTagName(ELEMENT_STRATEGY_GRAMMAR);

                String name = nlName.item(0).getTextContent();
                String shortName = nlShortName.item(0).getTextContent();
                String id = nlID.item(0).getTextContent();
                boolean selected = Boolean.parseBoolean(nlSelect.item(0).getTextContent());
                String grammar = nlGrammar.item(0).getTextContent();

                StrategyData data = new StrategyData(id, name, shortName, grammar, selected);
                strategySettings.add(data);
                //System.out.println("" + name);
                //System.out.println("" + id);
                //System.out.println("" + selected);
                //System.out.println("" + grammar);
            }

            expression = ROOT_ELEMENT_RADAR_SETTINGS;
            NodeList listIntervals = (NodeList) xpath.evaluate(expression, dom, XPathConstants.NODESET);
            Element elem = (Element) listIntervals.item(0); // only one element

            NodeList nlItems = elem.getElementsByTagName(ELEMENT_TIME_INTERVALS);
            String strIitems = nlItems.item(0).getTextContent();
            //System.out.println("array : " + strIitems);
            String[] items = strIitems.split(";");
            intervalSettings = new boolean[items.length];
            for (int i = 0; i < items.length; i++) {
                intervalSettings[i] = Boolean.parseBoolean(items[i]);
                //System.out.println(intervalSettings[i]);
            }

            expression = ROOT_ELEMENT_RADAR_SETTINGS + "/RefreshInterval";
            NodeList listRefreshInterval = (NodeList) xpath.evaluate(expression, dom, XPathConstants.NODESET);
            Element elemRefresh = (Element) listRefreshInterval.item(0); // only one element
            refreshInterval = Long.parseLong(elemRefresh.getTextContent());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveRadarSettings(boolean intervals[], ArrayList<StrategyData> strategies, long refreshRate) {

        //copy to current settings
        intervalSettings = intervals;
        strategySettings = strategies;
        refreshInterval = refreshRate;

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_RADAR_SETTINGS);
            doc.appendChild(root);

            Element eleRefreshInterval = doc.createElement("RefreshInterval");
            root.appendChild(eleRefreshInterval);
            Text txt = doc.createTextNode(String.valueOf(refreshRate));
            eleRefreshInterval.appendChild(txt);

            Element eleIntervals = doc.createElement(ELEMENT_TIME_INTERVALS);
            root.appendChild(eleIntervals);

            String strresult = "";
            for (int j = 0; j < intervals.length; j++) {
                if (j != intervals.length - 1) {
                    strresult += String.valueOf(intervals[j]) + ";";
                } else {
                    strresult += String.valueOf(intervals[j]) + "";
                }
            }
            CDATASection eleInt = doc.createCDATASection(strresult.trim());
            eleIntervals.appendChild(eleInt);

            Element eleStrategies = doc.createElement(ELEMENT_STRATEGIES);
            root.appendChild(eleStrategies);

            for (int j = 0; j < strategies.size(); j++) {

                StrategyData sData = strategies.get(j);

                Element eleStrategy = doc.createElement(ELEMENT_STRATEGY);
                eleStrategies.appendChild(eleStrategy);

                Element eleID = doc.createElement(ELEMENT_STRATEGY_ID);
                String str = sData.getUniqueID();
                Text text = doc.createTextNode(str);
                eleStrategy.appendChild(eleID);
                eleID.appendChild(text);

                Element eleName = doc.createElement(ELEMENT_STRATEGY_NAME);
                String strName = sData.getName().trim();
                text = doc.createTextNode(strName);
                eleStrategy.appendChild(eleName);
                eleName.appendChild(text);

                Element eleShortName = doc.createElement(ELEMENT_STRATEGY_SHORT_NAME);
                String strShortName = sData.getShortName().trim();
                text = doc.createTextNode(strShortName);
                eleStrategy.appendChild(eleShortName);
                eleShortName.appendChild(text);

                Element eleSelected = doc.createElement(ELEMENT_STRATEGY_SELECTED);
                String strSelected = String.valueOf(sData.isSelected());
                text = doc.createTextNode(strSelected);
                eleStrategy.appendChild(eleSelected);
                eleSelected.appendChild(text);

                Element eleGrammar = doc.createElement(ELEMENT_STRATEGY_GRAMMAR);
                String strGrammar = sData.getGrammar();
                CDATASection cData = doc.createCDATASection(strGrammar.trim());
                eleStrategy.appendChild(eleGrammar);
                eleGrammar.appendChild(cData);

            }

            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "true");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(RadarConstants.RADAR_SETTINGS_FILE_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== RadaerSettings class ======");
        }
    }

    public static boolean[] getIntervalSettings() {
        if (intervalSettings == null) {
            loadRadarSettings();
        }
        return intervalSettings;
    }

    public static ArrayList<StrategyData> getStrategyData() {
        if (strategySettings == null) {
            loadRadarSettings();
        }
        return strategySettings;
    }

    public static ArrayList<StrategyData> getSelectedStrategyData() {
        if (strategySettings == null) {
            loadRadarSettings();
        }
        ArrayList<StrategyData> data = new ArrayList<StrategyData>();
        for (int i = 0; i < strategySettings.size(); i++) {
            if (strategySettings.get(i).isSelected()) {
                data.add(strategySettings.get(i));
            }
        }
        return data;
    }

    public static int getSeletedIntervalCount() {

        int count = 0;
        for (int i = 0; i < intervalSettings.length; i++) {
            if (intervalSettings[i]) {
                count++;
                maxmSelectedInterval = Math.max(maxmSelectedInterval, TIME_INTERVALS[i]);
            }
        }
        return count;
    }

    public static long getMaxmSelectedTimeInterval() {

        if (maxmSelectedInterval == -1) {
            for (int i = 0; i < intervalSettings.length; i++) {
                if (intervalSettings[i]) {
                    maxmSelectedInterval = Math.max(maxmSelectedInterval, TIME_INTERVALS[i]);
                }
            }
        }
        return maxmSelectedInterval;
    }

    public static long getRefreshInterval() {
        return refreshInterval;
    }

    public static void setRefreshInterval(long refreshInterval) {
        RadarSettings.refreshInterval = refreshInterval;
    }

    /*  public static void main(String[] args) {

        ArrayList<StrategyData> sData = new ArrayList<StrategyData>();
        sData.add(new StrategyData("2165489232", "test 1", "plot 1;", true));
        sData.add(new StrategyData("8679834343", "test 2", "plot 2;", false));
        sData.add(new StrategyData("9932323099", "test 3", "plot 3;", true));

        //RadarSettings.saveRadarSettings(new boolean[]{true, false, true, true, true, false, false}, sData);
        RadarSettings.loadRadarSettings();
    }*/


}
