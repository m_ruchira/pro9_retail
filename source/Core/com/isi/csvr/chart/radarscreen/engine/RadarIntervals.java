package com.isi.csvr.chart.radarscreen.engine;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 10, 2009
 * Time: 4:21:46 PM
 * To change this template use File | Settings | File Templates.
 */
public enum RadarIntervals {

    INTERVAL_5_MIN(1, 5 * 60 * 1000),
    INTERVAL_10_MIN(2, 5 * 60 * 1000),
    INTERVAL_15_MIN(4, 5 * 60 * 1000),
    INTERVAL_30_MIN(8, 5 * 60 * 1000),
    INTERVAL_60_MIN(16, 5 * 60 * 1000),
    INTERVAL_1_DAY(32, 5 * 60 * 1000),
    INTERVAL_1_WEEK(64, 5 * 60 * 1000);

    private int id;
    private long value;

    RadarIntervals(int id, long val) {
        this.value = val;
        this.id = id;
    }

    public static RadarIntervals get(int value) {
        switch (value) {
            case 1:
                return INTERVAL_5_MIN;
            case 2:
                return INTERVAL_10_MIN;
            case 4:
                return INTERVAL_15_MIN;
            case 8:
                return INTERVAL_30_MIN;
            case 16:
                return INTERVAL_60_MIN;
            case 32:
                return INTERVAL_1_DAY;
            default:
                return INTERVAL_1_WEEK;
        }
    }

    public static RadarIntervals[] getRadarIntervals(int value) {

        ArrayList<RadarIntervals> alIntervals = new ArrayList<RadarIntervals>();
        if ((value & INTERVAL_5_MIN.id) == INTERVAL_5_MIN.id) alIntervals.add(INTERVAL_5_MIN);
        if ((value & INTERVAL_10_MIN.id) == INTERVAL_10_MIN.id) alIntervals.add(INTERVAL_10_MIN);
        if ((value & INTERVAL_15_MIN.id) == INTERVAL_15_MIN.id) alIntervals.add(INTERVAL_15_MIN);
        if ((value & INTERVAL_30_MIN.id) == INTERVAL_30_MIN.id) alIntervals.add(INTERVAL_30_MIN);
        if ((value & INTERVAL_60_MIN.id) == INTERVAL_60_MIN.id) alIntervals.add(INTERVAL_60_MIN);
        if ((value & INTERVAL_1_DAY.id) == INTERVAL_1_DAY.id) alIntervals.add(INTERVAL_1_DAY);
        if ((value & INTERVAL_1_WEEK.id) == INTERVAL_1_WEEK.id) alIntervals.add(INTERVAL_1_WEEK);

        //return  alIntervals.toArray(RadarIntervals[].class);
        return null;
    }

    public static int getRadarIntervalGroupValue(RadarIntervals[] ris) {
        int result = 0;
        for (RadarIntervals ri : ris) {
            result |= ri.id;
        }
        return result;
    }

    public static void main(String[] args) {

        System.out.println(RadarIntervals.getRadarIntervals(19));
    }

    public long Value() {
        return value;
    }

    public int ID() {
        return id;
    }

}
