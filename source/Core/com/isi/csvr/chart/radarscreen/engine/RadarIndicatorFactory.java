package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.chart.customindicators.Encoder;
import com.isi.csvr.chart.customindicators.IndicatorConfigInfo;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.IndicatorParser;
import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.shared.Settings;
import com.mubasher.formulagen.TWCompiler;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 22, 2009
 * Time: 11:13:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarIndicatorFactory {

    public static void rebuildAssembly() {
        IndicatorParser parser = new IndicatorParser(Settings.CHART_DATA_PATH + "/CustomIndicator.cgt");
        buildRadarScreenStrategies(parser);
    }

    public static void deleteOldVersionFiles() {

        String versionNumber = Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
        File dir = new File(RadarConstants.RADAR_CLASS_PATH);

        String[] files = dir.list();

        boolean isNewVersion = false;
        if (files != null) {
            for (int k = 0; k < files.length; k++) {
                String name = files[k];
                if (name.contains("RS_") && !name.contains("_" + versionNumber)) {
                    isNewVersion = true;
                    break;
                }
            }
        }

        //isNewVersion = true;
        if (isNewVersion) {

            for (int k = 0; k < files.length; k++) {
                if (files[k].contains("RS_")) {
                    File file = new File(RadarConstants.RADAR_CLASS_PATH + files[k]);
                    boolean result = file.delete();
                }
            }
        }
    }


    public static void buildRadarScreenStrategies(IndicatorParser parser) {

        deleteOldVersionFiles();
        IndicatorConfigInfo[] arrInfo = getRadarScreenStrategyInfo();

        if (arrInfo != null) {
            for (int i = 0; i < arrInfo.length; i++) {
                File calssFile = new File(RadarConstants.RADAR_CLASS_PATH + arrInfo[i].ClassName + ".ind");

                if (!calssFile.exists()) {
                    try {
                        CIProgram program = parser.parse(arrInfo[i].Grammer);
                        String code = IndicatorFactory.GenerateCode(arrInfo[i], program);
                        compileCode(code, arrInfo[i].ClassName);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                deleteSource(arrInfo[i].ClassName);
            }
        }
    }

    public static int compileCode(String source, String className) {
        int result = -1;
        try {
            result = TWCompiler.compile(getSourcePath(className, source), RadarConstants.RADAR_CLASS_PATH);

            if (result == 0) {
                File classFile = new File(RadarConstants.RADAR_CLASS_PATH + className + ".class");
                File fileRenamed = new File(RadarConstants.RADAR_CLASS_PATH + className + ".ind");
                if (fileRenamed.exists()) fileRenamed.delete();
                classFile.renameTo(fileRenamed);
                Encoder.encode(fileRenamed);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String getSourcePath(String className, String source) {
        String srcPath = (RadarConstants.RADAR_CLASS_PATH) + className + ".java";
        try {
            File file = new File(srcPath);
            if (file.exists()) file.delete();
            file.createNewFile();
            BufferedWriter bwriter = new BufferedWriter(new FileWriter(file));
            bwriter.write(source);
            bwriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return srcPath;
    }

    private static void deleteSource(String className) { // high priority
        String srcPath = (RadarConstants.RADAR_CLASS_PATH) + className + ".java";
        File file = new File(srcPath);
        if (file.exists()) file.delete();
    }

    private static IndicatorConfigInfo[] getRadarScreenStrategyInfo() {
        IndicatorConfigInfo[] infoArr = null;
        File file = new File(RadarConstants.RADAR_SETTINGS_FILE_PATH);
        if (file.exists()) {
            try {

                ArrayList<StrategyData> data = RadarSettings.getStrategyData();
                infoArr = new IndicatorConfigInfo[data.size() * 2];
                int count = 0;
                for (int i = 0; i < data.size(); i++) {

                    StrategyData dataItem = data.get(i);
                    //String grammer = dataItem.getGrammar();
                    String sname = dataItem.getShortName();
                    String name = dataItem.getName();
                    String buyclassName = dataItem.getBuyClassName() + "_" + Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
                    String sellclassName = dataItem.getSellClassName() + "_" + Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");

                    String buyGrammar = dataItem.getBuyGrammar();
                    //System.out.println("++++++++++ buy grammar ++++++++++\n "+buyGrammar);
                    String sellGrammar = dataItem.getSellGrammar();
                    //System.out.println("++++++++++ sell grammar +++++++++\n "+sellGrammar);

                    byte chartStyle = 0;
                    byte penStyle = 0;
                    infoArr[count] = new IndicatorConfigInfo("", buyGrammar, buyclassName, "true", sname, name, true, Color.BLACK, Color.BLACK, chartStyle, true, penStyle, 1f);
                    infoArr[count + 1] = new IndicatorConfigInfo("", sellGrammar, sellclassName, "true", sname, name, true, Color.BLACK, Color.BLACK, chartStyle, true, penStyle, 1f);
                    count = count + 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return infoArr;
    }
}
