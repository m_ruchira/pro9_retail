package com.isi.csvr.chart.radarscreen.engine;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 8:07:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarResult {

    private ArrayList<RadarTransactionPoint> transactions = new ArrayList<RadarTransactionPoint>();
    private String symbol;
    private StrategyData strategy;
    private ArrayList<StrategyResultInfo> resultInfo;
    private boolean isValid = false;
    //type of <strategy uniqueid, StrategyResultInfo>
    private Hashtable<Integer, StrategyResultInfo> strategyResults = new Hashtable<Integer, StrategyResultInfo>();

    public RadarResult(String symbol, StrategyData strategy) {
        this.strategy = strategy;
        this.symbol = symbol;
    }

    public RadarResult(String symbol) {
        this.symbol = symbol;
    }

    public ArrayList<RadarTransactionPoint> getTransactions() {
        return transactions;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public StrategyData getStrategy() {
        return strategy;
    }

    public void setStrategy(StrategyData strategy) {
        this.strategy = strategy;
    }

    public ArrayList<StrategyResultInfo> getProfitData() {
        return resultInfo;
    }

    public void setResultInfo(ArrayList<StrategyResultInfo> resultInfo) {
        this.resultInfo = resultInfo;
    }

    public Hashtable<Integer, StrategyResultInfo> getStrategyResults() {
        return strategyResults;
    }

    public void setStrategyResults(Hashtable<Integer, StrategyResultInfo> strategyResults) {
        this.strategyResults = strategyResults;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
