package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.chart.*;
import com.isi.csvr.chart.customindicators.IndicatorLoader;
import com.isi.csvr.chart.radarscreen.ui.RadarScreenWindow;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.scanner.Scans.ScanRecord;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDateFormat;

import java.awt.*;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 4:51:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class RadarScreenUpdator extends Thread {

    public final static int BUY = 0;
    public final static int SELL = 1;
    public long lastUpdateTime = -1;
    public boolean reforce = false; //used for breaking the scanRadar() method exucution and restart the thread
    private Hashtable<String, Integer> ohlcRecordCounter = new Hashtable<String, Integer>();
    private TWDateFormat formatter = new TWDateFormat("yyyy MMM dd - HH:mm");
    private String company = "TDWL~1010~0";
    private int count = 0;
    private boolean isActive = true;
    private long refreshInterval = RadarConstants.REFRESH_RATE;
    //for memory saving use global variables
    private RadarDataManager GDM = new RadarDataManager();
    private ArrayList<StrategyData> strategies;
    private String exchange;
    private byte selectedInterval;
    private long time;
    private ArrayList<String> symbols;
    private ArrayList<IntraDayOHLC> ohlcRecords;
    private ArrayList<ScanRecord> records;

    public RadarScreenUpdator() {
        super("****Radar screen data updator***");
        //this.setPriority(MAX_PRIORITY);
    }

    public static double calculateProfitsOldTest(int[] buySellArray, double[] priceArray) {

        double continuousProfit = 0;
        double realizedProfit = 0;
        final double lastTradedPrice = 100;
        double unrealizedBuySellDiff = 0;
        double unrealizedBuyTotal = 0;

        ArrayList<Double> continuousBuyPrices = new ArrayList<Double>();

        for (int i = 0; i < buySellArray.length; i++) {

            if (buySellArray[i] == BUY && (i + 1) <= buySellArray.length - 1 && buySellArray[i + 1] == SELL && continuousBuyPrices.size() == 0) {
                realizedProfit += (priceArray[i + 1] - priceArray[i]) / priceArray[i];
                i++;
                //} else if (buySellArray[i] == BUY && (i + 1) <= buySellArray.length - 1) {
            } else if (buySellArray[i] == BUY && (i + 1) <= buySellArray.length) {
                continuousBuyPrices.add(new Double(priceArray[i]));
                if (i == buySellArray.length - 1) {
                    double total = 0;
                    double tempBuyTotal = 0;
                    for (int j = 0; j < continuousBuyPrices.size(); j++) {
                        total += lastTradedPrice - continuousBuyPrices.get(j);
                        tempBuyTotal += continuousBuyPrices.get(j);
                    }
                    if (continuousBuyPrices.size() > 0) {
                        continuousProfit += total / tempBuyTotal;
                    }
                    continuousBuyPrices.clear();
                }
            } else if (buySellArray[i] == BUY && i == buySellArray.length - 1) {
                unrealizedBuySellDiff += lastTradedPrice - priceArray[i];
                unrealizedBuyTotal += priceArray[i];
            } else if (buySellArray[i] == SELL) {
                double total = 0;
                double tempBuyTotal = 0;
                for (int j = 0; j < continuousBuyPrices.size(); j++) {
                    total += priceArray[i] - continuousBuyPrices.get(j);
                    tempBuyTotal += continuousBuyPrices.get(j);
                }
                if (continuousBuyPrices.size() > 0) {
                    continuousProfit += total / tempBuyTotal;
                }
                continuousBuyPrices.clear();
            }
        }

        if (unrealizedBuyTotal > 0) {
            return (continuousProfit + realizedProfit + (unrealizedBuySellDiff / unrealizedBuyTotal)) * 100;
        }
        return (continuousProfit + realizedProfit) * 100;
    }

    public static double calculateProfitsNewTest(int[] buySellArray, double[] priceArray) {

        double realizedProfit = 0;
        double realizedBuyTotal = 0;
        double unrealizedProfit = 0;
        double unrealizedBuyTotal = 0;

        double pctProfit = 0;
        final double lastTradedPrice = 100;

        ArrayList<Double> continuousBuyPrices = new ArrayList<Double>();

        for (int i = 0; i < buySellArray.length; i++) {

            if (buySellArray[i] == BUY && (i + 1) <= buySellArray.length - 1 && buySellArray[i + 1] == SELL && continuousBuyPrices.size() == 0) { //buy,sell,buy,sell,buy,sell...
                realizedProfit += (priceArray[i + 1] - priceArray[i]);
                realizedBuyTotal += priceArray[i];
                i++;
            } else if (buySellArray[i] == BUY && (i + 1) <= buySellArray.length) { //buy,buy,buy.....continues
                continuousBuyPrices.add(priceArray[i]);
                if (i == buySellArray.length - 1) { //buy,sell,buy,buy,buy. last 3 buy's are unrelaized.
                    for (int j = 0; j < continuousBuyPrices.size(); j++) {
                        unrealizedProfit += lastTradedPrice - continuousBuyPrices.get(j);
                        unrealizedBuyTotal += continuousBuyPrices.get(j);
                    }
                    continuousBuyPrices.clear();
                }
            } else if (buySellArray[i] == BUY && i == buySellArray.length - 1) { //unrealized profit. only buy. no matching sell. profit=(last-buy)/buy
                unrealizedProfit += lastTradedPrice - priceArray[i];
                unrealizedBuyTotal += priceArray[i];
            } else if (buySellArray[i] == SELL) {  //buy,buy,buy,sell
                for (int j = 0; j < continuousBuyPrices.size(); j++) {
                    realizedProfit += priceArray[i] - continuousBuyPrices.get(j);
                    realizedBuyTotal += continuousBuyPrices.get(j);
                }
                continuousBuyPrices.clear();
            }
        }

        if (unrealizedBuyTotal > 0) {
            System.out.println("************ unrealized *************");
        }

        if (realizedBuyTotal > 0 || unrealizedBuyTotal > 0) {
            pctProfit = 100 * (realizedProfit + unrealizedProfit) / (realizedBuyTotal + unrealizedBuyTotal);
            return pctProfit;
        }
        return 0;
    }

    private void scanRadar() throws Exception {

        //GDM = new RadarDataManager();
        strategies = RadarSettings.getSelectedStrategyData();

        exchange = RadarScreenWindow.getSharedInstance().getSelectedExchange();
        selectedInterval = RadarScreenWindow.getSharedInstance().getSelectedInterval();
        time = System.currentTimeMillis();
        symbols = SymbolLoader.getSymbolList(ExchangeStore.getSharedInstance().getExchange(exchange));

        //Logger.logMessage("time taken to load symbols for " + exchange + " : " + (System.currentTimeMillis() - time) / 1000.0);
        //Logger.logMessage("no of symbols loaded : " + symbols.size());

        //time = System.currentTimeMillis();
        //int limit = symbols.size();
        for (int i = 0; i < symbols.size(); i++) {
            if (reforce) break;
            //load the symbol one by one and do the scan for each strategy to save mermory
            String cmpny = symbols.get(i);
            ohlcRecords = OHLCStore.getInstance().addIntradayRequest(cmpny).getList();
            if (ohlcRecords.size() == 0) {
                //Logger.logMessage("no records found for symbol : " + cmpny);
                continue;
            }
            //for optimization
            if (!ohlcRecordCounter.containsKey(cmpny)) {
                ohlcRecordCounter.put(cmpny, ohlcRecords.size());
            } else if (ohlcRecordCounter.get(cmpny) == ohlcRecords.size()) {
                continue;  // no need to scan for this symbol
            } else if (ohlcRecords.size() > ohlcRecordCounter.get(cmpny)) {
                //Logger.logMessage("new data record came for : " + cmpny + " at " + formatter.format(System.currentTimeMillis()));
                ohlcRecordCounter.put(cmpny, ohlcRecords.size());
            }


            //long time2 = System.currentTimeMillis();
            records = getScanRecordList(ohlcRecords, cmpny);
            //Logger.logMessage("time taken to conversion from IntradayOHLC to ScanRecord " + (System.currentTimeMillis() - time2) / 1000.0);
            //Logger.logMessage("array list size loaded by the intraday request for " + cmpny + " : " + records.size());

            long latestTime = getLatestTimeMillisec(ohlcRecords, cmpny);
            long referenceTime = getBeginTime(selectedInterval, latestTime);

            //Logger.logMessage("latest data record for " + cmpny + " at : " + formatter.format(latestTime));
            //Logger.logMessage("begning data record for " + cmpny + " at : " + formatter.format(referenceTime));
            //Logger.logMessage("Selected time interval : " + RadarSettings.INTERVAL_NAMES[selectedInterval]);
            //Logger.logMessage("begning data record for " + cmpny + " at : " + formatter.format(referenceTime));

            GDM.setAlRecords(records);
            RadarResult result = new RadarResult(cmpny);

            for (int j = 0; j < strategies.size(); j++) {
                if (reforce) break;
                GDM.getTransactions().clear();
                StrategyData strategyData = strategies.get(j);

                scanBuySellSignal(GDM, RadarConstants.LOGIC_TYPE_BUY, strategyData);
                //int buy = GDM.getTransactions().size();
                //Logger.logMessage("buy transaction count : " + GDM.getTransactions().size());

                scanBuySellSignal(GDM, RadarConstants.LOGIC_TYPE_SELL, strategyData);
                //Logger.logMessage("sell transaction count : " + (GDM.getTransactions().size() - buy));

                if (validForProfitCalculation(GDM, referenceTime)) {//validate basi requirements
                    calculateProfits(cmpny, strategyData, j, referenceTime, result, GDM);
                }
            }
            if (result.isValid() && !reforce) {
                RadarScreenWindow.getSharedInstance().getResultStore().put(cmpny, result);
            }
        }
        //Table table = RadarScreenWindow.getSharedInstance().getResultsTable();

        //Logger.logMessage("no of symbols produceTd result       : " + RadarScreenWindow.getSharedInstance().getResultStore().size());
        //System.out.println("no of symbols produced result       : " + RadarScreenWindow.getSharedInstance().getResultStore().size());
        //Logger.logMessage("time taken for the full market scan : " + (System.currentTimeMillis() - time) / 1000.0);

        System.out.println("time taken for the full market scan : " + (System.currentTimeMillis() - time) / 1000.0);

        for (int i = 0; i < strategies.size(); i++) {
            StrategyData strategyData = strategies.get(i);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.StrategyMonitor, strategyData.getShortName());
        }

    }

    private void calculateProfits(String cmpny, StrategyData strategy, int index, long referenceTime, RadarResult result, RadarDataManager gdm) {

        //TWDateFormat format = new TWDateFormat("yyyy MM dd -HH:mm");
        ArrayList<RadarTransactionPoint> transactions = gdm.getTransactions();

        StrategyResultInfo resultInfo = new StrategyResultInfo();
        resultInfo.setTransactions(transactions);

        Hashtable<Integer, StrategyResultInfo> strategyResults = result.getStrategyResults();

        //variables to caclculate end result
        double buyTotal = 0;
        int buyCount = 0;
        double sellTotal = 0;
        int sellCount = 0;
        double realizedProfit = 0;
        double realizedBuyTotal = 0;
        double unrealizedProfit = 0;
        double lastTradedPrice = DataStore.getSharedInstance().getStockObject(cmpny).getLastTradeValue();
        double unrealizedBuyTotal = 0;
        ArrayList<Double> continuousBuyPrices = new ArrayList<Double>();

        ArrayList transactionList = getSubList(gdm.getTransactions(), referenceTime);

        for (int i = 0; i < transactionList.size(); i++) {

            RadarTransactionPoint tp = (RadarTransactionPoint) transactionList.get(i);
            RadarTransactionPoint nexttp = null;

            double currentBarPrice = tp.getCurrentRecord().getClose();
            double nextBarPrice = 0;

            if (i < transactionList.size() - 1) {
                nexttp = (RadarTransactionPoint) transactionList.get(i + 1);
                nextBarPrice = nexttp.getCurrentRecord().getClose();
            }
            if (reforce) break;
            long transActionTime = tp.getTime();
            if (transActionTime < referenceTime) {
                continue; //not a valid transaction to take for a calculation
            }
            /*if (!strategy.getName().equals("") && tp.getTransactionType() == RadarConstants.LOGIC_TYPE_BUY) {
                Logger.logMessage("buy signal generated from " + strategy.getName() + " for symbol " + cmpny + " at " + format.format(transActionTime));
            } else if (!strategy.getName().equals("") && tp.getTransactionType() == RadarConstants.LOGIC_TYPE_SELL) {
                Logger.logMessage("sell signal generated from " + strategy.getName() + " for symbol " + cmpny + " at " + format.format(transActionTime));
            }*/

            if (tp.getTransactionType() == RadarConstants.LOGIC_TYPE_BUY && (i + 1) <= transactionList.size() - 1 &&
                    nexttp.getTransactionType() == RadarConstants.LOGIC_TYPE_SELL && continuousBuyPrices.size() == 0) { //buy,sell,buy,sell,buy,sell...
                realizedProfit += (nextBarPrice - currentBarPrice);
                realizedBuyTotal += currentBarPrice;
                buyTotal += currentBarPrice;
                sellTotal += nextBarPrice;
                buyCount++;
                sellCount++;
                i++;
            } else if (tp.getTransactionType() == RadarConstants.LOGIC_TYPE_BUY && (i + 1) <= transactionList.size()) { //buy,buy,buy.....continues
                continuousBuyPrices.add(currentBarPrice);
                if (i == transactionList.size() - 1) { //buy,sell,buy,buy,buy. last 3 buy's are unrelaized.
                    for (int j = 0; j < continuousBuyPrices.size(); j++) {
                        unrealizedProfit += lastTradedPrice - continuousBuyPrices.get(j);
                        unrealizedBuyTotal += continuousBuyPrices.get(j);
                        buyTotal += continuousBuyPrices.get(j);
                        buyCount++;
                    }
                    continuousBuyPrices.clear();
                }
            } else if (tp.getTransactionType() == RadarConstants.LOGIC_TYPE_BUY && i == transactionList.size() - 1) { //unrealized profit at the end. only buy. no matching sell. profit=(last-buy)/buy
                unrealizedProfit += lastTradedPrice - currentBarPrice;
                unrealizedBuyTotal += currentBarPrice;
                buyTotal += currentBarPrice;
                buyCount++;
            } else if (tp.getTransactionType() == RadarConstants.LOGIC_TYPE_SELL) {  //buy,buy,buy,sell
                for (int j = 0; j < continuousBuyPrices.size(); j++) {
                    realizedProfit += currentBarPrice - continuousBuyPrices.get(j);
                    realizedBuyTotal += continuousBuyPrices.get(j);
                    buyTotal += continuousBuyPrices.get(j);
                    buyCount++;
                }

                if (continuousBuyPrices.size() > 0) {
                    sellTotal += currentBarPrice;
                    sellCount++;
                }
                continuousBuyPrices.clear();
            }

        }//end of for loop transactionList

        //these are actual values
        if (buyCount > 0) resultInfo.setBuyAverage(buyTotal / buyCount);
        if (sellCount > 0) resultInfo.setSellAverage(sellTotal / sellCount);

        if (unrealizedBuyTotal > 0) {
            resultInfo.setProfitType(RadarConstants.PROFIT_TYPE_UNREALIZED);
        } else if (realizedBuyTotal > 0) {
            resultInfo.setProfitType(RadarConstants.PROFIT_TYPE_REALIZED);
        }
        if (realizedBuyTotal > 0 || unrealizedBuyTotal > 0) {
            result.setValid(true);
            double pctProfit = 100 * (realizedProfit + unrealizedProfit) / (realizedBuyTotal + unrealizedBuyTotal);
            resultInfo.setProfitRatio(pctProfit);
        }

        strategyResults.put(new Integer(index), resultInfo);
    }

    private ArrayList getSubList(ArrayList originalList, long referenceTime) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < originalList.size(); i++) {
            if (((RadarTransactionPoint) originalList.get(i)).getTime() >= referenceTime) {   //TODO:important
                result.add(originalList.get(i));
            }
        }
        return result;
    }

    private void scanBuySellSignal(RadarDataManager gdm, int logic, StrategyData strategyData) throws Exception {

        gdm.setCurrentBackTestLogic(logic);
        Class indClass = Class.forName(strategyData.getBuySellClassNameForLoad(logic), false, new IndicatorLoader(RadarConstants.RADAR_CLASS_PATH));
        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
        ChartProperties indicator = (ChartProperties) constructor.newInstance(null, null, Color.red, null);

        if (indicator == null) throw new Exception("Indicator creation error - " + logic);
        ((IndicatorBase) indicator).setPlotID(0);
        indicator.setOHLCPriority(GraphDataManager.INNER_Close);
        ((Indicator) indicator).insertIndicatorToGraphStore(gdm.getAlRecords(), gdm, logic);
    }

    ///basic validation
    private boolean validForProfitCalculation(RadarDataManager gdm, long referenceTime) {

        //TWDateFormat format = new TWDateFormat("yyyy MMM dd");
        //String date = format.format(GDM.getLastpoint().getTime() * 60000L);
        if (gdm.getTransactions().size() == 0 || gdm.getLastpoint() == null) {
            return false;
        } else if (gdm.getLastpoint().getTime() * 60000L < referenceTime) {
            return false;
        }
        return true;
    }

    private long getBeginTime(byte timeInterval, long latestTime) {

        switch (timeInterval) {
            case RadarSettings.TIME_INTERVAL_5_MIN:
                return (latestTime - RadarSettings.INTERVAL_5_MIN);
            case RadarSettings.TIME_INTERVAL_10_MIN:
                return (latestTime - RadarSettings.INTERVAL_10_MIN);
            case RadarSettings.TIME_INTERVAL_15_MIN:
                return (latestTime - RadarSettings.INTERVAL_15_MIN);
            case RadarSettings.TIME_INTERVAL_30_MIN:
                return (latestTime - RadarSettings.INTERVAL_30_MIN);
            case RadarSettings.TIME_INTERVAL_60_MIN:
                return (latestTime - RadarSettings.INTERVAL_60_MIN);
            case RadarSettings.TIME_INTERVAL_1_DAY: //TODO:
                return (latestTime - RadarSettings.INTERVAL_1_DAY);
            case RadarSettings.TIME_INTERVAL_1_WEEK:
                return (latestTime - RadarSettings.INTERVAL_1_WEEK);

            default:
                return (latestTime - RadarSettings.INTERVAL_5_MIN);
        }
    }

    private long getLatestTimeMillisec(ArrayList al, String symbol) {

        long result = 0, time = 0;
        if (al.size() > 0) {
            IntraDayOHLC dR = (IntraDayOHLC) al.get(al.size() - 1);
            if ((dR.getTime() == 0) && (al.size() > 1)) {
                dR = (IntraDayOHLC) al.get(al.size() - 2);
            }
            time = dR.getTime() * 60000L;
            time += ChartInterface.getTimeZoneAdjustment(symbol, time, true);
            result = Math.max(time, result);
        }

        if (result > 0) return result;
        return System.currentTimeMillis();
    }

    private ArrayList<ScanRecord> getScanRecordList(ArrayList<IntraDayOHLC> records, String cmpny) {
        ArrayList<ScanRecord> result = new ArrayList<ScanRecord>();

        for (int i = 0; i < records.size(); i++) {
            IntraDayOHLC record = records.get(i);
            long time = (record.getTime() * 60000L) + ChartInterface.getTimeZoneAdjustment(cmpny, record.getTime(), true);
            result.add(new ScanRecord(time, record.getOpen(), record.getHigh(), record.getLow(), record.getClose(), 0, 0, record.getVolume()));
        }
        return result;
    }

    public void run() {

        while (isActive) {
            try {
                if (count < 3) {
                    refreshInterval = RadarConstants.INITIAL_PULSE_RATE;  //uses for accelaration of ohlc data request
                    count++;
                } else {
                    refreshInterval = RadarConstants.REFRESH_RATE;
                }
                if (System.currentTimeMillis() >= lastUpdateTime + refreshInterval || reforce) {
                    System.out.println("************ scanner started *******");
                    reforce = false;
                    scanRadar();
                    lastUpdateTime = System.currentTimeMillis();
                    //System.gc();
                }
                //if (count < 3) {
                Thread.sleep(1000); //uses for accelaration of ohlc data request
                //}
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void forceStart() {
        count = 0;
        lastUpdateTime = -1;
        reforce = true;
        ohlcRecordCounter.clear();
        interrupt();
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
