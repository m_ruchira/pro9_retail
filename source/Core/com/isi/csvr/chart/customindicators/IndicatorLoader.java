package com.isi.csvr.chart.customindicators;

import java.io.File;

/**
 * <p>Title: Mubasher Pro Chart</p>
 * <p>Company: Mubasher</p>
 * <p>Copyright: Mubasher (c) 2007</p>
 * Author: udaka
 * Date: Feb 14, 2007
 * Time: 1:00:16 AM
 */
public class IndicatorLoader extends ClassLoader {

    private String classPath = IndicatorFactory.classPath; //default class path

    public IndicatorLoader() {
    }

    public IndicatorLoader(String path) {
        this.classPath = path;
    }

    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] b = loadClassData(name);
        return defineClass(name, b, 0, b.length);
    }

    private byte[] loadClassData(String name) {
        try {
            name = name.replaceAll("\\.", "/");
            //File file = new File(IndicatorFactory.classPath + name + ".ind");
            File file = new File(classPath + name + ".ind");
//            byte[] data = new byte[(int)file.length()];
//            FileInputStream in = new FileInputStream(file);
//            for (int i = 0; i < data.length; i++) {
//                data[i] = (byte)in.read();
//            }
//            in.close();
//            in = null;
//            try {
//                file.delete();
//            } catch (Exception e) {}
            return Encoder.decodeFile(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
