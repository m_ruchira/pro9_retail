package com.isi.csvr.chart.customindicators;

/**
 * User: Pramoda
 * Date: Feb 19, 2007
 * Time: 11:36:55 AM
 */
public class IndicatorMenuObject {

    private String indicatorName;
    private Class indicatorClass;


    public IndicatorMenuObject(Class indicatorClass, String indicatorName) {
        this.indicatorClass = indicatorClass;
        this.indicatorName = indicatorName;
    }


    public Class getIndicatorClass() {
        return indicatorClass;
    }

    public void setIndicatorClass(Class indicatorClass) {
        this.indicatorClass = indicatorClass;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

}
