package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:42:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class CISymbol {
    private int symbol;

    public CISymbol(String symbl) {
        this.symbol = CISymbol.getSymbolFromString(symbl);
    }

    public CISymbol(int pos) {
        try {
            this.symbol = pos;
        } catch (Exception ex) {
            this.symbol = SymbolLiteral.BUYARROW;
        }
    }

    public static int getSymbolFromString(String symbl) {
        symbl = symbl.toUpperCase();
        if (symbl.equals("BUYARROW")) {
            return SymbolLiteral.BUYARROW;
        } else if (symbl.equals("SELLARROW")) {
            return SymbolLiteral.SELLARROW;
        } else if (symbl.equals("THUMBSUP")) {
            return SymbolLiteral.THUMBSUP;
        } else if (symbl.equals("THUMBSDOWN")) {
            return SymbolLiteral.THUMBSDOWN;
        } else if (symbl.equals("HAPPYFACE")) {
            return SymbolLiteral.HAPPYFACE;
        } else if (symbl.equals("SADFACE")) {
            return SymbolLiteral.SADFACE;
        } else if (symbl.equals("FLAG")) {
            return SymbolLiteral.FLAG;
        } else if (symbl.equals("BOMB")) {
            return SymbolLiteral.BOMB;
        } else if (symbl.equals("CIRCLE")) {
            return SymbolLiteral.CIRCLE;
        } else if (symbl.equals("DIAMOND")) {
            return SymbolLiteral.DIAMOND;
        } else {
            return SymbolLiteral.BUYARROW;
        }
    }

    public int getSymbol() {
        return symbol;
    }
}
