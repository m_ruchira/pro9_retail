package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:41:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class CIPosition {
    private int poistion;

    public CIPosition(String pos) {
        this.poistion = CIPosition.getPositionFromString(pos);
    }

    public CIPosition(int pos) {
        try {
            this.poistion = pos;
        } catch (Exception ex) {
            this.poistion = PositionLiteral.ABOVE;
        }
    }

    public static int getPositionFromString(String pos) {
        pos = pos.toUpperCase();
        if (pos.equals("ABOVE")) {
            return PositionLiteral.ABOVE;
        } else if (pos.equals("MIDDLE")) {
            return PositionLiteral.CENTER;
        } else if (pos.equals("CENTER")) {
            return PositionLiteral.CENTER;
        } else if (pos.equals("HIDDEN")) {
            return PositionLiteral.CENTER;
        } else if (pos.equals("BELOW")) {
            return PositionLiteral.BELOW;
        } else {
            return PositionLiteral.ABOVE;
        }
    }

    public int getPoistion() {
        return poistion;
    }
}
