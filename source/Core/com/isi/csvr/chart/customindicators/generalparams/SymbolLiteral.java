package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:49:25 PM
 * To change this template use File | Settings | File Templates.
 */
public interface SymbolLiteral {
    public final int BUYARROW = 1,
            SELLARROW = 2,
            THUMBSUP = 3,
            THUMBSDOWN = 4,
            HAPPYFACE = 5,
            SADFACE = 6,
            FLAG = 7,
            BOMB = 8,
            CIRCLE = 9,
            DIAMOND = 10,
            ONE_CANDLE_PATTERN = 1000,
            TWO_CANDLE_PATTERN = 1001,
            THREE_CANDLE_PATTERN = 1002,
            FOUR_CANDLE_PATTERN = 1003,
            FIVE_CANDLE_PATTERN = 1004,
            SIX_CANDLE_PATTERN = 1005,
            SEVEN_CANDLE_PATTERN = 1006,
            EIGHT_CANDLE_PATTERN = 1007,
            NINE_CANDLE_PATTERN = 1008,
            TEN_CANDLE_PATTERN = 1009,
            BUY = 9000,
            SELL = 9001,
            SELL_SHORT = 9002,
            BUY_TO_COVER = 9003;
}