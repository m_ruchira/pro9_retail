package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:45:13 PM
 * To change this template use File | Settings | File Templates.
 */
public interface StyleLiteral {
    public final int SOLID = 0,
            DASH = 1,
            DOT = 2,
            DASHDOT = 3,
            DASHDOTDOT = 4;
}
