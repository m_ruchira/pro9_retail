package com.isi.csvr.chart.customindicators.generalparams;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:41:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class CIColor {
    //Color
    private Color color;

    public CIColor(ColorLiteral color) {
        this.color = CIColor.getColorFromColorLiteral(color);
    }

    public CIColor(int R, int G, int B) {
        this.color = new Color(R, G, B);
    }

    private static Color getColorFromColorLiteral(ColorLiteral color) {
        switch (color) {
            case BLACK:
                return Color.BLACK;
            case BLUE:
                return Color.BLUE;
            case NAVY:
                return new Color(0, 0, 128);
            case GRAY:
                return Color.GRAY;
            case SILVER:
                return new Color(128, 128, 128);
            case RED:
                return Color.RED;
            case GREEN:
                return new Color(0, 128, 0);
            case YELLOW:
                return Color.YELLOW;
            case PINK:
                return Color.PINK;
            case ORANGE:
                return Color.ORANGE;
            case BROWN:
                return new Color(128, 64, 0);
            case WHITE:
                return Color.WHITE;
            case OLIVE:
                return new Color(128, 128, 0);
            case CYAN:
                return Color.CYAN;
            case PURPLE:
                return new Color(128, 0, 128);
            case MAGENTA:
                return Color.MAGENTA;
            case LIME:
                return new Color(0, 255, 0);
            case MAROON:
                return new Color(128, 0, 0);
            default:
                return Color.BLACK;
        }
    }

    public static CIColor createLiteral(String color) {
        return new CIColor(CIColor.getLiteralForString(color));
    }

    public static ColorLiteral getLiteralForString(String color) {
        color = color.toUpperCase();
        if (color.equals("BLACK")) {
            return ColorLiteral.BLACK;
        } else if (color.equals("BLUE")) {
            return ColorLiteral.BLUE;
        } else if (color.equals("NAVY")) {
            return ColorLiteral.NAVY;
        } else if (color.equals("GRAY")) {
            return ColorLiteral.GRAY;
        } else if (color.equals("SILVER")) {
            return ColorLiteral.SILVER;
        } else if (color.equals("RED")) {
            return ColorLiteral.RED;
        } else if (color.equals("GREEN")) {
            return ColorLiteral.GREEN;
        } else if (color.equals("YELLOW")) {
            return ColorLiteral.YELLOW;
        } else if (color.equals("PINK")) {
            return ColorLiteral.PINK;
        } else if (color.equals("ORANGE")) {
            return ColorLiteral.ORANGE;
        } else if (color.equals("BROWN")) {
            return ColorLiteral.BROWN;
        } else if (color.equals("WHITE")) {
            return ColorLiteral.WHITE;
        } else if (color.equals("OLIVE")) {
            return ColorLiteral.OLIVE;
        } else if (color.equals("CYAN")) {
            return ColorLiteral.CYAN;
        } else if (color.equals("PURPLE")) {
            return ColorLiteral.PURPLE;
        } else if (color.equals("MAGENTA")) {
            return ColorLiteral.MAGENTA;
        } else if (color.equals("LIME")) {
            return ColorLiteral.LIME;
        } else if (color.equals("MAROON")) {
            return ColorLiteral.MAROON;
        } else {
            return ColorLiteral.BLACK;
        }
    }

    public Color getColor() {
        return color;
    }
}
