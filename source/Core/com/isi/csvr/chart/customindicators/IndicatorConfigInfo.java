package com.isi.csvr.chart.customindicators;

import java.awt.*;

/**
 * User: udaka
 * Date: Feb 4, 2007
 * Time: 6:04:53 PM
 */
public class IndicatorConfigInfo implements Comparable {
    public String NameSpace;
    public String Grammer;
    public String ClassName;
    public String HasItsOwnScale;
    public String ShortName;
    public String LongName; //Indicator Name
    public boolean IsStrategy;

    public Color Color;
    public Color WarningColor;
    public byte ChartStyle;
    public boolean UseSameColor;
    public byte PenStyle;
    public float PenWidth;

    public IndicatorConfigInfo(
            String NameSpace,
            String Grammer,
            String ClassName,
            String HasItsOwnScale,
            String ShortName,
            String LongName,
            boolean IsStrategy,
            Color Color,
            Color WarningColor,
            byte ChartStyle,
            boolean UseSameColor,
            byte PenStyle,
            float PenWidth
    ) {
        this.NameSpace = NameSpace;
        this.Grammer = Grammer;
        this.ClassName = ClassName;
        this.HasItsOwnScale = HasItsOwnScale;
        this.ShortName = ShortName;
        this.LongName = LongName;
        this.IsStrategy = IsStrategy;

        this.Color = Color;
        this.WarningColor = WarningColor;
        this.ChartStyle = ChartStyle;
        this.UseSameColor = UseSameColor;
        this.PenStyle = PenStyle;
        this.PenWidth = PenWidth;
    }

    public int compareTo(Object o) {
        return this.LongName.compareTo(((IndicatorConfigInfo) o).LongName);
    }
}
