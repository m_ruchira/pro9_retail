package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.StatementType;


public class CIObjectAssign extends CIStatement {

    private CIAssignment assignment;

    public CIObjectAssign(CIAssignment assignment) {
        super(StatementType.OBJECT_ASSIGN);
        this.assignment = assignment;
    }

    public CIAssignment getAssignment() {
        return assignment;
    }

    public String generateCode(CIProgram program) {
        return assignment.generateCode(program);
    }

}

