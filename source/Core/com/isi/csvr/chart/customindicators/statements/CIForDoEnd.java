package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;

import java.util.ArrayList;


public class CIForDoEnd extends CIStatement {

    private CIDeclareAssign initialAssign;
    private CIExpression boolExprssn;
    private CIAssignment loopAssign;
    private ArrayList statements;

    public CIForDoEnd(CIDeclareAssign initialAssign, CIExpression boolExprssn,
                      CIAssignment loopAssign, ArrayList statements) {
        super(StatementType.FOR_DO_END);
        this.initialAssign = initialAssign;
        this.boolExprssn = boolExprssn;
        this.loopAssign = loopAssign;
        this.statements = statements;
    }

    public CIDeclareAssign getInitialAssign() {
        return initialAssign;
    }

    public CIExpression getBoolExpression() {
        return boolExprssn;
    }

    public CIAssignment getLoopAssign() {
        return loopAssign;
    }

    public ArrayList getStatements() {
        return statements;
    }

    public String generateCode(CIProgram program) {
        ArrayList alTemp = program.collectTempVars();

        StringBuilder sb = new StringBuilder();
        boolean looping = program.addLoopingHeader(sb);

        sb.append("for(");
        sb.append(initialAssign.generateCode(program));
        sb.append(";");
        sb.append(boolExprssn.generateCode(program));
        sb.append(";");
        sb.append(loopAssign.generateCode(program));
        sb.append("){\n");
        for (int i = 0; i < statements.size(); i++) {
            CIStatement stmt = (CIStatement) statements.get(i);
            sb.append(stmt.generateCode(program));
            if ((stmt.getStatementType() == StatementType.OBJECT_ASSIGN) ||
                    (stmt.getStatementType() == StatementType.DECLARE_ASSIGN)) {
                String s = sb.toString();
                if (!s.endsWith(";") && !s.endsWith("\n") && !s.endsWith("}")) {
                    sb.append(";\n");
                }
            }
        }
        sb.append("}\n");

        if (looping) {
            program.addLoopingFooter(sb);
        }

        program.discardTempVars(alTemp);
        return sb.toString();
    }

}

