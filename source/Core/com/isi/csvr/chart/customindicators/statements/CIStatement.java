package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.ICodeGenerator;
import com.isi.csvr.chart.customindicators.StatementType;

public class CIStatement implements ICodeGenerator {

    private StatementType type;

    public CIStatement() {
        this.type = StatementType.EMPTY;
    }

    public CIStatement(StatementType type) {
        this.type = type;
    }

    public StatementType getStatementType() {
        return type;
    }

    public String generateCode(CIProgram program) {
        return "";
    }

}
