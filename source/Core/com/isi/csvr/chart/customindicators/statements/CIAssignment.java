package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.ICodeGenerator;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.ParamInfo;
import com.isi.csvr.chart.customindicators.ParamType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;


public class CIAssignment implements ICodeGenerator {

    //Identifier
    private String identifier;
    //Expression
    private CIExpression expression;

    public CIAssignment(String identifier, CIExpression expression) {
        this.identifier = identifier;
        this.expression = expression;
    }

    public String getIdentifier() {
        return identifier;
    }

    public CIExpression getExpression() {
        return expression;
    }

    public String generateCode(CIProgram program) {
        if (!program.htParams.containsKey(identifier)) {// DeclarationType.VAR
            StringBuilder sb = new StringBuilder();
            String stepName = program.getStepForVar(identifier);
            String code = expression.generateCode(program);
            if (stepName != null) {
                boolean looping = program.addLoopingHeader(sb);

                ParamInfo info = (ParamInfo) program.htParams.get(identifier);
                if (info != null && (info.ParamType == ParamType.DATA_ARRAY || info.ParamType == ParamType.MA_METHOD ||
                        info.ParamType == ParamType.CALC_METHOD || info.ParamType == ParamType.CC_PERIOD)) {
                    sb.append("    cR.setStepValue((byte)" + stepName + ".ordinal(), " + code + ");");//\n
                } else {
                    sb.append("    cR.setStepValue(" + stepName + ", " + code + ");");//\n
                }
                if (looping) {
                    program.addLoopingFooter(sb);
                }
            } else {
                sb.append(IndicatorFactory.Prefix + identifier);
                sb.append(" = ");
                sb.append(code);   // the end ";" is not written (commented below) as this is used inside loop assigns
                //sb.Append(";\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }


}


