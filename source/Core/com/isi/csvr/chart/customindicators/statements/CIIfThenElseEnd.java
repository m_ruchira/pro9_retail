package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;

import java.util.ArrayList;


public class CIIfThenElseEnd extends CIStatement {

    private CIExpression boolExprssn;
    private ArrayList ifStatements;
    private ArrayList elseStatements;

    public CIIfThenElseEnd(CIExpression boolExprssn, ArrayList ifStatements, ArrayList elseStatements) {
        super(StatementType.IF_THEN_ELSE_END);
        this.boolExprssn = boolExprssn;
        this.ifStatements = ifStatements;
        this.elseStatements = elseStatements;
    }

    public CIExpression getBoolExpression() {
        return boolExprssn;
    }

    public ArrayList getIfStatements() {
        return ifStatements;
    }

    public ArrayList getElseStatements() {
        return elseStatements;
    }

    public String generateCode(CIProgram program) {
        ArrayList alTemp = program.collectTempVars();

        StringBuilder sb = new StringBuilder();
        boolean looping = program.addLoopingHeader(sb);

        sb.append("if (");
        sb.append(boolExprssn.generateCode(program));
        sb.append("){\n");

        for (int i = 0; i < ifStatements.size(); i++) {
            CIStatement stmt = (CIStatement) ifStatements.get(i);
            sb.append(stmt.generateCode(program));
            if ((stmt.getStatementType() == StatementType.OBJECT_ASSIGN) ||
                    (stmt.getStatementType() == StatementType.DECLARE_ASSIGN)) {
                String s = sb.toString();
                if (!s.endsWith(";") && !s.endsWith("\n") && !s.endsWith("}")) {
                    sb.append(";\n");
                }
            }
        }
        sb.append("}else{\n");

        program.discardTempVars(alTemp);
        alTemp = program.collectTempVars();

        for (int i = 0; i < elseStatements.size(); i++) {
            CIStatement stmt = (CIStatement) elseStatements.get(i);
            sb.append(stmt.generateCode(program));
            if ((stmt.getStatementType() == StatementType.OBJECT_ASSIGN) ||
                    (stmt.getStatementType() == StatementType.DECLARE_ASSIGN)) {
                String s = sb.toString();
                if (!s.endsWith(";") && !s.endsWith("\n") && !s.endsWith("}")) {
                    sb.append(";\n");
                }
            }
        }
        sb.append("}\n");

        if (looping) {
            program.addLoopingFooter(sb);
        }

        program.discardTempVars(alTemp);
        return sb.toString();
    }
}

