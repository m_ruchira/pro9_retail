package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.DeclarationType;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.ParamType;
import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIIdentifier;

import java.util.ArrayList;


public class CIDeclareVar extends CIStatement {

    //DeclarationType
    private DeclarationType decType;
    //ParamType
    private ParamType paramType;
    //AlVars
    private ArrayList alVars;

    public CIDeclareVar(ArrayList alVars) {
        super(StatementType.DECLARE_VAR);
        this.alVars = alVars;
    }

    public DeclarationType getDeclarationType() {
        return decType;
    }

    public void setDeclarationType(DeclarationType decType) {
        this.decType = decType;
    }

    public ParamType getParamType() {
        return paramType;
    }

    public void setParamType(ParamType paramType) {
        this.paramType = paramType;
    }

    public ArrayList getAlVars() {
        return alVars;
    }

    public String generateCode(CIProgram program) {
        if (decType == DeclarationType.VAR) {
            StringBuilder sb = new StringBuilder();
            if (paramType == ParamType.DATA_ARRAY) {
                for (int i = 0; i < alVars.size(); i++) {
                    program.stepCount++;
                    String stepName = "STEP_" + program.stepCount;
                    program.addStepVar(stepName, ((CIIdentifier) alVars.get(i)).getIdentifier());
                }
            } else {
                sb.append(IndicatorFactory.getTypeString(paramType));
                sb.append(" ");
                for (int i = 0; i < alVars.size(); i++) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    CIIdentifier var = (CIIdentifier) alVars.get(i);
                    sb.append(var.generateCode(program));
                }
                sb.append(";\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }
}


