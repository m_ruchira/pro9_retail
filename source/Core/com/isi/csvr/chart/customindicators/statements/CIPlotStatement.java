package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.CustomIndicatorInterface;
import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;
import com.isi.csvr.chart.customindicators.generalparams.CIColor;
import com.isi.csvr.chart.customindicators.generalparams.CIPosition;
import com.isi.csvr.chart.customindicators.generalparams.CIStyle;
import com.isi.csvr.chart.customindicators.generalparams.CISymbol;

import java.awt.*;

public class CIPlotStatement extends CIStatement {
    //Expression
    private CIExpression expression;
    //Color
    private CIColor color;
    //WarningColor
    private CIColor wcolor;
    //Style
    private CIStyle style;
    //Thickness
    private float thickness;
    //Symbol
    private CISymbol symbol;
    //IsStrategy
    private boolean drawSymbol = false;
    //Position
    private CIPosition pos;

    public CIPlotStatement(CIExpression expression) {
        super(StatementType.PLOT);
        this.expression = expression;
    }

    public CIPlotStatement(CIExpression expression, CIColor color) {
        super(StatementType.PLOT);
        this.expression = expression;
        this.color = color;
        this.wcolor = color;
    }

    public CIPlotStatement(CIExpression expression, CIColor color, CIStyle style) {
        super(StatementType.PLOT);
        this.expression = expression;
        this.color = color;
        this.wcolor = color;
        this.style = style;
    }

    public CIPlotStatement(CIExpression expression, CIColor color, CIStyle style, float thickness) {
        super(StatementType.PLOT);
        this.expression = expression;
        this.color = color;
        this.wcolor = color;
        this.style = style;
        this.thickness = thickness;
    }

    public CIPlotStatement(CIExpression expression, CIColor upcolor, CIColor downcolor, CIStyle style, float thickness) {
        super(StatementType.PLOT);
        this.expression = expression;
        this.color = upcolor;
        this.wcolor = downcolor;
        this.style = style;
        this.thickness = thickness;
    }

    //used for drawsymbol
    public CIPlotStatement(CIExpression expression, CISymbol symbol) {
        super(StatementType.PLOT);
        this.drawSymbol = true;
        this.expression = expression;
        this.symbol = symbol;
    }

    //used for drawsymbol and markpattern
    public CIPlotStatement(CIExpression expression, CISymbol symbol, CIColor color) {
        super(StatementType.PLOT);
        this.drawSymbol = true;
        this.expression = expression;
        this.symbol = symbol;
        this.color = color;
        if (symbol.getSymbol() >= 1000) {
            this.pos = new CIPosition(0);
        }
    }

    //used for drawsymbol
    public CIPlotStatement(CIExpression expression, CISymbol symbol, CIColor color, CIPosition pos) {
        super(StatementType.PLOT);
        this.drawSymbol = true;
        this.expression = expression;
        this.symbol = symbol;
        this.color = color;
        this.pos = pos;
    }

    //used for markpattern
    public CIPlotStatement(CIExpression expression, CISymbol symbol, CIColor color, CIColor fontColor) {
        super(StatementType.PLOT);
        this.drawSymbol = true;
        this.expression = expression;
        this.symbol = symbol;
        this.color = color;
        this.wcolor = fontColor;
        this.pos = new CIPosition(0);
    }

    //used for markpattern
    public CIPlotStatement(CIExpression expression, CISymbol symbol, CIColor color, CIColor fontColor, CIPosition pos) {
        super(StatementType.PLOT);
        this.drawSymbol = true;
        this.expression = expression;
        this.symbol = symbol;
        this.color = color;
        this.wcolor = fontColor;
        this.pos = pos;
    }

    public CIExpression getExpression() {
        return expression;
    }

    public CIColor getColor() {
        return color;
    }

    public CIColor getWarningColor() {
        return wcolor;
    }

    public CIStyle getStyle() {
        return style;
    }

    public float getThickness() {
        return thickness;
    }

    public CISymbol getSymbol() {
        return symbol;
    }

    public void setSymbol(CISymbol value) {
        symbol = value;
    }

    public boolean isDrawSymbol() {
        return drawSymbol;
    }

    public void setDrawSymbol(boolean value) {
        drawSymbol = value;
    }

    public CIPosition getPosition() {
        return pos;
    }

    public void setPosition(CIPosition value) {
        pos = value;
    }

    public String generateCode(CIProgram program) {
        String finalValue = expression.generateCode(program);
        StringBuilder sb = new StringBuilder();
        sb.append("for(int i=0; i<al.size(); i++){\n");
        sb.append("    cR = (");
        sb.append(CustomIndicatorInterface.getChartRecordClassName());
        sb.append(")al.get(i);\n");
        sb.append("    try {\n");
        if (drawSymbol) {
            sb.append("        if (").append(finalValue).append("){\n");
            sb.append("            ");
            sb.append(CustomIndicatorInterface.getChartPointClassName());
            sb.append(" aPoint = GDM.getIndicatorPoint(cR.Time, index, getID());\n");
            sb.append("            if (aPoint != null) {\n"); //(&& (srcPoint != null))
            sb.append("                ");
            sb.append(CustomIndicatorInterface.getChartPropertiesClassName());
            sb.append(" cp = targetCP==null? innerSource: targetCP;\n");
            sb.append("                aPoint.setIndicatorValue(cp.getStrategyValue(GDM, cR.Time, this.position));\n"); //srcPoint.getValue(innerSource.OHLCPriority)
            sb.append("            }\n");
            sb.append("        } else {\n");
            sb.append("            GDM.removeIndicatorPoint(cR.Time, index, getID());\n");
            sb.append("        }\n");
        } else {
            sb.append("        double v_ALUE_ = ").append(finalValue).append(";\n");
            sb.append("        if ((v_ALUE_==Indicator.NULL) || Double.isNaN(v_ALUE_) || Double.isInfinite(v_ALUE_)||\n");
            sb.append("            (v_ALUE_==Double.NEGATIVE_INFINITY) || (v_ALUE_==Double.POSITIVE_INFINITY)) {\n");
            sb.append("            GDM.removeIndicatorPoint(cR.Time, index, getID());\n");
            sb.append("        } else {\n");
            sb.append("            ");
            sb.append(CustomIndicatorInterface.getChartPointClassName());
            sb.append(" aPoint = GDM.getIndicatorPoint(cR.Time, index, getID());\n");
            sb.append("            if (aPoint != null) {\n");
            sb.append("                aPoint.setIndicatorValue(v_ALUE_);\n");
            sb.append("            }\n");
            sb.append("        }\n");
        }
        sb.append("    } catch (Exception ex){\n");
        sb.append("        GDM.removeIndicatorPoint(cR.Time, index, getID());\n");
        sb.append("    }\n");
        sb.append("}\n");
        return sb.toString();
    }

    public String generatePropCode(CIProgram program) {
        StringBuilder sb = new StringBuilder();
        if (color != null) {
            Color c = color.getColor();
            sb.append("  this.setColor(new Color(" + c.getRGB() + "));\n");
        }
        if (wcolor != null) {
            Color c = wcolor.getColor();
            sb.append("  this.setWarningColor(new Color(" + c.getRGB() + "));\n");
        }
        if (style != null) {
            sb.append("  this.setPenStyle((byte)" + Integer.toString(style.getStyle()) + ");\n");
        }
        if (thickness > 0) {
            sb.append("  this.setPenWidth((float)" + Float.toString(thickness) + ");\n");
        }
        if (symbol != null) {
            int smbl = symbol.getSymbol();
            sb.append("  this.setSymbolID(" + Integer.toString(smbl) + ");\n");
            if (smbl >= 1000) {
                sb.append("  this.setItsOwnScale(false);\n");
            }
        }
        if (pos != null) {
            sb.append("  this.setPosition(" + Integer.toString(pos.getPoistion()) + ");\n");
        }
        return sb.toString();
    }

}

