//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Collections;
//using CustomIndicators.Expressions;
package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.CustomIndicatorInterface;
import com.isi.csvr.chart.customindicators.DeclarationType;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.ParamInfo;
import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;
import com.isi.csvr.chart.customindicators.expressions.CIIdentifier;
import com.isi.csvr.chart.customindicators.expressions.CIIndicator;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class CIProgram {

    //Temporary Variables
    public Hashtable htParams = new Hashtable();
    public Hashtable htVars = new Hashtable();

    ////PlotStatements
    //private ArrayList plotStatements;

    //public ArrayList getPlotStatements() {
    //    return plotStatements;
    //}
    public Hashtable htSteps = new Hashtable();
    public byte stepCount = 4; // keep reserved spots 0, 1, 2, 3, 4 for O, H, L, C, V
    public ArrayList alTempVars = null;
    public StringBuilder sbCalc = new StringBuilder();
    public int currentLoopBegin = 0;
    public int currentLoopEnd = 0;
    public int plotCount = 0;
    public StringBuilder sbPlotIDs = new StringBuilder();
    public boolean isStrategy = false;
    //Statements
    private ArrayList statements;
    private int level = 0; // depth of indentation (if-then-else)
    //Constructor
    public CIProgram(ArrayList statements) {
        this.statements = statements;
        //this.plotStatements = plotStmts;
    }

    public ArrayList getStatements() {
        return statements;
    }

    /// <summary>
    /// This generates the code for the entire custom indicator class
    /// </summary>
    /// <param name="template">template text to use for c# class</param>
    /// <returns>class code as a String</returns>
    public String GenerateCode(String template) throws Exception {
        //System.out.println("@@@@@@@"+v_ALUE_);
        if (statements != null) {
            extractTopLevelParamsAndVars(statements, 0);
            if (plotCount == 0) {
                throw new Exception("There must be at least one 'plot', 'drawsymbol' or 'markpattern' statement on the grammer!");
            }
            generateCalculation();
        }
        template = generateParams(template);
        //generatePlots();
        template = template.replace("<PlotCount>", Integer.toString(plotCount));
        template = template.replace("<SetPlotID>", sbPlotIDs.toString());
        template = template.replace("<IndicatorCalculation>", sbCalc.toString());
        template = template.replace("<STEPS>", generateSteps());
        return template;
    }

    //#region Extract Top Level Params And Vars
    private void extractTopLevelParamsAndVars(ArrayList statements, int level) throws Exception {
        plotCount = 0;
        for (int i = 0; i < statements.size(); i++) {
            CIStatement stmt = (CIStatement) statements.get(i);
            if (stmt.getStatementType() == StatementType.PLOT) {
                plotCount++;
            } else if (stmt.getStatementType() == StatementType.DECLARE_VAR) {
                CIDeclareVar decStmt = (CIDeclareVar) stmt;
                if (decStmt.getDeclarationType() == DeclarationType.PARAM) {
                    ////if (level >0){
                    ////    throw new Exception("Cannot declare params inside 'for', 'while' loops or if-then-else blocks.");
                    ////}
                    for (int k = 0; k < decStmt.getAlVars().size(); k++) {
                        CIIdentifier param = (CIIdentifier) decStmt.getAlVars().get(k);
                        String paramName = param.getIdentifier();
                        if (!htParams.containsKey(paramName)) {
                            htParams.put(paramName, new ParamInfo(paramName, level, param.getVarType()));
                        } else {
                            throw new Exception("duplicate parameter variable: '" + paramName + "'");
                        }
                    }
                } else { //DeclarationType.VAR
                    for (int k = 0; k < decStmt.getAlVars().size(); k++) {
                        CIIdentifier param = (CIIdentifier) decStmt.getAlVars().get(k);
                        String var = param.getIdentifier();
                        if (!htVars.containsKey(var)) {
                            htVars.put(var, new ParamInfo(var, level, param.getVarType()));
                        } else {
                            throw new Exception("duplicate variable: '" + var + "'");
                        }
                    }
                }
            } else if (stmt.getStatementType() == StatementType.DECLARE_ASSIGN) {
                CIDeclareAssign decAssnStmt = (CIDeclareAssign) stmt;
                String paramName = decAssnStmt.getVariable().getIdentifier();
                if (decAssnStmt.getDeclarationType() == DeclarationType.PARAM) {
                    if (!htParams.containsKey(paramName)) {
                        ParamInfo paramInfo = new ParamInfo(paramName, level, decAssnStmt.getVariable().getVarType());
                        paramInfo.setInitialValue(decAssnStmt.getAssignment().getExpression());
                        paramInfo.Code = paramInfo.getInitialValue().getParamValue(this);
                        htParams.put(paramName, paramInfo);
                    } else {
                        throw new Exception("duplicate parameter variable: '" + paramName + "'");
                    }
                } else { //DeclarationType.VAR
                    if (!htVars.containsKey(paramName)) {
                        htVars.put(paramName, new ParamInfo(paramName, level, decAssnStmt.getVariable().getVarType()));
                    } else {
                        throw new Exception("duplicate variable: '" + paramName + "'");
                    }
                }
            } else if (stmt.getStatementType() == StatementType.OBJECT_ASSIGN) {
                CIAssignment assn = ((CIObjectAssign) stmt).getAssignment();
                String name = assn.getIdentifier();
                if (htParams.containsKey(name)) {
                    if (level > 0) {
                        throw new Exception("Cannot initiallize a parameter inside 'for', 'while' loops or if-then-else blocks.\n Check parameter: '" + name + "'");
                    } else {
                        ParamInfo param = (ParamInfo) htParams.get(name);
                        if (param.assigned) {
                            throw new Exception("Cannot initiallize a parameter more than once.\n Check parameter: '" + name + "'");
                        } else {
                            param.setInitialValue(assn.getExpression());
                            param.Code = param.getInitialValue().getParamValue(this);
                        }
                    }
                } else if (htVars.containsKey(name)) {
                    ParamInfo param = (ParamInfo) htVars.get(name);
                    param.setInitialValue(assn.getExpression());
                } else {
                    throw new Exception("You must declare the type of a parameter or variable before assigning it a value. Check: '" + name + "'");
                }
            }
        }
    }
    //#endregion

    //#region Generate Params

    private String generateParams(String template) {
        int count = 0;
        StringBuilder sb = new StringBuilder();
        StringBuilder sbInit = new StringBuilder();
        StringBuilder sbAssign = new StringBuilder();
        StringBuilder sbLoad = new StringBuilder();
        StringBuilder sbSave = new StringBuilder();
        Enumeration keys = htParams.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            ParamInfo param = (ParamInfo) htParams.get(key);
            addProperty(sb, param);
            initProperty(sbInit, param);
            assignValuesFrom(sbAssign, param);
            loadProperties(sbLoad, param);
            saveProperties(sbSave, param);
            count++;
        }
        template = template.replace("<Properties>", sb.toString());
        template = template.replace("<PropertyInit>", sbInit.toString());
        template = template.replace("<AssignValuesFrom>", sbAssign.toString());
        template = template.replace("<LoadFromTemplate>", sbLoad.toString());
        template = template.replace("<SaveToTemplate>", sbSave.toString());
        return template;
    }

    private void saveProperties(StringBuilder sbSave, ParamInfo param) {
        //saveProperty("TimePeriods", this.timePeriods.ToString());
        String fieldName = IndicatorFactory.ParamPrefix + param.Name;//param.Name;
        sbSave.append(CustomIndicatorInterface.getTemplateFactoryClassName());
        sbSave.append(".saveProperty(chart, document, \"");
        sbSave.append(param.Name);
        sbSave.append("\", ");
        switch (param.ParamType) {
            case DOUBLE:
            case INT:
            case BOOL:
                sbSave.append("\"\"+this.").append(fieldName);
                break;
            case DATA_ARRAY:
            case MA_METHOD:
            case CALC_METHOD:
            case CC_PERIOD:
                sbSave.append("this.").append(fieldName).append(".name()");
//                sbSave.append("Byte.toString(this.").append(fieldName).append(")");
                break;
            case UNKNOWN:
            default:
                sbSave.append("\"\"");
                break;
        }
        sbSave.append(");\n");
    }

    private void loadProperties(StringBuilder sbLoad, ParamInfo param) {
        //this.timePeriods = int.Parse(loadProperty("TimePeriods"));
        String fieldName = IndicatorFactory.ParamPrefix + param.Name;//param.Name;
        sbLoad.append("this.");
        sbLoad.append(fieldName);
        sbLoad.append(" = ");
        switch (param.ParamType) {
            case DOUBLE:
                sbLoad.append("Double.parseDouble");
                break;
            case INT:
                sbLoad.append("Integer.parseInt");
                break;
            case BOOL:
                sbLoad.append("Boolean.parseBoolean");
                break;
            case DATA_ARRAY:
                sbLoad.append("OhlcPriority.valueOf");
                break;
            case MA_METHOD:
                sbLoad.append("MAMethod.valueOf");
                break;
            case CALC_METHOD:
                sbLoad.append("CalcMethod.valueOf");
                break;
            case CC_PERIOD:
                sbLoad.append("CopCurvePeriod.valueOf");
                break;
            case UNKNOWN:
            default:
                break;
        }
        sbLoad.append("(");
        sbLoad.append(CustomIndicatorInterface.getTemplateFactoryClassName());
        sbLoad.append(".loadProperty(xpath, document, preExpression+\"/");
        sbLoad.append(param.Name);
        sbLoad.append("\"));\n");
    }

    private void assignValuesFrom(StringBuilder sbAssign, ParamInfo param) {
        String fieldName = IndicatorFactory.ParamPrefix + param.Name;
        //this.xxx = ind.xxx;
        sbAssign.append("this.");
        sbAssign.append(fieldName);
        sbAssign.append(" = ind.");
        sbAssign.append(fieldName);
        sbAssign.append(";\n");
    }

    private void initProperty(StringBuilder sbInit, ParamInfo param) {
        String fieldName = IndicatorFactory.ParamPrefix + param.Name;
        sbInit.append(fieldName);
        sbInit.append(" = ");
        String code;
        if ((param.Code != null) && !param.Code.equals("")) {
            code = param.Code;
        } else {
            code = IndicatorFactory.getDeafaultValue(param.ParamType);
        }
        sbInit.append(code);
        sbInit.append(";\n");
    }

    private void addProperty(StringBuilder sb, ParamInfo param) {
        String type = IndicatorFactory.getTypeString(param.ParamType);
        String fieldName = IndicatorFactory.ParamPrefix + param.Name;
        sb.append("private ");
        sb.append(type);
        sb.append(" ");
        sb.append(fieldName);
        sb.append(";\n");
        sb.append("public ");
        sb.append(type);
        sb.append(" get").append(param.Name).append("(){\n");
        sb.append("return ").append(fieldName).append(";\n}\n");
        sb.append("public void set").append(param.Name);
        sb.append("(").append(type).append(" value){\n");
        sb.append(fieldName);
        sb.append(" = value;\n}\n");
    }
    //#endregion

    //#region Generate Calculation

    private void generateCalculation() {
        int currPlotID = 0;
        for (int i = 0; i < statements.size(); i++) {
            CIStatement stmt = (CIStatement) statements.get(i);
            if (stmt.getStatementType() == StatementType.PLOT) {
                boolean drawSymbol = ((CIPlotStatement) stmt).isDrawSymbol();
                isStrategy = isStrategy || drawSymbol;
                //Setting Properties
                sbPlotIDs.append("if (this.plotID==" + currPlotID + "){\n");
                sbPlotIDs.append("  this.drawSymbol = " + Boolean.toString(drawSymbol).toLowerCase() + ";\n");
                sbPlotIDs.append(((CIPlotStatement) stmt).generatePropCode(this));
                sbPlotIDs.append("  return;\n");
                sbPlotIDs.append("}\n");
                if (plotCount > 1) {
                    //Calculation
                    sbCalc.append("if (this.plotID==" + currPlotID + "){\n");
                    sbCalc.append(stmt.generateCode(this));
                    sbCalc.append("  return;\n");
                    //sbCalc.Append("  System.Diagnostics.Debug.WriteLine(\"**** <ClassName> Calc time \" + (entryTime - DateTime.Now.Ticks));\n");
                    sbCalc.append("}\n");
                } else {
                    sbCalc.append(stmt.generateCode(this));
                }
                currPlotID++;
            } else {
                sbCalc.append(stmt.generateCode(this));
                if ((stmt.getStatementType() == StatementType.OBJECT_ASSIGN) ||
                        (stmt.getStatementType() == StatementType.DECLARE_ASSIGN)) {
                    String s = sbCalc.toString();
                    if (!s.endsWith(";") && !s.endsWith("\n") && !s.endsWith("}") && !s.equals("")) {
                        sbCalc.append(";\n");
                    }
                }
            }
            if (currPlotID >= plotCount) break;
        }
        if (isStrategy) {
            sbPlotIDs.insert(0, "this.setStrategy(true);\n");
        }
    }

    public String getIndicatorStepString(String destStepName, CIIndicator ind) {
        String[] codes = new String[ind.getAlParams().size()];
        for (int i = 0; i < ind.getAlParams().size(); i++) {
            codes[i] = ((CIExpression) ind.getAlParams().get(i)).generateCode(this);
        }
        boolean isSrcNeeded = false;
        int auxStepCount = 0;
        // manually implementing ref
        boolean[] isSrcNeededRef = {isSrcNeeded};
        int[] auxStepCountRef = {auxStepCount};
        IndicatorFactory.getIndicatorAttributes(ind.getName(), isSrcNeededRef, auxStepCountRef);
        isSrcNeeded = isSrcNeededRef[0];
        auxStepCount = auxStepCountRef[0];

        StringBuilder sb = new StringBuilder();
        String srcStepName = "";
        if (isSrcNeeded) {
            boolean isAStep = (codes[0].indexOf("cR.getStepValue(STEP_") == 0);
            if (isAStep) {
                int start = codes[0].indexOf("(STEP_") + 1;
                int end = codes[0].indexOf(")");
                srcStepName = codes[0].substring(start, end);
            } else {
                this.stepCount++; //source step
                srcStepName = "STEP_" + this.stepCount;
                sb.append("for (int i = 0; i < al.size(); i++) {\n");
                sb.append("  try{\n");
                sb.append("    cR = (");
                sb.append(CustomIndicatorInterface.getChartRecordClassName());
                sb.append(")al.get(i);\n");
                // TODO: validate codes[0]
                sb.append("    cR.setStepValue(").append(srcStepName).append(", ").append(codes[0]).append(");\n ");
                sb.append("  }catch(Exception ex){}\n");
                sb.append("}\n");
            }
        }


        sb.append(IndicatorFactory.getExactIndicatorName(ind.getName()));
        sb.append(".calculateIndicator(GDM, al, ");
        sb.append(currentLoopBegin);
        // TODO: validate params
        int begin = isSrcNeeded ? 1 : 0;
        for (int i = begin; i < ind.getAlParams().size(); i++) {
            //, 9, MovingAverage.METHOD_EXPONENTIAL
            sb.append(", " + codes[i]);
        }
        if (auxStepCount > 0) {
            for (int i = 0; i < auxStepCount; i++) {
                this.stepCount++; //reserved steps
                String resvdStep = "STEP_" + this.stepCount;
                sb.append(", " + resvdStep);
            }
        }
        if (isSrcNeeded) {
            //, stepHminusL
            sb.append(", " + srcStepName);
        }
        sb.append(", " + destStepName + ");\n");

        return sb.toString();
    }

    public String getCumStepString(String destStepName, CIExpression exprsn) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn.generateCode(this);
        sb.append("try{\n");
        sb.append("  double s_u_m = Double.NaN;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    double c_u_r = " + code + ";\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("    s_u_m = (Double.isNaN(s_u_m)? c_u_r: s_u_m+c_u_r); }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", s_u_m);\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getSumStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn1.generateCode(this);
        String count = exprsn2.generateCode(this);
        sb.append("try {\n");
        sb.append("  double s_u_m = 0d;\n");
        sb.append("  double p_r_e = 0d, c_u_r = 0d;\n");
        sb.append("  int bIndex = 0;\n");
        sb.append("  int c_n_t = " + count + ";\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try {\n");
        sb.append("      c_u_r = " + code + ";\n");
        sb.append("    } catch (Exception ex) { c_u_r = Indicator.NULL; }\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      break; \n");
        sb.append("    } else { bIndex++; \n");
        sb.append("      cR.setStepValue(" + destStepName + ", Indicator.NULL);}\n");
        sb.append("  }\n");
        sb.append("  if (bIndex+c_n_t>al.size()) {\n");
        sb.append("    for (int i = bIndex; i < al.size(); i++) {\n");
        sb.append("      cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("      cR.setStepValue(" + destStepName + ", Indicator.NULL); }\n");
        sb.append("    return;\n");
        sb.append("  }\n");
        sb.append("  for (int i = bIndex; i < bIndex+c_n_t; i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      c_u_r = " + code + ";\n");
        sb.append("    } catch (Exception ex) { c_u_r = Indicator.NULL; }\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      s_u_m = s_u_m + c_u_r;}\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (i==bIndex+c_n_t-1?s_u_m:Indicator.NULL));\n");
        sb.append("  }\n");
        sb.append("  for (int k = bIndex+c_n_t; k < al.size(); k++) {\n");
        sb.append("    int i = k-c_n_t;\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      p_r_e = " + code + ";\n");
        sb.append("    } catch (Exception ex) { p_r_e = Indicator.NULL; }\n");
        sb.append("    if ((p_r_e!=Indicator.NULL) && !Double.isNaN(p_r_e) && !Double.isInfinite(p_r_e)&&\n");
        sb.append("        (p_r_e!=Double.NEGATIVE_INFINITY) && (p_r_e!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      s_u_m = s_u_m - p_r_e; }\n");
        sb.append("    i = k;\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      c_u_r = " + code + ";\n");
        sb.append("    }  catch (Exception ex) { c_u_r = Indicator.NULL; }\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      s_u_m = s_u_m + c_u_r; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", s_u_m);\n");
        sb.append("  }\n");
        sb.append("}  catch (Exception ex) {}\n");
        return sb.toString();
    }


    public String getRefStepString(CIExpression exprsn1) {
        String code = exprsn1.generateCode(this);
        boolean isAStep = (code.indexOf("cR.getStepValue(STEP_") == 0);
        if (isAStep) {
            int start = code.indexOf("(STEP_") + 1;
            int end = code.indexOf(")");
            return code.substring(start, end);
        } else {
            this.stepCount++; //source step
            String srcStepName = "STEP_" + this.stepCount;
            StringBuilder sb = new StringBuilder();
            sb.append("for (int i = 0; i < al.size(); i++) {\n");
            sb.append("  try{\n");
            sb.append("    cR = (");
            sb.append(CustomIndicatorInterface.getChartRecordClassName());
            sb.append(")al.get(i);\n");
            sb.append("    cR.setStepValue(" + srcStepName + ", " + code + ");\n");
            sb.append("  }catch(Exception ex){}\n");
            sb.append("}\n");
            sbCalc.append(sb.toString());
            return srcStepName;
        }
    }

    public String getStepForVar(String identifier) {
        Enumeration keys = htSteps.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String val = (String) htSteps.get(key);
            if (val.equals(identifier)) {
                return key;
            }
        }
        return null;
    }

    // #region Generate High Low Steps ////////////////////////////////////////////////////////////////////
    public String getHHVStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn1.generateCode(this);
        String periods = exprsn2.generateCode(this);
        sb.append("try{\n");
        sb.append("  int c_n_t = " + periods + ";\n");
        sb.append("  for (int k = 0; k < c_n_t-1; k++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  int be_gi_n = c_n_t-1;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    } catch(Exception ex) { be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  for (int k = be_gi_n; k < al.size(); k++) {\n");
        sb.append("    double h_h_v = -Double.MAX_VALUE;\n");
        sb.append("    for (int j = 0; j < c_n_t; j++) {\n");
        sb.append("      int i = k-j;\n");
        sb.append("      cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        h_h_v = Math.max(h_h_v, c_u_r); }\n");
        sb.append("    }\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==-Double.MAX_VALUE?Indicator.NULL:h_h_v));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getHHVBarsStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn1.generateCode(this);
        String periods = exprsn2.generateCode(this);
        sb.append("try{\n");
        sb.append("  int c_n_t = " + periods + ";\n");
        sb.append("  for (int i = 0; i < c_n_t-1; i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  int be_gi_n = c_n_t-1;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  for (int k = be_gi_n; k < al.size(); k++) {\n");
        sb.append("    double h_h_v = -Double.MAX_VALUE;\n");
        sb.append("    int i_n_d = 0;\n");
        sb.append("    for (int j = 0; j < c_n_t; j++) {\n");
        sb.append("      int i = k-j;\n");
        sb.append("      cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        if (c_u_r >= h_h_v){ i_n_d = j; }\n");
        sb.append("        h_h_v = Math.max(h_h_v, c_u_r); }\n");
        sb.append("    }\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==-Double.MAX_VALUE?Indicator.NULL:i_n_d));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getHighestStepString(String destStepName, CIExpression exprsn) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn.generateCode(this);
        sb.append("try{\n");
        sb.append("  int be_gi_n = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  double h_h_v = -Double.MAX_VALUE;\n");
        sb.append("  for (int i = be_gi_n; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      h_h_v = Math.max(h_h_v, c_u_r); }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==-Double.MAX_VALUE?Indicator.NULL:h_h_v));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getHighestBarsStepString(String destStepName, CIExpression exprsn) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn.generateCode(this);
        sb.append("try{\n");
        sb.append("  int be_gi_n = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("          (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  double h_h_v = -Double.MAX_VALUE;\n");
        sb.append("  int i_n_d = 0;\n");
        sb.append("  for (int i = be_gi_n; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    double c_u_r = " + code + ";\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      i_n_d = (c_u_r > h_h_v)? 0: i_n_d + 1; \n");
        sb.append("      h_h_v = Math.max(h_h_v, c_u_r); }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==-Double.MAX_VALUE?Indicator.NULL:i_n_d));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getHSinceStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        return getHLSince(true, false, destStepName, exprsn1, exprsn2, exprsn3);
    }

    public String getHSinceBarsStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        return getHLSince(true, true, destStepName, exprsn1, exprsn2, exprsn3);
    }

    public String getLLVStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn1.generateCode(this);
        String periods = exprsn2.generateCode(this);
        sb.append("try{\n");
        sb.append("  int c_n_t = " + periods + ";\n");
        sb.append("  for (int k = 0; k < c_n_t-1; k++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  int be_gi_n = c_n_t-1;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  for (int k = be_gi_n; k < al.size(); k++) {\n");
        sb.append("    double h_h_v = Double.MAX_VALUE;\n");
        sb.append("    for (int j = 0; j < c_n_t; j++) {\n");
        sb.append("      int i = k-j;\n");
        sb.append("      cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        h_h_v = Math.min(h_h_v, c_u_r); }\n");
        sb.append("    }\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==Double.MAX_VALUE?Indicator.NULL:h_h_v));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getLLVBarsStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn1.generateCode(this);
        String periods = exprsn2.generateCode(this);
        sb.append("try{\n");
        sb.append("  int c_n_t = " + periods + ";\n");
        sb.append("  for (int i = 0; i < c_n_t-1; i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  int be_gi_n = c_n_t-1;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  for (int k = be_gi_n; k < al.size(); k++) {\n");
        sb.append("    double h_h_v = Double.MAX_VALUE;\n");
        sb.append("    int i_n_d = 0;\n");
        sb.append("    for (int j = 0; j < c_n_t; j++) {\n");
        sb.append("      int i = k-j;\n");
        sb.append("      cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        if (c_u_r <= h_h_v){ i_n_d = j; }\n");
        sb.append("        h_h_v = Math.min(h_h_v, c_u_r); }\n");
        sb.append("    }\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(k);\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==Double.MAX_VALUE?Indicator.NULL:i_n_d));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getLowestStepString(String destStepName, CIExpression exprsn) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn.generateCode(this);
        sb.append("try{\n");
        sb.append("  int be_gi_n = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  double h_h_v = Double.MAX_VALUE;\n");
        sb.append("  for (int i = be_gi_n; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    double c_u_r = " + code + ";\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      h_h_v = Math.min(h_h_v, c_u_r); }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==Double.MAX_VALUE?Indicator.NULL:h_h_v));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getLowestBarsStepString(String destStepName, CIExpression exprsn) {
        StringBuilder sb = new StringBuilder();
        String code = exprsn.generateCode(this);
        sb.append("try{\n");
        sb.append("  int be_gi_n = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("      double c_u_r = " + code + ";\n");
        sb.append("      if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("           (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("        break; }else{ be_gi_n++; }\n");
        sb.append("    }catch(Exception ex){ be_gi_n++; }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", Indicator.NULL);\n");
        sb.append("  }\n");
        sb.append("  double h_h_v = Double.MAX_VALUE;\n");
        sb.append("  int i_n_d = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    double c_u_r = " + code + ";\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      i_n_d = (c_u_r < h_h_v)? 0: i_n_d + 1; \n");
        sb.append("      h_h_v = Math.min(h_h_v, c_u_r); }\n");
        sb.append("    cR.setStepValue(" + destStepName + ", (h_h_v==Double.MAX_VALUE?Indicator.NULL:i_n_d));\n");
        sb.append("  }\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }

    public String getLSinceStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        return getHLSince(false, false, destStepName, exprsn1, exprsn2, exprsn3);
    }

    public String getLSinceBarsStepString(String destStepName, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        return getHLSince(false, true, destStepName, exprsn1, exprsn2, exprsn3);
    }

    private String getHLSince(boolean isH, boolean bars, String destStepName, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        StringBuilder sb = new StringBuilder();
        String method;
        if (isH) {
            method = bars ? "  calculateHSinceBars" : "  calculateHSince";
        } else {
            method = bars ? "  calculateLSinceBars" : "  calculateLSince";
        }
        String Nth = exprsn1.generateCode(this);
        String cond = exprsn2.generateCode(this);
        String code = exprsn3.generateCode(this);
        this.stepCount++; // condition step
        String condIndex = Byte.toString(stepCount);
        String condStepName = "STEP_" + condIndex;
        this.stepCount++; // data step
        String dataIndex = Byte.toString(stepCount);
        String dataStepName = "STEP_" + dataIndex;
        String destIndex = destStepName.substring(5);
        sb.append("try{\n");
        sb.append("  int c_n_t = 0;\n");
        sb.append("  for (int i = 0; i < al.size(); i++) {\n");
        sb.append("    cR = (" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i);\n");
        sb.append("    try{\n");
        sb.append("       boolean isCond = " + cond + ";\n");
        sb.append("       if (isCond) c_n_t++;\n");
        sb.append("    }catch(Exception ex){}\n");
        sb.append("    cR.setStepValue(" + condStepName + ", c_n_t);\n");
        sb.append("    double c_u_r = Indicator.NULL;\n");
        sb.append("    try{\n");
        sb.append("      c_u_r = " + code + ";\n");
        sb.append("    }catch(Exception ex){}\n");
        sb.append("    if ((c_u_r!=Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r)&&\n");
        sb.append("        (c_u_r!=Double.NEGATIVE_INFINITY) && (c_u_r!=Double.POSITIVE_INFINITY)) {\n");
        sb.append("      cR.setStepValue(" + dataStepName + ", c_u_r); }\n");
        sb.append("    else{ cR.setStepValue(" + dataStepName + ", Indicator.NULL); }\n");
        sb.append("  }\n");
        sb.append(method + "(al, (byte)" + dataIndex + ", (byte)" + condIndex + ", (byte)" + destIndex + ", " + Nth + ");\n");
        sb.append("}catch(Exception ex){}\n");
        return sb.toString();
    }
    // #endregion /////////////////////////////////////////////////////////////////////////////////////////

    //private void generatePlots() {
    //    if (plotStatements.size() > 1) {
    //        sbCalc.append("switch(getPlotID()){\n");
    //        for (int i = 0; i < plotStatements.size(); i++) {
    //            CIStatement stmt = (CIStatement) plotStatements.get(i);
    //            sbCalc.append("    case " + i + ":\n");
    //            sbCalc.append(stmt.generateCode(this));
    //            sbCalc.append("        break;\n");
    //        }
    //        sbCalc.append("}\n");
    //    } else {
    //        CIStatement stmt = (CIStatement) plotStatements.get(0);
    //        sbCalc.append(stmt.generateCode(this));
    //    }
    //}

    private String generateSteps() {
        int fixedSteps = 4;
        if (stepCount > fixedSteps) {
            StringBuilder sb = new StringBuilder();
            String stepSize = Integer.toString(stepCount - fixedSteps);
            sb.append("//setting step size " + stepSize + "\n");
            sb.append("for (int i = 0; i < al.size(); i++) {\n");
            sb.append("    cR = (");
            sb.append(CustomIndicatorInterface.getChartRecordClassName());
            sb.append(")al.get(i);\n");
            sb.append("    cR.setStepSize(" + stepSize + ");\n");
            sb.append("}\n");
            sb.append("// steps involved\n");
            for (int i = fixedSteps + 1; i <= stepCount; i++) {
                String stepName = "STEP_" + i;
                sb.append("byte " + stepName + " = " + i + ";\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    public void addStepVar(String stepName, String var) {
        htSteps.put(stepName, var);
        if (alTempVars != null) {
            alTempVars.add(stepName);
        }
    }

    public ArrayList collectTempVars() {
        ArrayList result = alTempVars;
        alTempVars = new ArrayList();
        return result;
    }

    public void discardTempVars(ArrayList preAL) {
        if (alTempVars != null) {
            for (int i = 0; i < alTempVars.size(); i++) {
                htSteps.remove(alTempVars.get(i));
            }
        }
        alTempVars = preAL;
    }

    public boolean addLoopingHeader(StringBuilder sb) {
        if (level == 0) {
            sb.append("for (int i = 0; i < al.size(); i++) {\n");
            sb.append("  try{\n");
            sb.append("    cR = (");
            sb.append(CustomIndicatorInterface.getChartRecordClassName());
            sb.append(")al.get(i);\n");
            level = 1;
            return true;
        }
        return false;
    }

    public void addLoopingFooter(StringBuilder sb) {
        if (level > 0) {
            sb.append("\n  }catch(Exception ex){}\n");
            sb.append("}\n");
            level = 0;
        }
    }


}
