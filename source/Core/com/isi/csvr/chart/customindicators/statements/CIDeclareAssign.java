package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.DeclarationType;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.ParamType;
import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIIdentifier;

public class CIDeclareAssign extends CIStatement {

    //DeclarationType
    private DeclarationType decType;
    //Variable
    private CIIdentifier var;
    //Assignment
    private CIAssignment assignment;

    public CIDeclareAssign(DeclarationType decType, CIIdentifier var, CIAssignment assn) {
        super(StatementType.DECLARE_ASSIGN);
        this.decType = decType;
        this.var = var;
        this.assignment = assn;
    }

    public DeclarationType getDeclarationType() {
        return decType;
    }

    public void setDeclarationType(DeclarationType decType) {
        this.decType = decType;
    }

    public CIIdentifier getVariable() {
        return var;
    }

    public CIAssignment getAssignment() {
        return assignment;
    }

    public String generateCode(CIProgram program) {
        if (decType == DeclarationType.VAR) {
            StringBuilder sb = new StringBuilder();
            String code = assignment.getExpression().generateCode(program);
            if (var.getVarType() == ParamType.DATA_ARRAY) {
                program.stepCount++;
                String stepName = "STEP_" + program.stepCount;
                program.addStepVar(stepName, var.getIdentifier());

                boolean looping = program.addLoopingHeader(sb);
                sb.append("    cR.setStepValue(" + stepName + ", " + code + ");"); //\n
                if (looping) {
                    program.addLoopingFooter(sb);
                }
            } else {
                sb.append(IndicatorFactory.getTypeString(var.getVarType()));
                sb.append(" ");
                sb.append(var.generateCode(program));
                sb.append(" = ");
                sb.append(code);  // the end ";" is not written (commented below) as this is used inside loop assigns
                //sb.Append(";\n");
            }
            return sb.toString();
        } else {
            return "";
        }
    }
}

