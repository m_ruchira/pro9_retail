//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Collections;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.statements.CIProgram;

import java.util.ArrayList;

public class CIIndicator extends CIExpression {

    private String name;
    private ArrayList alParams;

    public CIIndicator(String name, ArrayList alParams) {
        this.name = name;
        this.alParams = alParams;
    }

    public String getName() {
        return name;
    }

    public ArrayList getAlParams() {
        return alParams;
    }

    public String getParamValue(CIProgram program) {
        // cannot init a param with indicators
        //throw new Exception("You cannot assign an indicator as an initial value for a param. Check '" + name + "'");
        return null;
    }

    public String generateCode(CIProgram program) {
        program.stepCount++; //destination step
        String varName = "STEP_" + program.stepCount;
        String stepString = program.getIndicatorStepString(varName, this);
        program.sbCalc.append(stepString);
        return "cR.getStepValue(" + varName + ")";
    }
}

