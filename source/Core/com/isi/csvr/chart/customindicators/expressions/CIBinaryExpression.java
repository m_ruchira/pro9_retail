package com.isi.csvr.chart.customindicators.expressions;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

import com.isi.csvr.chart.customindicators.statements.CIProgram;

public class CIBinaryExpression extends CIExpression {

    private CIExpression left;
    private CIExpression right;
    private String sign;

    public CIBinaryExpression(String sign, CIExpression left, CIExpression right) {
        this.sign = sign;
        this.left = left;
        this.right = right;
    }

    public CIExpression getLeft() {
        return left;
    }

    public CIExpression getRight() {
        return right;
    }

    public String getSign() {
        return sign;
    }

    //public override ParamType getVariableType() {
    //    if (sign.Equals("==") || sign.Equals(">")||
    //        sign.Equals("<") || sign.Equals("<=") ||
    //        sign.Equals(">=") || sign.Equals("<>") ||
    //        sign.Equals("and") || sign.Equals("or")) {
    //        return ParamType.BOOL;
    //    } else if (sign.Equals("+") || sign.Equals("-") ||
    //        sign.Equals("*") || sign.Equals("/")) {
    //        ParamType leftType = left.getVariableType();
    //        ParamType rightType = right.getVariableType();
    //        if ((leftType == ParamType.DATA_ARRAY) || (rightType == ParamType.DATA_ARRAY)) {
    //            return ParamType.DATA_ARRAY;
    //        } else if ((leftType == ParamType.DOUBLE) || (rightType == ParamType.DOUBLE)) {
    //            return ParamType.DOUBLE;
    //        } else if ((leftType == ParamType.INT) || (rightType == ParamType.INT)) {
    //            return ParamType.INT;
    //        } else {
    //            throw new Exception("Invalid Expression - you cannot use operator '"+sign
    //                + "' between operands of types " + leftType.ToString() + " and " + rightType.ToString());
    //        }
    //    } else {
    //        return left.getVariableType();
    //    }
    //}

    public String getParamValue(CIProgram program) {
        return left.getParamValue(program) + sign + right.getParamValue(program);
    }

    public String generateCode(CIProgram program) {
        return left.generateCode(program) + " " + sign + " " + right.generateCode(program);
    }
}

