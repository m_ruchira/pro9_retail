package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.statements.CIProgram;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

public class CIBracketExpression extends CIExpression {

    private CIExpression expression;

    public CIBracketExpression(CIExpression expression) {
        this.expression = expression;
    }

    public CIExpression getExpression() {
        return expression;
    }

    //public override ParamType getVariableType() {
    //    return expression.getVariableType();
    //}

    public String getParamValue(CIProgram program) {
        return expression.getParamValue(program);
    }

    public String generateCode(CIProgram program) {
        return "(" + expression.generateCode(program) + ")";
    }
}

