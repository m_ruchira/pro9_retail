package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.ICodeGenerator;
import com.isi.csvr.chart.customindicators.statements.CIProgram;

public abstract class CIExpression implements ICodeGenerator {
    //public abstract ParamType getVariableType();
    public abstract String getParamValue(CIProgram program);

    public abstract String generateCode(CIProgram program);
}

