//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.customindicators.ParamInfo;
import com.isi.csvr.chart.customindicators.ParamType;
import com.isi.csvr.chart.customindicators.statements.CIProgram;

public class CIIdentifier extends CIExpression {

    private String identifier;
    private ParamType varType;

    public CIIdentifier(String identifier, ParamType varType) {
        this.identifier = identifier;
        this.varType = varType;
    }

    public String getIdentifier() {
        return identifier;
    }

    public ParamType getVarType() {
        return varType;
    }

    //public override ParamType getVariableType() {
    //    return varType;
    //}

    public String getParamValue(CIProgram program) {
        String result;
        if (program.htParams.containsKey(identifier)) {
            ParamInfo param = (ParamInfo) program.htParams.get(identifier);
            result = param.getInitialValue().getParamValue(program);
        } else if (program.htVars.containsKey(identifier)) {
            ParamInfo param = (ParamInfo) program.htVars.get(identifier);
            //handle not assigned
            result = param.getInitialValue().getParamValue(program);
        } else {
            return null;
            //throw new Exception("You must declare a 'param' or 'var' before using it. Check: '" + identifier + "'");
        }
        return "(" + result + ")";
    }

    public String generateCode(CIProgram program) {
        if (!program.htParams.containsKey(identifier)) {
            String stepName = program.getStepForVar(identifier);
            if (stepName != null) {
                return "cR.getStepValue(" + stepName + ")";
            } else {
                return IndicatorFactory.Prefix + identifier;
            }
        } else {
            ParamInfo info = (ParamInfo) program.htParams.get(identifier);
            if (info.ParamType == ParamType.DATA_ARRAY) {
                return "cR.getStepValue((byte)" + IndicatorFactory.ParamPrefix + identifier + ".ordinal())";
            } else {
                return IndicatorFactory.ParamPrefix + identifier;
            }
        }
    }
}

