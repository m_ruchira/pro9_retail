package com.isi.csvr.chart.customindicators;

import com.isi.csvr.chart.customindicators.expressions.CIExpression;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Feb 4, 2007
 * Time: 5:34:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParamInfo {
    public String Name;
    public ParamType ParamType;
    public int Level;
    public boolean assigned = false;
    public boolean used = false;
    public String Code = "";
    private CIExpression initialValue = null;
    public ParamInfo(String Name, int Level, ParamType ParamType) {
        this.Name = Name;
        this.Level = Level;
        this.ParamType = ParamType;
    }

    public CIExpression getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(CIExpression value) {
        initialValue = value;
        assigned = true;
    }
}
