package com.isi.csvr.chart.customindicators;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * User: Uditha Nagahawatta
 * Date: Feb 10, 2007
 */
public class Encoder {

    private static final int START = 8;
    private final static byte[] KEY = "wdfnbvoirefdagsad;lkjne;rklfja;;lkajdfsadfviervifnvnbeieafewi3r319rf839erh39432pq20209duf9309uf3e4urf0wor024r9434tf0q903rhyfhhadfhqf2121`=1-`o12-oe-fjjanmvad.,mWSFK;ALSKDF;MMG;FBGKLNBL;SKDF".getBytes();
    private static final int KEY_LEN = KEY.length;

    public static void main(String[] args) {
        File file = new File(args[0]);
        encode(file);
    }


    public static byte[] decodeFile(File file) {
        try {
            FileInputStream in = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            int len = in.read(data);
            if (len == data.length) {
                for (int i = START; i < data.length; i++) {
                    data[i] ^= KEY[(i - START) % KEY_LEN];
                }
            }
            in.close();
            return data;
        } catch (IOException e) {
//            e.printStackTrace();
        }
        return null;
    }

    public static void encode(File classFile) {
        try {
            FileInputStream in = new FileInputStream(classFile);
            byte[] data = new byte[(int) classFile.length()];
            int len = in.read(data);
            if (len == data.length) {
                for (int i = START; i < data.length; i++) {
                    data[i] ^= KEY[(i - START) % KEY_LEN];
                }
                FileOutputStream out = new FileOutputStream(classFile);
                out.write(data);
                out.flush();
                out.close();
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
