package com.isi.csvr.chart.customindicators;

/**
 * User: udaka
 * Date: Feb 4, 2007
 * Time: 5:30:51 PM
 */
public enum FunctionType {
    ABS,
    ROUND,
    SQR,
    SQRT,
    LOG,
    LN,
    EXP,
    CUM,
    MIN,
    MAX,
    POW,
    REF,
    HIGHEST,
    LOWEST,
    HIGHESTBARS,
    LOWESTBARS,
    ACOS,
    ASIN,
    ATAN,
    CEILING,
    COS,
    COSH,
    FLOOR,
    FRAC,
    TRUNC,
    SIGN,
    SIN,
    SINH,
    TAN,
    TANH,
    SUM,
    HHV,
    LLV,
    HHVBARS,
    LLVBARS,
    ATAN2,
    PREC,
    HIGHESTSINCE,
    LOWESTSINCE,
    HIGHESTSINCEBARS,
    LOWESTSINCEBARS
}
