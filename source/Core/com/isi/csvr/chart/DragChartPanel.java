package com.isi.csvr.chart;

import com.isi.csvr.history.DataRecord;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: May 14, 2008
 * Time: 1:59:51 PM
 * To change this template use File | Settings | File Templates.
 */


public class DragChartPanel extends JPanel implements MouseListener, MouseMotionListener {

    //mouse is not on any of the theree handlers
    private static final int MOUSE_STATUS_NONE = 0;
    private int mouseStatus = MOUSE_STATUS_NONE;
    //mouse is on the Left most corner period handler
    private static final int MOUSE_STATUS_PERIOD_HANDLE = 1;
    //mouse is on the Left most corner zoom handler
    private static final int MOUSE_STATUS_LEFT_ZOOM = 2;
    //mouse is on the Right most corner zoom handler
    private static final int MOUSE_STATUS_RIGHT_ZOOM = 3;
    //lont time = DataRecord.getTime() should multiply this facator before getting the date time
    private static final long TIME_FACTOR = 60000L;
    private Point mouseDownPt = new Point(-100, -100);
    private int mouseDownPBegin = 0;
    private int mouseDownZBegin = 0;
    private int mouseDownZEnd = 0;
    //array list used to hold the TimeValue objects
    private ArrayList points = new ArrayList();
    //arraly list used to hold the year values drawn vertically.( 2006,2007,2008... etc)
    private ArrayList midNights = new ArrayList();
    //arralyist to hold all the per every minute
    private ArrayList arrayListData = null;
    //holds the points in the top margin of the entire graph
    private Point2D.Float[] pixelArray = null;
    //holds the points between the zoomed region of the graph
    private Point2D.Float[] loadedArray = null;
    //specify the mode(Intraday mode or hostory mode)
    private boolean isIntraDayMode = false;
    private float xFactor = 1.0f;
    private float yFactor = 1.0f;
    private int periodBegin = 0;
    private int zoomBegin = 0;
    private int zoomEnd = 0;
    private int width = 100;
    private int height = 80;
    private int topGap = 10;
    private GraphFrame parentGraph;

    public DragChartPanel() {
        createUI();
    }

    protected static String getDateString(long timeInMills) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeInMills);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int date = cal.get(Calendar.DATE);

        return (date + "/" + month + "/" + year);
    }

    public void paint(Graphics g) {

        super.paint(g);
        GraphicsPath gp = new GraphicsPath();

        if (pixelArray != null && pixelArray.length > 1) {

            g.setColor(Color.WHITE);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            //loaded range, this is the moving are when the mouse is dragged over
            Graphics2D g2 = (Graphics2D) g;
            Color col = new Color(20, 20, 90);
            g2.setColor(col);
            int type = AlphaComposite.SRC_OVER;
            float al = 0.1f;
            AlphaComposite ac = AlphaComposite.getInstance(type, al);
            g2.setComposite(ac);
            g.fillRect((int) loadedArray[0].getX(), 0, ((int) (loadedArray[loadedArray.length - 1].getX() - loadedArray[0].getX())), this.getHeight());

            //fills the area bounded by the top margin
            //good g.setColor(Color.BLUE);
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1));
            g.setColor(Color.GRAY);
            gp.fillPath(g, pixelArray, this.getHeight(), this.getWidth());

            if (loadedArray.length > 1) {
                gp.fillPath(g, loadedArray, this.getHeight(), this.getWidth());
            }

            //zoomed range
            Graphics2D g2d = (Graphics2D) g;
            Color c = new Color(10, 80, 200);
            g2d.setColor(c);
            int t = AlphaComposite.SRC_OVER;
            float alpha = .1f;
            AlphaComposite rule = AlphaComposite.getInstance(t, alpha);
            g2d.setComposite(rule);
            int x = (int) pixelArray[zoomBegin].getX();
            int y = 0;
            int width = (int) (pixelArray[zoomEnd].getX() - pixelArray[zoomBegin].getX());
            int height = this.getHeight();
            g2d.fillRect(x, y, width, height);

            //draw the year axis lables for history mode
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1));
            drawAxisLables(g);

            //draws the three mouse hadlers
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1));
            //Color color = new Color(40, 50, 100);
            //g.setColor(color);
            drawHandles(g);

            //draws the marginn of the region bounded
            g2.setColor(Color.DARK_GRAY);
            float[] dashArr = {0.01f};
            BasicStroke stroke = new BasicStroke(2.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 1.0f);
            g2.setStroke(stroke);

            gp.drawPath(g, pixelArray);
        }
    }

    private void createUI() {

        this.setBackground(Color.WHITE);
        this.setOpaque(true);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);

    }

    //public void setDataArray(StockGraph sg, ChartProperties cp, Period period, long interval, float beginPos, float zoom) {
    public void setDataArray(StockGraph sg, ChartProperties cp, int period, long interval, float beginPos, float zoom, long begin) {

        this.arrayListData = cp.getDataSource();
        this.isIntraDayMode = sg.isCurrentMode();
        calculatePixelValues();

        if (points.size() > 0) {
            setPeriodIndices(sg, period, begin);
            setZoomIndices(beginPos, zoom);
        }
        repaint();
    }

    private void calculatePixelValues() {

        float min = Float.MAX_VALUE;
        float max = 0.0f;

        width = Math.max(this.getWidth(), 100);
        height = Math.max(this.getHeight() - topGap, 40);

        points.clear();
        midNights.clear();

        if ((arrayListData != null) && (arrayListData.size() > 0)) {

            long[] lastMidnight = new long[1];
            lastMidnight[0] = 0;
            for (int i = 0; i < arrayListData.size(); i++) {

                Object tempObj = arrayListData.get(i);

                if ((tempObj != null) && (tempObj instanceof DataRecord)) {

                    DataRecord record = (DataRecord) tempObj;

                    //time = 0 for a history  record
                    if (record.getTime() == 0) {
                        continue;
                    } else {
                        long time = record.getTime();
                        float close = record.getClose();

                        detectMidNight(time, lastMidnight);
                        points.add(new TimeValue(time, close));

                        //updates the min and max of CLOSE values
                        min = Math.min(min, close);
                        max = Math.max(max, close);
                    }
                }
            }
        }

        if (points.size() > 1) {
            if (min == max) {
                min = min - 1;
                max = max + 1;
            } else {
                min = min - (max - min) / 2f;
            }

            pixelArray = new Point2D.Float[points.size()];
            xFactor = (float) width / (points.size() - 1.0f);
            yFactor = (float) height / (max - min);

            float lastValue = (float) ((TimeValue) points.get(0)).getValue();    //TODO
            float sm = 0.05f;

            for (int i = 0; i < points.size(); i++) {
                TimeValue tv = (TimeValue) points.get(i);
                lastValue = lastValue * (1 - sm) + (float) (tv.getValue() * sm);
                float yCordinate = (float) this.getHeight() - (lastValue - min) * yFactor;
                float xCordinate = i * xFactor;
                pixelArray[i] = new Point2D.Float(xCordinate, yCordinate);
            }
        } else if (points.size() == 1) {
            if (pixelArray != null && pixelArray.length >= 2) {
                pixelArray[0] = new Point2D.Float(0, width);
                pixelArray[1] = new Point2D.Float(height / 2f, height / 2f);
            }
        } else {
            pixelArray = null;
        }
    }

    private void detectMidNight(long time, long[] lastMid) {

        if (isIntraDayMode) {

            long mid = (time * TIME_FACTOR) / 86400000;
            if (lastMid[0] < mid) {
                lastMid[0] = mid;
                midNights.add(points.size());
            }
        }
        //if in history mode
        else {

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(time * TIME_FACTOR);
            int year = cal.get(Calendar.YEAR);

            if (lastMid[0] < year) {
                lastMid[0] = (long) year;
                midNights.add(points.size());
            }
        }
    }

    //draws the year axis lables in history mode and month/date axis lables in intra day mode.
    private void drawAxisLables(Graphics g) {

        float labelLength = 0, currecntPos = 0, lastPos = -1000;
        long time;
        String label = "";
        Calendar cal = Calendar.getInstance();
        Font font = new Font("arial", Font.BOLD, 9);

        g.setColor(Color.WHITE);
        g.setFont(font);
        FontMetrics metrics = g.getFontMetrics(font);

        for (int i = 0; i < midNights.size(); i++) {

            int index = (Integer) midNights.get(i);
            currecntPos = (float) pixelArray[index].getX();

            if (currecntPos > labelLength + lastPos + 2) {

                g.drawLine((int) currecntPos, 0, (int) currecntPos, this.getHeight());
                time = (((TimeValue) points.get(index)).getTime());
                cal.setTimeInMillis(time * TIME_FACTOR);

                if (!isIntraDayMode) {
                    label = String.valueOf(cal.get(Calendar.YEAR));
                } else {
                    DateFormat format = new SimpleDateFormat("dd MMM");
                    label = format.format(new Date(time * TIME_FACTOR));
                }
                g.drawString(label, (int) (currecntPos) + 2, this.getHeight() - 9);
                lastPos = currecntPos;
                labelLength = metrics.stringWidth(label);
            }
        }
    }

    private void setPeriodIndices(StockGraph sg, int p, long begin) {

        //this t refers to the original milliseconds used by java API. so this t should be divided by the TIME_FACTOR
        long t = 1L;
        if (p != StockGraph.CUSTOM_PERIODS) {
            t = sg.GDMgr.getPeriodBeginMillisec(p);
            t = t / TIME_FACTOR;
        } else {
            t = begin / TIME_FACTOR;
        }

        TimeValue tv = new TimeValue(t, 0);
        int result = -1;

        Collections.sort(points);
        result = Collections.binarySearch(points, tv);

        if (result < 0) {
            result = Math.min(~result, points.size() - 1);
        }

        String lblDateText = getDateString(t * TIME_FACTOR);
        SmartZoomWindow.lblDate.setText(lblDateText);
        periodBegin = result;
        refreshLoadedArray();
    }

    private void setZoomIndices(float beginPos, float zoom) {
        zoomBegin = Math.min(periodBegin + Math.round(beginPos * (points.size() - 1f - periodBegin) / 100f), points.size() - 1);
        zoomEnd = Math.min(periodBegin + Math.round((beginPos + zoom) * (points.size() - 1f - periodBegin) / 100f), points.size() - 1);
    }

    private void adjustPeriodBegin(float pbegin) {
        if (pbegin == 0f) {
            periodBegin = 0;
        } else {
            periodBegin = Math.max(Math.min(Math.round(pbegin / xFactor), points.size() - 1), 0);
            if (mouseDownZBegin < periodBegin) {
                zoomBegin = periodBegin;
                zoomEnd = Math.min(zoomBegin + mouseDownZEnd - mouseDownZBegin, points.size() - 1);
            } else {
                zoomBegin = mouseDownZBegin;
                zoomEnd = mouseDownZEnd;
            }
        }
        refreshLoadedArray();
    }

    private void adjustZoomBegin(float zbegin) {
        zoomBegin = Math.max(Math.min(Math.round(zbegin / xFactor), zoomEnd - 1), periodBegin);
    }

    private void adjustZoomEnd(float zEnd) {
        zoomEnd = Math.min(Math.max(Math.round(zEnd / xFactor), zoomBegin + 1), points.size() - 1);
    }

    private void refreshLoadedArray() {

        loadedArray = new Point2D.Float[pixelArray.length - periodBegin];

        for (int i = periodBegin; i < pixelArray.length; i++) {
            loadedArray[i - periodBegin] = pixelArray[i];
        }
        //TODO:
    }

    private int getMouseStatus(MouseEvent e) {

        int status = MOUSE_STATUS_NONE;
        try {
            if (isOnHandle((int) loadedArray[0].getX(), (int) (this.getHeight() * 0.5), e.getX(), e.getY())) {
                status = MOUSE_STATUS_PERIOD_HANDLE;
            } else {
                boolean isOnLZoom = isOnHandle((int) pixelArray[zoomBegin].getX(), (int) (this.getHeight() * 0.75), e.getX(), e.getY());
                boolean isOnRZoom = isOnHandle((int) pixelArray[zoomEnd].getX(), (int) (this.getHeight() * 0.75), e.getX(), e.getY());

                if (isOnLZoom && isOnRZoom) {
                    if ((e.getX() - pixelArray[zoomBegin].getX()) < (pixelArray[zoomEnd].getX() - e.getX())) {
                        status = MOUSE_STATUS_LEFT_ZOOM;
                    } else {
                        status = MOUSE_STATUS_RIGHT_ZOOM;
                    }
                } else if (isOnLZoom) {
                    status = MOUSE_STATUS_LEFT_ZOOM;

                } else if (isOnRZoom) {
                    status = MOUSE_STATUS_RIGHT_ZOOM;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            return MOUSE_STATUS_NONE;
        }
        return status;
    }

    private boolean isOnHandle(int x, int y, int xP, int yP) {

        int Hgap = 2;
        int Vgap = 6;

        int[] xPoints = {x - Hgap, x + Hgap, x + Hgap + Vgap, x + Hgap, x - Hgap, x - Hgap - Vgap};
        int[] yPoints = {y - Vgap, y - Vgap, y, y + Vgap, y + Vgap, y};

        Polygon pol = new Polygon(xPoints, yPoints, 6);

        return pol.contains(xP, yP);
    }

    //here the (x,y) is the center of the handle
    private void drawHandle(Graphics g, int x, int y, BasicStroke stroke) {

        int Hgap = 2;
        int Vgap = 6;

        //cordinates of the left triangle part of the mouse handler.
        int[] xPoints1 = {(x - Hgap), (x - Hgap), (x - Hgap - Vgap)};
        int[] yPoints1 = {(y - Vgap), (y + Vgap), (y)};

        //cordinates of the right triangle part of the mouse handler.
        int[] xPoints2 = {(x + Hgap), (x + Hgap), (x + Hgap + Vgap)};
        int[] yPoints2 = {(y - Vgap), (y + Vgap), (y)};

        Graphics2D g2d = (Graphics2D) g;

        //draws the outline around the handler
        g2d.drawPolygon(xPoints1, yPoints1, 3);
        g2d.drawPolygon(xPoints2, yPoints2, 3);

        //fill the handler with some color
        g2d.fill(new Polygon(xPoints1, yPoints1, 3));
        g2d.fill(new Polygon(xPoints2, yPoints2, 3));
    }

    private void drawHandles(Graphics g) {

        //(x1,y1) is the center of the left most corner period handler
        Color color2 = new Color(60, 80, 200);
        g.setColor(color2);
        int x1 = (int) loadedArray[0].getX();
        int y1 = (int) (this.getHeight() * 0.5);
        drawHandle(g, x1, y1, null);

        Color color = new Color(40, 50, 100);
        g.setColor(color);
        //(x2,y2) is the center of the left most corner zoom handler
        int x2 = (int) pixelArray[zoomBegin].getX();
        int y2 = (int) (this.getHeight() * 0.75);
        drawHandle(g, x2, y2, null);

        //(x3,y3) is the center of the right most corner zoom handler
        int x3 = (int) pixelArray[zoomEnd].getX();
        int y3 = (int) (this.getHeight() * 0.75);
        drawHandle(g, x3, y3, null);
    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

        mouseDownPt = e.getPoint();
        mouseDownPBegin = periodBegin;
        mouseDownZBegin = zoomBegin;
        mouseDownZEnd = zoomEnd;
        mouseStatus = getMouseStatus(e);
        if (mouseStatus != MOUSE_STATUS_NONE) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        }
        repaint();
    }

    public void mouseReleased(MouseEvent e) {

        if (mouseStatus == MOUSE_STATUS_PERIOD_HANDLE) {
            onSmartZoom(true, false, 0);
        }
        mouseStatus = MOUSE_STATUS_NONE;
        this.setCursor(Cursor.getDefaultCursor());
        repaint();
        ChartFrame.getSharedInstance().refreshToolBars();
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {

        if (mouseStatus == MOUSE_STATUS_NONE) {
            mouseStatus = getMouseStatus(e);

            if (mouseStatus == MOUSE_STATUS_NONE) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            } else {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            }
        } else {

            int Hmove = (int) (e.getX() - mouseDownPt.getX());
            switch (mouseStatus) {
                case MOUSE_STATUS_PERIOD_HANDLE:
                    float pbegin = (float) Math.min(Math.max(pixelArray[mouseDownPBegin].getX() + Hmove, 0), this.getWidth() - 2);
                    adjustPeriodBegin(pbegin); //TODO:
                    break;

                case MOUSE_STATUS_LEFT_ZOOM:
                    float zbegin = (float) Math.max(Math.min(pixelArray[mouseDownZBegin].getX() + Hmove, pixelArray[mouseDownZEnd].getX() - 1), pixelArray[mouseDownPBegin].getX());
                    adjustZoomBegin(zbegin);
                    onSmartZoom(false, false, 0);   //TODO:
                    break;

                case MOUSE_STATUS_RIGHT_ZOOM:
                    float zEnd = (float) Math.min(Math.max(pixelArray[mouseDownZEnd].getX() + Hmove, pixelArray[mouseDownZBegin].getX() + 1), this.getWidth());
                    adjustZoomEnd(zEnd);
                    onSmartZoom(false, false, 0); //TODO:
                    break;
            }
        }
        repaint();
    }

    public void setGraph(GraphFrame frame) {
        this.parentGraph = frame;
    }

    public void onSmartZoom(boolean periodAdjusted, boolean isFixedPeriodButton, int fixedPeriod) {

        float bPos = (zoomBegin - periodBegin) * 100f / ((float) loadedArray.length - 1f);
        float zoom = (zoomEnd - zoomBegin) * 100f / ((float) loadedArray.length - 1f);
        long pBeginMillis = 0;

        if (periodAdjusted) {

            pBeginMillis = ((TimeValue) points.get(periodBegin)).getTime();

            String lblDateText = getDateString(pBeginMillis * TIME_FACTOR);
            SmartZoomWindow.lblDate.setText(lblDateText);
            zoomBegin = periodBegin;
            zoomEnd = points.size() - 1;
            this.invalidate();

            if (isFixedPeriodButton) {

                parentGraph.graph.GDMgr.setPeriodBeginIndex(fixedPeriod, true);
                parentGraph.graph.setPeriod(fixedPeriod);
                setFixPeriod(parentGraph.graph, fixedPeriod, 0, 100, pBeginMillis);
                parentGraph.graph.reDrawAll();

            } else {

                int customRange = getCustomIntervalQuintity(parentGraph.graph.GDMgr.currentCustomPeriod, pBeginMillis);
                parentGraph.graph.GDMgr.setCustomBeginPeriods(parentGraph.graph.GDMgr.currentCustomPeriod, customRange);
                parentGraph.graph.GDMgr.setPeriodBeginIndex(StockGraph.CUSTOM_PERIODS, true);
                parentGraph.graph.setPeriod(StockGraph.CUSTOM_PERIODS);
                parentGraph.graph.reDrawAll();
            }

        } else {

            parentGraph.graph.zSlider.setZoomAndPos(zoom, bPos);
            parentGraph.graph.GDMgr.setZoomAndPos(zoom, bPos);
            parentGraph.graph.reDrawAll();
        }
    }

    public int getCustomIntervalQuintity(int period, long pBeginMillSec) {

        Calendar beginCal = Calendar.getInstance();
        beginCal.setTimeInMillis(pBeginMillSec * TIME_FACTOR);

        long currentTime = ((TimeValue) points.get(points.size() - 1)).getTime();
        long diff = (currentTime - pBeginMillSec) * TIME_FACTOR;


        switch (period) {
            case Calendar.MINUTE:
                return (int) (diff / (60 * 1000));

            case Calendar.DATE:
                return (int) (diff / (24 * 60 * 60 * 1000));

            default:
                return (int) (diff / (24 * 60 * 60 * 1000));
        }

    }

    private void setFixPeriod(StockGraph g, int period, float beginPos, float zoom, long pBeginMills) {

        if (points.size() > 0) {
            setPeriodIndices(g, period, pBeginMills);
            setZoomIndices(beginPos, zoom);
        }
    }

    public void mouseMoved(MouseEvent e) {

        mouseStatus = getMouseStatus(e);

        if (mouseStatus == MOUSE_STATUS_NONE) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } else {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        }
    }

    private void setGraphicsObjectToDefaults(Graphics g) {

        g.setColor(Color.WHITE);

        Graphics2D g2 = (Graphics2D) g;

        final float[] dashArr = {0.01f};
        BasicStroke stroke = new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 1.0f);

        g2.setStroke(stroke);
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC, 1));
    }
}
