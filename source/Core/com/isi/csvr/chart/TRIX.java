package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:34:17 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class TRIX extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_Trix;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public TRIX(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_TRIX") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_TRIX");
        this.timePeriods = 12;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte stepEMA1, byte stepEMA2, byte stepEMA3,
                                          byte srcIndex, byte destIndex) {

        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        int loopBegin = bIndex + (t_im_ePeriods - 1) * 3 + 1;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;
        ChartRecord cr;

        MovingAverage.getMovingAverage(al, 0, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA1);
        MovingAverage.getMovingAverage(al, t_im_ePeriods - 1, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA1, stepEMA2);
        MovingAverage.getMovingAverage(al, 2 * (t_im_ePeriods - 1), t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA2, stepEMA3);

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        cr = (ChartRecord) al.get(loopBegin - 1);
        double currVal, prevVal = cr.getStepValue(stepEMA3);
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA3);

            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(currVal == 0 ? 0 : 100f * (currVal - prevVal) / currVal);
            }*/
            cr.setValue(destIndex, currVal == 0 ? 0 : 100f * (currVal - prevVal) / currVal);
            prevVal = currVal;
        }
        //System.out.println("**** TRIX Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 12;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof TRIX) {
            TRIX trix = (TRIX) cp;
            this.timePeriods = trix.timePeriods;
            this.innerSource = trix.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_TRIX") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this includes 3 steps EMA1, EMA2, EMA3
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        int loopBegin = (timePeriods - 1) * 3 + 1;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;
        ChartRecord cr;
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepEMA1 = ChartRecord.STEP_1;
        byte stepEMA2 = ChartRecord.STEP_2;
        byte stepEMA3 = ChartRecord.STEP_3;

        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA1);
        MovingAverage.getMovingAverage(al, timePeriods - 1, timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA1, stepEMA2);
        MovingAverage.getMovingAverage(al, 2 * (timePeriods - 1), timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA2, stepEMA3);

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        cr = (ChartRecord) al.get(loopBegin - 1);
        double currVal, prevVal = cr.getStepValue(stepEMA3);
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getStepValue(stepEMA3);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(currVal == 0 ? 0 : 100f * (currVal - prevVal) / currVal);
            }
            prevVal = currVal;
        }
        //System.out.println("**** TRIX Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_TRIX");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}