package com.isi.csvr.chart;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Jun 8, 2011
 * Time: 4:18:27 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FillArea {

    public boolean isFillBands();

    public void setFillBands(boolean filling);

    public Color getFillTransparentColor();

    public int getTransparency();

    public Color getFillColor();

    public void setFillColor(Color color);
}
