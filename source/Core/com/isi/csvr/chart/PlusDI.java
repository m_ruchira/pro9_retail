package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 20, 2006
 * Time: 4:42:39 PM
 */
public class PlusDI extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_PLUS_DI;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public PlusDI(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_PLUS_DI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_PLUS_DI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    /**
     * This is to be used by other OHLC indicators those calculate an +DI as an intermediate step (DX, ADX and ADXR)
     *
     * @param al          The ArrayList which has Object[] as items. In this object[] first element is time, second is a float array containing O,H,L,C valuse
     *                    and third is a blank which the calculated value is loaded when the method is returned
     * @param bIndex      The begin index of the ArrayList which the calculation should start
     * @param timePeriods Time period of the calculation
     */
    // This includes 1 intermediate step, SumOfTrueRange
    public static void getPlusDI(ArrayList al, int bIndex, int timePeriods, byte destIndex, byte stepSumOfTR) {
        ChartRecord cr, crOld;
        double currVal, preVal = 0;

        if (al.size() < timePeriods || timePeriods < 2) return;

        long entryTime = System.currentTimeMillis();

        // Calculate the sum of true range
        AverageTrueRange.getSumOfTrueRange(al, 0, timePeriods, stepSumOfTR);
        // calculating first +DI
        for (int i = bIndex + 1; i < bIndex + timePeriods + 1; i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (((cr.High - crOld.High) > (crOld.Low - cr.Low)) && (cr.High > crOld.High)) {
                preVal += cr.High - crOld.High;
            }

            if (i == bIndex + timePeriods) {
                cr.setStepValue(destIndex, preVal * 100 / cr.getStepValue(stepSumOfTR));
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        // calculating the rest of +DI
        for (int i = bIndex + timePeriods + 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (((cr.High - crOld.High) > (crOld.Low - cr.Low)) && (cr.High > crOld.High)) {
                currVal = cr.High - crOld.High;
            } else {
                currVal = 0;
            }

            preVal = preVal - (preVal / timePeriods) + currVal;
            cr.setStepValue(destIndex, preVal * 100 / cr.getStepValue(stepSumOfTR));
        }

        //System.out.println("**** Inter +DI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof PlusDI) {
            PlusDI plusDI = (PlusDI) cp;
            this.timePeriods = plusDI.timePeriods;
            this.innerSource = plusDI.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_PLUS_DI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // This includes 1 intermediate step, SumOfTrueRange
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        ChartRecord cr, crOld;
        double currVal, preVal = 0;

        if (al.size() < timePeriods || timePeriods < 2) return;

        long entryTime = System.currentTimeMillis();
        //setting step size 1
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(1);
        }
        // 1 step involved
        byte stepSumOfTR = ChartRecord.STEP_1;

        // Calculate the sum of true range
        AverageTrueRange.getSumOfTrueRange(al, 0, timePeriods, stepSumOfTR);
        // calculating first +DI
        for (int i = 1; i < timePeriods + 1; i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (((cr.High - crOld.High) > (crOld.Low - cr.Low)) && (cr.High > crOld.High)) {
                preVal += cr.High - crOld.High;
            }

            if (i == timePeriods) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(preVal * 100 / cr.getStepValue(stepSumOfTR));
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        // calculating the rest of +DI
        for (int i = timePeriods + 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);

            if (((cr.High - crOld.High) > (crOld.Low - cr.Low)) && (cr.High > crOld.High)) {
                currVal = cr.High - crOld.High;
            } else {
                currVal = 0;
            }

            preVal = preVal - (preVal / timePeriods) + currVal;
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(preVal * 100 / cr.getStepValue(stepSumOfTR));
        }

        //System.out.println("**** +DI Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public String getShortName() {
        return Language.getString("IND_PLUS_DI");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
