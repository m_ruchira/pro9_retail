package com.isi.csvr.chart;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author unascribed
 * @version 1.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorStandardDeviationProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class StandardDeviation extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_StdDeviation;
    //Fields
    private int timePeriods;
    private float deviations;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public StandardDeviation(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("STD_DEVIATION") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_STD_DEVIATION");
        this.timePeriods = 14;
        this.deviations = 1f;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorStandardDeviationProperty idp = (IndicatorStandardDeviationProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());

            this.setTimePeriods(idp.getTimePeriods());
            this.setDeviations(idp.getDeviations());
        }
    }

    public static void getStandardDeviation(ArrayList al, int bIndex, int timePeriods, byte srcIndex, byte destIndex) {
        long entryTime = System.currentTimeMillis();

        if ((timePeriods >= al.size()) || (timePeriods < 2)) return;
        double zigmaX = 0, zigmaXX = 0;
        double xi, xPi, stdDev, xBase = 0;
        ChartRecord cr, crOld;

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(destIndex, Indicator.NULL);
        }

        for (int i = bIndex; i < (bIndex + timePeriods); i++) {
            cr = (ChartRecord) al.get(i);
            if (i == bIndex) {
                xBase = cr.getStepValue(srcIndex);
            }
            xi = cr.getStepValue(srcIndex) - xBase;
            zigmaX += xi;
            zigmaXX += xi * xi;
            if (i == (bIndex + timePeriods - 1)) {
                stdDev = (float) Math.sqrt(zigmaXX / timePeriods - zigmaX * zigmaX / timePeriods / timePeriods);
                cr.setStepValue(destIndex, stdDev);
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = (bIndex + timePeriods); i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - timePeriods);
            cr = (ChartRecord) al.get(i);
            xi = cr.getStepValue(srcIndex) - xBase;
            xPi = crOld.getStepValue(srcIndex) - xBase;

            zigmaX = zigmaX + xi - xPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;

            stdDev = (float) Math.sqrt(zigmaXX / timePeriods - zigmaX * zigmaX / timePeriods / timePeriods);
            cr.setStepValue(destIndex, stdDev);
        }

        //System.out.println("**** Inter SD Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, double deviations, byte srcIndex, byte destIndex) {

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        int loopBegin = bIndex + t_im_ePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }

        // when single point, standard deviation = 0
        if (t_im_ePeriods == 1) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, 0);
            }
            return;
        }

        double zigmaX = 0, zigmaXX = 0;
        double xi, xPi, xBase = 0;

        cr = (ChartRecord) al.get(bIndex);
        // no need to validate as bIndex is already validated
        xBase = cr.getStepValue(srcIndex);

        int count = 0;
        for (int i = bIndex; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            boolean isValid = false;
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = val - xBase;
                zigmaX += xi;
                zigmaXX += xi * xi;
            }
            if ((i >= loopBegin - 1) && isValid) {
                double var = Math.max(zigmaXX / count - zigmaX * zigmaX / count / count, 0);
                cr.setStepValue(destIndex, deviations * Math.sqrt(var));
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - t_im_ePeriods);
            double val = crOld.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                count--;

                xPi = crOld.getStepValue(srcIndex) - xBase;
                zigmaX = zigmaX - xPi;
                zigmaXX = zigmaXX - xPi * xPi;
            }
            boolean isValid = false;
            cr = (ChartRecord) al.get(i);
            val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = cr.getStepValue(srcIndex) - xBase;
                zigmaX = zigmaX + xi;
                zigmaXX = zigmaXX + xi * xi;
            }
            if (isValid && (count > 0)) {
                double var = Math.max(zigmaXX / count - zigmaX * zigmaX / count / count, 0);
                cr.setStepValue(destIndex, deviations * Math.sqrt(var));
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        //System.out.println("**** SD Step Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
        this.deviations = 1f;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.deviations = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/Deviations"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "Deviations", Float.toString(this.deviations));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof StandardDeviation) {
            StandardDeviation sd = (StandardDeviation) cp;
            this.timePeriods = sd.timePeriods;
            this.innerSource = sd.innerSource;
            this.deviations = sd.deviations;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("STD_DEVIATION") + " " + parent;
    }

    protected String getDeviationString() {
        DecimalFormat numformat = new DecimalFormat("##0.0###");
        return numformat.format(deviations);
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //deviations
    public float getDeviations() {
        return deviations;
    }

    public void setDeviations(float d) {
        deviations = d;
    }
    //############################################

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        double zigmaX = 0, zigmaXX = 0;
        double xi, xPi, xBase = 0;
        ChartRecord cr, crOld;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            if (i == 0) {
                xBase = cr.getValue(getOHLCPriority());
            }
            xi = cr.getValue(getOHLCPriority()) - xBase;
            zigmaX += xi;
            zigmaXX += xi * xi;
            if (i >= loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(deviations * Math.sqrt(zigmaXX / timePeriods - zigmaX * zigmaX / timePeriods / timePeriods));
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        //float tmpFloat;
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - timePeriods);
            cr = (ChartRecord) al.get(i);
            xi = cr.getValue(getOHLCPriority()) - xBase;
            xPi = crOld.getValue(getOHLCPriority()) - xBase;

            zigmaX = zigmaX + xi - xPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) { // check for isNaN is omitted
                aPoint.setIndicatorValue(deviations * Math.sqrt(zigmaXX / timePeriods - zigmaX * zigmaX / timePeriods / timePeriods));
            }
        }
        //System.out.println("**** SD Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("STD_DEVIATION");
    }

    //To be used by custom indicators
/*
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
        int t_im_ePeriods, double deviations, byte srcIndex, byte destIndex) {

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        int loopBegin = t_im_ePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) {
            for (int i = 0; i < loopBegin; i++) {
                cr = (ChartRecord)al.get(i);
//                cr.setValue(destIndex, Indicator.NULL);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }

        double zigmaX = 0, zigmaXX = 0;
        double xi, xPi, xBase = 0;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord)al.get(i);
            if (i == 0) {
                xBase = cr.getStepValue(srcIndex);
            }
            xi = cr.getStepValue(srcIndex) - xBase;
            zigmaX += xi;
            zigmaXX += xi * xi;
            if (i >= loopBegin - 1) {
//                cr.setValue(destIndex, deviations * Math.sqrt(zigmaXX / (double)t_im_ePeriods - zigmaX * zigmaX / (double)t_im_ePeriods / (double)t_im_ePeriods));
                cr.setStepValue(destIndex, deviations * Math.sqrt(zigmaXX / (double)t_im_ePeriods - zigmaX * zigmaX / (double)t_im_ePeriods / (double)t_im_ePeriods));
            } else {
//                cr.setValue(destIndex, Indicator.NULL);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord)al.get(i - t_im_ePeriods);
            cr = (ChartRecord)al.get(i);
            xi = cr.getStepValue(srcIndex) - xBase;
            xPi = crOld.getStepValue(srcIndex) - xBase;

            zigmaX = zigmaX + xi - xPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;

//            cr.setValue(destIndex, deviations * Math.sqrt(zigmaXX / (double)t_im_ePeriods - zigmaX * zigmaX / (double)t_im_ePeriods / (double)t_im_ePeriods));
            cr.setStepValue(destIndex, deviations * Math.sqrt(zigmaXX / (double)t_im_ePeriods - zigmaX * zigmaX / (double)t_im_ePeriods / (double)t_im_ePeriods));
        }
        System.out.println("**** SD Step Calc time " + (entryTime - System.currentTimeMillis()));
    }
*/

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

}
