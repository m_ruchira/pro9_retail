package com.isi.csvr.chart;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 8, 2005 - Time: 11:10:17 AM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public interface DataWindowListener {
    /**
     * Invoked when a mouse button has been moved on a component.
     */
    public void dataPointChanged(MouseEvent e, Point CurrentMousePos, StockGraph graph);
}
