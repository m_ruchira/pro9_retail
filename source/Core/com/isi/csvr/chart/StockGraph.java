/*
 * Classname:     CurrentFilesManager.java
 * @author Udaka Liyanapathirana
 * Version:       Chart 1.0_beta
 * Date:          22nd August 2001
 * Copyright:     (c) 2001 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

package com.isi.csvr.chart;


import com.isi.csvr.ChartInterface;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.options.ChartTheme;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;


public class StockGraph extends SplitPanel implements Serializable {

    //-------------------------------------
    // Public constants used
    //-------------------------------------
    // interval is uniquely defined by seconds in it
    public static final long YEARLY = 365 * 24 * 3600;
    public static final long QUARTERLY = 365 * 6 * 3600;
    public static final long MONTHLY = 30 * 24 * 3600;
    public static final long WEEKLY = 7 * 24 * 3600;
    public static final long DAILY = 1 * 24 * 3600;
    public final long MIN_HISTORY_INTERVAL = 7 * DAILY;
    private long interval = DAILY;
    public static final long EVERY60MINUTES = 60 * 60;
    public static final long EVERY30MINUTES = 1800;
    public static final long EVERY15MINUTES = 15 * 60;
    public static final long EVERY10MINUTES = 10 * 60;
    public static final long EVERY5MINUTES = 5 * 60;
    public static final long EVERYMINUTE = 60;
    // adjustment for the begin day of the week
    public static final long WEEK_BEGIN_ADJ_MONDAY = 4 * 24 * 3600;
    public static final long WEEK_BEGIN_ADJ_TUESDAY = 5 * 24 * 3600;
    public static final long WEEK_BEGIN_ADJ_WEDNESDAY = 6 * 24 * 3600;
    public static final long WEEK_BEGIN_ADJ_THURSDAY = 0 * 24 * 3600;
    public static final long WEEK_BEGIN_ADJ_FRIDAY = 1 * 24 * 3600;
    public static final long WEEK_BEGIN_ADJ_SATURDAY = 2 * 24 * 3600;
    private long beginDayOfWeekAdj = WEEK_BEGIN_ADJ_SATURDAY;
    public static final long WEEK_BEGIN_ADJ_SUNDAY = 3 * 24 * 3600;
    //const for grfstyles
    public final static int INT_GRAPH_STYLE_LINE = 0;
    public final static int INT_GRAPH_STYLE_BAR = 1;
    public final static int INT_GRAPH_STYLE_HLC = 2;
    public final static int INT_GRAPH_STYLE_OHLC = 3;
    private int graphStyle = INT_GRAPH_STYLE_OHLC;
    public final static int INT_GRAPH_STYLE_CANDLE = 4;
    public final static int INT_GRAPH_STYLE_PERCET = 5;
    public static final int ONE_DAY = 0;
    public static final int SIX_DAYS = 1;
    public static final int ONE_WEEK = 2;
    public static final int ONE_MONTH = 3;
    public static final int THREE_MONTHS = 4;
    public static final int SIX_MONTHS = 5;
    public static final int YTD = 6;
    public static final int ONE_YEAR = 7;
    private int period = ONE_YEAR;
    public static final int EIGHTEEN_MONTHS = 8;
    public static final int TWO_YEARS = 9;
    public static final int THREE_YEARS = 10;
    public static final int FIVE_YEARS = 11;
    public static final int TEN_YEARS = 12;
    public static final int ALL_HISTORY = 13;
    public static final int CUSTOM_PERIODS = 14;
    // legend types
    public static final int INT_LEGEND_SYMBOL = 0;
    private int legendType = INT_LEGEND_SYMBOL;
    public static final int INT_LEGEND_SHORT_DESCRIPTION = 1;
    public static final int INT_LEGEND_COMPANY_NAME = 2;
    public final static Font font_PLAIN_10 = Theme.getDefaultFont(Font.PLAIN, 10);
    public final static Font font_PLAIN_9 = Theme.getDefaultFont(Font.PLAIN, 9);
    public final static Font font_PLAIN_8 = Theme.getDefaultFont(Font.PLAIN, 8);
    public final static Font font_BOLD_10 = Theme.getDefaultFont(Font.BOLD, 10);
    public final static Font font_BOLD_9 = Theme.getDefaultFont(Font.BOLD, 9);
    public final static Font font_BOLD_8 = Theme.getDefaultFont(Font.BOLD, 8);
    public static Color fontColor = new Color(50, 100, 60);
    public static Color clDkShadow = UIManager.getColor("controlDkShadow");
    public static Color clLtShadow = UIManager.getColor("controlHighlight");
    public static Color clHighlight = UIManager.getColor("controlShadow");
    public static Color clLtHighlight = UIManager.getColor("controlLtHighlight");
    public final long MIN_INTRADAY_INTERVAL = 3600;
    public final Cursor Cursor_HAND = new Cursor(Cursor.HAND_CURSOR);

    //-------------------------------------
    // Protected/Private variables used
    //-------------------------------------
    // to hold Recd data
    public final Cursor Cursor_SE_RESIZE = new Cursor(Cursor.SE_RESIZE_CURSOR);
    public final int MAX_ALLOWED_GRAPHS = 20;
    public final int infoPnlHeight = 90;
    public final int infoPnlWidth = 125;
    //class written for the zooming slider
    protected final int sleepTime = 100;
    final BasicStroke BS_0p5f_solid = new BasicStroke(0.5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_0p8f_solid = new BasicStroke(0.8f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_1p0f_solid = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_1p2f_solid = new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    //final BasicStroke BS_1p5f_solid = new BasicStroke(1.5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_1p4f_solid = new BasicStroke(1.4f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);//todo shashika important
    final BasicStroke BS_1p8f_solid = new BasicStroke(1.8f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_2p0f_solid = new BasicStroke(2.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_2p5f_solid = new BasicStroke(2.5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_3p0f_solid = new BasicStroke(3.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    final BasicStroke BS_4p0f_solid = new BasicStroke(4.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    public ZoomingSlider zSlider = new ZoomingSlider(200, 14, 100, 0);
    public boolean isZoomed = false;
    public GraphDataManager GDMgr;
    //properties
    public String Symbol;
    public Color graphUpColor = new Color(100, 100, 190);
    public Color graphDownColor = new Color(100, 100, 190);
    public Color graphVolumeUpColor = Theme.getColor("GRAPH_VOLUME_UP_COLOR");
    public Color graphVolumeDownColor = Theme.getColor("GRAPH_VOLUME_DOWN_COLOR");
    public Color graphTurnOverUpColor = Theme.getColor("GRAPH_TURNOVER_UP_COLOR");
    public Color graphTurnOverDownColor = Theme.getColor("GRAPH_TURNOVER_UP_COLOR");
    //colors
    public Color graphAreaRightColor = new Color(100, 100, 190);
    public Color graphAreaLeftColor = new Color(100, 100, 190);
    public Color graphPnltitle_LeftColor = new Color(100, 100, 190);
    public Color graphPnltitle_RightColor = new Color(100, 100, 190);
    public Color barColor = new Color(200, 60, 0);
    public Color backColor = new Color(240, 220, 200);
    public Color borderColor = new Color(250, 70, 70);
    public Color crossColor = Color.black;
    public Color toolTipColor = new Color(230, 240, 250);
    public Color labelColor = new Color(180, 200, 170);
    public Color sessionBreakColor = new Color(70, 70, 151);
    public Color gridColor = Color.lightGray;
    public Color newsColor = new Color(243, 243, 200);
    public Color InactiveLineColor = new Color(200, 50, 50);
    public Color ActiveLineColor = new Color(0, 0, 200);
    public Color yearSeparatorColor = new Color(0, 0, 200);
    public Color chartSelectedColor = new Color(0, 0, 200);
    public Color dragZoomColor = Color.BLUE;
    public Color crossHairColor = Color.BLACK;
    public Color axisBGInnerColor = Color.WHITE;
    public Color axisBGOuterColor = Color.WHITE;
    public Color axisColor = Color.BLACK;
    public Color titleForeGrongColor = Color.BLACK;
    //fonts
    public Font graphTitleFont = new Font("Arial", 1, 12);
    //public variables
    public boolean NotifyChange = true;
    public boolean GraphReset = true;
    transient public boolean AnimationStopped = true;
    transient public String timeValue = " ";
    transient public String timeLable = " ";
    transient public String priceCloseValue = " ";
    transient public String priceOpenValue = " ";
    transient public String priceHighValue = " ";
    transient public String priceLowValue = " ";
    transient public String priceVolLable = " ";
    transient public String Announcement = " ";
    public Color currGrfColor = SystemColor.control;
    public String currSymbol = " ";
    public String currVolume = " ";
    public String currTurnOver = " ";
    public double XaxisPitch = 0.8;
    public double YaxisPitch = 0.4;
    public int SpotRadius = 2;
    public boolean drawSpot = false;
    public boolean showNews = true;
    public int LegendLength = 60;
    public String newsImagePath = "";
    protected boolean waiting = true;
    protected DynamicArray Recd = new DynamicArray();           // Base Graph Data
    protected ArrayList LinesArray = new ArrayList(5);
    protected int selectedPriceLineIndex = -1;
    protected int selectedVolumeLineIndex = -1;
    transient protected long minX, maxX;
    transient protected int minXGap;
    transient protected double minYp, maxYp;
    transient protected long minYv, maxYv;
    //tooltip
    transient protected JLabel lblToolTip;
    // to record the status
    protected boolean needToRepaint, needXaxisRepaint, needYaxisRepaint;
    // vars needed for Cross
    protected boolean isDirty = false;
    protected boolean isTTDirty = false;
    protected int ttLeft, ttTop, ttWidth, ttHeight;
    private boolean active = true;
    transient Thread myTimer = new Thread("TW Graph Timer") {
        public synchronized void run() {
            while (active) {
                waiting = false;
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException ss) {
                    ss.printStackTrace();
                }
            }
        }
    };
    transient private int ex, wy;
    private int graphCount = 0;  // Comparison Graph Count
    private boolean inThousands = true;
    private boolean graphVisible = true;
    transient private boolean sliderdragged = false;
    transient private boolean compGraphLoad = false;
    transient private boolean onLine = false;
    transient private boolean pcntState = true;
    private boolean baseGraph = false;
    private boolean yAxisRescaleDirty = false;
    private boolean rMarginRescaleDirty = false;
    private Font axisFont = new Font("Arial", Font.PLAIN, 9);
    private Font legendFont = new Font("Tahoma", Font.PLAIN, 12);
    //cashflow
    private boolean isCashFlowMode = false;
    private boolean currentMode;
    transient private GregorianCalendar calen = new GregorianCalendar(); //calen = Calendar.getInstance();
    private int currentXPos, currentYPos;
    private boolean blobChanged = false;
    private boolean blobOn = false;
    private long msmvCount = 0;
    private long beginInactiveGap = 0;
    private long endInactiveGap = 0;
    transient private boolean showCross = true;
    private int TotalHeightTovolGrfHeight = 4;
    private boolean showPercentages = false;
    private double zoom = 100;
    private double currPosPcnt = 0;
    transient private ToolTipChangeListener g_oToolTipListener = null;
    ///added by charithn
    private Cursor CURSOR_ZOOM_IN = createCursorFromImage("images/Theme" + Theme.getID() + "/zoom_in_cursor.gif");
    private Cursor CURSOR_ZOOM_OUT = createCursorFromImage("images/Theme" + Theme.getID() + "/zoom_out_cursor.gif");
    private Cursor CURSOR_BOX_ZOOM = createCursorFromImage("images/Theme" + Theme.getID() + "/box_zoom_cursor.gif");
    private Cursor CURSOR_DRAG_ZOOM = createCursorFromImage("images/Theme" + Theme.getID() + "/drag_zoom_cursor.gif");
    //Constructor
    public StockGraph() {
        try {
            jbInit();
            GDMgr = new GraphDataManager(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String extractSymbolFromStr(String symbStr) {
        try {
            String[] keyData = symbStr.split(Constants.KEY_SEPERATOR_CHARACTER);
            if ((keyData != null) && (keyData.length > 1)) {
                return keyData[1];
            } else {
                return symbStr;
            }
        } catch (Exception e) {
            return symbStr;
        }
    }

    public Color getAxisColor() {
        return axisColor;
    }

    public void setAxisColor(Color col) {
        axisColor = col;
        needXaxisRepaint = true;
        needYaxisRepaint = true;
        repaint();
    }

    public Color getAxisBGOuterColor() {
        return axisBGInnerColor;
    }

    public void setAxisBGOuterColor(Color axisRighttColor) {
        this.axisBGOuterColor = axisRighttColor;
    }

    public Color getAxisBGInnerColor() {
        return axisBGOuterColor;
    }

    public void setAxisBGInnerColor(Color axisBGinnerColor) {
        this.axisBGInnerColor = axisBGinnerColor;
    }

    public Color getChartSelectedColor() {
        return chartSelectedColor;
    }

    public void setChartSelectedColor(Color chartSelectedColor) {
        this.chartSelectedColor = chartSelectedColor;
    }

    public Color getGraphPnltitleRightColor() {
        return graphPnltitle_RightColor;
    }

    public void setGraphPnltitleRightColor(Color graphPnltitle_RightColor) {
        this.graphPnltitle_RightColor = graphPnltitle_RightColor;
    }

    public Color getGraphPnltitleLeftColor() {
        return graphPnltitle_LeftColor;
    }

    public void setGraphPnltitleLeftColor(Color graphPnltitle_LeftColor) {
        this.graphPnltitle_LeftColor = graphPnltitle_LeftColor;
    }

    public Color getDragZoomColor() {
        return dragZoomColor;
    }

    public void setDragZoomColor(Color dragZoomColor) {
        this.dragZoomColor = dragZoomColor;
    }

    public Color getCrossHairColor() {
        return crossHairColor;
    }

    public void setCrossHairColor(Color crossHairColor) {
        this.crossHairColor = crossHairColor;
    }

    public Color getTitleForeGroundColor() {
        return titleForeGrongColor;
    }

    public void setTitleForeGroundColor(Color col) {
        this.titleForeGrongColor = col;
    }

    public Font getGraphTitleFont() {
        return graphTitleFont;
    }

    public void setGraphTitleFont(Font graphTitleFont) {
        this.graphTitleFont = graphTitleFont;
    }

    public void addToolTipChangeListener(ToolTipChangeListener oToolTipListener) {
        g_oToolTipListener = oToolTipListener;
    }

    // common core for the constructor(s)
    private void jbInit() throws Exception {

        currentXPos = 0;
        currentYPos = 0;
        this.setLayout(null);

        setImageWdHt(250, 150);
        needToRepaint = true;
        needXaxisRepaint = true;
        needYaxisRepaint = true;

        lblToolTip = new JLabel();
        lblToolTip.setBounds(0, 0, 85, 59);
        lblToolTip.setVisible(false);

        zSlider.setBounds(1, 1, 200, sliderHeight);
        zSlider.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                sliderMouseDragged(e);
            }
        });
        zSlider.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                sliderMousePressed(e);
            }

            public void mouseReleased(MouseEvent e) {
                sliderMouseReleased(e);
            }
        });
        this.add(lblToolTip, null);
        this.add(zSlider, null);
    }

    // calculating available Wd, Ht for graph image when sizing the comp
    protected void setImageWdHt(int w, int h) {
        super.setImageWdHt(w, h);
        if (GDMgr != null) {
            if (GDMgr.getMaxPriceValue() != GDMgr.getTmpPriceValue()) {
                GDMgr.updateMaxPriceValue();
                super.setImageWdHt(w, h);
            }
        }
        int tmp_TOP = this.getHeight() - 12 - borderWidth;
        int tmp_LEFT = borderWidth + widthYaxis_1;
        zSlider.setBounds(tmp_LEFT, tmp_TOP, Wd, sliderHeight);
        zSlider.setVisible(true);
    }

    protected void setYAxisWidth() {
        super.setYAxisWidth();

        if (GDMgr != null) {
            widthYaxis = GDMgr.getMaxYAxisWidth() + 25;
            setYAxisWidths();
        }
    }

    protected void setYAxisWidths() {
        switch (yAxisPos) {
            case SplitPanel.AXIS_LEFT_ONLY:
                widthYaxis_1 = widthYaxis;
                widthYaxis_2 = 0;
                break;
            case SplitPanel.AXIS_RIGHT_ONLY:
                widthYaxis_1 = 0;
                widthYaxis_2 = widthYaxis;
                break;
            case SplitPanel.AXIS_BOTH_ENDS:
                widthYaxis_1 = widthYaxis;
                widthYaxis_2 = widthYaxis;
                break;
        }
    }

    public void recalculateRectBounds() {
        super.recalculateRectBounds();
        if (GDMgr != null)
            GDMgr.reCalculateGraphParams();
    }

    //  loading base graph data from flat files
    public void setBaseGraph(boolean bg) {
        baseGraph = bg;
    }

    public ChartProperties setBaseRecords(DynamicArray vectRecords, String smbl) {
        try {
            graphCount = 0;
            Symbol = smbl;
            GDMgr.resetEntireGraphStore();

            //newly added
            GDMgr.resetLastCandle();
            GDMgr.dynamicCandleStockVolume = 0;
            GDMgr.dynamicCandleStockTurnover = 0;
            GDMgr.dynamicCandleIsFirstMinute = true;

            initialliseRects();
            ChartProperties cp = ChartSettings.getSettings().getDefaultCP(vectRecords.getList(), smbl, GraphDataManager.ID_BASE, graphUpColor, mainRect);
            cp.setSpTable(OHLCStore.getInstance().getSplitPointTable(smbl));
            cp = GDMgr.addDataSource(vectRecords.getList(), smbl, graphUpColor, GraphDataManager.ID_BASE, mainRect, cp);
            GDMgr.setPeriodBeginIndex(period, true);
            setGraphStyle(cp.getChartStyle());
            GDMgr.addVolumeToSources();
            GDMgr.addTurnOverToSources();
            Recd = vectRecords;
            graphVisible = true;
            compGraphLoad = false;
            baseGraph = true;
            GraphReset = true;
            NotifyChange = true;
            reDrawAll();
            return cp;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ChartProperties setComparisonRecords(DynamicArray vectRecords, Vector ChartInfo, String strSymbol, Color c) {
        ChartProperties cp = null;
        try {
            if (graphCount > MAX_ALLOWED_GRAPHS - 1) {
                return cp;
            }
            graphCount++;
            cp = new ChartProperties(vectRecords.getList(), strSymbol, GraphDataManager.ID_COMPARE, c, mainRect);
            cp.setSpTable(OHLCStore.getInstance().getSplitPointTable(strSymbol));
            cp = GDMgr.addDataSource(vectRecords.getList(), strSymbol, c, GraphDataManager.ID_COMPARE, mainRect, cp);
            baseGraph = true;
            graphVisible = true;
            compGraphLoad = true;
            if (vectRecords.size() > 0) {
                NotifyChange = true;
                reDrawAll();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return cp;
    }

    public void setDragZoom(int x1, int x2) {
        if ((GDMgr != null)) {
            GDMgr.setDragZoom(x1, x2);
            needToRepaint = true;
            reDrawAll();
        }
    }

    public void setBoxZoom(int x1, int y1, int x2, int y2) {
        if ((GDMgr != null)) {
            GDMgr.setBoxZoom(x1, y1, x2, y2);
            needToRepaint = true;
            reDrawAll();
        }
    }

    //added by charithn
    public void setZoomIn(int x) {
        if (GDMgr != null) {
            GDMgr.setZoomIn(x, Wd, widthYaxis_1, widthYaxis_2, (int) mainRect.getX());
            needToRepaint = true;
            reDrawAll();
        }
    }

    //added by charithn
    public void setZoomOut(int x) {
        if (GDMgr != null) {
            GDMgr.setZoomOut(x, Wd, widthYaxis_1, widthYaxis_2, (int) mainRect.getX());
            needToRepaint = true;
            reDrawAll();
        }
    }

    private Cursor createCursorFromImage(String imgName) {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image img = tk.getImage(imgName);
        Cursor cur = tk.createCustomCursor(img, new Point(0, 0), "ZoomCursors");
        return cur;
    }

    public void toggleZoomingMode(int type) {

        switch (type) {
            case AbstractObject.INT_ZOOM_IN:
                this.setCursor(CURSOR_ZOOM_IN);
                break;

            case AbstractObject.INT_ZOOM_OUT:
                this.setCursor(CURSOR_ZOOM_OUT);
                break;

            case AbstractObject.INT_BOX_ZOOM:
                this.setCursor(CURSOR_BOX_ZOOM);
                break;

            case AbstractObject.INT_DRAG_ZOOM:
                this.setCursor(CURSOR_DRAG_ZOOM);
                break;

            default:
                this.setCursor(Cursor.getDefaultCursor());
                break;
        }
    }

    public Point2D.Double getActualPoint2D(Point2D.Float aP2D, int pnlID) {
        if (GDMgr != null) {
            return GDMgr.getActualPoint2D(aP2D, pnlID);
        }
        return new Point2D.Double(0, 0);
    }

    //remove added comparison graphs
    public void removeCompGraph(String smbl) {
        if (graphCount > 0) {
            graphCount--;
            GDMgr.removeDataSource(smbl);
        }
    }

    public long getIntervalConst(String s) {
        if (s.equalsIgnoreCase(Language.getString("1_YEAR"))) return YEARLY;
        else if (s.equalsIgnoreCase(Language.getString("3_MONTH"))) return QUARTERLY;
        else if (s.equalsIgnoreCase(Language.getString("MONTHLY"))) return MONTHLY;
        else if (s.equalsIgnoreCase(Language.getString("WEEKLY"))) return WEEKLY;
        else if (s.equalsIgnoreCase(Language.getString("DAILY"))) return DAILY;
        else if (s.equalsIgnoreCase(Language.getString("EVERY_MINUTE"))) return EVERYMINUTE;
        else if (s.equalsIgnoreCase(Language.getString("10_MIN"))) return EVERY10MINUTES;
        else if (s.equalsIgnoreCase(Language.getString("5_MIN"))) return EVERY5MINUTES;
        else if (s.equalsIgnoreCase(Language.getString("30_MIN"))) return EVERY30MINUTES;
        else if (s.equalsIgnoreCase(Language.getString("15_MIN"))) return EVERY15MINUTES;
        else if (s.equalsIgnoreCase(Language.getString("60_MIN"))) return EVERY60MINUTES;
        else if (s.equalsIgnoreCase(Language.getString("OTHER_PERIODS")))
            return GDMgr.currentCustomIntervalType * GDMgr.currentCustomIntervalFactor;
        else if (isCurrentMode()) return EVERYMINUTE;
        else
            return MONTHLY;
    }

    public String getIntervalString() {
        if (interval == YEARLY) {
            return Language.getString("1_YEAR");
        } else if (interval == QUARTERLY) {
            return Language.getString("3_MONTH");
        } else if (interval == MONTHLY) {
            return Language.getString("MONTHLY");
        } else if (interval == WEEKLY) {
            return Language.getString("WEEKLY");
        } else if (interval == DAILY) {
            return Language.getString("DAILY");
        } else if (interval == EVERYMINUTE) {
            return Language.getString("EVERY_MINUTE");
        } else if (interval == EVERY10MINUTES) {
            return Language.getString("10_MIN");
        } else if (interval == EVERY5MINUTES) {
            return Language.getString("5_MIN");
        } else if (interval == EVERY15MINUTES) {
            return Language.getString("15_MIN");
        } else if (interval == EVERY30MINUTES) {
            return Language.getString("30_MIN");
        } else if (interval == EVERY60MINUTES) {
            return Language.getString("60_MIN");
        } else {
            return Language.getString("OTHER_PERIODS");
        }
    }

    public int getPeriodConst(String s) {
        if (s.equalsIgnoreCase(Language.getString("1_DAY"))) return ONE_DAY;
        else if (s.equalsIgnoreCase(Language.getString("6_DAYS"))) return SIX_DAYS;
        else if (s.equalsIgnoreCase(Language.getString("1_WEEK"))) return ONE_WEEK;
        else if (s.equalsIgnoreCase(Language.getString("1_MONTH"))) return ONE_MONTH;
        else if (s.equalsIgnoreCase(Language.getString("3_MONTH"))) return THREE_MONTHS;
        else if (s.equalsIgnoreCase(Language.getString("6_MONTH"))) return SIX_MONTHS;
        else if (s.equalsIgnoreCase(Language.getString("YTD"))) return YTD;
        else if (s.equalsIgnoreCase(Language.getString("1_YEAR"))) return ONE_YEAR;
        else if (s.equalsIgnoreCase(Language.getString("18_MONTHS"))) return EIGHTEEN_MONTHS;
        else if (s.equalsIgnoreCase(Language.getString("2_YEAR"))) return TWO_YEARS;
        else if (s.equalsIgnoreCase(Language.getString("3_YEAR"))) return THREE_YEARS;
        else if (s.equalsIgnoreCase(Language.getString("5_YEAR"))) return FIVE_YEARS;
        else if (s.equalsIgnoreCase(Language.getString("10_YEAR"))) return TEN_YEARS;
        else if (s.equalsIgnoreCase(Language.getString("ALL"))) return ALL_HISTORY;
        else if (s.equalsIgnoreCase(Language.getString("OTHER_PERIODS"))) return CUSTOM_PERIODS;
        else
            return ALL_HISTORY;
    }

    //  get()/set() methods for properties

    // backColor property

    public String getPeriodString(int period) {
        switch (period) {
            case StockGraph.ONE_DAY:
                return Language.getString("1_DAY");
            case StockGraph.SIX_DAYS:
                return Language.getString("6_DAYS");
            case StockGraph.ONE_WEEK:
                return Language.getString("1_WEEK");
            case StockGraph.ONE_MONTH:
                return Language.getString("1_MONTH");
            case StockGraph.THREE_MONTHS:
                return Language.getString("3_MONTH");
            case StockGraph.SIX_MONTHS:
                return Language.getString("6_MONTH");
            case StockGraph.YTD:
                return Language.getString("YTD");
            case StockGraph.ONE_YEAR:
                return Language.getString("1_YEAR");
            case StockGraph.TWO_YEARS:
                return Language.getString("2_YEAR");
            case StockGraph.THREE_YEARS:
                return Language.getString("3_YEAR");
            case StockGraph.FIVE_YEARS:
                return Language.getString("5_YEAR");
            case StockGraph.TEN_YEARS:
                return Language.getString("10_YEAR");
            case StockGraph.CUSTOM_PERIODS:
                return Language.getString("OTHER_PERIODS");
            default:
                return Language.getString("ALL");
        }
    }

    public Color getBackColor() {
        return backColor;
    }

    public void setBackColor(Color c) {
        backColor = c;
        needToRepaint = true;
        repaint();
    }

    public Color getBorderColor() {
        return borderColor;
    }

    // borderColor property
    public void setBorderColor(Color c) {
        borderColor = c;
        repaint();
    }

    public int getBorderWidth() {
        return borderWidth;
    }

    public int getClearance() {
        return clearance;
    }

    public int getWidthYaxis() {
        return widthYaxis;
    }

    public int getHeightXaxis() {
        return heightXaxis;
    }

    public boolean isCurrentMode() {
        return currentMode;
    }

    // currentMode
    public void setCurrentMode(boolean isCurr) {
        if (currentMode != isCurr) {
            currentMode = isCurr;
            if (currentMode) {
                timeLable = " Time : ";
                GDMgr.setMinimumUnit(60);
            } else {
                timeLable = " Date : ";
                GDMgr.setMinimumUnit(3600 * 24);
            }
        }
    }

    // interval - needed when calculating weighted averages
    public void setIntervalFlag(long i) {
        interval = i;
        if (GDMgr != null)
            GDMgr.setIntervalFlag(i);
    }

    public long getInterval() {
        return interval;
    }

    public void setInterval(long i) {

        interval = i;
        if (GDMgr != null) {
            GDMgr.setInterval(i);
        }
    }

    public void setGraphFlag(int s) {
        graphStyle = s;
    }


    public void setPeriodFlag(int s) {
        period = s;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int p) {
        period = p;
        needToRepaint = true;
        compGraphLoad = false;
        if (GDMgr != null) {
            GDMgr.setPeriodBeginIndex(period, true);
        }
        reDrawAll();
    }

    public int getGraphStyle() {
        return graphStyle;
    }

    // graphStyle
    public void setGraphStyle(int s) {
        graphStyle = s;
        if (GDMgr != null)
            GDMgr.setGraphStyle((byte) s);
        needToRepaint = true;
        reDrawAll();
    }

    public Color getGTurnOverraphUpColor() {
        return graphTurnOverUpColor;
    }

    public void setTurnOverGraphUpColor(Color graphTurnOverUpColor) {
        this.graphTurnOverUpColor = graphTurnOverUpColor;
    }

    public Color getTurnOverGraphDownColor() {
        return graphTurnOverDownColor;
    }

    public void setTurnOverGraphDownColor(Color graphTurnOverDownColor) {
        this.graphTurnOverDownColor = graphTurnOverDownColor;
    }

    public void setYearSeparatorColor(Color c) {
        if (yearSeparatorColor != c) {
            yearSeparatorColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getGraphUpColor() {
        return graphUpColor;
    }

    // graphColor
    public void setGraphUpColor(Color c) {
        if (graphUpColor != c) {
            graphUpColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getGraphDownColor() {
        return graphDownColor;
    }

    public void setGraphDownColor(Color c) {
        if (graphDownColor != c) {
            graphDownColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getVolumeGraphUpColor() {
        return graphVolumeUpColor;
    }

    public void setVolumeGraphUpColor(Color c) {
        if (graphVolumeUpColor != c) {
            graphVolumeUpColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getVolumeGraphDownColor() {
        return graphVolumeDownColor;
    }

    public void setVolumeGraphDownColor(Color c) {
        if (graphVolumeDownColor != c) {
            graphVolumeDownColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getGraphAreaLeftColor() {
        return graphAreaLeftColor;
    }

    public void setGraphAreaLeftColor(Color c) {
        if (graphAreaLeftColor != c) {
            graphAreaLeftColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public Color getyearSeparatorColor() {
        return yearSeparatorColor;
    }

    public Color getGraphAreaRightColor() {
        return graphAreaRightColor;
    }

    public void setGraphAreaRightColor(Color c) {
        if (graphAreaRightColor != c) {
            graphAreaRightColor = c;
            needToRepaint = true;
            repaint();
        }
    }

    public void setShowCross(boolean show) {
        if (show) {
            showCross = true;
        } else {
            showCross = false;
        }
    }

    public boolean isShowPercentages() {
        return showPercentages;
    }

    public void setShowPercentages(boolean show) {
        if (show != showPercentages) {
            showPercentages = show;
            needToRepaint = true;
            reDrawAll();
        }
    }

    public void setPercentagesflag(boolean show) {
        showPercentages = show;
    }

    public Color getBarColor() {
        return barColor;
    }

    // barColor property
    public void setBarColor(Color c) {
        barColor = c;
        needToRepaint = true;
        repaint();
    }

    // crossColor property
    public void setCrossColor(Color c) {
        crossColor = c;
    }

    // labelColor property setTitleColor
    public void setLabelColor(Color c) {
        labelColor = c;
    }

    // titleColor property
    public void setTitleColor(Color c) {
        titleColor = c;
    }

    public void setGraphVisible(boolean b) {
        graphVisible = b;
    }

    // gridColor property
    public void setGridColor(Color c) {
        gridColor = c;
    }

    // Grapg count - a ReadOnly property
    public int getGraphCount() {
        return graphCount;
    }

    public String getTimeValue() {
        return timeValue;
    }

    // sets Time Value for both top and bottom graphs
    public void setTimeValue(long ex) {
        Date dd;
        SimpleDateFormat formatter;
        if (currentMode) {
            formatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
            dd = new Date(ex);
            timeValue = formatter.format(dd);
        } else {
            formatter = (interval == StockGraph.MONTHLY) ? new SimpleDateFormat("MMM yyyy") : new SimpleDateFormat("dd/MM/yy");    //   "S"
            dd = new Date(ex);
            timeValue = formatter.format(dd);
        }
    }

    public String getTimeLable() {
        return timeLable;
    }

    public String getPriceCloseValue() {
        return priceCloseValue;
    }

    public String getPriceVolLable() {
        return priceVolLable;
    }

    public String getCurrSymbol() {
        return currSymbol;
    }

    public Color getCurrGrfColor() {
        return currGrfColor;
    }

    public void setFontColor(Color c) {
        fontColor = c;
        needXaxisRepaint = true;
        needYaxisRepaint = true;
        repaint();
    }

    public void graphMouseDragged(MouseEvent e) {

        super.graphMouseDragged(e);

        try {
            boolean isOnLine = isOnaLine(e.getX(), e.getY(), false);
        } catch (Exception excptn) {
            excptn.printStackTrace();
        }

    }

    // overriding MouseMoved method
    public void graphMouseMoved(MouseEvent e) {

        super.graphMouseMoved(e);

        if (graphVisible && (GDMgr != null) && (GDMgr.getStore().size() > 0)) {

            ex = e.getX() - (borderWidth + widthYaxis_1 + 1);
            wy = e.getY() - (2 * titleHeight + 2 * borderWidth + 1);

            try {
                boolean isOnLine = isOnaLine(e.getX(), e.getY(), true);

                /*if (isOnLine){
                  msmvCount++;
                  updateToolTip(true,6);
                }else{
                  updateToolTip(false,6);

                  //setActiveCursor(e.getX(), e.getY());

                }*/
            } catch (Exception excptn) {
                excptn.printStackTrace();
            }
        }
    }

    public void setActiveCursor(int x, int y) {
        final boolean isOnEnd[] = new boolean[1];
        isOnEnd[0] = false;
        if (isOnEnd[0]) {
            this.setCursor(Cursor_SE_RESIZE);
        } else {
            this.setCursor(Cursor_HAND);
        }
        this.setCursor(Cursor.getDefaultCursor());
    }

    // remove dirty tool tip
    private void removeToolTip() {
        if (isTTDirty) {
            g_oToolTipListener.ToolTipDataChanged(false, (byte) 0, 0, null, null, null, null, null, null, null, null, null, null, false, "");
            isTTDirty = false;
            needToRepaint = false;
            repaint();
        }
    }

    public void updateToolTip(boolean drawMode, int dataRows, ArrayList<ToolTipDataRow> toolTipRows, boolean isMove, String aoTitle) {
        if (drawMode) {
            blobOn = true;
            byte byteMode = 1;
            if (currentMode) byteMode = 0;
            g_oToolTipListener.ToolTipDataChanged(true, byteMode, dataRows, currGrfColor, currSymbol, timeValue,
                    priceOpenValue, priceHighValue, priceLowValue, priceCloseValue, currVolume, currTurnOver, toolTipRows, isMove, aoTitle);

            if (blobChanged)
                repaint();
        } else {
            g_oToolTipListener.ToolTipDataChanged(false, (byte) 0, dataRows, SystemColor.control, null, null,
                    null, null, null, null, null, null, null, isMove, aoTitle);
            if (blobOn) {
                repaint();
            } else {
                currentYPos = 0;
                currentXPos = 0;
            }
            blobOn = false;
        }
    }

    // check whther the point (x,y) is on a Line considering colors
    private boolean isOnaLine(int ex, int ey, boolean isMove) {
        if (GDMgr != null) {
            return GDMgr.isOnaLine(ex, ey, false, isMove);
        }
        return false;
    }

    // overriding setBounds method
    public void setBounds(int x, int y, int width, int height) {

        super.setBounds(x, y, width, height);
        needToRepaint = true;
        try {
            //repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // paint methods used to draw the graph
    public void reDrawAll() {
        try {
            if (NotifyChange) {
                msmvCount = 0;
                setImageWdHt(getWidth(), getHeight());

                needToRepaint = true;
                repaint();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void clearAll() {
        if (GDMgr != null) GDMgr.resetEntireGraphStore();
        needToRepaint = true;
        repaint();
    }

    public void paintOnTheGraphicsObject(Graphics g, int width, int height, boolean isPrinting) {

        try {
            if (isPrinting) {
                Graphics2D g2 = (Graphics2D) g;
                GradientPaint redtowhite = new GradientPaint(0, 0, getGraphAreaLeftColor(), width, height - zSlider.getHeight(), getGraphAreaRightColor());
                g2.setPaint(redtowhite);

                g.fillRect(0, 0, width, height - zSlider.getHeight());

                setImageWdHt(width, height);
                needToRepaint = true;
            }

            //Paint Stock Graphs
            if (GDMgr != null) {
                if (!isPrinting) {
                    Color prevColor = g.getColor();

                    Graphics2D g2 = (Graphics2D) g;
                    GradientPaint redtowhite = new GradientPaint(0, 0, getGraphAreaLeftColor(), width, height - zSlider.getHeight(), getGraphAreaRightColor());
                    g2.setPaint(redtowhite);

                    g.fillRect(0, 0, width, height - zSlider.getHeight());
                    g.setColor(prevColor);
                }

                //to calculate title panel title height
                Graphics2D g2 = (Graphics2D) g;

                GradientPaint redtowhite = new GradientPaint(borderWidth, height - sliderHeight - borderWidth - heightXaxis, getAxisBGInnerColor(), borderWidth + mainRect.x, height - sliderHeight - borderWidth, getAxisBGOuterColor());
                g2.setPaint(redtowhite);
                g2.fillRect(borderWidth, height - sliderHeight - borderWidth - heightXaxis, mainRect.x, heightXaxis);

                GradientPaint redtowhite2 = new GradientPaint(0, height - sliderHeight - borderWidth, getAxisBGInnerColor(), mainRect.x, height - borderWidth, getAxisBGOuterColor());
                g2.setPaint(redtowhite2);
                g2.fillRect(0, height - sliderHeight - borderWidth, mainRect.x, sliderHeight);

                GDMgr.drawXGrid(g, isPrinting);
                GDMgr.drawYGrid(g, isPrinting);
                GDMgr.drawGraphsOnGraphics(g, isPrinting);
                //GDMgr.drawLegendOnGraphics(g, isPrinting, mainRect.x, 2, mainRect.width, legendHeight);
                GDMgr.drawLegendOnGraphics(g, isPrinting, 8, 0, mainRect.width, legendHeight);

                //draw Y-axes
                if ((yAxisPos == SplitPanel.AXIS_LEFT_ONLY) || (yAxisPos == SplitPanel.AXIS_BOTH_ENDS)) {
                    GDMgr.drawYaxis1OnGraphics(g, isPrinting);
                }
                if ((yAxisPos == SplitPanel.AXIS_RIGHT_ONLY) || (yAxisPos == SplitPanel.AXIS_BOTH_ENDS)) {
                    GDMgr.drawYaxis2OnGraphics(g, isPrinting);
                }
                if (yAxisPos == SplitPanel.AXIS_RIGHT_ONLY) {
                    GDMgr.drawYaxis1MarginOnGraphics(g, isPrinting);
                }

                //draw titles
                GDMgr.drawPanelTitles(g, isPrinting);

                //draw X-axis
                GDMgr.drawXaxisOnGraphics(g, isPrinting, mainRect.x, height - sliderHeight - borderWidth - heightXaxis, mainRect.width, heightXaxis);

                //added by charithn
                g.setColor(ChartTheme.getCustomThemeDataToApply().DragZoomColor);
                GDMgr.drawDragZoomArea(g);

                /********************separate cross hair implementation for axis lablels & graph-in drawing. only y
                 * values drawing should be done after y axis is drawn *******************/

                if (!isCashFlowMode()) {

                    try {
                        GDMgr.drawLastValueOfIndicator(g);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                GDMgr.drawCrossHairs(g);
                GDMgr.drawHighLowCrossHair(g);

            }
            //painting border with raised Outer line fpr printing only
            if (isPrinting) {
                try {
                    drawUniQuotesTagAndFrame(g, width, height);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                setImageWdHt(getWidth(), getHeight());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void drawUniQuotesTagAndFrame(Graphics g, int w, int h) {
        g.setClip(0, 0, w, h + sliderHeight);
        Graphics2D g2D = (Graphics2D) g;
        //frame
        g2D.setColor(new Color(200, 200, 200));
        g2D.setStroke(BS_1p8f_solid);
        g2D.drawRect(1, 1, w - 2, h - sliderHeight - 2);
        g2D.setFont(font_BOLD_9);

        String tag = Language.getString("PRINT_FOOTER_CLIENT_TITLE");
        int imgLen = g.getFontMetrics().stringWidth(Language.getString("PRINT_FOOTER_CLIENT_TITLE"));
        int tagLeft = borderWidth;//Math.max((w-tagLen)/2, borderWidth);
        g2D.setColor(Color.GRAY);
        g2D.drawString(tag, tagLeft, h + sliderHeight - 4);
        int availableW = w - (tagLeft + imgLen + 5);
        tag = ChartInterface.getFormattedPrintingTime();
        int tagLen = g.getFontMetrics().stringWidth(tag);
        if (tagLen <= availableW) {
            g2D.drawString(tag, w - tagLen, h + sliderHeight - 4);
        }
        g.setClip(null);
    }

    // overriden paint method
    public void paintGraphs(Graphics g) {

        try {
            if (GDMgr == null) return;

            if (!GDMgr.isBusyPainting()) {
                GDMgr.setBusyPainting(true);
                paintOnTheGraphicsObject(g, this.getWidth(), this.getHeight(), false);

                // state variable used to draw the drag image when mouse dragging on Y Axis
                yAxisRescaleDirty = false;
                rMarginRescaleDirty = false;

                GDMgr.setBusyPainting(false);
            }
            //g.setClip(r);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setActiveObjectIndex(int x, int y) {
        if (GDMgr != null) {
            GDMgr.setActiveObjectIndex(x, y, false);
        }
    }

    public void setActiveObjectIndexTest(int x, int y) {
        if (GDMgr != null) {
            GDMgr.setActiveObjectIndexTest(x, y, false);
        }
    }

    private String formatPriceField(String sValue) {
        float cellValue = 0;
        DecimalFormat numformat = null;


        if (!sValue.trim().equals("")) {
            cellValue = (new Float(sValue)).floatValue();
            numformat = new DecimalFormat("###,##0.00");
        }
        String num = numformat.format(cellValue);

        return num;
    }


    private String formatDecimalField(String sValue) {
        float cellValue = 0;
        DecimalFormat numformat = null;

        if (!sValue.trim().equals("")) {
            cellValue = (new Float(sValue)).floatValue();
            numformat = new DecimalFormat("###,##0");
        }

        return numformat.format(cellValue);
    }

    public void setSliderCaption() {

    }

    public void repaintZoomedGraph() {
        if (!GDMgr.isBusy()) {
            float bgnPos = zSlider.getBeginPos();
            float sZoom = zSlider.getZoom();
            GDMgr.setZoomAndPos(sZoom, bgnPos);
            needToRepaint = true;
            repaint();

            //ChartFrame.getSharedInstance().getActiveGraph().prepareTable();
            GDMgr.refreshDataWindow();
        }


    }


    public void graphMouseExited(MouseEvent e) {
        super.graphMouseExited(e);
        removeToolTip();
    }

    void sliderMouseDragged(MouseEvent e) {
        isZoomed = true;
        if (!waiting) {
            repaintZoomedGraph();
            waiting = true;
        }
    }

    void sliderMouseDragged() {
        isZoomed = true;
        repaintZoomedGraph();
    }

    void sliderMousePressed(MouseEvent e) {
        if (myTimer.isAlive()) {
            myTimer.resume();
        } else {
            myTimer.start();
        }
    }

    void sliderMouseReleased(MouseEvent e) {
        isZoomed = true;
        if (myTimer.isAlive()) {
            myTimer.suspend();
            // TODO: better to synchronize this with GDMgr.graphstore?
            repaintZoomedGraph();
        }
    }

    public void showVolumeGraph(boolean show) {
        showVolumeGrf = show;
        reDrawAll();
    }

    public void setVolumeGraphflag(boolean show) {
        if (show != showVolumeGrf) {
            showVolumeGrf = show;
            if (show) {
                if (maximized) {
                    setMaximized(false);
                }
                addNewPanel(false, volumeRect);
            } else {
                deletePanel(GDMgr.getIndexOfTheRect(panels, volumeRect));
            }
            reDrawAll();
        }
    }

    public boolean isShowingVolumeGrf() {
        return showVolumeGrf;
    }

    public void showTurnOverGraph(boolean show) {
        showTurnOverGrf = show;
        reDrawAll();
    }

    public void setTurnOverGraphFlag(boolean show) {
        if (show != showTurnOverGrf) {
            showTurnOverGrf = show;
            if (show) {
                if (maximized) {
                    setMaximized(false);
                }
                addNewPanel(false, turnOverRect);
            } else {
                deletePanel(GDMgr.getIndexOfTheRect(panels, turnOverRect));
            }
            reDrawAll();
        }
    }

    public boolean isShowingTurnOverGraph() {
        return showTurnOverGrf;
    }

    public void setInThousands(boolean bool) {
        inThousands = bool;
    }

    public boolean isPointOnPriceGraph(int x, int y) {
        if (!showVolumeGrf) return true;
        if (y > mainRect.y + mainRect.height) {
            return false;
        }
        return true;
    }

    public int getLegendType() {
        return legendType;
    }

    public void setLegendType(int lt) {
        if (legendType != lt) {
            legendType = lt;
            repaint();
        }
    }

    public void removeFocus() {
        GDMgr.setActiveObjectIndex(0, 0, true);
    }

    public void removeActiveObject(boolean isRectangularSelection) {
        if (GDMgr != null) {
            GDMgr.removeActiveObject(isRectangularSelection);
        }
    }

    public void toggleMaximize(int index) {
        super.toggleMaximize(index);
        reDrawAll();
    }

    public void deletePanel(int index) {
        if ((index < 0) || (index >= panels.size())) return;
        Rectangle r = (Rectangle) panels.get(index);
        if (r == volumeRect) {
            showVolumeGrf = false;
            if (gmListener != null) {
                gmListener.volumePanelDeleted();
            }
        } else if (r == turnOverRect) {
            showTurnOverGrf = false;
            gmListener.volumePanelDeleted(); //same can be used...
        } else {
            GDMgr.removePanel(r);
        }
        super.deletePanel(index);
        reDrawAll();
    }

    public void deleteISPPanel(int index) {

        super.deletePanel(index);
        reDrawAll();
    }

    public GraphDataManager getGDMgr() {
        return GDMgr;
    }

    public void killThread() {
        active = false;
    }

    public long getBeginDayOfWeekAdj() {
        return beginDayOfWeekAdj;
    }

    public void setBeginDayOfWeekAdj(long beginDayOfWeekAdj) {
        this.beginDayOfWeekAdj = beginDayOfWeekAdj;
    }

    public boolean isyAxisRescaleDirty() {
        return yAxisRescaleDirty;
    }

    public void setyAxisRescaleDirty(boolean yAxisRescaleDirty) {
        this.yAxisRescaleDirty = yAxisRescaleDirty;
    }

    public boolean isRightMarginRescaleDirty() {
        return rMarginRescaleDirty;
    }

    public void setRightMarginRescaleDirty(boolean rMarginRescaleDirty) {
        this.rMarginRescaleDirty = rMarginRescaleDirty;
    }

    public void refreshSemiLogStatus() {
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel wp = (WindowPanel) panels.get(i);
            if (wp == mainRect) {
                wp.setTitle(GDMgr.getMainGraphTitle());
            } else if (wp == volumeRect) {
                wp.setTitle(GDMgr.getVolumeGraphTitle());
            } else if (wp == turnOverRect) {
                wp.setTitle(GDMgr.getTurnOverGraphTitle());
            }
            wp.setSemiLogEnabled(true);
        }
        GDMgr.setSemiLogEnabledStatus();
    }


    public Font getAxisFont() {
        return axisFont;
    }

    public void setAxisFont(Font axisFont) {
        this.axisFont = axisFont;
    }


    public Font getLegendFont() {
        return legendFont;
    }

    public void setLegendFont(Font legendFont) {
        this.legendFont = legendFont;
    }

    public boolean isCashFlowMode() {
        return isCashFlowMode;
    }

    /**
     * *****************if need could move to the GDManager as well*********
     * brought from GraphFrame
     */

    public void setCashFlowMode(boolean cashFlowMode) {
        isCashFlowMode = cashFlowMode;
    }
}

