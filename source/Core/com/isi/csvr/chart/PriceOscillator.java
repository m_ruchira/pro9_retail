package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorOscillatorProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 9:24:01 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class PriceOscillator extends ChartProperties implements Indicator, Serializable {
    public static final int METHOD_PERCENT = 0;
    public static final int METHOD_POINTS = 1;
    private static final long serialVersionUID = UID_PRICE_OSCILL;
    //Fields
    private ChartProperties innerSource;
    private int shortPeriods;
    private int longPeriods;
    private byte method;
    private int calcMethod;
    private String tableColumnHeading;

    public PriceOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_PRICE_OSCILL") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_PRICE_OSCILL");
        this.method = MovingAverage.METHOD_SIMPLE;
        this.calcMethod = METHOD_PERCENT;
        this.shortPeriods = 1;
        this.longPeriods = 25;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorOscillatorProperty idp = (IndicatorOscillatorProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setShortPeriods(idp.getShortPeriod());
            this.setLongPeriods(idp.getLongPeriod());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int shortMAPeriod, int longMAPeriod, MAMethod method, byte step1, byte step2, byte srcIndex, byte destIndex) {
        byte stepShortMA = step1;
        byte stepLongerMA = step2;
        ChartRecord cr;
        int startIndex = bIndex + longMAPeriod - 1;
        if (al.size() < startIndex || shortMAPeriod == 0 || longMAPeriod == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        MovingAverage.getMovingAverage(al, bIndex, shortMAPeriod, (byte) method.ordinal(), srcIndex, stepShortMA);
        MovingAverage.getMovingAverage(al, bIndex, longMAPeriod, (byte) method.ordinal(), srcIndex, stepLongerMA);

        for (int i = 0; i < startIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = startIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, (cr.getValue(stepShortMA) - cr.getValue(stepLongerMA)) * 100 / cr.getValue(stepLongerMA));
        }
    }

    public static int getAuxStepCount() {
        return 2;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.shortPeriods = 1;
        this.longPeriods = 25;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.method = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
        this.calcMethod = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/CalcMethod"));
        this.shortPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/ShortPeriods"));
        this.longPeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/LongPeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "Method", Byte.toString(this.method));
        TemplateFactory.saveProperty(chart, document, "CalcMethod", Integer.toString(this.calcMethod));
        TemplateFactory.saveProperty(chart, document, "ShortPeriods", Integer.toString(this.shortPeriods));
        TemplateFactory.saveProperty(chart, document, "LongPeriods", Integer.toString(this.longPeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof PriceOscillator) {
            PriceOscillator po = (PriceOscillator) cp;
            this.innerSource = po.innerSource;
            this.method = po.method;
            this.calcMethod = po.calcMethod;
            this.shortPeriods = po.shortPeriods;
            this.longPeriods = po.longPeriods;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += "(" + Language.getString("PRICE_OSCILL_SHORT_TERM") + "=" + shortPeriods + ", " +
                Language.getString("PRICE_OSCILL_LONG_TERM") + "=" + longPeriods + ")";
        return Language.getString("IND_PRICE_OSCILL") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }
    //############################################

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this includes 6 intermediate steps: ReserveS1, ReserveS2, ReserveL1, ReserveL2, MAShort, MALong
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr;
        //setting step size 6
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(6);
        }
        // steps involved
        byte stepResS1 = ChartRecord.STEP_1;
        byte stepResS2 = ChartRecord.STEP_2;
        byte stepResL1 = ChartRecord.STEP_3;
        byte stepResL2 = ChartRecord.STEP_4;
        byte stepMAShort = ChartRecord.STEP_5;
        byte stepMALong = ChartRecord.STEP_6;

        MovingAverage.getVariableMovingAverage(al, 0, shortPeriods, method, GDM, stepResS1, stepResS2, getOHLCPriority(), stepMAShort);
        MovingAverage.getVariableMovingAverage(al, 0, longPeriods, method, GDM, stepResL1, stepResL2, getOHLCPriority(), stepMALong);

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(stepMALong);
            boolean isNull = (method == MovingAverage.METHOD_VOLUME_ADJUSTED) ? val == Indicator.NULL : (val == Indicator.NULL) || (i < longPeriods - 1);

            if (isNull) {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            } else {
                double currVal = cr.getStepValue(stepMAShort) - cr.getStepValue(stepMALong);
                if (calcMethod == PriceOscillator.METHOD_PERCENT) {
                    currVal = currVal * 100f / cr.getStepValue(stepMALong);
                }
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(currVal);
                }
            }
        }
        //System.out.println("**** Price Oscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_PRICE_OSCILL");
    }

    public int getCalcMethod() {
        return calcMethod;
    }

    public void setCalcMethod(int method) {
        calcMethod = method;
    }

    public byte getMethod() {
        return method;
    }

    public void setMethod(byte mthd) {
        method = mthd;
    }

    public int getLongPeriods() {
        return longPeriods;
    }

    public void setLongPeriods(int p) {
        longPeriods = p;
    }

    public int getShortPeriods() {
        return shortPeriods;
    }

    public void setShortPeriods(int p) {
        shortPeriods = p;
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
