package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 7:17:07 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

public class EaseOfMovement extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_EASE_OF_MOVEMENT;
    protected byte method;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public EaseOfMovement(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_EASE_OF_MOVEMENT") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_EASE_OF_MOVEMENT");
        this.timePeriods = 14;
        method = MovingAverage.METHOD_SIMPLE;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof EaseOfMovement) {
            EaseOfMovement eom = (EaseOfMovement) cp;
            this.timePeriods = eom.timePeriods;
            this.method = eom.method;
            this.innerSource = eom.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.method = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "Method", Byte.toString(this.method));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_EASE_OF_MOVEMENT") + " " + parent;
    }

    protected String getClose_Periods_MethodStr(boolean isIndicator) {

        return "(" + getPriorityStr(isIndicator) + ", " + timePeriods + ", "
                + getMethodString() + ")";
    }

    protected String getPriorityStr(boolean isIndicator) {
        if (isIndicator) {
            return Language.getString("INDICATOR");
        } else {
            switch (getOHLCPriority()) {
                case GraphDataManager.INNER_Open:
                    return Language.getString("OPEN");
                case GraphDataManager.INNER_High:
                    return Language.getString("HIGH");
                case GraphDataManager.INNER_Low:
                    return Language.getString("LOW");
                default:
                    return Language.getString("CLOSE");
            }
        }
    }

    protected String getMethodString() {
        switch (method) {
            case MovingAverage.METHOD_SIMPLE:
                return Language.getString("METHOD_SIMPLE");
            case MovingAverage.METHOD_WEIGHTED:
                return Language.getString("METHOD_WEIGHTED");
            case MovingAverage.METHOD_EXPONENTIAL:
                return Language.getString("METHOD_EXPONENTIAL");
            case MovingAverage.METHOD_VARIABLE:
                return Language.getString("METHOD_VARIABLE");
            case MovingAverage.METHOD_VOLUME_ADJUSTED:
                return Language.getString("METHOD_VOLUME_ADJUSTED");
            case MovingAverage.METHOD_TRANGULAR:
                return Language.getString("METHOD_TRANGULAR");
            case MovingAverage.METHOD_TIME_SERIES:
                return Language.getString("METHOD_TIME_SERIES");
            default:
                return Language.getString("METHOD_SIMPLE");
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //method
    public byte getMethod() {
        return method;
    }

    public void setMethod(byte tp) {
        method = tp;
    }

    // this has 4 intermediate steps: EOM, Reserved1 for MA, Reserved2 for MA and MovingAvg (Smoothing)
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal, MM;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;
        //setting step size 4
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(4);
        }
        // steps involved
        byte stepEOM = ChartRecord.STEP_1;
        byte stepRes1 = ChartRecord.STEP_2;
        byte stepRes2 = ChartRecord.STEP_3;
        byte stepMA = ChartRecord.STEP_4;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);
            MM = (cr.High + cr.Low - crOld.High - crOld.Low) / 2.0;
            //BR = volume/((cr.High-cr.Low)*10000f);  // this is not calculated to avoid division by 0 when H==L
            currVal = MM * (cr.High - cr.Low) * 10000.0 / cr.Volume; // MM/BR;
            cr.setStepValue(stepEOM, currVal);
        }
        MovingAverage.getVariableMovingAverage(al, 1, timePeriods, method, GDM, stepRes1, stepRes2, stepEOM, stepMA);  //Volume adjusted MA should be the ideal one
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(stepMA);
            boolean isNull = (method == MovingAverage.METHOD_VOLUME_ADJUSTED) ? val == Indicator.NULL : (val == Indicator.NULL) || (i < timePeriods);
            if (isNull) {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            } else {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(val);
            }
        }

        //System.out.println("**** Ease of Movement Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_EASE_OF_MOVEMENT");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
