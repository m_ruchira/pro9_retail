package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPriceChannelsProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 29, 2006
 * Time: 11:28:49 AM
 */
public class PriceChannel extends ChartProperties implements Indicator, FillArea {

    public final static byte PC_UPPER = 1;
    public final static byte PC_LOWER = 2;
    private static final long serialVersionUID = UID_PRICE_CHANNEL;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private byte bandType;
    private boolean fillBands;
    private Color fillColor;
    private int transparency;
    private String tableColumnHeading;

    public PriceChannel(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_PRICE_CHANNEL") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_PRICE_CHANNEL");
        timePeriods = 10;
        bandType = PC_UPPER;
        isIndicator = true;

        //fill bands related
        fillBands = true;
        fillColor = Color.blue;
        transparency = 30;

        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorPriceChannelsProperty idp = (IndicatorPriceChannelsProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());

            this.setFillBands(idp.isFillBands());
            this.setFillColor(idp.getFillColor());
            this.setTransparency(idp.getTransparency());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 10;
        this.fillBands = true;
        this.fillColor = Color.blue;
        this.transparency = 30;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof PriceChannel) {
            PriceChannel pc = (PriceChannel) cp;
            this.timePeriods = pc.timePeriods;
            this.innerSource = pc.innerSource;
            this.bandType = pc.bandType;

            this.fillBands = pc.fillBands;
            this.fillColor = pc.fillColor;
            this.transparency = pc.transparency;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.bandType = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/BandType"));

        this.fillBands = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/fillBands"));
        this.transparency = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/transparency"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/fillColor").split(",");
        this.fillColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "BandType", Byte.toString(this.bandType));

        TemplateFactory.saveProperty(chart, document, "fillBands", Boolean.toString(this.fillBands));
        TemplateFactory.saveProperty(chart, document, "transparency", Integer.toString(this.transparency));
        String strColor = this.fillColor.getRed() + "," + this.fillColor.getGreen() + "," + this.fillColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "fillColor", strColor);
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        return Language.getString("IND_PRICE_CHANNEL") + " " + parent;
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
    }

    public byte getBandType() {
        return bandType;
    }

    public void setBandType(byte bandType) {
        this.bandType = bandType;
    }


//############################################
//implementing Indicator

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currentVal;
        ChartRecord cr;

        if (al.size() < 2) return;
        long entryTime = System.currentTimeMillis();

        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = timePeriods; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);

            if (bandType == PC_UPPER) {
                currentVal = getMaxHigh(al, i, timePeriods);
            } else {
                currentVal = getMinLow(al, i, timePeriods);
            }

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(currentVal);
            }
        }

        //System.out.println("**** Price Channels Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_PRICE_CHANNEL");
    }

    private double getMinLow(ArrayList al, int fromIndex, int timePeriods) {
        if (fromIndex < timePeriods) return Indicator.NULL;
        double min = Double.MAX_VALUE;
        for (int i = fromIndex; i > (fromIndex - timePeriods); i--) {
            ChartRecord cr = (ChartRecord) al.get(i);
            min = Math.min(min, cr.Low);
        }
        return min;
    }

    private double getMaxHigh(ArrayList al, int fromIndex, int timePeriods) {
        if (fromIndex < timePeriods) return Indicator.NULL;
        double max = -100000d;
        for (int i = fromIndex; i > (fromIndex - timePeriods); i--) {
            ChartRecord cr = (ChartRecord) al.get(i);
            max = Math.max(max, cr.High);
        }
        return max;
    }

    //fill area related
    public boolean isFillBands() {
        return fillBands;
    }

    public void setFillBands(boolean fillBands) {
        this.fillBands = fillBands;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Color getFillTransparentColor() {
        return new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), transparency);
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

}
