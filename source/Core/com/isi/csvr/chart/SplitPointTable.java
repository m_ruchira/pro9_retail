package com.isi.csvr.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 11, 2005
 * Time: 11:49:13 AM
 */
public class SplitPointTable implements Serializable {
    private ArrayList<ChartSplitPoints> table;
    private ChartSplitComparator comparator = new ChartSplitComparator();

    public SplitPointTable() {
        table = new ArrayList<ChartSplitPoints>();
    }


    public void addRecord(ChartSplitPoints record) {
        int searchIndex = Collections.binarySearch(table, record, comparator);
        table.add(-searchIndex - 1, record);
    }

    public int indexOfChartSplitPoint(ChartSplitPoints record) {
        return Collections.binarySearch(table, record, comparator);
    }

    public ChartSplitPoints[] getRecords() {
        return table.toArray(new ChartSplitPoints[0]);
    }

    public void clear() {
        table.clear();
    }


    public String toString() {
        return "SplitPointTable{\n" + table + "\n}";
    }
}
