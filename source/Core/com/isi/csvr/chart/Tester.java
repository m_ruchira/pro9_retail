package com.isi.csvr.chart;

import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Aug 1, 2009
 * Time: 7:01:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class Tester extends JPanel implements MouseMotionListener, MouseListener {

    //control handle parameters
    private final int handleWidth = 10;
    private final int handleHeight = 24;
    private final int lineheight = 8;
    private final int arcWidth = 20;
    private final int archeight = 10;
    private final int barWidth = 2;
    private final int barGap = 1;
    private double minX = 1000;
    private double currentMin = minX;
    private double resultMin = minX;
    private double maxX = 8000;
    private double currentMax = maxX;
    private double resultMax = maxX;
    private int noOfBars = 20;
    private int leftHandleX = 0;
    private int leftHandleY = 0;
    private boolean leftFilled = false;
    private boolean rightFilled = false;
    private int rightHandleX = 0;
    private int rightHandleY = 0;
    //chart parameters
    private int yaxisWidth = 40;
    private int xAxisHeight = 40;
    private int legendHeight = 60;
    private double yFactor;
    private double xFactor;
    private double minY = Double.MAX_VALUE;
    private double maxY = -Double.MIN_VALUE;
    private int chartWidth;
    private int chartHeight;
    private double[] distribution;
    private TWDecimalFormat formatter = new TWDecimalFormat("###,###,###,##0.00");
    private boolean leftReadyToDrag = false;
    private boolean rightReadyToDrag = false;

    //colors
    private Color barColor = new Color(0, 0, 200, 80);
    private Color handleColor = Color.BLACK;
    private Color handleMouseOverColor = new Color(180, 0, 0, 100);
    private Color handleNormalColor = new Color(255, 255, 255, 0);
//    private Color handleNormalColor = new Color(0, 0, 110, 50);

    public Tester() {
        super();
        this.addMouseMotionListener(this);
        this.addMouseListener(this);
    }

    public static void main(String[] args) {

        //LookAndFeel lf = UIManager.getLookAndFeel();
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Tester t = new Tester();
        JFrame frame = new JFrame("Test");
        frame.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        frame.add(t);
        frame.setSize(400, 200);
        frame.setLocation(400, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setResizable(false);

        t.calculatePixelValues();
        t.repaint();
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Color.WHITE);
        //g.fillRect(yaxisWidth, legendHeight, chartWidth, chartHeight);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(Color.BLACK);
        //g.drawRect(yaxisWidth, legendHeight, chartWidth, chartHeight);
        g.setColor(barColor);
        g.drawLine(yaxisWidth, legendHeight + chartHeight, yaxisWidth + chartWidth, legendHeight + chartHeight);


        drawBars(g);

        drawHandles(g, leftHandleX, leftHandleY, leftFilled);
        drawHandles(g, rightHandleX, rightHandleY, rightFilled);

        drawLables(g);
    }

    //following 4 methods are very important. calculation related...
    public void calculatePixelValues() {

        minY = Double.MAX_VALUE;
        maxY = -Double.MIN_VALUE;

        chartWidth = Math.max(this.getWidth() - 2 * yaxisWidth, 100);
        chartHeight = Math.max(this.getHeight() - legendHeight - xAxisHeight, 50);

        xFactor = (chartWidth / (maxX - minX));

        leftHandleX = (int) getPixelForXValue(currentMin);
        leftHandleY = legendHeight + chartHeight - 2 - (handleHeight + 2 * lineheight) / 2;

        rightHandleX = (int) getPixelForXValue(currentMax);
        rightHandleY = leftHandleY;

        noOfBars = chartWidth / (barWidth + barGap);
        distribution = getCompanyDistribution(noOfBars);

        for (int i = 0; i < distribution.length; i++) {
            minY = Math.min(distribution[i], minY);
            maxY = Math.max(distribution[i], maxY);
        }
        double adj = (maxY - minY) * 0.2f;
        maxY = maxY + adj;

        yFactor = chartHeight / (maxY - minY);
    }

    private int getPixelForYValue(double value) {
        return (int) (legendHeight + chartHeight - (value - minY) * yFactor);
    }

    private double getXvalueForPixel(int pixel) {
        return (minX + (pixel - yaxisWidth) / xFactor);
    }

    private double getPixelForXValue(double value) {
        return (yaxisWidth + (value - minX) * xFactor);
    }

    //paint related
    public void drawHandles(Graphics g, int handleX, int handleY, boolean filled) {
        ((Graphics2D) g).setStroke(new BasicStroke(1f));
        g.setColor(handleColor);
        g.drawRoundRect(handleX - handleWidth / 2, handleY - handleHeight / 2, handleWidth, handleHeight, arcWidth, archeight);

        if (filled) {
            g.setColor(handleMouseOverColor);
        } else {
            g.setColor(handleNormalColor);
        }
        g.fillRoundRect(handleX - handleWidth / 2, handleY - handleHeight / 2, handleWidth, handleHeight, arcWidth, archeight);
        g.setColor(handleColor);

        ((Graphics2D) g).setStroke(new BasicStroke(1f));
        g.drawLine(handleX - 2, handleY + archeight / 2, handleX - 2, handleY - archeight / 2);
        g.drawLine(handleX + 2, handleY + archeight / 2, handleX + 2, handleY - archeight / 2);

        ((Graphics2D) g).setStroke(new BasicStroke(1.5f));
        g.drawLine(handleX, handleY + 2 + handleHeight / 2, handleX, handleY + 2 + handleHeight / 2 + lineheight);
        g.drawLine(handleX, handleY - handleHeight / 2, handleX, handleY - handleHeight / 2 - lineheight);
    }

    public void drawBars(Graphics g) {

        g.setColor(barColor);
        int x = yaxisWidth;

        try {
            for (int i = 0; i < distribution.length; i++) {
                int y = getPixelForYValue(distribution[i]);
                g.fillRect(x, y, barWidth, (chartHeight + legendHeight - y));
                x += barGap + barWidth;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("dis" + distribution);
            System.out.println("len" + distribution.length);
        }
    }

    private void drawLables(Graphics g) {

        g.setColor(new Color(255, 153, 51));
        String min = "Min X : " + formatter.format(resultMin);
        g.setFont(new Font("Tahoma", Font.BOLD, 11));
        g.drawString(min, yaxisWidth, legendHeight - 20);
        int width = g.getFontMetrics().stringWidth(min);
        g.setColor(new Color(0, 153, 102));
        String max = "Max X : " + formatter.format(resultMax);
        g.setFont(new Font("Tahoma", Font.BOLD, 11));
        g.drawString(max, yaxisWidth + width + 20, legendHeight - 20);
    }

    public void mouseDragged(MouseEvent e) {

        int x = e.getX();
        if (leftReadyToDrag && x >= yaxisWidth && x <= yaxisWidth + chartWidth) {
            currentMin = getXvalueForPixel(x);
            leftHandleX = e.getX();
            repaint();
        } else if (leftReadyToDrag && x < yaxisWidth) {
            leftHandleX = yaxisWidth;
            repaint();
        } else if (leftReadyToDrag && x > yaxisWidth + chartWidth) {
            leftHandleX = yaxisWidth + chartWidth;
            repaint();
        }

        if (rightReadyToDrag && x <= yaxisWidth + chartWidth && x >= yaxisWidth) {
            currentMax = getXvalueForPixel(x);
            rightHandleX = e.getX();
            repaint();
        } else if (rightReadyToDrag && x > yaxisWidth + chartWidth) {
            rightHandleX = yaxisWidth + chartWidth;
            repaint();
        } else if (rightReadyToDrag && x < yaxisWidth) {
            rightHandleX = yaxisWidth;
            repaint();
        }

        resultMax = Math.max(currentMin, currentMax);
        resultMin = Math.min(currentMin, currentMax);
    }

    public void mouseMoved(MouseEvent e) {

        if (isCursorOnHandle(e.getX(), e.getY(), leftHandleX, leftHandleY)) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
            leftFilled = true;
            repaint();
        } else if (isCursorOnHandle(e.getX(), e.getY(), rightHandleX, rightHandleY)) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
            rightFilled = true;
            repaint();
        } else {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            rightFilled = false;
            leftFilled = false;
            repaint();
        }
    }

    public void mousePressed(MouseEvent e) {
        if (isCursorOnHandle(e.getX(), e.getY(), leftHandleX, leftHandleY)) {
            leftReadyToDrag = true;
        } else if (isCursorOnHandle(e.getX(), e.getY(), rightHandleX, rightHandleY)) {
            rightReadyToDrag = true;
        }
    }

    public void mouseReleased(MouseEvent e) {
        leftReadyToDrag = false;
        rightReadyToDrag = false;

        resultMax = Math.max(currentMin, currentMax);
        resultMin = Math.min(currentMin, currentMax);

        //System.out.println("++++++++++ min ++++++++++ " + resultMin);
        //System.out.println("********** max ********** " + resultMax);
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseClicked(MouseEvent e) {
    }

    private boolean isCursorOnHandle(int x, int y, int handleX, int handleY) {

        RoundRectangle2D.Float rect = new RoundRectangle2D.Float(handleX - handleWidth / 2, handleY - handleHeight / 2, handleWidth, handleHeight, arcWidth, archeight);
        return rect.contains(x, y);
    }

    private double[] getCompanyDistribution(int bars) {
        Random r = new Random();
        int middle = noOfBars / 2;
        double[] result = new double[bars];
        int index = 0;
        /*for (int i = -middle; i < middle; i++) {
            if (i < 0) {
                result[index] = Math.pow(1.05, i);
            } else {
                result[index] = Math.pow(1.05, -i);
            }
            index++;
        }*/
        for (int i = 0; i < noOfBars; i++) {
            //result[i] = Math.random() * 1000;
            result[i] = r.nextGaussian() * 1000;
        }
        return result;
    }
}
