/**
 * <p>Title: focusableComboEditor</p>
 * <p>Description: ComboEditor with focus support</p>
 * <p>Copyright: Copyright (c) 2002 ISI</p>
 * <p>Company: ISI </p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

package com.isi.csvr.chart;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

public class FocusableComboEditor extends JTextField implements ComboBoxEditor {

    public FocusableComboEditor() {
    }

    public void addActionListener(ActionListener l) {
        super.addActionListener(l);
    }

    public void addKeyListener(KeyListener k) {
        //super.addKeyListener(k);
    }

    public void removeActionListener(ActionListener l) {
        super.removeActionListener(l);
    }

    public void selectAll() {
        super.selectAll();
    }

    public Object getItem() {
        return getText().trim();
    }

    public void setItem(Object anObject) {
        if (anObject != null)
            setText(anObject.toString());
        else
            setText("");
    }

    public Component getEditorComponent() {
        return this;
    }

//	public void setEnabled(boolean status){
//		super.setEnabled(status);
//		if (!status)
//		    SharedMethods.printLine("Enabled **** " + status,true);
//
//	}
}