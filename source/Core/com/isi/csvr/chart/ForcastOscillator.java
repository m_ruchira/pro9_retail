package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 4, 2006
 * Time: 2:09:40 PM
 */
public class ForcastOscillator extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_FO;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private MovingAverage signalLine;
    private Color signalColor;
    private int signalTimePeriods;
    private byte signalStyle;
    private String tableColumnHeading;

    public ForcastOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_FO") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_FO");
        this.timePeriods = 5;
        signalTimePeriods = 3;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 5;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ForcastOscillator) {
            ForcastOscillator fo = (ForcastOscillator) cp;
            this.timePeriods = fo.timePeriods;
            this.innerSource = fo.innerSource;
            this.signalTimePeriods = fo.signalTimePeriods;
            this.signalStyle = fo.signalStyle;
            this.signalColor = fo.signalColor;
            if ((this.signalLine != null) && (fo.signalLine != null)) {
                this.signalLine.assignValuesFrom(fo.signalLine);
            }
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_FO") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public MovingAverage getSignalLine() {
        return signalLine;
    }

    public void setSignalLine(MovingAverage signalLine) {
        this.signalLine = signalLine;
    }

    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color signalColor) {
        this.signalColor = signalColor;
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int signalTimePeriods) {
        this.signalTimePeriods = signalTimePeriods;
    }

    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(byte signalStyle) {
        this.signalStyle = signalStyle;
    }

    // this includes one intermediate step: Time Series Forecast
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        ChartRecord cr, crOld;
        //setting step size 1
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(1);
        }
        // steps involved
        byte stepForecast = ChartRecord.STEP_1;

        TimeSeriesForcast.getForcast(al, 0, timePeriods, GDM, getOHLCPriority(), stepForecast);

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        double val;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - 1);
            double close = cr.getValue(getOHLCPriority());
            val = (close == 0) ? 0 : (close - crOld.getStepValue(stepForecast)) * 100f / close;
            //if (Math.abs(val)>10000)
            //System.out.println(" close:"+close+" getOHLCPriority():"+getOHLCPriority()+" crOld.getStepValue(stepForecast):"+crOld.getStepValue(stepForecast)+" val:"+val);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(val);
            }
        }
        //System.out.println("**** ForcastOscillator Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_FO");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
