package com.isi.csvr.chart;

import java.awt.*;
import java.io.Serializable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class WindowPanel extends Rectangle implements Serializable {
    private static final long serialVersionUID = 1072600473330203803L;
    //fields
    private String title = "";
    private boolean inThousands = false;
    private boolean minimized = false;
    private boolean semiLog = false;  // determines whether the scale is semilog or not
    private boolean semiLogEnabled = false;   // dcetermines if scale has any negative numbers (if so, semiLog must be disabled)
    private boolean closable = true;


    public WindowPanel(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public WindowPanel(Rectangle r) {
        super(r);
    }

    public WindowPanel() {
        super();
    }

    //title
    public String getTitle() {
        return title;
    }

    public void setTitle(String s) {
        title = s;
    }

    //inThousands
    public boolean isInThousands() {
        return inThousands;
    }

    public void setInThousands(boolean b) {
        inThousands = b;
    }

    public boolean isMinimized() {
        return minimized;
    }

    //minimized
    public void setMinimized(boolean minimized) {
        this.minimized = minimized;
    }

    public boolean isSemiLog() {
        return semiLog;
    }

    public void setSemiLog(boolean semiLog) {
        this.semiLog = semiLog;
    }

    public boolean isSemiLogEnabled() {
        return semiLogEnabled;
    }

    public void setSemiLogEnabled(boolean semiLogEnabled) {
        this.semiLogEnabled = semiLogEnabled;
    }

    public String toString() {
        return getTitle();
    }

    public boolean isClosable() {
        return closable;
    }

    public void setClosable(boolean bClosable) {
        this.closable = bClosable;
    }

}