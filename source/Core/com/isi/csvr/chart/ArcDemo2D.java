package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Apr 21, 2009
 * Time: 3:27:41 PM
 * To change this template use File | Settings | File Templates.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.geom.QuadCurve2D;

public class ArcDemo2D extends JFrame {
    final static BasicStroke wideStroke = new BasicStroke(2.0f);

    public static void main(String s[]) {

        //J applet = new ArcDemo2D();
        ArcDemo2D d = new ArcDemo2D();
        d.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //f.getContentPane().add("Center", d);
        //applet.init();
        //d.pack();
        d.setSize(new Dimension(300, 300));
        d.setVisible(true);
    }

    public void init() {
        setBackground(Color.white);
        setForeground(Color.white);
    }

    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        /*  g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        */
        g2.setPaint(Color.gray);
        int x = 20;
        int y = 130;

        int width = 250;
        int height = 160;

        g.setColor(Color.RED);
        g.drawRect(x, y, width, height);
        g.setColor(Color.BLUE);
        g2.setStroke(wideStroke);
        //g.drawArc(x, y, 250, 250, 45, 270);

        //g2.draw(new Arc2D.Double(x, y, 250, 250, 0, 180, Arc2D.OPEN));

        QuadCurve2D q = new QuadCurve2D.Float();

        int ctrlx = 145, ctrly = 260;
        q.setCurve(x, y, ctrlx, ctrly, x + width, y);
        g2.draw(q);
        g.setColor(Color.BLACK);
        g.drawRect(ctrlx, ctrly, 5, 5);
        g.drawRect(x, y, 5, 5);
        g.drawRect(x + 250, y, 5, 5);

        int xx = 50, yy = 135;
        if (q.contains(xx, yy)) {
            System.out.println(" ************************* ");
        }

        if (q.intersects(xx, yy, 5, 5)) {
            System.out.println("intersect");
        }

        double[] res = new double[2];
        QuadCurve2D.solveQuadratic(new double[]{6, -5, 1}, res);

        g.setColor(Color.GREEN);
        g.drawRect(xx, yy, 5, 5);

        //PathIterator pi = q.getPathIterator() ;


    }
}


