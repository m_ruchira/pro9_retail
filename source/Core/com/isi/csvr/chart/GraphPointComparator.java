package com.isi.csvr.chart;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class GraphPointComparator implements Comparator {
    public int compare(Object o1, Object o2) {
        Object[] op1 = (Object[]) o1;
        Object[] op2 = (Object[]) o2;

        if ((op1 == null) || (op1.length < 1) || (op1[0] == null)) {
            return 1;
        }
        if ((op2 == null) || (op2.length < 1) || (op2[0] == null)) {
            return -1;
        }
        long L1 = (Long) op1[0];
        //long L2 = ((Long)op2[0]).longValue();
        long L2 = (Long) op2[0];

        if (L1 > L2) {
            return 1;
        } else if (L1 < L2) {
            return -1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}