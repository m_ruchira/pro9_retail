package com.isi.csvr.chart;

import java.awt.geom.Point2D;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Aug 21, 2008
 * Time: 6:04:14 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DragWindowListner {

    public void dataDragPointChanged(Point2D.Float e, StockGraph graph, int type);

    //public void setDataValues(String name, boolean currentMode, int type, String price, long interval, long date);


}
