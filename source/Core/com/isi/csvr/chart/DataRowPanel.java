package com.isi.csvr.chart;

import com.isi.csvr.chart.options.ChartTheme;
import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Oct 3, 2008
 * Time: 3:49:19 PM
 * To change this template use File | Settings | File Templates.
 */
class DataRowPanel extends JPanel {

    //IMPORTANT - never change this parameters
    public static final int EMPTY_LABEL_WIDTH = 8;  // this empty lable width and HGap in ToolTipDataChanged() methd(GraphFrame must be same)
    public static final int HGAP = 1;
    public static final int VGAP = 2;
    //private int rowHeight = 13;   // this rowHeight and rowHeight in ToolTipDataChanged() methd(GraphFrame must be same)
    private JLabel lblLeft;
    private JLabel lblRight;

    public DataRowPanel(JLabel lblLeft, JLabel lblRight, int width1, int width2, int rowHeight, boolean isBorderVisible) {

        String[] widths = {String.valueOf(EMPTY_LABEL_WIDTH), String.valueOf(width1), String.valueOf(width2), String.valueOf(EMPTY_LABEL_WIDTH)};
        String[] heights = {String.valueOf(rowHeight)};
        this.setLayout(new FlexGridLayout(widths, heights, HGAP, VGAP));

        this.lblLeft = lblLeft;
        this.lblRight = lblRight;
        JLabel lblEmpty1 = new JLabel("");
        JLabel lblEmpty2 = new JLabel("");

        lblLeft.setFont(ChartTheme.getCustomThemeDataToApply().ToolTipFont);
        lblRight.setFont(ChartTheme.getCustomThemeDataToApply().ToolTipFont);

        lblLeft.setForeground(ChartTheme.getCustomThemeDataToApply().TooltipForeground);
        lblRight.setForeground(ChartTheme.getCustomThemeDataToApply().TooltipForeground);

        if (!isBorderVisible) {  //TODO : need to finalize here...
            /* lblLeft.setBorder(BorderFactory.createEtchedBorder(ChartTheme.getCustomThemeDataToApply().TooltipBackground, ChartTheme.getCustomThemeDataToApply().TooltipForeground));
               lblRight.setBorder(BorderFactory.createEtchedBorder(ChartTheme.getCustomThemeDataToApply().TooltipBackground, ChartTheme.getCustomThemeDataToApply().TooltipForeground));
            */
            lblLeft.setBorder(BorderFactory.createLineBorder(ChartTheme.getCustomThemeDataToApply().TooltipBorderColor));
            lblRight.setBorder(BorderFactory.createLineBorder(ChartTheme.getCustomThemeDataToApply().TooltipBorderColor));

        }

        this.add(lblEmpty1);
        this.add(lblLeft);
        this.add(lblRight);
        this.add(lblEmpty2);

        this.setBackground(ChartTheme.getCustomThemeDataToApply().TooltipBackground);
        this.setPreferredSize(new Dimension(2 * EMPTY_LABEL_WIDTH + width1 + width2 + 4 * HGAP, rowHeight + VGAP));

        GUISettings.applyOrientation(this);
    }

    public JLabel getLblLeft() {
        return lblLeft;
    }

    public void setLblLeft(JLabel lblLeft) {
        this.lblLeft = lblLeft;
    }

    public JLabel getLblRight() {
        return lblRight;
    }

    public void setLblRight(JLabel lblRight) {
        this.lblRight = lblRight;
    }
}