package com.isi.csvr.chart;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * User: udaka
 * Date: Feb 9, 2007
 * Time: 8:56:07 PM
 */
public abstract class IndicatorBase extends ChartProperties implements Indicator, Serializable {

    //#################################################### to be used by custom indicators
    //Data arrays
    public final static byte INDICATOR = 0;
    public final static byte INNER_Open = 0;
    public final static byte INNER_High = 1;
    public final static byte INNER_Low = 2;
    public final static byte INNER_Close = 3;
    public final static byte INNER_Volume = 4;
    //Calc Methods
    public final static int CALC_METHOD_PERCENT = 0;
    public final static int CALC_METHOD_POINTS = 1;
    //MA Calc Methods
    public final static byte MA_METHOD_SIMPLE = 0;
    public final static byte MA_METHOD_WEIGHTED = 1;
    public final static byte MA_METHOD_EXPONENTIAL = 2;
    public final static byte MA_METHOD_VARIABLE = 3;
    public final static byte MA_METHOD_TIME_SERIES = 4;
    public final static byte MA_METHOD_TRANGULAR = 5;
    public final static byte MA_METHOD_VOLUME_ADJUSTED = 6;
    //Cust Ind ID
    public final static byte ID_CUSTOM_INDICATOR = 0;
    //####################################################
    //SymbolID - to be used for strategies
    protected int symbolID = -1;

    //protected String groupID = "";
    //Position - to be used for strategies
    protected int position = 1;
    //IsStrategy - to be used for strategies
    protected boolean isStrategy = false;
    //DrawSymbol - to be used for strategies
    protected boolean drawSymbol = false;
    //TargetCP - to be used for strategies
    protected ChartProperties targetCP;
    //hasItsOwnScale
    protected boolean itsOwnScale;
    //innerSource
    protected ChartProperties innerSource;
    //SignalParent
    protected IndicatorBase signalParent;
    //UniqueID
    protected String uniqueID;
    //PlotID
    protected int plotID;
    private String tableColumnHeading;
    //PlotCount
    private int plotCount;
    private String shortName;

    public IndicatorBase(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, "IndicatorBase" + Indicator.FD + symbl, anID, c, r);
        //innerSource = cp;
        itsOwnScale = true;
        //t_im_ePeriods = 14;
        uniqueID = this.getShortName() + System.currentTimeMillis();
        plotCount = 1;
        plotID = 0;
    }

    public IndicatorBase(String name, String sname, ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        this(data, "IndicatorBase" + Indicator.FD + symbl, anID, c, r);
        longName = name;
        shortName = sname;
    }

    public static double truncate(double value) {
        return (value > 0) ? Math.floor(value) : Math.ceil(value);
    }

    public static double frac(double value) {
        return value - truncate(value);
    }

    public static double precision(double value, double prec) {
        prec = (double) Math.max(Math.round(prec), 0);
        double factor = Math.pow(10, prec);
        return (double) Math.round(value * factor) / factor;
    }

    public static void calculateHSince(ArrayList al, byte dataIndex, byte condIndex, byte destIndex, int n) {
        Hashtable htCond = new Hashtable();
        int preCond = 0;
        ChartRecord cR, cRLoop;
        for (int i = 0; i < al.size(); i++) {
            cR = (ChartRecord) al.get(i);
            int cond = (int) cR.getStepValue(condIndex);
            if (cond > preCond) {
                preCond = cond;
                htCond.put(cond, i);
            }
            if ((cond >= n) && (cR.getStepValue(dataIndex) != Indicator.NULL) && (!Double.isNaN(cR.getStepValue(dataIndex)))) {
                int start = (Integer) htCond.get(cond - n + 1);
                double max = -Double.MAX_VALUE;
                for (int j = start; j <= i; j++) {
                    cRLoop = (ChartRecord) al.get(j);
                    double c_u_r = cRLoop.getValue(dataIndex);
                    if ((c_u_r != Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r) &&
                            (c_u_r != Double.NEGATIVE_INFINITY) && (c_u_r != Double.POSITIVE_INFINITY)) {
                        max = Math.max(max, c_u_r);
                    }
                }
                cR.setStepValue(destIndex, (max == -Double.MAX_VALUE ? Indicator.NULL : max));
            } else {
                cR.setStepValue(destIndex, Indicator.NULL);
            }
        }
    }

    public static void calculateHSinceBars(ArrayList al, byte dataIndex, byte condIndex, byte destIndex, int n) {
        Hashtable htCond = new Hashtable();
        int preCond = 0;
        ChartRecord cR, cRLoop;
        for (int i = 0; i < al.size(); i++) {
            cR = (ChartRecord) al.get(i);
            int cond = (int) cR.getStepValue(condIndex);
            if (cond > preCond) {
                preCond = cond;
                htCond.put(cond, i);
            }
            if ((cond >= n) && (cR.getStepValue(dataIndex) != Indicator.NULL) && (!Double.isNaN(cR.getStepValue(dataIndex)))) {
                int start = (Integer) htCond.get(cond - n + 1);
                double max = -Double.MAX_VALUE;
                int bars = 0;
                for (int j = start; j <= i; j++) {
                    cRLoop = (ChartRecord) al.get(j);
                    double c_u_r = cRLoop.getValue(dataIndex);
                    if ((c_u_r != Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r) &&
                            (c_u_r != Double.NEGATIVE_INFINITY) && (c_u_r != Double.POSITIVE_INFINITY)) {
                        if (c_u_r > max) bars = i - j;
                        max = Math.max(max, c_u_r);
                    }
                }
                cR.setStepValue(destIndex, (max == -Double.MAX_VALUE ? Indicator.NULL : bars));
            } else {
                cR.setStepValue(destIndex, Indicator.NULL);
            }
        }
    }

    public static void calculateLSince(ArrayList al, byte dataIndex, byte condIndex, byte destIndex, int n) {
        Hashtable htCond = new Hashtable();
        int preCond = 0;
        ChartRecord cR, cRLoop;
        for (int i = 0; i < al.size(); i++) {
            cR = (ChartRecord) al.get(i);
            int cond = (int) cR.getStepValue(condIndex);
            if (cond > preCond) {
                preCond = cond;
                htCond.put(cond, i);
            }
            if ((cond >= n) && (cR.getStepValue(dataIndex) != Indicator.NULL) && (!Double.isNaN(cR.getStepValue(dataIndex)))) {
                int start = (Integer) htCond.get(cond - n + 1);
                double min = Double.MAX_VALUE;
                for (int j = start; j <= i; j++) {
                    cRLoop = (ChartRecord) al.get(j);
                    double c_u_r = cRLoop.getValue(dataIndex);
                    if ((c_u_r != Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r) &&
                            (c_u_r != Double.NEGATIVE_INFINITY) && (c_u_r != Double.POSITIVE_INFINITY)) {
                        min = Math.min(min, c_u_r);
                    }
                }
                cR.setStepValue(destIndex, (min == Double.MAX_VALUE ? Indicator.NULL : min));
            } else {
                cR.setStepValue(destIndex, Indicator.NULL);
            }
        }
    }

    public static void calculateLSinceBars(ArrayList al, byte dataIndex, byte condIndex, byte destIndex, int n) {
        Hashtable htCond = new Hashtable();
        int preCond = 0;
        ChartRecord cR, cRLoop;
        for (int i = 0; i < al.size(); i++) {
            cR = (ChartRecord) al.get(i);
            int cond = (int) cR.getStepValue(condIndex);
            if (cond > preCond) {
                preCond = cond;
                htCond.put(cond, i);
            }
            if ((cond >= n) && (cR.getStepValue(dataIndex) != Indicator.NULL) && (!Double.isNaN(cR.getStepValue(dataIndex)))) {
                int start = (Integer) htCond.get(cond - n + 1);
                double min = Double.MAX_VALUE;
                int bars = 0;
                for (int j = start; j <= i; j++) {
                    cRLoop = (ChartRecord) al.get(j);
                    double c_u_r = cRLoop.getValue(dataIndex);
                    if ((c_u_r != Indicator.NULL) && !Double.isNaN(c_u_r) && !Double.isInfinite(c_u_r) &&
                            (c_u_r != Double.NEGATIVE_INFINITY) && (c_u_r != Double.POSITIVE_INFINITY)) {
                        if (c_u_r < min) bars = i - j;
                        min = Math.min(min, c_u_r);
                    }
                }
                cR.setStepValue(destIndex, (min == Double.MAX_VALUE ? Indicator.NULL : bars));
            } else {
                cR.setStepValue(destIndex, Indicator.NULL);
            }
        }
    }

    public static String[] getSymbolArray() {
        String str = Language.getString("STRATEGY_SYMBOL_ARRAY");
        return str.split(",");
    }

    public static String[] getSymbolPositionArray() {
        String strPOS = Language.getString("STRATEGY_SYMBOL_POS_ARRAY");
        return strPOS.split(",");
    }

    public static String[] getLabelPositionArray() {
        String strPOS = Language.getString("STRATEGY_LABEL_POS_ARRAY");
        return strPOS.split(",");
    }

    public int getSymbolID() {
        return symbolID;
    }

    public void setSymbolID(int value) {
        symbolID = value;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int value) {
        position = value;
    }

    public boolean IsStrategy() {
        return isStrategy;
    }

    public void setStrategy(boolean value) {
        isStrategy = value;
    }

    public boolean isDrawSymbol() {
        return drawSymbol;
    }

    public ChartProperties getTargetCP() {
        return targetCP;
    }

    public void setTargetCP(ChartProperties value) {
        targetCP = value;
    }

    public boolean isPattern() {
        return isStrategy && drawSymbol && (symbolID >= 1000);
    }

    public String getTableColumnHeading() {
        if (plotID != 0) {
            return shortName + "(" + plotID + ")";
        } else {
            return shortName;
        }
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
//    private String longName;

    public boolean hasItsOwnScale() {
        return itsOwnScale;
    }

    public void setItsOwnScale(boolean value) {
        itsOwnScale = value;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public IndicatorBase getSignalParent() {
        return signalParent;
    }

    public void setSignalParent(IndicatorBase value) {
        signalParent = value;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String value) {
        uniqueID = value;
    }

    public int getPlotID() {
        return plotID;
    }

    public void setPlotID(int value) {
        plotID = value;
    }

    public int getPlotCount() {
        return plotCount;
    }

    public void setPlotCount(int value) {
        plotCount = value;
    }

    public void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.uniqueID = TemplateFactory.loadProperty(xpath, document, preExpression + "/UniqueID");
        this.plotID = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/PlotID"));
        String tmpResult;
        tmpResult = TemplateFactory.loadProperty(xpath, document, preExpression + "/SymbolID");
        if (!tmpResult.equals("")) this.symbolID = Integer.parseInt(tmpResult);
        tmpResult = TemplateFactory.loadProperty(xpath, document, preExpression + "/Position");
        if (!tmpResult.equals("")) this.position = Integer.parseInt(tmpResult);
        tmpResult = TemplateFactory.loadProperty(xpath, document, preExpression + "/IsStrategy");
        if (!tmpResult.equals("")) this.isStrategy = Boolean.parseBoolean(tmpResult);
        if (this.symbolID > -1) {
            this.drawSymbol = true;
        }
        try {
            this.groupID = TemplateFactory.loadProperty(xpath, document, preExpression + "/GroupID");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /* Common Methods*/

    public void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "UniqueID", this.uniqueID);
        TemplateFactory.saveProperty(chart, document, "PlotID", Integer.toString(this.plotID));
        TemplateFactory.saveProperty(chart, document, "SymbolID", Integer.toString(this.symbolID));
        TemplateFactory.saveProperty(chart, document, "Position", Integer.toString(this.position));
        TemplateFactory.saveProperty(chart, document, "IsStrategy", Boolean.toString(this.isStrategy));
        try {
            TemplateFactory.saveProperty(chart, document, "GroupID", this.groupID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof IndicatorBase) {
            IndicatorBase ib = (IndicatorBase) cp;
            //this.t_im_ePeriods = ib.t_im_ePeriods;
            this.longName = ib.longName;
            this.itsOwnScale = ib.itsOwnScale;
            this.innerSource = ib.innerSource;
            this.signalParent = ib.signalParent;
            this.targetCP = ib.targetCP;
            //this.uniqueID = ib.uniqueID;
            this.plotID = ib.plotID;
            this.symbolID = ib.symbolID;
            this.position = ib.position;
            this.isStrategy = ib.isStrategy;
            this.drawSymbol = ib.drawSymbol;
        }
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public String toString() {
        String parent;
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(innerSource.getShortName()));
        if (ex != null) {
            String displaySymbolName = innerSource.getShortName().replaceFirst(SharedMethods.getExchangeFromKey(innerSource.getShortName()), ex.getDisplayExchange());
            innerSource.setDisplaySymbol(displaySymbolName);
        }

        if (innerSource instanceof Indicator) {
            parent = " (" + innerSource.getShortName() + ") "; //Language.getString("INDICATOR")
        } else {
            //parent = " (" + innerSource.getSymbol() + ") ";
            parent = " (" + innerSource.getDisplaySymbol() + ") ";
        }
        return getLongName() + parent;
    }

    public String getShortName() {
        return longName;
    }

    public String getLongName() {
        return longName;
    }

    public String getGroupID() {
        return groupID;
    }

    public void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    public abstract void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index);

    public void updateSignalParams(IndicatorBase indicator) {
    }
}
