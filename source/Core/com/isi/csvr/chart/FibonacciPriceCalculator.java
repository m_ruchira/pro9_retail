/*
package com.isi.csvr.chart;

import com.isi.util.FlexGridLayout;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.StringSelection;
import java.awt.print.Printable;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

*/
/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Sep 12, 2008
 * Time: 3:28:59 PM
 * To change this template use File | Settings | File Templates.
 */
/*
public class FibonacciPriceCalculator extends JDialog implements ActionListener, Themeable, Printable, WindowListener, ClipboardOwner {

    public static final double[] PERCENTAGE_ARRAY = new double[]{.333, .382, .4, .5, .6, .618, .666};
    private double[] points = new double[3];   //here the postion of the array is very importatnt

    private int WIDTH = 270;
    private int HEIGHT = 280;

    private JLabel lblPrice1Str;
    private JLabel lblPrice2Str;
    private JLabel lblPrice3Str;

    private JLabel lblRetracStr;
    private JLabel lbl1Str;
    private JLabel lbl2Str;
    private JLabel lbl3Str;
    private JLabel lbl4Str;
    private JLabel lbl5Str;
    private JLabel lbl6Str;
    private JLabel lbl7Str;

    private JLabel lblPrice1;
    private JLabel lblPrice2;
    private JLabel lblPrice3;
    private JLabel lblRetrc;

    private JLabel lbl1;  //33.3%
    private JLabel lbl2;  //38.2%
    private JLabel lbl3;  //40.0%
    private JLabel lbl4;  //50.0%
    private JLabel lbl5;  //60.0%
    private JLabel lbl6;  //61.8%
    private JLabel lbl7;  //66.6%

    private TWButton btnHelp;
    private TWButton btnOk;
    private TWButton btnPrint;
    private TWButton btnCopy;

    private TWDecimalFormat formatter = new TWDecimalFormat("###,###.00");
    ArrayList<JLabel> labels = new ArrayList<JLabel>();

    private StockGraph graph = null;

    private static FibonacciPriceCalculator self = null;

    private Clipboard clip = null;

    public FibonacciPriceCalculator() {

        this.points = new double[]{10, 20, 30}; //assgin to default values

        createUI();
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setLocationRelativeTo(ChartFrame.getSharedInstance());
        this.setModal(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        addActionListeners();

        clip = this.getToolkit().getSystemClipboard();
    }

    public void setGraph(StockGraph g) {
        this.graph = g;
    }

    public static FibonacciPriceCalculator getSharedInstance() {
        if (self == null) {
            self = new FibonacciPriceCalculator();
        }
        return self;
    }

    private void addActionListeners() {
        btnHelp.addActionListener(this);
        btnOk.addActionListener(this);
        btnPrint.addActionListener(this);
        btnCopy.addActionListener(this);
        this.addWindowListener(this);
    }

    public void createUI() {

        lblPrice1Str = new JLabel(" Price 1    =  ", JLabel.LEADING);
        lblPrice2Str = new JLabel(" Price 2    =  ", JLabel.LEADING);
        lblPrice3Str = new JLabel(" Price 3    =  ", JLabel.LEADING);

        lblPrice1 = new JLabel("");
        lblPrice2 = new JLabel("");
        lblPrice3 = new JLabel("");

        lblRetracStr = new JLabel(" % Retr    =  ", JLabel.LEADING);
        lblRetrc = new JLabel("76.83");
        lbl1Str = new JLabel(" 33.3 %    =", JLabel.LEADING);
        lbl1 = new JLabel("");
        lbl2Str = new JLabel(" 38.2 %    =", JLabel.LEADING);
        lbl2 = new JLabel("");
        lbl3Str = new JLabel(" 40.0 %    =", JLabel.LEADING);
        lbl3 = new JLabel("");
        lbl4Str = new JLabel(" 50.0 %    =", JLabel.LEADING);
        lbl4 = new JLabel("");
        lbl5Str = new JLabel(" 60.0 %    =", JLabel.LEADING);
        lbl5 = new JLabel("");
        lbl6Str = new JLabel(" 61.8 %    =", JLabel.LEADING);
        lbl6 = new JLabel("");
        lbl7Str = new JLabel(" 66.6 %    =", JLabel.LEADING);
        lbl7 = new JLabel("");

        labels.add(lblPrice1Str);
        labels.add(lblPrice1);
        labels.add(lblPrice2Str);
        labels.add(lblPrice2);
        labels.add(lblPrice3Str);
        labels.add(lblPrice3);

        labels.add(lblRetracStr);
        labels.add(lblRetrc);
        labels.add(lbl1Str);
        labels.add(lbl1);
        labels.add(lbl2Str);
        labels.add(lbl2);
        labels.add(lbl3Str);
        labels.add(lbl3);
        labels.add(lbl4Str);
        labels.add(lbl4);
        labels.add(lbl5Str);
        labels.add(lbl5);
        labels.add(lbl6Str);
        labels.add(lbl6);
        labels.add(lbl7Str);
        labels.add(lbl7);

        FlexGridLayout layoutAll = new FlexGridLayout(new String[]{"70%", "30%"}, new String[]{"100%"}, 5, 0);
        JPanel pnlAll = new JPanel();
        pnlAll.setLayout(layoutAll);
        pnlAll.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        String[] widths = new String[]{"70", "60"};
        String[] heights = new String[]{"20", "20", "20", "5", "20", "20", "20", "20", "20", "20", "20", "20"};
        JPanel pnlLeft = new JPanel(new FlexGridLayout(widths, heights, 2, 0));

        pnlLeft.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        FlexGridLayout layoutRight = new FlexGridLayout(new String[]{"100%"}, new String[]{"22", "22", "22", "22"}, 3, 5);
        JPanel pnlRight = new JPanel(layoutRight);

        btnOk = new TWButton(Language.getString("OK"));
        btnHelp = new TWButton(Language.getString("HELP"));
        btnPrint = new TWButton(Language.getString("PRINT"));
        btnCopy = new TWButton(Language.getString("COPY"));

        pnlRight.add((btnOk));
        pnlRight.add((btnHelp));
        pnlRight.add((btnPrint));
        pnlRight.add((btnCopy));

        pnlLeft.add(lblPrice1Str);
        pnlLeft.add(lblPrice1);
        pnlLeft.add(lblPrice2Str);
        pnlLeft.add(lblPrice2);
        pnlLeft.add(lblPrice3Str);
        pnlLeft.add(lblPrice3);
        pnlLeft.add(new JSeparator(JSeparator.HORIZONTAL));
        pnlLeft.add(new JSeparator(JSeparator.HORIZONTAL));
        pnlLeft.add((lblRetracStr));
        pnlLeft.add((lblRetrc));

        pnlLeft.add(lbl1Str);
        pnlLeft.add(lbl1);
        pnlLeft.add(lbl2Str);
        pnlLeft.add(lbl2);
        pnlLeft.add(lbl3Str);
        pnlLeft.add(lbl3);
        pnlLeft.add(lbl4Str);
        pnlLeft.add(lbl4);
        pnlLeft.add(lbl5Str);
        pnlLeft.add(lbl5);
        pnlLeft.add(lbl6Str);
        pnlLeft.add(lbl6);
        pnlLeft.add(lbl7Str);
        pnlLeft.add(lbl7);

        pnlAll.add(pnlLeft);
        pnlAll.add(pnlRight);

        this.add(pnlAll);
        this.setResizable(false);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle(Language.getString("GRAPH_FIB_CALCULATOR"));
        GUISettings.applyOrientation(this);
    }

    public double[] getPoints() {
        return points;
    }

    public void setPoints(double[] points) {
        this.points = points;
    }

    public void showReults(double[] points) {
        try {
            double[] resultArray = new double[PERCENTAGE_ARRAY.length];

            double point1 = points[0];
            double point2 = points[1];
            double point3 = points[2];
            double wave = point2 - point1;

            if (wave > 0) {
                for (int i = 0; i < PERCENTAGE_ARRAY.length; i++) {
                    resultArray[i] = point3 + (wave * PERCENTAGE_ARRAY[i]);
                }
            } else {
                for (int i = 0; i < PERCENTAGE_ARRAY.length; i++) {
                    resultArray[i] = point3 - (wave * PERCENTAGE_ARRAY[i]);
                }
            }

            this.lblPrice1.setText(formatter.format(point1));
            this.lblPrice2.setText(formatter.format(point2));
            this.lblPrice3.setText(formatter.format(point3));

            this.lbl1.setText(formatter.format(resultArray[0]));
            this.lbl2.setText(formatter.format(resultArray[1]));
            this.lbl3.setText(formatter.format(resultArray[2]));
            this.lbl4.setText(formatter.format(resultArray[3]));
            this.lbl5.setText(formatter.format(resultArray[4]));
            this.lbl6.setText(formatter.format(resultArray[5]));
            this.lbl7.setText(formatter.format(resultArray[6]));

            */
/* clearing the arralylist and fill with jlables for priting puropses*/
/*
            labels.clear();
            labels.add(lblPrice1Str);
            labels.add(lblPrice1);
            labels.add(lblPrice2Str);
            labels.add(lblPrice2);
            labels.add(lblPrice3Str);
            labels.add(lblPrice3);

            labels.add(lblRetracStr);
            labels.add(lblRetrc);
            labels.add(lbl1Str);
            labels.add(lbl1);
            labels.add(lbl2Str);
            labels.add(lbl2);
            labels.add(lbl3Str);
            labels.add(lbl3);
            labels.add(lbl4Str);
            labels.add(lbl4);
            labels.add(lbl5Str);
            labels.add(lbl5);
            labels.add(lbl6Str);
            labels.add(lbl6);
            labels.add(lbl7Str);
            labels.add(lbl7);

            applyTheme();
            this.getContentPane().repaint();
            this.setVisible(true);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(btnOk)) {
            this.setVisible(false);
            graph.GDMgr.fibCalBounds = null;
            graph.repaint();
        } else if (e.getSource().equals(btnHelp)) {

        } else if (e.getSource().equals(btnPrint)) {
            printPage();
        }else if(e.getSource().equals(btnCopy)) {
            copyToClipboard();
        }
    }

    public void printPage() {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setPrintable(this);

        if (printJob.printDialog()) {
            try {
                printJob.print();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void applyTheme() {
        this.getContentPane().setBackground(Theme.getColor("BACKGROUND"));
        //this.setBackground(Theme.getColor("BACKGROUND"));
    }

    public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {

        if (pageIndex >= 1) {
            return Printable.NO_SUCH_PAGE;
        }

        printDocument((Graphics2D) g);
        return Printable.PAGE_EXISTS;
    }

    public void printDocument(Graphics2D g2) {

        g2.setColor(Color.BLACK);

        g2.setFont(new Font("Times new roman", Font.BOLD, 14));
        g2.drawString("Fibonacci price retracement calculator", 160, 50);

        g2.setFont(new Font("Times new roman", Font.PLAIN, 14));

        int x = 70;
        int y = 80;
        int width = 75;
        int height = 20;

        for (JLabel label : labels) {
            g2.drawString(label.getText(), x, y);

            if (label.getText().contains("=")) {
                if (label.getText().contains("Price 3")) {
                    g2.drawLine(x, y + 5, x + 140, y + 5);

                }
                x = x + width;
            } else {
                y = y + height;
                x = x - width;
            }
        }
    }

    public void copyToClipboard() {
        String data = lbl2.getText().replaceAll("\n", "<br>");
        StringSelection text = new StringSelection(data);
        clip.setContents(text, this);
        data = null;
    }

    public void windowOpened(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.        
    }

    public void windowClosing(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        //graph.GDMgr.fibCalBounds = null;
        //graph.repaint();
    }

    public void windowClosed(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        graph.GDMgr.fibCalBounds = null;
        graph.repaint();
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

*/
package com.isi.csvr.chart;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Sep 12, 2008
 * Time: 3:28:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class FibonacciPriceCalculator extends JDialog implements ActionListener, Themeable, Printable, WindowListener, ClipboardOwner {

    public static final double[] PERCENTAGE_ARRAY = new double[]{.333, .382, .4, .5, .6, .616, .618};
    private static FibonacciPriceCalculator self = null;
    ArrayList<JLabel> labels = new ArrayList<JLabel>();
    private double[] points = new double[3];   //here the postion of the array is very importatnt
    private int WIDTH = 270;
    private int HEIGHT = 310;
    private JPanel pnlLeft = null;
    private JLabel lblPrice1Str;
    private JLabel lblPrice2Str;
    private JLabel lblPrice3Str;
    private JLabel lblRetracStr;
    private JLabel lbl1Str;
    private JLabel lbl2Str;
    private JLabel lbl3Str;
    private JLabel lbl4Str;
    private JLabel lbl5Str;
    private JLabel lbl6Str;
    private JLabel lbl7Str;
    private JLabel lblPrice1;
    private JLabel lblPrice2;
    private JLabel lblPrice3;
    private JLabel lblRetrc;
    private JLabel lbl1;  //33.3%
    private JLabel lbl2;  //38.2%
    private JLabel lbl3;  //40.0%
    private JLabel lbl4;  //50.0%
    private JLabel lbl5;  //60.0%
    private JLabel lbl6;  //61.8%
    private JLabel lbl7;  //66.6%
    private TWButton btnHelp;
    private TWButton btnOk;
    private TWButton btnPrint;
    private TWButton btnCopy;
    private TWDecimalFormat formatter = new TWDecimalFormat("###,###.00");
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("###,##0.00");
    private StockGraph graph = null;
    private Clipboard clip = null;

    public FibonacciPriceCalculator(JFrame owner) {

        super(owner);
        this.points = new double[]{10, 20, 30}; //assgin to default values

        createUI();
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setLocationRelativeTo(ChartFrame.getSharedInstance());
        this.setModal(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addActionListeners();

        clip = this.getToolkit().getSystemClipboard();
    }

    public static FibonacciPriceCalculator getSharedInstance() {
        if (self == null) {
            //self = new FibonacciPriceCalculator();
        }
        return self;
    }

    public void setGraph(StockGraph g) {
        this.graph = g;
    }

    private void addActionListeners() {
        btnHelp.addActionListener(this);
        btnOk.addActionListener(this);
        btnPrint.addActionListener(this);
        btnCopy.addActionListener(this);
        this.addWindowListener(this);
    }

    public void createUI() {

        lblPrice1Str = new JLabel(" " + Language.getString("PRICE_1"), JLabel.LEADING);
        lblPrice2Str = new JLabel(" " + Language.getString("PRICE_2"), JLabel.LEADING);
        lblPrice3Str = new JLabel(" " + Language.getString("PRICE_3"), JLabel.LEADING);

        lblPrice1 = new JLabel("");
        lblPrice2 = new JLabel("");
        lblPrice3 = new JLabel("");

        lblRetracStr = new JLabel(" " + Language.getString("PER_RETR"), JLabel.LEADING);
        lblRetrc = new JLabel(" 76.83");
        lbl1Str = new JLabel(" 33.3 % ", JLabel.LEADING);
        lbl1 = new JLabel("");
        lbl2Str = new JLabel(" 38.2 % ", JLabel.LEADING);
        lbl2 = new JLabel("");
        lbl3Str = new JLabel(" 40.0 % ", JLabel.LEADING);
        lbl3 = new JLabel("");
        lbl4Str = new JLabel(" 50.0 % ", JLabel.LEADING);
        lbl4 = new JLabel("");
        lbl5Str = new JLabel(" 60.0 % ", JLabel.LEADING);
        lbl5 = new JLabel("");
        lbl6Str = new JLabel(" 61.8 % ", JLabel.LEADING);
        lbl6 = new JLabel("");
        lbl7Str = new JLabel(" 66.6 % ", JLabel.LEADING);
        lbl7 = new JLabel("");

        labels.add(lblPrice1Str);
        labels.add(lblPrice1);
        labels.add(lblPrice2Str);
        labels.add(lblPrice2);
        labels.add(lblPrice3Str);
        labels.add(lblPrice3);

        labels.add(lblRetracStr);
        labels.add(lblRetrc);
        labels.add(lbl1Str);
        labels.add(lbl1);
        labels.add(lbl2Str);
        labels.add(lbl2);
        labels.add(lbl3Str);
        labels.add(lbl3);
        labels.add(lbl4Str);
        labels.add(lbl4);
        labels.add(lbl5Str);
        labels.add(lbl5);
        labels.add(lbl6Str);
        labels.add(lbl6);
        labels.add(lbl7Str);
        labels.add(lbl7);

        FlexGridLayout layoutAll = new FlexGridLayout(new String[]{"70%", "30%"}, new String[]{"100%"}, 5, 0);
        JPanel pnlAll = new JPanel();
        pnlAll.setLayout(layoutAll);
        pnlAll.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        String[] widths = new String[]{"65", "90"};
        String[] heights = new String[]{"20", "20", "20", "20", "20", "20", "20", "20", "20", "20", "20", "20"};
        pnlLeft = new JPanel(new FlexGridLayout(widths, heights, 2, 1));

        //pnlLeft.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        //FlexGridLayout layoutRight = new FlexGridLayout(new String[]{"100%"}, new String[]{"22", "22", "22", "22"}, 3, 5);
        FlexGridLayout layoutRight = new FlexGridLayout(new String[]{"100%"}, new String[]{"22", "22", "22"}, 3, 5);
        JPanel pnlRight = new JPanel(layoutRight);

        btnOk = new TWButton(Language.getString("OK"));
        btnHelp = new TWButton(Language.getString("HELP"));
        btnPrint = new TWButton(Language.getString("PRINT"));
        btnCopy = new TWButton(Language.getString("COPY"));

        pnlRight.add((btnOk));
        //pnlRight.add((btnHelp));
        pnlRight.add((btnPrint));
        pnlRight.add((btnCopy));

        setBorders();

        pnlLeft.add(lblPrice1Str);
        pnlLeft.add(lblPrice1);
        pnlLeft.add(lblPrice2Str);
        pnlLeft.add(lblPrice2);
        pnlLeft.add(lblPrice3Str);
        pnlLeft.add(lblPrice3);
        pnlLeft.add(new JLabel(""));
        pnlLeft.add(new JLabel(""));
        pnlLeft.add((lblRetracStr));
        pnlLeft.add((lblRetrc));

        pnlLeft.add(lbl1Str);
        pnlLeft.add(lbl1);
        pnlLeft.add(lbl2Str);
        pnlLeft.add(lbl2);
        pnlLeft.add(lbl3Str);
        pnlLeft.add(lbl3);
        pnlLeft.add(lbl4Str);
        pnlLeft.add(lbl4);
        pnlLeft.add(lbl5Str);
        pnlLeft.add(lbl5);
        pnlLeft.add(lbl6Str);
        pnlLeft.add(lbl6);
        pnlLeft.add(lbl7Str);
        pnlLeft.add(lbl7);

        pnlAll.add(pnlLeft);
        pnlAll.add(pnlRight);

        this.add(pnlAll);
        this.setResizable(false);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle(Language.getString("GRAPH_FIB_CALCULATOR"));
        GUISettings.applyOrientation(this);
    }

    private void setBorders() {

        Color c = Color.BLACK;
        Border border = BorderFactory.createLineBorder(c);

        lblPrice1Str.setBorder(border);
        lblPrice1.setBorder(border);

        lblPrice2Str.setBorder(border);
        lblPrice2.setBorder(border);

        lblPrice3Str.setBorder(border);
        lblPrice3.setBorder(border);

        lblRetracStr.setBorder(border);
        lblRetrc.setBorder(border);

        lbl1Str.setBorder(border);
        lbl1.setBorder(border);

        lbl2Str.setBorder(border);
        lbl2.setBorder(border);

        lbl3Str.setBorder(border);
        lbl3.setBorder(border);

        lbl4Str.setBorder(border);
        lbl4.setBorder(border);

        lbl5Str.setBorder(border);
        lbl5.setBorder(border);

        lbl6Str.setBorder(border);
        lbl6.setBorder(border);

        lbl7Str.setBorder(border);
        lbl7.setBorder(border);

    }

    public double[] getPoints() {
        return points;
    }

    public void setPoints(double[] points) {
        this.points = points;
    }

    public void showReults(double[] points) {
        try {
            double[] resultArray = new double[PERCENTAGE_ARRAY.length];

            double point1 = points[0];
            double point2 = points[1];
            double point3 = points[2];
            double wave = point2 - point1;

            for (int i = 0; i < PERCENTAGE_ARRAY.length; i++) {
                resultArray[i] = point3 + (wave * PERCENTAGE_ARRAY[i]);
            }

            String emprtString = " ";
            this.lblPrice1.setText(emprtString + formatter.format(point1));
            this.lblPrice2.setText(emprtString + formatter.format(point2));
            this.lblPrice3.setText(emprtString + formatter.format(point3));

            String retrVal = "N/A";
            if ((point2 - point1) != 0) {
                retrVal = pctFormatter.format((point2 - point3) / (point2 - point1));
            }
            this.lblRetrc.setText(emprtString + retrVal);
            this.lbl1.setText(emprtString + formatter.format(resultArray[0]));
            this.lbl2.setText(emprtString + formatter.format(resultArray[1]));
            this.lbl3.setText(emprtString + formatter.format(resultArray[2]));
            this.lbl4.setText(emprtString + formatter.format(resultArray[3]));
            this.lbl5.setText(emprtString + formatter.format(resultArray[4]));
            this.lbl6.setText(emprtString + formatter.format(resultArray[5]));
            this.lbl7.setText(emprtString + formatter.format(resultArray[6]));

            /* clearing the arralylist and fill with jlables for priting puropses*/
            labels.clear();
            labels.add(lblPrice1Str);
            labels.add(lblPrice1);
            labels.add(lblPrice2Str);
            labels.add(lblPrice2);
            labels.add(lblPrice3Str);
            labels.add(lblPrice3);

            labels.add(lblRetracStr);
            labels.add(lblRetrc);
            labels.add(lbl1Str);
            labels.add(lbl1);
            labels.add(lbl2Str);
            labels.add(lbl2);
            labels.add(lbl3Str);
            labels.add(lbl3);
            labels.add(lbl4Str);
            labels.add(lbl4);
            labels.add(lbl5Str);
            labels.add(lbl5);
            labels.add(lbl6Str);
            labels.add(lbl6);
            labels.add(lbl7Str);
            labels.add(lbl7);

            applyTheme();
            this.getContentPane().repaint();
            this.setVisible(true);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(btnOk)) {
            this.setVisible(false);
            graph.GDMgr.fibCalBounds = null;
            graph.repaint();
        } else if (e.getSource().equals(btnHelp)) {

        } else if (e.getSource().equals(btnPrint)) {
            printPage();
        } else if (e.getSource().equals(btnCopy)) {
            copyToClipboard();
        }
    }

    public void printPage() {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        printJob.setPrintable(this);

        if (printJob.printDialog()) {
            try {
                printJob.print();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void applyTheme() {
        this.getContentPane().setBackground(Theme.getColor("BACKGROUND"));
        //this.setBackground(Theme.getColor("BACKGROUND"));
    }

    public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {

        if (pageIndex >= 1) {
            return Printable.NO_SUCH_PAGE;
        }

        printDocument((Graphics2D) g);
        return Printable.PAGE_EXISTS;
    }

    public void printDocument(Graphics2D g2) {

        g2.setColor(Color.BLACK);

        g2.setFont(new Font("Times new roman", Font.BOLD, 14));
        g2.drawString("Fibonacci price retracement calculator", 160, 50);

        g2.setFont(new Font("Times new roman", Font.PLAIN, 14));

        int x = 70;
        int y = 80;
        int width = 75;
        int height = 20;

        /*for (JLabel label : labels) {
            g2.drawString(label.getText(), x, y);

            if (label.getText().contains("=")) {
                if (label.getText().contains("Price 3")) {
                    g2.drawLine(x, y + 5, x + 140, y + 5);

                }
                x = x + width;
            } else {
                y = y + height;
                x = x - width;
            }
        }*/

        for (int i = 0; i < labels.size(); i++) {

            String text = labels.get(i).getText();
            g2.drawString(text, x, y);

            if (i % 2 == 0) {
                if (text.contains(Language.getString("PRICE_3"))) {
                    g2.drawLine(x, y + 5, x + 140, y + 5);
                }
                x = x + width;
            } else {
                y = y + height;
                x = x - width;
            }
        }
    }

    public void copyToClipboard() {

        Component[] comps = pnlLeft.getComponents();
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < comps.length; i++) {
            if (comps[i] instanceof JLabel) {
                String text = ((JLabel) comps[i]).getText();
                if (text != null && text.length() > 0) {
                    buffer.append(text);
                    if (i % 2 == 0) {
                        buffer.append(" = ");
                    } else {
                        buffer.append("\r\n");
                    }
                }
            }
        }


        String data = buffer.toString();
        StringSelection text = new StringSelection(data);
        clip.setContents(text, this);

        //System.out.println("===== buffer ===== " + data);
        data = null;
    }

    public void windowOpened(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowClosing(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        //graph.GDMgr.fibCalBounds = null;
        //graph.repaint();
    }

    public void windowClosed(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        graph.GDMgr.fibCalBounds = null;
        graph.repaint();
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

