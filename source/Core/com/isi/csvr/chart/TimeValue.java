package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: May 14, 2008
 * Time: 1:54:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class TimeValue implements Comparable {

    //time value in long
    private long time;

    //close value
    private double value;

    public TimeValue(long tm, double vl) {
        this.time = tm;
        this.value = vl;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public double getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    //implements the CompareTo() method since its needed before Sortintg the ArrayList<TimeValue>.
    //Collections.Sort(list, key)
    public int compareTo(Object obj) {

        TimeValue tv = (TimeValue) obj;
        return new Long(this.time).compareTo(tv.getTime());
    }
}
