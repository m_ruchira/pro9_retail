package com.isi.csvr.chart;


import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;


//public class SearchTreeModel implements TreeModel {
public class ChartSearchTreeModel implements TreeModel {
    private ChartExchangeNode map;

    public ChartSearchTreeModel(ChartExchangeNode map) {
        //System.out.println("");
        this.map = map;
    }

    public Object getRoot() {
        return map;
    }


    public Object getChild(Object parent, int index) {
        return ((ChartExchangeNode) parent).getChild(index);

    }

    public int getChildCount(Object parent) {
        return ((ChartExchangeNode) parent).getChildCount();

    }

    public boolean isLeaf(Object node) {
        if (node instanceof ChartExchangeNode) {
            return false;
        } else {
            return true;
        }

    }

    public void valueForPathChanged(TreePath path, Object newValue) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getIndexOfChild(Object parent, Object child) {
        return ((ChartExchangeNode) parent).getIndex(child);
    }

    public void addTreeModelListener(TreeModelListener l) {
    }

    public void removeTreeModelListener(TreeModelListener l) {
    }
}
