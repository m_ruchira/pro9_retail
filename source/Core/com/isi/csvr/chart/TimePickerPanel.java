package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Aug 26, 2008
 * Time: 11:31:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class TimePickerPanel extends JPanel implements KeyListener {

    FlexGridLayout layout = new FlexGridLayout(new String[]{"27", "5", "27"}, new String[]{"18"}, 2, 1);
    private JTextField txtHours;
    private JTextField txtMins;

    public TimePickerPanel() {
        super();
        createUI();
    }

    public JTextField getTxtHours() {
        return txtHours;
    }

    public JTextField getTxtMins() {
        return txtMins;
    }

    private void createUI() {

        this.setLayout(layout);
        this.setPreferredSize(new Dimension(60, 20));

        txtHours = new JTextField();
        txtMins = new JTextField();
        txtHours.setToolTipText(Language.getString("CUSTOM_INTERVAL_HOURS"));
        txtMins.setToolTipText(Language.getString("CUSTOM_INTERVAL_MINUTES"));

        this.add(txtHours);
        this.add(new JLabel(":"));
        this.add(txtMins);

        txtMins.addKeyListener(this);
        txtHours.addKeyListener(this);
    }

    public void setHours(String val) {
        this.txtHours.setText(val);
    }

    public void setMins(String val) {
        this.txtMins.setText(val);
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == txtHours) {
            try {
                int hours = 0;
                String text = txtHours.getText().trim();
                if (!text.equals("")) {
                    hours = Integer.parseInt(text);
                    if (!isValidHour(hours)) {
                        txtHours.setText("12");
                    }
                }
            } catch (Exception ex) {
                txtHours.setText("12");
            }
        } else if (e.getSource() == txtMins) {
            try {

                int min = 0;
                String text = txtMins.getText().trim();
                if (!text.equals("")) {
                    min = Integer.parseInt(text);
                    if (!isValidMin(min)) {
                        txtMins.setText("00");
                    }
                }


            } catch (Exception ex) {
                txtMins.setText("00");
            }
        }
    }

    public boolean isValidHour(int hour) {
        for (int i = 0; i < 24; i++) {
            if (hour == i) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidMin(int min) {
        for (int i = 0; i < 60; i++) {
            if (min == i) {
                return true;
            }
        }
        return false;
    }

    public void disableComponent() {
        txtHours.setEnabled(false);
        txtMins.setEnabled(false);

    }

    public void enableComponent() {
        txtHours.setEnabled(true);
        txtMins.setEnabled(true);

    }

    public void controlComponent(boolean enable) {
        if (enable) {
            enableComponent();
        } else {
            disableComponent();
        }
    }

    public void setTime(long millis) {

        try {
            SimpleDateFormat timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
            String time = timeFormatter.format(millis);
            String[] tokens = time.split("-");
            String hours = tokens[1].split(":")[0];
            String mins = tokens[1].split(":")[1];

            txtHours.setText(hours);
            txtMins.setText(mins);
        } catch (Exception e) {
            e.printStackTrace();
            txtHours.setText("");
            txtMins.setText("");
        }
    }
}
