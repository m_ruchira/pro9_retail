package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:33:53 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class MoneyFlowIndex extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_MoneyFlowIndex;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public MoneyFlowIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_MONEY_FLOW") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MONEY_FLOW");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods,
                                          byte destIndex) {
        double posFlow = 0d, negFlow = 0d;
        double currVal, preVal, currValOld, preValOld;
        ChartRecord cr, crOld;

        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        cr = (ChartRecord) al.get(0);
        preVal = (cr.High + cr.Low + cr.Close) / 3f;
        preValOld = preVal;

        int loopBegin = bIndex + t_im_ePeriods + 1;

        for (int i = bIndex + 1; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High + cr.Low + cr.Close) / 3f;
            if (preVal > currVal) {
                negFlow += currVal * cr.Volume;
            } else {
                posFlow += currVal * cr.Volume;
            }
            preVal = currVal;
            if (i == loopBegin - 1) {
                /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
                }*/
                cr.setValue(destIndex, posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High + cr.Low + cr.Close) / 3f;
            if (preVal > currVal) {
                negFlow += currVal * cr.Volume;
            } else {
                posFlow += currVal * cr.Volume;
            }
            preVal = currVal;

            crOld = (ChartRecord) al.get(i - t_im_ePeriods);
            currValOld = (crOld.High + crOld.Low + crOld.Close) / 3f;
            if (preValOld > currValOld) {
                negFlow -= currValOld * crOld.Volume;
            } else {
                posFlow -= currValOld * crOld.Volume;
            }
            preValOld = currValOld;

            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
            }*/
            cr.setValue(destIndex, posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
        }
        //System.out.println("**** Money Flow Index Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MoneyFlowIndex) {
            MoneyFlowIndex mfi = (MoneyFlowIndex) cp;
            this.timePeriods = mfi.timePeriods;
            this.innerSource = mfi.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_MONEY_FLOW") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    /*
    I have used MFI = posFlow*100f/(posFlow+negFlow)
    which is correct and the best equation - if you follow the steps in metastock
    to arrive at this by first calculating "Money Ratio" it can lead to a division by zero
    when negative flow is zero - so it should not be used.
    */
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double posFlow = 0d, negFlow = 0d;
        double currVal, preVal, currValOld, preValOld;
        ChartRecord cr, crOld;

        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        cr = (ChartRecord) al.get(0);
        preVal = (cr.High + cr.Low + cr.Close) / 3f;
        preValOld = preVal;

        int loopBegin = timePeriods + 1;

        for (int i = 1; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High + cr.Low + cr.Close) / 3f;
            if (preVal > currVal) {
                negFlow += currVal * cr.Volume;
            } else {
                posFlow += currVal * cr.Volume;
            }
            preVal = currVal;
            if (i == loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.High + cr.Low + cr.Close) / 3f;
            if (preVal > currVal) {
                negFlow += currVal * cr.Volume;
            } else {
                posFlow += currVal * cr.Volume;
            }
            preVal = currVal;

            crOld = (ChartRecord) al.get(i - timePeriods);
            currValOld = (crOld.High + crOld.Low + crOld.Close) / 3f;
            if (preValOld > currValOld) {
                negFlow -= currValOld * crOld.Volume;
            } else {
                posFlow -= currValOld * crOld.Volume;
            }
            preValOld = currValOld;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(posFlow + negFlow == 0 ? 0 : posFlow * 100f / (posFlow + negFlow));
            }
        }
        //System.out.println("**** Money Flow Index Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_MONEY_FLOW");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
