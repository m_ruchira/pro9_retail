package com.isi.csvr.chart;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class LinearRegression extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_LRI;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public LinearRegression(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("LRI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_LRI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte srcIndex, byte destIndex) {
        ChartRecord cr, crOld;
        long entryTime = System.currentTimeMillis();

        ///////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        ////////////////////////////////////////////////////////////

        int loopBegin = bIndex + t_im_ePeriods;
        if ((loopBegin >= al.size()) || (t_im_ePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }

        // if a single point or two points are taken
        // to calculate LR, result is always the last point
        if (t_im_ePeriods < 3) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi = 0, xPi, b1, b0;

        int count = 0;
        for (int i = bIndex; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            boolean isValid = false;
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = GDM.getClosestIndexFortheTime(cr.Time);
                yi = val;
                zigmaX += xi;
                zigmaY += yi;
                zigmaXX += xi * xi;
                zigmaXY += xi * yi;
            }
            if ((i >= loopBegin - 1) && isValid) {
                Sxx = zigmaXX - zigmaX * zigmaX / count;
                Sxy = zigmaXY - zigmaX * zigmaY / count;
                b1 = Sxx == 0 ? 0 : Sxy / Sxx;
                b0 = zigmaY / count - b1 * zigmaX / count;
                cr.setStepValue(destIndex, b0 + b1 * xi);
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - t_im_ePeriods);
            double val = crOld.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                count--;

                xPi = GDM.getClosestIndexFortheTime(crOld.Time);
                yPi = val;
                zigmaX = zigmaX - xPi;
                zigmaY = zigmaY - yPi;
                zigmaXX = zigmaXX - xPi * xPi;
                zigmaXY = zigmaXY - xPi * yPi;
            }

            boolean isValid = false;
            cr = (ChartRecord) al.get(i);
            val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                isValid = true;
                count++;

                xi = GDM.getClosestIndexFortheTime(cr.Time);
                yi = val;
                zigmaX = zigmaX + xi;
                zigmaY = zigmaY + yi;
                zigmaXX = zigmaXX + xi * xi;
                zigmaXY = zigmaXY + xi * yi;
            }

            if (isValid && (count > 0)) {
                Sxx = zigmaXX - zigmaX * zigmaX / count;
                Sxy = zigmaXY - zigmaX * zigmaY / count;
                b1 = Sxx == 0 ? 0 : Sxy / Sxx;
                b0 = zigmaY / count - b1 * zigmaX / count;

                cr.setStepValue(destIndex, b0 + b1 * xi);
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        //System.out.println("**** LR Step Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof LinearRegression) {
            LinearRegression linearRegression = (LinearRegression) cp;
            this.timePeriods = linearRegression.timePeriods;
            this.innerSource = linearRegression.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("LRI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;
        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi, xPi, b1, b0;
        ChartRecord cr, crOld;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            zigmaX += xi;
            zigmaY += yi;
            zigmaXX += xi * xi;
            zigmaXY += xi * yi;
            if (i >= loopBegin - 1) {
                Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
                Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
                b1 = (float) (Sxy / Sxx);
                b0 = (float) (zigmaY / timePeriods - b1 * zigmaX / timePeriods);
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(b0 + b1 * xi);
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - loopBegin);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getValue(getOHLCPriority());
            xPi = GDM.getClosestIndexFortheTime(crOld.Time);
            yPi = crOld.getValue(getOHLCPriority());

            zigmaX = zigmaX + xi - xPi;
            zigmaY = zigmaY + yi - yPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;
            zigmaXY = zigmaXY + xi * yi - xPi * yPi;

            Sxx = zigmaXX - zigmaX * zigmaX / timePeriods;
            Sxy = zigmaXY - zigmaX * zigmaY / timePeriods;
            b1 = Sxy / Sxx;
            b0 = zigmaY / timePeriods - b1 * zigmaX / timePeriods;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(b0 + b1 * xi);
            }
        }
        //System.out.println("**** LinearRegression Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("LRI");
    }

    //To be used by custom indicators
/*
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
        int t_im_ePeriods, byte srcIndex, byte destIndex) {

        ChartRecord cr, crOld;
        long entryTime = System.currentTimeMillis();
        int loopBegin = t_im_ePeriods;
        if ((loopBegin >= al.size()) || (loopBegin < 2)) {
            for (int i = 0; i < loopBegin; i++) {
                cr = (ChartRecord)al.get(i);
//                cr.setValue(destIndex, Indicator.NULL);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }

        double zigmaX = 0, zigmaY = 0, zigmaXX = 0, zigmaXY = 0, yi, yPi, Sxx, Sxy;
        double xi, xPi, b1, b0;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord)al.get(i);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getStepValue(srcIndex);
            zigmaX += xi;
            zigmaY += yi;
            zigmaXX += xi * xi;
            zigmaXY += xi * yi;
            if (i >= loopBegin - 1) {
                Sxx = zigmaXX - zigmaX * zigmaX / t_im_ePeriods;
                Sxy = zigmaXY - zigmaX * zigmaY / t_im_ePeriods;
                b1 = (float)(Sxy / Sxx);
                b0 = (float)(zigmaY / t_im_ePeriods - b1 * zigmaX / t_im_ePeriods);
//                cr.setValue(destIndex, b0 + b1 * xi);
                cr.setStepValue(destIndex, b0 + b1 * xi);
            } else {
//                cr.setValue(destIndex, Indicator.NULL);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord)al.get(i);
            crOld = (ChartRecord)al.get(i - loopBegin);
            xi = GDM.getClosestIndexFortheTime(cr.Time);
            yi = cr.getStepValue(srcIndex);
            xPi = GDM.getClosestIndexFortheTime(crOld.Time);
            yPi = crOld.getStepValue(srcIndex);

            zigmaX = zigmaX + xi - xPi;
            zigmaY = zigmaY + yi - yPi;
            zigmaXX = zigmaXX + xi * xi - xPi * xPi;
            zigmaXY = zigmaXY + xi * yi - xPi * yPi;

            Sxx = zigmaXX - zigmaX * zigmaX / t_im_ePeriods;
            Sxy = zigmaXY - zigmaX * zigmaY / t_im_ePeriods;
            b1 = Sxy / Sxx;
            b0 = zigmaY / t_im_ePeriods - b1 * zigmaX / t_im_ePeriods;

//            cr.setValue(destIndex, b0 + b1 * xi);
            cr.setStepValue(destIndex, b0 + b1 * xi);
        }
        System.out.println("**** LR Step Calc time " + (entryTime - System.currentTimeMillis()));
    }
*/

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}