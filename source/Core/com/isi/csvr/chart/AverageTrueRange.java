package com.isi.csvr.chart;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Mar 31, 2005 - Time: 7:31:03 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class AverageTrueRange extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_AvgTrueRange;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public AverageTrueRange(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_AVG_TRUE_RANGE") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_AVG_TRUE_RANGE");
        this.timePeriods = 14;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    /**
     * This is to be used by other OHLC indicators those calculate a Sum of True Range as an intermediate step (Directional Movement Indicators)
     *
     * @param al          The ArrayList which has Object[] as items. In this object[] first element is time, second is a float array containing O,H,L,C valuse
     *                    and third is a blank which the calculated value is loaded when the method is returned
     * @param bIndex      The begin index of the ArrayList which the calculation should start
     * @param timePeriods Time period of the calculation
     */
    public static void getSumOfTrueRange(ArrayList al, int bIndex, int timePeriods, byte destIndex) {
        ChartRecord cr, crOld;
        double currVal, tmpSum, preVal = 0;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        for (int i = 0; i < bIndex; i++) {
            if (i >= al.size()) return;
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(destIndex, Indicator.NULL);
        }
        if (al.size() < bIndex + 1) return;

        cr = (ChartRecord) al.get(bIndex);
        currVal = cr.High - cr.Low;
        tmpSum = currVal;
        cr.setStepValue(destIndex, Indicator.NULL);

        for (int i = bIndex + 1; i < bIndex + timePeriods; i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            tmpSum += currVal;
            if (i == timePeriods - 1) {
                preVal = tmpSum;
                cr.setStepValue(destIndex, preVal);
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = bIndex + timePeriods; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);

            preVal = preVal - (preVal / timePeriods) + currVal;
            cr.setStepValue(destIndex, preVal);
        }

        //System.out.println("**** Inter Sum of True Range Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static void getAverageTrueRangeOld(ArrayList al, int bIndex, int timePeriods, byte destIndex) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        ChartRecord cr, crOld;
        double currVal, tmpSum, preVal = 0;

        /////////// no need to avoid null points as there is no srcIndex ////////////

        int loopBegin = bIndex + timePeriods;
        if ((al.size() <= loopBegin) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        //calculating bIndex
        cr = (ChartRecord) al.get(bIndex);
        currVal = cr.High - cr.Low;
        tmpSum = currVal;
        //calculating initail value for ATR
        for (int i = bIndex + 1; i < loopBegin; i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            tmpSum += currVal;
            if (i == loopBegin - 1) {
                preVal = tmpSum / timePeriods;
                cr.setValue(destIndex, preVal);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            preVal = preVal + (currVal - preVal) / timePeriods;
            cr.setValue(destIndex, preVal);
        }
        //System.out.println("****Inter Average True Range Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte destIndex) {
        getAverageTrueRange(al, bIndex, t_im_ePeriods, destIndex);

    }

    /**
     * This is to be used by other OHLC indicators those calculate an Average True Range as an intermediate step
     * (Directional Movement Indicators)
     *
     * @param al          The ArrayList which is already passed to AverageTrueRange.getSumOfTrueRange() method.
     *                    The values of this ArrayList are overwritten by this method
     * @param bIndex      The begin index of the ArrayList which the calculation should start
     * @param timePeriods Time period of the calculation
     */
/*
    public static void getAverageTrueRange(ArrayList al, int bIndex, int timePeriods) {
        Object[] oa1;
        float currVal, preVal;
        //tmp var for sop
        if (al.size()<timePeriods) return;
        long entryTime = System.currentTimeMillis();

        for (int i=0; i<=bIndex; i++) {
            if (i>=al.size()) return;
            oa1 = (Object[])al.get(i);
            oa1[2] = null;
        }

        for(int i=(bIndex + 1); i<(bIndex + timePeriods); i++){
            oa1 = (Object[])al.get(i);
            currVal = (Float)oa1[2];
            if (i==timePeriods-1){
                preVal = currVal/timePeriods;
                oa1[2] = preVal;
            } else {
                oa1[2] = null;
            }
        }

        //calculating rest and store temporarily in oa[2]
        for(int i=(bIndex + timePeriods); i<al.size(); i++){
            oa1 = (Object[])al.get(i);
            currVal = (Float)oa1[2];

            oa1[2] = currVal/timePeriods;
        }

        System.out.println("**** Inter Average True Range Calc time " + (entryTime - System.currentTimeMillis()));
    }
*/
    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public static void getAverageTrueRange(ArrayList al, int bIndex, int timePeriods, byte destIndex) {

        //tmp var for sop
        //long entryTime = System.currentTimeMillis();

        ChartRecord cr, crOld;
        double currVal, tmpSum, preVal = 0;

        /////////// no need to avoid null points as there is no srcIndex ////////////

        int loopBegin = bIndex + timePeriods;
        if ((al.size() <= loopBegin) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        //calculating bIndex
        cr = (ChartRecord) al.get(bIndex);
        currVal = cr.High - cr.Low;
        tmpSum = currVal;
        //calculating initail value for ATR
        for (int i = bIndex + 1; i < loopBegin; i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            tmpSum += currVal;
            if (i == loopBegin - 1) {
                preVal = tmpSum / timePeriods;
                cr.setValue(destIndex, preVal);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        for (int i = loopBegin; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            preVal = preVal + (currVal - preVal) / timePeriods;
            cr.setValue(destIndex, preVal);
        }

    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof AverageTrueRange) {
            AverageTrueRange atr = (AverageTrueRange) cp;
            this.timePeriods = atr.timePeriods;
            this.innerSource = atr.innerSource;
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_AVG_TRUE_RANGE") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }
    //############################################

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // here the final wilders smoothing step is done internally for optimal performance
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        ChartRecord cr, crOld;
        double currVal, tmpSum, preVal = 0;
        //tmp var for sop
        if (al.size() < timePeriods) return;
        long entryTime = System.currentTimeMillis();

        //calculating time 0
        cr = (ChartRecord) al.get(0);
        currVal = cr.High - cr.Low;
        tmpSum = currVal;
        //calculating initail value for ATR
        for (int i = 1; i < timePeriods; i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            tmpSum += currVal;
            if (i == timePeriods - 1) {
                preVal = tmpSum / timePeriods;
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(preVal);
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        for (int i = timePeriods; i < al.size(); i++) {
            crOld = (ChartRecord) al.get(i - 1);
            cr = (ChartRecord) al.get(i);
            currVal = Math.max(cr.High - cr.Low, cr.High - crOld.Close);
            currVal = Math.max(currVal, crOld.Close - cr.Low);
            preVal = preVal + (currVal - preVal) / timePeriods;
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(preVal);
        }
        //System.out.println("**** Average True Range Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_AVG_TRUE_RANGE");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }


}
