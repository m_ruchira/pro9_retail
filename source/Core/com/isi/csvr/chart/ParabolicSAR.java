package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorParabolicSARProperties;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 8:10:04 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class ParabolicSAR extends ChartProperties implements Indicator, Serializable {
    private static final long serialVersionUID = UID_PARABOLIC_SAR;
    //Fields
    private ChartProperties innerSource;
    private float step;
    private float max;
    private String tableColumnHeading;

    public ParabolicSAR(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_PARABOLIC_SAR") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_PARABOLIC_SAR");
        this.step = 0.02f;
        this.max = 0.2f;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorParabolicSARProperties idp = (IndicatorParabolicSARProperties) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setStep(idp.getStep());
            this.setMax(idp.getMax());
        }
    }

    //############################################
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          double step, double maximum, byte destIndex) {

        float stepF = (float) step;
        float maximumF = (float) maximum;

        long entryTime = System.currentTimeMillis();
        if (al.size() < 1) return;

        ChartRecord cr;
        double accFactor = maximumF, extremePr, sar, preSar;
        boolean isLong = true, isChanged = false, plotting = false;

        /////////// no need to avoid null points as there is no srcIndex ////////////

        for (int i = 0; i < Math.min(bIndex, al.size()); i++) { //al.Count used here only if there are not enough points even to start
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        if (bIndex >= al.size()) { // not enough points to start calculating Parabolic SAR
            return;
        }

        //first point
        cr = (ChartRecord) al.get(bIndex);
        extremePr = cr.High;
        sar = cr.Low;
        preSar = sar + (extremePr - sar) * accFactor;
        //remove first point
        cr.setValue(destIndex, Indicator.NULL);

        for (int i = bIndex + 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            //accelaretion factor
            if (isChanged) {
                plotting = true;
                accFactor = stepF;
            } else {
                if (accFactor < maximumF) {
                    if (isLong) {
                        if (cr.High > extremePr) accFactor += stepF;
                    } else {
                        if (cr.Low < extremePr) accFactor += stepF;
                    }
                    if (accFactor > maximumF) accFactor = maximumF;
                }
            }
            //extreme Price
            double preExtreme = extremePr;
            if (isLong) {
                if (isChanged) {
                    extremePr = cr.High;
                } else {
                    if (cr.High > extremePr) extremePr = cr.High;
                }
            } else {
                if (isChanged) {
                    extremePr = cr.Low;
                } else {
                    if (cr.Low < extremePr) extremePr = cr.Low;
                }
            }
            //sar
            if (isChanged) {
                sar = preExtreme;
            } else {
                sar = preSar;
            }
            //preSar
            preSar = sar + (extremePr - sar) * accFactor;
            //isChanged
            isChanged = false;
            if (isLong) {
                if (cr.Low < sar) {
                    isLong = false;
                    isChanged = true;
                }
            } else {
                if (cr.High > sar) {
                    isLong = true;
                    isChanged = true;
                }
            }

            if (plotting) {
                cr.setValue(destIndex, sar);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        //System.out.println("**** Inner ParabolicSAR Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.max = 0.2f;
        this.step = 0.02f;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.step = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/Step"));
        this.max = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression + "/Max"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "Step", Float.toString(this.step));
        TemplateFactory.saveProperty(chart, document, "Max", Float.toString(this.max));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof ParabolicSAR) {
            ParabolicSAR co = (ParabolicSAR) cp;
            this.innerSource = co.innerSource;
            this.step = co.step;
            this.max = co.max;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += "(" + Language.getString("PARABOLIC_SAR_STEP") + "=" + step + ", " +
                Language.getString("PARABOLIC_SAR_MAX") + "=" + max + ")";
        return Language.getString("IND_PARABOLIC_SAR") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        if (al.size() < 1) return;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();

        ChartRecord cr;
        double accFactor = max, extremePr, sar, preSar;
        boolean isLong = true, isChanged = false;

        cr = (ChartRecord) al.get(0);
        extremePr = cr.High;
        sar = cr.Low;
        preSar = sar + (extremePr - sar) * accFactor;

        ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
        if (aPoint != null) {
            aPoint.setIndicatorValue(sar);
        }

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            //accelaretion factor
            if (isChanged) {
                accFactor = step;
            } else {
                if (accFactor < max) {
                    if (isLong) {
                        if (cr.High > extremePr) accFactor += step;
                    } else {
                        if (cr.Low < extremePr) accFactor += step;
                    }
                    if (accFactor > max) accFactor = max;
                }
            }
            //extreme Price
            double preExtreme = extremePr;
            if (isLong) {
                if (isChanged) {
                    extremePr = cr.High;
                } else {
                    if (cr.High > extremePr) extremePr = cr.High;
                }
            } else {
                if (isChanged) {
                    extremePr = cr.Low;
                } else {
                    if (cr.Low < extremePr) extremePr = cr.Low;
                }
            }
            //sar
            if (isChanged) {
                sar = preExtreme;
            } else {
                sar = preSar;
            }
            //preSar
            preSar = sar + (extremePr - sar) * accFactor;
            //isChanged
            isChanged = false;
            if (isLong) {
                if (cr.Low < sar) {
                    isLong = false;
                    isChanged = true;
                }
            } else {
                if (cr.High > sar) {
                    isLong = true;
                    isChanged = true;
                }
            }

            aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(sar);
            }
        }
        //System.out.println("**** Parabolic SAR Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_PARABOLIC_SAR");
    }

    public float getStep() {
        return step;
    }

    public void setStep(float step) {
        this.step = step;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
