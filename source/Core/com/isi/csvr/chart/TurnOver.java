package com.isi.csvr.chart;

import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Oct 4, 2008
 * Time: 1:08:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class TurnOver extends ChartProperties implements Indicator, Serializable {

    private static final long serialVersionUID = UID_TURNOVER;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public TurnOver(ArrayList<IntraDayOHLC> data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("GRAPH_TURNOVER") + Indicator.FD + symbl, anID, c, r);
        setOHLCPriority(GraphDataManager.INNER_TurnOver);
        tableColumnHeading = Language.getString("GRAPH_TURNOVER");
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof TurnOver) {
            TurnOver vol = (TurnOver) cp;
            this.innerSource = vol.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "";
        }
        return Language.getString("GRAPH_TURNOVER") + parent;
    }

    public String getShortName() {
        return Language.getString("GRAPH_TURNOVER");
    }

    /* ================================ implmenting Indicator Interface methods ========================*/
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int graphIndex) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, graphIndex, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.TurnOver);
                //System.out.println("========= cr.TurnOver ========= " + cr.TurnOver);
            }
        }
        //System.out.println("**** Volume Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    /* ============================== end of implmenting Indicator Interface methods ========================*/
}
