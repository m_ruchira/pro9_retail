package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: Apr 17, 2005 - Time: 5:04:37 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public final class CustomDialogs {

    final static int CMB_HEIGHT = 22;
    final static int CMB_WIDTH = 70;
    final static int LBL_HEIGHT = 20;
    final static int GAP = 1;
    final static int LEFT = 5;
    final static Dimension DIM_DIALOG = new Dimension(340, 250);
    static ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif");
    private static int[] periodSettings = null;
    private static long[] intervalSettings = null;

    public static CustomDialog getSemiLogScaleDialog(ArrayList panels, final Hashtable result) {
        final CustomDialog cdlg = new CustomDialog();
        JPanel pnl = cdlg.getPanel();
        cdlg.setTitle(Language.getString("SET_SEMI_LOG_SCALE"));
        //pnl.setLayout(null);
        pnl.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100%"}, 5, 5));
        pnl.setPreferredSize(new Dimension(458, 300));


        GraphLabel lblSelectWindows = new GraphLabel(Language.getString("SELECT_WINDOWS_TO_SET_SEMILOG"));
        lblSelectWindows.setBounds(LEFT, LEFT, 400, CMB_HEIGHT);

        final JPanel pnlAvailable = new JPanel();
        pnlAvailable.setLayout(new BoxLayout(pnlAvailable, BoxLayout.PAGE_AXIS));
        //pnlAvailable.setLayout(new GridLayout(100, 1, 3, 3));
        for (int i = 0; i < panels.size(); i++) {
            final WindowPanel wp = (WindowPanel) panels.get(i);
            final JCheckBox chk = new JCheckBox(wp.getTitle());
            chk.setSelected(wp.isSemiLog());
            chk.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    result.put(wp, chk.isSelected());
                }
            });
            chk.setEnabled(wp.isSemiLogEnabled());
            pnlAvailable.add(chk);
        }

        JScrollPane spAvailable = new JScrollPane(pnlAvailable);
        spAvailable.setBounds(new Rectangle(LEFT, 30, 450, 270));
        spAvailable.setAutoscrolls(true);
        pnl.add(lblSelectWindows);
        pnl.add(spAvailable);

        return cdlg;
    }

    public static CustomDialog getRightMarginDialog(int historyMgn, int intraMgn) {
        final CustomDialog cdlg = new CustomDialog();
        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(285, 50));
        cdlg.setTitle(Language.getString("SET_CHART_RMARGIN"));
        String[] PanelWidths = {"10", "100%", "15", "75", "10"};
        String[] PanelHeights = {"10", "20", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        pnl.setLayout(flexGridLayout);

        GraphLabel lblTimePeriods = new GraphLabel(Language.getString("CHART_NO_OF_INTERVALS"));
        lblTimePeriods.setBounds(LEFT, GAP, 200, CMB_HEIGHT);
        SpinnerNumberModel model = new SpinnerNumberModel(12, 2, 1000, 1);
        final JSpinner spinTimePeriods = new JSpinner(model);
        spinTimePeriods.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinTimePeriods.getModel()).setValue(new Integer(historyMgn));

        GraphLabel lblRTPeriods = new GraphLabel(Language.getString("CHART_INRADAY_RMARGIN"));
        lblRTPeriods.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, 200, CMB_HEIGHT);
        SpinnerNumberModel modelRT = new SpinnerNumberModel(12, 2, 1000, 1);
        final JSpinner spinRT = new JSpinner(modelRT);
        spinRT.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, 100, CMB_HEIGHT);
        ((SpinnerNumberModel) spinRT.getModel()).setValue(new Integer(intraMgn));

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int val1 = ((SpinnerNumberModel) spinTimePeriods.getModel()).getNumber().intValue();
                int val2 = ((SpinnerNumberModel) spinRT.getModel()).getNumber().intValue();
                cdlg.result = new int[]{val1, val2};
            }
        });
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblTimePeriods);
        pnl.add(new JLabel());
        pnl.add(spinTimePeriods);
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblRTPeriods);
        pnl.add(new JLabel());
        pnl.add(spinRT);
        pnl.add(new JLabel());

        return cdlg;
    }

    public static CustomDialog getDefaultPropertiesDialog(final ChartProperties cp, final GraphFrame graphFrame) {
        final CustomDialog cdlg = new CustomDialog();
        JPanel pnlDlg = cdlg.getPanel();
        cdlg.setTitle(Language.getString("SET_DEFAULT_PROPERTIES"));
        cdlg.setSize(DIM_DIALOG);
        pnlDlg.setLayout(null);

        final ChartSettings cs = ChartSettings.getSettings();
        TWTabbedPane tabbedPane = null;
        tabbedPane = new TWTabbedPane();
        tabbedPane.setSize(DIM_DIALOG);

        JPanel panel = PropertyDialogFactory.createGeneralPanel(cp, cp, null);
        JPanel pnl = new JPanel();
        pnl.setSize(DIM_DIALOG);
        pnl.setLayout(null);

        JPanel otherPanel = new JPanel();
        otherPanel.setSize(DIM_DIALOG);
        otherPanel.setLayout(null);

        //History
        JPanel pnlHistory = new JPanel();
        pnlHistory.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlHistory.setLayout(null);
        pnlHistory.setBounds(LEFT + 153, GAP, 145, 5 * CMB_HEIGHT + 6 * GAP + 10);
        //Intraday
        JPanel pnlIntraday = new JPanel();
        pnlIntraday.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                "", TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlIntraday.setLayout(null);
        pnlIntraday.setBounds(LEFT, GAP, 145, 5 * CMB_HEIGHT + 6 * GAP + 10);

        final GraphLabel lblPeriod2 = new GraphLabel(Language.getString("PERIOD"));
        lblPeriod2.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        final JComboBox cmbPeriod2 = new JComboBox();
        cmbPeriod2.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        cmbPeriod2.setEditable(false);
        cmbPeriod2.setFont(StockGraph.font_BOLD_10);
        final String[] saPeriods2 = GraphFrame.getHistoryPeriods();
        final int[] iaPeriods2 = {StockGraph.ONE_MONTH, StockGraph.THREE_MONTHS, StockGraph.SIX_MONTHS,
                StockGraph.YTD, StockGraph.ONE_YEAR, StockGraph.TWO_YEARS, StockGraph.THREE_YEARS,
                StockGraph.FIVE_YEARS, StockGraph.TEN_YEARS, StockGraph.ALL_HISTORY,
                StockGraph.CUSTOM_PERIODS
        };
        for (int i = 0; i < saPeriods2.length; i++) {
            cmbPeriod2.addItem(saPeriods2[i]);
            if (iaPeriods2[i] == cs.getPeriod()) {
                cmbPeriod2.setSelectedIndex(i);
            }
        }

        //added by charithn to pop up the set period dialg box in custom periods
        //cmdperiod2- combo box of history mode
        cmbPeriod2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JComboBox cmb = (JComboBox) e.getSource();
                String itemString = (String) cmb.getSelectedItem();
                //check whether its the custom periods selected
                if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                    CustomDialog dlg = null;
                    if (graphFrame != null) {
                        dlg = CustomDialogs.getCustomTimePeriodsDialog(graphFrame.graph.GDMgr.currentCustomPeriod,
                                graphFrame.graph.GDMgr.currentCustomRange, false);
                    } else {
                        //dlg = CustomDialogs.getCustomTimePeriodsDialog(Calendar.DAY_OF_YEAR, 60, false);  //ToDo: hard coded
                        dlg = CustomDialogs.getCustomTimePeriodsDialog(ChartSettings.getSettings().getCustomPeriod(), ChartSettings.getSettings().getCustomRange(), false);
                    }
                    dlg.setModal(true);
                    dlg.setLocationRelativeTo(ChartFrame.getSharedInstance());
                    dlg.pack();
                    dlg.setResizable(false);
                    dlg.setVisible(true);

                    if (dlg.result != null) {
                        //copies the results returned from the cutom period dialog box
                        periodSettings = (int[]) dlg.result;
                    } else {
                        return;
                    }
                } else {
                    //clear the period setting results in combobox menu changed event.
                    //if "other periods" is not selected.
                    periodSettings = null;
                }
            }
        });


        final GraphLabel lblInterval2 = new GraphLabel(Language.getString("INTERVAL"));
        lblInterval2.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        final JComboBox cmbInterval2 = new JComboBox();
        cmbInterval2.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        cmbInterval2.setEditable(false);
        cmbInterval2.setFont(StockGraph.font_BOLD_10);
        final String[] saIntervals2 = GraphFrame.getHistoryIntervals();
        final long[] customInterval = new long[]{-1L};
        if (graphFrame != null) {
            customInterval[0] = graphFrame.graph.GDMgr.currentCustomIntervalType *
                    graphFrame.graph.GDMgr.currentCustomIntervalFactor;
        } else {
            customInterval[0] = ChartSettings.getSettings().getCustomIntervalType() *
                    ChartSettings.getSettings().getCustomIntervalFactor();
        }
        final long[] laIntervals2 = {StockGraph.DAILY, StockGraph.WEEKLY, StockGraph.MONTHLY, customInterval[0]};
        for (int i = 0; i < saIntervals2.length; i++) {
            cmbInterval2.addItem(saIntervals2[i]);
            if (laIntervals2[i] == cs.getInterval()) {
                cmbInterval2.setSelectedIndex(i);
            }
        }

        //added by charithn
        //cmbInterval2 - interval combox in history mode
        cmbInterval2.addActionListener(new ActionListener() {
                                           public void actionPerformed(ActionEvent e) {

                                               JComboBox cmb = (JComboBox) e.getSource();
                                               String itemString = (String) cmb.getSelectedItem();

                                               //check whether its the custom interval selected
                                               if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                                                   CustomDialog dlg = null;
                                                   if (graphFrame != null) {
                                                       dlg = CustomDialogs.getCustomIntervalsDialog(graphFrame.graph.GDMgr.currentCustomIntervalType,
                                                               graphFrame.graph.GDMgr.currentCustomIntervalFactor, false);
                                                   } else {
                                                       dlg = CustomDialogs.getCustomIntervalsDialog(1L, 3L, false);   //false since its not in intraday moed
                                                       //dlg = CustomDialogs.getCustomIntervalsDialog(ChartSettings.getSettings().getCustomIntervalType(), ChartSettings.getSettings().getCustomIntervalFactor(), false);
                                                   }
                                                   dlg.setModal(true);
                                                   dlg.setLocationRelativeTo(ChartFrame.getSharedInstance());
                                                   dlg.pack();
                                                   dlg.setResizable(false);
                                                   dlg.setVisible(true);
                                                   if (dlg.result != null) {
                                                       intervalSettings = (long[]) dlg.result;
                                                   } else {
                                                       return;
                                                   }
                                               } else {
                                                   //clear the interval setting results in combobox menu changed event
                                                   intervalSettings = null;
                                               }
                                           }
                                       }
        );

        final GraphLabel lblPeriod = new GraphLabel(Language.getString("PERIOD"));
        lblPeriod.setBounds(LEFT, 2 * GAP + CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        final JComboBox cmbPeriod = new JComboBox();
        cmbPeriod.setBounds(LEFT, 3 * GAP + 2 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        cmbPeriod.setEditable(false);
        cmbPeriod.setFont(StockGraph.font_BOLD_10);

        final String[] saPeriods = GraphFrame.getCurrentPeriods();
        final int[] iaPeriods = {StockGraph.ONE_DAY, StockGraph.SIX_DAYS, StockGraph.ALL_HISTORY,
                StockGraph.CUSTOM_PERIODS};
        for (int i = 0; i < saPeriods.length; i++) {
            cmbPeriod.addItem(saPeriods[i]);
            if (iaPeriods[i] == cs.getPeriod()) {
                cmbPeriod.setSelectedIndex(i);
            }
        }

        //added by charithn to pop up the set period dialg box in custom periods
        //cmdPeriod - combo box for intraday mode
        cmbPeriod.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JComboBox cmb = (JComboBox) e.getSource();
                String itemString = (String) cmb.getSelectedItem();
                //check whether its the custom periods selected
                if (itemString.equals(Language.getString("OTHER_PERIODS"))) {

                    CustomDialog dlg = null;
                    if (graphFrame != null) {
                        dlg = CustomDialogs.getCustomTimePeriodsDialog(graphFrame.graph.GDMgr.currentCustomPeriod,
                                graphFrame.graph.GDMgr.currentCustomRange, true);
                    } else {
                        //dlg = CustomDialogs.getCustomTimePeriodsDialog(Calendar.DAY_OF_YEAR, 60, true);  //ToDo: hard coded
                        dlg = CustomDialogs.getCustomTimePeriodsDialog(ChartSettings.getSettings().getCustomPeriod(), ChartSettings.getSettings().getCustomRange(), true);
                    }
                    dlg.setModal(true);
                    dlg.setLocationRelativeTo(ChartFrame.getSharedInstance());
                    dlg.pack();
                    dlg.setResizable(false);
                    dlg.setVisible(true);

                    if (dlg.result != null) {
                        //copies the results returned from the cutom interval dialog box
                        periodSettings = (int[]) dlg.result;
                    } else {
                        return;
                    }
                } else {
                    //clear the cuctom period settings
                    periodSettings = null;
                }
            }
        });

        final GraphLabel lblInterval = new GraphLabel(Language.getString("INTERVAL"));
        lblInterval.setBounds(LEFT, 4 * GAP + 3 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        final JComboBox cmbInterval = new JComboBox();
        cmbInterval.setBounds(LEFT, 5 * GAP + 4 * CMB_HEIGHT, CMB_WIDTH, CMB_HEIGHT);
        cmbInterval.setEditable(false);
        cmbInterval.setFont(StockGraph.font_BOLD_10);
        final String[] saIntervals = GraphFrame.getCurrentIntervals();
        final long[] laIntervals = {StockGraph.EVERYMINUTE, StockGraph.EVERY5MINUTES, StockGraph.EVERY10MINUTES,
                StockGraph.EVERY15MINUTES, StockGraph.EVERY30MINUTES, StockGraph.EVERY60MINUTES, customInterval[0]};

        boolean customInter = true;
        for (int i = 0; i < saIntervals.length; i++) {
            cmbInterval.addItem(saIntervals[i]);
            if (laIntervals[i] == cs.getInterval()) {
                cmbInterval.setSelectedIndex(i);
                customInter = false;
            }
        }

        if (customInter) {
            cmbInterval.setSelectedIndex(saIntervals.length - 1);
        }

        //added by charithn
        //cmbInterval - interval combox in intraday mode
        cmbInterval.addActionListener(new ActionListener() {
                                          public void actionPerformed(ActionEvent e) {

                                              JComboBox cmb = (JComboBox) e.getSource();
                                              String itemString = (String) cmb.getSelectedItem();

                                              //check whether its the custom periods selected
                                              if (itemString.equals(Language.getString("OTHER_PERIODS"))) {
                                                  CustomDialog dlg = null;

                                                  if (graphFrame != null) {
                                                      dlg = CustomDialogs.getCustomIntervalsDialog(graphFrame.graph.GDMgr.currentCustomIntervalType,
                                                              graphFrame.graph.GDMgr.currentCustomIntervalFactor, true);
                                                  } else {
                                                      dlg = CustomDialogs.getCustomIntervalsDialog(1L, 3L, true);   //true since its  in intraday moed
                                                      //dlg = CustomDialogs.getCustomIntervalsDialog(ChartSettings.getSettings().getCustomIntervalType(), ChartSettings.getSettings().getCustomIntervalFactor(), true);
                                                  }

                                                  dlg.setModal(true);
                                                  dlg.setLocationRelativeTo(ChartFrame.getSharedInstance());
                                                  dlg.pack();
                                                  dlg.setResizable(false);
                                                  dlg.setVisible(true);
                                                  if (dlg.result != null) {
                                                      intervalSettings = (long[]) dlg.result;
                                                  } else {
                                                      return;
                                                  }
                                              } else {
                                                  //clear the custom interval setting results in combobox menu changed event
                                                  intervalSettings = null;
                                              }
                                          }
                                      }
        );


        final JCheckBox chkShowLastPriceLine = new JCheckBox(Language.getString("SHOW_CURRENT_PRICE_LINE"));
        chkShowLastPriceLine.setBounds(LEFT + 10, 10, 2 * CMB_WIDTH + 30, CMB_HEIGHT);
        chkShowLastPriceLine.setSelected(cs.isShowLastPriceLine());
        chkShowLastPriceLine.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        final JCheckBox chkShowOrderLines = new JCheckBox(Language.getString("SHOW_ORDER_LINES"));
        chkShowOrderLines.setBounds(LEFT + 10, 1 * CMB_HEIGHT + 10, 2 * CMB_WIDTH, CMB_HEIGHT);
        chkShowOrderLines.setSelected(cs.isShowOrderLines());
        chkShowOrderLines.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        final JCheckBox chkShowBidAskTags = new JCheckBox(Language.getString("SHOW_BID_ASK_TAGS"));
        chkShowBidAskTags.setBounds(LEFT + 10, 2 * CMB_HEIGHT + 10, 2 * CMB_WIDTH, CMB_HEIGHT);
        chkShowBidAskTags.setSelected(cs.isShowBidAskTags());
        chkShowBidAskTags.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        final JCheckBox chkShowMinMaxLines = new JCheckBox(Language.getString("SHOW_MIN_MAX_LINES"));
        chkShowMinMaxLines.setBounds(LEFT + 10, 3 * CMB_HEIGHT + 10, 2 * CMB_WIDTH, CMB_HEIGHT);
        chkShowMinMaxLines.setSelected(cs.isShowMinMaxLines());
        chkShowMinMaxLines.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        final JCheckBox chkShowVWAP = new JCheckBox(Language.getString("CHART_VWAP_SHOW"));
        chkShowVWAP.setBounds(LEFT + 10, 4 * CMB_HEIGHT + 10, 2 * CMB_WIDTH, CMB_HEIGHT);
        chkShowVWAP.setSelected(cs.isShowVWAP());
        chkShowVWAP.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        final JCheckBox chkOpenInProCharts = new JCheckBox(Language.getString("OPEN_IN_PROCHARTS"));
        chkOpenInProCharts.setBounds(LEFT + 10, 5 * CMB_HEIGHT + 10, 2 * CMB_WIDTH + 30, CMB_HEIGHT);
        chkOpenInProCharts.setSelected(cs.isOpenInProChart());
        chkOpenInProCharts.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        //todo - may be wrong - this is never used.so didn't finalize. 
        final JCheckBox chkShowPrvCloseLine = new JCheckBox(Language.getString("SHOW_PREVIOUS_CLOSE_LINE"));
        chkShowLastPriceLine.setBounds(LEFT + 10, 6 * CMB_HEIGHT + 10, 2 * CMB_WIDTH + 30, CMB_HEIGHT);
        chkShowLastPriceLine.setSelected(cs.isShowLastPriceLine());
        chkShowLastPriceLine.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        //later move to complete option dialog - sathyajith
//        final JCheckBox chkUseCustomGraphColors = new JCheckBox(Language.getString("ENABLE_CUSTOM_GRAPH_COLORS"));
//        chkUseCustomGraphColors.setBounds(LEFT+10, 6*CMB_HEIGHT+10,2*CMB_WIDTH+50,CMB_HEIGHT);
//        chkUseCustomGraphColors.setSelected(ChartSettings.customGraphColors);
//        chkUseCustomGraphColors.setFont(Theme.getDefaultFont(Font.PLAIN, 12));

        otherPanel.add(chkShowLastPriceLine);
        otherPanel.add(chkShowOrderLines);
        otherPanel.add(chkShowBidAskTags);
        otherPanel.add(chkShowMinMaxLines);
        otherPanel.add(chkShowVWAP);
        otherPanel.add(chkOpenInProCharts);
        otherPanel.add(chkShowPrvCloseLine);
//        otherPanel.add(chkUseCustomGraphColors);

        ButtonGroup bg = new ButtonGroup();
        final JRadioButton rbIntraDay = new JRadioButton(Language.getString("INTRADAY"));
        rbIntraDay.setBounds(LEFT, GAP, 80, CMB_HEIGHT);
        rbIntraDay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (rbIntraDay.isSelected()) {
                    lblPeriod2.setEnabled(false);
                    lblInterval2.setEnabled(false);
                    cmbPeriod2.setEnabled(false);
                    cmbInterval2.setEnabled(false);
                    lblPeriod.setEnabled(true);
                    lblInterval.setEnabled(true);
                    cmbPeriod.setEnabled(true);
                    cmbInterval.setEnabled(true);
                }
            }
        });
        rbIntraDay.setSelected(cs.isCurrentMode());

        final JRadioButton rbHistory = new JRadioButton(Language.getString("HISTORY"));
        rbHistory.setBounds(LEFT, GAP, 80, CMB_HEIGHT);
        rbHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (rbHistory.isSelected()) {
                    lblPeriod.setEnabled(false);
                    lblInterval.setEnabled(false);
                    cmbPeriod.setEnabled(false);
                    cmbInterval.setEnabled(false);
                    lblPeriod2.setEnabled(true);
                    lblInterval2.setEnabled(true);
                    cmbPeriod2.setEnabled(true);
                    cmbInterval2.setEnabled(true);
                }
            }
        });
        rbHistory.setSelected(!cs.isCurrentMode());
        if (rbHistory.isSelected()) {
            lblPeriod.setEnabled(false);
            lblInterval.setEnabled(false);
            cmbPeriod.setEnabled(false);
            cmbInterval.setEnabled(false);
            lblPeriod2.setEnabled(true);
            lblInterval2.setEnabled(true);
            cmbPeriod2.setEnabled(true);
            cmbInterval2.setEnabled(true);
        } else {
            lblPeriod2.setEnabled(false);
            lblInterval2.setEnabled(false);
            cmbPeriod2.setEnabled(false);
            cmbInterval2.setEnabled(false);
            lblPeriod.setEnabled(true);
            lblInterval.setEnabled(true);
            cmbPeriod.setEnabled(true);
            cmbInterval.setEnabled(true);
        }

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int intPeriod;
                long longInterval;
                boolean isCurrentMode = rbIntraDay.isSelected();
                if (isCurrentMode) {
                    intPeriod = iaPeriods[cmbPeriod.getSelectedIndex()];
                    longInterval = laIntervals[cmbInterval.getSelectedIndex()];
                } else {
                    intPeriod = iaPeriods2[cmbPeriod2.getSelectedIndex()];
                    longInterval = laIntervals2[cmbInterval2.getSelectedIndex()];
                }
//                boolean objShowVolume = chkShowVolume.isSelected();
                boolean objShowBidAskTags = chkShowBidAskTags.isSelected();
                boolean objShowLastLine = chkShowLastPriceLine.isSelected();
                boolean objShowPrvCloseLine = chkShowPrvCloseLine.isSelected();
                boolean objShowOrderLines = chkShowOrderLines.isSelected();
                boolean objShowMinMaxLines = chkShowMinMaxLines.isSelected();
                boolean objOpenInProCharts = chkOpenInProCharts.isSelected();
//                boolean objUseCustomGraphColors = chkUseCustomGraphColors.isSelected();
                boolean objShowVWAP = chkShowVWAP.isSelected();

                //added by charithn
                int customPeriods;
                int customRange;

                //if user sets his custom values, get it from there. else use the already saved values..
                if (periodSettings != null && periodSettings.length == 2) {
                    customPeriods = periodSettings[0];
                    customRange = periodSettings[1];

                } else {
                    customPeriods = ChartSettings.getSettings().getCustomPeriod();
                    customRange = ChartSettings.getSettings().getCustomRange();
                }

                long customIntervalType = 0L;
                long customIntervalFactor = 0L;

                if (intervalSettings != null) {
                    customIntervalType = intervalSettings[0];
                    customIntervalFactor = intervalSettings[1];
                    longInterval = customIntervalType * customIntervalFactor;

                } else {
                    customIntervalFactor = ChartSettings.getSettings().getCustomIntervalFactor();
                    customIntervalType = ChartSettings.getSettings().getCustomIntervalType();
                }

                if (graphFrame != null) {
                    //changed by charthin
                    //if user selects custom values then it should uses those values. not the values from the existing graph frame.
                    if (periodSettings == null || periodSettings.length != 2) {
                        customPeriods = graphFrame.graph.GDMgr.currentCustomPeriod;
                        customRange = graphFrame.graph.GDMgr.currentCustomRange;
                    }

                    if (intervalSettings == null || intervalSettings.length != 2) {
                        customIntervalType = graphFrame.graph.GDMgr.currentCustomIntervalType;
                        customIntervalFactor = graphFrame.graph.GDMgr.currentCustomIntervalFactor;

                    }
                    graphFrame.graph.GDMgr.setShowBidAskTags(objShowBidAskTags);
                    graphFrame.graph.GDMgr.setShowCurrentPriceLine(objShowLastLine);
                    graphFrame.graph.GDMgr.setShowOrderLines(objShowOrderLines);
                    graphFrame.graph.GDMgr.setShowMinMaxPriceLines(objShowMinMaxLines);
                    graphFrame.graph.GDMgr.setVWAP(objShowVWAP);
                }

                //saves the selected values in the xml file
                cs.setProperties(isCurrentMode, intPeriod, longInterval,
                        objShowLastLine, objShowPrvCloseLine, objShowOrderLines, objShowBidAskTags,
                        objShowMinMaxLines, cp, customPeriods, customRange, objOpenInProCharts,
                        customIntervalType, customIntervalFactor, objShowVWAP);
            }
        });

        bg.add(rbIntraDay);
        bg.add(rbHistory);
        pnlIntraday.add(rbIntraDay);
        pnlIntraday.add(lblPeriod);
        pnlIntraday.add(cmbPeriod);

        pnlIntraday.add(lblInterval);
        pnlIntraday.add(cmbInterval);
        pnlHistory.add(rbHistory);
        pnlHistory.add(lblPeriod2);
        pnlHistory.add(cmbPeriod2);

        pnlHistory.add(lblInterval2);
        pnlHistory.add(cmbInterval2);
        pnl.add(pnlIntraday);
        pnl.add(pnlHistory);
        tabbedPane.addTab(Language.getString("GENERAL"), pnl, Language.getString("SET_DEFAULT_PROPERTIES"));
        tabbedPane.addTab(Language.getString("BASE_CHART_PROPERTIES"), panel, Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("OTHER_CONFIG"), otherPanel, Language.getString("OTHER_CONFIG_HINT"));
        pnlDlg.add(tabbedPane);
        return cdlg;
    }

    public static CustomDialog getCreateLayoutDialog(JInternalFrame[] graphFrames) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SELECT_CHARTS"));

        JPanel pnl = cdlg.getPanel();
        String[] PanelWidths = {"45%", "10%", "45%"};
        String[] PanelHeights = {"100%"};
        pnl.setLayout(new FlexGridLayout(PanelWidths, PanelHeights, 5, 5));

        //pnl.setLayout(new BorderLayout(4, 4));
        JPanel topPanel = new JPanel(null);
        topPanel.setPreferredSize(new Dimension(660, 20));
        //JPanel centerPanel = new JPanel(null);
        JPanel centerPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"150", "43", "100%"}, 0, 0));
        centerPanel.setPreferredSize(new Dimension(60, 350));

        Point point = new Point();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = ge.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = device.getDefaultConfiguration();
        int xLocation = graphicsConfiguration.getBounds().width / 2 - 330;
        int yLocation = graphicsConfiguration.getBounds().height / 2 - 185;
        point.setLocation(xLocation, yLocation);
        cdlg.setLocation(point);

        final Vector<JInternalFrame> availableFrames = new Vector<JInternalFrame>();
        final Vector<JInternalFrame> selectedFrames = new Vector<JInternalFrame>();
        for (int i = 0; i < graphFrames.length; i++) {
            if (!graphFrames[i].toString().trim().equals(""))
                availableFrames.add(graphFrames[i]);
        }
        final JList lstAvailable = new JList(availableFrames);
        final JList lstSelected = new JList(selectedFrames);
        lstAvailable.setFont(new TWFont("Arial", 0, 12));
        lstSelected.setFont(new TWFont("Arial", 0, 12));

        GraphLabel lblAvailableCharts = new GraphLabel(Language.getString("AVAILABLE_CHARTS"));
        //lblAvailableCharts.setBounds(LEFT, GAP, 300, CMB_HEIGHT);
        lstAvailable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstAvailable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if ((e.getClickCount() > 1) && (lstAvailable.getSelectedIndex() >= 0)
                        && (availableFrames.size() > lstAvailable.getSelectedIndex())) {
                    JInternalFrame frame = availableFrames.remove(lstAvailable.getSelectedIndex());
                    selectedFrames.add(frame);
                    lstAvailable.updateUI();
                    lstSelected.updateUI();
                }
            }
        });


        JScrollPane spAvailable = new JScrollPane(lstAvailable);
        spAvailable.setPreferredSize(new Dimension(300, 350));
        spAvailable.setBorder(BorderFactory.createTitledBorder(Language.getString("SELECTED_CHARTS")));
        spAvailable.setAutoscrolls(true);

        GraphLabel lblSelectedCharts = new GraphLabel(Language.getString("SELECTED_CHARTS"));
        //lblSelectedCharts.setBounds(LEFT + 370, GAP, 300, CMB_HEIGHT);
        lstSelected.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lstSelected.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if ((e.getClickCount() > 1) && (lstSelected.getSelectedIndex() >= 0)
                        && (selectedFrames.size() > lstSelected.getSelectedIndex())) {
                    JInternalFrame frame = selectedFrames.remove(lstSelected.getSelectedIndex());
                    availableFrames.add(frame);
                    lstAvailable.updateUI();
                    lstSelected.updateUI();
                }
            }
        });
        JScrollPane spSelected = new JScrollPane(lstSelected);
        spSelected.setBorder(BorderFactory.createTitledBorder(Language.getString("AVAILABLE_CHARTS")));
        spSelected.setPreferredSize(new Dimension(300, 350));
        //spSelected.setLocation(LEFT+260,GAP+30);
        spSelected.setAutoscrolls(true);

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cdlg.result = selectedFrames;
            }
        });

        JButton btnAdd = new JButton(Language.getString("LAYOUT_DIALOG_>"));
        //btnAdd.setBounds(LEFT, 130, 50, 40);
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((lstAvailable.getSelectedIndex() >= 0) && (availableFrames.size() > lstAvailable.getSelectedIndex())) {
                    JInternalFrame frame = availableFrames.remove(lstAvailable.getSelectedIndex());
                    selectedFrames.add(frame);
                    lstAvailable.updateUI();
                    lstSelected.updateUI();
                }
            }
        });
        JButton btnRemove = new JButton(Language.getString("LAYOUT_DIALOG_<"));
        //btnRemove.setBounds(LEFT, 175, 50, 40);
        btnRemove.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((lstSelected.getSelectedIndex() >= 0) && (selectedFrames.size() > lstSelected.getSelectedIndex())) {
                    JInternalFrame frame = selectedFrames.remove(lstSelected.getSelectedIndex());
                    availableFrames.add(frame);
                    lstAvailable.updateUI();
                    lstSelected.updateUI();
                }
            }
        });


        JPanel buttonPnl = new JPanel(new FlexGridLayout(new String[]{"5%", "90%", "5%"}, new String[]{"20", "3", "20"}, 0, 0));
        buttonPnl.add(new JLabel(""));
        buttonPnl.add(btnAdd);
        buttonPnl.add(new JLabel(""));

        buttonPnl.add(new JLabel(""));
        buttonPnl.add(new JLabel(""));
        buttonPnl.add(new JLabel(""));

        buttonPnl.add(new JLabel(""));
        buttonPnl.add(btnRemove);
        buttonPnl.add(new JLabel(""));

        topPanel.add(lblAvailableCharts);
        topPanel.add(lblSelectedCharts);
        centerPanel.add(new JLabel(""));
        centerPanel.add(buttonPnl);
        centerPanel.add(new JLabel(""));

        pnl.add(spAvailable);
        pnl.add(centerPanel);
        pnl.add(spSelected);
        return cdlg;
    }


    // Added - Pramoda
    public static CustomDialog getCustomTimePeriodsDialog(int period, int range, boolean isIntraday) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SET_CUSTOM_TIME_PERIODS"));
        final int[] periodSettings = new int[2];

        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(285, 40));
        String[] PanelWidths = {"10", "90", "10", "75", "10", "75", "10"};
        String[] PanelHeights = {"10", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        pnl.setLayout(flexGridLayout);

        JLabel lblCustomPeriods = new JLabel(Language.getString("CUSTOM_TIME_PERIOD"));
        lblCustomPeriods.setBounds(new Rectangle(10, 10, 250, 20));
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblCustomPeriods);
        pnl.add(new JLabel());
//        range = (range > 0) ? range: 60;
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(range, 1, Integer.MAX_VALUE, 1);
        final JSpinner rangeField = new JSpinner(spinnerNumberModel);
        rangeField.setBounds(new Rectangle(10, 35, 110, 30));
        pnl.add(rangeField);

        final JComboBox periodType = new JComboBox();
        if (isIntraday) {
            periodType.addItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_HOURS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_DAYS"));
        } else {
            periodType.addItem(Language.getString("CUSTOM_PERIOD_DAYS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
            periodType.addItem(Language.getString("CUSTOM_PERIOD_YEARS"));
        }
        periodType.setBounds(new Rectangle(130, 35, 110, 30));
        pnl.add(new JLabel());
        pnl.add(periodType);
        pnl.add(new JLabel());


        switch (period) {
            case Calendar.MINUTE:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
                break;
            case Calendar.HOUR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_HOURS"));
                break;
            case Calendar.DAY_OF_YEAR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
                break;
            case Calendar.MONTH:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
                break;
            case Calendar.YEAR:
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_YEARS"));
                break;
            default:
                if (isIntraday) {
                    periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
                } else {
                    periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
                }
                break;
        }

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedItem = (String) periodType.getSelectedItem();
                if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MINUTES"))) {
                    periodSettings[0] = Calendar.MINUTE;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_HOURS"))) {
                    periodSettings[0] = Calendar.HOUR;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_DAYS"))) {
                    periodSettings[0] = Calendar.DAY_OF_YEAR;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MONTHS"))) {
                    periodSettings[0] = Calendar.MONTH;
                } else {
                    periodSettings[0] = Calendar.YEAR;
                }
                try {
                    periodSettings[1] = ((SpinnerNumberModel) rangeField.getModel()).getNumber().intValue();
                    cdlg.result = periodSettings;
                } catch (NumberFormatException e1) {
                    cdlg.result = null;
                }
            }
        });

//        cdlg.btnCancel.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                cdlg.result = null;
//            }
//        });

        GUISettings.applyOrientation(cdlg);
        return cdlg;
    }


    public static CustomDialog getCustomIntervalsDialog(long intervalType, long factor, boolean isIntraday) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SET_CUSTOM_INTERVALS"));
        final long[] periodSettings = new long[2];

        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(285, 40));
        String[] PanelWidths = {"5", "100", "5", "75", "10", "75", "10"};
        String[] PanelHeights = {"10", "20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        pnl.setLayout(flexGridLayout);

        JLabel lblCustomPeriods = new JLabel(Language.getString("CUSTOM_INTERVALS"));
        lblCustomPeriods.setBounds(new Rectangle(10, 10, 250, 20));
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(new JLabel());
        pnl.add(lblCustomPeriods);
        pnl.add(new JLabel());
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(factor, 1, Long.MAX_VALUE, 1);
        final JSpinner rangeField = new JSpinner(spinnerNumberModel);
        rangeField.setBounds(new Rectangle(10, 35, 110, 30));
        pnl.add(rangeField);

        final JComboBox periodType = new JComboBox();
        if (isIntraday) {
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_MINUTES"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_HOURS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_DAYS"));

            //these doesnt need in intraday mode

            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_MONTHS"));
            //periodType.addItem(Language.getString("CUSTOM_INTERVAL_YEARS"));
        } else {
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_DAYS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_MONTHS"));
            periodType.addItem(Language.getString("CUSTOM_INTERVAL_YEARS"));
        }
        periodType.setBounds(new Rectangle(130, 35, 110, 30));
        pnl.add(new JLabel());
        pnl.add(periodType);
        pnl.add(new JLabel());


        if (intervalType == 60L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
        } else if (intervalType == 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_HOURS"));
        } else if (intervalType == 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
        } else if (intervalType == 7 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_INTERVAL_WEEKS"));
        } else if (intervalType == 30 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MONTHS"));
        } else if (intervalType == 365 * 24 * 3600L) {
            periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_YEARS"));
        } else {
            if (isIntraday) {
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_MINUTES"));
            } else {
                periodType.setSelectedItem(Language.getString("CUSTOM_PERIOD_DAYS"));
            }
        }

        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedItem = (String) periodType.getSelectedItem();
                if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MINUTES"))) {
                    periodSettings[0] = 60L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_HOURS"))) {
                    periodSettings[0] = 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_DAYS"))) {
                    periodSettings[0] = 24 * 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_INTERVAL_WEEKS"))) {
                    periodSettings[0] = 7 * 24 * 3600L;
                } else if (selectedItem.equals(Language.getString("CUSTOM_PERIOD_MONTHS"))) {
                    periodSettings[0] = 30 * 24 * 3600L;
                } else {
                    periodSettings[0] = 365 * 24 * 3600L;
                }
                try {
                    periodSettings[1] = ((SpinnerNumberModel) rangeField.getModel()).getNumber().intValue();
                    cdlg.result = periodSettings;
                    ChartSettings.getSettings().setCustomIntervalType(periodSettings[0]);
                } catch (NumberFormatException e1) {
                    cdlg.result = null;
                }
            }
        });
        GUISettings.applyOrientation(cdlg);
        return cdlg;
    }


    //Added - Pramoda
    public static CustomDialog getInsertIndicatorsDialog(String[] indicators, final HashMap<String, Byte> indicatorMap,
                                                         final ArrayList currentlyAvailableCharts, final boolean isVolumeShown,
                                                         final WindowPanel volumeRect) {
        final CustomDialog cdlg = new CustomDialog();
        cdlg.setTitle(Language.getString("SELECT_INDICATORS"));
        final byte[] indicatorSelection = new byte[3];
        indicatorSelection[2] = 0;

        JPanel pnl = cdlg.getPanel();
        pnl.setPreferredSize(new Dimension(625, 400));
        pnl.setLayout(null);
        JPanel topPanel = new JPanel(null);
        topPanel.setBounds(new Rectangle(0, 0, 625, 20));

        Point point = new Point();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = ge.getDefaultScreenDevice();
        GraphicsConfiguration graphicsConfiguration = device.getDefaultConfiguration();
        int xLocation = graphicsConfiguration.getBounds().width / 2 - 310;
        int yLocation = graphicsConfiguration.getBounds().height / 2 - 200;
        point.setLocation(xLocation, yLocation);
        cdlg.setLocation(point);

        final JList lstAvailable = new JList(indicators);
        final Vector<ChartProperties> allowedCurves = new Vector<ChartProperties>();
        final JList lstSelected = new JList(allowedCurves);

        final JRadioButton onNewWindow = new JRadioButton(Language.getString("INDICATOR_ON_NEW_WINDOW"));
        onNewWindow.setBounds(new Rectangle(LEFT, 380, 300, 20));
        final JRadioButton onSameWindow = new JRadioButton(Language.getString("INDICATOR_ON_SAME_WINDOW"));
        onSameWindow.setBounds(new Rectangle(320, 380, 300, 20));
        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(onNewWindow);
        btnGroup.add(onSameWindow);
        onSameWindow.setSelected(true);
        onNewWindow.setEnabled(false);
        onSameWindow.setEnabled(false);

        GraphLabel lblAvailableCharts = new GraphLabel(Language.getString("SELECT_INDICATORS"));
        lblAvailableCharts.setBounds(LEFT, GAP, 300, CMB_HEIGHT);
        lstAvailable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//        lstAvailable.setSelectedIndex(0);
        final boolean[] diOnDi = new boolean[]{false};

        lstAvailable.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                String indicatorName = (String) lstAvailable.getSelectedValue();
                byte id = indicatorMap.get(indicatorName);
                if (id < GraphDataManager.ID_MULTI_HIGH && id > GraphDataManager.ID_MULTI_LOW) {
                    allowedCurves.clear();
                    allowedCurves.add((ChartProperties) currentlyAvailableCharts.get(0));
                    lstSelected.updateUI();
                    switch (id) {
                        case GraphDataManager.ID_MEDIAN_PRICE:
                        case GraphDataManager.ID_PARABOLIC_SAR:
                        case GraphDataManager.ID_TYPICAL_PRICE:
                        case GraphDataManager.ID_WEIGHTED_CLOSE:
                        case GraphDataManager.ID_PRICE_CHANNELS:
                            onSameWindow.setSelected(true);
                            onNewWindow.setSelected(false);
                            onNewWindow.setEnabled(true);
                            onSameWindow.setEnabled(true);
                            break;
                        default:
                            onSameWindow.setSelected(false);
                            onNewWindow.setSelected(true);
                            onNewWindow.setEnabled(false);
                            onSameWindow.setEnabled(false);
                            break;
                    }
                } else {
                    onNewWindow.setEnabled(true);
                    onSameWindow.setEnabled(true);
                    switch (id) {
                        case GraphDataManager.ID_BOLLINGER_BANDS:
                        case GraphDataManager.ID_DEMA:
                        case GraphDataManager.ID_COPP_CURVE:
                        case GraphDataManager.ID_ENVILOPES:
                        case GraphDataManager.ID_LRI:
                        case GraphDataManager.ID_MOVING_AVERAGE:
                        case GraphDataManager.ID_TEMA:
                        case GraphDataManager.ID_TSF:
                        case GraphDataManager.ID_WILDERS_SMOOTHING:
                        case GraphDataManager.ID_ZIGZAG:
                            onNewWindow.setEnabled(true);
                            onSameWindow.setEnabled(true);
                            onSameWindow.setSelected(true);
                            onNewWindow.setSelected(false);
                            allowedCurves.clear();
                            for (Object currentlyAvailableChart : currentlyAvailableCharts) {
                                ChartProperties cp = (ChartProperties) currentlyAvailableChart;
                                if (isVolumeShown || cp.getRect() != volumeRect) {
                                    allowedCurves.add(cp);
                                }
                            }
                            lstSelected.updateUI();
                            break;
                        case GraphDataManager.ID_CMO:
//                        case GraphDataManager.ID_DMI:
                        case GraphDataManager.ID_FO:
//                        case GraphDataManager.ID_INERTIA:
//                        case GraphDataManager.ID_LRS:
//                        case GraphDataManager.ID_R_SQUARED:
                        case GraphDataManager.ID_RMI:
                        case GraphDataManager.ID_RSI:
                        case GraphDataManager.ID_SDI:
//                        case GraphDataManager.ID_STD_ERR_BANDS:
                        case GraphDataManager.ID_TRIX:
                        case GraphDataManager.ID_VERT_HORI_FILTER:
                            onNewWindow.setEnabled(false);
                            onSameWindow.setEnabled(false);
                            onSameWindow.setSelected(false);
                            onNewWindow.setSelected(true);
                            allowedCurves.clear();
                            for (Object currentlyAvailableChart : currentlyAvailableCharts) {
                                ChartProperties cp = (ChartProperties) currentlyAvailableChart;
                                if (isVolumeShown || cp.getRect() != volumeRect) {
                                    allowedCurves.add(cp);
                                }
                            }
                            lstSelected.updateUI();
                            break;
                        default:
                            onNewWindow.setEnabled(false);
                            onSameWindow.setEnabled(false);
                            allowedCurves.clear();
                            allowedCurves.add((ChartProperties) currentlyAvailableCharts.get(0));
                            lstSelected.updateUI();
                            break;
                    }
                }
                //TODO:
                for (Object currentlyAvailableChart : currentlyAvailableCharts) {
                    ChartProperties cp = (ChartProperties) currentlyAvailableChart;
                    if ((cp.getID() == GraphDataManager.ID_PLUS_DI && id == GraphDataManager.ID_MINUS_DI) ||
                            (cp.getID() == GraphDataManager.ID_MINUS_DI && id == GraphDataManager.ID_PLUS_DI)) {
                        onNewWindow.setEnabled(true);
                        onNewWindow.setSelected(true);
                        onSameWindow.setText(Language.getString("INDICATOR_ON_DI_WINDOW"));
                        onSameWindow.setEnabled(true);
                        diOnDi[0] = true;
                    } else {
                        onSameWindow.setText(Language.getString("INDICATOR_ON_SAME_WINDOW"));
                    }
                }
            }
        });

        JScrollPane spAvailable = new JScrollPane(lstAvailable);
        spAvailable.setBounds(new Rectangle(LEFT, 30, 300, 350));
        spAvailable.setAutoscrolls(true);

        GraphLabel lblSelectedCharts = new GraphLabel(Language.getString("INNER_WINDOWS"));
        lblSelectedCharts.setBounds(LEFT + 315, GAP, 300, CMB_HEIGHT);
        lstSelected.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane spSelected = new JScrollPane(lstSelected);
        spSelected.setBounds(new Rectangle(320, 30, 300, 350));
        spSelected.setAutoscrolls(true);


        cdlg.btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    indicatorSelection[0] = indicatorMap.get(lstAvailable.getSelectedValue());
                    indicatorSelection[1] =
                            (byte) currentlyAvailableCharts.indexOf(lstSelected.getSelectedValue());
                    if (onNewWindow.isEnabled() && onSameWindow.isEnabled() && onNewWindow.isSelected()) {
                        indicatorSelection[2] = 1;
                    } else if (onNewWindow.isEnabled() && onSameWindow.isEnabled() && onSameWindow.isSelected()) {
                        indicatorSelection[2] = 2;
                    }
                    if (diOnDi[0]) {
                        if (onNewWindow.isEnabled() && onSameWindow.isEnabled() && onNewWindow.isSelected()) {
                            indicatorSelection[2] = 1;
                        } else if (onNewWindow.isEnabled() && onSameWindow.isEnabled() && onSameWindow.isSelected()) {
                            indicatorSelection[2] = 4;
                        }
                    }

                    cdlg.result = indicatorSelection;
                } catch (Exception e1) {
                    cdlg.result = null;
                }
            }
        });

        topPanel.add(lblAvailableCharts);
        topPanel.add(lblSelectedCharts);
        pnl.add(topPanel);
        pnl.add(spAvailable);
        pnl.add(spSelected);
        pnl.add(onNewWindow);
        pnl.add(onSameWindow);
        return cdlg;
    }
}


class CustomDialog extends JDialog implements ActionListener {
    public TWButton btnOK = new TWButton(Language.getString("OK"));
    public TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
    public Object result;
    private boolean ModalResult = false;
    private JPanel bottomPanel = new JPanel();
    private JPanel centerPanel = new JPanel();

    public CustomDialog() throws HeadlessException {
        //super(Client.getInstance().getFrame());
        super(ChartInterface.getChartFrameParentComponent());
        initiallizeUI();
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        this.setSize(PropertyDialogFactory.DIM_DIALOG);
        //this.setResizable(true);
    }

    private void initiallizeUI() {
        btnOK.addActionListener(this);
        btnCancel.addActionListener(this);
        String[] PanelWidths = {"100%", "75", "10", "75", "10"};
        String[] PanelHeights = {"20"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 1, 1);
        bottomPanel.setLayout(flexGridLayout);
        bottomPanel.setPreferredSize(new Dimension(0, 30));
        bottomPanel.add(new JLabel());
        bottomPanel.add(btnOK);
        bottomPanel.add(new JLabel());
        bottomPanel.add(btnCancel);
        bottomPanel.add(new JLabel());
//        String[] PanelWidths = {"100%"};
//        String[] PanelHeights = {"100%","30"};
//
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(centerPanel, BorderLayout.CENTER);
        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
        ModalResult = false;
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        ModalResult = false;
        if (obj == btnOK) {
            ModalResult = true;
            this.setVisible(false);
        } else if (obj == btnCancel) {
            this.setVisible(false);
        }
    }

    public JPanel getPanel() {
        return centerPanel;
    }

    public boolean getModalResult() {
        return ModalResult;
    }

}