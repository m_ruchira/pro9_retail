package com.isi.csvr.chart;

import java.awt.*;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Sep 6, 2008
 * Time: 9:03:43 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DataWindowTableListener {

    public void dataPointChangedOnGraph(MouseEvent e, Point CurrentMousePos, StockGraph graph);

}
