package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 27, 2006
 * Time: 11:57:52 AM
 */
public class DetrendPriceOscillator extends ChartProperties implements Indicator {

    private static final long serialVersionUID = UID_DPO;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public DetrendPriceOscillator(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_DPO") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_DPO");
        this.timePeriods = 20;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte stepMA,
                                          byte srcIndex, byte destIndex) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crNew;

        // steps involved

        MovingAverage.getMovingAverage(al, bIndex, t_im_ePeriods, MovingAverage.METHOD_SIMPLE, srcIndex, stepMA);

        int step = t_im_ePeriods / 2 + 1;
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((i + step >= bIndex + t_im_ePeriods - 1) && (i + step < al.size())) { //todo supicious
                crNew = (ChartRecord) al.get(i + step);

                cr.setValue(destIndex, cr.getStepValue(srcIndex) - crNew.getStepValue(stepMA));

            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        // System.out.println("**** DPO Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 1;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 20;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof DetrendPriceOscillator) {
            DetrendPriceOscillator dpo = (DetrendPriceOscillator) cp;
            this.timePeriods = dpo.timePeriods;
            this.innerSource = dpo.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_DPO") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }
    //############################################

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this has one intermediate step: MA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crNew;
        //setting step size 1
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(1);
        }
        // steps involved
        byte stepMA = ChartRecord.STEP_1;

        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_SIMPLE, this.getOHLCPriority(), stepMA);

        int step = timePeriods / 2 + 1;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((i + step >= timePeriods - 1) && (i + step < al.size())) {
                crNew = (ChartRecord) al.get(i + step);
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(cr.getValue(this.getOHLCPriority()) - crNew.getStepValue(stepMA));
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }

        //System.out.println("**** DPO Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_DPO");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

}
