package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorEnvelopesProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 7:19:33 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class Envelope extends ChartProperties implements Indicator, Serializable, FillArea {
    public final static byte ENVELOPE_BOTTOM = 0;
    public final static byte ENVELOPE_TOP = 1;
    private static final long serialVersionUID = UID_ENVILOPES;
    protected byte method;
    protected float shift = 0.03f;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private byte style;
    private boolean fillBands;
    private Color fillColor;
    private int transparency;
    private String tableColumnHeading;

    public Envelope(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_ENVILOPES") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_ENVILOPES");
        this.timePeriods = 25;
        method = MovingAverage.METHOD_SIMPLE;
        style = Envelope.ENVELOPE_TOP;

        //fill bands related
        fillBands = true;
        fillColor = Color.blue;
        transparency = 30;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorEnvelopesProperty idp = (IndicatorEnvelopesProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getParam1());
            this.setMethod(idp.getParam2());

            this.setFillBands(idp.isFillBands());
            this.setFillColor(idp.getFillColor());
            this.setTransparency(idp.getTransparency());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_i_mePeriods, byte stepRes1, byte stepRes2, byte stepMA, byte style,
                                          byte shift, byte method, byte srcIndex, byte destIndex) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr;

        int loopBegin = bIndex + t_i_mePeriods - 1;
        if (loopBegin > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }
        float factor = (style == Envelope.ENVELOPE_TOP) ? 1 + shift : 1 - shift;


        /* // steps involved
     byte stepRes1 = ChartRecord.STEP_1;
     byte stepRes2 = ChartRecord.STEP_2;
     byte stepMA = ChartRecord.STEP_3;*/

        MovingAverage.getVariableMovingAverage(al, 0, t_i_mePeriods, method, GDM, stepRes1, stepRes2, srcIndex, stepMA);

        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(stepMA);
            boolean isNull = (method == MovingAverage.METHOD_VOLUME_ADJUSTED) ? val == Indicator.NULL : (val == Indicator.NULL) || (i < loopBegin);
            if (isNull) {
                cr.setValue(destIndex, Indicator.NULL);
            } else {
                /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(val * factor);*/
                cr.setValue(destIndex, val * factor);
            }
        }
        //System.out.println("**** Envelope Calc time " + (entryTime - System.currentTimeMillis()));

    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 25;
        this.method = MovingAverage.METHOD_SIMPLE;
        this.fillBands = true;
        this.fillColor = Color.blue;
        this.transparency = 30;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.style = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/EnvelopeStyle"));

        this.fillBands = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/fillBands"));
        this.transparency = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/transparency"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/fillColor").split(",");
        this.fillColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "EnvelopeStyle", Byte.toString(this.style));

        TemplateFactory.saveProperty(chart, document, "fillBands", Boolean.toString(this.fillBands));
        TemplateFactory.saveProperty(chart, document, "transparency", Integer.toString(this.transparency));
        String strColor = this.fillColor.getRed() + "," + this.fillColor.getGreen() + "," + this.fillColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "fillColor", strColor);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof Envelope) {
            Envelope env = (Envelope) cp;
            this.timePeriods = env.timePeriods;
            this.method = env.method;
            this.innerSource = env.innerSource;
            this.style = env.style;

            this.fillBands = env.fillBands;
            this.fillColor = env.fillColor;
            this.transparency = env.transparency;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += "(" + getMethodString() + ", " + timePeriods + ")";
        return Language.getString("IND_ENVILOPES") + " " + parent;
    }

    protected String getClose_Periods_MethodStr(boolean isIndicator) {

        return "(" + getPriorityStr(isIndicator) + ", " + timePeriods + ", "
                + getMethodString() + ")";
    }

    protected String getPriorityStr(boolean isIndicator) {
        if (isIndicator) {
            return Language.getString("INDICATOR");
        } else {
            switch (getOHLCPriority()) {
                case GraphDataManager.INNER_Open:
                    return Language.getString("OPEN");
                case GraphDataManager.INNER_High:
                    return Language.getString("HIGH");
                case GraphDataManager.INNER_Low:
                    return Language.getString("LOW");
                default:
                    return Language.getString("CLOSE");
            }
        }
    }

    protected String getMethodString() {
        switch (method) {
            case MovingAverage.METHOD_SIMPLE:
                return Language.getString("METHOD_SIMPLE");
            case MovingAverage.METHOD_WEIGHTED:
                return Language.getString("METHOD_WEIGHTED");
            case MovingAverage.METHOD_EXPONENTIAL:
                return Language.getString("METHOD_EXPONENTIAL");
            case MovingAverage.METHOD_VARIABLE:
                return Language.getString("METHOD_VARIABLE");
            case MovingAverage.METHOD_VOLUME_ADJUSTED:
                return Language.getString("METHOD_VOLUME_ADJUSTED");
            case MovingAverage.METHOD_TRANGULAR:
                return Language.getString("METHOD_TRANGULAR");
            case MovingAverage.METHOD_TIME_SERIES:
                return Language.getString("METHOD_TIME_SERIES");
            default:
                return Language.getString("METHOD_SIMPLE");
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //method
    public byte getMethod() {
        return method;
    }
    //############################################

    public void setMethod(byte tp) {
        method = tp;
    }

    // this has 3 intermediate steps: Reserved1 for MA, Reserved2 for MA and MA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods - 1;
        if (loopBegin > al.size()) return;
        float factor = (style == Envelope.ENVELOPE_TOP) ? 1 + shift : 1 - shift;

        ChartRecord cr;
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepRes1 = ChartRecord.STEP_1;
        byte stepRes2 = ChartRecord.STEP_2;
        byte stepMA = ChartRecord.STEP_3;

        MovingAverage.getVariableMovingAverage(al, 0, timePeriods, method, GDM, stepRes1, stepRes2, getOHLCPriority(), stepMA);

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(stepMA);
            boolean isNull = (method == MovingAverage.METHOD_VOLUME_ADJUSTED) ? val == Indicator.NULL : (val == Indicator.NULL) || (i < loopBegin);
            if (isNull) {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            } else {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null)
                    aPoint.setIndicatorValue(val * factor);
            }
        }
        //System.out.println("**** Envelope Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public String getShortName() {
        return Language.getString("IND_ENVILOPES");
    }

    public byte getStyle() {
        return style;
    }

    public void setStyle(byte style) {
        this.style = style;
    }

    //fill area related
    public boolean isFillBands() {
        return fillBands;
    }

    public void setFillBands(boolean fillBands) {
        this.fillBands = fillBands;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Color getFillTransparentColor() {
        return new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), transparency);
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    // To be used by custom indicators

}
