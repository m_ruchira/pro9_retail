package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.DynamicArray;
import org.w3c.dom.*;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * User: Udaka
 * Date: Apr 4, 2006
 * Time: 12:46:22 AM
 */
public class LayoutFactory {

    public static boolean LoadLayout(File layoutFile, Vector<JInternalFrame> selectedFrames, boolean isDetached,
                                     GraphFrame existingGraphFrame, boolean isTempFile) {
        try {
            // Modified 29-10-2007
            load(layoutFile, selectedFrames, isDetached, existingGraphFrame, true, null, layoutFile.getName(), isTempFile);
            ChartFrame.getSharedInstance().tileWindows();

            if (ChartFrame.getSharedInstance().getDesktop().getAllFrames().length > 0 &&
                    ChartFrame.getSharedInstance().getDesktop().getAllFrames()[0] instanceof GraphFrame) {
                ((GraphFrame) ChartFrame.getSharedInstance().getDesktop().getAllFrames()[0]).setSelected(true);
                ChartFrame.getSharedInstance().setActiveGraph((GraphFrame) ChartFrame.getSharedInstance().getDesktop().getAllFrames()[0]);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }


    public static boolean openChtFile(String chtFileName, Vector<JInternalFrame> selectedFrames, boolean isDetached,
                                      GraphFrame existingGraphFrame) {
        int bufferLength = 2048;
        File layoutFile = null;
        Hashtable<String, File> dataFiles = new Hashtable<String, File>();
        try {
            String path = chtFileName.substring(0, chtFileName.lastIndexOf(File.separator));
            FileInputStream fis = new FileInputStream(chtFileName);
            ZipInputStream zis = new ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                System.out.println("Extracting: " + entry);
                int count;
                byte data[] = new byte[bufferLength];
                // write the files to the disk
                FileOutputStream fos = new FileOutputStream(path + File.separator + entry.getName());
                BufferedOutputStream dest = new BufferedOutputStream(fos, bufferLength);
                while ((count = zis.read(data, 0, bufferLength)) > -1) {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
                fos.close();
                if (entry.getName().endsWith(".mcl")) {
                    layoutFile = new File(path + File.separator + entry.getName());
                } else if (entry.getName().endsWith(".dat")) {
                    File dataFile = new File(path + File.separator + entry.getName());
                    //String key = entry.getName().substring(0, entry.getName().indexOf("."));
                    String key = entry.getName().split(".dat")[0];
                    dataFiles.put(key, dataFile);
                }
            }
            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        try {
            load(layoutFile, selectedFrames, isDetached, existingGraphFrame, false, dataFiles, chtFileName, false);

            layoutFile.delete();
            Collection<File> files = dataFiles.values();
            for (File file : files) {
                file.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        //cFrame.tileWindows();
        return true;
    }

    public static String getAttribute(NamedNodeMap attributes, String tagName) {
        String val = "-1";
        try {
            val = attributes.getNamedItem(tagName).getNodeValue();
        } catch (Exception e) {

        }
        return val;
    }

    private static void load(File layoutFile, Vector<JInternalFrame> selectedFrames, boolean isDetached,
                             GraphFrame existingGraphFrame, boolean isLayout,
                             Hashtable<String, File> dataFiles, String chtFileName, boolean isTempFile) throws Exception {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        FileInputStream is = new FileInputStream(layoutFile);
        Document document = builder.parse(is);

        XPath xpath = XPathFactory.newInstance().newXPath();

        String expression = "Layout/ChartFrame";
        boolean isShowSnapBall = false;
        boolean isShowCrossHair = false;
        Node chartFrame = null;
        try {
            chartFrame = (Node) xpath.evaluate(expression, document, XPathConstants.NODE);
        } catch (Exception ex) {
            // do nothing
        } finally {
            if (chartFrame != null) {
                try {
                    NamedNodeMap chartFrameAttributes = chartFrame.getAttributes();
                    isShowSnapBall = Boolean.parseBoolean(
                            chartFrameAttributes.getNamedItem("isShowSnapBall").getNodeValue());
                    isShowCrossHair = Boolean.parseBoolean(
                            chartFrameAttributes.getNamedItem("isShowCrossHairs").getNodeValue());
                } catch (NullPointerException ex) { // ignore - backward compatibility}
                }
            }
        }

        GraphFrame gf;

        ChartFrame.getSharedInstance().setShowSnapBall(isShowSnapBall);
        ChartFrame.getSharedInstance().setShowCrossHairs(isShowCrossHair);

        expression = "Layout/GraphWindow";

        // First, obtain the element as a node.
        NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);

        if ((graphWindows != null) && (graphWindows.getLength() > 0)) {

            for (int i = 0; i < graphWindows.getLength(); i++) {
                Node graphWindow = graphWindows.item(i);
                NamedNodeMap attributes = graphWindow.getAttributes();
                boolean isCurrentMode = Boolean.parseBoolean(attributes.getNamedItem("Mode").getNodeValue());
                int period = Integer.parseInt(attributes.getNamedItem("Period").getNodeValue());
                long interval = Long.parseLong(attributes.getNamedItem("Interval").getNodeValue());
                String[] saBounds = attributes.getNamedItem("Bounds").getNodeValue().split(",");
                //System.out.println("mode, period, interval = " + isCurrentMode + ", " + period + ", " + interval);

                //GraphFrame gf;
                if (!isDetached) {
                    gf = ChartFrame.getSharedInstance().addNewGraph(false, true);
                } else {
                    gf = existingGraphFrame;
                }
                StockGraph graph = gf.graph;
                gf.CurrentMode = isCurrentMode;
                graph.setCurrentMode(isCurrentMode);
                graph.setPeriodFlag(period);
                graph.setIntervalFlag(interval);

                boolean isShowCurrentPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowCurrentPriceLine"));
                boolean isShowBidAskTags = Boolean.parseBoolean(getAttribute(attributes, "isShowBidAskTags"));
                boolean isShowOrderLines = Boolean.parseBoolean(getAttribute(attributes, "isShowOrderLines"));
                boolean isShowMinMaxPrices = Boolean.parseBoolean(getAttribute(attributes, "isShowMinMaxLines"));
                boolean bIsShowVolume = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeInProchart"));
                boolean bIsShowTurnOver = Boolean.parseBoolean(getAttribute(attributes, "isShowTurnOverInProChart"));
                boolean bIsShowAnnouncecment = Boolean.parseBoolean(getAttribute(attributes, "isShowAnnouncements"));
                boolean bIsShowSplits = Boolean.parseBoolean(getAttribute(attributes, "isShowSplits"));
                boolean bIsShowVolByPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeByPrice"));
                byte volByPrice = Byte.parseByte(getAttribute(attributes, "showVolumeByPrice"));
                byte crossHairType = Byte.parseByte(getAttribute(attributes, "crossHairType"));
                boolean isshowPreviousCloseLine = Boolean.parseBoolean(getAttribute(attributes, "isShowPreviousCloseLine"));
                boolean isTableMode = Boolean.parseBoolean(getAttribute(attributes, "isTableMode"));
                int rMarginHistory = Integer.parseInt(getAttribute(attributes, "rightMarginHistory"));
                int rMarginIntraday = Integer.parseInt(getAttribute(attributes, "rightMarginIntraday"));

                graph.GDMgr.setShowCurrentPriceLine(isShowCurrentPrice);
                graph.GDMgr.setShowBidAskTags(isShowBidAskTags);
                graph.GDMgr.setShowOrderLines(isShowOrderLines);
                graph.GDMgr.setShowMinMaxPriceLines(isShowMinMaxPrices);
                graph.setVolumeGraphflag(bIsShowVolume);
                graph.setTurnOverGraphFlag(bIsShowTurnOver);
                graph.GDMgr.setShowAnnouncements(bIsShowAnnouncecment);
                graph.GDMgr.setShowSplits(bIsShowSplits);
                graph.GDMgr.setShowVolumeByPrice(bIsShowVolByPrice);
                graph.GDMgr.setShowPreviousCloseLine(isshowPreviousCloseLine);
                if (volByPrice > -1) graph.GDMgr.setVolumeByPriceType(volByPrice);
                if (crossHairType > -1) gf.graph.GDMgr.setCrossHairType(crossHairType);
                if (rMarginHistory > -1 && rMarginIntraday > -1)
                    gf.graph.GDMgr.setRightMargin(rMarginHistory, rMarginIntraday);

                //this will raise a exception in tab mode. so should be before the exception
                try {
                    String linkID = String.valueOf(attributes.getNamedItem("linkID").getNodeValue());
                    if (!(gf instanceof GraphFrameSymbolwise)) { //no need to read this propert for main board charts
                        gf.setLinkedGroupID(linkID);
                    }
                } catch (Exception e) {
                    System.out.println("++++++++ link ID +++++++++++ ");
                }

                if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
                    int customPeriod = Integer.parseInt(getAttribute(attributes, "periodType"));
                    int customrange = Integer.parseInt(getAttribute(attributes, "range"));

                    if (customPeriod > -1) graph.GDMgr.currentCustomPeriod = customPeriod;
                    if (customrange > -1) graph.GDMgr.currentCustomRange = customrange;
                }

                long nCustomIntervalType = Long.parseLong(getAttribute(attributes, "customIntervalType"));
                long ncustomIntervalFactor = Long.parseLong(getAttribute(attributes, "customIntervalFactor"));
                if (nCustomIntervalType > -1) graph.GDMgr.currentCustomIntervalType = nCustomIntervalType;
                if (ncustomIntervalFactor > -1) graph.GDMgr.currentCustomIntervalFactor = ncustomIntervalFactor;


                int graphIndex = (i + 1);
                expression = "Layout/GraphWindow[" + graphIndex + "]/Chart";
                NodeList charts = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                //System.out.println("Chart count: " + charts.getLength());
                if ((charts != null) && (charts.getLength() > 0)) {
                    Hashtable<String, String> htSources = new Hashtable<String, String>();
                    Hashtable htPanels = new Hashtable();
                    Hashtable<Integer, ChartProperties> indicators = new Hashtable<Integer, ChartProperties>();
                    ChartProperties srcCP = null;

                    //added to setGroupIDs for old layouts
                    ArrayList<ChartProperties> cpsWithEmptyGroupID = new ArrayList<ChartProperties>();
                    for (int j = 0; j < charts.getLength(); j++) {
                        Node chart = charts.item(j);
                        int chartIndex = (j + 1);
                        byte ID = Byte.parseByte(chart.getAttributes().getNamedItem("ID").getNodeValue());
                        htSources.put(chart.getAttributes().getNamedItem("SourceID").getNodeValue(), Integer.toString(graph.GDMgr.getSources().size()));
                        if (ID == GraphDataManager.ID_BASE) {
                            //add base symbol
                            String symbol = chart.getAttributes().getNamedItem("Symbol").getNodeValue();
                            ChartProperties cp;
                            if (isLayout) {
                                cp = gf.setBaseSymbol(symbol);
                            } else {
                                cp = gf.setBaseSymbol(symbol, dataFiles.get(symbol));
                            }
                            cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                            gf.setBasicPropertiesFromCP();
                            htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(), Integer.toString(graph.getIDforPanel(cp.getRect())));
                        } else if (ID == GraphDataManager.ID_COMPARE) {
                            String symbol = chart.getAttributes().getNamedItem("Symbol").getNodeValue();
                            ChartProperties cp;
                            if (isLayout) {
                                cp = gf.setComparisonSymbol(symbol);
                            } else {
                                cp = gf.setComparisonSymbol(symbol, dataFiles.get(symbol));
                            }
                            cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                        } else if (ID == GraphDataManager.ID_VOLUME) {
                            ChartProperties cp = graph.GDMgr.getVolumeCP();
                            cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                            htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(), Integer.toString(graph.getIDforPanel(cp.getRect())));
                        } else if (ID == GraphDataManager.ID_TURNOVER) {
                            ChartProperties cp = graph.GDMgr.getTurnOverCP();
                            cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                            htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(), Integer.toString(graph.getIDforPanel(cp.getRect())));
                        } else {
                            //add indicator
                            String innerSourceID = chart.getAttributes().getNamedItem("InnerSource").getNodeValue();
                            int mappedID = Integer.parseInt(htSources.get(innerSourceID).toString());
                            ChartProperties innerSource = (ChartProperties) graph.GDMgr.getSources().get(mappedID);
                            String strPnlID = chart.getAttributes().getNamedItem("PanelID").getNodeValue();//
                            WindowPanel r = null;
                            boolean isNewPanel = false;
                            if (htPanels.containsKey(strPnlID)) {
                                r = (WindowPanel) graph.panels.get(Integer.parseInt(htPanels.get(strPnlID).toString()));
                            } else {
                                r = new WindowPanel();
                                isNewPanel = true;
                            }
                            ChartProperties cp;
                            if (ID == GraphDataManager.ID_CUSTOM_INDICATOR) {
                                ChartProperties dummyCP = new ChartProperties(null, null, ID, null, r);
                                dummyCP.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                                String longName = dummyCP.longName;
                                IndicatorMenuObject indicatorMenuObject = null;
                                for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().customIndicators) {
                                    if (imo.getIndicatorName().equals(longName)) {
                                        indicatorMenuObject = imo;
                                    }
                                }
                                if (indicatorMenuObject == null && ChartInterface.isSystemStrategiesEnabled()) {
                                    for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alStrategies) {
                                        if (imo.getIndicatorName().equals(longName)) {
                                            indicatorMenuObject = imo;
                                        }
                                    }
                                }
                                if (indicatorMenuObject == null && ChartInterface.isSystemPatternsEnabled()) {
                                    for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alPatterns) {
                                        if (imo.getIndicatorName().equals(longName)) {
                                            indicatorMenuObject = imo;
                                        }
                                    }
                                }
                                if (indicatorMenuObject == null) {
                                    //MessageBox.Show(ResourceCollection.MSG_CANNOT_FIND_CUSTOM_INDICATOR + " '" + longName + "'");
                                    continue;
                                } else {
                                    cp = graph.GDMgr.addCustomIndicator(indicatorMenuObject.getIndicatorName(),
                                            indicatorMenuObject.getIndicatorClass(), true, innerSource, r);
                                }
                            } else {
                                cp = graph.GDMgr.addIndicator(ID, true, innerSource, r, false);
                            }
                            /*if (!htPanels.containsKey(strPnlID)) {
                                htPanels.put(strPnlID, Integer.toString(graph.getIDforPanel(r)));
                            }*/
                            cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", true);
                            indicators.put(graph.GDMgr.getSources().size() - 1, cp);

                            /*if (cp.isHidden()) {
                                gf.isHideAll = false;
                            }*/

                            if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                                //int indexCp = graph.GDMgr.getIndexOfTheRect(graph.panels, r);
                                if (TemplateFactory.allHidden(cp, graph)) {
                                    graph.deleteISPPanel(graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
                                } else {
                                    int pnlID = TemplateFactory.getPnlIDForHiddenCp(cp, graph);
                                    cp.setRect((WindowPanel) graph.panels.get(pnlID));
                                }

                                graph.reDrawAll();

                            } else {
                                if (!htPanels.containsKey(strPnlID)) {
                                    htPanels.put(strPnlID, Integer.toString(graph.getIDforPanel(r)));
                                }
                            }


                            if ((cp instanceof IndicatorBase) && ((IndicatorBase) cp).IsStrategy()) {
                                if (((IndicatorBase) cp).isDrawSymbol()) {
                                    ((IndicatorBase) cp).setTargetCP(srcCP);
                                } else {
                                    srcCP = cp;
                                }
                            }
                            try {
                                ((Indicator) cp).setTableColumnHeading(chart.getAttributes().
                                        getNamedItem("tableColumnHeading").getNodeValue());
                            } catch (Exception e) {
                                // Ignore for backwards compatibility
                            }
                            graph.GDMgr.addSignalLines(cp);
                            if (isNewPanel) {
                                //r.setTitle(cp.toString());
                                // Modified on 23-10-2007
                                String bg = StockGraph.extractSymbolFromStr(graph.GDMgr.getBaseGraph());
                                r.setTitle(cp.getShortName() + " (" + bg + ")");


                            }
                            if (cp.getGroupID().equals("")) {
                                cpsWithEmptyGroupID.add(cp);
                            }
                        }
                    }

                    // ADDED for fixing compatibility issues------------------------------------------------
                    ArrayList<Integer> panelIDs = new ArrayList<Integer>();
                    for (int k = 0; k < cpsWithEmptyGroupID.size(); k++) {
                        ChartProperties cp = cpsWithEmptyGroupID.get(k);
                        {
                            int panelID = graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect());
                            if (!panelIDs.contains(panelID)) {
                                panelIDs.add(panelID);
                            }
                        }
                    }

                    for (int j = 0; j < panelIDs.size(); j++) {
                        String gID = System.currentTimeMillis() + "_" + (int) (Math.random() * 100000000) % 1000000;
                        for (int k = 0; k < cpsWithEmptyGroupID.size(); k++) {
                            ChartProperties cp = cpsWithEmptyGroupID.get(k);
                            int panelID = graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect());
                            if (panelIDs.get(j) == panelID) {
                                cp.setGroupID(gID);
                            }
                        }
                    }

                    //-----------------------------------------------------------------------------------
                    //Following is to attache the proper signal indicators to the signal parent indicators
                    // This was not done in earlier templating (before 8/12/2006)
                    try {
                        for (int j = 0; j < charts.getLength(); j++) {
                            Node chart = charts.item(j);
                            Boolean isSignalIndicator = Boolean.parseBoolean(
                                    chart.getAttributes().getNamedItem("isSignalIndicator").getNodeValue());
                            if (isSignalIndicator) {
                                int signalParentIndex = Integer.parseInt(chart.getAttributes().
                                        getNamedItem("signalParent").getNodeValue());
                                byte toID = Byte.parseByte(chart.getAttributes().
                                        getNamedItem("toID").getNodeValue());
                                int thisID = Integer.parseInt(chart.getAttributes().
                                        getNamedItem("SourceID").getNodeValue());
                                ChartProperties parentCP = indicators.get(signalParentIndex);
                                parentCP.setSignalParent(true);
                                (indicators.get(thisID)).setSignalIndicator(true);
                                (indicators.get(thisID)).setSignalSourceID(parentCP.getID());
                                switch (toID) {
                                    case GraphDataManager.ID_MACD:
                                        if (indicators.get(thisID) instanceof MovingAverage) {
                                            ((MACD) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                        } else if (indicators.get(thisID) instanceof MACDHistogram) {
                                            ((MACD) parentCP).setHistogram((MACDHistogram) indicators.get(thisID));
                                        }
                                        break;
                                    case GraphDataManager.ID_BOLLINGER_BANDS:
                                        BollingerBand bollingerBand = ((BollingerBand) indicators.get(thisID));
                                        byte bbtype = bollingerBand.getBandType();

                                        if (bollingerBand.getGroupID().equals(parentCP.getGroupID())) {
                                            if (bbtype == BollingerBand.BT_UPPER) {
                                                ((BollingerBand) parentCP).setUpper(bollingerBand);
                                            } else if (bbtype == BollingerBand.BT_LOWER) {
                                                ((BollingerBand) parentCP).setLower(bollingerBand);
                                            }
                                        }
                                        break;
                                    case GraphDataManager.ID_KELTNER_CHANNELS:
                                        KeltnerChannels keltnerChannels = ((KeltnerChannels) indicators.get(thisID));
                                        byte kcType = keltnerChannels.getKeltnerChannelType();

                                        if (keltnerChannels.getGroupID().equals(parentCP.getGroupID())) {
                                            if (kcType == KeltnerChannels.KC_UPPER) {
                                                ((KeltnerChannels) parentCP).setUpper(keltnerChannels);
                                            } else if (kcType == KeltnerChannels.KC_LOWER) {
                                                ((KeltnerChannels) parentCP).setLower(keltnerChannels);
                                            }
                                        }
                                        break;
                                    case GraphDataManager.ID_STOCHAST_OSCILL:
                                        ((StocasticOscillator) parentCP).setSignalLine(
                                                (MovingAverage) indicators.get(thisID));
                                        break;
                                    case GraphDataManager.ID_KLINGER_OSCILL:
                                        ((KlingerOscillator) parentCP).setSignalLine(
                                                (MovingAverage) indicators.get(thisID));
                                        break;
                                    case GraphDataManager.ID_PRICE_CHANNELS:
                                        break;
                                    case GraphDataManager.ID_FO:
                                        ((ForcastOscillator) parentCP).setSignalLine(
                                                (MovingAverage) indicators.get(thisID));
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    } catch (NullPointerException e) {
                        //Ignore for backward compatibility
                    }


                    expression = "Layout/GraphWindow[" + graphIndex + "]/Object";
                    NodeList objects = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                    System.out.println("Object count: " + objects.getLength());
                    if ((objects != null) && (objects.getLength() > 0)) {
                        for (int j = 0; j < objects.getLength(); j++) {
                            Node xmlobj = objects.item(j);
                            int objIndex = (j + 1);

                            int ID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("Type").getNodeValue());
                            ChartProperties innerSource = null;
                            String innerSourceID = xmlobj.getAttributes().getNamedItem("Target").getNodeValue();
                            if (!innerSourceID.equals("")) {
                                int mappedID = Integer.parseInt(htSources.get(innerSourceID).toString());
                                innerSource = (ChartProperties) graph.GDMgr.getSources().get(mappedID);
                            }
                            String strPnlID = xmlobj.getAttributes().getNamedItem("PanelID").getNodeValue();
                            int pnlID = Integer.parseInt(htPanels.get(strPnlID).toString());
                            WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
                            AbstractObject ao;
                            if (ID == AbstractObject.INT_REGRESSION || ID == AbstractObject.INT_STD_ERROR_CHANNEL ||
                                    ID == AbstractObject.INT_RAFF_REGRESSION || ID == AbstractObject.INT_STD_DEV_CHANNEL ||
                                    ID == AbstractObject.INT_EQUI_DIST_CHANNEL) {
                                ao = AbstractObject.CreateObject(ID, null, null, innerSource, r, graph.panels, graph);
                            } else {
                                ao = AbstractObject.CreateObject(ID, null, null, null, innerSource, r, graph.panels, graph);
                            }
                            ao.loadFromLayout(xpath, document, expression + "[" + objIndex + "]");
                            ao.pnlID = pnlID;
                            graph.GDMgr.addToObjectArray(ao);
                        }
                    }
                    //Loading Highlighted Areas
                    expression = "Layout/GraphWindow[" + graphIndex + "]/HighlightedArea";
                    NodeList highlights = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                    if ((highlights != null) && (highlights.getLength() > 0)) {
                        ChartProperties topCp, botCp;
                        int topID, botID;
                        Color fillColor;
                        float alpha;
                        graph.GDMgr.getHighLightedSources().clear(); //clear before loading from the template
                        for (int j = 0; j < highlights.getLength(); j++) {
                            Node xmlobj = highlights.item(j);
                            topID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("TopCP").getNodeValue());
                            botID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("BottomCP").getNodeValue());
                            topCp = (ChartProperties) graph.GDMgr.getSources().get(topID);
                            botCp = (ChartProperties) graph.GDMgr.getSources().get(botID);
                            fillColor = new Color(Integer.parseInt(xmlobj.getAttributes().getNamedItem("Color").getNodeValue()));
                            alpha = Float.parseFloat(xmlobj.getAttributes().getNamedItem("Alpha").getNodeValue());
                            HighlightedArea ha = new HighlightedArea(topCp, botCp, fillColor, alpha);
                            graph.GDMgr.getHighLightedSources().add(ha);
                        }
                    }

                    //graph.GDMgr.reConstructGraphStore();
                    graph.GDMgr.reloadGraphStoreData();
                    graph.reDrawAll();
                } else {
                    if (isTempFile) {
                        layoutFile.delete();
                    }
                    throw new Exception();
                }

                gf.setBounds(Integer.parseInt(saBounds[0]), Integer.parseInt(saBounds[1]), Integer.parseInt(saBounds[2]),
                        Integer.parseInt(saBounds[3]));
                gf.setGraphTitle();
                gf.setTableMode(!isTableMode);
                gf.switchPanel();
                if (!isDetached) {
                    if (!isLayout) gf.setMaximum(true);
                    gf.show();
                    //ChartFrame.getSharedInstance().tileWindows();   //important when saved with side bar
                    if (isDetached) {
                        gf.setFileName(layoutFile.getPath());
                        gf.setGraphTitle();
                    } else {
                        gf.setFileName(chtFileName);
                        //commented by charithn to remove the "static chart path" from graph title bar   
                        //gf.setTitle(Language.getString("STATIC_CHART") + " - " + chtFileName);
                    }
                    selectedFrames.add(gf);
                }
                is.close();
            }
            if (isTempFile) {
                layoutFile.delete();
            }
        } else {
            if (isTempFile) {
                layoutFile.delete();
            }
            //SharedMethods.printLine("charith", true);
            throw new Exception();
        }
    }

    public static void SaveLayout(File layoutFile, GraphFrame[] graphFrames) throws Exception {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = document.createElement("Layout");
            root.setAttribute("Name", layoutFile.getName());
            document.appendChild(root);

            for (int i = 0; i < graphFrames.length; i++) {
                GraphFrame gf = graphFrames[i];
                save(gf, document, root);
            }

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            FileOutputStream out = new FileOutputStream(layoutFile);
            Result dest = new StreamResult(out);
            aTransformer.transform(src, dest);
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static void saveChtFile(String chtFileName, GraphFrame graphFrame) throws Exception {
        try {
            String path = chtFileName.substring(0, chtFileName.lastIndexOf(File.separator));
            String layoutFileName = System.currentTimeMillis() + ".mcl";
            File layoutFile = new File(path + File.separator + layoutFileName);

            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = document.createElement("Layout");
            root.setAttribute("Name", layoutFile.getName());
            document.appendChild(root);

            save(graphFrame, document, root);

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            FileOutputStream out = new FileOutputStream(layoutFile);
            Result dest = new StreamResult(out);
            aTransformer.transform(src, dest);
            out.flush();
            out.close();

            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(chtFileName));
            zipOut.setMethod(ZipOutputStream.DEFLATED);
            ZipEntry chartLayout = new ZipEntry(layoutFileName);
            zipOut.putNextEntry(chartLayout);
            int n;
            byte[] buffer = new byte[2048];
            FileInputStream fileinputstream = new FileInputStream(layoutFile);
            while ((n = fileinputstream.read(buffer)) > -1) {
                zipOut.write(buffer, 0, n);
            }
            fileinputstream.close();
            layoutFile.delete();
            zipOut.closeEntry();

            Hashtable<String, DynamicArray> dataRecords = new Hashtable<String, DynamicArray>();
            populateRecords(dataRecords, graphFrame);
            Enumeration<String> symbolKeys = dataRecords.keys();
            while (symbolKeys.hasMoreElements()) {
                String key = symbolKeys.nextElement();
                File dataFile = new File(path + File.separator + key + ".dat");
                saveRecords(key, dataRecords.get(key), dataFile, graphFrame.CurrentMode);

                ZipEntry data = new ZipEntry(key + ".dat");
                zipOut.putNextEntry(data);
                fileinputstream = new FileInputStream(dataFile);
                while ((n = fileinputstream.read(buffer)) > -1) {
                    zipOut.write(buffer, 0, n);
                }
                fileinputstream.close();
                dataFile.delete();
                zipOut.closeEntry();
            }

            zipOut.flush();
            zipOut.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    private static void save(GraphFrame gf, Document document, Element root) {
        StockGraph graph = gf.graph;

        // chart frame specific
        Element chartFrame = document.createElement("ChartFrame");
        boolean showSnapBall = false;
        boolean showCrossHair = false;
        if (gf.getChartFrame() != null) {
            showSnapBall = gf.getChartFrame().isShowSnapBall();
            showCrossHair = gf.getChartFrame().isShowCrossHairs();
        }
        chartFrame.setAttribute("isShowSnapBall", Boolean.toString(showSnapBall));
        chartFrame.setAttribute("isShowCrossHairs", Boolean.toString(showCrossHair));

        root.appendChild(chartFrame);

        // graph window specific
        Element graphWindow = document.createElement("GraphWindow");
        String strBounds = "";
        strBounds = new StringBuffer().append(gf.getX() + ",").append(gf.getY()).append(",").append(gf.getWidth()).append(",").append(gf.getHeight()).toString();

        graphWindow.setAttribute("Bounds", strBounds);
        graphWindow.setAttribute("Mode", Boolean.toString(graph.isCurrentMode()));
        graphWindow.setAttribute("Period", Integer.toString(graph.getPeriod()));
        graphWindow.setAttribute("Interval", Long.toString(graph.getInterval()));
        graphWindow.setAttribute("isTableMode", Boolean.toString(gf.isTableMode()));

        graphWindow.setAttribute("isShowCurrentPriceLine", Boolean.toString(graph.GDMgr.isShowCurrentPriceLine()));
        graphWindow.setAttribute("isShowPreviousCloseLine", Boolean.toString(graph.GDMgr.isshowPreviousCloseLine()));
        graphWindow.setAttribute("isShowBidAskTags", Boolean.toString(graph.GDMgr.isShowBidAskTags()));
        graphWindow.setAttribute("isShowOrderLines", Boolean.toString(graph.GDMgr.isShowOrderLines()));
        graphWindow.setAttribute("isShowMinMaxLines", Boolean.toString(graph.GDMgr.isShowMinMaxPriceLines()));
        graphWindow.setAttribute("isShowVolumeInProchart", Boolean.toString(graph.isShowingVolumeGrf()));
        graphWindow.setAttribute("isShowTurnOverInProChart", Boolean.toString(graph.isShowingTurnOverGraph()));
        graphWindow.setAttribute("isShowAnnouncements", Boolean.toString(graph.GDMgr.isShowingAnnouncements()));
        graphWindow.setAttribute("isShowSplits", Boolean.toString(graph.GDMgr.isShowingSplits()));
        graphWindow.setAttribute("isShowVolumeByPrice", Boolean.toString(graph.GDMgr.isShowVolumeByPrice()));

        graphWindow.setAttribute("showVolumeByPrice", Byte.toString(graph.GDMgr.getVolumeByPriceType()));
        graphWindow.setAttribute("crossHairType", Byte.toString(graph.GDMgr.getCrossHairType()));

        graphWindow.setAttribute("rightMarginHistory", Integer.toString(graph.GDMgr.getRightMarginHistory()));
        graphWindow.setAttribute("rightMarginIntraday", Integer.toString(graph.GDMgr.getRightMarginIntraday()));
        graphWindow.setAttribute("linkID", gf.getLinkID());

        if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
            graphWindow.setAttribute("periodType", Integer.toString(graph.GDMgr.currentCustomPeriod));
            graphWindow.setAttribute("range", Integer.toString(graph.GDMgr.currentCustomRange));
        }
        if (graph.getInterval() ==
                (graph.GDMgr.currentCustomIntervalType * graph.GDMgr.currentCustomIntervalFactor)) {
            graphWindow.setAttribute("customIntervalType", Long.toString(graph.GDMgr.currentCustomIntervalType));
            graphWindow.setAttribute("customIntervalFactor", Long.toString(graph.GDMgr.currentCustomIntervalFactor));
        }
        root.appendChild(graphWindow);

        ArrayList alSources = graph.GDMgr.getSources();
        Hashtable<Integer, ChartProperties> signalParents = new Hashtable<Integer, ChartProperties>();
        for (int j = 0; j < alSources.size(); j++) {
            ChartProperties cp = (ChartProperties) alSources.get(j);
            if (cp.isSignalParent() && !cp.isSignalIndicator()) {
                signalParents.put(j, cp);
            }
            if ((cp.getID() == GraphDataManager.ID_VOLUME) && !graph.isShowingVolumeGrf()) continue;
            if ((cp.getID() == GraphDataManager.ID_TURNOVER) && !graph.isShowingTurnOverGraph()) continue;

            Element chart = document.createElement("Chart");
            if (cp.getID() <= GraphDataManager.ID_COMPARE) {
                chart.setAttribute("Symbol", cp.getSymbol());
            }
            chart.setAttribute("ID", Byte.toString(cp.getID()));
            chart.setAttribute("SourceID", Integer.toString(j));
            chart.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(cp.getRect())));
            chart.setAttribute("isSignalIndicator", Boolean.toString(cp.isSignalIndicator()));
            chart.setAttribute("IsUserSaved", Boolean.toString(cp.isUserSaved()));
            if (cp.isSignalIndicator()) {
                chart.setAttribute("toID", Byte.toString(cp.getSignalSourceID()));
            }
            if (cp.getID() > GraphDataManager.ID_COMPARE) {
                int innersourceID = ((Indicator) cp).getInnerSourceIndex(alSources);
                chart.setAttribute("InnerSource", Integer.toString(innersourceID));
                chart.setAttribute("tableColumnHeading", ((Indicator) cp).getTableColumnHeading());
                //shashika
                if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                    chart.setAttribute("PanelID", TemplateFactory.HIDDEN_PANEL_TEMP_ID);
                }
            }
            cp.saveTemplate(chart, document);
            graphWindow.appendChild(chart);
        }

        //Following is to save signal indicator relationships
        Enumeration<Integer> keys = signalParents.keys();
        while (keys.hasMoreElements()) {
            int signalParentId = keys.nextElement();
            NodeList chartNodeList = graphWindow.getElementsByTagName("Chart");
            for (int k = 0; k < chartNodeList.getLength(); k++) {
                Node chartNode = chartNodeList.item(k);
                Node attribute = chartNode.getAttributes().getNamedItem("toID");
                if (attribute == null) {
                    continue;
                }
                Byte toId = Byte.parseByte(attribute.getNodeValue());
                if (toId == signalParents.get(signalParentId).getID()) {
                    ((Element) chartNode).setAttribute("signalParent", Integer.toString(signalParentId));
                }
            }
        }

        //Saving Highlighted Areas
        ArrayList<HighlightedArea> alHighlights = graph.GDMgr.getHighLightedSources();
        for (int j = 0; j < alHighlights.size(); j++) {
            HighlightedArea ha = alHighlights.get(j);
            Element xmlobj = document.createElement("HighlightedArea");
            xmlobj.setAttribute("TopCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getTopCP())));
            xmlobj.setAttribute("BottomCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getBottomCP())));
            xmlobj.setAttribute("Color", Integer.toString(ha.getFillColor().getRGB()));
            xmlobj.setAttribute("Alpha", Float.toString(ha.getAlpha()));
            graphWindow.appendChild(xmlobj);
        }

        ArrayList alOblects = graph.GDMgr.getObjArray();
        for (int j = 0; j < alOblects.size(); j++) {
            AbstractObject ao = (AbstractObject) alOblects.get(j);
            Element xmlobj = document.createElement("Object");
            xmlobj.setAttribute("Type", Integer.toString(ao.getObjType()));
            String innersourceID = "";
            if (ao.getTarget() != null) {
                innersourceID = Integer.toString(graph.GDMgr.getIndexForSourceCP(ao.getTarget()));
            }
            xmlobj.setAttribute("Target", innersourceID);
            xmlobj.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(ao.getRect())));
            ao.saveToLayout(xmlobj, document);
            graphWindow.appendChild(xmlobj);
        }

    }

    private static void populateRecords(Hashtable<String, DynamicArray> dataRecords, GraphFrame graphFrame) {
        ArrayList sources = graphFrame.graph.GDMgr.getSources();
        for (int i = 0; i < sources.size(); i++) {
            ChartProperties cp = (ChartProperties) sources.get(i);
            if (cp.getID() == GraphDataManager.ID_BASE || cp.getID() == GraphDataManager.ID_COMPARE) {
                DynamicArray records = new DynamicArray();
                DynamicArray store = graphFrame.graph.GDMgr.getStore();
                int index = graphFrame.graph.GDMgr.getIndexForSourceCP(cp);
                for (int j = 0; j < store.size(); j++) {
                    Object[] oa = (Object[]) store.get(j);
                    long time = (Long) oa[0];
                    if (oa.length > index + 1) {
                        ChartPoint chartPoint = (ChartPoint) oa[index + 1];
                        if (chartPoint != null) {
                            IntraDayOHLC ohlcRecord = new IntraDayOHLC(cp.getSymbol(), time / 60000, (float) chartPoint.Open, (float) chartPoint.High,
                                    (float) chartPoint.Low, (float) chartPoint.Close, (long) chartPoint.Volume, 0, chartPoint.TurnOver); // todo intraday vwap
                            records.add(ohlcRecord);
                        }
                    }
                }
                dataRecords.put(cp.getSymbol(), records);
            }
        }
    }

    private static void saveRecords(String symbol, DynamicArray records, File dataFile, boolean isCurrentMode) throws Exception {
        FileOutputStream fout = new FileOutputStream(dataFile);
        for (int i = 0; i < records.size(); i++) {
            IntraDayOHLC intraDayOHLC = (IntraDayOHLC) records.get(i);
            long time = (intraDayOHLC.getTime() * 60000L - ChartInterface.getTimeZoneAdjustment(symbol, intraDayOHLC.getTime() * 60000L, isCurrentMode)) / 60000L;
            String recordString = time + "," + intraDayOHLC.getOpen() + "," + intraDayOHLC.getHigh() +
                    "," + intraDayOHLC.getLow() + "," + intraDayOHLC.getClose() + "," + intraDayOHLC.getVolume() + "," + intraDayOHLC.getVwap() + "," + intraDayOHLC.getTurnOver() + "\n";
            fout.write(recordString.getBytes());
        }
        fout.close();
    }

    public static DynamicArray loadRecords(String symbol, File dataFile) throws Exception {
        DynamicArray records = new DynamicArray();
        BufferedReader bis = new BufferedReader(new FileReader(dataFile));
        String line;
        while ((line = bis.readLine()) != null) {
            String[] entries = line.split(",");
            if (entries.length == 8) {
                IntraDayOHLC ohlcRecord = new IntraDayOHLC(symbol, Long.parseLong(entries[0]), Float.parseFloat(entries[1]),
                        Float.parseFloat(entries[2]), Float.parseFloat(entries[3]), Float.parseFloat(entries[4]),
                        Long.parseLong(entries[5]), Float.parseFloat(entries[6]), Double.parseDouble(entries[7]));
                records.add(ohlcRecord);
            } else if (entries.length == 6) {
                IntraDayOHLC ohlcRecord = new IntraDayOHLC(symbol, Long.parseLong(entries[0]), Float.parseFloat(entries[1]),
                        Float.parseFloat(entries[2]), Float.parseFloat(entries[3]), Float.parseFloat(entries[4]),
                        Long.parseLong(entries[5]), -1, -1);
                records.add(ohlcRecord);
            }

        }
        bis.close();

        return records;
    }

}
