package com.isi.csvr.chart;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class SplitPanel extends JPanel {

    public final static byte AXIS_LEFT_ONLY = 0;
    protected byte yAxisPos = AXIS_LEFT_ONLY;
    public final static byte AXIS_RIGHT_ONLY = 1;
    public final static byte AXIS_BOTH_ENDS = 2;
    public static final int borderWidth = 0;
    public static final int clearance = 6;
    public static final int heightXaxis = 25;
    public static final int legendHeight = 15;
    public static int titleHeight = 18;
    public static int sliderHeight = 14;
    public final int MINIMUM_WIDTH = 20;
    public Color titleColor = Color.BLUE;
    public ArrayList panels = new ArrayList();
    public float[] splitters = new float[4];
    public WindowPanel mainRect = new WindowPanel(this.getBounds());
    public WindowPanel volumeRect = new WindowPanel(0, 0, 0, 0);
    public WindowPanel turnOverRect = new WindowPanel(0, 0, 0, 0);
    protected int widthYaxis = 20;
    protected int widthYaxis_1 = 20;
    protected int widthYaxis_2 = 0;
    protected int Title_Top = 16;
    protected boolean maximized = false;
    protected int maximizedPanelID = 0;
    protected int Wd, Ht;
    protected int GrfPHt, GrfPWd, GrfVHt, GrfVWd;
    protected GraphMouseListener gmListener = null;
    protected boolean splitterDragged = false;
    protected boolean closeBtnPressed = false;
    protected boolean maximizeBtnPressed = false;
    protected boolean slideBtnPressed = false;
    protected boolean showVolumeGrf = false;
    protected boolean showTurnOverGrf = false;
    protected boolean titleDragged = false;
    protected int sourcePanelID = -1;
    protected int destPanelID = -1;
    protected boolean below = false;
    Image imgIconWinClose = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_win_close.gif");
    Image imgIconWinMaximize = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_win_maximize.gif");
    Image imgIconWinRestore = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_win_restore.gif");
    Image imgIconWinCollapse = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_win_expand.gif"); //Changed - Pramoda
    Image imgIconWinExpand = Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_win_collapse.gif"); //Changed - Pramoda
    int MouseDownX = 0;
    int MouseDownY = 0;
    int activeSplitter = -1;
    int prevY = -1;
    private Color splitterColor = Color.BLACK;
    private Cursor defaultCursor;

    public SplitPanel() {
        panels.add(mainRect);
        panels.add(volumeRect);
        panels.add(turnOverRect);
        initialliseRects();
        defaultCursor = Cursor.getDefaultCursor();
        this.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(MouseEvent e) {
                graphMouseMoved(e);
                splitPanelMouseMoved(e);
            }

            public void mouseDragged(MouseEvent e) {
                splitPanelMouseDragged(e);
                graphMouseDragged(e);
            }
        });
        this.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseExited(MouseEvent e) {
                graphMouseExited(e);
            }

            public void mousePressed(MouseEvent e) {
                //super.mousePressed(e);
                splitPanelMousePressed(e);
            }

            public void mouseReleased(MouseEvent e) {
                splitPanelMouseReleased(e);
            }
        });
    }

    public void addGraphMouseListener(GraphMouseListener gml) {
        this.gmListener = gml;
    }

    public void removeGraphMouseListener() {
        this.gmListener = null;
    }

    /*private void initialliseRects(){
         splitters[0] = 0f;
         splitters[1] = 0.75f;
         splitters[2] = 1.0f;
         recalculateRectBounds();
     }*/

    public void initialliseRects() {
        panels.clear();
        panels.add(mainRect);

        /*if (showVolumeGrf) {
            panels.add(volumeRect);
            splitters = new float[3];
            splitters[0] = 0f;
            splitters[1] = 0.75f;
            splitters[2] = 1.0f;
        } else {
            splitters = new float[2];
            splitters[0] = 0f;
            splitters[1] = 1.0f;
        }

         if (showTurnOverGrf) {      
            panels.add(turnOverRect);
            splitters = new float[3];
            splitters[0] = 0f;
            splitters[1] = 0.75f;
            splitters[2] = 1.0f;
        } else {
            splitters = new float[2];
            splitters[0] = 0f;
            splitters[1] = 1.0f;
        }
        recalculateRectBounds();*/

        if (showVolumeGrf && showTurnOverGrf) {
            panels.add(volumeRect);
            panels.add(turnOverRect);
            splitters = new float[4];
            splitters[0] = 0f;
            splitters[1] = 0.5f;
            splitters[2] = 0.75f;
            splitters[3] = 1.0f;
        } else if (showVolumeGrf) {
            panels.add(volumeRect);
            splitters = new float[3];
            splitters[0] = 0f;
            splitters[1] = 0.75f;
            splitters[2] = 1.0f;
        } else if (showTurnOverGrf) {
            panels.add(turnOverRect);
            splitters = new float[3];
            splitters[0] = 0f;
            splitters[1] = 0.75f;
            splitters[2] = 1.0f;
        } else {
            splitters = new float[2];
            splitters[0] = 0f;
            splitters[1] = 1.0f;
        }
        recalculateRectBounds();
    }

    public void recalculateRectBounds() {
        int h, t, l = widthYaxis_1 + borderWidth;
        for (int i = 0; i < panels.size(); i++) {
            if (splitters.length <= i + 1) break;
            if (maximized) {
                if (i == maximizedPanelID) {
                    t = Math.round(0f * Ht) + borderWidth + legendHeight;
                    h = Math.round((1.0f - 0f) * Ht);
                    Rectangle r = (Rectangle) panels.get(i);
                    r.setBounds(l, t, Wd, h);
                } else {
                    t = Math.round((1.0f - 0f) * Ht) + borderWidth + legendHeight;
                    h = 0;
                    Rectangle r = (Rectangle) panels.get(i);
                    r.setBounds(l, t, Wd, h);
                }
            } else {
                t = Math.round(splitters[i] * Ht) + borderWidth + legendHeight;
                h = Math.round((splitters[i + 1] - splitters[i]) * Ht);
                Rectangle r = (Rectangle) panels.get(i);
                r.setBounds(l, t, Wd, h);
//                ((WindowPanel)r).setOriginalBounds(l, t, Wd, h);
            }
        }
    }

    public WindowPanel addNewPanel(boolean onTop, WindowPanel r) {
        if (getIDforPanel(r) < 0) {    // !panels.contains(r)
            if (onTop) {
                panels.add(0, r);
            } else {
                panels.add(r);
            }
            float[] newSplitters = new float[splitters.length + 1];
            float newH = 1f / (splitters.length + 1f);
            float newRatio = 1f - newH; //noOfPanels+2
            int adj = 0;
            if (onTop) adj = 1;
            for (int i = 0; i < splitters.length; i++) {
                newSplitters[i + adj] = splitters[i] * newRatio + newH * adj;
            }
            newSplitters[0] = 0f;
            newSplitters[newSplitters.length - 1] = 1f;
            splitters = newSplitters;
        }
        recalculateRectBounds();
        return r;
    }

    public WindowPanel addNewPanel(boolean onTop) {
        final WindowPanel r = new WindowPanel();
        return addNewPanel(onTop, r);
    }


    public void paint(Graphics g) {
        super.paint(g);
        try {
            paintGraphs(g);
        } catch (Exception ex) {
        }
        g.setClip(0, 0, this.getWidth(), this.getHeight());
        for (int i = 0; i < panels.size(); i++) {
            g.setColor(splitterColor);
            WindowPanel r = (WindowPanel) panels.get(i);
            Image slideImg = (r.isMinimized()) ? imgIconWinExpand : imgIconWinCollapse;
            if (maximized) {
                if (i == maximizedPanelID) {
                    //drawing Slide rect on right-top //############
                    Rectangle sR = getSlideRect(r);
                    g.drawImage(slideImg, sR.x + 2, sR.y + 1, this);
                    sR = null;
                    //drawing Maximize rect on right-top //############
                    Rectangle mR = getMaximizeRect(r);
                    g.drawImage(imgIconWinRestore, mR.x + 2, mR.y + 1, this);
                    mR = null;
                    //drawing Close rect on right-top //############
                    if (r != mainRect && r.isClosable()) {
                        Rectangle cR = getCloseRect(r);
                        g.drawImage(imgIconWinClose, cR.x + 2, cR.y + 1, this);
                        cR = null;
                    }
                    //#######################################
                } else {
                    // draw nothing
                }
            } else {
                if (i > 0) {
                    g.fill3DRect(borderWidth, r.y - 1, this.getWidth() - borderWidth, 2, true);
                }
                //drawing Slide rect on right-top //############
                Rectangle sR = getSlideRect(r);
                g.drawImage(slideImg, sR.x + 2, sR.y + 1, this);
                sR = null;
                //drawing Restore rect on right-top //############
                Rectangle mR = getMaximizeRect(r);
                g.drawImage(imgIconWinMaximize, mR.x + 2, mR.y + 1, this);
                mR = null;
                //drawing "X" on right-top //############
                if (r != mainRect && r.isClosable()) {
                    Rectangle cR = getCloseRect(r);
                    g.drawImage(imgIconWinClose, cR.x + 2, cR.y + 1, this);
                    cR = null;
                }
                //#######################################
            }
        }
        g.setClip(null);
    }

    public Rectangle getCloseRect(Rectangle r) {
        Rectangle rect = new Rectangle();
        int w = (yAxisPos == SplitPanel.AXIS_LEFT_ONLY) ? r.width : r.width + widthYaxis;
        rect.setLocation(r.x + w - titleHeight, r.y + 3);
        rect.setSize(titleHeight, titleHeight);
        return rect;
    }

    public Rectangle getMaximizeRect(Rectangle r) {
        Rectangle rect = new Rectangle();
        int w = (yAxisPos == SplitPanel.AXIS_LEFT_ONLY) ? r.width : r.width + widthYaxis;
        rect.setLocation(r.x + w - 2 * titleHeight, r.y + 3);
        rect.setSize(titleHeight, titleHeight);
        return rect;
    }

    public Rectangle getSlideRect(Rectangle r) {
        Rectangle rect = new Rectangle();
        int w = (yAxisPos == SplitPanel.AXIS_LEFT_ONLY) ? r.width : r.width + widthYaxis;
        rect.setLocation(r.x + w - 3 * titleHeight, r.y + 3);
        rect.setSize(titleHeight, titleHeight);
        return rect;
    }

    public Rectangle getTitleRect(Rectangle r) {
        Rectangle rect = new Rectangle();
        int w = (yAxisPos == SplitPanel.AXIS_LEFT_ONLY) ? r.width : r.width + widthYaxis;
        rect.setLocation(borderWidth, r.y);
        rect.setSize(r.x - borderWidth + w - 3 * titleHeight, titleHeight);  // titleHeight = button width, button height  (3 buttons)
        return rect;
    }

    public boolean isOnYaxis(int x, int y) {
        for (int i = 0; i < panels.size(); i++) {
            Rectangle r = (Rectangle) panels.get(i);
            if ((r.y + titleHeight <= y) && (r.y + r.height >= y)) {
                //checking left Y axis
                if ((yAxisPos == SplitPanel.AXIS_LEFT_ONLY) || (yAxisPos == SplitPanel.AXIS_BOTH_ENDS)) {
                    if ((r.x - widthYaxis <= x) && (r.x >= x)) {
                        return true;
                    }
                }
                //checking right Y axis
                if ((yAxisPos == SplitPanel.AXIS_RIGHT_ONLY) || (yAxisPos == SplitPanel.AXIS_BOTH_ENDS)) {
                    if ((r.x + r.width <= x) && (r.x + r.width + widthYaxis >= x)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
        setImageWdHt(width, height);
    }

    private void splitPanelMouseMoved(MouseEvent e) {
        if (getActiveSplitter(e.getY()) > 0) {
            setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        } else {
            Cursor currCur = getCursor();
            if (currCur.getType() == Cursor.N_RESIZE_CURSOR) {
                setCursor(defaultCursor);
            } else {
                if (currCur.getType() != defaultCursor.getType()) {
                    defaultCursor = currCur;
                }
            }
        }
    }

    private void splitPanelMouseDragged(MouseEvent e) {
        if (splitterDragged) {
            int currY = e.getY();
            Rectangle topRect = (Rectangle) panels.get(activeSplitter - 1);
            Rectangle botRect = (Rectangle) panels.get(activeSplitter);
            if (currY <= topRect.y + titleHeight) {
                currY = topRect.y + titleHeight + 1;
            } else if (currY >= botRect.y + botRect.height - titleHeight) {
                currY = botRect.y + botRect.height - 1 - titleHeight;
            }
            Graphics g = getGraphics();
//			g.setXORMode(Color.gray);
            g.setXORMode(Color.RED);
//			g.setColor(Color.lightGray);
            g.setColor(Color.RED);
            if (prevY != -1) {
                g.fill3DRect(borderWidth, prevY - 1, topRect.x + topRect.width, 2, false);
            }
            g.fill3DRect(borderWidth, currY - 1, topRect.x + topRect.width, 2, false);
            prevY = currY;
        } else if (titleDragged) {
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Image image = toolkit.getImage("images/Theme" + Theme.getID() + "/charts/graph_obj_panel_drag.gif");
            Cursor moveCursor = toolkit.createCustomCursor(image, new Point(0, 0), "");
            setCursor(moveCursor);
            if (sourcePanelID > -1) {
                destPanelID = getPanelIDatPoint(e.getX(), e.getY());
                if (destPanelID > -1) {
                    Rectangle r = (Rectangle) panels.get(destPanelID);
                    if ((r.y + r.height / 2) > e.getY()) {
                        below = false;
                    } else {
                        below = true;
                    }
                }
            }
            repaint();
        } else {
            if (gmListener != null) {
                gmListener.graphMouseDragged(e);
            }
        }
    }

    private void splitPanelMousePressed(MouseEvent e) {
        activeSplitter = getActiveSplitter(e.getY());
        splitterDragged = (activeSplitter > 0);
        titleDragged = (getActiveTitleBar(e.getX(), e.getY()) > -1);
        if (titleDragged) {
            sourcePanelID = getPanelIDatPoint(e.getX(), e.getY());
        } else {
            sourcePanelID = -1;
        }
        MouseDownX = e.getX();
        MouseDownY = e.getY();
        prevY = -1;
        Rectangle cR, mR, sR, r;
        for (int i = 0; i < panels.size(); i++) {
            r = (Rectangle) panels.get(i);
            // checking for the slide btn press
            sR = getSlideRect(r);
            if (sR.contains(MouseDownX, MouseDownY)) {
                slideBtnPressed = true;
                break;
            }
            // checking for the maximize btn press
            mR = getMaximizeRect(r);
            if (mR.contains(MouseDownX, MouseDownY)) {
                maximizeBtnPressed = true;
                break;
            }
            // checking for the close btn press
            if (r == mainRect) continue;
            if (!((WindowPanel) r).isClosable()) continue;

            cR = getCloseRect(r);
            if (cR.contains(MouseDownX, MouseDownY)) {
                closeBtnPressed = true;
                break;
            }
        }
        if (!splitterDragged && !closeBtnPressed) {
            if (gmListener != null) {
                gmListener.graphMousePressed(e);
            }
        }
    }

    private void splitPanelMouseReleased(MouseEvent e) {

        //changed by charithn. IMPORTANT - to affect the cursor image in zooming mode.
        if (BottomToolBar.CURSOR_STATUS == BottomToolBar.STATUS_NONE) {
            setCursor(Cursor.getDefaultCursor());
        }

        if (splitterDragged) {
            boolean topRectMinimized = false, bottomRectMinimized = false;
            int currY = e.getY();
            Rectangle topRect = (Rectangle) panels.get(activeSplitter - 1);
            Rectangle botRect = (Rectangle) panels.get(activeSplitter);
            if (currY <= topRect.y + titleHeight) {
                currY = topRect.y + titleHeight + 1;
                topRectMinimized = true;
            } else if (currY >= botRect.y + botRect.height - titleHeight) {
                currY = botRect.y + botRect.height - titleHeight - 1;
                bottomRectMinimized = true;
            }
            float spGradiant = (splitters[activeSplitter + 1] - splitters[activeSplitter - 1])
                    / ((float) topRect.height + botRect.height);
            splitters[activeSplitter] = splitters[activeSplitter - 1] +
                    spGradiant * (currY - topRect.y);
            ((WindowPanel) topRect).setMinimized(topRectMinimized);
            ((WindowPanel) botRect).setMinimized(bottomRectMinimized);
            recalculateRectBounds();
            repaint();
            prevY = -1;
        } else if (titleDragged && e.getClickCount() == 1) {
            titleDragged = false;
            Rectangle source = (Rectangle) panels.get(sourcePanelID);
            if (destPanelID > -1) {
                if (below) {
                    panels.add(destPanelID + 1, source);
                } else {
                    panels.add(destPanelID, source);
                }
                if (sourcePanelID < destPanelID) {
                    panels.remove(sourcePanelID);
                } else {
                    panels.remove(sourcePanelID + 1);
                }
                int newDestId = getIDforPanel(source);
                rePositionSplitters(sourcePanelID, newDestId);
            }
            recalculateRectBounds();
            repaint();
            sourcePanelID = -1;
            destPanelID = -1;
        } else {
            Rectangle cR, mR, sR, tR, r;
            for (int i = 0; i < panels.size(); i++) {
                r = (Rectangle) panels.get(i);
                //checking for double click maximize
                tR = getTitleRect(r);
                if ((e.getClickCount() > 1) && tR.contains(MouseDownX, MouseDownY)) {
                    toggleMaximize(i);
                    break;
                }
                // invoke slide btn press
                sR = getSlideRect(r);
                if (slideBtnPressed && sR.contains(e.getPoint())) {
                    toggleSlide(i);
                    repaint();// added to avoid the paint delay
                    break;
                }
                // invoke maximize btn press
                mR = getMaximizeRect(r);
                if (maximizeBtnPressed && mR.contains(e.getPoint())) {
                    toggleMaximize(i);
                    break;
                }
                // invoke close btn press
                if (r == mainRect) continue;
                cR = getCloseRect(r);
                if (closeBtnPressed && cR.contains(e.getPoint())) {
                    deletePanel(i);
                    break;
                }
            }
            cR = null;
            if (!closeBtnPressed) {
                if (gmListener != null) {
                    gmListener.graphMouseReleased(e);
                }
            }
        }
        splitterDragged = false;
        closeBtnPressed = false;
    }

    private void rePositionSplitters(int srcId, int destId) {
        Vector<Float> windowRelativeHeights = new Vector<Float>();
        for (int i = 0; i < panels.size(); i++) {
            windowRelativeHeights.add(splitters[i + 1] - splitters[i]);
        }

        windowRelativeHeights.add(destId, windowRelativeHeights.remove(srcId));

        for (int i = 0; i < windowRelativeHeights.size(); i++) {
            splitters[i + 1] = splitters[i] + windowRelativeHeights.get(i);
        }
    }

    private int getActiveSplitter(int y) {
        if (!maximized) {
            for (int i = 1; i < panels.size(); i++) {
                Rectangle r = (Rectangle) panels.get(i);
                if ((r.y - y) * (r.y - y) < 4) {
                    return i;
                }
            }
        }
        return -1;
    }

    private int getActiveTitleBar(int x, int y) {
        if (!maximized) {
            for (int i = 0; i < panels.size(); i++) {
                Rectangle r = (Rectangle) panels.get(i);
                if (getTitleRect(r).contains(x, y)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public int getBottomPanelBottom() {
        return Ht + legendHeight + borderWidth;
    }

    // maximized
    public boolean isMaximized() {
        return maximized;
    }

    public void setMaximized(boolean maximized) {
        this.maximized = maximized;
    }

    // maximizedPanelID
    public int getMaximizedPanelID() {
        return maximizedPanelID;
    }

    public void setMaximizedPanelID(int maximizedPanelID) {
        this.maximizedPanelID = maximizedPanelID;
    }


    //######################################
    //to be overriden by StockGraph class
    protected void paintGraphs(Graphics g) {
    }

    protected void graphMouseMoved(MouseEvent e) {
        if (gmListener != null) {
            gmListener.graphMouseMoved(e);
        }
    }

    protected void graphMouseDragged(MouseEvent e) {
        if (gmListener != null) {
            gmListener.graphMouseDragged(e);
        }
    }

    protected void graphMouseExited(MouseEvent e) {
        if (gmListener != null) {
            gmListener.graphMouseExited(e);
        }
    }

    protected void toggleMaximize(int index) {
        if (maximized) {
            maximized = false;
            maximizedPanelID = 0;
        } else {
            maximized = true;
            maximizedPanelID = index;
        }
        recalculateRectBounds();
    }

    protected void toggleSlide(int index) {
        WindowPanel wp = (WindowPanel) panels.get(index);
        if (maximized) {
            maximized = false;
            maximizedPanelID = 0;
        }
        wp.setMinimized(!wp.isMinimized());
        rearrangeSplitters(wp.isMinimized(), index);
        recalculateRectBounds();
    }

    protected void rearrangeSplitters(boolean minimize, int index) {
        float titleFixedGap = 1f * titleHeight / (float) Ht;
        int fixedCount = 0;
        float variableHt = 0f;
        float availableHt = 1f;
        boolean isMainMinimized = false;
        float expannedPanelHt = 0f;
        for (int i = 0; i < panels.size(); i++) {
            WindowPanel wp = (WindowPanel) panels.get(i);
            if (i == 0) {
                isMainMinimized = wp.isMinimized();
            }
            if (i == index) continue;
            if (wp.isMinimized()) {
                fixedCount++;
            } else {
                variableHt = variableHt + splitters[i + 1] - splitters[i];
            }
        }
        if (minimize) {
            if (variableHt == 0f) { //attempting to minimize all
                if (index == 0) { // attempt to minimise base
                    WindowPanel wp = (WindowPanel) panels.get(panels.size() - 1);
                    wp.setMinimized(false);
                    variableHt = 1f - fixedCount * titleFixedGap;
                    index = panels.size() - 1;
                } else {
                    WindowPanel wp = (WindowPanel) panels.get(0);
                    wp.setMinimized(false);
                    variableHt = 1f - fixedCount * titleFixedGap;
                    index = 0;
                }
                expannedPanelHt = 1f - fixedCount * titleFixedGap;
            } else {
                fixedCount++;
            }
            availableHt = 1f - fixedCount * titleFixedGap;
        } else if (variableHt == 0f) {
            availableHt = 1f - fixedCount * titleFixedGap;
            expannedPanelHt = availableHt;
            variableHt = availableHt;
        } else {
            availableHt = 1f - fixedCount * titleFixedGap;
            float preVariableHt = variableHt;
            if (index == 0) { // Main graph = 2*any other graph
                variableHt = variableHt * (panels.size() - fixedCount + 1f) / (panels.size() - fixedCount - 1f);
            } else if (isMainMinimized) {  // main graph is minimised
                variableHt = variableHt * (panels.size() - fixedCount) / (panels.size() - fixedCount - 1f);
            } else { // main graph is in the mix
                variableHt = variableHt * (panels.size() - fixedCount + 1f) / (panels.size() - fixedCount);
            }
            expannedPanelHt = variableHt - preVariableHt;
        }
        float ratio = availableHt / variableHt;
        splitters[0] = 0f;
        float preValue = 0f;
        for (int i = 0; i < panels.size(); i++) {
            float tmpPreValue = splitters[i + 1];
            WindowPanel wp = (WindowPanel) panels.get(i);
            if (wp.isMinimized()) {
                splitters[i + 1] = splitters[i] + titleFixedGap;
            } else {
                if (i == index) {
                    splitters[i + 1] = splitters[i] + expannedPanelHt * ratio;
                } else {
                    splitters[i + 1] = splitters[i] + (splitters[i + 1] - preValue) * ratio;
                }
            }
            preValue = tmpPreValue;
        }
    }

    protected void deletePanel(int index) {
        float[] newSplitters = new float[splitters.length - 1];
        float recH = splitters[index + 1] - splitters[index];
        float newRatio = 1f / (1f - recH);
        for (int i = index; i < splitters.length - 1; i++) {
            splitters[i] = splitters[i + 1] - recH;
        }
        for (int i = 0; i < newSplitters.length; i++) {
            newSplitters[i] = splitters[i] * newRatio;
        }
        newSplitters[0] = 0f;
        newSplitters[newSplitters.length - 1] = 1f;
        splitters = newSplitters;
        panels.remove(index);
        if (maximized) {
            maximized = false;
            maximizedPanelID = 0;
        }
        recalculateRectBounds();
    }

    protected void setImageWdHt(int w, int h) {
        setYAxisWidth();
        Wd = w - widthYaxis_1 - widthYaxis_2 - 2 * borderWidth;
        Ht = h - heightXaxis - sliderHeight - legendHeight - 2 * borderWidth;
//		boolean sliderVisible = true;
        if (Wd < MINIMUM_WIDTH) {
            Wd = MINIMUM_WIDTH;
        } // why not restrict height too??
        recalculateRectBounds();
    }

    public void setImageWdHt() {
        setImageWdHt(getWidth(), getHeight());
    }

    protected void setYAxisWidth() {
        widthYaxis = 50;
        widthYaxis_1 = widthYaxis;
        widthYaxis_2 = 0;
    }

    public int getPanelIDatPoint(float x, float y) {
        for (int i = 0; i < panels.size(); i++) {
            Rectangle r = (Rectangle) panels.get(i);
            if ((r.y <= y) && (r.y + r.height > y)) return i;
        }
        return -1; //TODO: was 0
    }

    public int getIDforPanel(Rectangle rect) {
        for (int i = 0; i < panels.size(); i++) {
            Rectangle r = (Rectangle) panels.get(i);
            if (r == rect) return i;
        }
        return -1;
    }
    //######################################
}

