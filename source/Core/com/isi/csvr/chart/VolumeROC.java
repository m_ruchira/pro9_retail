package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Mar 30, 2006
 * Time: 2:52:11 PM
 */
public class VolumeROC extends ChartProperties implements Indicator {
    private static final long serialVersionUID = UID_VOLUME_ROC;
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    public VolumeROC(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_VOLUME_ROC") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_VOLUME_ROC");
        this.timePeriods = 12;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte destIndex) {
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;


        if (al.size() < t_im_ePeriods || t_im_ePeriods < 2) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }
        for (int i = 0; i < t_im_ePeriods; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = t_im_ePeriods; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - t_im_ePeriods);

            /*ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(100f * (cr.Volume - crOld.Volume) / crOld.Volume);
            }*/
            cr.setValue(destIndex, 100f * (cr.Volume - crOld.Volume) / crOld.Volume);
        }

        //System.out.println("**** Vol.ROC Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 12;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof VolumeROC) {
            VolumeROC vroc = (VolumeROC) cp;
            this.timePeriods = vroc.timePeriods;
            this.innerSource = vroc.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_VOLUME_ROC") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        if (al.size() < timePeriods || timePeriods < 2) return;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;

        for (int i = 0; i < timePeriods; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = timePeriods; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            crOld = (ChartRecord) al.get(i - timePeriods);

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(100f * (cr.Volume - crOld.Volume) / crOld.Volume);
            }
        }

        //System.out.println("**** Vol.ROC Calc time " + (entryTime - System.currentTimeMillis()));
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public String getShortName() {
        return Language.getString("IND_VOLUME_ROC");
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}
