package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Apr 10, 2008
 * Time: 1:28:50 AM
 * To change this template use File | Settings | File Templates.
 */

import java.awt.*;
import java.util.ArrayList;

public class ZigZagSlope extends ZigZag {

    private static final long serialVersionUID = UID_ZIGZAG_SLOPE;

    public ZigZagSlope(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, symbl, anID, c, r);
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          double reversalAmount, CalcMethod calculationMethod, byte srcIndex, byte destIndex) {

        ChartRecord cr;
        int calcMethod = calculationMethod.equals(CalcMethod.PERCENT) ? 0 : 1;
        long entryTime = System.currentTimeMillis();

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        if (bIndex >= al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        int dynIndex, minIndex, maxIndex, preIndex;
        int state = STATE_ZERO, tmpState;
        double currVal, dynVal, minVal, maxVal;

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        cr = (ChartRecord) al.get(bIndex);
        currVal = cr.getValue(srcIndex);
        cr.setValue(destIndex, Indicator.NULL);
        minVal = currVal;
        maxVal = currVal;
        minIndex = bIndex;
        maxIndex = bIndex;
        dynVal = currVal;
        dynIndex = bIndex;
        preIndex = bIndex;


        for (int i = bIndex + 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(srcIndex);
            if (state == STATE_ZERO) cr.setValue(destIndex, Indicator.NULL);
            switch (state) {
                case STATE_ZERO:
                    state = compareVals(currVal, maxVal, calcMethod, reversalAmount);
                    if (state == STATE_ZERO) {
                        state = compareVals(currVal, minVal, calcMethod, reversalAmount);
                        if (state == STATE_ZERO) {
                            if (currVal > maxVal) {
                                maxVal = currVal;
                                maxIndex = i;
                            } else if (currVal < minVal) {
                                minVal = currVal;
                                minIndex = i;
                            }
                        } else if (minIndex != bIndex) {
                            ChartRecord crTmp = (ChartRecord) al.get(minIndex);
                            crTmp.setValue(destIndex, (int) state);
                            dynVal = currVal;
                            dynIndex = i;
                            preIndex = minIndex;
                        }
                    } else if (maxIndex != bIndex) {
                        ChartRecord crTmp = (ChartRecord) al.get(maxIndex);
                        crTmp.setValue(destIndex, (int) state);
                        dynVal = currVal;
                        dynIndex = i;
                        preIndex = maxIndex;
                    }
                    break;
                case STATE_POSITIVE:
                    tmpState = compareVals(currVal, dynVal, calcMethod, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal > dynVal) {
                                dynVal = currVal;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            dynVal = currVal;
                            dynIndex = i;
                            break;
                        case STATE_NEGATIVE:
                            for (int k = preIndex + 1; k <= dynIndex; k++) {
                                ChartRecord crTmp = (ChartRecord) al.get(k);
                                crTmp.setValue(destIndex, STATE_POSITIVE);
                            }
                            preIndex = dynIndex;
                            //setIndicatorValue(al, destIndex, dynIndex, dynVal);
                            dynVal = currVal;
                            dynIndex = i;
                            state = STATE_NEGATIVE;
                            break;
                    }
                    break;
                case STATE_NEGATIVE:
                    tmpState = compareVals(currVal, dynVal, calcMethod, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal < dynVal) {
                                dynVal = currVal;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            for (int k = preIndex + 1; k <= dynIndex; k++) {
                                ChartRecord crTmp = (ChartRecord) al.get(k);
                                crTmp.setValue(destIndex, STATE_NEGATIVE);
                            }
                            preIndex = dynIndex;
                            //setIndicatorValue(al, destIndex, dynIndex, dynVal);
                            dynVal = currVal;
                            dynIndex = i;
                            state = STATE_POSITIVE;
                            break;
                        case STATE_NEGATIVE:
                            dynVal = currVal;
                            dynIndex = i;
                            break;
                    }
                    break;
            }
            if (i == al.size() - 1) {
                for (int k = preIndex + 1; k < al.size(); k++) {
                    ChartRecord crTmp = (ChartRecord) al.get(k);
                    crTmp.setValue(destIndex, state);
                }
            }
        }
        //System.out.println("**** Inner ZigZagSlope Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public static int getAuxStepCount() {
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        long dynTime = 0, maxTime, minTime;
        int state = STATE_ZERO, tmpState;
        double currVal, dynVal = 0, maxVal, minVal;
        int dynIndex = 0, minIndex = 0, maxIndex = 0, preIndex = 0;
        ChartRecord cr;

        cr = (ChartRecord) al.get(0);
        currVal = cr.getValue(getOHLCPriority());
        GDM.removeIndicatorPoint(cr.Time, index, getID());
        maxVal = currVal;
        maxTime = cr.Time;
        minVal = currVal;
        minTime = cr.Time;

        for (int i = 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.getValue(getOHLCPriority());
            if (state == 0) GDM.removeIndicatorPoint(cr.Time, index, getID());
            switch (state) {
                case STATE_ZERO:
                    state = compareVals(currVal, maxVal, method, reversalAmount);
                    if (state == STATE_ZERO) {
                        state = compareVals(currVal, minVal, method, reversalAmount);
                        if (state == STATE_ZERO) {
                            if (currVal > maxVal) {
                                maxVal = currVal;
                                maxTime = cr.Time;
                                maxIndex = i;
                            } else if (currVal < minVal) {
                                minVal = currVal;
                                minTime = cr.Time;
                                minIndex = i;
                            }
                        } else {
                            addIndicatorPoint(GDM, index, minTime, (int) state);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            dynIndex = i;
                            preIndex = minIndex;
                        }
                    } else {
                        addIndicatorPoint(GDM, index, maxTime, (int) state);
                        dynVal = currVal;
                        dynTime = cr.Time;
                        dynIndex = i;
                        preIndex = maxIndex;
                    }
                    break;
                case STATE_POSITIVE:
                    tmpState = compareVals(currVal, dynVal, method, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal > dynVal) {
                                dynVal = currVal;
                                dynTime = cr.Time;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            dynVal = currVal;
                            dynTime = cr.Time;
                            dynIndex = i;
                            break;
                        case STATE_NEGATIVE:
                            for (int k = preIndex + 1; k <= dynIndex; k++) {
                                ChartRecord crTmp = (ChartRecord) al.get(k);
                                addIndicatorPoint(GDM, index, crTmp.Time, STATE_POSITIVE);
                            }
                            preIndex = dynIndex;
                            //addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            dynIndex = i;
                            state = STATE_NEGATIVE;
                            break;
                    }
                    break;
                case STATE_NEGATIVE:
                    tmpState = compareVals(currVal, dynVal, method, reversalAmount);
                    switch (tmpState) {
                        case STATE_ZERO:
                            if (currVal < dynVal) {
                                dynVal = currVal;
                                dynTime = cr.Time;
                                dynIndex = i;
                            }
                            break;
                        case STATE_POSITIVE:
                            for (int k = preIndex + 1; k <= dynIndex; k++) {
                                ChartRecord crTmp = (ChartRecord) al.get(k);
                                addIndicatorPoint(GDM, index, crTmp.Time, STATE_NEGATIVE);
                            }
                            preIndex = dynIndex;
                            //addIndicatorPoint(GDM, index, dynTime, dynVal);
                            dynVal = currVal;
                            dynTime = cr.Time;
                            dynIndex = i;
                            state = STATE_POSITIVE;
                            break;
                        case STATE_NEGATIVE:
                            dynVal = currVal;
                            dynTime = cr.Time;
                            dynIndex = i;
                            break;
                    }
                    break;
            }
            if (i == al.size() - 1) {
                for (int k = preIndex + 1; k < al.size(); k++) {
                    ChartRecord crTmp = (ChartRecord) al.get(k);
                    addIndicatorPoint(GDM, index, crTmp.Time, state);
                }
                //addIndicatorPoint(GDM, index, dynTime, dynVal);
                //if (dynTime != cr.Time) {
                //    addIndicatorPoint(GDM, index, cr.Time, currVal);
                //}
            }
        }
        //System.out.println("**** Zig Zag Slope Calc time " + (entryTime - System.currentTimeMillis()));
    }

}
