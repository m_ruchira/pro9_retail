package com.isi.csvr.chart;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 21, 2005 - Time: 3:48:10 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class ChartSettings {

    public static boolean isUserSavedColors = false;
    public static boolean userSavingGraphColors = false;
    private static ChartSettings chartSettings = null;
    private boolean currentMode = true;
    private int period = StockGraph.ONE_DAY;
    private int periodIntraday = StockGraph.ONE_DAY;
    private int periodHistory = StockGraph.ONE_YEAR;
    private long intervalIntraday = StockGraph.EVERYMINUTE;
    private long intervalHistory = StockGraph.DAILY;
    private long interval = StockGraph.EVERYMINUTE;
    private boolean showVolume = false;
    private boolean showTurnover = false;
    private boolean showLastPriceLine = false;
    private boolean showPreviousCloseLine = false;
    private boolean showOrderLines = false;
    private boolean showBidAskTags = false;
    private boolean showMinMaxLines = false;
    private boolean openInProChart = false;
    private boolean showVolumeInProChart = false;
    private boolean showTurnOverInProChart = false;
    private boolean showAnnouncement = true;
    private boolean showSplits = true;
    //    public static boolean customGraphColors = false;
    private boolean showVWAP = false;
    private ChartProperties cp = new ChartProperties(null, "", GraphDataManager.ID_BASE, Color.BLACK, null);
    private ChartProperties cpVolume = new ChartProperties(null, "", GraphDataManager.ID_VOLUME, Color.BLACK, null);
    private ChartProperties cpTurnover = new ChartProperties(null, "", GraphDataManager.ID_TURNOVER, Color.BLACK, null);
    private int customPeriod = Calendar.DAY_OF_YEAR;
    private int customRange = 60;
    private long customIntervalType = 24 * 3600L;
    private long customIntervalFactor = 3L;
    private Document document;
    private XPath xpath;
    private String expression;

    private ChartSettings() {
        loadSettingsFromTemplateFile();
    }

    public static String getAttribute(NamedNodeMap attributes, String tagName) {
        String val = "-1";
        try {
            val = attributes.getNamedItem(tagName).getNodeValue();
        } catch (Exception e) {

        }
        return val;
    }

    public static ChartSettings getSettings() {
        if (chartSettings == null) {
            //chartSettings = new ChartSettings();
        }
        return new ChartSettings();
    }

    public void loadSettingsFromTemplateFile() {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new File(TemplateFactory.DEFAULT_TEMPLATE_FILE));

            xpath = XPathFactory.newInstance().newXPath();
            expression = "Template/GraphWindow";
            NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
            Node graphWindow = graphWindows.item(0);
            NamedNodeMap attributes = graphWindow.getAttributes();

            currentMode = "true".equals(attributes.getNamedItem("Mode").getNodeValue());
            period = Integer.parseInt(attributes.getNamedItem("Period").getNodeValue());
            interval = Long.parseLong(attributes.getNamedItem("Interval").getNodeValue());
            showVolume = false;
            showLastPriceLine = "true".equals(attributes.getNamedItem("isShowCurrentPriceLine").getNodeValue());
            showOrderLines = "true".equals(attributes.getNamedItem("isShowOrderLines").getNodeValue());
            showBidAskTags = "true".equals(attributes.getNamedItem("isShowBidAskTags").getNodeValue());
            showMinMaxLines = "true".equals(attributes.getNamedItem("isShowMinMaxLines").getNodeValue());
            showVolumeInProChart = "true".equals(attributes.getNamedItem("isShowVolumeInProchart").getNodeValue());
            showTurnOverInProChart = "true".equals(attributes.getNamedItem("isShowTurnOverInProChart").getNodeValue());
            showAnnouncement = "true".equals(attributes.getNamedItem("isShowAnnouncements").getNodeValue());
            showSplits = "true".equals(attributes.getNamedItem("isShowSplits").getNodeValue());

            //newly added ones----------------------------------------------------------------------------
            showPreviousCloseLine = Boolean.parseBoolean(getAttribute(attributes, "isShowPreviousCloseLine"));
            openInProChart = Boolean.parseBoolean(getAttribute(attributes, "isShowPreviousCloseLine"));
            showVWAP = Boolean.parseBoolean(getAttribute(attributes, "isShowVWAP"));
            int nCustomPeriod = Integer.parseInt(getAttribute(attributes, "periodType"));
            int ncustomRange = Integer.parseInt(getAttribute(attributes, "range"));
            long nCustomIntervalType = Long.parseLong(getAttribute(attributes, "customIntervalType"));
            long ncustomIntervalFactor = Long.parseLong(getAttribute(attributes, "customIntervalFactor"));

            if (nCustomPeriod > -1) customPeriod = nCustomPeriod;
            if (ncustomRange > -1) customRange = ncustomRange;
            if (nCustomIntervalType > -1) customIntervalType = nCustomIntervalType;
            if (ncustomIntervalFactor > -1) customIntervalFactor = ncustomIntervalFactor;
            //--------------------------------------------------------------------------------------------

            expression = "Template/GraphWindow/Chart";
            NodeList charts = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
            if ((charts != null) && (charts.getLength() > 0)) {
                for (int j = 0; j < charts.getLength(); j++) {
                    Node chart = charts.item(j);
                    int chartIndex = (j + 1);
                    byte ID = Byte.parseByte(chart.getAttributes().getNamedItem("ID").getNodeValue());
                    if (ID == GraphDataManager.ID_BASE) {
                        cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                    } else if (ID == GraphDataManager.ID_VOLUME) {
                        //showVolume = true;
                        cpVolume.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                    } else if (ID == GraphDataManager.ID_TURNOVER) {
                        //showVolume = true;
                        cpTurnover.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
    }

    private void updateProperties() {
        try {
            xpath = XPathFactory.newInstance().newXPath();
            expression = "Template/GraphWindow";
            NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
            Node graphWindow = graphWindows.item(0);

            ((Element) graphWindow).setAttribute("Mode", Boolean.toString(currentMode));
            ((Element) graphWindow).setAttribute("Period", Integer.toString(period));
            ((Element) graphWindow).setAttribute("periodType", Integer.toString(customPeriod));
            ((Element) graphWindow).setAttribute("range", Integer.toString(customRange));
            ((Element) graphWindow).setAttribute("customIntervalType", Long.toString(customIntervalType));
            ((Element) graphWindow).setAttribute("customIntervalFactor", Long.toString(customIntervalFactor));
            ((Element) graphWindow).setAttribute("Interval", Long.toString(interval));

            ((Element) graphWindow).setAttribute("isShowCurrentPriceLine", Boolean.toString(showLastPriceLine));
            ((Element) graphWindow).setAttribute("isShowPreviousCloseLine", Boolean.toString(showPreviousCloseLine));
            ((Element) graphWindow).setAttribute("isShowOrderLines", Boolean.toString(showOrderLines));
            ((Element) graphWindow).setAttribute("isShowBidAskTags", Boolean.toString(showBidAskTags));
            ((Element) graphWindow).setAttribute("isShowMinMaxLines", Boolean.toString(showMinMaxLines));
            ((Element) graphWindow).setAttribute("isOpenInProchart", Boolean.toString(openInProChart));
            ((Element) graphWindow).setAttribute("isShowVWAP", Boolean.toString(showVWAP));
            ((Element) graphWindow).setAttribute("isShowVolumeInProchart", Boolean.toString(showVolumeInProChart));
            ((Element) graphWindow).setAttribute("isShowTurnOverInProChart", Boolean.toString(showTurnOverInProChart));
            ((Element) graphWindow).setAttribute("isShowAnnouncements", Boolean.toString(showAnnouncement));
            ((Element) graphWindow).setAttribute("isShowSplits", Boolean.toString(showSplits));


            expression = "Template/GraphWindow/Chart";
            NodeList charts = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
            if ((charts != null) && (charts.getLength() > 0)) {
                for (int j = 0; j < charts.getLength(); j++) {
                    Node chart = charts.item(j);
                    byte ID = Byte.parseByte(chart.getAttributes().getNamedItem("ID").getNodeValue());
                    if (ID == GraphDataManager.ID_BASE) {
                        cp.modifyTemplate((Element) chart, document);
                    } else if (ID == GraphDataManager.ID_VOLUME) {
                        cpVolume.modifyTemplate((Element) chart, document);
                    } else if (ID == GraphDataManager.ID_TURNOVER) {
                        cpTurnover.modifyTemplate((Element) chart, document);
                    }
                }
            }

        } catch (Exception e) {
            System.out.println("Error in updating setting file!");
            e.printStackTrace();
        }
    }

    public synchronized void save() {
        try {
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            Result dest = new StreamResult(new File(TemplateFactory.DEFAULT_TEMPLATE_FILE));
            aTransformer.transform(src, dest);

            //added by shashika for saving intraday and history defaults
            xpath = XPathFactory.newInstance().newXPath();
            expression = "Template/GraphWindow";
            NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
            Node graphWindow = graphWindows.item(0);
            //intraday
            ((Element) graphWindow).setAttribute("Mode", Boolean.toString(true));
            ((Element) graphWindow).setAttribute("Period", Integer.toString(periodIntraday));
            ((Element) graphWindow).setAttribute("Interval", Long.toString(intervalIntraday));
            dest = new StreamResult(new File(TemplateFactory.INTRADAY_DEFAULT_TEMPLATE_FILE));
            aTransformer.transform(src, dest);
            //history
            ((Element) graphWindow).setAttribute("Mode", Boolean.toString(false));
            ((Element) graphWindow).setAttribute("Period", Integer.toString(periodHistory));
            ((Element) graphWindow).setAttribute("Interval", Long.toString(intervalHistory));
            dest = new StreamResult(new File(TemplateFactory.HISTORY_DEFAULT_TEMPLATE_FILE));
            aTransformer.transform(src, dest);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isCurrentMode() {
        return currentMode;
    }

    public boolean isShowVolume() {
        return showVolume;
    }

    public boolean isShowTurnOver() {
        return showTurnover;
    }

    public boolean isShowLastPriceLine() {
        return showLastPriceLine;
    }

    public boolean isShowPreviousCloseLine() {
        return showPreviousCloseLine;
    }

    public boolean isShowOrderLines() {
        return showOrderLines;
    }

    public boolean isShowBidAskTags() {
        return showBidAskTags;
    }

    public boolean isShowMinMaxLines() {
        return showMinMaxLines;
    }


    public boolean isOpenInProChart() {
        return openInProChart;
    }

    public boolean isShowVolumeInProChart() {
        return showVolumeInProChart;
    }

    public boolean isShowAnnouncements() {
        return showAnnouncement;
    }

    public boolean isShowVWAP() {
        return showVWAP;
    }

    public int getPeriod() {
        return period;
    }

    public int getCustomPeriod() {
        return customPeriod;
    }

    public int getCustomRange() {
        return customRange;
    }


    public long getCustomIntervalFactor() {
        return customIntervalFactor;
    }

    public void setCustomIntervalFactor(long customIntervalFactor) {
        this.customIntervalFactor = customIntervalFactor;
    }

    public long getCustomIntervalType() {
        return customIntervalType;
    }

    public void setCustomIntervalType(long customIntervalType) {
        this.customIntervalType = customIntervalType;
    }

    public long getInterval() {
        return interval;
    }

    public ChartProperties getCP() {
        return cp;
    }

    public ChartProperties getVolumeCP() {
        return cpVolume;
    }

    public ChartProperties getTurnoverCP() {
        return cpTurnover;
    }

    public ChartProperties getDefaultCP(ArrayList aList, String smbl, byte ID, Color graphColor, WindowPanel mainRect) {
        ChartProperties aCP = new ChartProperties(aList, smbl, ID, graphColor, mainRect);
        if (ID == GraphDataManager.ID_BASE) {
            aCP.assignValuesFrom(cp);
        } else if (ID == GraphDataManager.ID_VOLUME) {
            aCP.assignValuesFrom(cpVolume);
        } else if (ID == GraphDataManager.ID_TURNOVER) {
            aCP.assignValuesFrom(cpTurnover);
        }
        aCP.setDataSource(aList);
        aCP.setSymbol(smbl);
        aCP.setRect(mainRect);
        return aCP;
    }

    public void setProperties(boolean isCurrentMode, int intPeriod, long longInterval,
                              boolean showLastPriceLine, boolean showPrvCloseLine, boolean showOrderLines, boolean showBidAskTags,
                              boolean showMinMaxLines, ChartProperties aCP, int customPeriod, int customRange,
                              boolean openInProCharts, long customIntervalType, long customIntervalFactor, boolean showVWAP) {
        currentMode = isCurrentMode;
        period = intPeriod;

        if (customPeriod > -1 && customRange > -1) {
            this.customPeriod = customPeriod;
            this.customRange = customRange;
        }

        if (customIntervalType > -1 && customIntervalFactor > -1) {
            this.customIntervalType = customIntervalType;
            this.customIntervalFactor = customIntervalFactor;
        }

        interval = longInterval;
        this.showLastPriceLine = showLastPriceLine;
        this.showPreviousCloseLine = showPrvCloseLine;
        this.showOrderLines = showOrderLines;
        this.showBidAskTags = showBidAskTags;
        this.showMinMaxLines = showMinMaxLines;
        this.openInProChart = openInProCharts;
        this.showVWAP = showVWAP;
        if (aCP.getID() == GraphDataManager.ID_BASE) {
            cp.assignValuesFrom(aCP);
        } else if (aCP.getID() == GraphDataManager.ID_VOLUME) {
            cpVolume.assignValuesFrom(aCP);
        } else if (aCP.getID() == GraphDataManager.ID_TURNOVER) {
            cpTurnover.assignValuesFrom(aCP);
        }
        updateProperties();
        save();
    }

    public void setIntraHistoryProperties(boolean isCurrentMode, int intPeriodIntraday, long longIntervalIntraday, int intPeriodHistory, long longIntervalHistory,
                                          boolean showLastPriceLine, boolean showOrderLines, boolean showBidAskTags,
                                          boolean showMinMaxLines, ChartProperties aCP, int customPeriod, int customRange,
                                          boolean openInProCharts, long customIntervalType, long customIntervalFactor, boolean showVWAP) {
        currentMode = isCurrentMode;
        //period = intPeriod;
        periodIntraday = intPeriodIntraday;
        periodHistory = intPeriodHistory;

        if (customPeriod > -1 && customRange > -1) {
            this.customPeriod = customPeriod;
            this.customRange = customRange;
        }

        if (customIntervalType > -1 && customIntervalFactor > -1) {
            this.customIntervalType = customIntervalType;
            this.customIntervalFactor = customIntervalFactor;
        }

        //interval = longInterval;
        intervalIntraday = longIntervalIntraday;
        intervalHistory = longIntervalHistory;

        this.showLastPriceLine = showLastPriceLine;
        this.showOrderLines = showOrderLines;
        this.showBidAskTags = showBidAskTags;
        this.showMinMaxLines = showMinMaxLines;
        this.openInProChart = openInProCharts;
        this.showVWAP = showVWAP;
        if (aCP.getID() == GraphDataManager.ID_BASE) {
            cp.assignValuesFrom(aCP);
        } else if (aCP.getID() == GraphDataManager.ID_VOLUME) {
            cpVolume.assignValuesFrom(aCP);
        } else if (aCP.getID() == GraphDataManager.ID_TURNOVER) {
            cpTurnover.assignValuesFrom(aCP);
        }
        updateProperties();
        save();
    }


    public void setCustomPeriodProperties(int customPeriod, int customRange) {
        this.customPeriod = customPeriod;
        this.customRange = customRange;
        updateProperties();
        save();
    }

    public void setCustomIntervalProperties(long customIntervalType, long customIntervalFactor) {
        this.customIntervalType = customIntervalType;
        this.customIntervalFactor = customIntervalFactor;
        updateProperties();
        save();
    }
}
