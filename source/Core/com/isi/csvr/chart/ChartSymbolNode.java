package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 16, 2008
 * Time: 4:30:31 PM
 * To change this template use File | Settings | File Templates.
 */

//public class SymbolNode extends ExchangeNode{
public class ChartSymbolNode extends ChartExchangeNode {
    public int type = 0;
    public String exchange;
    public String symbol;
    public int instrumentType = -1;
    public String market = null;
    public String shortDescription;
    public String description;
    public boolean isMatched = false;
    public double openVal = 0.00;
    public double closeVal = 0.00;
    public double highVal = 0.00;
    public double lowVal = 0.00;
    public String bestBid = "";
    public String bestOffer = "";

    public ChartSymbolNode() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getInstrumentType() {
        return this.instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isMatched() {
        return isMatched;
    }

    public void setMatched(boolean matched) {
        isMatched = matched;
    }

    public double getOpenVal() {
        return openVal;
    }

    public void setOpenVal(double openVal) {
        this.openVal = openVal;
    }

    public double getHighVal() {
        return highVal;
    }

    public void setHighVal(double highVal) {
        this.highVal = highVal;
    }

    public double getCloseVal() {
        return closeVal;
    }

    public void setCloseVal(double closeVal) {
        this.closeVal = closeVal;
    }

    public double getLowVal() {
        return lowVal;
    }

    public void setLowVal(double lowVal) {
        this.lowVal = lowVal;
    }

    public String getBestBid() {
        return bestBid;
    }

    public void setBestBid(String bestBid) {
        this.bestBid = bestBid;
    }

    public String getBestOffer() {
        return bestOffer;
    }

    public void setBestOffer(String bestOffer) {
        this.bestOffer = bestOffer;
    }

    public boolean isExchangeType() {
        return false;    //To change body of overridden methods use File | Settings | File Templates.
    }

    public boolean isLeaf() {
        return true;
    }
}