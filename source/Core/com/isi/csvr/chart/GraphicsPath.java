package com.isi.csvr.chart;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: May 14, 2008
 * Time: 4:02:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class GraphicsPath {

    //fill the region bouded by the array of points and the bottom of the region. this will fill the entire region
    public void fillPath(Graphics g, Point2D.Float[] points) {

        for (int i = 0; i < points.length; i++) {

            Graphics2D g2 = (Graphics2D) g;

            //x,y coordinates of the top margin points
            float x1 = (float) points[i].getX();
            float y1 = (float) points[i].getY();

            //x,y coordinates of the bottom line
            float x2 = (float) points[i].getX();
            float y2 = 50f;
            Line2D.Float lineFloat = new Line2D.Float(x1, y1, x2, y2);
            g2.draw(lineFloat);

        }
    }

    //fill the region bouded by the array of points and the bottom of the region. this will fill the entire region
    public void fillPath(Graphics g, Point2D.Float[] points, float yBasis, int width) {

        Graphics2D g2 = (Graphics2D) g;

        int[] x = new int[points.length + 2];
        int[] y = new int[points.length + 2];

        x[0] = (int) points[0].getX();
        y[0] = (int) yBasis;

        for (int i = 1; i <= points.length; i++) {
            x[i] = (int) points[i - 1].getX();
            y[i] = (int) points[i - 1].getY();
        }

        //x[points.length + 1] = width;
        x[points.length + 1] = (int) points[points.length - 1].getX();
        y[points.length + 1] = (int) yBasis;
        g2.fillPolygon(x, y, points.length + 2);


    }

    //connect and draw lines for a given sequence of coordinates
    public void drawPath(Graphics g, Point2D.Float[] points) {

        for (int i = 0; i < points.length - 1; i++) {

            Graphics2D g2 = (Graphics2D) g;

            //x,y coordinates of the first point
            float x1 = (float) points[i].getX();
            float y1 = (float) points[i].getY();
            Point2D.Float point1 = new Point2D.Float(x1, y1);

            //x,y coordinates of the second point
            float x2 = (float) points[i + 1].getX();
            float y2 = (float) points[i + 1].getY();
            Point2D.Float point2 = new Point2D.Float(x2, y2);

            ///draw the line coonecting the above 2 points
            Line2D.Float line = new Line2D.Float(point1, point2);

            g2.draw(line);

        }
    }
}
