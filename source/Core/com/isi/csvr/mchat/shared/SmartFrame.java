package com.isi.csvr.mchat.shared;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SmartFrame extends JFrame implements MouseMotionListener, MouseListener, ActionListener, NonNavigatable {
    Point sp;
    int compStartHeight = 0;
    int compStartWidth = 0;
    int startingX = 0;
    int startingY = 0;
    int minHeight = 100;
    int minWidth = 100;
    ImageIcon maximizeIcon = null;
    ImageIcon maximizeIconRoll = null;
    private JPanel titlePanel = null;
    private JPanel titleMessagePanel = null;
    private JPanel logoPanel = null;
    private JPanel contentPanel = null;
    private JButton minimize = null;
    private JButton maximize = null;
    private JButton closeButton = null;
    private FlexGridLayout layout = null;
    private JLabel titleLabel = null;
    private JLabel titleMessageLabel = null;
    private ImageIcon titleImage = null;
    private String headerWidth = "30";
    private boolean noHeader = false;
    private ImageIcon maximizeMinimizeIcon = null;
    private ImageIcon maximizeMinimizeIconRoll = null;

    public SmartFrame() {
        try {
            setVisible(false);
            setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            try {
                setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100", "100%"}, 0, 0));
            } catch (Exception e) {
                e.printStackTrace();
            }
            setUndecorated(true);
            addMouseMotionListener(this);
            addMouseListener(this);

            titleLabel = new JLabel();
            ImageIcon minimizeIcon = new ImageIcon("images/mchat/mchat_en/minimize.png");
            maximizeIcon = new ImageIcon("images/mchat/mchat_en/maximize.png");
            maximizeMinimizeIcon = new ImageIcon("images/mchat/mchat_en/restore-down.png");
            ImageIcon closeIcon = new ImageIcon("images/mchat/mchat_en/close.png");

            ImageIcon minimizeIconRoll = new ImageIcon("images/mchat/mchat_en/minimize-s.png");
            maximizeIconRoll = new ImageIcon("images/mchat/mchat_en/maximize-s.png");
            ImageIcon closeIconRoll = new ImageIcon("images/mchat/mchat_en/close-s.png");
            maximizeMinimizeIconRoll = new ImageIcon("images/mchat/mchat_en/restore-down-s.png");

            titleImage = new ImageIcon("images/mchat/mchat_en/ChatIcon.png");

            minimize = new JButton(minimizeIcon);
            minimize.setBorderPainted(false);
            minimize.setContentAreaFilled(false);
            minimize.addActionListener(this);
            minimize.setRolloverEnabled(true);
            minimize.setRolloverIcon(minimizeIconRoll);

            maximize = new JButton(maximizeIcon);
            maximize.setBorderPainted(false);
            maximize.setContentAreaFilled(false);
            maximize.addActionListener(this);
            maximize.setRolloverIcon(maximizeIconRoll);

            closeButton = new JButton(closeIcon);
            closeButton.setBorderPainted(false);
            closeButton.setContentAreaFilled(false);
            closeButton.addActionListener(this);
            closeButton.setRolloverIcon(closeIconRoll);

//            minimize.setToolTipText(Language.getString("MINIMISE_WINDOW"));
//            maximize.setToolTipText(Language.getString("MAXIMISE_WINDOW"));
//            closeButton.setToolTipText(Language.getString("CLOSE_WINDOW"));

            logoPanel = new JPanel() {
                public void paint(Graphics g) {
                    paintGradient(g, getWidth(), 100);
                    super.paintChildren(g);
                }
            };

            logoPanel.setOpaque(true);
            logoPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "100%"}, 0, 0));
            logoPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE, 4));

            titlePanel = new JPanel();
            titlePanel.setLayout(new FlexGridLayout(new String[]{"100%", "25", "25", "25"}, new String[]{"100%"}, 0, 0));
            titlePanel.add(titleLabel);
            titlePanel.add(minimize);
            titlePanel.add(maximize);
            titlePanel.add(closeButton);
            titlePanel.setOpaque(false);

            logoPanel.add(titlePanel);
            logoPanel.add(new JLabel());

            contentPanel = new JPanel();

            getContentPane().add(logoPanel);
            getContentPane().add(contentPanel);
            ((JPanel) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.black, 2));

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
        }
    }

    public SmartFrame(boolean noHeader) {
        try {
            try {
                this.noHeader = noHeader;
                setVisible(false);
                setForeground(Theme.getColor("MCHAT_BG_COLOR"));
                try {
                    setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "100%"}, 0, 0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setUndecorated(true);

                addMouseMotionListener(this);
                addMouseListener(this);

                titleLabel = new JLabel();
                titleLabel.setHorizontalAlignment(JLabel.LEADING);
                titleMessageLabel = new JLabel();
                titleMessageLabel.setHorizontalAlignment(JLabel.LEADING);
                titleMessageLabel.setForeground(new Color(10018815));
                if (MChatLanguage.isLTR) {
                    titleMessageLabel.setFont(new Font("Tahoma", Font.PLAIN, 10));
                }
                ImageIcon minimizeIcon = new ImageIcon("images/mchat/mchat_en/minimize.png");
                maximizeIcon = new ImageIcon("images/mchat/mchat_en/maximize.png");
                ImageIcon closeIcon = new ImageIcon("images/mchat/mchat_en/close.png");
                maximizeMinimizeIcon = new ImageIcon("images/mchat/mchat_en/restore-down.png");

                ImageIcon minimizeIconRoll = new ImageIcon("images/mchat/mchat_en/minimize-s.png");
                maximizeIconRoll = new ImageIcon("images/mchat/mchat_en/maximize-s.png");
                ImageIcon closeIconRoll = new ImageIcon("images/mchat/mchat_en/close-s.png");
                maximizeMinimizeIconRoll = new ImageIcon("images/mchat/mchat_en/restore-down-s.png");

                titleImage = new ImageIcon("images/mchat/mchat_en/ChatIcon.png");
                try {
                    setIconImage(Toolkit.getDefaultToolkit().getImage("images/mchat/mchat_en/ChatIcon.png"));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                minimize = new JButton(minimizeIcon);
                minimize.setBorderPainted(false);
                minimize.setContentAreaFilled(false);
                minimize.addActionListener(this);
                minimize.setRolloverEnabled(true);
                minimize.setRolloverIcon(minimizeIconRoll);

                maximize = new JButton(maximizeIcon);
                maximize.setBorderPainted(false);
                maximize.setContentAreaFilled(false);
                maximize.addActionListener(this);
                maximize.setRolloverIcon(maximizeIconRoll);

                closeButton = new JButton(closeIcon);
                closeButton.setBorderPainted(false);
                closeButton.setContentAreaFilled(false);
                closeButton.addActionListener(this);
                closeButton.setRolloverIcon(closeIconRoll);

                logoPanel = new JPanel() {
                    public void paint(Graphics g) {
                        paintGradient(g, getWidth(), 100);
                        super.paintChildren(g);
                    }
                };

                logoPanel.setOpaque(true);
                logoPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "15", "100%"}, 0, 0));
                logoPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE, 4));

                titlePanel = new JPanel();
                titlePanel.setLayout(new FlexGridLayout(new String[]{"100%", "25", "25", "25"}, new String[]{"100%"}, 0, 0));

                titleMessagePanel = new JPanel();
                titleMessagePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
                titleMessagePanel.setOpaque(false);
                titleMessagePanel.add(titleMessageLabel);

                titlePanel.add(titleLabel);
                titlePanel.add(minimize);
                titlePanel.add(maximize);
                titlePanel.add(closeButton);
                titlePanel.setOpaque(false);

                logoPanel.add(titlePanel);
                logoPanel.add(titleMessagePanel);
                logoPanel.add(new JLabel());

                contentPanel = new JPanel();

                getContentPane().add(logoPanel);
                getContentPane().add(contentPanel);
                ((JPanel) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.black, 2));

                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                GUISettings.applyOrientation(this);
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }
    }

    public void setMinSize(int minWidth, int minHeight) {
        this.minWidth = minWidth;
        this.minHeight = minHeight;
    }

    public void setSmartImageIcon(ImageIcon icon) {
        try {
            titleLabel.setIcon(icon);
        } catch (Exception e) {
        }
    }

    public void setDefaultImageIcon() {
        try {
            titleLabel.setIcon(new ImageIcon("images/mchat/mchat_en/chat-window-up-icon.png"));
        } catch (Exception e) {
        }
    }

    public void setTitileFont(Font font) {
        if (MChatLanguage.isLTR) {
            titleLabel.setFont(font);
        }
    }

    public void setTitleColor() {
        titleLabel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        titleLabel.setForeground(Color.white);
    }

    public void setChatWindowMessage(String message) {
        try {
            this.titleMessageLabel.setText(message);
            this.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSmartLayout(LayoutManager layout) {
        contentPanel.setLayout(layout);
    }

    public Container getSmartContentPanel() {
        return contentPanel;
    }

    public void setTitle(String title) {
        super.setTitle(title);
        this.titleLabel.setText(title);
        this.repaint();
    }

    public void setSmartTitle(String title) {
        super.setTitle(title);
    }

    public void mouseMoved(MouseEvent e) {
        try {
            Point p = e.getPoint();
            if (p.y < 5 && (p.x > e.getComponent().getSize().width - 5)) {
                setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
            } else if (p.x < 5 && (p.y > e.getComponent().getSize().height - 5)) {
                setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
            } else if ((p.y > e.getComponent().getSize().height - 5) && (p.x > e.getComponent().getSize().width - 5)) {
                setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
            } else if (p.y > e.getComponent().getSize().height - 5) {
                setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
            } else if (p.x > e.getComponent().getSize().width - 5) {
                setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
            } else if (p.x < 5 && p.y < 5) {
                setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
            } else if (p.x < 5) {
                setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
            } else if (p.y < 5) {
                setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
            } else {
                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception e1) {
        }
    }

    public void mouseDragged(MouseEvent e) {
        try {
            Point p = e.getPoint();
            if (getCursor().getType() == Cursor.N_RESIZE_CURSOR) {
                if (sp.y < 5) {
                    int x = getX() + p.x - sp.x;
                    int y = getY() + p.y - sp.y;
                    int nextHeight = compStartHeight + startingY - y;
                    if (nextHeight > minHeight) {
                        setBounds(x, y, compStartWidth, nextHeight);
                        repaint();
                        setVisible(true);
                    }
                } else {
                    int nextHeight = compStartHeight + p.y - sp.y;
                    if (nextHeight > minHeight) {
                        setSize(getSize().width, nextHeight);
                        repaint();
                        setVisible(true);
                    }
                }
            } else if (getCursor().getType() == Cursor.W_RESIZE_CURSOR) {
                if (sp.x < 5) {
                    int x = getX() + p.x - sp.x;
                    int y = getY() + p.y - sp.y;
                    int nextWidth = compStartWidth + startingX - x;
                    if (nextWidth > minWidth) {
                        setBounds(x, y, nextWidth, compStartHeight);
                        repaint();
                        setVisible(true);
                    }
                } else {
                    int nextWidth = compStartWidth + p.x - sp.x;
                    if (nextWidth > minWidth) {
                        setSize(nextWidth, getSize().height);
                        repaint();
                        setVisible(true);
                    }
                }
            } else if (getCursor().getType() == Cursor.NW_RESIZE_CURSOR) {
                int x = getX() + p.x - sp.x;
                int y = getY() + p.y - sp.y;
                int nextHeight = compStartHeight + startingY - y;
                int nextWidth = compStartWidth + startingX - x;
                if ((nextWidth > minWidth) && (nextHeight > minHeight)) {
                    setBounds(x, y, nextWidth, nextHeight);
                    repaint();
                    setVisible(true);
                }
            } else if (getCursor().getType() == Cursor.SW_RESIZE_CURSOR) {
                int x = getX() + p.x - sp.x;
                int y = getY() + p.y - sp.y;
                int nextHeight = compStartHeight + p.y - sp.y;
                int nextWidth = compStartWidth + startingX - x;
                if ((nextWidth > minWidth) && (nextHeight > minHeight)) {
                    setBounds(x, startingY, nextWidth, nextHeight);
                    repaint();
                    setVisible(true);
                }
            } else if (getCursor().getType() == Cursor.SE_RESIZE_CURSOR) {
                int x = getX() + p.x - sp.x;
                int y = getY() + p.y - sp.y;
                int nextHeight = compStartHeight + p.y - sp.y;
                int nextWidth = compStartWidth + p.x - sp.x;
                if ((nextWidth > minWidth) && (nextHeight > minHeight)) {
                    setBounds(startingX, startingY, nextWidth, nextHeight);
                    repaint();
                    setVisible(true);
                }
            } else if (getCursor().getType() == Cursor.NE_RESIZE_CURSOR) {
                int x = getX() + p.x - sp.x;
                int y = getY() + p.y - sp.y;
                int nextHeight = compStartHeight + startingY - y;
                int nextWidth = compStartWidth + p.x - sp.x;
                if ((nextWidth > minWidth) && (nextHeight > minHeight)) {
                    setBounds(startingX, y, nextWidth, nextHeight);
                    repaint();
                    setVisible(true);
                }
            } else {
                int x = getX() + p.x - sp.x;
                int y = getY() + p.y - sp.y;
                repaint();
                setLocation(x, y);
            }
        } catch (Exception e1) {
        }
    }


    public void mousePressed(MouseEvent e) {
        try {
            sp = e.getPoint();
            compStartHeight = getSize().height;
            compStartWidth = getSize().width;
            startingX = getX();
            startingY = getY();
        } catch (Exception e1) {
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
        if (sp == null) {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        sp = null;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == closeButton) {
            closeButtonActionPerformed();
        } else if (e.getSource() == minimize) {
            setExtendedState(JFrame.ICONIFIED);
        } else if (e.getSource() == maximize) {
            if (getExtendedState() == JFrame.MAXIMIZED_BOTH) {
                setExtendedState(JFrame.NORMAL);
                maximize.setIcon(maximizeIcon);
                maximize.setRolloverIcon(maximizeIconRoll);
            } else {
                setExtendedState(JFrame.MAXIMIZED_BOTH);
                maximize.setIcon(maximizeMinimizeIcon);
                maximize.setRolloverIcon(maximizeMinimizeIconRoll);
            }
        }
    }

    public void closeButtonActionPerformed() {
        setVisible(false);
    }

    private void paintGradient(Graphics g, int width, int height) {
        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;

            titleLeft = new ImageIcon("images/mchat/mchat_en/up-line.jpg");
            titleCenter = new ImageIcon("images/mchat/mchat_en/up-line.jpg");
            if (!noHeader) {
                titleRight = new ImageIcon("images/mchat/mchat_en/up-logo.jpg");
            } else {
                titleRight = new ImageIcon("images/mchat/mchat_en/blank-background.jpg");
            }

            iconWidth = titleLeft.getIconWidth();
            if (iconWidth > 0) {
                titleLeft.paintIcon(null, g, 0, 0);
                xOffset = iconWidth;
            }
            iconWidth = titleCenter.getIconWidth();
            if (iconWidth > 0) {
                for (int i = xOffset; i <= width; i += iconWidth) {
                    titleCenter.paintIcon(null, g, i, 0);
                }
            }
            iconWidth = titleRight.getIconWidth();
            if (iconWidth > 0) {
                titleRight.paintIcon(null, g, width - iconWidth, 0);
            }
        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }
}