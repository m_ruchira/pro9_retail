package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.MChatChatWindow;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatPopupFrame extends SmartFrame {
    public static final int OK_PRESSED = 1;
    public static final int CANCEL_PRESSED = 2;
    private static int returnVal = CANCEL_PRESSED;
    private JPanel buttonPane = null;
    private JPanel labelPane = null;
    private JButton okButton = null;
    private JButton cancelButton = null;
    private JLabel messageLabel = null;
    private String message = null;
    private Component parent = null;

    public MChatPopupFrame() {
        super(true);
    }

    public MChatPopupFrame(String title, String message, Component parent) {
        super(true);
        try {
            setTitle(title);
            setTitleColor();
            setTitileFont(MChatMeta.defaultFont);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            this.message = message;
            this.parent = parent;
            initializeComponents();
        } catch (Exception e) {
        }
    }

    public void initializeComponents() {
        try {
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"65", "30", "100%"}, 0, 0)));

            messageLabel = new JLabel("     " + message);
            messageLabel.setForeground(Color.white);

            if (MChatLanguage.isLTR) {
                messageLabel.setFont(MChatMeta.defaultFont);
            }

            labelPane = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            labelPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            labelPane.add(messageLabel);

            buttonPane = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "5", "80"}, new String[]{"100%"}, 0, 0));
            buttonPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            okButton = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/ok-but.jpg"));
            okButton.setBorder(BorderFactory.createEmptyBorder());
            okButton.setContentAreaFilled(false);
            okButton.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/ok-but-up.jpg"));
            okButton.setFocusPainted(false);

            cancelButton = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but.jpg"));
            cancelButton.setBorder(BorderFactory.createEmptyBorder());
            cancelButton.setContentAreaFilled(false);
            cancelButton.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but-up.jpg"));
            cancelButton.setFocusPainted(false);

            buttonPane.add(new JLabel());
            buttonPane.add(okButton);
            buttonPane.add(new JLabel());
            buttonPane.add(cancelButton);

            getSmartContentPanel().add(labelPane);
            getSmartContentPanel().add(buttonPane);

            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocationRelativeTo(parent);
            setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            setMinSize(420, 140);
            setSize(420, 140);
            setResizable(true);
            setVisible(true);

            okButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        okButtonPressed();
                    } catch (Exception e1) {
                    }
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        cancelButtonPressed();
                    } catch (Exception e1) {
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void okButtonPressed() {
        returnVal = OK_PRESSED;
        MChatChatWindow.tabClose();
        dispose();
    }

    private void cancelButtonPressed() {
        returnVal = CANCEL_PRESSED;
        dispose();
    }
}
