package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.MChat;
import com.isi.csvr.mchat.client.MChatChatForm;
import com.isi.csvr.mchat.file.MChatFileSendGUIPanel;
import com.isi.csvr.mchat.file.MChatFileServerSender;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatScreenCaptureForm extends Thread {
    private SmartFrame frame = null;
    private float screenHeight = 0;
    private float screenWidth = 0;
    private float m = 0;
    private JButton bCacel = null;
    private JButton bSend = null;
    private String user = null;
    private Image image = null;
    private JTextPane displayArea = null;

    public MChatScreenCaptureForm(String user, boolean isCaptured, JTextPane displayArea, String nickname) {
        try {
            this.user = user;
            this.displayArea = displayArea;
            Toolkit tk = Toolkit.getDefaultToolkit();
            Dimension dim = tk.getScreenSize();
            screenHeight = dim.height - 20;
            screenWidth = dim.width;
            m = (screenHeight / screenWidth);
            frame = new SmartFrame(true);
            frame.setVisible(false);
            frame.setSize((int) (screenWidth) / 2 + 50, (int) (screenHeight) / 2 + 100);
            frame.setLocationRelativeTo(com.isi.csvr.Client.getInstance().getFrame());
            frame.setMinSize((int) (screenWidth) / 2 + 50, (int) (screenHeight) / 2 + 100);
            frame.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            frame.setTitle(MChatLanguage.getString("SCREEN_SHOT"));
            frame.setTitileFont(MChatMeta.defaultFontBold);
            frame.setTitleColor();
            JPanel panel = new MChatScreenShotBuilder(isCaptured);
            panel.setOpaque(false);
            panel.setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            frame.setSmartLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"92%", "8%"}, 5, 5));
//            frame.setTitle(Language.getString("SEND_SCREEN"));
            frame.setResizable(false);

            Image im1 = frame.getToolkit().getImage(MChatSettings.IMAGE_PATH + "/ChatIcon.png");
            frame.setIconImage(im1);
            frame.getSmartContentPanel().add(panel);
            JPanel buttonPanel = new JPanel();
            buttonPanel.setLayout(new FlexGridLayout(new String[]{"15%", "15%", "40%", "15%", "15%"}, new String[]{"100%"}, 0, 0));
            buttonPanel.setForeground(Theme.getColor("MCHAT_BG_COLOR"));

            bCacel = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but.jpg"));
            bCacel.setContentAreaFilled(false);
            bCacel.setBorder(BorderFactory.createEmptyBorder());
            bCacel.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but.jpg"));

            bSend = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/send-but.jpg"));
            bSend.setContentAreaFilled(false);
            bSend.setBorder(BorderFactory.createEmptyBorder());
            bSend.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/send-but-up.jpg"));

            buttonPanel.add(new JLabel(""));
            buttonPanel.add(bCacel);
            buttonPanel.add(new JLabel(""));
            buttonPanel.add(bSend);
            buttonPanel.add(new JLabel(""));

            frame.getSmartContentPanel().add(buttonPanel);
            frame.setVisible(true);
            frame.setExtendedState(JFrame.NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }

        final String userFinal = user;
        final MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(Settings.getAbsolutepath() + "Temp/screencapture.jpg", true, null, nickname, false);
        try {
            sendProgressGUIPanel.setDisplayArea(displayArea);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        final JTextPane displayPanel = displayArea;

        bSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible();
                try {
                    try {
                        displayPanel.setCaretPosition(displayPanel.getDocument().getLength());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    try {
                        displayPanel.insertComponent(sendProgressGUIPanel);
                        displayPanel.getDocument().insertString(displayPanel.getDocument().getLength(), "  \n", null);
                    } catch (BadLocationException ea) {
                        ea.printStackTrace();
                    }

                    final MChatFileServerSender fileServerSender = new MChatFileServerSender(MChatSettings.serverFileIP, MChatSettings.serverFilePort, sendProgressGUIPanel);
                    if (fileServerSender.connect()) {
                        new Thread() {
                            public void run() {
                                fileServerSender.sendFileSendingRequest(Settings.getAbsolutepath() + "Temp/screencapture.jpg", userFinal);
                            }
                        }.start();
                        frame.setVisible(false);
                    } else {
                        System.out.println("<DEBUG> File sever connection error !");
                    }
                } catch (Exception e1) {
                }
            }
        });

        bCacel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                setVisible();
            }
        });
        GUISettings.applyOrientation(frame);
    }

    private void setVisible() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    try {
                        if (MChat.getSharedInstance().getExtendedState() == JFrame.ICONIFIED) {
                            MChat.getSharedInstance().setExtendedState(JFrame.NORMAL);
                        }
                        if (MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED) {
                            MChatChatForm.getSharedInstance().setExtendedState(JFrame.NORMAL);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void run() {
        double x = 0;
        double y = 0;
        int xPos = 0;
        int yPos = 0;
        int width = 0;
        int height = 0;

        while (frame != null && frame.isVisible()) {
            try {
                sleepThread(1000);
                x = x + 70;
                y = m * x;
                String xStr = "" + x;
                String yStr = "" + y;
                if (xStr.indexOf(".") > 0) {
                    xStr = xStr.substring(0, xStr.indexOf("."));
                }

                if (yStr.indexOf(".") > 0) {
                    yStr = yStr.substring(0, yStr.indexOf("."));
                }
                xPos = Integer.parseInt(xStr);
                yPos = Integer.parseInt(yStr);

                width = frame.getX() - xPos - 10;
                height = frame.getY() - yPos - 20;

                frame.setBounds(xPos, yPos, width, height);
                if (xPos > screenWidth || yPos > screenHeight) {
                    frame.setVisible(false);
                    frame = null;
                    try {
                        stop();
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    private void sleepThread(int time) {
        try {
            sleep(time);
        } catch (InterruptedException e) {
        }
    }
}
