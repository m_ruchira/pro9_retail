package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.MChatClient;
import com.isi.csvr.mchat.datastore.MChatDataStore;


public class MChatPulseSender extends Thread implements Disposable {
    private static MChatPulseSender self = null;

    public MChatPulseSender() {
        start();
    }

    public static MChatPulseSender getSharedInstance() {
        if (self == null) {
            self = new MChatPulseSender();
        }
        return self;
    }

    public void stopPulse() {
        try {
            stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
//            destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        self = null;
    }

    public void run() {
        while (true) {
            try {
                if (!MChatClient.getSharedInstance().isConnected()) {
                    try {
                        synchronized (this) {
                            wait();
                            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.sendPulse());
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.sendPulse());
                }
                Thread.sleep(3 * 60 * 1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void destroySelf() {
        self = null;
    }
}
