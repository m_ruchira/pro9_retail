package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.neva.Coroutine;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.sound.sampled.AudioFormat;
import javax.swing.*;
import javax.swing.text.StyleConstants;
import java.util.Enumeration;
import java.util.Iterator;

public class MChatSharedMethods {

    private static final int NULL = 183;

    public static String createRegisterRequest() {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.REGISTER_REQUEST + MChatMeta.FS + MChatMeta.USERNAME + MChatMeta.DS + com.isi.csvr.shared.Settings.getUserID() + MChatMeta.FS
                + MChatMeta.NICK_NAME + MChatMeta.DS + MChatMeta.DISPLAY_ID + MChatMeta.FS + MChatMeta.NEW_USER_FNAME + MChatMeta.DS + MChatMeta.FIRST_NAME + MChatMeta.FS +
                MChatMeta.NEW_USER_LNAME + MChatMeta.DS + MChatMeta.LAST_NAME + MChatMeta.FS +
                MChatMeta.END;
    }

    public static String createChatRequest(String destination, String message) {
        String italic = "0";
        String bold = "0";
        if (StyleConstants.isItalic(MChatDataStore.chatTextAttributes)) {
            italic = "1";
        }
        if (StyleConstants.isBold(MChatDataStore.chatTextAttributes)) {
            bold = "1";
        }
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.CHAT + MChatMeta.FS +
                MChatMeta.DES_ID + MChatMeta.DS + destination + MChatMeta.FS +
                MChatMeta.TEXT_MESSAGE + MChatMeta.DS + message + MChatMeta.FS +
                MChatMeta.DISPLAY_MESSEGE_FONT_BOLD + MChatMeta.DS + bold + MChatMeta.FS +
                MChatMeta.DISPLAY_MESSEGE_FONT_ITALIC + MChatMeta.DS + italic + MChatMeta.FS +
                MChatMeta.DISPLAY_MESSEGE_FONT_NAME + MChatMeta.DS + StyleConstants.getFontFamily(MChatDataStore.chatTextAttributes) + MChatMeta.FS +
                MChatMeta.DISPLAY_MESSEGE_FONT_SIZE + MChatMeta.DS + StyleConstants.getFontSize(MChatDataStore.chatTextAttributes) + MChatMeta.FS + MChatMeta.END;
    }

    public static String createConversationRequest(String convID, String originator, String destinationIDs, String message) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.CONVERSATION + MChatMeta.FS + MChatMeta.CONVERSATION + MChatMeta.DS + convID + MChatMeta.FS + MChatMeta.CONVERSATION_ORIGINATOR + MChatMeta.DS + originator + MChatMeta.FS
                + MChatMeta.CONVERSATION_LIST + MChatMeta.DS + destinationIDs + MChatMeta.FS + MChatMeta.CONVERSATION_MESSAGE + MChatMeta.DS + message + MChatMeta.FS + MChatMeta.END;
    }

    public static String createConversationMsg(String source, String message, String conID, String originator, String destinations) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.CONVERSATION + MChatMeta.FS + MChatMeta.DES_ID + MChatMeta.DS + source + MChatMeta.FS + MChatMeta.CONVERSATION_MESSAGE + MChatMeta.DS + message + MChatMeta.FS
                + MChatMeta.CONVERSATION + MChatMeta.DS + conID + MChatMeta.FS + MChatMeta.CONVERSATION_ORIGINATOR + MChatMeta.DS + originator + MChatMeta.FS + MChatMeta.CONVERSATION_LIST + MChatMeta.DS + destinations + MChatMeta.FS + MChatMeta.END;
    }

    public static String createAddContactRequest(String newContact) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.ADD_CONTACT + MChatMeta.FS + MChatMeta.ADD_CONTACT + MChatMeta.DS + newContact + MChatMeta.FS + MChatMeta.END;
    }

    public static String createRemoveContactRequest(String contact) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.REMOVE_CONTACT + MChatMeta.FS + MChatMeta.REMOVE_CONTACT + MChatMeta.DS + contact + MChatMeta.FS + MChatMeta.END;
    }

    public static String sendPulse() {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.PULSE + MChatMeta.FS + MChatMeta.END;
    }

    public static String loginUserRequest(String proID, String loginID, String applicationMode) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.INIT_REQUEST + MChatMeta.FS + MChatMeta.USERNAME + MChatMeta.DS + proID + MChatMeta.FS + MChatMeta.NICK_NAME + MChatMeta.DS + loginID + MChatMeta.FS + MChatMeta.APP_MODE + MChatMeta.DS + applicationMode + MChatMeta.FS + MChatMeta.END;
    }

    public static String loginNickNameRequest(String proID) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.REMOVE_CONTACT + MChatMeta.FS + MChatMeta.REMOVE_CONTACT + MChatMeta.DS + proID + MChatMeta.FS + MChatMeta.END;
    }

    public static String nudgeContactRequest(String contactNickName) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.NUDGE_CONTACT + MChatMeta.FS + MChatMeta.NUDGE_CONTACT + MChatMeta.DS + contactNickName + MChatMeta.FS + MChatMeta.END;
    }


    public static String createLogoutRequest() {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.LOGOUT_REQUEST + MChatMeta.FS + MChatMeta.END;
    }

    public static String createNudgeRequest(String des) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.NUDGE_REQUEST + MChatMeta.FS + MChatMeta.DES_ID + MChatMeta.DS + des + MChatMeta.FS + MChatMeta.END;
    }

    public static String sendInvitationReply(String ans) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.ADD_CONTACT_INVITATION + MChatMeta.FS + MChatMeta.ADD_CONTACT_INVITATION + MChatMeta.DS + ans + MChatMeta.FS + MChatMeta.END;
    }

    public static String searchContacts(String searchSubType, String text) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.SEARCH_CONTACT + MChatMeta.FS + MChatMeta.SEARCH_STRING + MChatMeta.DS + text + MChatMeta.FS + MChatMeta.SEARCH_SUB_TYPE + MChatMeta.DS + searchSubType + MChatMeta.FS + MChatMeta.END;
    }

    public static JRadioButton getSelection(ButtonGroup group) {
        for (Enumeration e = group.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton) e.nextElement();
            if (b.getModel() == group.getSelection()) {
                return b;
            }
        }
        return null;
    }

    public static String createAddNewRequest(String request) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.ADD_NEW_CONTACT + MChatMeta.FS + MChatMeta.ADD_NEW_CONTACT
                + MChatMeta.DS + request + MChatMeta.FS + MChatMeta.END;
    }

    public static String createRejectNewRequest(String request) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.REMOVE_CONTACT + MChatMeta.FS + MChatMeta.REMOVE_CONTACT
                + MChatMeta.DS + request + MChatMeta.FS + MChatMeta.END;
    }

    public static String createCurrentStateChangeNotice(int currentState) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.APPEARING_STATUS + MChatMeta.FS + MChatMeta.USERNAME + MChatMeta.DS + MChatSettings.MUBASHER_ID + MChatMeta.FS + MChatMeta.STATE_CHANGE + MChatMeta.DS + currentState + MChatMeta.FS + MChatMeta.END;
    }

    public static String createPersonalMessageChangeNotice(String message, int show) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.PERSONAL_MESSAGE + MChatMeta.FS + MChatMeta.USERNAME + MChatMeta.DS + MChatSettings.MUBASHER_ID + MChatMeta.FS + MChatMeta.PERSONAL_MESSAGE_CHANGE + MChatMeta.DS + message + MChatMeta.FS + MChatMeta.PERSONAL_SHOWMETO_ALL + MChatMeta.DS + show + MChatMeta.FS + MChatMeta.END;
    }

    public static String createEditContactRequest(String message) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.EDIT_DISPLAY_NAME + MChatMeta.FS +
                MChatMeta.USERNAME + MChatMeta.DS + MChatSettings.MUBASHER_ID + MChatMeta.FS +
                MChatMeta.EDIT_DISPLAY_NAME + MChatMeta.DS + message + MChatMeta.FS + MChatMeta.END;
    }

    public static String createAddContactPersonalInvitationRequest(String proID, String invitation) {
        return MChatMeta.MSG_ID + MChatMeta.DS + MChatMeta.ADD_CONTACT + MChatMeta.FS + MChatMeta.ADD_CONTACT + MChatMeta.DS + proID + MChatMeta.FS + MChatMeta.PERSONAL_INVITAION + MChatMeta.DS + invitation + MChatMeta.FS + MChatMeta.END;
    }

    public static boolean createMutex() {
        /* find the main window of the already running instance */
        Coroutine coro;
        try {
            coro = new Coroutine("Kernel32", "CreateMutexA");
            coro.addArgNull();
            coro.addArg(true);
            coro.addArg("MubasherChatMutex");
            coro.invoke();
            if (coro.lastOsError() == NULL) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        } finally {
            coro = null;
        }
    }

    public static boolean tooManyInstances() {
        try {
            /* C++ definitions
                    #define SW_HIDE             0
                    #define SW_SHOWNORMAL       1
                    #define SW_NORMAL           1
                    #define SW_SHOWMINIMIZED    2
                    #define SW_SHOWMAXIMIZED    3
                    #define SW_MAXIMIZE         3
                    #define SW_SHOWNOACTIVATE   4
                    #define SW_SHOW             5
                    #define SW_MINIMIZE         6
                    #define SW_SHOWMINNOACTIVE  7
                    #define SW_SHOWNA           8
                    #define SW_RESTORE          9
                    #define SW_SHOWDEFAULT      10
                   */
            boolean tooManyInstances = false;

            /* check for multiple instances */

            tooManyInstances = !createMutex(); // if mutex is created, this is the first instance

            if (!tooManyInstances) {
                return false;
            }

            /* find the main window of the already running instance */
            Coroutine coro = new Coroutine("USER32", "FindWindowA");
            coro.addArg("com.isi.csvr.Client");
            coro.addArg((String) null);
            coro.invoke();

            int hwnd = coro.answerAsInteger();
            if (hwnd != 0) {
                /* show the window if it is iconized */
                Coroutine coro2 = new Coroutine("USER32", "ShowWindow");
                coro2.addArg(hwnd);
                coro2.addArg(3);
                coro2.invoke();

                /* bring the window to focus */
                Coroutine coro3 = new Coroutine("USER32", "SetForegroundWindow");
                coro3.addArg(hwnd);
                coro3.invoke();
            }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return true;
        }
    }

    public static String getFormatName(Object o) {
        try {
            // Create an image input stream on the image
            ImageInputStream iis = ImageIO.createImageInputStream(o);

            // Find all image readers that recognize the image format
            Iterator iter = ImageIO.getImageReaders(iis);
            if (!iter.hasNext()) {
                // No readers found
                return null;
            }

            // Use the first reader
            ImageReader reader = (ImageReader) iter.next();

            // Close stream
            iis.close();

            // Return the format name
            return reader.getFormatName();
        } catch (Exception e) {
        }
        // The image could not be read
        return null;
    }

    public static AudioFormat getAudioFormat() {
        float sampleRate = 8000.0F;
        int sampleSizeInBits = 8;
        int channels = 1;
        boolean signed = true;
        boolean bigEndian = false;
        return new AudioFormat(
                sampleRate,
                sampleSizeInBits,
                channels,
                signed,
                bigEndian);
    }

}
