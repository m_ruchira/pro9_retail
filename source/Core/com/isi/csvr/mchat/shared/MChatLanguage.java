package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.datastore.MChatDataStore;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Hashtable;

public class MChatLanguage implements Disposable {

    public static boolean isLTR = true;
    private static Hashtable hashtable;
    private static MChatLanguage self = null;
    private String defaultLan = "eng";
    private FileInputStream lanInput = null;

    private MChatLanguage() {
        MChatDataStore.getSharedInstance().addDisposableListener(this);
    }

    public static MChatLanguage getsharedInstance() {
        if (self == null) {
            self = new MChatLanguage();
        }
        return self;
    }

    public static String getString(String id) {
        String string = null;
        try {
            if (id == null) {
                return "";
            } else {
                string = (String) hashtable.get(id);
            }
            if (string == null) {
                return "";
            } else {
                if (isLTR) {
                    return string;
                } else {
                    return getNativeString(string);
                }
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static String getNativeString(String sUnicode) {
        int i = 0;
        int buffindex = 0;
        char[] buf = new char[sUnicode.length()];
        int iLen = 0;
        char ch;
        char next;
        iLen = sUnicode.length();

        while (i < iLen) {
            ch = getNext(sUnicode, i++);
            if (ch == '\\') {
                if ((next = getNext(sUnicode, i++)) == 'u') {
                    if ((iLen - i) >= 4) {
                        buf[buffindex++] = processUnicode(sUnicode.substring(i, i + 4));
                        i += 4;
                    } else {
                        buf[buffindex++] = '\\';
                        buf[buffindex++] = 'u';
                        while (i < iLen) buf[buffindex++] = sUnicode.charAt(i++);
                        i = iLen;
                    }
                } else if (next == -1) {
                    return (new String(buf, 0, buffindex));
                } else {
                    buf[buffindex++] = '\\';
                    i--;
                }
            } else {
                buf[buffindex++] = ch;
            }
        }
        return (new String(buf, 0, buffindex));
    }

    private static char getNext(String sUnicode, int i) {
        if (i < sUnicode.length())
            return sUnicode.charAt(i);
        else
            return (char) -1;
    }

    private static char processUnicode(String sUnicode) {
        char ch;
        int d = 0;
        loop:
        for (int i = 0; i < 4; i++) {
            ch = sUnicode.charAt(i);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    d = (d << 4) + ch - '0';
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    d = (d << 4) + 10 + ch - 'a';
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    d = (d << 4) + 10 + ch - 'A';
                    break;
                default:
                    break loop;
            }
        }
        return (char) d;
    }

    public void load() {
        hashtable = new Hashtable();
        try {
            isLTR = MChatMeta.isLTR;
            lanInput = new FileInputStream("languages/chat_" + com.isi.csvr.shared.Language.getSelectedLanguage() + ".properties");
        } catch (FileNotFoundException e) {
            try {
                isLTR = true;
                lanInput = new FileInputStream("languages/chat_EN.properties");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        int ch;
        StringBuffer buffer = new StringBuffer();
        String line;

        while (true) {
            try {
                ch = lanInput.read();
                if (ch == -1) {
                    lanInput.close();
                    break;
                } else if ((char) ch == 'Z') {
                    lanInput.close();
                    break;
                } else if ((char) ch != '\n') {
                    buffer.append((char) ch);
                } else {
                    line = buffer.toString();
                    MChatStringTokenizer tokens = null;
                    try {
                        tokens = new MChatStringTokenizer(line, "=");
                        String idPart = tokens.nextToken().trim();
                        String stringPart = tokens.nextToken().trim();
                        hashtable.put(idPart, stringPart);
                    } catch (Exception e) {
//                        System.out.println("<ERROR> Language file line error = " + line);
                    }
                    tokens = null;
                    buffer = new StringBuffer();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void destroySelf() {
        hashtable = null;
        isLTR = true;
        self = null;
    }
}
