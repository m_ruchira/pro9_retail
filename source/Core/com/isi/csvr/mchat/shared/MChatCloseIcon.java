package com.isi.csvr.mchat.shared;

import javax.swing.*;
import java.awt.*;

public class MChatCloseIcon implements Icon {
    public void paintIcon(Component c, Graphics g, int x, int y) {
        try {
            g.setColor(Color.DARK_GRAY);
            g.drawLine(6, 6, getIconWidth() - 7, getIconHeight() - 7);
            g.drawLine(getIconWidth() - 7, 6, 6, getIconHeight() - 7);
        } catch (Exception e) {
        }
    }

    public int getIconWidth() {
        return 17;
    }

    public int getIconHeight() {
        return 17;
    }
}
