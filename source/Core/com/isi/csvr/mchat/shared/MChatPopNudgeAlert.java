package com.isi.csvr.mchat.shared;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;


public class MChatPopNudgeAlert extends JFrame {
    private static MChatPopNudgeAlert self;
    JPanel contentPane;
    String message = "";
    String sender = "";
    Border border = BorderFactory.createEtchedBorder();
    Border botTitileborder = BorderFactory.createTitledBorder(border, "");
    int i = 0;

    public MChatPopNudgeAlert(String ms, String sd) {
        try {
            this.setResizable(false);
            this.setAlwaysOnTop(true);
            this.message = ms;
            this.sender = sd;
            Image im = Toolkit.getDefaultToolkit().getImage("images/mchat/mchat_en/ChatIcon.png");
            this.setIconImage(im);
            setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            setTitle(MChatLanguage.getString("ALERT"));
            String[] namePanelWidths5 = {"100%"};
            String[] namePanelHeights5 = {"50%", "50%"};

            FlexGridLayout flexGridLayout5 = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 0);
//            contentPane = (JPanel) this.getContentPane();
//            contentPane.setBackground(Color.orange);
            contentPane = new JPanel() {
                public void paint(Graphics g) {
                    paintAlert(g, getWidth(), getHeight());
                    super.paintChildren(g);
                }
            };
            contentPane.setLayout(flexGridLayout5);
            contentPane.setForeground(Theme.getColor("MCHAT_BG_COLOR"));

            JPanel top = new JPanel();
            top.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            contentPane.add(createUpper());
            contentPane.add(createLower());
            contentPane.setBorder(botTitileborder);
            getContentPane().add(contentPane);
            Timer timer = new Timer();
            timer.schedule(new DisposingWindow(this), 0, 50);
            GUISettings.applyOrientation(this);
        } catch (SecurityException e) {
        }
    }

    public static MChatPopNudgeAlert getSharedInstance(String ms, String sd) {
        self = new MChatPopNudgeAlert(ms, sd);
        return self;
    }

    private void paintAlert(Graphics g, int width, int height) {
        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;

            titleLeft = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");
            titleCenter = new ImageIcon("images/mchat/mchat_en/alert.jpg");
            titleRight = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");

            iconWidth = titleLeft.getIconWidth();
            if (iconWidth > 0) {
                titleLeft.paintIcon(null, g, 0, 0);
                xOffset = iconWidth;
            }
            iconWidth = titleCenter.getIconWidth();
            if (iconWidth > 0) {
                for (int i = xOffset; i <= width; i += iconWidth) {
                    titleCenter.paintIcon(null, g, i, 0);
                }
            }
            iconWidth = titleRight.getIconWidth();
            if (iconWidth > 0) {
                titleRight.paintIcon(null, g, width - iconWidth, 0);
            }
        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }

    public JPanel createUpper() {
        JPanel upper = null;
        try {
            upper = new JPanel();
            String[] namePanelWidths5 = {"50%", "50%"};
            String[] namePanelHeights5 = {"100%"};
            FlexGridLayout flexGridLayoutu = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 5);
            upper.setLayout(flexGridLayoutu);
            upper.setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            upper.add(new JLabel(new ImageIcon("images/mchat/mchat_en/alert.png")));
            JLabel lb_alert = new JLabel(MChatLanguage.getString("ALERT"));
            if (MChatLanguage.isLTR) {
                lb_alert.setFont(MChatMeta.defaultFont);
            }
            lb_alert.setForeground(Color.RED);
            upper.add(lb_alert);
        } catch (Exception e) {
        }
        return upper;
    }

    public JPanel createLower() {
        JPanel lower = null;
        try {
            lower = new JPanel();
            String[] namePanelWidths5 = {"100%"};
            String[] namePanelHeights5 = {"50%", "50%"};
            FlexGridLayout flexGridLayoutl = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 5);
            lower.setLayout(flexGridLayoutl);
            lower.setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            JLabel lb_message = new JLabel();
            lb_message.setHorizontalAlignment(SwingConstants.CENTER);
            lb_message.setForeground(Color.black);
            lb_message.setText(sender + " " + message);
            if (MChatLanguage.isLTR) {
                lb_message.setFont(MChatMeta.defaultFont);
            }
            JLabel lb_sender = new JLabel();
            lb_sender.setHorizontalAlignment(SwingConstants.CENTER);
            lower.add(lb_message);
            lower.add(lb_sender);
        } catch (Exception e) {
        }
        return lower;
    }

    public class DisposingWindow extends TimerTask {
        JFrame alert;

        public DisposingWindow(JFrame al) {
            this.alert = al;
        }

        public void run() {
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            if (i % 4 == 0) {
                alert.setLocation(dim.width - 250, dim.height - 120);
                contentPane.repaint();
            } else if (i % 4 == 1) {
                alert.setLocation(dim.width - 250, dim.height - 130);
                contentPane.repaint();
            } else if (i % 4 == 2) {
                alert.setLocation(dim.width - 240, dim.height - 130);
                contentPane.repaint();
            } else if (i % 4 == 3) {
                alert.setLocation(dim.width - 240, dim.height - 120);
                contentPane.repaint();
            }
            if (i > 40) {
                i = 0;
                alert.dispose();
                this.cancel();
            }
            i++;
        }
    }
}
