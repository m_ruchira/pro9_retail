/**
 * Created by IntelliJ IDEA.
 * User: chanaka
 * Date: Feb 22, 2008
 * Time: 2:04:55 PM
 * To change this template use File | Settings | File Templates.
 */
package com.isi.csvr.mchat.shared;


import com.isi.csvr.mchat.client.MChatChatForm;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class MChatFontSelector extends JFrame {

    // Results:

    /**
     * The index of the default size (e.g., 14 point == 4)
     */
    protected static final int DEFAULT_SIZE = 4;
    public static MChatFontSelector self = null;
    public static String fontName = null;
    private static SimpleAttributeSet attributes = null;
    /**
     * The font the user has chosen
     */
    protected Font resultFont;

    // Working fields
    /**
     * The resulting font name
     */
    protected String resultName;
    /**
     * The resulting font size
     */
    protected int resultSize;
    /**
     * The resulting boldness
     */
    protected boolean isBold;
    /**
     * The resulting italicness
     */
    protected boolean isItalic;
    /**
     * Display text
     */
    protected String displayText = "Mubasher";
    /**
     * The list of Fonts
     */
    protected String fontList[];
    /**
     * The font name chooser
     */
    protected List fontNameChoice;
    /**
     * The font size chooser
     */
    protected List fontSizeChoice;
    /**
     * The list of font sizes
     */
    protected String fontSizes[] = {"8", "10", "11", "12", "14", "16", "18",
            "20", "24", "30", "36"};
    /**
     * The display area. Use a JLabel as the AWT label doesn't always honor
     * setFont() in a timely fashion :-)
     */
    protected JLabel previewArea;
    /**
     * The bold and italic choosers
     */
    Checkbox bold, italic;

    /**
     * Construct a FontChooser -- Sets title and gets array of fonts on the
     * system. Builds a GUI to let the user choose one font at one size.
     */
    public MChatFontSelector() {
        super(MChatLanguage.getString("CHOOSE_FONT"));
        Rectangle rec = MChatChatForm.getSharedInstance().getBounds();
        setLocation((int) rec.getX() - 100, (int) rec.getY() + 50);
        Container cp = getContentPane();

        Panel top = new Panel();
        top.setLayout(new FlowLayout());

        fontNameChoice = new List(8);
        top.add(fontNameChoice);

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        // For JDK 1.1: returns about 10 names (Serif, SansSerif, etc.)
        // fontList = toolkit.getFontList();
        // For JDK 1.2: a much longer list; most of the names that come
        // with your OS (e.g., Arial), plus the Sun/Java ones (Lucida,
        // Lucida Bright, Lucida Sans...)
        fontList = GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getAvailableFontFamilyNames();


        for (int i = 0; i < fontList.length; i++)
            fontNameChoice.add(fontList[i]);
        fontNameChoice.select(0);

        fontNameChoice.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                try {
                    previewFont();
                } catch (Exception e1) {
                }
            }
        });

        fontSizeChoice = new List(8);

        fontSizeChoice.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                try {
                    previewFont();
                } catch (Exception e1) {
                }
            }
        });
        top.add(fontSizeChoice);

        for (int i = 0; i < fontSizes.length; i++)
            fontSizeChoice.add(fontSizes[i]);
        fontSizeChoice.select(DEFAULT_SIZE);

        cp.add(top, BorderLayout.NORTH);

        Panel attrs = new Panel();
        top.add(attrs);
        attrs.setLayout(new GridLayout(0, 1));
        attrs.add(bold = new Checkbox(MChatLanguage.getString("BOLD"), false));
        attrs.add(italic = new Checkbox(MChatLanguage.getString("ITALIC"), false));

        previewArea = new JLabel(displayText, JLabel.CENTER);
        previewArea.setSize(200, 50);
        cp.add(previewArea, BorderLayout.CENTER);

        Panel bot = new Panel();

        JButton okButton = new JButton(MChatLanguage.getString("APPLY"));
        bot.add(okButton);
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                previewFont();
                dispose();
                setVisible(false);
            }
        });

        JButton pvButton = new JButton(MChatLanguage.getString("PREVIEW"));
        bot.add(pvButton);
        pvButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                previewFont();
            }
        });

        JButton canButton = new JButton(MChatLanguage.getString("CANCEL"));
        bot.add(canButton);
        canButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Set all values to null. Better: restore previous.
                resultFont = null;
                resultName = null;
                resultSize = 0;
                isBold = false;
                isItalic = false;

                dispose();
                setVisible(false);
            }
        });

        cp.add(bot, BorderLayout.SOUTH);
        cp.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

        previewFont(); // ensure view is up to date!

        pack();
        setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        attributes = new SimpleAttributeSet();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(100, 100);
    }

    public static void setAttributes(int size, Color color, boolean bold, boolean italic, String name) {
        try {
            StyleConstants.setFontSize(attributes, size);
            StyleConstants.setForeground(attributes, color);
            StyleConstants.setBold(attributes, bold);
            StyleConstants.setItalic(attributes, italic);
            StyleConstants.setFontFamily(attributes, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static SimpleAttributeSet getAttributeSet() {
        return attributes;
    }

    public static MChatFontSelector getSharedInstance() {
        if (self == null) {
            self = new MChatFontSelector();
        }
        return self;
    }

    public String getFontName() {
        return fontName;
    }

    /**
     * Called from the action handlers to get the font info, build a font, and
     * set it.
     */
    protected void previewFont() {
        resultName = fontNameChoice.getSelectedItem();
        String resultSizeName = fontSizeChoice.getSelectedItem();
        int resultSize = Integer.parseInt(resultSizeName);
        isBold = bold.getState();
        isItalic = italic.getState();
        int attrs = Font.PLAIN;
        if (isBold)
            attrs = Font.BOLD;
        if (isItalic)
            attrs |= Font.ITALIC;
        resultFont = new Font(resultName, attrs, resultSize);
        // System.out.println("resultName = " + resultName + "; " +
        //     "resultFont = " + resultFont);
        previewArea.setFont(resultFont);
        pack(); // ensure Dialog is big enough.
    }

    /**
     * Retrieve the selected font name.
     */
    public String getSelectedName() {
        return resultName;
    }

    /**
     * Retrieve the selected size
     */
    public int getSelectedSize() {
        return resultSize;
    }

    /**
     * Retrieve the selected font, or null
     */
    public Font getSelectedFont() {
        return resultFont;
    }
}
