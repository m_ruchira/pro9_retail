package com.isi.csvr.mchat.shared;

import com.isi.csvr.shared.Settings;
import com.isi.csvr.theme.Theme;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MChatScreenShotBuilder extends JPanel {
    BufferedImage image;
    double scale = .5;

    public MChatScreenShotBuilder(boolean isCaptured) {
        String path = Settings.getAbsolutepath() + "Temp/screencapture.jpg";
        setForeground(Theme.getColor("MCHAT_BG_COLOR"));
        if (!isCaptured) {
            try {
                BufferedImage screencapture = new Robot().createScreenCapture(
                        new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

                // Save as JPEG
                File file = new File(path);
                ImageIO.write(screencapture, "jpg", file);
            } catch (AWTException e) {

            } catch (IOException e) {
            }
        }

        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void paintComponent(Graphics g) {
        try {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            int w = getWidth();
            int h = getHeight();
            int iw = image.getWidth();
            int ih = image.getHeight();
            double x = (w - scale * iw) / 2;
            double y = (h - scale * ih) / 2;
            AffineTransform at = AffineTransform.getTranslateInstance(x, y);
            at.scale(scale, scale);
            g2.drawRenderedImage(image, at);
            repaint();
        } catch (Exception e) {
        }
    }
}
