package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.MChat;
import com.isi.csvr.mchat.client.MChatChatForm;
import com.isi.csvr.mchat.client.MChatChatWindow;
import com.isi.csvr.mchat.client.MChatMessageProcessor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MChatCloseTabButton extends JPanel implements ActionListener, MouseListener {
    String user = null;
    private JTabbedPane pane;
    private JPopupMenu popup = null;
    private JLabel label = null;

    public MChatCloseTabButton(JTabbedPane pane, int index, String user) {
        try {
            this.user = user;
            this.pane = pane;
            setOpaque(false);
            label = new JLabel(
                    pane.getTitleAt(index),
                    pane.getIconAt(index),
                    JLabel.LEFT);
            add(label);
            Icon closeIcon = new MChatCloseIcon();
            JButton btClose = new JButton("");
            btClose.setPreferredSize(new Dimension(
                    closeIcon.getIconWidth(), closeIcon.getIconHeight()));
            add(btClose);
            btClose.addActionListener(this);
            pane.setTabComponentAt(index, this);
            btClose.addMouseListener(this);
            JMenuItem menuItem;

            //Create the popup menu.

            popup = new JPopupMenu();
            menuItem = new JMenuItem(MChatLanguage.getString("CLOSE"));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        closeTab();
                    } catch (Exception e1) {
                    }
                }
            });
            popup.add(menuItem);
            menuItem = new JMenuItem(MChatLanguage.getString("CLOSE_ALL_BUT_THIS"));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        closeAll(false);
                    } catch (Exception e1) {
                    }
                }
            });
            popup.add(menuItem);
            menuItem = new JMenuItem(MChatLanguage.getString("CLOSE_ALL"));
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        closeAll(true);
                    } catch (Exception e1) {
                    }
                }
            });
            popup.add(menuItem);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeAll(boolean closeME) {
        int size = pane.getTabCount();
        for (int i = 0; i < size; i++) {
            if (!closeME) {
                if (i == pane.indexOfTabComponent(this)) {
                    continue;
                }
            }
            try {
                MChatChatWindow cw = (MChatChatWindow) pane.getComponentAt(i);
                if (cw.isInConversation) {
                    cw.isInConversation = false;
                    cw.convOriginator = null;
                    cw.convDestinations = null;
                    MChatMessageProcessor.getSharedInstance().getConvMsgHash().remove(cw.conversationID);
                    MChatMessageProcessor.getSharedInstance().getConvHash().remove(cw.conversationID);
                    cw.conversationID = null;
                }
                try {
                    MChatMessageProcessor.getSharedInstance().removeObserver(((MChatChatWindow) pane.getComponentAt(i)));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                try {
                    pane.remove(i);
                } catch (Exception e1) {
                }
                try {
                    MChat.openWindows.remove(cw.getUser());
                    if (MChat.openWindows.size() < 1) {
                        MChatChatForm.getSharedInstance().setVisible(false);
                    }
                } catch (Exception e) {
                }
            } catch (Exception e) {
            }
        }
    }

    private void closeTab() {
        try {
            int i = pane.indexOfTabComponent(this);
            if (i != -1) {
                MChatChatWindow cw = (MChatChatWindow) pane.getComponentAt(i);
                if (cw.isInConversation) {
                    cw.isInConversation = false;
                    cw.convOriginator = null;
                    cw.convDestinations = null;
                    MChatMessageProcessor.getSharedInstance().getConvMsgHash().remove(cw.conversationID);
                    MChatMessageProcessor.getSharedInstance().getConvHash().remove(cw.conversationID);
                    cw.conversationID = null;
                }
                try {
                    MChatMessageProcessor.getSharedInstance().removeObserver(((MChatChatWindow) pane.getComponentAt(i)));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                try {
                    pane.remove(i);
                } catch (Exception e1) {
                }
            }
            MChat.openWindows.remove(user);
            if (MChat.openWindows.size() < 1) {
                MChatChatForm.getSharedInstance().setVisible(false);
            }
        } catch (Exception e1) {
        }
    }

    public void actionPerformed(ActionEvent e) {
        int i = pane.indexOfTabComponent(this);
        if (i != -1) {
            MChatChatWindow cw = (MChatChatWindow) pane.getComponentAt(i);
            if (cw.isInConversation) {
                cw.isInConversation = false;
                cw.convOriginator = null;
                cw.convDestinations = null;
                MChatMessageProcessor.getSharedInstance().getConvMsgHash().remove(cw.conversationID);
                MChatMessageProcessor.getSharedInstance().getConvHash().remove(cw.conversationID);
                cw.conversationID = null;
            }
            try {
                MChatMessageProcessor.getSharedInstance().removeObserver(((MChatChatWindow) pane.getComponentAt(i)));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            pane.remove(i);
        }
        MChat.openWindows.remove(user);
        if (MChat.openWindows.size() < 1) {
            MChatChatForm.getSharedInstance().setVisible(false);
        }
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            try {
                popup.show(this, e.getX(), e.getY());
            } catch (Exception e1) {
            }
        }
    }
}

