package com.isi.csvr.mchat.shared;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MChatQueue {
    private List list;
    private int counter = 0;

    /**
     * Constructor
     */
    public MChatQueue() {
        list = Collections.synchronizedList(new LinkedList());
    }

    public void add(Object object) {
        list.add(object);
    }

    public Object pop() {
        return list.remove(0);
    }

    public int size() {
        return list.size();
    }

    public void clear() {
        list.clear();
    }
}