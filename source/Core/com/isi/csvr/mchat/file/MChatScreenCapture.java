package com.isi.csvr.mchat.file;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MChatScreenCapture {
    public static String capture() throws
            AWTException, IOException {
        // capture the whole screen
        BufferedImage screencapture = new Robot().createScreenCapture(
                new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

        // Save as JPEG
        File file = new File("mchat//screencapture.jpg");
        ImageIO.write(screencapture, "jpg", file);
        return file.getPath();
        // Save as PNG
        // File file = new File("screencapture.png");
        // ImageIO.write(screencapture, "png", file);
    }
}
