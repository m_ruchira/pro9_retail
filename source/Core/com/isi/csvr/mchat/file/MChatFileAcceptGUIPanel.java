package com.isi.csvr.mchat.file;

import com.isi.csvr.mchat.client.MChat;
import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;
import sun.awt.shell.ShellFolder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

public class MChatFileAcceptGUIPanel extends JPanel implements ActionListener {
    private JProgressBar progressBar = null;
    private String filePath = null;
    private JButton openButton = null;
    private JPanel fileIconPanel = null;
    private JPanel progressPanel = null;
    private JPanel fileNamePanel = null;
    private JPanel fileButtonpanel = null;
    private JLabel lbFileName = null;
    private boolean isSend = false;
    private JTextPane displayArea = null;
    private String path = null;
    private JButton okButton = null;
    private JButton noButton = null;
    private JButton saveButton = null;
    private String sFrame = null;
    private String fileReqID = null;
    private String fileSource = null;
    private String fileSize = null;
    private String fileName = null;
    private String desFilePath = null;
    private MouseListener listener = null;

    public MChatFileAcceptGUIPanel(String filePath, boolean isSend, String fullPath, String fileReqID, String fileSource, String fileName, String fileSize) {
        try {
            this.filePath = filePath;
            this.isSend = isSend;
            this.path = fullPath;
            this.fileReqID = fileReqID;
            this.fileSource = fileSource;
            this.fileSize = fileSize;
            this.fileName = fileName;

            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "24", "17", "15", "100%"}, 4, 4));
            setOpaque(false);
            //image
            fileIconPanel = new JPanel();
            fileIconPanel.setLayout(new FlexGridLayout(new String[]{"5", "40", "5"}, new String[]{"100%"}, 1, 1));
            fileIconPanel.setOpaque(false);

            fileIconPanel.add(new JLabel());
            fileIconPanel.add(getFileIcon(filePath));
            fileIconPanel.add(new JLabel());

            //file name
            fileNamePanel = new JPanel(new FlexGridLayout(new String[]{"5", "100%", "5"}, new String[]{"100%"}, 0, 0));
            fileNamePanel.setOpaque(false);

            File file = new File(filePath);
            String message = null;
            if (isSend) {
                message = MChatLanguage.getString("SEND_FILE") + " : " + file.getName();
            } else {
                message = MChatLanguage.getString("RECEIVE_FILE") + " : " + file.getName() + " (" + (Integer.parseInt(fileSize) / 1024) + " KB)";
            }

            listener = new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    try {
                        Desktop.getDesktop().open(new File(path));
                    } catch (IOException e1) {
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    lbFileName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                }

                public void mouseExited(MouseEvent e) {
                }
            };

            lbFileName = new JLabel(message);
            if (MChatLanguage.isLTR) {
                lbFileName.setFont(MChatMeta.defaultFont);
            }

            lbFileName.addMouseListener(listener);

            fileNamePanel.add(new JLabel());
            fileNamePanel.add(lbFileName);
            fileNamePanel.add(new JLabel());
            //progress bar
            progressPanel = new JPanel();
            progressPanel.setLayout(new FlexGridLayout(new String[]{"5", "200", "5"}, new String[]{"100%"}, 1, 1));
            progressPanel.setOpaque(false);

            progressBar = new JProgressBar();
            progressBar.setStringPainted(true);
            progressBar.setString(MChatLanguage.getString("WAITING_TO_RECIEVE_FILE"));
            if (MChatLanguage.isLTR) {
                progressBar.setFont(MChatMeta.smallFont);
            }
            progressBar.setMaximum(100);
            progressBar.setForeground(Color.GREEN);
            progressBar.setBackground(Color.white);
            progressBar.setBorder(BorderFactory.createLineBorder(Color.gray));
            progressBar.setOpaque(false);

            progressPanel.add(new JLabel());
            progressPanel.add(progressBar);
            progressPanel.add(new JLabel());

            okButton = new JButton("<HTML><u>" + MChatLanguage.getString("YES") + "</u><HTML>");
            okButton.setContentAreaFilled(false);
            okButton.setBorder(BorderFactory.createEmptyBorder());
            okButton.addActionListener(this);
            okButton.setForeground(Color.blue);
            okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                okButton.setFont(MChatMeta.defaultFont);
            }

            noButton = new JButton("<HTML><u>" + MChatLanguage.getString("NO") + "</u><HTML>");
            noButton.addActionListener(this);
            noButton.setBorder(BorderFactory.createEmptyBorder());
            noButton.setContentAreaFilled(false);
            noButton.setForeground(Color.blue);
            noButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                noButton.setFont(MChatMeta.defaultFont);
            }

            saveButton = new JButton("<HTML><u>" + MChatLanguage.getString("SAVE") + "</u><HTML>");
            saveButton.addActionListener(this);
            saveButton.setBorder(BorderFactory.createEmptyBorder());
            saveButton.setContentAreaFilled(false);
            saveButton.setForeground(Color.blue);
            saveButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                saveButton.setFont(MChatMeta.defaultFont);
            }

            fileButtonpanel = new JPanel();
            fileButtonpanel.setLayout(new FlexGridLayout(new String[]{"70", "70", "70"}, new String[]{"100%"}, 1, 1));
            fileButtonpanel.setOpaque(false);
            fileButtonpanel.add(okButton);
            fileButtonpanel.add(noButton);
            fileButtonpanel.add(saveButton);

            add(fileIconPanel);
            add(fileNamePanel);
            add(progressPanel);
            add(fileButtonpanel);
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setFilePath(String path) {
        this.path = path;
    }

    public JTextPane getDisplayArea() {
        return displayArea;
    }

    public void setDisplayArea(JTextPane displayArea) {
        this.displayArea = displayArea;
    }

    private JLabel getFileIcon(String path) {
        ImageIcon icon = null;
        try {
            File file = new File(path);
            icon = null;
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                ShellFolder shellFolder = ShellFolder.getShellFolder(file);
                icon = new ImageIcon(shellFolder.getIcon(true));

            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return new JLabel(icon);
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public void actionPerformed(ActionEvent e) {
        final String fileReqIDFinal = fileReqID;
        final String fileSourceFinal = fileSource;
        final String fileNameFinal = fileName;
        final String fileSizeFinal = fileSize;

        final MChatFileServerReader MChatFileServerReader = new MChatFileServerReader(MChatSettings.serverFileIP, MChatSettings.serverFilePort, this);

        if (e.getSource() == okButton) {
            fileButtonpanel.setVisible(false);
            if (MChatFileServerReader.connect()) {
                File f = new File(System.getProperty("user.home") + "/My Received Files/" + fileNameFinal);
                new Thread() {
                    public void run() {
                        MChatFileServerReader.sendFileAcceptRequest(fileReqIDFinal, fileSourceFinal, fileNameFinal, fileSizeFinal, null);
                    }
                }.start();

            }
        } else if (e.getSource() == noButton) {
            fileButtonpanel.setVisible(false);
            progressBar.setVisible(false);
            lbFileName.setText(lbFileName.getText() + " " + MChatLanguage.getString("FILE_REJECTED"));
            lbFileName.setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/graph_tb_open_act.gif"));
            lbFileName.setForeground(Color.red);
            removeMouseListener();
            try {
                if (MChatFileServerReader.connect()) {
                    MChatFileServerReader.sendFileRejectRequest(fileReqIDFinal, fileSourceFinal, fileNameFinal, fileSizeFinal);
                }
            } catch (Exception e5) {
                e5.printStackTrace();
            }
        } else if (e.getSource() == saveButton) {
            fileButtonpanel.setVisible(false);
            desFilePath = showSaveDialog(MChatLanguage.getString("SAVE_A_FILE"), MChatLanguage.getString("SAVE"), MChatLanguage.getString("SAVE_A_FILE"), 'O', new File(fileNameFinal));
            if (desFilePath != null) {
                final MChatFileServerReader MChatFileServerSenderSaveAs = new MChatFileServerReader(MChatSettings.serverFileIP, MChatSettings.serverFilePort, this);
                if (MChatFileServerSenderSaveAs.connect()) {
                    new Thread() {
                        public void run() {
                            MChatFileServerSenderSaveAs.sendFileAcceptRequest(fileReqIDFinal, fileSourceFinal, fileNameFinal, fileSizeFinal, desFilePath);
                        }
                    }.start();
                } else {
                    System.out.println("<DEBUG> File sever connection error !");
                }
            } else {
                fileButtonpanel.setVisible(false);
                progressBar.setVisible(false);
                lbFileName.setText(lbFileName.getText() + " " + MChatLanguage.getString("FILE_REJECTED"));
                lbFileName.setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/graph_tb_open_act.gif"));
                lbFileName.setForeground(Color.red);
                removeMouseListener();
                try {
                    if (MChatFileServerReader.connect()) {
                        MChatFileServerReader.sendFileRejectRequest(fileReqIDFinal, fileSourceFinal, fileNameFinal, fileSizeFinal);
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
            }
        }
    }

    private String showSaveDialog(String dialogTitle, String approveButtonText, String approveButtonToolTip,
                                  char approveButtonMnemonic, File file) {
        JFileChooser files = new JFileChooser();
        files.setDialogTitle(dialogTitle);
        files.setApproveButtonText(approveButtonText);
        files.setApproveButtonMnemonic(approveButtonMnemonic);
        files.setApproveButtonToolTipText(approveButtonToolTip);
        files.setFileSelectionMode(files.FILES_ONLY);
        files.rescanCurrentDirectory();
        files.setMultiSelectionEnabled(false);
        files.setSelectedFile(file);

        int result = files.showSaveDialog(MChat.getSharedInstance());

        if (result == files.APPROVE_OPTION) {
            return files.getSelectedFile().getAbsolutePath();
        } else {
            return null;
        }
    }

    public JLabel getLable() {
        return lbFileName;
    }


    public void reLayout(String message) {
        try {
            removeAll();
            setLayout(null);
            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "10"}, 4, 4));
            fileIconPanel.removeAll();
            fileIconPanel.setLayout(null);
            fileIconPanel.setLayout(new FlexGridLayout(new String[]{"5", "40", "5"}, new String[]{"100%"}, 1, 1));
            fileIconPanel.setOpaque(false);
            fileIconPanel.add(new JLabel());
            fileIconPanel.add(getFileIcon(filePath));
            fileIconPanel.add(new JLabel());
//            fileIconPanel.remove(lbFileName);
            lbFileName.setText(message);
            add(fileIconPanel);
            displayArea.insertComponent(lbFileName);
        } catch (Exception e) {
        }
    }

    public void removeMouseListener() {
        try {
            lbFileName.removeMouseListener(listener);
            lbFileName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (Exception e) {
        }
    }
}
