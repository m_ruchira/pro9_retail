package com.isi.csvr.mchat.file;

import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatSettings;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class MChatFileReader extends Thread {
    public long progValue;
    protected MChatFileServerReader serverReaderMChat = null;
    protected String fileName = null;
    int fileSize = 0;
    String dfPath = "";
    byte[] toread = new byte[1024];
    boolean visited = false;
    private JProgressBar progressBar = null;
    private MChatFileAcceptGUIPanel progressGUIPanelMChat = null;
    private String nickName = null;
    private String fName = null;

    public MChatFileReader(MChatFileServerReader serverMChat, String fName, int fileSize, String desfName, MChatFileAcceptGUIPanel progressGUIPanelMChat, String nickName) {
        try {
            serverReaderMChat = serverMChat;
            fileName = fName;
            this.fileSize = fileSize;
            this.dfPath = desfName;
            this.progressBar = progressGUIPanelMChat.getProgressBar();
            this.progressGUIPanelMChat = progressGUIPanelMChat;
            this.nickName = nickName;
            this.setPriority(Thread.NORM_PRIORITY + 3);
            this.start();
        } catch (Exception e) {
        }
    }

    public void run() {
        try {
            if (serverReaderMChat.isConnected()) {
                read();
                sleepThread(10);
            } else {
                sleepThread(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            sleepThread(5000);
        }
    }

    private void sleepThread(int iTime) {
        try {
            Thread.sleep(iTime);
        } catch (Exception e) {
        }
    }

    private void read() {
        try {
            int len = 0;
            int bytcount = 0;
            byte[] fileRead = new byte[1024];
            int toRead = 1024;
            fName = System.getProperty("user.home") + "/My Received Files/" + fileName;
            File saveDir = null;
            try {
                saveDir = new File(System.getProperty("user.home") + "/My Received Files/");
                saveDir.mkdir();
            } catch (Exception e) {
                e.printStackTrace();
            }
            saveDir = null;

            try {
                FileOutputStream inFile = null;
                try {
                    if (dfPath.equals("")) {
                        inFile = new FileOutputStream(fName);
                    } else {
                        inFile = new FileOutputStream(dfPath);
                    }
                } catch (FileNotFoundException e) {
                }
                int count = 1;

                while (true) {
                    try {
                        if (fileSize > 1024) {
                            byte[] fileRead1 = new byte[toRead];
                            if (serverReaderMChat.bufIn != null && serverReaderMChat.cSocket != null && !serverReaderMChat.cSocket.isClosed()) {
                                try {
                                    len = serverReaderMChat.bufIn.read(fileRead1);
                                } catch (IOException e) {
                                    fileTransferError();
                                    break;
                                }
                                if (len < 0) {
                                    fileTransferError();
                                    break;
                                }
                            } else {
                                fileTransferError();
                                break;
                            }
                            bytcount += len;
                            inFile.write(fileRead1, 0, len);
                            count++;
                            progressCalculator(bytcount, fileSize);
                            if (bytcount == fileSize) {
                                break;
                            }

                            if ((fileSize - bytcount) < 1024) {
                                fileRead1 = null;
                                toRead = fileSize - bytcount;
                            }

                        } else {
                            byte[] fileRead1 = new byte[fileSize];
                            len = serverReaderMChat.bufIn.read(fileRead1, 0, fileSize);
                            bytcount += len;
                            inFile.write(fileRead1);
                            progressCalculator(bytcount, fileSize);
                            fileRead1 = null;
                            inFile.close();
                            break;
                        }
                    } catch (Exception e) {
                        fileTransferError();
                        break;
                    }
                    count++;
                }
                if (bytcount != fileSize) {
                    fileTransferError();
                }
                try {
                    inFile.close();
                } catch (IOException e) {
                }
            } catch (Exception e) {
                fileTransferError();
            }
            serverReaderMChat.close();
            serverReaderMChat.isConnected = false;
        } catch (Exception e) {
        }
    }

    private void fileTransferError() {
        progressGUIPanelMChat.removeMouseListener();
        progressGUIPanelMChat.getLable().setText(MChatLanguage.getString("FILE_FAILED") + " " + fileName);
        progressGUIPanelMChat.getLable().setForeground(Color.red);
        progressGUIPanelMChat.getLable().setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/graph_tb_open_act.gif"));
        progressGUIPanelMChat.getProgressBar().setVisible(false);
        try {
            serverReaderMChat.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        MChatSettings.setFileTranferInProgress(false);
    }

    public void progressCalculator(int byteCount, long fsize) {
        try {
            double b = byteCount;
            double f = fsize;

            progValue = Math.round((b / f) * 100);

//            if (((int) progValue % 4) == 0) {
            if (progValue >= progressBar.getMaximum()) {
                MChatSettings.setFileTranferInProgress(false);
                progressBar.setValue(progressBar.getMaximum());
                progressBar.setString(100 + "%");
                try {
                    if (!visited) {
                        progressBar.setVisible(false);
                        progressGUIPanelMChat.getLable().setForeground(Color.blue);
                        try {
                            if (dfPath.equals("")) {
                                progressGUIPanelMChat.getLable().setText("<HTML><span> " + MChatLanguage.getString("FILE_RECIEVED") + " </span><u>" + fName + "</u></HTML>");
                                progressGUIPanelMChat.reLayout("<HTML><span> " + MChatLanguage.getString("FILE_RECIEVED") + " </span><u>" + fName + "</u><span></HTML>");

                            } else {
                                progressGUIPanelMChat.setFilePath(dfPath);
                                progressGUIPanelMChat.reLayout("<HTML><span> " + MChatLanguage.getString("FILE_RECIEVED") + " </span><u>" + dfPath + "</u><span></HTML>");
//                                progressGUIPanelMChat.getLable().setText("<HTML><span> "+ MChatLanguage.getString("FILE_RECIEVED")+" </span><u>" + dfPath + "</u><span></HTML>");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        progressGUIPanelMChat.getLable().setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/complete.jpg"));
                        progressGUIPanelMChat.getDisplayArea().getDocument().insertString(progressGUIPanelMChat.getDisplayArea().getDocument().getLength(), ("\n"), null);
                        visited = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MChatSettings.setFileTranferInProgress(true);
                progressBar.setStringPainted(true);
                progressBar.setString(progValue + "%");
                progressBar.setValue((int) progValue);
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
