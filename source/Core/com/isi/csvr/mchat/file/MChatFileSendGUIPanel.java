package com.isi.csvr.mchat.file;

import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatPaintedImagePanel;
import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;
import sun.awt.shell.ShellFolder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

public class MChatFileSendGUIPanel extends JPanel {

    private static boolean isActionEnabled = true;
    private JProgressBar progressBar = null;
    private String filePath = null;
    private JPanel fileNamePanel = null;
    private JPanel fileIconPanel = null;
    private JPanel progressPanel = null;
    private JPanel fileButtonpanel = null;
    private JLabel lbFileName = null;
    private boolean isSend = false;
    private JTextPane displayArea = null;
    private String path = null;
    private JButton noButton = null;
    private JButton openButton = null;
    private String recieverName = null;
    private MChatFileServerSender sender = null;
    private boolean isCancelled = false;
    private MouseListener listener = null;

    public MChatFileSendGUIPanel(String filePath, boolean isSend, String fullPath, String recieverName, boolean isImageFile) {
        try {
            this.filePath = filePath;
            this.isSend = isSend;
            this.path = fullPath;
            this.recieverName = recieverName;
            setPreferredSize(new Dimension(200, 110));

            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "50", "15", "17", "15", "100%"}, 4, 4));
            setOpaque(false);

            //image
            fileIconPanel = new JPanel();
            fileIconPanel.setLayout(new FlexGridLayout(new String[]{"15", "50", "15"}, new String[]{"100%"}, 1, 1));
            fileIconPanel.setOpaque(false);

            fileIconPanel.add(new JLabel());
            if (isImageFile) {
                if (getThubnailImage(filePath) != null) {
                    fileIconPanel.add(getThubnailImage(filePath));
                } else {
                    fileIconPanel.add(getFileIcon(filePath));
                }
            } else {
                fileIconPanel.add(getFileIcon(filePath));
            }
            fileIconPanel.add(new JLabel());

            //file name
            fileNamePanel = new JPanel(new FlexGridLayout(new String[]{"5", "300", "5"}, new String[]{"100%"}, 0, 0));
            fileNamePanel.setOpaque(false);

            File file = new File(filePath);
            String message = null;
            if (isSend) {
                message = MChatLanguage.getString("SEND_FILE") + " : " + file.getName() + " (" + (file.length() / 1024) + " KB)";
            } else {
                message = MChatLanguage.getString("RECEIVE_FILE") + " : " + file.getName() + " (" + (file.length() / 1024) + ")";
            }

            listener = new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    try {
                        Desktop.getDesktop().open(new File(path));
                    } catch (IOException e1) {
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    lbFileName.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                }

                public void mouseExited(MouseEvent e) {
                }
            };

            lbFileName = new JLabel(message);
            if (MChatLanguage.isLTR) {
                lbFileName.setFont(MChatMeta.defaultFont);
            }

            lbFileName.addMouseListener(listener);

            fileNamePanel.add(new JLabel());
            fileNamePanel.add(lbFileName);
            fileNamePanel.add(new JLabel());
            //progress bar
            progressPanel = new JPanel();
            progressPanel.setLayout(new FlexGridLayout(new String[]{"5", "200", "5"}, new String[]{"100%"}, 1, 1));
            progressPanel.setOpaque(false);

            progressBar = new JProgressBar();
            progressBar.setStringPainted(true);
            progressBar.setString(MChatLanguage.getString("FILE_SEND_WAITING") + " " + recieverName + " " + MChatLanguage.getString("FILE_SEND_WAITING_ACCEPT"));
            if (MChatLanguage.isLTR) {
                progressBar.setFont(MChatMeta.smallFont);
            }
            progressBar.setMaximum(100);
            progressBar.setForeground(Color.GREEN);
            progressBar.setBackground(Color.white);
            progressBar.setBorder(BorderFactory.createLineBorder(Color.gray));
            progressBar.setOpaque(false);

            progressPanel.add(new JLabel());
            progressPanel.add(progressBar);
            progressPanel.add(new JLabel());

            noButton = new JButton("<HTML><u>" + MChatLanguage.getString("CANCEL") + "</u><HTML>");
            noButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        cancelled();
                        getLable().setText(MChatLanguage.getString("FILE_TRANSFER_CANCELLED"));
                        getLable().setForeground(Color.red);
                        setCancelButtonInvisible();
                        removeMouseListener();
                        progressBar.setVisible(false);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
            noButton.setBorder(BorderFactory.createEmptyBorder());
            noButton.setContentAreaFilled(false);
            noButton.setForeground(Color.blue);
            noButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            if (MChatLanguage.isLTR) {
                noButton.setFont(MChatMeta.defaultFont);
            }

            openButton = new JButton("<HTML><u>" + MChatLanguage.getString("OPEN") + "</u><HTML>");
            openButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

            openButton.setBorder(BorderFactory.createEmptyBorder());
            openButton.setContentAreaFilled(false);
            openButton.setForeground(Color.blue);
            openButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            if (MChatLanguage.isLTR) {
                openButton.setFont(MChatMeta.defaultFont);
            }

            fileButtonpanel = new JPanel();
            fileButtonpanel.setLayout(new FlexGridLayout(new String[]{"70", "100%"}, new String[]{"100%"}, 1, 1));
            fileButtonpanel.setOpaque(false);
            fileButtonpanel.add(noButton);
            fileButtonpanel.add(new JLabel());

            JLabel sends = new JLabel(MChatMeta.DISPLAY_ID + " " + MChatLanguage.getString("SENDS"));
            sends.setForeground(Color.gray);
            if (MChatLanguage.isLTR) {
                sends.setFont(MChatMeta.defaultFont);
            }
            add(sends);
            add(fileIconPanel);
            add(fileNamePanel);
            add(progressPanel);
            add(fileButtonpanel);
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setActionEnable(boolean value) {
        isActionEnabled = value;
    }

    public boolean isCancelClicked() {
        return isCancelled;
    }

    private synchronized void cancelled() {
        isCancelled = true;
    }

    public void setCancelButtonInvisible() {
        noButton.setVisible(false);
        fileButtonpanel.removeAll();
        fileButtonpanel.add(openButton);
        fileButtonpanel.add(new JLabel());
        fileButtonpanel.updateUI();
        updateUI();
    }

/*
    public void setFileServerSender(FileServerSender sender) {
        this.sender = sender;
    }
*/

    public void removeMouseListener() {
        try {
            lbFileName.removeMouseListener(listener);
            lbFileName.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } catch (Exception e) {
        }
    }

    public void setFilePath(String path) {
        this.path = path;
    }

    public JTextPane getDisplayArea() {
        return displayArea;
    }

    public void setDisplayArea(JTextPane displayArea) {
        this.displayArea = displayArea;
    }

    private JLabel getFileIcon(String path) {
        ImageIcon icon = null;
        try {
            File file = new File(path);
            icon = null;
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                ShellFolder shellFolder = ShellFolder.getShellFolder(file);
                icon = new ImageIcon(shellFolder.getIcon(true));

            } catch (Exception e) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JLabel(icon);
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public JLabel getLable() {
        return lbFileName;
    }

    private JPanel getThubnailImage(String path) {
        JPanel imagePanel = null;
        try {
            imagePanel = new MChatPaintedImagePanel(path, 0.1);
        } catch (Exception e) {
            imagePanel = null;
        }
        return imagePanel;
    }
}
