package com.isi.csvr.mchat.file;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;

import javax.swing.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.TimeZone;

public class MChatFileServerReader implements ApplicationListener {
    public boolean isConnected = false;
    public JProgressBar pro = null;
    public JLabel link = new JLabel();
    protected Socket cSocket = null;
    protected BufferedOutputStream bufOut = null;
    protected BufferedInputStream bufIn = null;
    long fsize = 0;
    int kbytes = 1;
    int bytcount = MChatMeta.BYTE_LEN;
    private String serverIP = null;
    private int serverPort = 0;
    private String destFilepath = "";
    private long progValue = 0;
    private JProgressBar progressBar = null;
    private MChatFileAcceptGUIPanel progressGUIPanelMChat = null;

    public MChatFileServerReader(String serverIP, int port, MChatFileAcceptGUIPanel progressGUIPanelMChat) {
        try {
            this.serverIP = serverIP;
            this.serverPort = port;
            this.progressBar = progressGUIPanelMChat.getProgressBar();
            this.progressGUIPanelMChat = progressGUIPanelMChat;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendFileAcceptRequest(String reqID, String src, String fName, String length, String desFilePath) {
        try {
            byte[] temp;
            byte[] buf = new byte[MChatMeta.BYTE_LEN];
            String messageType = MChatMeta.FILE_ACCEPT_REQUEST;
            String source = src;
            String destination = MChatSettings.MUBASHER_ID;
            String name = fName;
            String size = "" + length;
            String requestID = "" + reqID;

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());

            temp = name.getBytes();
            System.arraycopy(temp, 0, buf, 52, name.length());

            temp = size.getBytes();
            System.arraycopy(temp, 0, buf, 180, size.length());

            write(buf, 0, MChatMeta.BYTE_LEN);
            flush();
            if (desFilePath != null) {
                this.destFilepath = desFilePath;
            }
            new MChatFileReader(this, name, Integer.parseInt(size), destFilepath, progressGUIPanelMChat, null);
        } catch (Exception e) {
        }
    }

    public JLabel getLink() {
        return link;
    }

    private void write(byte[] b, int off, int len) {
        try {
            if (bufOut != null) {
                bufOut.write(b, off, len);
                bufOut.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean connect() {
        try {
            cSocket = createSocket(serverIP, serverPort);
            bufIn = new BufferedInputStream(cSocket.getInputStream());
            bufOut = new BufferedOutputStream(cSocket.getOutputStream());
            isConnected = true;
        } catch (Exception e) {
            isConnected = false;
            e.printStackTrace();
        }
        return isConnected;
    }

    public Socket createSocket(String ip, int port) throws Exception {
        Socket socket;
        try {
            socket = new Socket(ip, port);
        } catch (Exception e) {
            return null;
        }
        return socket;
    }

    public void flush() {
        try {
            bufOut.flush();
        } catch (Exception e) {
            close();
        }
    }

    public void close() {
        if (bufIn != null) {
            try {
                bufIn.close();
            } catch (IOException e) {
            }
        }
        bufIn = null;
        if (bufOut != null) {
            try {
                bufOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bufOut = null;
        if (cSocket != null) {
            try {
                cSocket.close();
            } catch (Exception e) {
            }
        }
        cSocket = null;
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
        try {
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void sendFileRejectRequest(String reqID, String src, String fName, String length) {
        try {
            byte[] temp;
            byte[] buf = new byte[MChatMeta.BYTE_LEN];
            String messageType = MChatMeta.FILE_REJECT_REQUEST;
            String source = src;
            String destination = MChatSettings.MUBASHER_ID;
            String name = fName;
            String size = "" + length;
            String requestID = "" + reqID;

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());

            temp = name.getBytes();
            System.arraycopy(temp, 0, buf, 52, name.length());

            temp = size.getBytes();
            System.arraycopy(temp, 0, buf, 180, size.length());

            write(buf, 0, MChatMeta.BYTE_LEN);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return isConnected;
    }
}
