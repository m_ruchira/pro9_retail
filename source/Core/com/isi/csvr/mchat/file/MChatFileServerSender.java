package com.isi.csvr.mchat.file;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.mchat.datastore.MChatFileStore;
import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.mchat.shared.MChatUniqueID;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.TimeZone;

public class MChatFileServerSender implements ApplicationListener {
    public boolean isConnected = false;
    public JProgressBar pro = null;
    protected Socket cSocket = null;
    protected BufferedOutputStream bufOut = null;
    protected InputStream bufIn = null;
    long fsize = 0;
    int kbytes = 1;
    int bytcount = MChatMeta.BYTE_LEN;
    private String serverIP = null;
    private int serverPort = 0;
    private String destFilepath = "";
    private long progValue = 0;
    private JProgressBar progressBar = null;
    private boolean visited = false;
    private MChatFileSendGUIPanel sendProgressGUIPanel = null;

    public MChatFileServerSender(String serverIP, int port, MChatFileSendGUIPanel sendProgressGUIPanel) {
        try {
            this.serverIP = serverIP;
            this.serverPort = port;
            this.progressBar = sendProgressGUIPanel.getProgressBar();
            this.sendProgressGUIPanel = sendProgressGUIPanel;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return isConnected;
    }

    private synchronized void write(byte[] b, int off, int len) {
        try {
            if (isConnected && !cSocket.isClosed() && bufOut != null) {
                bufOut.write(b, off, len);
                bufOut.flush();
            }
        } catch (Exception e) {
            try {
                close();
                progressBar.setVisible(false);
                sendProgressGUIPanel.getLable().setText(MChatLanguage.getString(""));
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public void sendFileSendingRequest(String fileName, String desUser) {
        try {
            File file = new File(fileName);
            byte[] buf = new byte[200];
            byte[] readfile = new byte[1024];
            byte[] temp;
            int index;
            String messageType = MChatMeta.FILE_SEND_REQUEST;
            String source = MChatMeta.MUBASHER_ID;
            String destination = desUser;
            String name = file.getName();
            String size = "" + file.length();
            String requestID = "" + MChatUniqueID.get();
            fsize = file.length();

            try {
                MChatFileStore.getSharedInstance().addToSenderTable(requestID, this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());
            temp = null;

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());
            temp = null;

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());
            temp = null;

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());
            temp = null;

            temp = name.getBytes();
            System.arraycopy(temp, 0, buf, 52, name.length());
            temp = null;

            temp = size.getBytes();
            System.arraycopy(temp, 0, buf, 180, size.length());
            temp = null;

            write(buf, 0, 200);  //write file headers
            MChatFileStore.getSharedInstance().addToSenderTable(requestID, this);
            //wait untill file accepted or rejest request comes 
            try {
                synchronized (this) {
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //write the file to socket
            FileInputStream fileIn = new FileInputStream(file);
//            File fileTemp = new File("test.jpg");
//            FileOutputStream fileOut = new FileOutputStream(fileTemp);
            while ((index = fileIn.read(readfile, 0, 1024)) != -1) {
                if (!sendProgressGUIPanel.isCancelClicked()) {
                    try {
                        bytcount = bytcount + index;
                        write(readfile, 0, 1024);
//                        fileOut.write(readfile,0,1024);
                        kbytes++;
                        progressCalculator(bytcount, fsize);
                    } catch (Exception e) {
                    }
                } else {
                    try {
                        close();
                        break;
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
            MChatFileStore.getSharedInstance().removeFromSender(requestID);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean connect() {
        try {
            cSocket = createSocket(serverIP, serverPort);
            bufIn = new BufferedInputStream(cSocket.getInputStream());
            bufOut = new BufferedOutputStream(cSocket.getOutputStream());
            isConnected = true;
        } catch (Exception e) {
            isConnected = false;
            e.printStackTrace();
        }
        return isConnected;
    }

    public Socket createSocket(String ip, int port) throws Exception {
        Socket socket;
        try {
            socket = new Socket(ip, port);
        } catch (Exception e) {
            return null;
        }
        return socket;
    }

    public void flush() {
        try {
            bufOut.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() {
        if (bufIn != null) {
            try {
                bufIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bufIn = null;
        if (bufOut != null) {
            try {
                bufOut.flush();
                bufOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bufOut = null;
        cSocket = null;
    }

    public void progressCalculator(int byteCount, long fsize) {
        try {
            double b = byteCount;
            double f = fsize;

            if (fsize < 1024) {
                progressBar.setVisible(false);
                sendProgressGUIPanel.setCancelButtonInvisible();
                progressBar.setValue(progressBar.getMaximum());
                return;
            }
            progValue = Math.round((b / f) * progressBar.getMaximum());
//            if (((int) progValue % 4) == 0) {
            if (progValue >= progressBar.getMaximum()) {
                progressBar.setValue(progressBar.getMaximum());
                progressBar.setString(100 + "%");
                MChatSettings.setFileTranferInProgress(false);
                try {
                    if (!visited) {
                        progressBar.setVisible(false);
                        sendProgressGUIPanel.setCancelButtonInvisible();
                        sendProgressGUIPanel.getLable().setText(sendProgressGUIPanel.getLable().getText() + " " + MChatLanguage.getString("COMPLETED"));
                        visited = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MChatSettings.setFileTranferInProgress(true);
                progressBar.setStringPainted(true);
                progressBar.setString(progValue + "%");
                progressBar.setValue((int) progValue);
            }
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MChatFileSendGUIPanel getProgressPanel() {
        return sendProgressGUIPanel;
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
        try {
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }
}
