package com.isi.csvr.mchat.file;

import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.shared.Settings;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MChatClipBoardGrabber {
    private static Image image = null;

    public static void getImageFromClipboard(String user, JTextPane displayArea, JTextPane txtArea, String nickname) {

        // get the system clipboard
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

        // get the contents on the clipboard in a
        // Transferable object
        Transferable clipboardContents = systemClipboard.getContents(null);

        // check if contents are empty, if so, return null
        if (clipboardContents == null) {

        } else {
            try {
                // make sure content on clipboard is
                // falls under a format supported by the
                // imageFlavor Flavor
                if (clipboardContents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    try {
                        String st = (String) clipboardContents.getTransferData(DataFlavor.stringFlavor);
                        txtArea.getDocument().insertString(txtArea.getDocument().getLength(), st, null);
                    } catch (UnsupportedFlavorException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (clipboardContents.isDataFlavorSupported(DataFlavor.imageFlavor)) {
                    // convert the Transferable object
                    // to an Image object
                    image = (Image) clipboardContents.getTransferData(DataFlavor.imageFlavor);
                    File file = new File(Settings.getAbsolutepath() + "Temp/screencapture.jpg");
                    ImageIO.write(toBufferedImage(image), "jpg", file);
                    new MChatScreenCaptureForm(user, true, displayArea, nickname);
                } else if (clipboardContents.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                    final java.util.List fileList = (java.util.List) clipboardContents.getTransferData(DataFlavor.javaFileListFlavor);
                    for (int i = 0; i < fileList.size(); i++) {
                        final int j = i;
                        final String u = user;
                        String text = MChatMeta.N_NAME + MChatMeta.DS + MChatMeta.DISPLAY_ID + MChatMeta.FS +
                                MChatMeta.DISPLAY_MESSEGE + MChatMeta.DS + "file:///" + fileList.get(i) + MChatMeta.FS;
                        File file = ((File) (fileList.get(j)));
                        boolean isImageFile = false;
                        if (MChatSharedMethods.getFormatName(fileList.get(i)) != null) {
                            isImageFile = true;
                        } else {
                            isImageFile = false;
                        }
                        try {
                            if (file.length() > MChatMeta.maximumFile) {
                                MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(((File) fileList.get(i)).getCanonicalPath(), true, ((File) fileList.get(i)).getPath(), nickname, isImageFile);
                                sendProgressGUIPanel.getLable().setForeground(Color.red);
                                sendProgressGUIPanel.getLable().setText(((File) fileList.get(i)).getName() + " " + MChatLanguage.getString("FILE_MESSAGE"));
                                sendProgressGUIPanel.removeMouseListener();
                                sendProgressGUIPanel.setCancelButtonInvisible();
                                sendProgressGUIPanel.getProgressBar().setVisible(false);
                                displayArea.insertComponent(sendProgressGUIPanel);
                                displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                                continue;
                            } else if (file.length() == 0) {
                                MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(file.getCanonicalPath(), true, file.getPath(), null, false);
                                sendProgressGUIPanel.getLable().setForeground(Color.red);
                                sendProgressGUIPanel.getLable().setText(file.getName() + " " + MChatLanguage.getString("FILE_EMPTY"));
                                sendProgressGUIPanel.setCancelButtonInvisible();
                                sendProgressGUIPanel.getProgressBar().setVisible(false);
                                displayArea.insertComponent(sendProgressGUIPanel);
                                displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                                continue;
                            }
                        } catch (IOException e) {
                        } catch (BadLocationException e) {
                        }

                        final MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(((File) fileList.get(i)).getCanonicalPath(), true, ((File) fileList.get(i)).getPath(), nickname, isImageFile);
                        try {
                            sendProgressGUIPanel.setDisplayArea(displayArea);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        try {
                            displayArea.setCaretPosition(displayArea.getDocument().getLength());
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        try {
                            displayArea.insertComponent(sendProgressGUIPanel);
                            displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }

                        final MChatFileServerSender fileServerSender = new MChatFileServerSender(MChatSettings.serverFileIP, MChatSettings.serverFilePort, sendProgressGUIPanel);
//                        sendProgressGUIPanel.setFileServerSender(fileServerSender);
                        if (fileServerSender.connect()) {
                            new Thread() {
                                public void run() {
                                    try {
                                        fileServerSender.sendFileSendingRequest(((File) (fileList.get(j))).getCanonicalPath(), u);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } else {
                            System.out.println("<DEBUG> File sever connection error !");
                        }
                    }
                }
            } catch (UnsupportedFlavorException ufe) {
                ufe.printStackTrace();
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        }
    }

    private static BufferedImage toBufferedImage(Image src) {
        BufferedImage dest = null;
        try {
            int w = src.getWidth(null);
            int h = src.getHeight(null);
            int type = BufferedImage.TYPE_INT_RGB;
            dest = new BufferedImage(w, h, type);
            Graphics2D g2 = dest.createGraphics();
            g2.drawImage(src, 0, 0, null);
            g2.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dest;
    }

    public static boolean isMoreThanLimit() {
        Clipboard systemClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable clipboardContents = systemClipboard.getContents(null);
        if (clipboardContents == null) {
        } else {
            try {
                if (clipboardContents.isDataFlavorSupported(DataFlavor.imageFlavor)) {

                } else if (clipboardContents.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                    final java.util.List fileList = (java.util.List) clipboardContents.getTransferData(DataFlavor.javaFileListFlavor);
                    for (int i = 0; i < fileList.size(); i++) {
                        final int j = i;
                        File file = ((File) (fileList.get(j)));
                        if (file.length() > 104857600) {
                            return true;
                        }
                    }
                }
            } catch (Exception ex) {

            }
        }
        return false;
    }
}
