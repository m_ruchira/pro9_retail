package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MChatOptionsForm extends SmartFrame implements Disposable {
    private static boolean isSelected = true;
    private static MChatOptionsForm self = null;
    public String requestMessage = null;
    private JPanel buttonPane = null;
    private JPanel publishPane = null;
    private JPanel personlaMessagePane = null;
    private JPanel editContactPanel = null;
    private JPanel labelPane = null;
    private JPanel labelPaneEdit = null;
    private JScrollPane messageScroll = null;
    private JScrollPane messageScrollEdit = null;
    private JButton addPersonalMessage = null;
    private JButton cancelPersonalMessage = null;
    private JLabel emptyLabel_1 = null;
    private JLabel emptyLabel_2 = null;
    private JLabel publishMe = null;
    private JLabel personalLabel = null;
    private JLabel editLabel = null;
    private JTextArea personlaMessageArea = null;
    private JTextField editContactTextArea = null;
    private int show = 0;

    public MChatOptionsForm() {
        super(true);
        try {
            setTitle(MChatLanguage.getString("OPTIONS"));
            setTitleColor();
            setTitileFont(MChatMeta.defaultFont);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            setDefaultImageIcon();
            initializeComponents();
            MChatDataStore.getSharedInstance().addDisposableListener(this);
        } catch (Exception e) {
        }
    }

    public static MChatOptionsForm getSharedInstance() {
        if (self == null) {
            self = new MChatOptionsForm();
        }
        return self;
    }

    public void closeButtonActionPerformed() {
        cancelChangePersonlaMessage();
    }

    public void initializeComponents() {
        try {
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "20", "55", "20", "30", "100%"}, 0, 0)));
            publishPane = new JPanel(new FlexGridLayout(new String[]{"10", "200", "50"}, new String[]{"100%"}, 0, 0));
            publishPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            publishMe = new JLabel(MChatLanguage.getString("SHOWME"));
            if (MChatLanguage.isLTR) {
                publishMe.setFont(MChatMeta.defaultFont);
            }
            publishMe.setForeground(Color.white);
            final ImageIcon selected = new ImageIcon(MChatSettings.IMAGE_PATH + "/tick-icon.jpg");
            final ImageIcon notSelected = new ImageIcon(MChatSettings.IMAGE_PATH + "/un-tick-icon.jpg");
            if (MChatSettings.isSearchable) {
                publishMe.setIcon(selected);
            } else {
                publishMe.setIcon(notSelected);
            }

            publishMe.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    if (isSelected) {
                        publishMe.setIcon(notSelected);
                        show = 1;
                        isSelected = false;
                        MChatSettings.setProperty("IS_SEARCH_ALLOWED", "1");
                    } else {
                        publishMe.setIcon(selected);
                        show = 0;
                        isSelected = true;
                        MChatSettings.setProperty("IS_SEARCH_ALLOWED", "0");
                    }

                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }
            });

            publishPane.add(new JLabel());
            publishPane.add(publishMe);
            publishPane.add(new JLabel());

            labelPane = new JPanel(new FlexGridLayout(new String[]{"10", "100%"}, new String[]{"100%"}, 0, 0));
            labelPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            personalLabel = new JLabel(MChatLanguage.getString("PERSONAL_MESSAGE"));
            personalLabel.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));

            if (MChatLanguage.isLTR) {
                personalLabel.setFont(MChatMeta.defaultFont);
            }
            labelPane.add(new JLabel());
            labelPane.add(personalLabel);

            personlaMessagePane = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
            personlaMessageArea = new JTextArea();
            personlaMessageArea.setForeground(Color.black);
            personlaMessageArea.setBackground(Color.white);
            personlaMessageArea.setLineWrap(true);
            personlaMessageArea.setWrapStyleWord(true);
            if (MChatLanguage.isLTR) {
                personlaMessageArea.setFont(MChatMeta.defaultFont);
            }
            personlaMessageArea.setBorder(BorderFactory.createEmptyBorder());
            messageScroll = new JScrollPane();
            messageScroll.setBorder(BorderFactory.createEmptyBorder());
            messageScroll.setViewportView(personlaMessageArea);
            personlaMessagePane.add(new JLabel());
            personlaMessagePane.add(messageScroll);
            personlaMessagePane.add(new JLabel());
            personlaMessagePane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));


            labelPaneEdit = new JPanel(new FlexGridLayout(new String[]{"10", "100%"}, new String[]{"100%"}, 0, 0));
            labelPaneEdit.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            editLabel = new JLabel(MChatLanguage.getString("EDIT_DISPLAY_NAME"));
            editLabel.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));

            if (MChatLanguage.isLTR) {
                editLabel.setFont(MChatMeta.defaultFont);
            }
            labelPaneEdit.add(new JLabel());
            labelPaneEdit.add(editLabel);

            editContactPanel = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
            editContactTextArea = new JTextField(50);
            editContactTextArea.setText(MChatMeta.DISPLAY_ID);
            editContactTextArea.setForeground(Color.black);
            editContactTextArea.setBackground(Color.white);
//            editContactTextArea.setLineWrap(true);
//            editContactTextArea.setWrapStyleWord(true);
            if (MChatLanguage.isLTR) {
                editContactTextArea.setFont(MChatMeta.defaultFont);
            }
            editContactTextArea.setBorder(BorderFactory.createEmptyBorder());
            messageScrollEdit = new JScrollPane();
            messageScrollEdit.setBorder(BorderFactory.createEmptyBorder());
            messageScrollEdit.setViewportView(editContactTextArea);

            editContactPanel.add(new JLabel());
            editContactPanel.add(editContactTextArea);
            editContactPanel.add(new JLabel());
            editContactPanel.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            buttonPane = new JPanel(new FlexGridLayout(new String[]{"25%", "25%", "25%", "25%"}, new String[]{"70"}, 0, 0));
            buttonPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            emptyLabel_1 = new JLabel();
            buttonPane.add(emptyLabel_1);

            addPersonalMessage = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/Save-but.jpg"));
            addPersonalMessage.setBorder(BorderFactory.createEmptyBorder());
            addPersonalMessage.setContentAreaFilled(false);
            addPersonalMessage.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/Save-but-up.jpg"));
            addPersonalMessage.setFocusPainted(false);

            cancelPersonalMessage = new JButton(MChatLanguage.getString("REJECT"));
            cancelPersonalMessage = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but.jpg"));
            cancelPersonalMessage.setBorder(BorderFactory.createEmptyBorder());
            cancelPersonalMessage.setContentAreaFilled(false);
            cancelPersonalMessage.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but-up.jpg"));
            cancelPersonalMessage.setFocusPainted(false);

            emptyLabel_2 = new JLabel();
            buttonPane.add(addPersonalMessage);
            buttonPane.add(cancelPersonalMessage);
            buttonPane.setForeground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            buttonPane.add(emptyLabel_2);

            getSmartContentPanel().add(publishPane);
            getSmartContentPanel().add(labelPane);
            getSmartContentPanel().add(personlaMessagePane);
            getSmartContentPanel().add(labelPaneEdit);
            getSmartContentPanel().add(editContactPanel);
            getSmartContentPanel().add(buttonPane);
//            Rectangle confirmRect = getGraphicsConfiguration().getBounds();
//            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocationRelativeTo(com.isi.csvr.Client.getInstance().getFrame());
            setVisible(true);
            setMinSize(400, 250);
            setSize(400, 250);
            setResizable(false);

            addPersonalMessage.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        changePersonalMessage();
                    } catch (Exception e1) {
                    }
                }
            });
            cancelPersonalMessage.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    cancelChangePersonlaMessage();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changePersonalMessage() {
        try {
            if (isSelected) {
                show = 1;
            } else {
                show = 0;
            }
            if ((editContactTextArea.getText() != null) &&
                    (editContactTextArea.getText().length() > 0) &&
                    (!MChatMeta.DISPLAY_ID.equalsIgnoreCase(editContactTextArea.getText()))) {
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createEditContactRequest(editContactTextArea.getText()));
            }
            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createPersonalMessageChangeNotice(personlaMessageArea.getText(), show));
            this.dispose();
        } catch (Exception e1) {
        }
    }

    private void cancelChangePersonlaMessage() {
        try {
            this.dispose();
        } catch (Exception e1) {
        }
    }

    public void destroySelf() {
        self = null;
    }
}
