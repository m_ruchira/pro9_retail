package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.Disposable;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.mchat.shared.MChatSplash;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.Set;

public class MChatClient extends Thread implements Disposable {
    public static boolean isActive = false;
    private static MChatClient self = null;
    private static boolean isConnectionOK = false;
    SocketChannel client;
    Selector selector;
    String serverIP = "";
    int serverPort = 0;
    long sentTime = 0;
    long recTime = 0;
    private boolean isConnected = false;

    public MChatClient() {
        serverIP = MChatSettings.serverIP;
        serverPort = MChatSettings.serverRTPort;
        MChatDataStore.getSharedInstance().addDisposableListener(this);
    }

    public static MChatClient getSharedInstance() {
        if (self == null) {
            self = new MChatClient();
        }
        return self;
    }

    public void stopClient() {
        try {
            stop();
        } catch (Exception e) {
        }
        self = null;
    }

    public synchronized boolean isConnected() {
        return isConnectionOK;
    }

    public synchronized void setConnected(boolean flag) {
        isConnected = flag;
    }

    private void setDisconnected() {
        try {
            if (isConnected) {
                selector = null;
                isConnected = false;
                MChatSettings.isConnected = false;
                setConnectionOK(false);
                MChatDataStore.getSharedInstance().getOutQue().clear();
                try {
                    synchronized (MChatSplash.getSharedInstance()) {
                        MChatSplash.getSharedInstance().notify();
                    }
                } catch (Exception e) {

                }
                try {
                    MChat.getSharedInstance().showLoadingImage();
                } catch (Exception e) {

                }
            }
        } catch (Exception e) {
        }
    }

    private synchronized void setConnectionOK(boolean flag) {
        MChatSearchForm.getSharedInstance().setEnabled(true);
        MChat.getSharedInstance().setEnabled(true);
        MChatChatForm.getSharedInstance().setEnabled(true);
        isConnectionOK = flag;
    }

    public boolean isActive() {
        return isActive;
    }

    public void run() {
        isActive = true;
        while (true) {
            if (isConnected) {
                try {
                    while (selector.select(500) > 0) {
                        Set keys = selector.selectedKeys();
                        Iterator i = keys.iterator();
                        while (i.hasNext()) {
                            try {
                                SelectionKey key = (SelectionKey) i.next();
                                i.remove();
                                SocketChannel channel = (SocketChannel) key.channel();
                                if (key.isConnectable()) {
                                    if (channel.isConnectionPending()) {
                                        try {
                                            setConnectionOK(channel.finishConnect());
                                        } catch (IOException e) {
                                            sleepThread(10);
                                            setDisconnected();
                                            break;
                                        }
                                    }
                                }

                                if (key.isReadable()) {
                                    try {
                                        SocketChannel client = (SocketChannel) key.channel();
                                        int BUFFER_SIZE = 1028;
                                        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                                        try {
                                            client.read(buffer);
                                        } catch (Exception e) {
                                            sleepThread(10);
                                            setDisconnected();
                                            break;
                                        }
                                        buffer.flip();
                                        Charset charset = Charset.forName("ISO-8859-1");
                                        CharsetDecoder decoder = charset.newDecoder();
                                        CharBuffer charBuffer = decoder.decode(buffer);
                                        String readMessage = UnicodeUtils.getNativeStringFromCompressed(charBuffer.toString());

                                        if (readMessage != null && !readMessage.equalsIgnoreCase("")) {
//                                            System.out.println("Read from Server : " + readMessage);
                                        }
                                        MChatDataStore.getSharedInstance().addToInque(readMessage);
                                        buffer.clear();
                                        sleepThread(10);
                                    } catch (Exception e) {
                                        sleepThread(10);
                                    }
                                }

                                if (key.isWritable()) {
                                    int BUFFER_SIZE = 1028;
                                    ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                                    while (MChatDataStore.getSharedInstance().getOutQue().size() > 0) {
                                        try {
                                            String outMessage = (String) MChatDataStore.getSharedInstance().getOutQue().pop();
                                            String writeMessage = UnicodeUtils.getCompressedUnicodeString(outMessage);

                                            buffer =
                                                    ByteBuffer.wrap(
                                                            (writeMessage).getBytes());
                                            try {
                                                client.write(buffer);
//                                                System.out.println("Write to server : " + outMessage);
                                            } catch (IOException e) {
                                                setDisconnected();
                                                sleepThread(10);
                                                break;
                                            }
//                                            if (Meta.IS_SENDING_LOGOUT) {
//                                                if (DataStore.getSharedInstance().getOutQue().size() == 0) {
//                                                    Meta.IS_FINISH_LOGOUT = true;
//                                                }
//                                            }
                                            buffer.clear();
                                        } catch (Exception e) {
                                            sleepThread(10);
                                        }
                                    }
                                    sleepThread(10);
                                }
                            } catch (Exception e) {
                                sleepThread(10);
                            }
                        }
                    }
                } catch (Exception e) {
                    sleepThread(10);
                    setDisconnected();
                }
            } else {
                sleepThread(1000);
                connect();
            }
            sleepThread(10);
        }
    }

    private void connect() {
        try {
            System.out.println("<DEBUG> Try to connect to chat server ..");
            MChatSearchForm.getSharedInstance().setEnabled(false);
            MChat.getSharedInstance().setEnabled(false);
            MChatChatForm.getSharedInstance().setEnabled(false);
            // Create client SocketChannel
            client = SocketChannel.open();
            client.configureBlocking(false);
            // Connection to host port 8000
            client.connect(new java.net.InetSocketAddress(serverIP, serverPort));
            // Create selector
            selector = Selector.open();
            // Record to selector (OP_CONNECT type)
            SelectionKey clientKey = client.register(selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ | SelectionKey.OP_WRITE);
            isConnected = true;
            MChatSettings.isConnected = true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("<ERROR> No connection ");
            setDisconnected();
        }
    }

    private void sleepThread(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
        }
    }

    public void destroySelf() {
        try {
            isActive = false;
            isConnectionOK = false;
            stop();
        } catch (Exception e) {
        }
        self = null;
    }
}
