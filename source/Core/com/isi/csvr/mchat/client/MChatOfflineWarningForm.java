package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MChatOfflineWarningForm extends SmartFrame implements Disposable {
    private static boolean isSelected = true;
    private static MChatOfflineWarningForm self = null;
    public String requestMessage = null;
    private JPanel buttonPane = null;
    private JPanel labelPane = null;
    private JButton closeButton = null;
    private JLabel donotShowCheckLabel = null;
    private JLabel messageLabel = null;

    public MChatOfflineWarningForm() {
        super(true);
        try {
            setTitle(MChatLanguage.getString("OFFLINE"));
            setTitleColor();
            setTitileFont(MChatMeta.defaultFont);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            setTitileFont(MChatMeta.defaultFontBold);
            setTitleColor();
            setDefaultImageIcon();
            initializeComponents();
            MChatDataStore.getSharedInstance().addDisposableListener(this);
        } catch (Exception e) {
        }
    }

    public static MChatOfflineWarningForm getSharedInstance() {
        if (self == null) {
            self = new MChatOfflineWarningForm();
        }
        return self;
    }

    public void initializeComponents() {
        try {
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"65", "30", "100%"}, 0, 0)));
            //setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            donotShowCheckLabel = new JLabel(MChatLanguage.getString("DO_NOT_SHOW"));
            donotShowCheckLabel.setForeground(Color.white);

            if (MChatLanguage.isLTR) {
                donotShowCheckLabel.setFont(MChatMeta.defaultFont);
            }
            final ImageIcon selected = new ImageIcon(MChatSettings.IMAGE_PATH + "/tick-icon.jpg");
            final ImageIcon notSelected = new ImageIcon(MChatSettings.IMAGE_PATH + "/un-tick-icon.jpg");

            if (!MChatSettings.isOfflineWarningFormDisabled) {
                donotShowCheckLabel.setIcon(selected);
                isSelected = false;
            } else {
                donotShowCheckLabel.setIcon(notSelected);
                isSelected = true;
            }
            donotShowCheckLabel.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    if (isSelected) {
                        donotShowCheckLabel.setIcon(notSelected);
                        isSelected = false;
                    } else {
                        donotShowCheckLabel.setIcon(selected);
                        isSelected = true;
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }
            });

            messageLabel = new JLabel(MChatLanguage.getString("DETAIL_MESSAGE"));
            messageLabel.setForeground(Color.white);

            if (MChatLanguage.isLTR) {
                messageLabel.setFont(MChatMeta.defaultFont);
            }

            labelPane = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "50", "100%"}, 0, 0));
            labelPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            labelPane.add(donotShowCheckLabel);
            labelPane.add(messageLabel);

            buttonPane = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "100%"}, new String[]{"100%"}, 0, 0));
            buttonPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));


            closeButton = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but.jpg"));
            closeButton.setBorder(BorderFactory.createEmptyBorder());
            closeButton.setContentAreaFilled(false);
            closeButton.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but-up.jpg"));
            closeButton.setFocusPainted(false);

            buttonPane.add(new JLabel());
            buttonPane.add(closeButton);
            buttonPane.add(new JLabel());

            getSmartContentPanel().add(labelPane);
            getSmartContentPanel().add(buttonPane);

            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((int) dim.getWidth() - (this.getWidth() + 800), (int) dim.getHeight() - (this.getHeight() + 600));
            setVisible(true);
            setMinSize(420, 140);
            setSize(420, 140);
            setResizable(false);

            closeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (isSelected) {
                            MChatSettings.setProperty("NO_OFFLINE_WARNING_WINDOW", "0");
                        } else {
                            MChatSettings.setProperty("NO_OFFLINE_WARNING_WINDOW", "1");
                        }

                        dispose();
                    } catch (Exception e1) {
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroySelf() {
        self = null;
        dispose();
    }
}
