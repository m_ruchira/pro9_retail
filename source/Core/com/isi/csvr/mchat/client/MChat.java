package com.isi.csvr.mchat.client;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.history.MChatHistoryData;
import com.isi.csvr.mchat.history.MChatHistoryDataSet;
import com.isi.csvr.mchat.history.MChatHistoryTableCreate;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Vector;


public class MChat extends SmartFrame implements NonNavigatable, Themeable, KeyListener, ApplicationListener {

    public static Vector contactList = null;
    public static JPanel mainPanel;
    public static JPanel iconPane;
    public static JPanel contactPanel;
    public static JLabel statusBotton;
    public static Hashtable contactObjects;
    public static Vector openWindows;
    public static String unwantedContact = null;
    public static JLabel labelOnlineCount;
    public static MenuItem signOut = null;
    static ImageIcon onlineImg;
    static ImageIcon signOutImg;
    static ImageIcon offlineImg;
    static ImageIcon busyImg;
    static ImageIcon awayImg;
    private static MChat self = null;
    private static boolean isLoadingVisible = false;
    private static TrayIcon trayIcon = null;
    public JTable table;
    public JTextField nickNameTxtField = null;
    public JCheckBox rmbrCBox = null;
    public JCheckBox autoSignInCBox = null;
    public CardLayout cardLayout;
    public MenuItem itemOpen = null;
    String response = null;
    private MChatLanguage language = null;
    private int currentStatus;
    private JLabel titlePicLbl;
    private boolean isLoggedIn = false;
    //    public JTextField newLoginNameTF = null;
    //    public JTextField newMubIDTF = null;
    //    public JTextField firstNameTF = null;
    //    public JTextField lastNameTF = null;
    private JTextField searchContactTF = null;
    private JMenuBar menuBar;
    private JMenu file;
    private JMenu contacts;
    private JMenu tools;
    private JMenu help;
    private JMenuItem removeContactMenuItem;
    private JMenuItem logouttMenuItem;
    private JMenuItem exitMenuItem;
    private boolean isLoginPanelVisible = false;
    private JPanel loadingPanel = null;
    private TableRowSorter<DefaultTableModel> sorter;
    private String proUserID = null;
    private int mode = 0;
    private JPanel contactListPanel = null;
    private PopupMenu systemTrayPopup = null;
    private SystemTray sysTray = null;

    private MChat() {
        try {
            try {
                if (!com.isi.csvr.shared.Language.isLTR()) {
                    MChatMeta.isLTR = false;
                    MChatSettings.IMAGE_PATH = "images/mchat/mchat_ar/";
                } else {
                    MChatSettings.IMAGE_PATH = "images/mchat/mchat_en/";
                    MChatMeta.isLTR = true;
                }
                language = MChatLanguage.getsharedInstance();
                language.load();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
//                DataStore.getSharedInstance().loadSmilies();
            } catch (Exception e) {
                e.printStackTrace();
            }

            setResizable(false);
            setSmartTitle(MChatLanguage.getString("MUBASHER_CHAT"));
//            setTitileFont(Meta.defaultFontBold);
//            setTitleColor();
            setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            setSmartLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            setForeground(Color.WHITE);
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "/ChatIcon.png");
            setIconImage(im1);
            Rectangle scrnRect = getGraphicsConfiguration().getBounds();
            setLocation((int) scrnRect.getWidth() - 300, (int) scrnRect.getHeight() - 600);
            setSize(275, 525);
            setMinSize(275, 525);

            contactList = new Vector();
            cardLayout = new CardLayout();

            titlePicLbl = new JLabel(new ImageIcon("images/mchat/header.png"));
//            getSmartContentPanel().add(titlePicLbl);

            mainPanel = new JPanel();
//            mainPanel.setOpaque(false);
            mainPanel.setLayout(cardLayout);
//            mainPanel.setBackground(Color.red/*Theme.getColor("MCHAT_BG_COLOR")*/);

            searchContactTF = new JTextField("");
            searchContactTF.setColumns(10);
            searchContactTF.setBackground(Color.white);
            GUISettings.applyOrientation(searchContactTF);
            contactObjects = new Hashtable(0);
            openWindows = new Vector();
            labelOnlineCount = new JLabel();

            onlineImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/green_but.png");
            offlineImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/red_but.png");
            busyImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/busy.png");
            awayImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/away.png");
            signOutImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/signout.gif");

            contactPanel = new JPanel();
            contactPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            contactPanel.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
            contactPanel.setBackground(Color.WHITE);
            mainPanel.add(MChatLoginForm.getSharedInstance(), "LoginPanel");
            mainPanel.add(MChatRegistrationForm.getSharedInstance(), "RegistrationForm");
            getSmartContentPanel().add(mainPanel);
            try {
                if (MChatSettings.isFirstTime) {
                    showRegistrationPanel();
                } else {
                    showLoginPanel();
                }
            } catch (Exception e) {
            }

            try {
                new MChatAuthenticator();
                new MChatPulseSender();
                ConnectionNotifier.getInstance().addConnectionListener(this);
                createMainGUI();
                Theme.registerComponent(this);
                MChatSmiliesFrame.getSharedInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Application.getInstance().addApplicationListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                MChatFontSelector.getSharedInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                MChatChatForm.getSharedInstance();
            } catch (Exception e) {
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized MChat getSharedInstance() {
        if (self == null) {
            self = new MChat();
        }
        return self;
    }

    public void reset() {
        self = null;
        trayIcon = null;
        signOut = null;
        labelOnlineCount = null;
        isLoadingVisible = false;
        unwantedContact = null;
        openWindows = null;
        contactObjects = null;
        busyImg = null;
        offlineImg = null;
        awayImg = null;
        statusBotton = null;
        contactPanel = null;
        iconPane = null;
        mainPanel = null;
        contactList = null;
        dispose();
    }

    public void showLoginPanel() {
        try {
            cardLayout.show(mainPanel, "LoginPanel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showRegistrationPanel() {
        try {
            try {
//                MChatSettings.setProperty("IS_FIRST_TIME", "1");
                MChatSettings.save();
            } catch (Exception e) {
                e.printStackTrace();
            }
            cardLayout.show(mainPanel, "RegistrationForm");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getApplicationMode() {
        return this.mode;
    }

    public void setApplicationMode(String mode) {
        MChatSettings.APP_MODE = mode;
    }

    public String getProUserID() {
        return this.proUserID;
    }

    public void setProUserID(String proUserID) {
        this.proUserID = proUserID;
    }

    public void addSystemTrayIcon() {
        try {
            if (SystemTray.isSupported()) {
                final SystemTray tray = SystemTray.getSystemTray();
                sysTray = tray;
                Image image = Toolkit.getDefaultToolkit().getImage(MChatSettings.IMAGE_PATH + "/ChatIcon.png");
                final PopupMenu popup = new PopupMenu();
                systemTrayPopup = popup;
                MenuItem itemClose = new MenuItem(MChatLanguage.getString("CLOSE"));
                itemClose.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        logOut();
                    }
                });

                itemOpen = new MenuItem(MChatLanguage.getString("OPEN_MUBASHER_CHAT"));
                if (MChatLanguage.isLTR) {
                    itemOpen.setFont(MChatMeta.defaultFont);
                }
                itemOpen.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        setVisible(true);
                        setExtendedState(JFrame.NORMAL);
                    }
                });

                signOut = new MenuItem(MChatLanguage.getString("SIGN_OUT"));
                if (MChatLanguage.isLTR) {
                    signOut.setFont(MChatMeta.defaultFont);
                }
                signOut.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        signOut();
                    }
                });

                popup.add(itemOpen);
                popup.add(signOut);
//                popup.add(itemClose);
                if (trayIcon == null) {
                    trayIcon = new TrayIcon(image, MChatLanguage.getString("MUBASHER_CHAT"), popup);
                    trayIcon.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            displayMChat();
                        }
                    });

                    try {
                        tray.add(trayIcon);
                    } catch (AWTException e) {
                        System.err.println("Can't add to tray");
                    }
                }
            } else {
                System.err.println("Tray unavailable");
            }
        } catch (HeadlessException e) {
            e.printStackTrace();
        }
    }

    public boolean isLoginPanelVisible() {
        return isLoginPanelVisible;
    }

    public void setLoginPanelVisible(boolean flag) {
        isLoginPanelVisible = flag;
    }

    private void signOut() {
        try {
            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createLogoutRequest());
            setVisible(false);
            MChatChatForm.getSharedInstance().setVisible(false);
            MChatSearchForm.getSharedInstance().setVisible(false);
            systemTrayPopup.removeAll();
            sysTray.remove(trayIcon);
            MChatDataStore.getSharedInstance().disposeAll();
            reset();
            MubasherChat.getSharedInstance().dispose();
//            MChatDataStore.destroy();
            MChatSettings.destroySelf();
        } catch (Exception e) {
        }
        System.gc();
        System.gc();
    }

    public void displayMChat() {
        try {
            if (MChat.getSharedInstance().getExtendedState() == JFrame.ICONIFIED) {
                MChat.getSharedInstance().setExtendedState(JFrame.NORMAL);
            } else {
                MChat.getSharedInstance().setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createLanguagePanel() {
        mainPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"50", "50", "50", "25"}, 20, 0));

        final JRadioButton engRButton = new JRadioButton("English");
        engRButton.setSelected(true);
        engRButton.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        final JRadioButton arbRButton = new JRadioButton("Arabic");
        arbRButton.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        arbRButton.setEnabled(false);
        final ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(engRButton);
        bgroup.add(arbRButton);

        JButton next = new JButton("Next");
        next.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton button = MChatSharedMethods.getSelection(bgroup);
                if (button == engRButton) {
                    MChatMeta.LANGUAGE_ID = "1";
                } else if (button == arbRButton) {
                    MChatMeta.LANGUAGE_ID = "2";
                }
                language = MChatLanguage.getsharedInstance();
//                createLoginPanel();
//                readDataFile();
            }
        });

        JPanel temp = new JPanel(new FlexGridLayout(new String[]{"30%", "40%", "30%"}, new String[]{"100%"}, 0, 0));
        temp.setBackground(Color.getColor(String.valueOf(0)));
        temp.add(new JLabel());
        temp.add(next);
        temp.add(new JLabel());

        mainPanel.add(new JLabel("Select Preferred Language"));
        mainPanel.add(engRButton);
        mainPanel.add(arbRButton);
        mainPanel.add(temp);
        add(mainPanel);
    }

    public synchronized void processContactList() {
        try {
            contactList.removeAllElements();
            Enumeration enu = contactObjects.keys();
            while (enu.hasMoreElements()) {
                try {
                    MChatContactObject MChatContact = (MChatContactObject) contactObjects.get(enu.nextElement());
                    if (MChatContact.status.equalsIgnoreCase(MChatMeta.ONLINE_SYMBOL)) {
                        contactList.add(onlineImg);
                    } else if (MChatContact.status.equalsIgnoreCase(MChatMeta.OFFLINE_SYMBOL) || MChatContact.status.equalsIgnoreCase(MChatMeta.PENDING_SYMBOL)) {
                        contactList.add(offlineImg);
                    } else if (MChatContact.status.equalsIgnoreCase(MChatMeta.BUSY_SYMBOL)) {
                        contactList.add(busyImg);
                    } else if (MChatContact.status.equalsIgnoreCase(MChatMeta.AWAY_SYMBOL)) {
                        contactList.add(awayImg);
                    }
                    contactList.add(MChatContact.userID);
                    contactList.add(MChatContact.nickName);
                    if (MChatContact.personalMessage != null && MChatContact.personalMessage.length() > 0) {
                        contactList.add(" - " + MChatContact.personalMessage);
                    } else {
                        contactList.add("");
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }

    public synchronized void populateContacts() {
        try {
            String[] columnNames = {"", "", "", ""};
            Object[][] data = new Object[contactObjects.size()][4];
            contactPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            contactPanel.setBorder(BorderFactory.createEmptyBorder());
            contactPanel.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
            for (int i = 0; i < contactObjects.size(); i++) {
                try {
                    data[i][0] = contactList.get(4 * i);
                    data[i][1] = contactList.get(4 * i + 2);
                    data[i][2] = contactList.get(4 * i + 1);
                    data[i][3] = contactList.get(4 * i + 3);

                } catch (Exception e) {
                    System.out.println("<ERROR> In side catch populate contacts");
                }
            }
            DefaultTableModel model = new DefaultTableModel(data, columnNames);

            sorter = new TableRowSorter<DefaultTableModel>(model);
            table = new JTable(model) {
                public Class getColumnClass(int column) {
                    return getValueAt(0, column).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
            TableColumn iconCol = table.getColumnModel().getColumn(0);
            iconCol.setResizable(false);
            iconCol.setPreferredWidth(4);

            TableColumn nickNameCol = table.getColumnModel().getColumn(1);
            nickNameCol.setResizable(false);
//            nickNameCol.setPreferredWidth(100);

            TableColumn proIDCol = table.getColumnModel().getColumn(2);
            proIDCol.setMinWidth(0);
            proIDCol.setMaxWidth(0);
            proIDCol.setResizable(false);

            TableColumn personalMessageColumn = table.getColumnModel().getColumn(3);
            personalMessageColumn.setMinWidth(0);
//            personalMessageColumn.setMaxWidth(100);
            personalMessageColumn.setResizable(false);

            table.setRowHeight(25);
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.setShowGrid(false);
            table.getTableHeader().setReorderingAllowed(false);
            table.setIntercellSpacing(new Dimension(0, 0));
            table.setBackground(Color.WHITE);
            table.setRowSorter(sorter);
            table.setRowSelectionInterval(0, 0);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);

            final JPanel comp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
//            GUISettings.applyOrientation(comp);

            final JLabel onlineImage = new JLabel();
            final JLabel name = new JLabel();
            final JLabel personalMessageLabel = new JLabel();

            comp.add(onlineImage);
            comp.add(name);
            comp.add(personalMessageLabel);

            TableCellRenderer renderer = new TableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    try {
                        comp.removeAll();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comp.setBorder(BorderFactory.createEmptyBorder());
                    try {
                        if (isSelected && column == 1) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
                            label.setForeground(Color.black);
                            comp.add(label);
                            comp.setBackground(new Color(13822710));
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        } else if (isSelected && column == 0) {
                            JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.CENTER);
                            comp.add(label);
                            comp.setBackground(new Color(13822710));
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        } else if (!isSelected && column == 0) {
                            JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.CENTER);
                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        } else if (!isSelected && column == 3) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFontItalic);
                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        } else if (isSelected && column == 3) {
                            if (table.getValueAt(row, column) != null) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(new Color(5526612));
                                label.setFont(MChatMeta.defaultFontItalic);
                                comp.add(label);
                                comp.setBackground(new Color(13822710));
                            }
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        } else {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setForeground(Color.black);
                            label.setFont(MChatMeta.defaultFont);
                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            try {
                                MChatContactObject obj = (MChatContactObject) contactObjects.get((String) table.getValueAt(row, 2));
                                if (obj != null) {
                                    String status = null;
                                    if (obj.status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                        status = MChatLanguage.getString("ONLINE");
                                    } else if (obj.status.equals(MChatMeta.OFFLINE_SYMBOL) || obj.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                        status = MChatLanguage.getString("OFFLINE");
                                    } else if (obj.status.equals(MChatMeta.BUSY_SYMBOL)) {
                                        status = MChatLanguage.getString("BUSY");
                                    } else if (obj.status.equals(MChatMeta.AWAY_SYMBOL)) {
                                        status = MChatLanguage.getString("AWAY");
                                    }
                                    try {
                                        if (!((obj.personalMessage).equals(""))) {
                                            comp.setToolTipText(obj.nickName + "- " + "'" + obj.personalMessage + "'" + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        } else {
                                            comp.setToolTipText(obj.nickName + "<" + status + "> " + "(" + obj.fName + ", " + obj.lName + ")");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                            }
                            return comp;
                        }
                    } catch (Exception e) {
                        return new JPanel();
                    }
                }
            };

            table.getColumnModel().getColumn(0).setCellRenderer(renderer);
            table.getColumnModel().getColumn(1).setCellRenderer(renderer);
            table.getColumnModel().getColumn(3).setCellRenderer(renderer);

            JScrollPane scrollPane = new JScrollPane(table);
            scrollPane.setBorder(BorderFactory.createEmptyBorder());
            scrollPane.getViewport().setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
            table.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent mouseEvent) {
                    if (mouseEvent.getClickCount() == 2) {
                        String userName = table.getValueAt(table.getSelectedRow(), 1).toString();
                        String userID = table.getValueAt(table.getSelectedRow(), 2).toString();
                        try {
                            MChatContactObject contact = (MChatContactObject) contactObjects.get(userID);
                            if (contact != null) {
                                if (contact.status.equals(MChatMeta.OFFLINE_SYMBOL) && MChatSettings.getInt("NO_OFFLINE_WARNING_WINDOW") != 0) {
                                    MChatOfflineWarningForm.getSharedInstance().setVisible(true);
                                } else if (contact.status.equals(MChatMeta.PENDING_SYMBOL)) {
                                    return;
                                }
                            }
                        } catch (Exception e) {
                        }

                        if (!openWindows.contains(userID)) {
                            openWindows.add(userID);
                            MChatChatWindow cw = MChatDataStore.getChatWindow(userID);
                            if (cw == null) {
                                cw = new MChatChatWindow(userID, userName, openWindows.size() - 1);
                                MChatDataStore.addChatWindow(userID, cw);
                            }
                            MChatChatForm.tabbedChatPane.addTab(userName, cw);
                            MChatChatForm.tabbedChatPane.setSelectedComponent(cw);
                            MChatChatForm.getSharedInstance().setLastMessage(cw);
                        }
                        MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(userID), true);
                        MChatChatForm.getSharedInstance().setChatWindowMessage((String) (table.getValueAt(table.getSelectedRow(), 3)));
                        try {
                            MChatChatForm.tabbedChatPane.selectTab(openWindows.indexOf(userID));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED) {
                                MChatChatForm.getSharedInstance().setExtendedState(JFrame.NORMAL);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        MChatChatForm.getSharedInstance().setVisible(true);
                    } else if (mouseEvent.isMetaDown()) {
                        JPopupMenu popup = new JPopupMenu();
                        popup.addPopupMenuListener(new MyPopupListener());

                        JMenuItem menuItem = new JMenuItem(MChatLanguage.getString("REMOVE_CONTACT"));
                        menuItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                        menuItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

                        if (MChatLanguage.isLTR) {
                            menuItem.setFont(MChatMeta.defaultFont);
                        }

                        menuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                String contact;
                                try {
//                                    clearHistory();
                                    contact = table.getValueAt(table.getSelectedRow(), 1).toString();
                                    new MChatRemoveContactForm(contact, table.getValueAt(table.getSelectedRow(), 2).toString());
                                } catch (Exception e1) {
                                }
                            }
                        });
                        menuItem.addFocusListener(new FocusListener() {
                            public void focusGained(FocusEvent e) {
                                setForeground(Color.cyan);
                                setBackground(Color.darkGray);
                            }

                            public void focusLost(FocusEvent e) {
                            }
                        });

                        JMenuItem menuItemHistory = new JMenuItem("CALL");
                        if (MChatLanguage.isLTR) {
                            menuItemHistory.setFont(MChatMeta.defaultFont);
                        }
                        JMenuItem menuItemClearHistory = new JMenuItem(MChatLanguage.getString("CLEAR_HISTORY"));
                        if (MChatLanguage.isLTR) {
                            menuItemClearHistory.setFont(MChatMeta.defaultFont);
                        }

                        menuItemClearHistory.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                clearHistory();
                            }
                        });

                        menuItemHistory.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
 /*                               final String contact = table.getValueAt(table.getSelectedRow(), 2).toString();
                                final MChatVoiceServerSender voiceSender = new MChatVoiceServerSender(MChatSettings.voiceServerIP, MChatSettings.voiceServerPort);
                                if (voiceSender.connect()) {
                                    new Thread() {
                                        public void run() {
                                           voiceSender.sendCallRequest(contact);
                                        }
                                    }.start();
 */
//                            }
                                try {
                                    String contact = table.getValueAt(table.getSelectedRow(), 1).toString();
                                    String path = "history/mchat/" + contact + ".txt";
                                    File file = new File(path);
                                    if (file.exists()) {
                                        MChatHistoryDataSet tabData = new MChatHistoryDataSet();
                                        MChatHistoryData data1 = new MChatHistoryData();
                                        MChatHistoryTableCreate tab = new MChatHistoryTableCreate();
                                        tabData.setColCount();
                                        tabData.loadColumnNames();
                                        tabData.setDataVector(data1.getResult(contact));
                                        tab.setDataModel(tabData);
                                        tab.initializeHistoryTable();
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        });
                        try {
                            String contact = table.getValueAt(table.getSelectedRow(), 1).toString();
                            String path = "history/mchat/" + contact + ".txt";
                            File file = new File(path);
                            popup.add(menuItem);
//                            popup.add(menuItemHistory);
//                            popup.add(menuItemClearHistory);
                            if (!file.exists()) {
                                menuItemClearHistory.setEnabled(false);
//                                menuItemHistory.setEnabled(false);
                            } else {
                            }
                            popup.show(table, mouseEvent.getX(), mouseEvent.getY());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        /*  try {
                            popup.add(menuItem);
                            popup.add(menuItemHistory);
                            popup.add(menuItemClearHistory);
                            popup.show(table, mouseEvent.getX(), mouseEvent.getY());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    }
                }

                public void mousePressed(MouseEvent mouseEvent) {
                }

                public void mouseExited(MouseEvent mouseEvent) {
                }

                public void mouseReleased(MouseEvent mouseEvent) {
                }

                public void mouseEntered(MouseEvent mouseEvent) {
                }
            });
            table.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    int x = e.getKeyCode();
                    if (e.getKeyCode() == 10) {
                        String userName = table.getValueAt(table.getSelectedRow(), 1).toString();
                        String userID = table.getValueAt(table.getSelectedRow(), 2).toString();
                        if (!openWindows.contains(userID)) {
                            openWindows.add(userID);
                            MChatChatWindow cw = MChatDataStore.getChatWindow(userID);
                            if (cw == null) {
                                cw = new MChatChatWindow(userID, userName, openWindows.size() - 1);
                            }
                            MChatChatForm.tabbedChatPane.addTab(userName, cw);
                            MChatChatForm.tabbedChatPane.setSelectedComponent(cw);
                        }
                        MChatChatForm.getSharedInstance().setVisible(true);
                    } else if ((x >= 44 && x <= 57) || (x == 59) || (x == 61) || (x >= 65 && x <= 93) || (x >= 96 && x <= 107) || (x >= 109 && x <= 111) || (x == 222) || (x == 192)) {
                        searchContactTF.requestFocus(true);
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
            table.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
            contactPanel.add(scrollPane);
            GUISettings.applyOrientation(table);
            scrollPane.repaint();
//            GUISettings.applyOrientation(this);
            contactPanel.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearHistory() {
        try {
            String contact = null;
            File file = null;
            try {
                contact = table.getValueAt(table.getSelectedRow(), 1).toString();
                String path = "history/mchat/" + contact + ".txt";
                file = new File(path);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void addContactMenuActionPerformed(ActionEvent e) {
        try {
            searchContacts();
        } catch (Exception e1) {
        }
    }

    private void removeContactMenuActionPerformed(ActionEvent e) {
        try {
            String contact = null;
            try {
                contact = table.getValueAt(table.getSelectedRow(), 1).toString();
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, MChatLanguage.getString("SELECT_CONTACT_TO_REMOVE"));
                contact = "";
            }
            if (!contact.equalsIgnoreCase("")) {
                int num = JOptionPane.showConfirmDialog(this, MChatLanguage.getString("REMOVE") + " " + contact + " " + MChatLanguage.getString("FROM_CONTACT_LIST"), MChatLanguage.getString("REMOVE_CONTACT"), JOptionPane.WARNING_MESSAGE);
                if (num == 0) {
                    unwantedContact = contact;
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createRemoveContactRequest(table.getValueAt(table.getSelectedRow(), 2).toString()));
                }
            }
        } catch (HeadlessException e1) {

        }
    }

    public void createMenubar() {
        try {
            menuBar = new JMenuBar();
            menuBar.setBackground(Color.WHITE);
            file = new JMenu(MChatLanguage.getString("FILE"));
            file.setBackground(Color.WHITE);
            exitMenuItem = new JMenuItem(MChatLanguage.getString("EXIT"), KeyEvent.VK_T);
            file.add(exitMenuItem);
            logouttMenuItem = new JMenuItem(MChatLanguage.getString("LOG_OUT"), KeyEvent.VK_T);
            file.add(logouttMenuItem);
//            menuBar.add(file);
            exitMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.exit(0);
                }
            });

            logouttMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        logOut();
                        MChat.getSharedInstance().setVisible(true);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
            contacts = new JMenu(MChatLanguage.getString("CONTACTS"));
            contacts.setBackground(Color.WHITE);
            removeContactMenuItem = new JMenuItem(MChatLanguage.getString("REMOVE_CONTACT"));
            if (MChatLanguage.isLTR) {
                removeContactMenuItem.setFont(MChatMeta.defaultFont);
            }

            removeContactMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    removeContactMenuActionPerformed(e);
                }
            });
            contacts.add(removeContactMenuItem);
            menuBar.add(contacts);
            tools = new JMenu(MChatLanguage.getString("TOOLS"));
            tools.setBackground(Color.WHITE);
//            menuBar.add(tools);
            help = new JMenu(MChatLanguage.getString("HELP"));
            help.setBackground(Color.WHITE);
//            menuBar.add(help);
            JPanel menuPanel = new JPanel(new FlexGridLayout(new String[]{"170", "65", "100%"}, new String[]{"100%"}, 0, 0));

/*
            final JLabel statusLbl = new JLabel(new ImageIcon("mchat/green_but.png"));
            final JComboBox status = new JComboBox();
            status.addItem(Language.getString("ONLINE"));
            status.addItem(Language.getString("OFFLINE"));
            status.setSelectedIndex(0);
            status.setForeground(Meta.ONLINE_COLOR);
            currentStatus = status.getSelectedIndex() + 1;
            status.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
            status.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    if ((status.getSelectedIndex() + 1) != currentStatus) {
                        if ((status.getSelectedItem()).equals(Language.getString("ONLINE"))) {
                            status.setForeground(Meta.ONLINE_COLOR);
                            statusLbl.setIcon(new ImageIcon("mchat/green_but.png"));
                            currentStatus = status.getSelectedIndex() + 1;
                        } else if ((status.getSelectedItem()).equals(Language.getString("OFFLINE"))) {
                            status.setForeground(Color.DARK_GRAY);
                            statusLbl.setIcon(new ImageIcon("mchat/red_but.png"));
                            currentStatus = status.getSelectedIndex() + 1;
                        }
                        DataStore.getSharedInstance().getOutQue().add(SharedMethods.createCurrentStateChangeNotice(currentStatus));
                    }
                }
            });
*/

//            menuPanel.add(menuBar);
//            menuPanel.add(status);
//            menuPanel.add(statusLbl);
            menuPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
//            mainPanel.add(menuPanel);
        } catch (Exception e) {

        }
    }

    public void logOut() {
        try {
            System.out.println("Logging..... out.......");
            MChat.getSharedInstance().repaint();
            MChat.getSharedInstance().dispose();
            contactObjects.clear();
            openWindows.clear();
            contactList.clear();
//            Meta.IS_SENDING_LOGOUT = true;
//            DataStore.getSharedInstance().getOutQue().add(SharedMethods.createLogoutRequest());
            MChatChatForm.getSharedInstance().dispose();
            MChatChatForm.tabbedChatPane = null;
            try {
                MChatPulseSender.getSharedInstance().stopPulse();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                MChatAuthenticator.getSharedInstance().stopAuthenticator();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                MChatClient.getSharedInstance().stopClient();
            } catch (Exception e) {
                e.printStackTrace();
            }
            self = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createIconPane() {
        try {
            iconPane = new JPanel();
            iconPane.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            iconPane.setLayout(new FlexGridLayout(new String[]{"31", "31", "100%"}, new String[]{"100%"}, 2, 2));
            JButton addContactIcon = null;
            JButton searchIcon = null;
            try {
                addContactIcon = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/addContact.png"));
                searchIcon = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/search.png"));
            } catch (Exception e) {
                //            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            searchIcon.setBorder(BorderFactory.createEmptyBorder());
            searchIcon.setContentAreaFilled(false);
            searchIcon.setToolTipText(MChatLanguage.getString("SEARCH_NEW_CONTACTS"));
            searchIcon.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    addContactMenuActionPerformed(e);
                    ((JButton) e.getSource()).setContentAreaFilled(false);
                }
            });
            searchIcon.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    try {
                        ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/searchFocused.png"));
                    } catch (Exception e1) {
                    }
                }

                public void mouseExited(MouseEvent e) {
                    ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/search.png"));
                }
            });
            iconPane.add(searchIcon);

            addContactIcon.setBorder(BorderFactory.createEmptyBorder());
            addContactIcon.setContentAreaFilled(false);
            addContactIcon.setToolTipText(MChatLanguage.getString("ADD_CONTACT"));
            addContactIcon.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ((JButton) e.getSource()).setContentAreaFilled(false);
                }
            });
            addContactIcon.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/addContactFocused.png"));
                }

                public void mouseExited(MouseEvent e) {
                    ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/mchat/addContact.png"));
                }
            });
            iconPane.add(addContactIcon);
            iconPane.add(new JLabel());
//            mainPanel.add(iconPane);
        } catch (Exception e) {
        }
    }

    private void newFilter() {
        try {
            if (table != null) {
                RowFilter<DefaultTableModel, Object> rf = null;
                try {
                    try {
                        if (searchContactTF.getText().trim().equalsIgnoreCase(MChatLanguage.getString("FIND_CONTACT"))) {
                            rf = RowFilter.regexFilter("(?i)" + "", 1);
                        } else {
                            rf = RowFilter.regexFilter("(?i)" + searchContactTF.getText(), 1);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (java.util.regex.PatternSyntaxException e) {
                    return;
                }
                sorter.setRowFilter(rf);
            }
        } catch (Exception e) {
        }
    }

    public void showContactList() {
        try {
            cardLayout.show(mainPanel, "contactListPanel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createMainGUI() {
        try {
            contactListPanel = new JPanel();
            contactListPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"32", "0", "100%", "35"}, 1, 1));
            contactListPanel.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            createContactListView();
            createFooterPanel();

            mainPanel.add(contactListPanel, "contactListPanel");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createFooterPanel() {
        try {
            JPanel statusPanel = new JPanel();
            GUISettings.applyOrientation(statusPanel);
            statusPanel.setLayout(new FlexGridLayout(new String[]{"100", "5", "100%"}, new String[]{"100%"}, 1, 1));
            statusPanel.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            statusPanel.add(labelOnlineCount);

            ImageIcon icon = new ImageIcon("images/mchat/mchat_en/arrow.png");
            /*final JLabel*/
            statusBotton = new JLabel((MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("ONLINE") + ")"), icon, JLabel.LEADING);
            statusBotton.setBorder(BorderFactory.createEmptyBorder());
            statusBotton.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            statusBotton.setForeground(Color.white);
            statusBotton.setBorder(BorderFactory.createEmptyBorder());
            if (MChatLanguage.isLTR) {
                statusBotton.setFont(MChatMeta.defaultFont);
            }
            statusBotton.setVerticalTextPosition(AbstractButton.CENTER);
            statusBotton.setHorizontalTextPosition(AbstractButton.LEADING);

            final JPopupMenu pop = new JPopupMenu();
            JMenuItem onlineItem = new JMenuItem(MChatLanguage.getString("ONLINE"), onlineImg);

            onlineItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            onlineItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
            if (MChatLanguage.isLTR) {
                onlineItem.setFont(MChatMeta.defaultFont);
            }

            onlineItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    currentStatus = 1;
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createCurrentStateChangeNotice(currentStatus));
                    statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("ONLINE") + ")");
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    statusBotton.setToolTipText(statusBotton.getText());
                }
            });


            JMenuItem offlineItem = new JMenuItem(MChatLanguage.getString("OFFLINE"), offlineImg);
            if (MChatLanguage.isLTR) {
                offlineItem.setFont(MChatMeta.defaultFont);
            }
            offlineItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            offlineItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

            offlineItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    currentStatus = 2;
                    statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("OFFLINE") + ")");
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createCurrentStateChangeNotice(currentStatus));
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    statusBotton.setToolTipText(statusBotton.getText());
                }
            });
            //added 2008.06.04
            JMenuItem busyItem = new JMenuItem(MChatLanguage.getString("BUSY"), busyImg);
            if (MChatLanguage.isLTR) {
                busyItem.setFont(MChatMeta.defaultFont);
            }
            busyItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            busyItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

            busyItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    currentStatus = 3;
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createCurrentStateChangeNotice(currentStatus));
                    statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("BUSY") + ")");
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    statusBotton.setToolTipText(statusBotton.getText());
                }
            });

            JMenuItem awayItem = new JMenuItem(MChatLanguage.getString("AWAY"), awayImg);
            if (MChatLanguage.isLTR) {
                awayItem.setFont(MChatMeta.defaultFont);
            }
            awayItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            awayItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

            awayItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    currentStatus = 4;
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createCurrentStateChangeNotice(currentStatus));
                    statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("AWAY") + ")");
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    statusBotton.setToolTipText(statusBotton.getText());
                }
            });

            JMenuItem signOut = new JMenuItem(MChatLanguage.getString("SIGN_OUT"), signOutImg);
            if (MChatLanguage.isLTR) {
                signOut.setFont(MChatMeta.defaultFont);
            }
            signOut.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            signOut.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

            signOut.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    signOut();
                }
            });

            ImageIcon optionsImg = new ImageIcon(MChatSettings.IMAGE_PATH + "/options.gif");
            JMenuItem personalMessageItem = new JMenuItem(MChatLanguage.getString("OPTIONS"), optionsImg);
            if (MChatLanguage.isLTR) {
                personalMessageItem.setFont(MChatMeta.defaultFont);
            }
            personalMessageItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            personalMessageItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

            personalMessageItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        MChatOptionsForm.getSharedInstance().setVisible(true);
                    } catch (Exception e1) {
                    }
                }
            });

            pop.add(onlineItem);
            pop.add(offlineItem);
            pop.add(busyItem);
            pop.add(awayItem);
            pop.add(personalMessageItem);
            pop.add(signOut);

            statusBotton.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent mouseEvent) {
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    pop.show(statusBotton, statusBotton.getHorizontalAlignment(), statusBotton.getVerticalAlignment() + statusBotton.getHeight());

                }

                public void mousePressed(MouseEvent mouseEvent) {
                }

                public void mouseExited(MouseEvent mouseEvent) {
                }

                public void mouseReleased(MouseEvent mouseEvent) {
                    statusBotton.setBorder(BorderFactory.createEmptyBorder());
                }

                public void mouseEntered(MouseEvent mouseEvent) {
                }
            });

            statusBotton.addFocusListener(new FocusListener() {
                public void focusGained(FocusEvent e) {
                    try {
                        statusBotton.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                        statusBotton.setBorder(BorderFactory.createLineBorder(Color.white));
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                public void focusLost(FocusEvent e) {
                    try {
                        statusBotton.setBorder(BorderFactory.createEmptyBorder());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

            statusPanel.add(new JLabel());
            statusPanel.add(statusBotton);

            contactListPanel.add(statusPanel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void paintGradient(Graphics g, int width, int height) {
        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;

            titleLeft = new ImageIcon(MChatSettings.IMAGE_PATH + "\\emo-bar.jpg");
            titleCenter = new ImageIcon(MChatSettings.IMAGE_PATH + "\\emo-bar.jpg");
            titleRight = new ImageIcon(MChatSettings.IMAGE_PATH + "\\emo-bar.jpg");

            iconWidth = titleLeft.getIconWidth();
            if (iconWidth > 0) {
                titleLeft.paintIcon(null, g, 0, 0);
                xOffset = iconWidth;
            }
            iconWidth = titleCenter.getIconWidth();
            if (iconWidth > 0) {
                for (int i = xOffset; i <= width; i += iconWidth) {
                    titleCenter.paintIcon(null, g, i, 0);
                }
            }
            iconWidth = titleRight.getIconWidth();
            if (iconWidth > 0) {
                titleRight.paintIcon(null, g, width - iconWidth, 0);
            }
        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }

    private void createContactListView() {
        try {
            JPanel searchContactPanel = new JPanel(new FlexGridLayout(new String[]{"5", "100%", "29"}, new String[]{"100%"}, 3, 3)) {
                public void paint(Graphics g) {
                    paintGradient(g, getWidth(), 32);
                    super.paintChildren(g);
                }
            };
            searchContactPanel.setBorder(BorderFactory.createEmptyBorder());

            JPanel findPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"3", "100%", "3"}, 2, 0));
            findPanel.setBorder(BorderFactory.createEmptyBorder());
            findPanel.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            GUISettings.applyOrientation(searchContactPanel);
            GUISettings.applyOrientation(findPanel);
            searchContactPanel.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            searchContactTF.setForeground(Color.LIGHT_GRAY);
            searchContactTF.setFont(MChatMeta.defaultFont);
            searchContactTF.setText(MChatLanguage.getString("FIND_CONTACT"));
            final DocumentListener docListner = new DocumentListener() {
                public void changedUpdate(DocumentEvent e) {
                    newFilter();
                }

                public void insertUpdate(DocumentEvent e) {
                    newFilter();
                }

                public void removeUpdate(DocumentEvent e) {
                    newFilter();
                }
            };
            searchContactTF.addFocusListener(new FocusListener() {
                public void focusGained(FocusEvent e) {
                    if (e.getOppositeComponent() != table) {
                        searchContactTF.setText("");
                        searchContactTF.setForeground(Color.BLACK);
                        searchContactTF.getDocument().addDocumentListener(docListner);
                    } else {
                        try {
                            if (searchContactTF.getText().trim().equals(MChatLanguage.getString("FIND_CONTACT"))) {
                                searchContactTF.setText("");
                                searchContactTF.setForeground(Color.BLACK);
                            } else {
                                searchContactTF.setText(MChatLanguage.getString("FIND_CONTACT"));
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        searchContactTF.getDocument().addDocumentListener(docListner);
                    }
                }

                public void focusLost(FocusEvent e) {
                    if (e.getOppositeComponent() != table) {
//                        searchContactTF.setText("");
//                        searchContactTF.getDocument().removeDocumentListener(docListner);
                        searchContactTF.setForeground(Color.LIGHT_GRAY);
                        searchContactTF.setText(MChatLanguage.getString("FIND_CONTACT"));
                    }
                }
            });
            searchContactTF.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 40) {
                        table.requestFocus(true);
                    } else if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        searchContactTF.setText("");
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
            searchContactPanel.add(new JLabel());
            searchContactTF.setBorder(BorderFactory.createEmptyBorder());


            JPanel dummyPanel = new JPanel(new FlexGridLayout(new String[]{"6", "100%"}, new String[]{"100%"}, 0, 0));
            dummyPanel.setOpaque(false);
            dummyPanel.setBackground(Color.white);

            JTextField dummyField = new JTextField();
            dummyField.setBackground(Color.white);
            dummyField.setBorder(BorderFactory.createEmptyBorder());
            dummyField.setEditable(false);

            dummyPanel.add(dummyField);
            dummyPanel.add(searchContactTF);

            findPanel.add(new JLabel());
            findPanel.add(dummyPanel);
            findPanel.add(new JLabel());
            searchContactPanel.add(findPanel);
            JButton searchIcon = null;
            try {
                try {
                    searchIcon = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "\\AddContact.png"));
                } catch (Exception e) {
                }

                searchIcon.setBorder(BorderFactory.createEmptyBorder());
                searchIcon.setContentAreaFilled(false);
                searchIcon.setToolTipText(MChatLanguage.getString("SEARCH_NEW_CONTACTS"));
                searchIcon.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        searchContacts();
                        ((JButton) e.getSource()).setContentAreaFilled(false);
                    }
                });
                searchIcon.addMouseListener(new MouseListener() {
                    public void mouseClicked(MouseEvent e) {
                    }

                    public void mousePressed(MouseEvent e) {
                    }

                    public void mouseReleased(MouseEvent e) {
                    }

                    public void mouseEntered(MouseEvent e) {
                        try {
                            ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "\\AddContact_hover.png"));
                        } catch (Exception e1) {
                        }
                    }

                    public void mouseExited(MouseEvent e) {
                        ((JButton) e.getSource()).setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "\\AddContact.png"));
                    }
                });
            } catch (Exception e) {
            }
//            searchContactPanel.add(new JLabel(new ImageIcon("mchat/addContact.png")));

            searchContactPanel.add(searchIcon);
//            searchContactPanel.add(status);
//            searchContactPanel.add(statusLbl);
//            createIamOnlinePanel();
            contactListPanel.add(searchContactPanel);
            Image tabImage = getToolkit().getImage(MChatSettings.IMAGE_PATH + "\\search_co.png");
            ImageIcon tabIcon = new ImageIcon(tabImage);
            contactListPanel.add(new JLabel());
            contactListPanel.add(contactPanel);
        } catch (Exception e) {
        }
    }

    private void createIamOnlinePanel() {
        JPanel statusPanel = new JPanel();
        statusPanel.setLayout(new FlexGridLayout(new String[]{"100", "25", "100%"}, new String[]{"100%"}, 3, 3));
        statusPanel.setForeground(Theme.getColor("MCHAT_BG_COLOR"));

        final JLabel statusLbl = new JLabel(new ImageIcon(MChatSettings.IMAGE_PATH + "/green_but.png"));
        final JComboBox status = new JComboBox() {
            public void paint(Graphics g) {
                paintGradient(g, getWidth(), 25);
                super.paintChildren(g);
            }
        };

        status.addItem(MChatLanguage.getString("ONLINE"));
        status.addItem(MChatLanguage.getString("OFFLINE"));
        status.setSelectedIndex(0);
        status.setForeground(MChatMeta.ONLINE_COLOR);

        currentStatus = status.getSelectedIndex() + 1;
        status.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if ((status.getSelectedIndex() + 1) != currentStatus) {
                    if ((status.getSelectedItem()).equals(MChatLanguage.getString("ONLINE"))) {
                        status.setForeground(MChatMeta.ONLINE_COLOR);
                        statusLbl.setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/green_but.png"));
                        currentStatus = status.getSelectedIndex() + 1;
                    } else if ((status.getSelectedItem()).equals(MChatLanguage.getString("OFFLINE"))) {
                        status.setForeground(Color.DARK_GRAY);
                        statusLbl.setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/red_but.png"));
                        currentStatus = status.getSelectedIndex() + 1;
                    }
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createCurrentStateChangeNotice(currentStatus));
                }
            }
        });

        JButton statusButton = new JButton(MChatLanguage.getString("ONLINE"), new ImageIcon(MChatSettings.IMAGE_PATH + "/green_but.png"));
        statusButton.setForeground(Theme.getColor("MCHAT_BG_COLOR"));

        JPopupMenu popup = new JPopupMenu();
        popup.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
        popup.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
        JMenuItem menuItem1 = new JMenuItem(MChatLanguage.getString("ONLINE"));
        JMenuItem menuItem2 = new JMenuItem(MChatLanguage.getString("OFFLINE"));
        popup.add(menuItem1);
        popup.add(menuItem2);

        statusButton.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {

            }
        });

        statusPanel.add(statusButton);
        statusPanel.add(statusLbl);
        contactListPanel.add(statusPanel);
    }

    public void addNewContact(String newproID) {
        try {
            Enumeration e1 = MChat.contactObjects.keys();
            boolean isAlreadyContact = false;
            while (e1.hasMoreElements()) {
                if (newproID.equals(e1.nextElement())) {
                    isAlreadyContact = true;
                    break;
                }
            }
            if (!isAlreadyContact) {
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createAddContactRequest(newproID));
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("SUCCESSFULLY_ADD_CONTACTS"), 1);
            } else if (newproID.equalsIgnoreCase(MChatMeta.MUBASHER_ID)) {
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("CONTACT_EXIST"), 1);
            } else {
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("CONTACT_EXIST"), 1);
            }
        } catch (Exception e) {
        }
    }

    public synchronized void showLoadingImage() {
        try {
            if (!isLoadingVisible) {
                try {
                    loadingPanel.setVisible(true);
                    isLoadingVisible = true;
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }

    public synchronized void hideLoadingImage() {
        try {
            if (isLoadingVisible) {
                try {
                    loadingPanel.setVisible(false);
                    isLoadingVisible = false;
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }

    public void searchContacts() {
        try {
            MChatSearchForm.emptyRenderer();
            MChatSearchForm.tableShowContacts.removeAll();
            MChatSearchForm.tableShowContacts.repaint();
            MChatSearchForm.searchMessage.setText("");
            MChatSearchForm.getSharedInstance().setVisible(true);
        } catch (Exception e) {
        }
    }

    public void getBackMainPanel() {
        try {
            repaint();
            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"50", "100%"}, 0, 0));
            add(titlePicLbl);
            add(mainPanel);
            setVisible(true);
        } catch (Exception e) {
        }
    }

    public void setSearchedContacts(Object[][] contactData, Object[] columnNames) {
        try {
            MChatSearchForm.tableShowContacts.removeAll();
            MChatSearchForm.tableShowContacts.repaint();
            if (MChatLanguage.isLTR) {
                MChatSearchForm.tableShowContacts.setFont(MChatMeta.defaultFont);
                MChatSearchForm.tableShowContacts.getTableHeader().setFont(MChatMeta.defaultFont);
                MChatSearchForm.tableShowContacts.setShowHorizontalLines(true);
            }
            if (contactData.length > 100) {
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("TOO_MANY_RESULTS"), 1);
                Object[][] limitedData = new Object[100][5];
                for (int i = 0; i < 100; i++) {
                    for (int j = 0; j < 5; j++) {
                        limitedData[i][j] = contactData[i][j];
                    }
                }
                try {
                    MChatSearchForm.getSharedInstance().getSearchTableModel().setDataVector(limitedData, columnNames);
                } catch (Exception e) {
                }
            } else {
                try {
                    MChatSearchForm.getSharedInstance().getSearchTableModel().setDataVector(contactData, columnNames);
                } catch (Exception e) {
                }
                MChatSearchForm.getSharedInstance().giveMsg(contactData.length + " " + MChatLanguage.getString("NO_OF_RESULTS"), 0);
            }
            TableColumn proIDCol = MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(4);
            proIDCol.setMinWidth(0);
            proIDCol.setMaxWidth(0);
            proIDCol.setResizable(false);

            TableColumn firstCol = MChatSearchForm.tableShowContacts.getColumnModel().getColumn(0);
            firstCol.setMinWidth(60);
            firstCol.setMaxWidth(60);
            firstCol.setPreferredWidth(60);

            if (contactData.length == 0) {
                MChatSearchForm.emptyRenderer();
            } else {
                TableCellRenderer renderer = new TableCellRenderer() {
                    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                        JPanel comp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
                        try {
                            comp.removeAll();
                            table.repaint();
                            table.setShowHorizontalLines(true);
                            table.setShowGrid(true);
                            table.setGridColor(Color.white);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        comp.setBorder(BorderFactory.createEmptyBorder());
                        try {
                            if (isSelected && column == 1 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
//                                label.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 1 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 1 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_ODD_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 1 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_EVEN_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 0 && row % 2 == 0) {
                                JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.CENTER);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 0 && row % 2 == 1) {
                                JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.CENTER);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 0 && row % 2 == 0) {
                                JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.CENTER);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_EVEN_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 0 && row % 2 == 1) {
                                JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.CENTER);
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_ODD_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 2 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_EVEN_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 2 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_ODD_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 2 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 2 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 3 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_EVEN_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 3 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                label.setForeground(Color.black);
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_ODD_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 3 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 3 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 4 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_EVEN_BACKGROUND_COLOR"));
                                return comp;
                            } else if (!isSelected && column == 4 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_NOT_SELECTED_ODD_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 4 && row % 2 == 0) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else if (isSelected && column == 4 && row % 2 == 1) {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_SELECTED_BACKGROUND_COLOR"));
                                return comp;
                            } else {
                                JLabel label = new JLabel((String) table.getValueAt(row, column));
                                label.setHorizontalAlignment(JLabel.LEADING);
                                label.setForeground(Color.black);
                                if (MChatLanguage.isLTR) {
                                    label.setFont(MChatMeta.defaultFont);
                                }
                                comp.add(label);
                                comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                                return comp;
                            }
                        } catch (Exception e) {
                            return new JPanel();
                        }
                    }
                };
                try {
                    MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(0).setCellRenderer(renderer);
                    MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(1).setCellRenderer(renderer);
                    MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(2).setCellRenderer(renderer);
                    MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(3).setCellRenderer(renderer);
                    MChatSearchForm.getSharedInstance().getSearchResultTable().getColumnModel().getColumn(4).setCellRenderer(renderer);
                    MChatSearchForm.getSharedInstance().getSearchResultTable().updateUI();
                    MChatSearchForm.getSharedInstance().getSearchResultTable().repaint();
                    MChatSearchForm.getSharedInstance().setSearchInProgress(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
        }
    }

    public void applyTheme() {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {

    }

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
        try {
            logOut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }
}
