package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

public class MChatPersonalInvitationForm extends SmartFrame {
    public String invitationMessage = null;
    private JPanel buttonPane = null;
    private JPanel bannerPane = null;
    private JPanel personalInvitationPane = null;
    private JPanel labelPane = null;
    private JScrollPane personalIvitationScroll = null;
    private JButton addPersonalInvitation = null;
    private JButton cancelPersonalInvitation = null;
    private JLabel emptyLabel_1 = null;
    private JLabel emptyLabel_2 = null;
    private JLabel publishMe = null;
    private JLabel invitationLbl = null;
    private JTextArea personalInvitationArea = null;
    private String proID = null;
    private String nickName = null;
    private String fName = null;
    private String lName = null;

    public MChatPersonalInvitationForm() {
        super(true);
        try {
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setTitle(MChatLanguage.getString("ADD_CONTACT"));
            setTitleColor();
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            setTitileFont(MChatMeta.defaultFont);
            setDefaultImageIcon();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initializeComponents(String id, String nick, String fName, String lName) {
        try {
            proID = id;
            nickName = nick;
            this.fName = fName;
            this.lName = lName;
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "20", "40", "100%"}, 0, 0)));
            bannerPane = new JPanel(new FlexGridLayout(new String[]{"10", "100%"}, new String[]{"100%"}, 0, 0));
            bannerPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            publishMe = new JLabel();
            publishMe.setText(MChatLanguage.getString("PERSONAL_MESSAGE_BANNER_1") + " " + nick + " " + MChatLanguage.getString("PERSONAL_MESSAGE_BANNER_2"));
            if (MChatLanguage.isLTR) {
                publishMe.setFont(MChatMeta.defaultFont);
            }
            publishMe.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
            bannerPane.add(new JLabel());
            bannerPane.add(publishMe);

            labelPane = new JPanel(new FlexGridLayout(new String[]{"10", "100%"}, new String[]{"100%"}, 0, 0));
            labelPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            invitationLbl = new JLabel(MChatLanguage.getString("PERSONAL_INVITATION"));
            if (MChatLanguage.isLTR) {
                invitationLbl.setFont(MChatMeta.defaultFont);
            }
            invitationLbl.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
            labelPane.add(new JLabel());
            labelPane.add(invitationLbl);

            personalInvitationPane = new JPanel(new FlexGridLayout(new String[]{"10", "100%", "10"}, new String[]{"100%"}, 0, 0));
            personalInvitationArea = new JTextArea();
            if (MChatLanguage.isLTR) {
                personalInvitationArea.setFont(MChatMeta.defaultFont);
            }
            personalInvitationArea.setBackground(Color.white);
            personalInvitationArea.setForeground(Color.black);
            personalInvitationArea.setLineWrap(true);
            personalInvitationArea.setWrapStyleWord(true);
            personalIvitationScroll = new JScrollPane();
            personalIvitationScroll.setBorder(BorderFactory.createEmptyBorder());
            personalIvitationScroll.setViewportView(personalInvitationArea);
            personalInvitationPane.add(new JLabel());
            personalInvitationPane.add(personalIvitationScroll);
            personalInvitationPane.add(new JLabel());
            personalInvitationPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            buttonPane = new JPanel(new FlexGridLayout(new String[]{"25%", "25%", "25%", "25%"}, new String[]{"70"}, 0, 0));
            buttonPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            emptyLabel_1 = new JLabel();
            buttonPane.add(emptyLabel_1);
            addPersonalInvitation = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/send-but.jpg"));
            addPersonalInvitation.setBorder(BorderFactory.createEmptyBorder());
            addPersonalInvitation.setContentAreaFilled(false);
            addPersonalInvitation.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/send-but.jpg"));
            addPersonalInvitation.setFocusPainted(false);

            cancelPersonalInvitation = new JButton(MChatLanguage.getString("REJECT"));
            cancelPersonalInvitation = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but.jpg"));
            cancelPersonalInvitation.setBorder(BorderFactory.createEmptyBorder());
            cancelPersonalInvitation.setContentAreaFilled(false);
            cancelPersonalInvitation.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but.jpg"));
            cancelPersonalInvitation.setFocusPainted(false);

            emptyLabel_2 = new JLabel();
            buttonPane.add(addPersonalInvitation);
            buttonPane.add(cancelPersonalInvitation);
            buttonPane.setForeground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            buttonPane.add(emptyLabel_2);

            getSmartContentPanel().add(bannerPane);
            getSmartContentPanel().add(labelPane);
            getSmartContentPanel().add(personalInvitationPane);
            getSmartContentPanel().add(buttonPane);
            Rectangle confirmRect = getGraphicsConfiguration().getBounds();
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocationRelativeTo(MChatSearchForm.getSharedInstance());
            setVisible(true);
            setSize(400, 200);
            setMinSize(400, 200);
            setResizable(false);

            addPersonalInvitation.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        sendPersonalInvitation();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
            cancelPersonalInvitation.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    cancelPersonalInvitation();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendPersonalInvitation() {
        try {
            Enumeration e1 = MChat.contactObjects.keys();
            boolean isAlreadyContact = false;
            while (e1.hasMoreElements()) {
                if (proID.equals(e1.nextElement())) {
                    isAlreadyContact = true;
                    break;
                }
            }
            if ((!isAlreadyContact)) {
                if (!personalInvitationArea.getText().equalsIgnoreCase("")) {
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createAddContactPersonalInvitationRequest(proID, personalInvitationArea.getText()));
                } else {
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createAddContactPersonalInvitationRequest(proID, MChatLanguage.getString("HI")));
                }
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("SUCCESSFULLY_ADD_CONTACTS"), 1);
                try {
                    MChatContactObject contactObject = new MChatContactObject();
                    contactObject.userID = proID;
                    if (contactObject.userID.equalsIgnoreCase("null")) {
                        return;
                    }
                    contactObject.nickName = nickName;
                    contactObject.status = MChatMeta.PENDING_SYMBOL;
                    contactObject.fName = fName;
                    contactObject.lName = lName;
                    contactObject.personalMessage = "";
                    MChat.getSharedInstance().contactObjects.put(contactObject.userID, contactObject);
                    MChat.contactPanel.removeAll();
                    MChat.getSharedInstance().processContactList();
                    MChat.getSharedInstance().populateContacts();
                    MChat.contactPanel.repaint();
                    MChat.getSharedInstance().setVisible(true);
                    setVisible(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("CONTACT_EXIST"), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cancelPersonalInvitation() {
        try {
            this.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
