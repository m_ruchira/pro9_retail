package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

public class MChatChatForm extends SmartFrame implements Themeable, Disposable {
    public static TWTabbedPane tabbedChatPane = null;
    private static MChatChatForm self = null;
    public JTable table;
    private JPanel chatPanel = null;

    public MChatChatForm() {
        super(true);
        try {
            setSize(450, 400);
            setMinSize(450, 400);
            setLocationRelativeTo(com.isi.csvr.Client.getInstance().getFrame());
            setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            setSmartLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            setDefaultImageIcon();
            chatPanel = new JPanel();
            try {
                chatPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5));
            } catch (Exception e) {
                e.printStackTrace();

            }
            chatPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            try {
            } catch (Exception e) {
                e.printStackTrace();
            }
            setTitileFont(MChatMeta.defaultFontBold);
            setTitleColor();

            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "/ChatIcon.png");
            setIconImage(im1);
            tabbedChatPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, TWTabbedPane.COMPONENT_PLACEMENT.Left, TWTabbedPane.LAYOUT_POLICY.WrapTabLayout, "ch", true);

            tabbedChatPane.addTabPanelListener(new TWTabbedPaleListener() {
                public void tabStareChanged(TWTabEvent event) {
                    if (event.getState() == TWTabEvent.STATE_SELECTED) {
                        String user = null;
                        String status = null;
                        String nickName = null;
                        String personalMessage = null;
                        try {
                            user = ((MChatChatWindow) tabbedChatPane.getSelectedComponent()).user;
                            status = ((MChatContactObject) MChat.contactObjects.get(user)).status;
                            nickName = ((MChatContactObject) MChat.contactObjects.get(user)).nickName;
                            personalMessage = ((MChatContactObject) MChat.contactObjects.get(user)).personalMessage;
                        } catch (Exception e1) {
                        }
                        setLastMessage(null);
                        try {
                            setTitle(nickName + " - " + MChatLanguage.getString("CONVERSATION"));
                            if (personalMessage != null) {
                                setChatWindowMessage(personalMessage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            chatPanel.add(tabbedChatPane);
            getSmartContentPanel().add(chatPanel);
            tabbedChatPane.applyTheme();
            Theme.registerComponent(this);
            MChatDataStore.getSharedInstance().addDisposableListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MChatChatForm getSharedInstance() {
        if (self == null) {
            self = new MChatChatForm();
        }
        return self;
    }

    public void applyTheme() {
        tabbedChatPane.applyTheme();
    }

    public void setLastMessage(MChatChatWindow cw) {
        String user = "";
        String status = "";
        String nickName = "";

        if (cw == null) {
            cw = ((MChatChatWindow) MChatChatForm.tabbedChatPane.getSelectedComponent());
        }
        try {
            user = cw.user;
            status = ((MChatContactObject) MChat.contactObjects.get(user)).status;
            nickName = ((MChatContactObject) MChat.contactObjects.get(user)).nickName;
        } catch (Exception e1) {
        }
        try {
            if (status.equals(MChatMeta.ONLINE_SYMBOL)) {
                cw.btnFile.setEnabled(true);
                cw.btnCapture.setEnabled(true);
                cw.btnNudge.setEnabled(true);
                cw.setLastLabelText(" " + nickName + " " + MChatLanguage.getString("CURRENT_ONLINE"),
                        new ImageIcon("images/mchat/mchat_en/green_but.png"));
            } else if (status.equals(MChatMeta.OFFLINE_SYMBOL)) {
                cw.btnFile.setEnabled(false);
                cw.btnCapture.setEnabled(false);
                cw.btnNudge.setEnabled(false);
                cw.setLastLabelText(" " + nickName + " " + MChatLanguage.getString("CURRENT_OFFLINE"),
                        new ImageIcon("images/mchat/mchat_en/red_but.png"));
            } else if (status.equals(MChatMeta.BUSY_SYMBOL)) {
                cw.btnFile.setEnabled(true);
                cw.btnCapture.setEnabled(true);
                cw.btnNudge.setEnabled(true);
                cw.setLastLabelText(" " + nickName + " " + MChatLanguage.getString("CURRENT_BUSY"),
                        new ImageIcon("images/mchat/mchat_en/busy.png"));
            } else if (status.equals(MChatMeta.AWAY_SYMBOL)) {
                cw.btnFile.setEnabled(true);
                cw.btnCapture.setEnabled(true);
                cw.btnNudge.setEnabled(true);
                cw.setLastLabelText(" " + nickName + " " + MChatLanguage.getString("CURRENT_AWAY"),
                        new ImageIcon("images/mchat/mchat_en/away.png"));
            } else if (status.equals(MChatMeta.PENDING_SYMBOL)) {
                cw.btnFile.setEnabled(false);
                cw.btnCapture.setEnabled(false);
                cw.btnNudge.setEnabled(false);
                cw.setLastLabelText(" " + nickName + " " + MChatLanguage.getString("CURRENT_OFFLINE"),
                        new ImageIcon("images/mchat/mchat_en/red_but.png"));
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void destroySelf() {
        tabbedChatPane = null;
        self = null;
        dispose();
    }
}
