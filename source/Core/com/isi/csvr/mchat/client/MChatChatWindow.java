package com.isi.csvr.mchat.client;

import com.isi.csvr.FontChooser;
import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.file.MChatClipBoardGrabber;
import com.isi.csvr.mchat.file.MChatFileSendGUIPanel;
import com.isi.csvr.mchat.file.MChatFileServerSender;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.mchat.voice.MChatCallGUIPanel;
import com.isi.csvr.mchat.voice.MChatVoiceServerSender;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.NonNavigatable;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.TimerTask;
import java.util.Vector;


public class MChatChatWindow extends JPanel implements ActionListener, NonNavigatable, DropTargetListener {
    public static String watermark = "Beta Version";
    public static Font watermarkFont = new TWFont("Arial", Font.BOLD, 120);
    public MChatMessageProcessor msgProcessor = null;
    public MChatDisplayMessageListener displayArea;
    public JScrollPane scrPane;
    public String user = null;
    public boolean isInConversation = false;
    public String convOriginator = null;
    public String convDestinations = null;
    public String conversationID = null;
    public JLabel lastMessageLabel = null;
    JTextPane txtMessage;
    JTextField searchContactTF;
    JButton btnOK;
    JButton btnClose;
    JButton btnNudge;
    JButton btnFile;
    JButton btnCall;
    JButton btnMchat;
    JButton btnContacts;
    JButton btnConversation;
    ImageIcon iconNudge;
    ImageIcon iconCapture;
    ImageIcon iconCall;
    ImageIcon iconOpenFile;
    ImageIcon iconConversation;
    ImageIcon iconClose;
    JButton btnFont;
    JButton btnSmilies;
    JButton btnItalic;
    JButton btnBold;
    JButton btnCapture;
    ImageIcon iconFont;
    ImageIcon iconSmilies;
    String nickName = null;
    JTable table;
    int currentID;
    int i = 0;
    private ArrayList<String> convesationMembers = null;
    private JFileChooser files = null;
    private TableRowSorter<DefaultTableModel> sorter;
    private DropTarget dt = null;
    private Vector messageHistoryStore = new Vector();
    private JPopupMenu latestHistoryPopup = new JPopupMenu("");
    private FontChooser fontChooser;
    private MouseEvent event = null;
    private JPopupMenu messagePopUp = new JPopupMenu();

    public MChatChatWindow(String userName, String nickName, int id) {
        try {
            this.currentID = id;
            user = userName;
            System.out.println("created user  = " + user);
            convesationMembers = new ArrayList<String>();
            setBackground(Color.WHITE);
            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"75%", "20", "37", "100%"}));
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((int) dim.getWidth() - (this.getWidth() + 20), (int) dim.getHeight() - (this.getHeight() + 20));
            this.nickName = nickName;
            final String nickNameFinal = this.nickName;
            msgProcessor = MChatMessageProcessor.getSharedInstance();
            msgProcessor.addWindow(this);
//            HTMLEditorKit edKit = new HTMLEditorKit();
            displayArea = new MChatDisplayMessageListener(this);
            GUISettings.applyOrientation(displayArea);
            if (MChatLanguage.isLTR) {
                displayArea.setFont(MChatMeta.defaultFont);
            }
            displayArea.setBackground(Color.white);
            displayArea.setSelectionColor(Color.BLUE);
            displayArea.setSelectedTextColor(Color.white);
            displayArea.addHyperlinkListener(new HyperlinkListener() {
                public void hyperlinkUpdate(HyperlinkEvent e) {
                    try {
                        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                            if (e.getURL() != null) {
                                if (e.getURL().getProtocol().equalsIgnoreCase("file")) {
                                    try {
                                        Desktop.getDesktop().open(new File(e.getURL().getFile()));
                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                    }
                                } else {
                                    Runtime.getRuntime().exec("start " + e.getURL());
                                }
                            } else if (e.getDescription() != null) {
                                Runtime.getRuntime().exec("start " + e.getDescription());
                            }
                        }
                    } catch (Exception e1) {
                    }
                }
            });
            displayArea.setBorder(BorderFactory.createEmptyBorder());
            txtMessage = new JTextPane();
            txtMessage.setBackground(Color.white);
            txtMessage.setForeground(Color.black);

//            TextAreaReader reader = new TextAreaReader(txtMessage);
//            Thread inputReader = new Thread(reader);
//            inputReader.start();
            txtMessage.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    event = e;
                    messagePopUp.removeAll();
                    if (event.isMetaDown()) {
                        JMenuItem menuCopy = new JMenuItem(MChatLanguage.getString("COPY") /*copy*/);
                        JMenuItem menuPaste = new JMenuItem(MChatLanguage.getString("PASTE") /*paste*/);
                        JMenuItem menuSelectAll = new JMenuItem(MChatLanguage.getString("SELECT_ALL")/*, selectAll*/);

                        menuCopy.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                        menuCopy.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

                        menuPaste.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                        menuPaste.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

                        menuSelectAll.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                        menuSelectAll.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));

                        if (MChatLanguage.isLTR) {
                            menuCopy.setFont(MChatMeta.defaultFont);
                            menuPaste.setFont(MChatMeta.defaultFont);
                            menuSelectAll.setFont(MChatMeta.defaultFont);
                        }

                        menuCopy.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                copyTextArea();
                            }
                        });
                        menuSelectAll.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                selectTextArea(txtMessage);
                            }
                        });

                        menuPaste.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                pasteTextArea();
                            }
                        });

                        messagePopUp.add(menuCopy);
                        messagePopUp.add(menuPaste);
                        messagePopUp.add(menuSelectAll);
                        messagePopUp.show(txtMessage, e.getX(), e.getY());
                        messagePopUp.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                        messagePopUp.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
                    } else {
                        try {
//                            int offset = txtMessage.viewToModel(event.getPoint());
//                            int start = Utilities.getWordStart(txtMessage, offset);
//                            int end = Utilities.getWordEnd(txtMessage, offset);
//                            String word = txtMessage.getDocument().getText(start, end - start);
//                            int rowStart = Utilities.getRowStart(txtMessage, offset);
//                            int rowEnd = Utilities.getRowEnd(txtMessage, offset);
                            txtMessage.setSelectionColor(Color.BLUE);
                            txtMessage.setSelectedTextColor(Color.white);
                        } catch (Exception e2) {
                        }
                    }
                }

                public void mousePressed(MouseEvent e) {

                }

                public void mouseReleased(MouseEvent e) {

                }

                public void mouseEntered(MouseEvent e) {

                }

                public void mouseExited(MouseEvent e) {

                }
            });


            if (MChatLanguage.isLTR) {
                txtMessage.setFont(MChatMeta.defaultFont);
            }

            try {
//                MChatDataStore.getSharedInstance().loadTextTemplates();
            } catch (Exception e) {
                e.printStackTrace();
            }

            dt = new DropTarget(txtMessage, this);
            searchContactTF = new JTextField("");
            createSearchContactList();
            txtMessage.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    boolean ctrlDown = ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) ==
                            KeyEvent.CTRL_DOWN_MASK);

                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            sendChat();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        try {
                            e.consume();
                        } catch (Exception e1) {

                        }
                    } else if (ctrlDown) {
                        if (e.getKeyCode() == KeyEvent.VK_V) {
                            try {
                                MChatClipBoardGrabber.getImageFromClipboard(user, displayArea, txtMessage, nickNameFinal);
                                setScrollBar();
                                try {
                                    e.consume();
                                } catch (Exception e1) {

                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                            try {
                                /*JPopupMenu popup = new JPopupMenu();
                                popup.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                                popup.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
                                popup.setLightWeightPopupEnabled(true);

                                for (int j = 0; j < MChatDataStore.getSharedInstance().getTxtTemplates().size(); j++) {
                                    JMenuItem menuItem = new JMenuItem(MChatDataStore.getSharedInstance().getTxtTemplates().get(j));
                                    menuItem.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                                    menuItem.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
                                    if (MChatLanguage.isLTR) {
                                        menuItem.setFont(MChatMeta.defaultFont);
                                    }
                                    menuItem.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
//                                            insertTemplateText(e);
                                        }
                                    });
                                    popup.add(menuItem);
                                }
//                                popup.show(txtMessage, (int) txtMessage.getLocation().getX(), (int) txtMessage.getLocation().getY());
                                try {
                                    e.consume();
                                } catch (Exception e1) {

                                }*/
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    } else if (e.getKeyCode() == KeyEvent.VK_DOWN ||
                            e.getKeyCode() == KeyEvent.VK_UP) {
                        try {
//                            latestHistoryPopup.show(txtMessage, (int) txtMessage.getLocation().getX(), (int) txtMessage.getLocation().getY());
                        } catch (Exception e1) {
                        }
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });

            txtMessage.addFocusListener(new FocusListener() {
                public void focusGained(FocusEvent e) {
                    try {
                        MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(user), false);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                public void focusLost(FocusEvent e) {
                }
            });

            btnOK = new JButton(MChatLanguage.getString("SEND"));
            iconNudge = new ImageIcon(MChatSettings.IMAGE_PATH + "/Nudge.png");

            btnNudge = new JButton(iconNudge);
            btnNudge.setBorder(BorderFactory.createEmptyBorder());
            btnNudge.setContentAreaFilled(false);
            btnNudge.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/Nudge-up.png"));

            btnMchat = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/chat-win.png"));
            btnMchat.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/chat-win-up.png"));
            btnMchat.setBorder(BorderFactory.createEmptyBorder());
            btnMchat.setContentAreaFilled(false);

            iconConversation = new ImageIcon(MChatSettings.IMAGE_PATH + "/conv.png");
            btnConversation = new JButton(iconConversation);

            btnContacts = new JButton(iconNudge);

            iconFont = new ImageIcon(MChatSettings.IMAGE_PATH + "/Font.png");
            btnFont = new JButton(iconFont);
            btnFont.setBorder(BorderFactory.createEmptyBorder());
            btnFont.setContentAreaFilled(false);
            btnFont.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/Font-up.png"));

            iconSmilies = new ImageIcon(MChatSettings.IMAGE_PATH + "/smile.png");
            btnSmilies = new JButton(iconSmilies);
            btnSmilies.setBorder(BorderFactory.createEmptyBorder());
            btnSmilies.setContentAreaFilled(false);
            btnSmilies.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/smile-up.png"));

            iconCapture = new ImageIcon(MChatSettings.IMAGE_PATH + "/ScreenCapture.png");
            btnCapture = new JButton(iconCapture);
            btnCapture.setBorder(BorderFactory.createEmptyBorder());
            btnCapture.setContentAreaFilled(false);
            btnCapture.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/ScreenCapture-up.png"));

            iconCall = new ImageIcon(MChatSettings.IMAGE_PATH + "/calling.png");
            btnCall = new JButton(iconCall);
            btnCall.setBorder(BorderFactory.createEmptyBorder());
            btnCall.setContentAreaFilled(false);
            btnCall.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/calling-up.png"));

            iconOpenFile = new ImageIcon(MChatSettings.IMAGE_PATH + "/File_Open.png");
            btnFile = new JButton(iconOpenFile);
            btnFile.setBorder(BorderFactory.createEmptyBorder());
            btnFile.setContentAreaFilled(false);
            btnFile.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/File_Open-up.png"));

            btnItalic = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_italic.gif"));
            btnItalic.addActionListener(this);
            btnItalic.setCursor(new Cursor(Cursor.HAND_CURSOR));

            btnBold = new JButton(new ImageIcon("images/Theme" + Theme.getID() + "/chat_bold.gif"));
            btnBold.addActionListener(this);
            btnBold.setCursor(new Cursor(Cursor.HAND_CURSOR));

            iconClose = new ImageIcon(MChatSettings.IMAGE_PATH + "/Chat_close.png");
            btnClose = new JButton(iconClose);
            btnClose.setBorder(BorderFactory.createEmptyBorder());
            btnClose.setContentAreaFilled(false);
            btnClose.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-up.png"));
            btnClose.addActionListener(new ActionListener() {
                                           public void actionPerformed(ActionEvent e) {
                                               closeTab();
                                           }
                                       }
            );

            btnContacts.addActionListener(new
                                                  ActionListener() {
                                                      public void actionPerformed(ActionEvent e) {
                                                      }
                                                  }
            );

            final String nName = nickName;

            btnSmilies.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        MChatSmiliesFrame.getSharedInstance().setLocationRelativeTo(btnSmilies);
                        MChatSmiliesFrame.getSharedInstance().setVisible(true);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });
            btnFile.addActionListener(new
                                              ActionListener() {
                                                  public void actionPerformed(ActionEvent e) {
                                                      try {
                                                          File[] files = showDialog(MChatLanguage.getString("OPEN_A_FILE"), MChatLanguage.getString("OPEN"), MChatLanguage.getString("OPEN_A_FILE"), 'O', null);
                                                          for (int i = 0; i < files.length; i++) {
                                                              try {
                                                                  final String filePath = files[i].getAbsolutePath();
                                                                  boolean isImageFile = false;
                                                                  if (files[i].length() > MChatMeta.maximumFile) {
                                                                      MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(files[i].getCanonicalPath(), true, files[i].getPath(), null, false);
                                                                      sendProgressGUIPanel.getLable().setForeground(Color.red);
                                                                      sendProgressGUIPanel.getLable().setText(files[i].getName() + " " + MChatLanguage.getString("FILE_MESSAGE"));
                                                                      sendProgressGUIPanel.setCancelButtonInvisible();
                                                                      sendProgressGUIPanel.getProgressBar().setVisible(false);
                                                                      sendProgressGUIPanel.removeMouseListener();
                                                                      displayArea.insertComponent(sendProgressGUIPanel);
                                                                      displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                                                      displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                                                                      continue;
                                                                  } else if (files[i].length() == 0) {
                                                                      MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(files[i].getCanonicalPath(), true, files[i].getPath(), null, false);
                                                                      sendProgressGUIPanel.getLable().setForeground(Color.red);
                                                                      sendProgressGUIPanel.getLable().setText(files[i].getName() + " " + MChatLanguage.getString("FILE_EMPTY"));
                                                                      sendProgressGUIPanel.setCancelButtonInvisible();
                                                                      sendProgressGUIPanel.getProgressBar().setVisible(false);
                                                                      displayArea.insertComponent(sendProgressGUIPanel);
                                                                      displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                                                      displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                                                                      continue;
                                                                  }
                                                                  if (MChatSharedMethods.getFormatName(files[i]) != null) {
                                                                      isImageFile = true;
                                                                  } else {
                                                                      isImageFile = false;
                                                                  }
                                                                  final MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(filePath, true, files[i].getPath(), nickNameFinal, isImageFile);
                                                                  try {
                                                                      sendProgressGUIPanel.setDisplayArea(displayArea);
                                                                  } catch (Exception e1) {
                                                                      e1.printStackTrace();
                                                                  }
                                                                  final MChatFileServerSender fileServerSender = new MChatFileServerSender(MChatSettings.serverFileIP, MChatSettings.serverFilePort, sendProgressGUIPanel);
//                                        sendProgressGUIPanel.setFileServerSender(fileServerSender);
                                                                  if (fileServerSender.connect()) {
                                                                      new Thread() {
                                                                          public void run() {
                                                                              try {
                                                                                  try {
                                                                                      displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                                                                  } catch (Exception e1) {
                                                                                      e1.printStackTrace();
                                                                                  }
                                                                                  displayArea.insertComponent(sendProgressGUIPanel);
                                                                                  displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);

                                                                                  String text = MChatMeta.N_NAME + MChatMeta.DS + MChatMeta.DISPLAY_ID + MChatMeta.FS +
                                                                                          MChatMeta.DISPLAY_MESSEGE + MChatMeta.DS + "file:///" + filePath + MChatMeta.FS;

                                                                                  fileServerSender.sendFileSendingRequest(filePath, user);
                                                                              } catch (Exception e1) {
                                                                                  e1.printStackTrace();
                                                                              }
                                                                          }
                                                                      }.start();

                                                                      try {
                                                                          setScrollBar();
                                                                      } catch (Exception e1) {
                                                                          e1.printStackTrace();
                                                                      }
                                                                  } else {
                                                                      System.out.println("<DEBUG> File sever connection error !");
                                                                  }
                                                              } catch (Exception e1) {
                                                                  e1.printStackTrace();
                                                                  System.out.println("<DEBUG>Cannot send File");
                                                              }
                                                          }
                                                      } catch (Exception e1) {
                                                      }
                                                  }
                                              }
            );

            btnMchat.addActionListener(new
                                               ActionListener() {
                                                   public void actionPerformed(ActionEvent e) {
                                                       MubasherChat.getSharedInstance().displayMChat();
                                                   }
                                               }
            );

            btnCall.addActionListener(new
                                              ActionListener() {
                                                  public void actionPerformed(ActionEvent e) {
                                                      final MChatCallGUIPanel callGUI = new MChatCallGUIPanel("");
                                                      try {
                                                          callGUI.setDisplayArea(displayArea);
                                                      } catch (Exception e1) {
                                                          e1.printStackTrace();
                                                      }
                                                      final MChatVoiceServerSender voiceSender = new MChatVoiceServerSender(MChatSettings.voiceServerIP, MChatSettings.voiceServerPort, callGUI);
                                                      if (voiceSender.connect()) {
                                                          new Thread() {
                                                              public void run() {
                                                                  try {
                                                                      try {
                                                                          displayArea.setCaretPosition(displayArea.getDocument().getLength());
                                                                      } catch (Exception e1) {
                                                                          e1.printStackTrace();
                                                                      }
                                                                      displayArea.insertComponent(callGUI);
                                                                      displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                                                                      voiceSender.sendCallRequest(user);
                                                                  } catch (Exception e1) {
                                                                      e1.printStackTrace();
                                                                  }
                                                              }
                                                          }.start();

                                                          try {
                                                              setScrollBar();
                                                          } catch (Exception e1) {
                                                              e1.printStackTrace();
                                                          }
                                                      } else {
                                                          System.out.println("<DEBUG> File sever connection error !");
                                                      }
                                                  }
                                              }
            );

            btnConversation.addActionListener(new
                                                      ActionListener() {
                                                          public void actionPerformed(ActionEvent e) {
                                                              inviteConversationPanel();
                                                          }
                                                      }

            );

            btnFont.addActionListener(this);
            btnCapture.addActionListener(this);
            btnNudge.addActionListener(this);
            displayArea.setEditable(false);

            btnNudge.setToolTipText(MChatLanguage.getString("NUD"));
            btnFont.setToolTipText(MChatLanguage.getString("FONT"));
            btnSmilies.setToolTipText(MChatLanguage.getString("SMILIE"));
            btnCapture.setToolTipText(MChatLanguage.getString("SCREEN_SHOT"));
            btnFile.setToolTipText(MChatLanguage.getString("FILE_SEND"));
            btnClose.setToolTipText(MChatLanguage.getString("CLOSE_CHAT"));
            btnMchat.setToolTipText(MChatLanguage.getString("MUBASHER_CHAT"));

            scrPane = new JScrollPane(displayArea);
            scrPane.getVerticalScrollBar().setValueIsAdjusting(true);
            scrPane.setBorder(BorderFactory.createEmptyBorder());
//            scrPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            btnOK.addActionListener(this);
            JPanel lastMessagePanel = new JPanel(new FlexGridLayout(new String[]{"3", "100%"}, new String[]{"100%"}));
            lastMessagePanel.setBorder(BorderFactory.createEmptyBorder());
            lastMessagePanel.setBackground(Color.white);
            lastMessageLabel = new JLabel("");
            if (MChatLanguage.isLTR) {
                lastMessageLabel.setFont(MChatMeta.defaultFont);
            }
            lastMessageLabel.setForeground(Color.gray);
            lastMessagePanel.setForeground(Color.gray);
            lastMessagePanel.add(new JLabel());
            lastMessagePanel.add(lastMessageLabel);
            add(scrPane);
            add(lastMessagePanel);
            createIconPane();
            createBottomPane();
            GUISettings.applyOrientation(this);
            repaint();
            this.setVisible(true);
        } catch (Exception e) {
        }
    }

    public static void closeTab() {
        if (MChatSettings.isFileTranferInProgress()) {
            new MChatPopupFrame(MChatLanguage.getString("CLOSE_CHAT"), MChatLanguage.getString("CLOSE_WHILE_PROGRESS"), MChatChatForm.getSharedInstance());
        } else {
            tabClose();
        }
    }

    public static void tabClose() {
        int i = MChatChatForm.tabbedChatPane.getSelectedIndex();
        if (i != -1) {
            String user = ((MChatChatWindow) MChatChatForm.tabbedChatPane.getComponentAt(i)).user;
            try {
                MChat.openWindows.removeElement(user);
                if (MChatChatForm.tabbedChatPane.getTabComponentAt(i + 1) != null) {
                    MChatChatForm.tabbedChatPane.setSelectedIndex(MChatChatForm.tabbedChatPane.getTabCount() - 1);
                } else if (MChatChatForm.tabbedChatPane.getTabComponentAt(i - 1) != null) {
                    MChatChatForm.tabbedChatPane.setSelectedIndex(i - 1);
                }
                if (MChatChatForm.tabbedChatPane.getTabCount() == 1) {
                    MChatChatForm.tabbedChatPane.removeTab(i);
                    MChatChatForm.getSharedInstance().setVisible(false);
                    MChat.openWindows.removeAllElements();
                    MChat.openWindows.trimToSize();
                } else {
                    MChatChatForm.tabbedChatPane.removeTab(i);
                }

            } catch (Exception e1) {
            }
        }
    }

    public void setLastLabelText(String message, ImageIcon icon) {
        lastMessageLabel.setText(message);
        lastMessageLabel.setIcon(icon);
    }

    public synchronized void setLastUpdateLabel(String time, String date) {
        lastMessageLabel.setText(" " + MChatLanguage.getString("LAST_MESSAGE") + " " + time + " " + MChatLanguage.getString("ON") + " " + date + ".");
    }

    public void setLabel() {
//        MChatChatForm.getSharedInstance().setLastMessageLabel();
    }

    private void pasteTextArea() {
        Clipboard system = Toolkit.getDefaultToolkit().getSystemClipboard();
        if (system != null) {
            try {
                String selected = ((String) system.getContents(system).getTransferData(DataFlavor.stringFlavor)).trim();
                if (!selected.equals(null)) {
                    txtMessage.getDocument().insertString(txtMessage.getDocument().getLength(), selected, null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void selectTextArea(JTextPane txtMessage) {
        txtMessage.setSelectionColor(Color.BLUE);
        txtMessage.setSelectedTextColor(Color.WHITE);
        txtMessage.selectAll();
    }

    private void copyTextArea() {
        try {
            StringBuffer sbf = new StringBuffer();
            String copy = txtMessage.getSelectedText();
//            if ((textArea instanceof JTextPane )) {
            if (copy == null) {
                copy = displayArea.getSelectedText();
            }
            if (copy != null) {
                sbf.append(copy);
                sbf.append("\n");
            }
//            } else {
//                return;
//            }
            StringSelection stsel = new StringSelection(sbf.toString());
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stsel, stsel);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void setScrollBar() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                scrPane.getVerticalScrollBar().setValue(scrPane.getVerticalScrollBar().getMaximum());
                try {
                    scrPane.doLayout();
                    scrPane.updateUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUser() {
        return user;
    }

    public String getReceiver() {
        return nickName;
    }

    public void sendChat() {
//        if (DataStore.getSharedInstance().getInputText() != null
//        && DataStore.getSharedInstance().getInputText().length() > 0) {
//            txtMessage.setText(DataStore.getSharedInstance().getInputText());
//            DataStore.getSharedInstance().resetInputText();
//        }
        if (!txtMessage.getText().trim().equals("")) {
//            MChatHistoryChatCreate.getSharedInstance().writeHistorytFile(nickName + ".txt", txtMessage.getText(), MChatMeta.DISPLAY_ID, nickName);
/*
            try {
                if (!messageHistoryStore.contains(txtMessage.getText())) {
                    if (messageHistoryStore.size() >= 5) {
                        try {
                            messageHistoryStore.remove((0));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    messageHistoryStore.add(txtMessage.getText());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
*/

            try {
                /*               latestHistoryPopup.removeAll();
                               for (int j = 0; j < messageHistoryStore.size(); j++) {
                                   JMenuItem item = null;
                                   if (messageHistoryStore.size() == 1) {
                                       item = new JMenuItem((String) messageHistoryStore.get(j));
                                   } else {
                                       item = new JMenuItem((String) messageHistoryStore.get(messageHistoryStore.size() - (j + 1)));
                                   }
                                   if (item != null) {
                                       if (MChatLanguage.isLTR) {
                                           item.setFont(MChatMeta.defaultFont);
                                       }
                                       item.addActionListener(new ActionListener() {
                                           public void actionPerformed(ActionEvent e) {
                                               insertTemplateText(e);
                                           }
                                       });
                                   }
                                   latestHistoryPopup.add(item);
                               }
                */
            } catch (Exception e1) {
                e1.printStackTrace();
            }

            if (!isInConversation) {
                try {
                    String italic = "0";
                    String bold = "0";
                    if (StyleConstants.isItalic(MChatDataStore.chatTextAttributes)) {
                        italic = "1";
                    }
                    if (StyleConstants.isBold(MChatDataStore.chatTextAttributes)) {
                        bold = "1";
                    }

                    String text = MChatMeta.N_NAME + MChatMeta.DS + MChatMeta.DISPLAY_ID + MChatMeta.FS +
                            MChatMeta.DISPLAY_MESSEGE + MChatMeta.DS + txtMessage.getText() + MChatMeta.FS +
                            MChatMeta.DISPLAY_MESSEGE_FONT_ITALIC + MChatMeta.DS + italic + MChatMeta.FS +
                            MChatMeta.DISPLAY_MESSEGE_FONT_BOLD + MChatMeta.DS + bold + MChatMeta.FS +
                            MChatMeta.DISPLAY_MESSEGE_FONT_NAME + MChatMeta.DS + StyleConstants.getFontFamily(MChatDataStore.chatTextAttributes) + MChatMeta.FS +
                            MChatMeta.DISPLAY_MESSEGE_FONT_SIZE + MChatMeta.DS + StyleConstants.getFontSize(MChatDataStore.chatTextAttributes) + MChatMeta.FS + "\n";
                    //change this method
                    displayArea.getDocument().insertString(displayArea.getDocument().getLength(), text, null);
                    setScrollBar();

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createChatRequest(user, txtMessage.getText()));
                txtMessage.setText("");
            } else {
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createConversationMsg(MChatSettings.MUBASHER_ID, txtMessage.getText().trim(), conversationID, convOriginator, convDestinations));
                txtMessage.setText("");
            }
        }
        try {
//            setScrollBar();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void insertTemplateText(ActionEvent e) {
        try {
            txtMessage.setText(((JMenuItem) e.getSource()).getText());
            sendChat();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void inviteConversationPanel() {
        try {
            final SmartFrame inviteCon = new SmartFrame();
            inviteCon.setTitle(MChatLanguage.getString("INVITE_CONV"));
            Image im1 = getToolkit().getImage("images\\mchat\\ChatIcon.png");
            inviteCon.setIconImage(im1);
            inviteCon.setResizable(false);
            inviteCon.setSize(275, 450);
            inviteCon.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

            JPanel inviteConPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"53", "20", "250", "30"}, 10, 10));
            inviteConPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            JPanel contactPanel = new JPanel();
            contactPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            Object[] columnNames = {"", "", "", ""};
            Object[][] data = new Object[MChat.contactObjects.size()][4];
            contactPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            contactPanel.setBackground(Color.WHITE);

            Enumeration enu = MChat.contactObjects.keys();

            for (int i = 0; enu.hasMoreElements(); i++) {
                try {
                    MChatContactObject MChatContact = (MChatContactObject) MChat.contactObjects.get(enu.nextElement());
                    //                if (/*MChatContact.status.equalsIgnoreCase("1") &&*/ !MChatContact.userID.equals(user)) {
                    data[i][0] = new JCheckBox();
                    if (MChatContact.status.equals("1")) {
                        data[i][1] = new ImageIcon("images/mchat/green_but.png");
                    } else {
                        data[i][1] = new ImageIcon("images/mchat/red_but.png");
                    }
                    data[i][2] = MChatContact.nickName;
                    data[i][3] = MChatContact.userID;
                    //                }
                } catch (Exception e) {
                }
            }
            DefaultTableModel model = new DefaultTableModel(data, columnNames);
            sorter = new TableRowSorter<DefaultTableModel>(model);
            table = new JTable(model) {
                public Class getColumnClass(int column) {
                    return getValueAt(0, column).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

            };
            TableColumn checkBoxCol = table.getColumnModel().getColumn(0);
            checkBoxCol.setResizable(false);
            checkBoxCol.setPreferredWidth(20);
            TableColumn iconCol = table.getColumnModel().getColumn(1);
            iconCol.setResizable(false);
            TableColumn nickNameCol = table.getColumnModel().getColumn(2);
            nickNameCol.setResizable(false);
            nickNameCol.setPreferredWidth(245);
            if (!MChatLanguage.isLTR) {
                DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                renderer.setHorizontalAlignment(JLabel.RIGHT);
                nickNameCol.setCellRenderer(renderer);
            }
            TableColumn proIDCol = table.getColumnModel().getColumn(3);
            proIDCol.setMinWidth(0);
            proIDCol.setMaxWidth(0);
            proIDCol.setResizable(false);
            table.setRowHeight(30);
            table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            table.setShowGrid(false);
            table.getTableHeader().setReorderingAllowed(false);
            table.setIntercellSpacing(new Dimension(0, 0));
            table.setBackground(Color.WHITE);
            table.setRowSorter(sorter);
            TableCellRenderer renderer = new TableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    JPanel comp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
                    JCheckBox checkBox = new JCheckBox();
                    if (isSelected && column == 2) {
                        comp.add(new JLabel((String) table.getValueAt(row, column)));
                        if (table.getValueAt(row, 2) != null) {
                            comp.setBackground(new Color(13822710));
                        } else {
                            comp.setBackground(Color.WHITE);
                        }
                        return comp;
                    } else if (isSelected && column == 1) {
                        comp.add(new JLabel((ImageIcon) table.getValueAt(row, column)));
                        if (table.getValueAt(row, 2) != null) {
                            comp.setBackground(new Color(13822710));
                        } else {
                            comp.setBackground(Color.WHITE);
                        }
                        return comp;
                    } else if (!isSelected && column == 1) {
                        comp.add(new JLabel((ImageIcon) table.getValueAt(row, column)));
                        comp.setBackground(Color.WHITE);
                        return comp;
                    } else if (isSelected && column == 0) {
                        checkBox.setSelected(true);
                        if (table.getValueAt(row, 2) != null) {
                            checkBox.setBackground(new Color(13822710));
                        } else {
                            comp.setBackground(Color.WHITE);
                        }
                        if (table.getValueAt(row, 2) != null) {
                            comp.add(checkBox);
                        }
                        if (table.getValueAt(row, 2) != null) {
                            comp.setBackground(new Color(13822710));
                        } else {
                            comp.setBackground(Color.WHITE);
                        }
                        return comp;
                    } else if (!isSelected && column == 0) {
                        checkBox.setBackground(Color.WHITE);
                        if (table.getValueAt(row, 2) != null) {
                            comp.add(checkBox);
                        }
                        comp.setBackground(Color.WHITE);
                        return comp;

                    } else {
                        comp.add(new JLabel((String) table.getValueAt(row, column)));
                        comp.setBackground(Color.WHITE);
                        return comp;
                    }
                }
            };
            table.getColumnModel().getColumn(0).setCellRenderer(renderer);
            table.getColumnModel().getColumn(1).setCellRenderer(renderer);
            table.getColumnModel().getColumn(2).setCellRenderer(renderer);
            JScrollPane scrollPane = new JScrollPane(table);
            scrollPane.getViewport().setBackground(Color.WHITE);
            table.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent mouseEvent) {
                }

                public void mousePressed(MouseEvent mouseEvent) {
                }

                public void mouseExited(MouseEvent mouseEvent) {
                }

                public void mouseReleased(MouseEvent mouseEvent) {
                }

                public void mouseEntered(MouseEvent mouseEvent) {
                }
            });
            table.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    int x = e.getKeyCode();
                    if ((x >= 44 && x <= 57) || (x == 59) || (x == 61) || (x >= 65 && x <= 93) || (x >= 96 && x <= 107) || (x >= 109 && x <= 111) || (x == 222) || (x == 192)) {
                        searchContactTF.requestFocus(true);
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
            contactPanel.add(scrollPane);

            JPanel butPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 5, 5));
            butPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            JButton okBtn = new JButton(MChatLanguage.getString("OK"));
            okBtn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    inviteForConversation();
                    inviteCon.dispose();
                }
            });
            JButton cancelBtn = new JButton(MChatLanguage.getString("CANCEL"));
            cancelBtn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    inviteCon.dispose();
                }
            });
            butPanel.add(okBtn);
            butPanel.add(cancelBtn);

            inviteConPanel.add(new JLabel(new ImageIcon("images\\mchat\\header.png")));
            inviteConPanel.add(searchContactTF);
            inviteConPanel.add(contactPanel);
            inviteConPanel.add(butPanel);
            table.requestFocus();
            inviteCon.add(inviteConPanel);
            inviteCon.repaint();
            inviteCon.repaint();
            inviteCon.setVisible(true);
        } catch (HeadlessException e) {
        }
    }

    private void inviteForConversation() {
        try {
            int[] selectedRows = table.getSelectedRows();
            String destinations = user;
            if (selectedRows.length != 0) {
                for (int i = 0; i < selectedRows.length; i++) {
                    if (!table.getValueAt(selectedRows[i], 3).equals(user)) {
                        destinations = destinations + "," + table.getValueAt(selectedRows[i], 3);
                    }
                }
                conversationID = Long.toString(MChatUniqueID.get());
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createConversationRequest(conversationID, MChatSettings.MUBASHER_ID, destinations, "Welcome"));
                convOriginator = MChatSettings.MUBASHER_ID;
                convDestinations = destinations;
                MChatMessageProcessor.getSharedInstance().addToConvHash(conversationID, this);
                MChatQueue msgQue = new MChatQueue();
                MChatMessageProcessor.getSharedInstance().addtoConvMsgHash(conversationID, msgQue);
                isInConversation = true;
            }
        } catch (Exception e) {
        }
    }

    private void createSearchContactList() {
        try {
            searchContactTF.setForeground(Color.LIGHT_GRAY);
            searchContactTF.setText(MChatLanguage.getString("FIND_CONTACT"));
            final DocumentListener docListner = new DocumentListener() {
                public void changedUpdate(DocumentEvent e) {
                    newFilter();
                }

                public void insertUpdate(DocumentEvent e) {
                    newFilter();
                }

                public void removeUpdate(DocumentEvent e) {
                    newFilter();
                }
            };
            searchContactTF.addFocusListener(new FocusListener() {
                public void focusGained(FocusEvent e) {
                    if (e.getOppositeComponent() != table) {
                        searchContactTF.setText("");
                        searchContactTF.setForeground(Color.BLACK);
                        searchContactTF.getDocument().addDocumentListener(docListner);
                    } else {
                        try {
                            if (searchContactTF.getText().trim().equals(MChatLanguage.getString("FIND_CONTACT"))) {
                                searchContactTF.setText("");
                                searchContactTF.setForeground(Color.BLACK);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        searchContactTF.getDocument().addDocumentListener(docListner);
                    }
                }

                public void focusLost(FocusEvent e) {
                    if (e.getOppositeComponent() != table) {
                        searchContactTF.getDocument().removeDocumentListener(docListner);
                        searchContactTF.setText("");
                        searchContactTF.setForeground(Color.LIGHT_GRAY);
                        searchContactTF.setText(MChatLanguage.getString("FIND_CONTACT"));
                    }
                }
            });
            searchContactTF.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 40) { // if down arrow key is pressed
                        table.requestFocus(true);
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
//           searchContact.add(searchContactTF);
//           searchContact.add(new JLabel(new ImageIcon("res/search_co.png")));
//           mainPanel.add(searchContact);
        } catch (Exception e) {
        }
    }

    private void newFilter() {
        try {
            RowFilter<DefaultTableModel, Object> rf = null;
            try {
                rf = RowFilter.regexFilter("(?i)" + searchContactTF.getText(), 2);
            } catch (java.util.regex.PatternSyntaxException e) {
                return;
            }
            sorter.setRowFilter(rf);
        } catch (Exception e) {
        }
    }

    private File[] showDialog(String
                                      dialogTitle, String
                                      approveButtonText, String
                                      approveButtonToolTip,
                              char approveButtonMnemonic,
                              File file) throws Exception {
        if (files == null) {
            files = new JFileChooser();
        }
        GUISettings.applyOrientation(files);
        files.setDialogTitle(dialogTitle);
        files.setApproveButtonText(approveButtonText);
        files.setApproveButtonMnemonic(approveButtonMnemonic);
        files.setApproveButtonToolTipText(approveButtonToolTip);
        files.setFileSelectionMode(files.FILES_ONLY);
        files.rescanCurrentDirectory();
        files.setMultiSelectionEnabled(true);
        files.setSelectedFile(file);

        int result = files.showDialog(this, null);
        return ((result == files.APPROVE_OPTION) ? files.getSelectedFiles() : null);
    }

    public void update() {
        try {
            if (!isInConversation) {
                MChatQueue msgQue = ((MChatQueue) msgProcessor.getChatMessages().get(user));
                msgProcessor.getChatMessages().remove(user);
                if (msgQue != null) {
                    while (msgQue.size() > 0) {
                        try {
                            String text = MChatMeta.N_NAME + MChatMeta.DS + nickName + MChatMeta.FS +
                                    MChatMeta.DISPLAY_MESSEGE + MChatMeta.DS + msgQue.pop() + MChatMeta.FS;

                            displayArea.getDocument().insertString(displayArea.getDocument().getLength(), (text + "\n"), null);
                            try {
                                setLastUpdateLabel(MChatSettings.getTime(), MChatSettings.getDate());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            setScrollBar();
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                MChatQueue msgQue = (MChatQueue) msgProcessor.getConvMsgHash().get(conversationID);
                if (msgQue != null) {
                    while (msgQue.size() > 0) {
//                        displayArea.append((msgQue.pop()) + "\n");
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        try {
            if (actionEvent.getSource() == btnOK) {
                if (!txtMessage.getText().trim().equals("")) {
                    try {
                        String text = MChatMeta.N_NAME + MChatMeta.DS + MChatMeta.DISPLAY_ID + MChatMeta.FS +
                                MChatMeta.DISPLAY_MESSEGE + MChatMeta.DS + txtMessage.getText() + MChatMeta.FS;

                        displayArea.getDocument().insertString(displayArea.getDocument().getLength(), (text + "\n"), null);
//                        setScrollBar();
                    } catch (BadLocationException e) {
                        e.printStackTrace();
                    }

//                    displayArea.append(Meta.DISPLAY_ID + " : " + txtMessage.getText() + "\n");
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createChatRequest(user, txtMessage.getText()));
                    txtMessage.setText("");
                }
            } else if (actionEvent.getSource() == btnNudge) {
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createNudgeRequest(user));
                java.util.Timer timer = new java.util.Timer();
                timer.schedule(new DisposingWindow(MChatChatForm.getSharedInstance()), 0, 50);
                String message = "<span style='font-size:" + "12" + ";"
                        + " font-family:" + "Tahoma" + ";"
                        + " color:#545454" + "'>&nbsp;&nbsp;" + (MChatLanguage.getString("NUDGE_MESSAGE")) + "</span>";

                try {
                    displayArea.getDocument().insertString(displayArea.getDocument().getLength(), message + "\n", null);
                } catch (BadLocationException e) {

                }
            } else if (actionEvent.getSource() == btnFont) {
                fontChooser = new FontChooser(MChatChatForm.getSharedInstance(), true);
                SwingUtilities.updateComponentTreeUI(fontChooser);
                int style;
                if (StyleConstants.isBold(MChatDataStore.chatTextAttributes))
                    style = Font.BOLD;
                else if (StyleConstants.isItalic(MChatDataStore.chatTextAttributes))
                    style = Font.ITALIC;
                else
                    style = Font.PLAIN;
                Font oldFont = new TWFont(StyleConstants.getFontFamily(MChatDataStore.chatTextAttributes), style, StyleConstants.getFontSize(MChatDataStore.chatTextAttributes));
                Font newBodyFont = fontChooser.showDialog(MChatLanguage.getString("SELECT_FONT"), oldFont);
                if ((newBodyFont != null) && (newBodyFont != oldFont)) {
                    StyleConstants.setFontFamily(MChatDataStore.chatTextAttributes, newBodyFont.getFontName());
                    StyleConstants.setFontSize(MChatDataStore.chatTextAttributes, newBodyFont.getSize());
                    if (newBodyFont.getStyle() == Font.BOLD) {
                        StyleConstants.setBold(MChatDataStore.chatTextAttributes, true);
                        StyleConstants.setItalic(MChatDataStore.chatTextAttributes, false);
                    } else if (newBodyFont.getStyle() == Font.ITALIC) {
                        StyleConstants.setItalic(MChatDataStore.chatTextAttributes, true);
                        StyleConstants.setBold(MChatDataStore.chatTextAttributes, false);
                    } else {
                        StyleConstants.setBold(MChatDataStore.chatTextAttributes, false);
                        StyleConstants.setItalic(MChatDataStore.chatTextAttributes, false);
                    }
                }
                txtMessage.setFont(newBodyFont);
            } else if (actionEvent.getSource() == btnCapture) {
                if (MChat.getSharedInstance().getExtendedState() == JFrame.NORMAL) {
                    MChat.getSharedInstance().setExtendedState(JFrame.ICONIFIED);
                }
                if (MChatChatForm.getSharedInstance().getExtendedState() == JFrame.NORMAL) {
                    MChatChatForm.getSharedInstance().setExtendedState(JFrame.ICONIFIED);
                }
                System.gc();
                System.gc();
                try {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            try {
                                new MChatScreenCaptureForm(user, false, displayArea, nickName);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionEvent.getSource() == btnBold) {
                try {
                    boolean isBold = StyleConstants.isBold(MChatDataStore.chatTextAttributes);
                    StyleConstants.setItalic(MChatDataStore.chatTextAttributes, false);
                    StyleConstants.setBold(MChatDataStore.chatTextAttributes, !isBold);
                    int style = txtMessage.getFont().getStyle();
                    if (style == Font.BOLD) {
                        style = Font.PLAIN;
                    } else {
                        style = Font.BOLD;
                    }
                    txtMessage.setFont(txtMessage.getFont().deriveFont(style));
                    txtMessage.requestFocus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (actionEvent.getSource() == btnItalic) {
                try {
                    boolean isItalic = StyleConstants.isItalic(MChatDataStore.chatTextAttributes);
                    StyleConstants.setBold(MChatDataStore.chatTextAttributes, false);
                    StyleConstants.setItalic(MChatDataStore.chatTextAttributes, !isItalic);
                    int style = txtMessage.getFont().getStyle();
                    if (style == Font.ITALIC) {
                        style = Font.PLAIN;
                    } else {
                        style = Font.ITALIC;
                    }
                    txtMessage.setFont(txtMessage.getFont().deriveFont(style));
                    txtMessage.requestFocus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (HeadlessException e) {
        }
    }

    private void paintGradient(Graphics g, int width, int height) {
        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;

            titleLeft = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");
            titleCenter = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");
            titleRight = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");

            iconWidth = titleLeft.getIconWidth();
            if (iconWidth > 0) {
                titleLeft.paintIcon(null, g, 0, 0);
                xOffset = iconWidth;
            }
            iconWidth = titleCenter.getIconWidth();
            if (iconWidth > 0) {
                for (int i = xOffset; i <= width; i += iconWidth) {
                    titleCenter.paintIcon(null, g, i, 0);
                }
            }
            iconWidth = titleRight.getIconWidth();
            if (iconWidth > 0) {
                titleRight.paintIcon(null, g, width - iconWidth, 0);
            }
        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }

    private void createIconPane() {
        try {
            JPanel iconPanel = new JPanel() {
                public void paint(Graphics g) {
                    paintGradient(g, getWidth(), 35);
                    super.paintChildren(g);
                }
            };
            iconPanel.setLayout(new FlexGridLayout(new String[]{"31", "31", "31", "31", "31", "31", "31", "100%", "31"}, new String[]{"6", "25", "6"}, 0, 0));
//            iconPanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
            iconPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
//            iconPanel.setPreferredSize(new Dimension(225, 30));
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
            iconPanel.add(new JLabel());
//            iconPanel.add(new JLabel());

            iconPanel.add(btnNudge);
//            btnFont.setEnabled(false);
            iconPanel.add(btnFont);
//            iconPanel.add(btnBold);
//            iconPanel.add(btnItalic);
            btnBold.setEnabled(false);
            btnItalic.setEnabled(false);
            iconPanel.add(btnCapture);
//            iconPanel.add(new JLabel());
            iconPanel.add(btnFile);
            iconPanel.add(btnSmilies);
            if (MChatSettings.isVoiceEnable) {
                btnCall.setEnabled(true);
            } else {
                btnCall.setEnabled(false);
            }
            iconPanel.add(btnCall);
            iconPanel.add(btnMchat);
//            iconPanel.add(btnConversation);
            iconPanel.add(new JLabel());
            iconPanel.add(btnClose);
            iconPanel.setVisible(true);
            add(iconPanel);
        } catch (Exception e) {

        }
    }

/*
    public void paint(Graphics g) {
        super.paint(g);
        FontMetrics fontMetrics = getFontMetrics(watermarkFont);
        Rectangle rectangle = getVisibleRect();
        //        System.out.println(rectangle);
        Graphics2D g2D = (Graphics2D) g;
        g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
        g2D.setColor(Color.red);
        g2D.setFont(watermarkFont);
        Image image = Toolkit.getDefaultToolkit().getImage("images\\mchat\\header.png");
        g2D.drawImage(image, (int) rectangle.getX() + (int) ((rectangle.getWidth() - image.getWidth(null)) / 2),
                (int) rectangle.getY() + (int) (rectangle.getHeight() / 2), null);
*/
/*
        g2D.drawString(watermark, (int) rectangle.getX() + (int) ((rectangle.getWidth() - fontMetrics.stringWidth(watermark)) / 2),
                (int) rectangle.getY() + (int) (rectangle.getHeight() / 2));
*/
/*
    }
*/

    private void createBottomPane() {
        try {
            JPanel bottomPanel = new JPanel();
            bottomPanel.setLayout(new FlexGridLayout(new String[]{"2", "100%"}, new String[]{"100%"}, 0, 0));
            bottomPanel.setBackground(getBackground());
            bottomPanel.setBorder(BorderFactory.createEmptyBorder());
//            txtMessage.setLineWrap(true);
            JScrollPane scrollPane = new JScrollPane(txtMessage);
            scrollPane.setBorder(BorderFactory.createEmptyBorder());
            JTextArea dummyArea = new JTextArea();
            dummyArea.setBackground(Color.white);
            dummyArea.setEditable(false);
            bottomPanel.add(dummyArea);
            bottomPanel.add(scrollPane);
//            bottomPanel.add(btnOK);
            add(bottomPanel);
        } catch (Exception e) {

        }
    }

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent dtde) {
        try {
            File file = null;
            Icon icon = null;

            FileSystemView view = FileSystemView.getFileSystemView();
            Transferable tr = dtde.getTransferable();
            DataFlavor[] flavors = tr.getTransferDataFlavors();

            for (int i = 0; i < flavors.length; i++) {
                if (flavors[i].isFlavorJavaFileListType()) {
                    dtde.acceptDrop(DnDConstants.ACTION_COPY);
                    final java.util.List list = (java.util.List) tr.getTransferData(flavors[i]);
                    for (int j = 0; j < list.size(); j++) {
                        final int k = 0;
                        file = (File) list.get(j);
                        if (file.length() > MChatMeta.maximumFile) {
                            MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(file.getCanonicalPath(), true, file.getPath(), null, false);
                            sendProgressGUIPanel.getLable().setForeground(Color.red);
                            sendProgressGUIPanel.getLable().setText(file.getName() + " " + MChatLanguage.getString("FILE_MESSAGE"));
                            sendProgressGUIPanel.removeMouseListener();
                            sendProgressGUIPanel.setCancelButtonInvisible();
                            sendProgressGUIPanel.getProgressBar().setVisible(false);
                            displayArea.insertComponent(sendProgressGUIPanel);
                            displayArea.setCaretPosition(displayArea.getDocument().getLength());
                            displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                            continue;
                        } else if (file.length() == 0) {
                            MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(file.getCanonicalPath(), true, file.getPath(), null, false);
                            sendProgressGUIPanel.getLable().setForeground(Color.red);
                            sendProgressGUIPanel.getLable().setText(file.getName() + " " + MChatLanguage.getString("FILE_EMPTY"));
                            sendProgressGUIPanel.setCancelButtonInvisible();
                            sendProgressGUIPanel.getProgressBar().setVisible(false);
                            displayArea.insertComponent(sendProgressGUIPanel);
                            displayArea.setCaretPosition(displayArea.getDocument().getLength());
                            displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                            continue;
                        }

                        icon = view.getSystemIcon(file);
                        String filePath = file.getCanonicalPath();
                        boolean isImageFile = false;
                        if (MChatSharedMethods.getFormatName(file) != null) {
                            isImageFile = true;
                        } else {
                            isImageFile = false;
                        }

                        final MChatFileSendGUIPanel sendProgressGUIPanel = new MChatFileSendGUIPanel(filePath, true, file.getPath(), this.nickName, isImageFile);
                        try {
                            sendProgressGUIPanel.setDisplayArea(displayArea);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        try {
                            displayArea.setCaretPosition(displayArea.getDocument().getLength());
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        try {
                            displayArea.insertComponent(sendProgressGUIPanel);
                            displayArea.getDocument().insertString(displayArea.getDocument().getLength(), "  \n", null);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }

                        final MChatFileServerSender fileServerSender = new MChatFileServerSender(MChatSettings.serverFileIP, MChatSettings.serverFilePort, sendProgressGUIPanel);
//                        sendProgressGUIPanel.setFileServerSender(fileServerSender);
                        if (fileServerSender.connect()) {
                            new Thread() {
                                public void run() {
                                    try {
                                        fileServerSender.sendFileSendingRequest(((File) (list.get(k))).getCanonicalPath(), user);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } else {
                            System.out.println("<DEBUG> File sever connection error !");
                        }
                        try {
                            setScrollBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    dtde.dropComplete(true);
                    return;
                }
            }
            dtde.rejectDrop();
        } catch (Exception e) {
            e.printStackTrace();
            dtde.rejectDrop();
        }
    }


    public class DisposingWindow extends TimerTask {
        JFrame alert;
        Rectangle rec = MChatChatForm.getSharedInstance().getBounds();

        public DisposingWindow(JFrame al) {
            this.alert = al;
        }

        public void run() {
            try {
                if (i % 4 == 0) {
                    alert.setLocation((int) rec.getX() - 5, (int) rec.getY() - 5);
                    repaint();
                } else if (i % 4 == 1) {
                    alert.setLocation((int) rec.getX() + 5, (int) rec.getY() + 5);
                    repaint();
                } else if (i % 4 == 2) {
                    alert.setLocation((int) rec.getX() - 5, (int) rec.getY() - 5);
                    repaint();
                } else if (i % 4 == 3) {
                    alert.setLocation((int) rec.getX() + 5, (int) rec.getY() + 5);
                    repaint();
                }
                if (i > 15) {
                    i = 0;
                    this.cancel();
                }
                i++;
            } catch (Exception e) {
            }
        }
    }
}
