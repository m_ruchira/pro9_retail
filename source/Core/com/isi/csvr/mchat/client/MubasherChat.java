package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.shared.Settings;

import javax.swing.*;

public class MubasherChat {
    public static MubasherChat self = null;

    public MubasherChat() {
        init();
        displayMChat();
    }

    private static void init() {
        try {
            MChatSettings.Load(Settings.CONFIG_DATA_PATH + "/mchat.dll");
            MChatSettings.serverIP = MChatSettings.getString("SERVER_IP");
            MChatSettings.serverRTPort = MChatSettings.getInt("SERVER_PORT");
            MChatSettings.serverFilePort = MChatSettings.getInt("SERVER_FILE_PORT");
            MChatSettings.serverFileIP = MChatSettings.getString("SERVER_FILE_IP");

            MChatSettings.voiceServerPort = MChatSettings.getInt("VOICE_SERVER_PORT");
            MChatSettings.voiceServerIP = MChatSettings.getString("VOICE_SERVER_IP");
//            Settings.isRememberME = (Settings.getInt("REMEMBER_ME") == 1);
//        if (Settings.isRememberME) {
            MChatSettings.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
            MChatMeta.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
//            MChatSettings.NICK_NAME = MChatSettings.getString("NICK_NAME");
//            Settings.isAutoSign = (Settings.getInt("AUTO_SIGN") == 1);
            MChatSettings.isSearchable = (MChatSettings.getInt("IS_SEARCH_ALLOWED") == 1);
            MChatSettings.isVoiceEnable = (MChatSettings.getInt("IS_VOICE_ENABLE") == 1);
//            MChatSettings.isFirstTime = (MChatSettings.getInt("IS_FIRST_TIME") == 1);
            MChatSettings.isOfflineWarningFormDisabled = (MChatSettings.getInt("NO_OFFLINE_WARNING_WINDOW") == 1);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static MubasherChat getSharedInstance() {
        if (self == null) {
            System.out.println("<DEBUG> New Instance");
            self = new MubasherChat();
        }
        return self;
    }

    public void displayMChat() {
        try {
            MChat.getSharedInstance().setProUserID(com.isi.csvr.shared.Settings.getUserID());
            MChat.getSharedInstance().addSystemTrayIcon();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (MChat.getSharedInstance().getExtendedState() == JFrame.ICONIFIED) {
                MChat.getSharedInstance().setExtendedState(JFrame.NORMAL);
            } else {
                MChat.getSharedInstance().setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dispose() {
        self = null;
    }
}
