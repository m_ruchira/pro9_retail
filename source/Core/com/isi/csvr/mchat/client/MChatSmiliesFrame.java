package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.shared.Disposable;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatStringTokenizer;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.*;
import java.util.Hashtable;

public class MChatSmiliesFrame extends JFrame implements Disposable {

    private static MChatSmiliesFrame self = null;
    private static Hashtable<String, String> smiliesList = null;
    private static Hashtable<String, String> smiliesPath = null;
    JPanel iconPanel1 = null;
    JPanel iconPanel2 = null;
    JPanel iconPanel3 = null;
    JPanel iconPanel4 = null;
    JPanel iconPanel5 = null;
    JPanel bannerPaneHistoryEmotions = null;
    JPanel bannerPaneEmotions = null;
    JPanel bannerPaneHistoryLbl = new JPanel();
    // JButton smilebtn = null;
    String stringrep = null;
    int smileCount = 0;
    private boolean isMore = false;
    private boolean isExist = false;
    private boolean exist = false;
    private String completeStr = null;
    private String correct = null;

    public MChatSmiliesFrame() {
        try {
            setTitle("");
            setSize(100, 20);
            setResizable(false);
            smiliesList = new Hashtable<String, String>();
            smiliesPath = new Hashtable<String, String>();

            bannerPaneHistoryLbl.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
            bannerPaneHistoryLbl.setBackground(Color.WHITE);
            JLabel emotionsHistory = new JLabel("Recently used emoticons");
            emotionsHistory.setBackground(Color.WHITE);
            bannerPaneHistoryLbl.add(emotionsHistory);


            bannerPaneHistoryEmotions = new JPanel();
            bannerPaneHistoryEmotions.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            bannerPaneHistoryEmotions.setBackground(Color.WHITE);

            bannerPaneEmotions = new JPanel();
            bannerPaneEmotions.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
            bannerPaneEmotions.setBackground(Color.WHITE);
            JLabel emotions = new JLabel("Pinned emoticons");
            emotions.setBackground(Color.WHITE);
            bannerPaneEmotions.add(emotions);


            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "25", "25", "25", "100%"}, 0, 0));
            iconPanel1 = new JPanel();
            iconPanel1.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            iconPanel1.setBackground(Color.WHITE);
            iconPanel2 = new JPanel();
            iconPanel2.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            iconPanel2.setBackground(Color.WHITE);
            iconPanel3 = new JPanel();
            iconPanel3.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            iconPanel3.setBackground(Color.WHITE);
            iconPanel4 = new JPanel();
            iconPanel4.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            iconPanel4.setBackground(Color.WHITE);
            iconPanel5 = new JPanel();
            iconPanel5.setLayout(new FlexGridLayout(new String[]{"25", "25", "25", "25", "25"}, new String[]{"25"}, 0, 0));
            iconPanel5.setBackground(Color.WHITE);
            WindowFocusListener listener = new WindowFocusListener() {
                public void windowGainedFocus(WindowEvent e) {
                }

                public void windowLostFocus(WindowEvent e) {
                    setVisible(false);
                }

            };
            this.addWindowFocusListener(listener);

            try {
                getTokens();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MChatSmiliesFrame getSharedInstance() {
        if (self == null) {
            self = new MChatSmiliesFrame();
        }
        return self;
    }

    public void getTokens() {
        String line = null;
        String tooltip = null;
        String name = null;
        try {
            BufferedReader in = new BufferedReader(new FileReader("images/mchat/smilies/smilies.txt"));
            while ((line = in.readLine()) != null) {
                try {
                    MChatStringTokenizer st = new MChatStringTokenizer(line, "=");
                    tooltip = st.nextToken().trim();
                    stringrep = st.nextToken().trim();
                    name = st.nextToken().trim();
                    ImageIcon smileimg = new ImageIcon("images/mchat/smilies/" + name);
                    JButton smilebtn = new JButton(smileimg);
                    smilebtn.setToolTipText(tooltip + " " + stringrep);

                    String folder = System.getProperty("user.dir");
                    try {
                        if (stringrep.length() > 0) {
                            smiliesList.put(stringrep, "<img src=\"file:///" + folder + "\\images\\mchat\\smilies\\" + name + "\"></img>");
                            smiliesPath.put(stringrep, folder + "\\images\\mchat\\smilies\\" + name);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    smilebtn.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try {
                                String command = ((JButton) e.getSource()).getToolTipText().split(" ")[1].trim();
                                TWTabbedPane pane = MChatChatForm.tabbedChatPane;
                                JTextPane textPane = ((MChatChatWindow) pane.getComponentAt(pane.getSelectedIndex())).txtMessage;
//                                String smileImg = SmiliesFrame.getSharedInstance().getSmiliesRef(command);
                                textPane.getDocument().insertString(textPane.getDocument().getLength(), command + " ", null);
                                setVisible(false);
                                try {
                                    textPane.setCaretPosition(textPane.getDocument().getLength());
                                    textPane.requestFocus();
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }

//                                ((MChatChatWindow) pane.getComponentAt(pane.getSelectedIndex())).sendChat();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    });
                    if (smileCount < 5) {
                        try {
                            iconPanel1.add(smilebtn);
                            add(iconPanel1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (smileCount < 10) {
                        try {
                            iconPanel2.add(smilebtn);
                            setSize(125, 52);
                            add(iconPanel2);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (smileCount < 15) {
                        try {
                            iconPanel3.add(smilebtn);
                            add(iconPanel3);
                            setSize(125, 75);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (smileCount < 20) {
                        try {
                            iconPanel4.add(smilebtn);
                            setSize(125, 95);
                            add(iconPanel4);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } /*else if (smileCount < 50) {
                        try {
                            iconPanel5.add(smilebtn);
                             add(iconPanel5);
                            setSize(258, 110);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }*/
                    smileCount++;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            in.close();

//            add(bannerPaneHistoryLbl);
//            add(bannerPaneHistoryEmotions);
//            add(bannerPaneEmotions);
//            add(iconPanel1);
//            add(iconPanel2);
//            add(iconPanel3);
//            add(iconPanel4);
//            add(iconPanel5);
//            setTitle(Language.getString("SMILIES_FRAME_TITLE"));
            setUndecorated(true);
//            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean readSmiliesHistory(String name) {
        try {
            int tok = 0;
            int smileCount = 0;
            MChatStringTokenizer st = null;
            String path = "images/mchat/smilies/smilieshistory.txt";
            String ap = "";
            File file = new File(path);
            if (file.exists()) {
                try {
                    FileInputStream fileIn = new FileInputStream(file);
                    while ((tok = fileIn.read()) != -1) {
                        ap = ap + (char) tok;
                    }
                    fileIn.close();
                    try {
                        st = new MChatStringTokenizer(ap, MChatMeta.HDS);
                        while (st.hasMoreElements()) {
                            String str = (String) st.nextToken();
                            MChatStringTokenizer next = new MChatStringTokenizer(str, MChatMeta.HEND, false);
                            while (next.hasMoreElements()) {
                                String smileName = (String) next.nextToken();
                                completeStr = completeStr + MChatMeta.HDS + smileName + MChatMeta.HEND;
                                ++smileCount;
                                if (smileCount == 9) {
                                    isMore = true;
                                    correct = completeStr;
                                    completeStr = "";
                                }
                                if (name.equals(smileName)) {
                                    isExist = true;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
            }
        } catch (Exception e) {
            throw new RuntimeException("String is not in right format!");
        }
        return isExist;
    }

    public void writeSmiliesHistory(String name) {
        int append = 0;
        String string = "";
        exist = readSmiliesHistory(name);
        if (!exist) {
            try {
                String path = "images/mchat/smilies/smilieshistory.txt";
                File file = new File(path);
                if (file.exists()) {
                    if (!isMore) {
                        try {
                            FileWriter fw = new FileWriter(path, true);
                            fw.write(MChatMeta.HDS + name + MChatMeta.HEND);
                            fw.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        FileWriter fw = new FileWriter(path);
                        fw.write(MChatMeta.HDS + name + MChatMeta.HEND + correct);
                        correct = "";
                        completeStr = "";
                        fw.close();
                    }
                } else {
                    try {
                        file.createNewFile();
                        FileWriter fw = new FileWriter(path);
                        fw.write(MChatMeta.HDS + name + MChatMeta.HEND);
                        fw.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
        }
        isExist = false;
        exist = false;
        isMore = false;
        correct = "";
        completeStr = "";
    }

    public void loadHistory() {
        try {
            int tok = 0;
            int smileCount = 0;
            MChatStringTokenizer st = null;
            String path = "images/mchat/smilies/smilieshistory.txt";
            String ap = "";
            File file = new File(path);
            if (file.exists()) {
                try {
                    FileInputStream fileIn = new FileInputStream(file);
                    while ((tok = fileIn.read()) != -1) {
                        ap = ap + (char) tok;
                    }
                    fileIn.close();
                    try {
                        st = new MChatStringTokenizer(ap, MChatMeta.HDS);
                        while (st.hasMoreElements()) {
                            String str = st.nextToken();
                            MChatStringTokenizer next = new MChatStringTokenizer(str, MChatMeta.HEND, false);
                            while (next.hasMoreElements()) {
                                String smileName = next.nextToken();
                                MChatStringTokenizer sm = new MChatStringTokenizer(smileName, "  ", false);
                                String nextsmile = sm.nextToken();
                                ImageIcon smileimg = new ImageIcon("res/sm/sm_history/" + nextsmile);
                                JButton smilebtn = new JButton(smileimg);
                                //                                str = smilebtn.getToolTipText();
                                smilebtn.setToolTipText(smileName);
                                smilebtn.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        JButton btn = (JButton) e.getSource();
                                        //                                        btn.getToolTipText();
                                        //                                        writeSmiliesHistory(btn.getToolTipText());
                                    }
                                });
                                bannerPaneHistoryEmotions.add(smilebtn);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
            }
        } catch (Exception e) {
            throw new RuntimeException("String is not in right format!");
        }
    }

    public String getSmiliesRef(String key) {
        return smiliesList.get(key);
    }

    public String getSmiliesImage(String key) {
        return smiliesPath.get(key);
    }

    public void destroySelf() {
        self = null;
        smiliesList = null;
        smiliesPath = null;
    }
}
