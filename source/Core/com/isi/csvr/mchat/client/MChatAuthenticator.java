package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.Disposable;
import com.isi.csvr.mchat.shared.MChatSettings;

public class MChatAuthenticator extends Thread implements Disposable {
    private static MChatAuthenticator self = null;
    private static boolean isFirstTime = true;

    public MChatAuthenticator() {
        MChatDataStore.getSharedInstance().addDisposableListener(this);
        start();
    }

    public static MChatAuthenticator getSharedInstance() {
        if (self == null) {
            self = new MChatAuthenticator();
        }
        return self;
    }

    public void stopAuthenticator() {
        try {
            this.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        self = null;
    }

    public void run() {
        while (true) {
            try {
                if (com.isi.csvr.shared.Settings.isConnected()
                        && MChatSettings.isAutoSign
                        && !MChatSettings.isAuthenticated
                        && MChatSettings.isConnected && (com.isi.csvr.shared.Settings.getUserID().length() > 0)
                    /*&& MChatSettings.NICK_NAME.length() > 0*/
                    /*&& MChatSettings.isFirstTime*/) {
                    if (isFirstTime) {
                        sleep(5000);
                        isFirstTime = false;
                    }
                    MChatLoginForm.getSharedInstance().signInBtnActionPerformed();
                }
            } catch (Exception e) {
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void destroySelf() {
        isFirstTime = true;
        try {
            stop();
        } catch (Exception e) {
        }
        self = null;
    }
}
