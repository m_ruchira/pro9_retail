package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.Disposable;
import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;
import java.util.TimerTask;


public class MChatPopMessageAlert extends JFrame implements Disposable {
    public static Hashtable<String, Integer> timeMap = null;
    private static MChatPopMessageAlert self;
    JPanel contentPane;
    String message = "";
    String sender = "";
    String source = "";
    String sourceName = "";
    Border border = BorderFactory.createEtchedBorder();
    Border botTitileborder = BorderFactory.createTitledBorder(border, "");
    int i = 0;
    boolean isFile = false;

    public MChatPopMessageAlert(String ms, String sd, String source, String sourceName, boolean isFile) {
        try {
            this.setResizable(false);
            this.setAlwaysOnTop(true);
            this.message = ms;
            this.sender = sd;
            this.source = source;
            this.sourceName = sourceName;
            this.isFile = isFile;
            timeMap = new Hashtable<String, Integer>();
            Image im = Toolkit.getDefaultToolkit().getImage("images/mchat/mchat_en/chat-window-up-icon.png");
            this.setIconImage(im);
            setTitle(MChatLanguage.getString("ALERT"));
            String[] namePanelWidths5 = {"100%"};
            String[] namePanelHeights5 = {"50%", "50%"};

            FlexGridLayout flexGridLayout5 = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 0);
//            contentPane = (JPanel) this.getContentPane();
//            contentPane.setBackground(Color.orange);
            contentPane = new JPanel() {
                public void paint(Graphics g) {
                    paintAlert(g, getWidth(), getHeight());
                    super.paintChildren(g);
                }
            };
            contentPane.setLayout(flexGridLayout5);
            contentPane.setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            JPanel top = new JPanel();
            top.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            contentPane.add(createUpper());
            contentPane.add(createLower());
            contentPane.setBorder(botTitileborder);
            getContentPane().add(contentPane);

            java.util.Timer timer = new java.util.Timer();
            timer.schedule(new DisposingWindow(this), 0, 200);
            setForeground(Theme.getColor("MCHAT_BG_COLOR"));
            GUISettings.applyOrientation(this);
        } catch (SecurityException e) {
        }
    }

    public static MChatPopMessageAlert getSharedInstance(String ms, String sd, String source, String sourceName, boolean isFile) {
        self = new MChatPopMessageAlert(ms, sd, source, sourceName, isFile);
        return self;
    }

    private void paintAlert(Graphics g, int width, int height) {
        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;

            titleLeft = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");
            titleCenter = new ImageIcon("images/mchat/mchat_en/alert.jpg");
            titleRight = new ImageIcon(MChatSettings.IMAGE_PATH + "/emo-bar.jpg");

            iconWidth = titleLeft.getIconWidth();
            if (iconWidth > 0) {
                titleLeft.paintIcon(null, g, 0, 0);
                xOffset = iconWidth;
            }
            iconWidth = titleCenter.getIconWidth();
            if (iconWidth > 0) {
                for (int i = xOffset; i <= width; i += iconWidth) {
                    titleCenter.paintIcon(null, g, i, 0);
                }
            }
            iconWidth = titleRight.getIconWidth();
            if (iconWidth > 0) {
                titleRight.paintIcon(null, g, width - iconWidth, 0);
            }
        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }

    public JPanel createUpper() {
        JPanel upper = null;
        try {
            upper = new JPanel();
            String[] namePanelWidths5 = {"100%"};
            String[] namePanelHeights5 = {"100%"};
            FlexGridLayout flexGridLayoutu = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 5);
            upper.setLayout(flexGridLayoutu);
            upper.add(new JLabel(new ImageIcon("images/mchat/mchat_en/alert.png")));
            JLabel lb_alert = new JLabel("");
            if (MChatLanguage.isLTR) {
                lb_alert.setFont(MChatMeta.defaultFont);
            }
            lb_alert.setForeground(Color.red);
//            upper.add(lb_alert);
        } catch (Exception e) {
        }
        return upper;
    }

    public JPanel createLower() {
        JPanel lower = null;
        try {
            lower = new JPanel();
            String[] namePanelWidths5 = {"100%"};
            String[] namePanelHeights5 = {"80%", "50%"};
            FlexGridLayout flexGridLayoutl = new FlexGridLayout(namePanelWidths5, namePanelHeights5, 0, 5);
            lower.setLayout(flexGridLayoutl);

            final JEditorPane lb_message = new JEditorPane();
//            lb_message.setHorizontalAlignment(SwingConstants.LEFT);
            lb_message.setForeground(Color.BLACK);
            if (MChatLanguage.isLTR) {
                lb_message.setFont(MChatMeta.defaultFont);
            }
            String popMessage = "";
//            if (MChatMeta.isLTR) {
            if (!isFile) {
                String smileImg = MChatSmiliesFrame.getSharedInstance().getSmiliesRef(message);
                if (smileImg != null) {
                    message = smileImg;
                }
                popMessage = "<html><span style='font-size:" + "11" + ";"
                        + " font-family:" + "Tahoma" + ";"
                        + " color:#0000FF" + "'>&nbsp;&nbsp;" + sender + " " + MChatLanguage.getString("SAYS") + message + "</span><html>";
            } else {
                popMessage = "<html><span style='font-size:" + "11" + ";"
                        + " font-family:" + "Tahoma" + ";"
                        + " color:#0000FF" + "'>&nbsp;&nbsp;" + message + "</span><html>";
            }
//            }
            lb_message.setBackground(null);
            lb_message.setEditable(false);
            lb_message.setContentType("text/html");
            lb_message.setText(popMessage);
            try {
                lower.updateUI();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            lb_message.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {

                    try {

                        if (!MChat.openWindows.contains(source)) {
                            MChat.openWindows.add(source);
                            MChatChatWindow cw = MChatDataStore.getChatWindow(source);
                            if (cw == null) {
                                cw = new MChatChatWindow(source, sourceName, MChat.openWindows.size() - 1);
                                MChatDataStore.addChatWindow(source, cw);
                            }
                            MChatChatForm.tabbedChatPane.addTab(sourceName, cw);
                            MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(source), true);
                        }
                        MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(source), true);
                        MChatChatForm.getSharedInstance().setTitle(sourceName + " - " + MChatLanguage.getString("CONVERSATION"));
                        MChatChatForm.getSharedInstance().setVisible(true);
                        MChatChatForm.getSharedInstance().setExtendedState(Frame.NORMAL);

                    } catch (Exception e1) {
                    }
                    self.dispose();
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    lb_message.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                }

                public void mouseExited(MouseEvent e) {
                }
            });

//            lb_message.setText("   "+sender+" "+MChatLanguage.getString("SAYS") + " " + message);
//            lb_message.add(area);

            JLabel lb_sender = new JLabel();
            lb_sender.setHorizontalAlignment(SwingConstants.CENTER);
            lower.add(lb_message);
            lower.add(lb_sender);
        } catch (Exception e) {
        }
        return lower;
    }

    public void destroySelf() {
        timeMap = null;
        self = null;
    }

    public class DisposingWindow extends TimerTask {
        JFrame alert;

        public DisposingWindow(JFrame al) {
            this.alert = al;
        }

        public void run() {
            if (i > 40) {
                i = 0;
                alert.dispose();
                this.cancel();
            }
            i++;
        }
    }
}
