package com.isi.csvr.mchat.client;


import com.isi.csvr.mchat.shared.MChatLanguage;

import javax.swing.*;
import java.awt.*;

public class MChatGUISettings {
    public MChatGUISettings() {
    }

    public static void applyOrientation(Component c) {
        if (MChatLanguage.isLTR) {
            applyOrientation(c, ComponentOrientation.LEFT_TO_RIGHT);
        } else {
//            System.out.println("arabic orientation");
            applyOrientation(c, ComponentOrientation.RIGHT_TO_LEFT);
        }
    }

    public static void applyOrientation(Component c, ComponentOrientation o) {
        try {
            c.setComponentOrientation(o);

            if (c instanceof JMenu) {
                JMenu menu = (JMenu) c;
                int ncomponents = menu.getMenuComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    applyOrientation(menu.getMenuComponent(i), o);
                }
            } else if (c instanceof javax.swing.JCheckBox) {
                javax.swing.JCheckBox obj = (javax.swing.JCheckBox) c;
                if (o == ComponentOrientation.LEFT_TO_RIGHT) {
                    obj.setHorizontalTextPosition(SwingConstants.RIGHT);
                    obj.setHorizontalAlignment(SwingConstants.LEFT);
                } else {
                    obj.setHorizontalTextPosition(SwingConstants.LEFT);
                    obj.setHorizontalAlignment(SwingConstants.RIGHT);
                }
            } else if (c instanceof javax.swing.JRadioButton) {
                javax.swing.JRadioButton obj = (javax.swing.JRadioButton) c;
                if (o == ComponentOrientation.LEFT_TO_RIGHT) {
                    obj.setHorizontalTextPosition(SwingConstants.RIGHT);
                    obj.setHorizontalAlignment(SwingConstants.LEFT);
                } else {
                    obj.setHorizontalTextPosition(SwingConstants.LEFT);
                    obj.setHorizontalAlignment(SwingConstants.RIGHT);
                }
            } else if (c instanceof java.awt.Container) {
                java.awt.Container container = (java.awt.Container) c;
                int ncomponents = container.getComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    try {
                        applyOrientation(container.getComponent(i), o);
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

}
