package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.datastore.MChatFileStore;
import com.isi.csvr.mchat.file.MChatFileAcceptGUIPanel;
import com.isi.csvr.mchat.file.MChatFileServerSender;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.mchat.voice.MChatCallAcceptGUIPanel;
import com.isi.csvr.mchat.voice.MChatVoiceDataStore;
import com.isi.csvr.mchat.voice.MChatVoiceServerSender;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class MChatMessageProcessor implements Disposable {
    public static HashMap chatMessages;
    public static HashMap conversationWindows;
    public static Hashtable<String, Integer> timeMap = null;
    private static ArrayList observers;
    private static MChatMessageProcessor self = null;
    private static HashMap commandMessages;
    private static HashMap conversationMessages;
    private static MChatPopMessageAlert pa = null;
    private MChatConfirmAddForm requesHandle = null;
    private Vector requestVec = null;
    private MChatConfirmAddData requestinfo = null;

    public MChatMessageProcessor() {
        try {
            observers = new ArrayList();
            chatMessages = new HashMap();
            commandMessages = new HashMap();
            conversationWindows = new HashMap();
            conversationMessages = new HashMap();
            self = this;
            timeMap = new Hashtable<String, Integer>();
        } catch (Exception e) {
        }
        MChatDataStore.getSharedInstance().addDisposableListener(this);
    }

    public static MChatMessageProcessor getSharedInstance() {
        if (self == null) {
            self = new MChatMessageProcessor();
        }
        return self;
    }

    public static void notifyObservers() {
        try {
            Iterator i = observers.iterator();
            while (i.hasNext()) {
                MChatChatWindow o = (MChatChatWindow) i.next();
                o.update();
            }
        } catch (Exception e) {
        }
    }

    public HashMap getChatMessages() {
        return chatMessages;
    }

/*    private void processMessageAlerts(String sourceName, String message) {
        DateFormat dateFormatTime = new SimpleDateFormat("mm");
        Date date = new Date();
        if (timeMap.containsKey(sourceName)) {
            int time = timeMap.get(sourceName);
            int dif = Integer.parseInt(dateFormatTime.format(date)) - time;
            if (dif >= 1) {
                timeMap.put(sourceName, Integer.parseInt(dateFormatTime.format(date)));
                if ((MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED)) {
                    try {
                        pa = MChatPopMessageAlert.getSharedInstance(message, sourceName);
                        pa.setUndecorated(true);
                        pa.setSize(240, 120);
                        pa.setVisible(true);
                        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                        pa.setLocation(dim.width - 240, dim.height - 120);
                    } catch (HeadlessException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            timeMap.put(sourceName, Integer.parseInt(dateFormatTime.format(date)));
            if ((MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED)) {
                try {
                    pa = MChatPopMessageAlert.getSharedInstance(message, sourceName);
                    pa.setUndecorated(true);
                    pa.setSize(240, 120);
                    pa.setVisible(true);
                    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                    pa.setLocation(dim.width - 240, dim.height - 120);
                } catch (HeadlessException e) {
                    e.printStackTrace();
                }
            }
        }
    }  */

    public HashMap getCommandMessages() {
        return commandMessages;
    }

    public void processMessages() {
        String messages = null;
        while (MChatDataStore.getSharedInstance().getInQue().size() > 0) {
            try {
                messages = MChatDataStore.getSharedInstance().getFromInque();
                StringTokenizer firstLevelTok = new StringTokenizer(messages, MChatMeta.END, false);
                while (firstLevelTok.hasMoreElements()) {
                    try {
                        String message = ((String) firstLevelTok.nextElement());
                        StringTokenizer secondLevelTok = new StringTokenizer(message, MChatMeta.FS, false);
                        String type_part = ((String) secondLevelTok.nextElement());
                        StringTokenizer dataTok = new StringTokenizer(type_part, MChatMeta.DS, false);
                        String messageID = ((String) dataTok.nextElement()).trim();
                        String messageType = null;
                        if (messageID.equalsIgnoreCase(MChatMeta.MSG_ID)) {
                            messageType = ((String) dataTok.nextElement());
                        }
                        if (messageType.equalsIgnoreCase(MChatMeta.CHAT)) {
                            processChatData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.USERNAME)) {
                            processUsertData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.CONTACT_LIST)) {
                            String data = secondLevelTok.nextToken();
                            try {
                                processContactListData(data);
                            } catch (Exception e) {
                            }
                        } else if (messageType.equalsIgnoreCase(MChatMeta.REGISTER_REQUEST)) {
                            processNewUserData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.ADD_CONTACT)) {
                            processAddContactData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.REMOVE_CONTACT)) {
                            processRemoveConactData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.STATE_CHANGE)) {
                            processStateChangeData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.EDIT_DISPLAY_NAME)) {
                            processEditContactData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.PERSONAL_MESSAGE)) {
                            processPersonalMessage(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.NUDGE_REQUEST)) {
                            processNudgeData(secondLevelTok.nextToken());
                        } else if (messageType.equalsIgnoreCase(MChatMeta.ADD_CONTACT_INVITATION)) {
                            popUpInvitation(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.SEARCH_CONTACT)) {
                            processContactsearcgData(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.PULSE) ||
                                messageType.equalsIgnoreCase(MChatMeta.ONLINE_COUNT)) {
                            processOnlineCount((String) secondLevelTok.nextElement());
                        } else if (messageType.equalsIgnoreCase(MChatMeta.NEW_ADD_RQUEST)) {
                            processNewAddRequest(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.CONVERSATION)) {
//                            processConverations(secondLevelTok);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.FILE_TRANSFER)) {
                            processFileTransfer(message);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.FILE_REJECT)) {
                            processFileReject(message);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.FILE_ACCEPTED)) {
                            processFileAcceptedRequest(message);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.VOICE_CALL)) {
                            processVoiceCall(message);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.VOICE_CALL_ACCEPTED)) {
                            processVoiceAccepted(message);
                        } else if (messageType.equalsIgnoreCase(MChatMeta.VOICE_CALL_REJECT)) {
                            processCallReject(message);
                        }

                    } catch (Exception e) {
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    private void processMessageAlerts(String sourceName, String message, String source, String fontName, String fontSize) {
        DateFormat dateFormatTime = new SimpleDateFormat("mm");
        Date date = new Date();
/*        try {
            pa = MChatPopMessageAlert.getSharedInstance(message, sourceName, source, sourceName, false);
            pa.setUndecorated(true);
            pa.setSize(240, 120);
            pa.setVisible(true);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            pa.setLocation(dim.width - 240, dim.height - 120);
        } catch (HeadlessException e) {
            e.printStackTrace();
        }*/
        if (timeMap.containsKey(sourceName)) {
            int time = timeMap.get(sourceName);
            int dif = Integer.parseInt(dateFormatTime.format(date)) - time;
            if (dif >= 1) {
                timeMap.put(sourceName, Integer.parseInt(dateFormatTime.format(date)));
                if ((MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED)) {
                    try {
                        pa = MChatPopMessageAlert.getSharedInstance(message, sourceName, source, sourceName, false);
                        pa.setUndecorated(true);
                        pa.setSize(240, 120);
                        pa.setVisible(true);
                        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                        pa.setLocation(dim.width - 240, dim.height - 120);
                    } catch (HeadlessException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            timeMap.put(sourceName, Integer.parseInt(dateFormatTime.format(date)));
            if ((MChatChatForm.getSharedInstance().getExtendedState() == JFrame.ICONIFIED)) {
                try {
                    pa = MChatPopMessageAlert.getSharedInstance(message, sourceName, source, sourceName, false);
                    pa.setUndecorated(true);
                    pa.setSize(240, 120);
                    pa.setVisible(true);
                    Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                    pa.setLocation(dim.width - 240, dim.height - 120);
                } catch (HeadlessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void processPersonalMessage(StringTokenizer messageTok) {
        String user = null;
        String newMessage = null;
        String status = null;
        while (messageTok.hasMoreElements()) {
            try {
                String part = messageTok.nextToken();
                StringTokenizer dataTok = new StringTokenizer(part, MChatMeta.DS, false);
                String token = dataTok.nextToken();
                String data = dataTok.nextToken();
                if (token.equalsIgnoreCase(MChatMeta.USERNAME)) {
                    user = data;
                } else if (token.equalsIgnoreCase(MChatMeta.PERSONAL_MESSAGE_CHANGE)) {
                    newMessage = data;
                }
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }
        if (newMessage != null && user != null) {
            try {
                MChatContactObject temp = ((MChatContactObject) MChat.contactObjects.remove(user));
                temp.personalMessage = newMessage;
                try {
                    MChat.contactObjects.put(temp.userID, temp);
                    MChat.contactPanel.removeAll();
                    MChat.getSharedInstance().processContactList();
                    MChat.getSharedInstance().populateContacts();
                    MChat.contactPanel.repaint();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                MChatChatForm.getSharedInstance().setChatWindowMessage(newMessage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            MChat.contactPanel.updateUI();
        } catch (Exception e) {
        }
    }

    private void processVoiceCall(String message) {
        StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
        String typePart = firstTok.nextToken();
        String reqIDPart = firstTok.nextToken();
        String sourcePart = firstTok.nextToken();
        String nickNamePart = firstTok.nextToken();

        String fileReqID = null;
        String fileSource = null;
        String nickName = null;

        StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
            fileReqID = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
            fileSource = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
            nickName = secondTok.nextToken();
        }


        String popMessage = nickName + " is calling you";
        try {
            pa = MChatPopMessageAlert.getSharedInstance(popMessage, nickName, fileSource, nickName, true);
            pa.setUndecorated(true);
            pa.setSize(240, 120);
            pa.setVisible(true);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            pa.setLocation(dim.width - 240, dim.height - 120);
        } catch (HeadlessException e) {
            e.printStackTrace();
        }

        MChatCallAcceptGUIPanel acceptGUIPanelMChat = new MChatCallAcceptGUIPanel(fileSource, fileReqID, nickName);
        String sourceName = "";
        if (fileSource != null) {
            try {
                MChatChatWindow cw = null;
                if (!MChat.openWindows.contains(fileSource)) {
                    sourceName = ((String) MChat.contactList.get(MChat.contactList.indexOf(fileSource) + 1));
                    MChat.openWindows.add(fileSource);
                    cw = MChatDataStore.getChatWindow(fileSource);
                    if (cw == null) {
                        cw = new MChatChatWindow(fileSource, sourceName, MChat.openWindows.size() - 1);
                        MChatDataStore.addChatWindow(fileSource, cw);
                    }
                    MChatChatForm.tabbedChatPane.addTab(sourceName, cw);
                    MChatChatForm.tabbedChatPane.setSelectedComponent(cw);
                    try {
                        cw.displayArea.setCaretPosition(cw.displayArea.getDocument().getLength());
                        cw.displayArea.insertComponent(acceptGUIPanelMChat);
                        cw.displayArea.getDocument().insertString(cw.displayArea.getDocument().getLength(), "  \n", null);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    try {
                        MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(fileSource), true);
                        MChatChatForm.getSharedInstance().setTitle(sourceName + " - " + MChatLanguage.getString("CONVERSATION"));
                        MChatChatForm.getSharedInstance().setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    cw = MChatDataStore.getChatWindow(fileSource);
                    if (cw != null) {
                        try {
                            cw.displayArea.setCaretPosition(cw.displayArea.getDocument().getLength());
                            cw.displayArea.insertComponent(acceptGUIPanelMChat);
                            cw.displayArea.getDocument().insertString(cw.displayArea.getDocument().getLength(), "  \n", null);
                            MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(fileSource), true);
                            MChatChatForm.getSharedInstance().setTitle(cw.nickName + " - " + MChatLanguage.getString("CONVERSATION"));
                        } catch (Exception e7) {
                            e7.printStackTrace();
                        }
                    }
                }
                try {
                    acceptGUIPanelMChat.setDisplayArea(cw.displayArea);
                } catch (Exception e6) {
                    e6.printStackTrace();
                }
                MChatMessageProcessor.notifyObservers();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private void processFileTransfer(String message) {
        StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
        String typePart = firstTok.nextToken();
        String reqIDPart = firstTok.nextToken();
        String sourcePart = firstTok.nextToken();
        String nickNamePart = firstTok.nextToken();
        String fileNamePart = firstTok.nextToken();
        String fileSizePart = firstTok.nextToken();

        String fileReqID = null;
        String fileSource = null;
        String fileName = null;
        String fileSize = null;
        String nickName = null;

        StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
            fileReqID = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
            fileSource = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(fileNamePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_NAME)) {
            fileName = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(fileSizePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_SIZE)) {
            fileSize = secondTok.nextToken();
        }

        secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
        if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
            nickName = secondTok.nextToken();
        }
        String popMessage = nickName + " " + MChatLanguage.getString("SEND_FILE") + " " + fileName;
        try {
            pa = MChatPopMessageAlert.getSharedInstance(popMessage, nickName, fileSource, nickName, true);
            pa.setUndecorated(true);
            pa.setSize(240, 120);
            pa.setVisible(true);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            pa.setLocation(dim.width - 240, dim.height - 120);
        } catch (HeadlessException e) {
            e.printStackTrace();
        }


        MChatFileAcceptGUIPanel acceptGUIPanelMChat = new MChatFileAcceptGUIPanel(fileName, false, System.getProperty("user.home") + "\\My Received Files\\" + fileName, fileReqID, fileSource, fileName, fileSize);
        String sourceName = "";
        if (fileSource != null && fileName != null) {
            try {
                MChatChatWindow cw = null;
                if (!MChat.openWindows.contains(fileSource)) {
                    sourceName = ((String) MChat.contactList.get(MChat.contactList.indexOf(fileSource) + 1));
                    MChat.openWindows.add(fileSource);
                    cw = MChatDataStore.getChatWindow(fileSource);
                    if (cw == null) {
                        cw = new MChatChatWindow(fileSource, sourceName, MChat.openWindows.size() - 1);
                        MChatDataStore.addChatWindow(fileSource, cw);
                    }
                    MChatChatForm.tabbedChatPane.addTab(sourceName, cw);
                    MChatChatForm.tabbedChatPane.setSelectedComponent(cw);
                    try {
                        cw.displayArea.setCaretPosition(cw.displayArea.getDocument().getLength());
                        cw.displayArea.insertComponent(acceptGUIPanelMChat);
                        cw.displayArea.getDocument().insertString(cw.displayArea.getDocument().getLength(), "  \n", null);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    try {
                        MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(fileSource), true);
                        MChatChatForm.getSharedInstance().setTitle(sourceName + " - " + MChatLanguage.getString("CONVERSATION"));
                        MChatChatForm.getSharedInstance().setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    cw = MChatDataStore.getChatWindow(fileSource);
                    if (cw != null) {
                        try {
                            cw.displayArea.setCaretPosition(cw.displayArea.getDocument().getLength());
                            cw.displayArea.insertComponent(acceptGUIPanelMChat);
                            cw.displayArea.getDocument().insertString(cw.displayArea.getDocument().getLength(), "  \n", null);
                            MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(fileSource), true);
                            MChatChatForm.getSharedInstance().setTitle(cw.nickName + " - " + MChatLanguage.getString("CONVERSATION"));
                        } catch (Exception e7) {
                            e7.printStackTrace();
                        }
                    }
                }
                try {
                    acceptGUIPanelMChat.setDisplayArea(cw.displayArea);
                } catch (Exception e6) {
                    e6.printStackTrace();
                }
                MChatMessageProcessor.notifyObservers();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private void processFileReject(String message) {
        try {
            StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
            String typePart = firstTok.nextToken();
            String reqIDPart = firstTok.nextToken();
            String sourcePart = firstTok.nextToken();
            String nickNamePart = firstTok.nextToken();
            String fileNamePart = firstTok.nextToken();
            String fileSizePart = firstTok.nextToken();

            String fileReqID = null;
            String fileSource = null;
            String fileName = null;
            String fileSize = null;
            String nickName = null;

            StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
                fileReqID = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
                fileSource = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NAME)) {
                fileName = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileSizePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SIZE)) {
                fileSize = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
                nickName = secondTok.nextToken();
            }

            MChatFileServerSender fileSender = MChatFileStore.getSharedInstance().getSender(fileReqID);
            if (fileSender != null) {
                try {
                    fileSender.getProgressPanel().getProgressBar().setVisible(false);
                    fileSender.getProgressPanel().getLable().setForeground(Color.red);
                    fileSender.getProgressPanel().getLable().setText(fileSender.getProgressPanel().getLable().getText() + " " + MChatLanguage.getString("FILE_REJECTED"));
                    fileSender.getProgressPanel().getLable().setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/graph_tb_open_act.gif"));
                    fileSender.getProgressPanel().setCancelButtonInvisible();
                    fileSender.getProgressPanel().removeMouseListener();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    fileSender.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            MChatFileStore.getSharedInstance().removeFromSender(fileReqID);
        } catch (Exception e) {
        }
    }

    private void processCallReject(String message) {
        try {
            StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
            String typePart = firstTok.nextToken();
            String reqIDPart = firstTok.nextToken();
            String sourcePart = firstTok.nextToken();
            String nickNamePart = firstTok.nextToken();
            String fileNamePart = firstTok.nextToken();
            String fileSizePart = firstTok.nextToken();

            String fileReqID = null;
            String fileSource = null;
            String fileName = null;
            String fileSize = null;
            String nickName = null;

            StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
                fileReqID = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
                fileSource = secondTok.nextToken();
            }

/*
            secondTok = new StringTokenizer(fileNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NAME)) {
                fileName = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileSizePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SIZE)) {
                fileSize = secondTok.nextToken();
            }
*/

            secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
                nickName = secondTok.nextToken();
            }

            MChatVoiceServerSender voiceSender = MChatVoiceDataStore.getSharedInstance().getSender(fileReqID);
            if (voiceSender != null) {
                try {
//                    voiceSender.getProgressPanel().getProgressBar().setVisible(false);
//                    voiceSender.getProgressPanel().getLable().setForeground(Color.red);
                    voiceSender.getCallGUIPanel().getJlabel().setText(MChatLanguage.getString("CALL_REJECT_3") + " " + nickName);
                    voiceSender.getCallGUIPanel().getJlabel().setIcon(null);
//                    voiceSender.getProgressPanel().getLable().setIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/graph_tb_open_act.gif"));
//                    voiceSender.getProgressPanel().setCancelButtonInvisible();
//                    voiceSender.getProgressPanel().removeMouseListener();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    voiceSender.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            MChatFileStore.getSharedInstance().removeFromSender(fileReqID);
        } catch (Exception e) {
        }
    }

    private void processVoiceAccepted(String message) {
        try {
            StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
            String typePart = firstTok.nextToken();
            String reqIDPart = firstTok.nextToken();
            String sourcePart = firstTok.nextToken();
            String nickNamePart = firstTok.nextToken();
//            String fileNamePart = firstTok.nextToken();
//            String fileSizePart = firstTok.nextToken();

            String fileReqID = null;
            String fileSource = null;
            String fileName = null;
            String fileSize = null;
            String nickName = null;

            StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
                fileReqID = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
                fileSource = secondTok.nextToken();
            }

/*
            secondTok = new StringTokenizer(fileNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NAME)) {
                fileName = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileSizePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SIZE)) {
                fileSize = secondTok.nextToken();
            }
*/

            secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
                nickName = secondTok.nextToken();
            }

            MChatVoiceServerSender voiceSender = MChatVoiceDataStore.getSharedInstance().getSender(fileReqID);
            if (voiceSender != null) {
                try {
                    synchronized (voiceSender) {
                        voiceSender.notify();
                    }
                } catch (Exception e) {
                }
            }
//            voiceSender.getCallGUIPanel().getJlabel().setText(MChatLanguage.getString("CALL_REJECT_3")+ " "+nickName);
            voiceSender.getCallGUIPanel().getJlabel().setIcon(new ImageIcon("images/mchat/mchat_en/incall.gif"));
        } catch (Exception e) {
        }
    }

    private void processFileAcceptedRequest(String message) {
        try {
            StringTokenizer firstTok = new StringTokenizer(message, MChatMeta.FS);
            String typePart = firstTok.nextToken();
            String reqIDPart = firstTok.nextToken();
            String sourcePart = firstTok.nextToken();
            String nickNamePart = firstTok.nextToken();
            String fileNamePart = firstTok.nextToken();
            String fileSizePart = firstTok.nextToken();

            String fileReqID = null;
            String fileSource = null;
            String fileName = null;
            String fileSize = null;
            String nickName = null;

            StringTokenizer secondTok = new StringTokenizer(reqIDPart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_REQUEST_ID)) {
                fileReqID = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(sourcePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SOURCE_ID)) {
                fileSource = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NAME)) {
                fileName = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(fileSizePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_SIZE)) {
                fileSize = secondTok.nextToken();
            }

            secondTok = new StringTokenizer(nickNamePart, MChatMeta.DS);
            if (secondTok.nextToken().equals(MChatMeta.FILE_NICK_NAME)) {
                nickName = secondTok.nextToken();
            }

            MChatFileServerSender fileSender = MChatFileStore.getSharedInstance().getSender(fileReqID);
            if (fileSender != null) {
                try {
                    synchronized (fileSender) {
                        fileSender.notify();
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }

    private String showSaveDialog(String dialogTitle, String approveButtonText, String approveButtonToolTip,
                                  char approveButtonMnemonic, File file) {
        JFileChooser files = new JFileChooser();
        files.setDialogTitle(dialogTitle);
        files.setApproveButtonText(approveButtonText);
        files.setApproveButtonMnemonic(approveButtonMnemonic);
        files.setApproveButtonToolTipText(approveButtonToolTip);
        files.setFileSelectionMode(files.FILES_ONLY);
        files.rescanCurrentDirectory();
        files.setMultiSelectionEnabled(false);
        files.setSelectedFile(file);

        int result = files.showSaveDialog(MChat.getSharedInstance());

        return ((result == files.APPROVE_OPTION) ? files.getSelectedFile().getAbsolutePath() : "");
    }

    private void processConverations(StringTokenizer msg) {
        try {
            String messageSenderPart = msg.nextToken();
            String msgPart = msg.nextToken();
            String convIDPart = msg.nextToken();
            String originatorPart = msg.nextToken();
            String convMembersPart = msg.nextToken();

            StringTokenizer idTok = new StringTokenizer(convIDPart, MChatMeta.DS);
            if (idTok.nextToken().equalsIgnoreCase(MChatMeta.CONVERSATION)) {
                String conID = idTok.nextToken();
                MChatChatWindow cw = null;

                StringTokenizer originatorTok = new StringTokenizer(originatorPart, MChatMeta.DS);
                String originatorID = null;
                if (originatorTok.nextToken().equalsIgnoreCase(MChatMeta.CONVERSATION_ORIGINATOR)) {
                    originatorID = originatorTok.nextToken();
                    //                originatorID = "thushara";

                    if (conversationWindows.containsKey(conID)) {
                        cw = (MChatChatWindow) conversationWindows.get(conID);
                    } else {
                        if (!MChat.openWindows.contains(originatorID)) {
                            MChat.openWindows.add(originatorID);

                            cw = new MChatChatWindow(originatorID, ((MChatContactObject) MChat.contactObjects.get(originatorID)).nickName, MChat.openWindows.size() - 1);
                            MChatChatForm.tabbedChatPane.addTab(((MChatContactObject) MChat.contactObjects.get(originatorID)).nickName, cw);
                            MChatChatForm.tabbedChatPane.setSelectedComponent(cw);
//                            new CloseTabButton(ChatForm.tabbedChatPane, MChat.openWindows.size() - 1, originatorID);
                        }
                        cw.isInConversation = true;
                        cw.conversationID = conID;
                        cw.convOriginator = originatorID;

                        StringTokenizer convMembersTok = new StringTokenizer(convMembersPart, MChatMeta.DS);
                        if (convMembersTok.nextToken().equalsIgnoreCase(MChatMeta.CONVERSATION_LIST)) {
                            StringTokenizer desTok = new StringTokenizer(convMembersTok.nextToken(), ",");
                            String destinations = originatorID;
                            while (desTok.hasMoreTokens()) {
                                String contact = desTok.nextToken();
                                if (contact.equals(MChatSettings.MUBASHER_ID)) {
                                    continue;
                                } else {
                                    destinations = destinations + "," + contact;
                                }
                            }
                            cw.convDestinations = destinations;
                        }

                        addToConvHash(conID, cw);
                        MChatQueue msgQue = new MChatQueue();
                        addtoConvMsgHash(conID, msgQue);

                        Rectangle rec = MChat.getSharedInstance().getBounds();
//                        MChat.chatFrame.setLocation((int) rec.getX() - 100, (int) rec.getY() + 50);
                        MChatChatForm.getSharedInstance().setVisible(true);
                    }

                    StringTokenizer msgSenderTok = new StringTokenizer(messageSenderPart, MChatMeta.DS);

                    if (msgSenderTok.nextToken().equalsIgnoreCase(MChatMeta.DES_ID)) {
                        String msgSender = msgSenderTok.nextToken();
                        StringTokenizer msgTok = new StringTokenizer(msgPart, MChatMeta.DS);
                        if (msgTok.nextToken().equalsIgnoreCase(MChatMeta.CONVERSATION_MESSAGE)) {
                            String message = msgTok.nextToken();

                            if (!msgSender.equalsIgnoreCase("") && !message.equalsIgnoreCase("")) {
                                String senderNickName = ((MChatContactObject) MChat.contactObjects.get(msgSender)).nickName;
                                ((MChatQueue) getConvMsgHash().get(conID)).add(senderNickName + " : " + message);
                                notifyObservers();
                            }
                        }
                    }


                }
            }
        } catch (Exception e) {
        }
    }

    private void processNewAddRequest(StringTokenizer requestTok) {
        try {
            requestinfo = new MChatConfirmAddData();
            requesHandle = new MChatConfirmAddForm();
            while (requestTok.hasMoreElements()) {
                StringTokenizer addRequestTok = new StringTokenizer(((String) requestTok.nextElement()), MChatMeta.DS, false);
                String contactResponse = ((String) addRequestTok.nextElement());
                String request = ((String) addRequestTok.nextElement());
                if (contactResponse.equalsIgnoreCase(MChatMeta.NEW_ADD_RQUEST)) {
                    if (!request.equals("")) {
                        requestinfo.setRequestData(request);
                    }
                }
            }
            requestVec = requestinfo.getRequest();
            if (((String) requestVec.elementAt(0)).length() > 0) {
                requesHandle.initializeComponents((String) requestVec.elementAt(0), (String) requestVec.elementAt(1), (String) requestVec.elementAt(2), (String) requestVec.elementAt(3), (String) requestVec.elementAt(4));
            }
        } catch (Exception e) {
        }
    }

    private void processOnlineCount(String data) {
        try {
            StringTokenizer tok = new StringTokenizer(data, MChatMeta.DS);
            String tag = (String) tok.nextElement();
            MChatSettings.onlineUserCount = Integer.parseInt((String) tok.nextElement());
            String message = " " + MChatSettings.onlineUserCount + " " + MChatLanguage.getString("NO_OF_USERS_ONLINE");
            MChat.labelOnlineCount.setForeground(Color.white);
            MChat.labelOnlineCount.setText(message);
            if (MChatLanguage.isLTR) {
                MChat.labelOnlineCount.setFont(MChatMeta.defaultFont);
            }
        } catch (Exception e) {

        }
    }

    private void processContactsearcgData(StringTokenizer messageTok) {
        try {
            String typePart = null;
            String searchPart = null;
            Object[] columnNames = new Object[]{MChatLanguage.getString("STATUS"), MChatLanguage.getString("ID"), MChatLanguage.getString("FNAME"), MChatLanguage.getString("LNAME"), MChatLanguage.getString("PRO_ID")};
            try {
                StringTokenizer tok = new StringTokenizer(messageTok.nextToken(), MChatMeta.DS);
                typePart = tok.nextToken();
                searchPart = tok.nextToken();
            } catch (Exception e) {
            }
            int noOfResults = 1;

            if (typePart.equalsIgnoreCase(MChatMeta.SEARCH_STRING)) {
                if (searchPart.equalsIgnoreCase(MChatMeta.NO_SEARCH_RESULTS_FOUND)) {
                    MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("NO_RESULTS_FOUND"), 1);
                    MChatSearchForm.getSharedInstance().setSearchInProgress(false);
                    MChat.getSharedInstance().setSearchedContacts(new Object[0][0], columnNames);
                } else {
                    StringTokenizer resultsTok = null;
                    try {
                        resultsTok = new StringTokenizer(searchPart, "#");
                        noOfResults = resultsTok.countTokens();
                    } catch (Exception e) {
                    }
                    Object[][] contactData = new Object[noOfResults][5];
                    for (int i = 0; resultsTok.hasMoreTokens(); i++) {
                        try {
                            StringTokenizer fieldData = null;
                            try {
                                fieldData = new StringTokenizer(resultsTok.nextToken(), ",");
                            } catch (Exception e) {
                                continue;
                            }
                            String status = null;
                            String proID = null;
                            String contactID = null;
                            String fName = null;
                            String lName = null;
                            try {
                                proID = fieldData.nextToken();
                                contactID = fieldData.nextToken();
                                fName = fieldData.nextToken();
                                lName = fieldData.nextToken();
                                status = fieldData.nextToken();
                            } catch (Exception e) {

                            }
                            if (!(proID.equalsIgnoreCase("") || proID.equalsIgnoreCase(null))) {
                                if (status.equals(MChatMeta.ONLINE_SYMBOL)) {
                                    contactData[i][0] = MChat.onlineImg;
                                } else if (status.equals(MChatMeta.OFFLINE_TAG)) {
                                    contactData[i][0] = MChat.offlineImg;
                                } else if (status.equals(MChatMeta.BUSY_SYMBOL)) {
                                    contactData[i][0] = MChat.busyImg;
                                } else if (status.equals(MChatMeta.AWAY_SYMBOL)) {
                                    contactData[i][0] = MChat.awayImg;
                                }
                                contactData[i][1] = contactID;
                                contactData[i][2] = fName;
                                contactData[i][3] = lName;
                                contactData[i][4] = proID;
                            }
                        } catch (Exception e) {
                        }
                    }
                    MChat.getSharedInstance().setSearchedContacts(contactData, columnNames);
                }
            }
        } catch (Exception e) {

        }
    }

    private void popUpInvitation(StringTokenizer messageTok) {
        try {
            StringTokenizer tok = new StringTokenizer(messageTok.nextToken(), MChatMeta.DS);
            String typePart = tok.nextToken();
            String contactPart = tok.nextToken();

            if (typePart.equalsIgnoreCase(MChatMeta.ADD_CONTACT_INVITATION)) {
                int ans = JOptionPane.showConfirmDialog(null, contactPart + MChatLanguage.getString("ALLOW_CONTACT_ADD"), MChatLanguage.getString("INVITATION"), JOptionPane.YES_NO_OPTION);
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.sendInvitationReply(Integer.toString(ans)));
            }
        } catch (Exception e) {
        }
    }

    private void processNudgeData(String data) {
        try {
            StringTokenizer toks = new StringTokenizer(data, MChatMeta.DS, false);
            String user = null;
            String tok = null;
            while (toks.hasMoreTokens()) {
                tok = toks.nextToken();
                if (tok.equalsIgnoreCase(MChatMeta.USERNAME)) {
                    user = toks.nextToken();
                }
            }
            if (user != null) {
                MChatQueue chatQue;
                if (chatMessages.containsKey(user)) {
                    chatQue = ((MChatQueue) chatMessages.get(user));
                    chatMessages.remove(user);
                } else {
                    chatQue = new MChatQueue();
                }
                chatQue.add(MChatLanguage.getString("NUDGE"));
                String sourceName = ((String) MChat.contactList.get(MChat.contactList.indexOf(user) + 1));
                chatMessages.put(user, chatQue);
                if (!MChat.openWindows.contains(user)) {
                    MChat.openWindows.add(user);
                    MChatChatWindow cw = MChatDataStore.getChatWindow(user);
                    if (cw == null) {
                        cw = new MChatChatWindow(user, sourceName, MChat.openWindows.size() - 1);
                        MChatDataStore.addChatWindow(user, cw);
                    }
                    MChatChatForm.tabbedChatPane.addTab(sourceName, cw);
                }
                try {
                    MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(user), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyObservers();
                messageAlert(MChatLanguage.getString("NUDGE"), sourceName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processStateChangeData(StringTokenizer messageTok) {
        try {
            String user = null;
            String status = null;
            String nickName = null;
            while (messageTok.hasMoreElements()) {
                try {
                    String part = messageTok.nextToken();
                    StringTokenizer dataTok = new StringTokenizer(part, MChatMeta.DS, false);
                    String token = dataTok.nextToken();
                    String data = dataTok.nextToken();
                    if (token.equalsIgnoreCase(MChatMeta.USERNAME)) {
                        user = data;
                    } else if (token.equalsIgnoreCase(MChatMeta.STATUS)) {
                        status = data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (user != null && status != null) {
                try {
                    MChatContactObject temp = ((MChatContactObject) MChat.contactObjects.remove(user));
                    temp.status = status;
                    try {
                        MChat.contactObjects.put(temp.userID, temp);
                        MChat.contactPanel.removeAll();
                        MChat.getSharedInstance().processContactList();
                        MChat.getSharedInstance().populateContacts();
                        MChat.contactPanel.repaint();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        MChatChatForm.tabbedChatPane.highlightTab(MChatChatForm.tabbedChatPane.getSelectedIndex(), true);
                    } catch (Exception e) {
                    }
                    MChatChatForm.getSharedInstance().setLastMessage(null);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
        try {
            MChat.contactPanel.updateUI();
        } catch (Exception e) {
        }
    }

    private void processEditContactData(StringTokenizer messageTok) {
        try {
            String user = null;
            String newDisplayName = null;
            String nickName = null;
            while (messageTok.hasMoreElements()) {
                try {
                    String part = messageTok.nextToken();
                    StringTokenizer dataTok = new StringTokenizer(part, MChatMeta.DS, false);
                    String token = dataTok.nextToken();
                    String data = dataTok.nextToken();
                    if (token.equalsIgnoreCase(MChatMeta.EDIT_DISPLAY_NAME)) {
                        newDisplayName = data;
                    } else if (token.equalsIgnoreCase(MChatMeta.USERNAME)) {
                        user = data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (user != null && newDisplayName != null) {
                try {
                    MChatMeta.DISPLAY_ID = newDisplayName;
                    MChatContactObject temp = ((MChatContactObject) MChat.contactObjects.remove(user));
                    temp.nickName = newDisplayName;
                    try {
                        MChat.contactObjects.put(temp.userID, temp);
                        MChat.contactPanel.removeAll();
                        MChat.getSharedInstance().processContactList();
                        MChat.getSharedInstance().populateContacts();
                        MChat.contactPanel.repaint();
                        if (temp.status.equalsIgnoreCase(MChatMeta.OFFLINE_SYMBOL)) {
                            MChat.statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("OFFLINE") + ")");
                        } else if (temp.status.equalsIgnoreCase(MChatMeta.ONLINE_SYMBOL)) {
                            MChat.statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("ONLINE") + ")");
                        } else if (temp.status.equalsIgnoreCase(MChatMeta.BUSY_SYMBOL)) {
                            MChat.statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("BUSY") + ")");
                        } else if (temp.status.equalsIgnoreCase(MChatMeta.AWAY_SYMBOL)) {
                            MChat.statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("AWAY") + ")");
                        } else if (temp.status.equalsIgnoreCase(MChatMeta.PENDING_SYMBOL)) {
                            MChat.statusBotton.setText(MChatMeta.DISPLAY_ID + " (" + MChatLanguage.getString("OFFLINE") + ")");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        MChatChatForm.tabbedChatPane.highlightTab(MChatChatForm.tabbedChatPane.getSelectedIndex(), true);
                    } catch (Exception e) {
                    }
                    MChatChatForm.getSharedInstance().setLastMessage(null);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
        try {
            MChat.contactPanel.updateUI();
        } catch (Exception e) {
        }
    }

    private void processAddContactData(StringTokenizer messageTok) {
        String typePart;
        try {
            typePart = messageTok.nextToken();
            StringTokenizer statusTok = new StringTokenizer(typePart, MChatMeta.DS);
            String temp1 = statusTok.nextToken();
            String temp2 = statusTok.nextToken();
            if (temp1.equalsIgnoreCase(MChatMeta.ADD_CONTACT)) {
                if (temp2.equalsIgnoreCase(MChatMeta.ADD_CONTACT_SUCCESS)) {
                    MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("CONTACT_ADD_SUCCESS"), 0);
                } else if (temp2.equalsIgnoreCase(MChatMeta.ADD_CONTACT_ERROR)) {
                    MChatSearchForm.getSharedInstance().giveMsg(MChatLanguage.getString("CANNOT_ADD"), 1);
                    return;
                }
            }

        } catch (Exception e) {
        }
    }

    private void processRemoveConactData(StringTokenizer messageTok) {
        try {
            String temp = messageTok.nextToken();
            StringTokenizer tok = null;
            try {
                tok = new StringTokenizer(temp, MChatMeta.DS);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String typePart = null;
            try {
                typePart = tok.nextToken();
            } catch (Exception e) {
                e.printStackTrace();
            }
            String dataPart = tok.nextToken();

            if (typePart.equalsIgnoreCase(MChatMeta.REMOVE_CONTACT)) {
                if (dataPart.equalsIgnoreCase(MChatMeta.REMOVE_CONTACT_SUCCESS)) {
                    MChat.contactPanel.repaint();
                    MChat.getSharedInstance().setVisible(true);
                } else if (dataPart.equalsIgnoreCase(MChatMeta.REMOVE_CONTACT_FAILED)) {
                    JOptionPane.showMessageDialog(null, MChatLanguage.getString("CANNOT_REMOVE") + " " + MChat.unwantedContact, MChatLanguage.getString("ERROR"), 3);
                    MChat.unwantedContact = "";
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    private void processUsertData(StringTokenizer messagetok) {
        try {
            MChat.getSharedInstance().hideLoadingImage();
        } catch (Exception e) {
        }

        try {
            String userData = ((String) messagetok.nextElement());
            StringTokenizer dataTok = new StringTokenizer(userData, MChatMeta.DS, false);
            String userTag = ((String) dataTok.nextElement());
            String userName = ((String) dataTok.nextElement());
            if (userTag.equalsIgnoreCase(MChatMeta.USERNAME)) {
                if (userName.equalsIgnoreCase(MChatMeta.ERROR_INVALID_NICKNAME)) {
                    MChatLoginForm.getSharedInstance().giveMsg(MChatLanguage.getString("INVALID_NICKNAME"), 1);
                    MChatSettings.isAuthenticated = false;
                    MChatLoginForm.loadingLabel.setVisible(false);
                } else if (userName.equalsIgnoreCase(MChatMeta.ERROR_NEW_USER)
                        || (userName.equalsIgnoreCase(MChatMeta.NICK_NAME_NOT_EXIST))) {
                    MChatLoginForm.getSharedInstance().giveMsg(MChatLanguage.getString("USER_DONT_EXIST"), 1);
                    MChatSettings.isAuthenticated = false;
                    MChatMeta.DISPLAY_ID = "";
                    MChat.getSharedInstance().cardLayout.show(MChat.mainPanel, "RegistrationForm");
                    MChatLoginForm.loadingLabel.setVisible(false);
                } else {
                    MChatSettings.isAuthenticated = true;
                    MChatMeta.DISPLAY_ID = userName;
                    MChat.getSharedInstance().showContactList();
                    MChat.signOut.enable(true);
//                    if (MChatLanguage.isLTR) {
                    MChat.statusBotton.setFont(MChatMeta.defaultFont);
                    String online = userName + " (" + MChatLanguage.getString("ONLINE") + ")";
                    MChat.statusBotton.setText(online);
//                    }

                    try {
                        synchronized (MChatPulseSender.getSharedInstance()) {
                            MChatPulseSender.getSharedInstance().notify();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        MChatSettings.save();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processNewUserData(StringTokenizer messagetok) {
        try {
            String statusData = ((String) messagetok.nextElement());
            StringTokenizer dataTok = new StringTokenizer(statusData, MChatMeta.DS, false);
            String newUserTag = ((String) dataTok.nextElement());
            String statusTag = ((String) dataTok.nextElement());
            if (newUserTag.equalsIgnoreCase(MChatMeta.REGISTER_REQUEST)) {
                if (statusTag.equalsIgnoreCase(MChatMeta.REGISTRATION_SUCSESS)) {
                    MChatSettings.isFirstTime = false;
                    try {
                        MChatSettings.save();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (MChatLanguage.isLTR) {
                        MChat.statusBotton.setFont(MChatMeta.defaultFont);
                        if (MChat.statusBotton.getText().equalsIgnoreCase("")) {
                            if (MChatMeta.STATUS.equalsIgnoreCase("0")) {
                                String offline = MChatRegistrationForm.getSharedInstance().getNickName() + " (" + MChatLanguage.getString("OFFLINE") + ")";
                                MChat.statusBotton.setText(offline);
                                MChat.statusBotton.setToolTipText(MChat.statusBotton.getText());
                            } else {
                                String online = MChatRegistrationForm.getSharedInstance().getNickName() + " (" + MChatLanguage.getString("ONLINE") + ")";
                                MChat.statusBotton.setText(online);
                                MChat.statusBotton.setToolTipText(MChat.statusBotton.getText());
                            }
                        }
                    } else {
                        MChat.statusBotton.setFont(MChatMeta.defaultFont);
                        if (MChat.statusBotton.getText().equalsIgnoreCase("")) {
                            if (MChatMeta.STATUS.equalsIgnoreCase("0")) {
                                String offline = MChatRegistrationForm.getSharedInstance().getNickName() + " (" + MChatLanguage.getString("OFFLINE") + ")";
                                MChat.statusBotton.setText(offline);
                                MChat.statusBotton.setToolTipText(MChat.statusBotton.getText());
                            } else {
                                String online = MChatRegistrationForm.getSharedInstance().getNickName() + " (" + MChatLanguage.getString("ONLINE") + ")";
                                MChat.statusBotton.setText(online);
                                MChat.statusBotton.setToolTipText(MChat.statusBotton.getText());
                            }
                        }
                    }
                    MChat.getSharedInstance().showContactList();
                    try {
                        synchronized (MChatPulseSender.getSharedInstance()) {
                            MChatPulseSender.getSharedInstance().notify();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (statusTag.equalsIgnoreCase(MChatMeta.ERROR_IN_REGISTRATION)) {
                    MChatRegistrationForm.getSharedInstance().giveMsg(MChatLanguage.getString("ERROR_REGISTRATION"), 1);
                    MChatRegistrationForm.getSharedInstance().newLoginNameTF.setText("");
                    MChatRegistrationForm.getSharedInstance().firstNameTF.setText("");
                    MChatRegistrationForm.getSharedInstance().lastNameTF.setText("");
                    MChatMeta.DISPLAY_ID = "";
                    MChatMeta.FIRST_NAME = "";
                    MChatMeta.LAST_NAME = "";
                    return;
                } else if (statusTag.equalsIgnoreCase(MChatMeta.ALREADY_REGISTERED)) {
                    MChatRegistrationForm.getSharedInstance().giveMsg(MChatLanguage.getString("USER_EXIST"), 1);
                    MChatRegistrationForm.getSharedInstance().newLoginNameTF.setText("");
                    MChatRegistrationForm.getSharedInstance().firstNameTF.setText("");
                    MChatRegistrationForm.getSharedInstance().lastNameTF.setText("");
                    MChatMeta.DISPLAY_ID = "";
                    MChatMeta.FIRST_NAME = "";
                    MChatMeta.LAST_NAME = "";
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    private void processChatData(StringTokenizer tokens) {
        try {
            String source = null;
            String sourceName = null;
            String message = null;
            String messageOnly = null;
            String fontName = null;
            String fontSize = null;
            String bold = null;
            String italic = null;
            String dateTime = null;
            boolean isOffline = false;

            while (tokens.hasMoreElements()) {
                try {
                    StringTokenizer dataTok = new StringTokenizer(((String) tokens.nextElement()), MChatMeta.DS, false);
                    String tok = ((String) dataTok.nextElement());
                    String data = ((String) dataTok.nextElement());
                    if (tok.equalsIgnoreCase(MChatMeta.DES_ID)) {
                        source = data;
                    } else if (tok.equalsIgnoreCase(MChatMeta.TEXT_MESSAGE)) {
                        message = data;
                        messageOnly = data;
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.DISPLAY_MESSEGE_FONT_NAME))) {
                        fontName = data;
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.DISPLAY_MESSEGE_FONT_SIZE))) {
                        fontSize = data;
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.DISPLAY_MESSEGE_FONT_ITALIC))) {
                        italic = data;
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.DISPLAY_MESSEGE_FONT_BOLD))) {
                        bold = data;
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.DATE_TIME))) {
                        dateTime = data;
                        System.out.println("date " + dateTime);
                    } else if (tok.equalsIgnoreCase(String.valueOf(MChatMeta.IS_OFFLINE_MESSAGE))) {
                        isOffline = data.equalsIgnoreCase("1");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (source != null && message != null) {
                MChatQueue chatQue;
                if (chatMessages.containsKey(source)) {
                    chatQue = ((MChatQueue) chatMessages.get(source));
                    chatMessages.remove(source);
                } else {
                    chatQue = new MChatQueue();
                }
                message = message + MChatMeta.FS +
                        MChatMeta.DISPLAY_MESSEGE_FONT_NAME + MChatMeta.DS + fontName + MChatMeta.FS +
                        MChatMeta.DISPLAY_MESSEGE_FONT_SIZE + MChatMeta.DS + fontSize + MChatMeta.FS +
                        MChatMeta.DISPLAY_MESSEGE_FONT_ITALIC + MChatMeta.DS + italic + MChatMeta.FS +
                        MChatMeta.DISPLAY_MESSEGE_FONT_BOLD + MChatMeta.DS + bold + MChatMeta.FS;
                if (isOffline) {
                    message = message + MChatMeta.DATE_TIME + MChatMeta.DS + dateTime + MChatMeta.FS +
                            MChatMeta.IS_OFFLINE_MESSAGE + MChatMeta.DS + "1" + MChatMeta.FS;
                }
                chatQue.add(message);
                sourceName = ((String) MChat.contactList.get(MChat.contactList.indexOf(source) + 1));
//                if (MChatLanguage.isLTR) {
//                    MChatHistoryChatCreate.getSharedInstance().writeHistorytFile(sourceName + ".txt", messageOnly, sourceName, MChatMeta.DISPLAY_ID);
//                }
//                MChatHistoryChatCreate.getSharedInstance().writeHistorytFile(sourceName + ".txt", messageOnly, sourceName, MChatMeta.DISPLAY_ID);
                chatMessages.put(source, chatQue);
                if (!MChat.openWindows.contains(source)) {
                    MChat.openWindows.add(source);
                    MChatChatWindow cw = MChatDataStore.getChatWindow(source);
                    if (cw == null) {
                        cw = new MChatChatWindow(source, sourceName, MChat.openWindows.size() - 1);
                        MChatDataStore.addChatWindow(source, cw);
                    }
                    MChatChatForm.tabbedChatPane.addTab(sourceName, cw);
                    MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(source), true);
                }
                MChatChatForm.tabbedChatPane.highlightTab(MChat.openWindows.indexOf(source), true);
                MChatChatForm.getSharedInstance().setTitle(sourceName + " - " + MChatLanguage.getString("CONVERSATION"));
                if (!MChatChatForm.getSharedInstance().isVisible()) {
                    MChatChatForm.getSharedInstance().setVisible(true);
                    MChatChatForm.getSharedInstance().setExtendedState(JFrame.ICONIFIED);
                }
                notifyObservers();
                try {
                    processMessageAlerts(sourceName, messageOnly, source, fontName, fontSize);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
        }
    }

    private void processContactListData(String message) {
        try {
            if (!message.equalsIgnoreCase("") || !message.equals(null)) {
                StringTokenizer tok = new StringTokenizer(message, MChatMeta.DS, false);
                if (tok.nextToken().equals(MChatMeta.CONTACT_LIST)) {
                    StringTokenizer contacts = null;
                    MChat.getSharedInstance().contactObjects.clear();
                    if (tok.hasMoreElements()) {
                        try {
                            try {
                                contacts = new StringTokenizer(tok.nextToken(), "|", false);
                            } catch (Exception e) {
                            }
                            while (contacts.hasMoreElements()) {
                                try {
                                    StringTokenizer contactInfo = new StringTokenizer(contacts.nextToken(), ",", false);
                                    for (int i = 0; contactInfo.hasMoreTokens(); i++) {
                                        try {
                                            MChatContactObject MChatContact = new MChatContactObject();
                                            MChatContact.userID = contactInfo.nextToken();
                                            if (MChatContact.userID.equalsIgnoreCase("null")) {
                                                continue;
                                            }
                                            MChatContact.nickName = contactInfo.nextToken();
                                            MChatContact.status = contactInfo.nextToken();
                                            MChatContact.fName = contactInfo.nextToken();
                                            MChatContact.lName = contactInfo.nextToken();
                                            try {
                                                MChatContact.personalMessage = contactInfo.nextToken();
                                            } catch (Exception e) {
//                                                e.printStackTrace();
                                            }
                                            MChat.getSharedInstance().contactObjects.put(MChatContact.userID, MChatContact);
                                        } catch (Exception e) {
                                        }
                                    }
                                } catch (Exception e) {
                                }
                            }
                            MChat.contactPanel.removeAll();
                            MChat.getSharedInstance().processContactList();
                            MChat.getSharedInstance().populateContacts();
                            MChat.contactPanel.repaint();
                            MChat.getSharedInstance().setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        MChat.contactPanel.removeAll();
                        MChat.contactPanel.repaint();
                        MChat.getSharedInstance().setVisible(true);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addWindow(MChatChatWindow cw) {
        try {
            observers.add(cw);
        } catch (Exception e) {
        }
        try {
            MChatDataStore.addChatWindow(cw.user, cw);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeObserver(MChatChatWindow cw) {
        try {
            observers.remove(cw);
        } catch (Exception e) {
        }
        try {
//            MChatDataStore.removeChatWindow(cw.user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void messageAlert(String ms, String sd) {
        try {
            MChatPopNudgeAlert pa = MChatPopNudgeAlert.getSharedInstance(ms, sd);
            pa.setUndecorated(true);
            pa.setSize(240, 120);
            pa.setVisible(true);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            pa.setLocation(dim.width - 240, dim.height - 120);
        } catch (HeadlessException e) {
        }
    }

    public void addtoConvMsgHash(String convID, MChatQueue que) {
        try {
            conversationMessages.put(convID, que);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap getConvMsgHash() {
        return conversationMessages;
    }

    public void addToConvHash(String convID, MChatChatWindow chaWin) {
        try {
            conversationWindows.put(convID, chaWin);
        } catch (Exception e) {
        }
    }

    public HashMap getConvHash() {
        return conversationWindows;
    }

    public void destroySelf() {
        self = null;
        observers = null;
        chatMessages = null;
        commandMessages = null;
        conversationMessages = null;
        conversationWindows = null;
        pa = null;
        timeMap = null;
    }
}
