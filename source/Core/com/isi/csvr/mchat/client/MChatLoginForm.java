package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MChatLoginForm extends JPanel implements Disposable {
    public static JLabel loadingLabel = null;
    public static JLabel autoSignLabel = null;
    private static MChatLoginForm self = null;
    private static boolean isSelected = true;
    private JButton signInBtn = null;
    private JTextField userNameTxtField = null;
    private JTextField nickNameTxtField = null;
    private JCheckBox rmbrCBox = null;
    private JTextArea errorTxtArea = null;
    private JPanel errorPanel = null;
    private ImageIcon loadingImage = null;
    private JPanel loadingPanel = null;

    public MChatLoginForm() {
        try {
            setLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "100", "50", "70", "30", "50", "100%"}, 20, 10)));
            setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            JLabel loginIDLbl = new JLabel(MChatLanguage.getString("LOGIN_ID"));
            if (MChatLanguage.isLTR) {
                loginIDLbl.setFont(MChatMeta.defaultFont);
            }
            loginIDLbl.setVerticalTextPosition(SwingConstants.BOTTOM);
            loginIDLbl.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
            JLabel emailLbl = new JLabel(MChatLanguage.getString("EMAIL"));

            JPanel loginPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "20", "30", "20"}, 0, 0));
            loginPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            userNameTxtField = new JTextField("");
            nickNameTxtField = new JTextField("");
            nickNameTxtField.setText(MChatSettings.NICK_NAME);
            nickNameTxtField.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            nickNameTxtField.setBackground(Color.white);
            if (MChatLanguage.isLTR) {
                nickNameTxtField.setFont(MChatMeta.defaultFont);
            }
//            nickNameTxtField.setEditable(false);

            nickNameTxtField.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 10) {
                        signInBtnActionPerformed();
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
//            loginPanel.add(emailLbl);
//            loginPanel.add(userNameTxtField);
            loginPanel.add(new JLabel());
            loginPanel.add(new JLabel());
//            loginPanel.add(nickNameTxtField);

            JPanel buttonPanel = new JPanel();
            buttonPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            buttonPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5));

            signInBtn = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/sign-in-but.jpg"));
            signInBtn.setContentAreaFilled(false);
            signInBtn.setBorder(BorderFactory.createEmptyBorder());
            signInBtn.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/sign-in-but-up.jpg"));
            signInBtn.setFocusPainted(false);
            signInBtn.setToolTipText(MChatLanguage.getString("SIGN_IN"));

            signInBtn.addActionListener
                    (new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            signInBtnActionPerformed();
                        }
                    });
//            //signInBtn.setRolloverIcon(new ImageIcon());

//            buttonPanel.add(new JLabel());
            buttonPanel.add(new JLabel());
            buttonPanel.setBorder(BorderFactory.createEmptyBorder());
//            buttonPanel.add(new JLabel());

            rmbrCBox = new JCheckBox(MChatLanguage.getString("REMEMBER_ME"), false);
            rmbrCBox.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            rmbrCBox.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
            rmbrCBox.setEnabled(true);
            rmbrCBox.setSelected(false);

            rmbrCBox.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 10) {
                        rmbrCBox.setSelected(true);
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });
            rmbrCBox.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    if (rmbrCBox.isSelected()) {
                        try {
                            MChatSettings.setProperty("REMEMBER_ME", "0");
                            MChatSettings.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
                            MChatSettings.NICK_NAME = nickNameTxtField.getText();
//                            MChatSettings.setProperty("MUBASHER_ID", MChatSettings.MUBASHER_ID);
//                            MChatSettings.setProperty("NICK_NAME", MChatSettings.NICK_NAME);
                        } catch (Exception e1) {
                        }
                    } else {
                        MChatSettings.setProperty("REMEMBER_ME", "0");
                        MChatSettings.MUBASHER_ID = "";
                        MChatSettings.NICK_NAME = "";
//                        MChatSettings.setProperty("MUBASHER_ID", "");
//                        MChatSettings.setProperty("NICK_NAME", "");
                    }
                    MChatSettings.save();
                }
            });

            autoSignLabel = new JLabel(MChatLanguage.getString("AUTO_SIGN_IN"));
            if (MChatLanguage.isLTR) {
                autoSignLabel.setFont(MChatMeta.defaultFont);
            }
            autoSignLabel.setForeground(Color.white);
            final ImageIcon selected = new ImageIcon(MChatSettings.IMAGE_PATH + "/tick-icon.jpg");
            final ImageIcon notSelected = new ImageIcon(MChatSettings.IMAGE_PATH + "/un-tick-icon.jpg");
            if (MChatSettings.isAutoSign) {
                autoSignLabel.setIcon(selected);
            } else {
                autoSignLabel.setIcon(notSelected);
            }

            autoSignLabel.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    if (isSelected) {
                        autoSignLabel.setIcon(notSelected);
                        MChatSettings.setProperty("AUTO_SIGN", "0");
                        MChatSettings.MUBASHER_ID = "";
                        MChatSettings.NICK_NAME = "";
//                        MChatSettings.setProperty("MUBASHER_ID", "");
//                        MChatSettings.setProperty("NICK_NAME", "");
                        isSelected = false;
                        MChatSettings.isAutoSign = false;
                        MChatSettings.save();
                    } else {
                        autoSignLabel.setIcon(selected);
                        MChatSettings.setProperty("AUTO_SIGN", "1");
                        MChatSettings.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
                        MChatSettings.NICK_NAME = nickNameTxtField.getText();
//                        MChatSettings.setProperty("MUBASHER_ID", MChatSettings.MUBASHER_ID);
//                        MChatSettings.setProperty("NICK_NAME", MChatSettings.NICK_NAME);
                        MChatSettings.isAutoSign = true;
                        isSelected = true;
                        MChatSettings.save();
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }
            });

            if (MChatSettings.isRememberME) {
                userNameTxtField.setText(MChatSettings.MUBASHER_ID);
                nickNameTxtField.setText(MChatSettings.NICK_NAME);
                rmbrCBox.setSelected(true);
            }

            autoSignLabel.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {
                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == 10) {
                        autoSignLabel.setIcon(selected);
                        isSelected = true;
                        MChatSettings.isAutoSign = true;
                    }
                }

                public void keyReleased(KeyEvent e) {
                }
            });

            try {
                errorPanel = new JPanel();
                errorPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
                errorPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

                errorTxtArea = new JTextArea("");
                if (MChatLanguage.isLTR) {
                    errorTxtArea.setFont(MChatMeta.defaultFont);
                }
                errorTxtArea.setLineWrap(true);
                errorTxtArea.setEditable(false);
                errorTxtArea.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

                loadingImage = new ImageIcon(MChatSettings.IMAGE_PATH + "/loader.gif");
                loadingLabel = new JLabel(loadingImage);
                loadingLabel.setForeground(Theme.getColor("MCHAT_ERROR_FONT_COLOR"));
                if (MChatLanguage.isLTR) {
                    loadingLabel.setFont(MChatMeta.defaultFont);
                }
                loadingLabel.setVisible(false);

                loadingPanel = new JPanel();
                loadingPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
                loadingPanel.add(loadingLabel);

//                loadingPanel.setVisible(false);
//                errorPanel.add(loadingPanel);
                errorPanel.add(errorTxtArea);
            } catch (Exception e) {
                e.printStackTrace();
            }


            JPanel newUserPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%%"}, new String[]{"100%"}, 0, 0));
            newUserPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            String text = "<HTML><u>" + MChatLanguage.getString("NEW_USER") + "</u>";
            final JLabel newUserLbl = new JLabel(text);
            if (MChatLanguage.isLTR) {
                newUserLbl.setFont(MChatMeta.defaultFont);
            }
            newUserLbl.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
            newUserLbl.setVerticalTextPosition(JLabel.TOP);
            newUserLbl.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    errorTxtArea.setText("");
                    try {
                        MChat.getSharedInstance().cardLayout.show(MChat.mainPanel, "RegistrationForm");
                    } catch (Exception e1) {
                    }
//                    createNewUserPanel();
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    newUserLbl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                }

                public void mouseExited(MouseEvent e) {
                }
            });
            newUserPanel.add(newUserLbl);
            newUserPanel.add(new JLabel());

            final JComboBox lang = new JComboBox();
            lang.addItem("English");
            lang.addItem("Arabic");
            lang.addItemListener(new ItemListener() {
                public void itemStateChanged(ItemEvent e) {
                    if (lang.getSelectedIndex() == 0) {
//                        System.out.println("english");
                        MChatMeta.isLTR = true;
                    } else if (lang.getSelectedItem().equals("Arabic")) {
//                        System.out.println("arabic");
                        MChatMeta.isLTR = false;
                    }
                    MChatLanguage.getsharedInstance().load();
                    //                GUISettings.applyOrientation(this);
                }
            });
            lang.setEnabled(false);
            loginPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            buttonPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            loadingPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            errorPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            add(new JLabel());
            add(loginPanel);
            add(buttonPanel);
            add(loadingPanel);
            add(new JLabel());
            add(errorPanel);
//            mainPanel.add(rmbrCBox);
//        mainPanel.add(new JLabel());
//            mainPanel.add(lang);
//            add(new JLabel(""));
            add(new JLabel());
            GUISettings.applyOrientation(this);
            if (MChatSettings.isAutoSign) {
                signInBtnActionPerformed();
            }
            MChatDataStore.getSharedInstance().addDisposableListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MChatLoginForm getSharedInstance() {
        if (self == null) {
            self = new MChatLoginForm();
        }
        return self;
    }

    private void saveUser() {
        try {
//        if (isSelected) {
//            Settings.setProperty("AUTO_SIGN", "1");
            MChatSettings.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
            MChatSettings.NICK_NAME = nickNameTxtField.getText();
//            MChatSettings.setProperty("MUBASHER_ID", MChatSettings.MUBASHER_ID);
//            MChatSettings.setProperty("NICK_NAME", MChatSettings.NICK_NAME);
            MChatSettings.isAutoSign = true;
//        } else {
//            Settings.setProperty("AUTO_SIGN", "0");
//            Settings.isAutoSign = false;
//        }
            MChatSettings.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void signInBtnActionPerformed() {
        try {
            loadingLabel.setVisible(true);
            if (com.isi.csvr.shared.Settings.isConnected()) {
                errorTxtArea.setText("");
                if (com.isi.csvr.shared.Settings.getUserID().length() > 0) {
                    MChatMeta.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
                    MChatMeta.DISPLAY_ID = nickNameTxtField.getText();
                    try {
                        if (!MChatClient.getSharedInstance().isActive()) {
                            MChatClient.getSharedInstance().start();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.loginUserRequest(MChatMeta.MUBASHER_ID, nickNameTxtField.getText(), MChatSettings.APP_MODE));
                    MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.loginUserRequest(MChatMeta.MUBASHER_ID, "", MChatSettings.APP_MODE));
                    try {
                        saveUser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    giveMsg(MChatLanguage.getString("USER_ID_EMPTY"), 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void giveMsg(String errorMsg, int msgType) {
        try {
            if (msgType == 1) {
                errorTxtArea.setForeground(Theme.getColor("MCHAT_ERROR_FONT_COLOR"));
            } else {
                errorTxtArea.setForeground(Color.BLACK);
            }
            errorTxtArea.setText(errorMsg);
        } catch (Exception e) {
        }
    }

    public void destroySelf() {
        self = null;
        loadingLabel = null;
        autoSignLabel = null;
    }

    class CheckBoxIcon implements Icon {
        public void paintIcon(Component component, Graphics g, int x, int y) {
            ImageIcon selected = new ImageIcon(MChatSettings.IMAGE_PATH + "/tick-icon.jpg");
            ImageIcon notSelected = new ImageIcon(MChatSettings.IMAGE_PATH + "/un-tick-icon.jpg");

            try {
                AbstractButton abstractButton = (AbstractButton) component;
                ButtonModel buttonModel = abstractButton.getModel();
                if (buttonModel.isSelected()) {
                    MChatSettings.setProperty("REMEMBER_ME", "1");
                    MChatSettings.MUBASHER_ID = com.isi.csvr.shared.Settings.getUserID();
                    MChatSettings.NICK_NAME = nickNameTxtField.getText();
//                    MChatSettings.setProperty("MUBASHER_ID", MChatSettings.MUBASHER_ID);
//                    MChatSettings.setProperty("NICK_NAME", MChatSettings.NICK_NAME);
                    if (MChatLanguage.isLTR) {
                        selected.paintIcon(null, g, 0, 1);
                    } else {
                        selected.paintIcon(null, g, 0, 1);
                    }
                } else {
                    notSelected.paintIcon(null, g, 0, 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public int getIconWidth() {
            return 10;
        }

        public int getIconHeight() {
            return 10;
        }
    }
}
