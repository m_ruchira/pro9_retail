package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MChatRegistrationForm extends JPanel implements Disposable {
    private static MChatRegistrationForm self = null;
    public JTextField newLoginNameTF = null;
    public JTextField newMubIDTF = null;
    public JTextField firstNameTF = null;
    public JTextField lastNameTF = null;
    public JTextArea errorTxtArea = null;
    private String nickName = null;

    public MChatRegistrationForm() {
        setLayout(new FlexGridLayout(new String[]{"70", "145"}, new String[]{"50", "20", "20", "20", "30", "50", "50", "35", "100%"}, 10, 15));
        setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        final JButton registerBtn = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/Register-but.jpg"));
        registerBtn.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/Register-but-up.jpg"));
        registerBtn.setFocusPainted(false);

        errorTxtArea = new JTextArea();
        errorTxtArea.setEditable(false);
        errorTxtArea.setLineWrap(true);
        errorTxtArea.setWrapStyleWord(true);

        errorTxtArea.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

        newLoginNameTF = new JTextField("");
        newLoginNameTF.setBackground(Color.white);
        newLoginNameTF.setForeground(Color.black);

        newLoginNameTF.setBorder(BorderFactory.createEmptyBorder());
        newLoginNameTF.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    registerBtn.setFocusPainted(true);
                    regBtnActionPerformed();
                }
            }

            public void keyReleased(KeyEvent e) {
            }

        });

        newMubIDTF = new JTextField(16);
        newMubIDTF.setBorder(BorderFactory.createEmptyBorder());
        newMubIDTF.setBackground(Color.white);
        newMubIDTF.setForeground(Color.black);

        firstNameTF = new JTextField("");
        firstNameTF.setBorder(BorderFactory.createEmptyBorder());
        firstNameTF.setBackground(Color.white);
        firstNameTF.setForeground(Color.black);

        firstNameTF.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    registerBtn.setFocusPainted(true);
                    regBtnActionPerformed();
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        });

        lastNameTF = new JTextField("");
        lastNameTF.setBorder(BorderFactory.createEmptyBorder());
        lastNameTF.setBackground(Color.white);
        lastNameTF.setForeground(Color.black);

        lastNameTF.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
            }

            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == 10) {
                    if (Language.getSelectedLanguage().equals("EN")) {
                        registerBtn.setFocusPainted(true);
                        regBtnActionPerformed();
                    }
                }
            }

            public void keyReleased(KeyEvent e) {
            }
        });

        JPanel btnPanel = new JPanel(new FlexGridLayout(new String[]{"35", "75", "30"}, new String[]{"100%"}, 0, 0));
        btnPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        registerBtn.setContentAreaFilled(false);
        registerBtn.setBorder(BorderFactory.createEmptyBorder());
//        registerBtn.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        registerBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                regBtnActionPerformed();
            }
        });
        btnPanel.add(new JLabel());
        btnPanel.add(registerBtn);
        btnPanel.add(new JLabel());

        JPanel backPanel = new JPanel(new FlexGridLayout(new String[]{"40", "100%"}, new String[]{"100%"}, 0, 0));
        backPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

        final JLabel backLbl = new JLabel("<HTML><u>" + MChatLanguage.getString("BACK") + "</u>");
        backLbl.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
        if (MChatLanguage.isLTR) {
            backLbl.setFont(MChatMeta.defaultFont);
        }
        backLbl.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                errorTxtArea.setText("");
                try {
                    MChat.getSharedInstance().cardLayout.show(MChat.mainPanel, "LoginPanel");
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
                backLbl.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        backPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
        backPanel.add(new JLabel());
        backPanel.add(new JLabel());

        add(new JLabel());
        add(new JLabel());
//        add(new JLabel(Language.getString("EMAIL") + "*"));
//        add(newMubIDTF);
        JLabel loginId = new JLabel(MChatLanguage.getString("LOGIN_ID") + "*");
        loginId.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
        if (MChatLanguage.isLTR) {
            loginId.setFont(MChatMeta.defaultFont);
            newLoginNameTF.setFont(MChatMeta.defaultFont);
        }

        JLabel fName = new JLabel(MChatLanguage.getString("FNAME") + "*");
        fName.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
        if (MChatLanguage.isLTR) {
            fName.setFont(MChatMeta.defaultFont);
            firstNameTF.setFont(MChatMeta.defaultFont);
        }

        JLabel lName = new JLabel(MChatLanguage.getString("LNAME") + "*");
        lName.setForeground(Theme.getColor("MCHAT_FONT_COLOR"));
        if (MChatLanguage.isLTR) {
            lName.setFont(MChatMeta.defaultFont);
            lastNameTF.setFont(MChatMeta.defaultFont);
        }

        add(loginId);
        add(newLoginNameTF);

        add(fName);
        add(firstNameTF);

        add(lName);
        add(lastNameTF);

        add(new JLabel());
        add(btnPanel);
        add(new JLabel());
        add(errorTxtArea);
        add(new JLabel());
        add(new JLabel());
        add(backPanel);
        add(new JLabel());
        repaint();
        setVisible(true);
//        Settings.isFirstTime = true;
        GUISettings.applyOrientation(this);
        MChatDataStore.getSharedInstance().addDisposableListener(this);
    }

    public static MChatRegistrationForm getSharedInstance() {
        if (self == null) {
            self = new MChatRegistrationForm();
        }
        return self;
    }

    public String getNickName() {
        return nickName;
    }

    private void regBtnActionPerformed() {
        errorTxtArea.setText("");
        MChatMeta.MUBASHER_ID = newMubIDTF.getText().trim();
        MChatMeta.DISPLAY_ID = newLoginNameTF.getText().trim();
        MChatMeta.FIRST_NAME = firstNameTF.getText().trim();
        MChatMeta.LAST_NAME = lastNameTF.getText().trim();

        if ((newLoginNameTF.getText().trim().equalsIgnoreCase(""))) {
            giveMsg(MChatLanguage.getString("LOGIN_ID") + " " + MChatLanguage.getString("FIELDS_EMPTY"), 1);
        } else if (newLoginNameTF.getText().trim().length() > 16) {
            giveMsg(MChatLanguage.getString("LOGIN_ID") + " " + MChatLanguage.getString("FIELDS_LENGTH"), 1);
        } else if (firstNameTF.getText().trim().equalsIgnoreCase("")) {
            giveMsg(MChatLanguage.getString("FNAME") + " " + MChatLanguage.getString("FIELDS_EMPTY"), 1);
        } else if (firstNameTF.getText().trim().length() > 16) {
            giveMsg(MChatLanguage.getString("FNAME") + " " + MChatLanguage.getString("FIELDS_LENGTH"), 1);
        } else if (lastNameTF.getText().trim().equalsIgnoreCase("")) {
            giveMsg(MChatLanguage.getString("LNAME") + " " + MChatLanguage.getString("FIELDS_EMPTY"), 1);
        } else if (lastNameTF.getText().trim().length() > 16) {
            giveMsg(MChatLanguage.getString("LNAME") + " " + MChatLanguage.getString("FIELDS_LENGTH"), 1);
        } else {
            try {
                if (!MChatClient.getSharedInstance().isAlive()) {
                    MChatClient.getSharedInstance().start();
                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            nickName = MChatMeta.DISPLAY_ID;
            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createRegisterRequest());
        }
    }

    public synchronized void giveMsg(String errorMsg, int msgType) {
        try {
            if (msgType == 1) {
                errorTxtArea.setForeground(Theme.getColor("MCHAT_ERROR_FONT_COLOR"));
            } else {
                errorTxtArea.setForeground(Color.BLACK);
            }
            errorTxtArea.setText(errorMsg);
        } catch (Exception e) {

        }
    }

    public void destroySelf() {
        self = null;
    }
}
