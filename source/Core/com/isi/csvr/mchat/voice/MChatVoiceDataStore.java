package com.isi.csvr.mchat.voice;

import java.util.Hashtable;

public class MChatVoiceDataStore {
    private static Hashtable<String, MChatVoiceServerSender> fileSenderRequestTable = null;
    private static Hashtable<String, MChatVoiceServerReader> fileReaderRequestTable = null;
    private static MChatVoiceDataStore self = null;

    public MChatVoiceDataStore() {
        fileSenderRequestTable = new Hashtable<String, MChatVoiceServerSender>();
        fileReaderRequestTable = new Hashtable<String, MChatVoiceServerReader>();
    }

    public static MChatVoiceDataStore getSharedInstance() {
        if (self == null) {
            self = new MChatVoiceDataStore();
        }
        return self;
    }

    public void addToSenderTable(String id, MChatVoiceServerSender sender) {
        try {
            fileSenderRequestTable.put(id, sender);
        } catch (Exception e) {
        }
    }

    public void removeFromSender(String id) {
        try {
            fileSenderRequestTable.remove(id);
        } catch (Exception e) {
        }
    }

    public MChatVoiceServerSender getSender(String id) {
        return fileSenderRequestTable.get(id);
    }

    public void addToReaderTable(String id, MChatVoiceServerReader readerMChat) {
        try {
            fileReaderRequestTable.put(id, readerMChat);
        } catch (Exception e) {
        }
    }

    public void removeFromReaderTable(String id) {
        try {
            fileReaderRequestTable.remove(id);
        } catch (Exception e) {
        }
    }

    public MChatVoiceServerReader getReader(String id) {
        return fileReaderRequestTable.get(id);
    }

    public void destroySelf() {
        fileSenderRequestTable = null;
        fileReaderRequestTable = null;
        self = null;
    }

}
