package com.isi.csvr.mchat.voice;

import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.mchat.shared.MChatSharedMethods;

import javax.sound.sampled.*;


public class MChatVoiceReader extends Thread {
    private MChatVoiceServerReader serverReader = null;
    private AudioFormat audioFormat;
    private SourceDataLine speakerDataLine;

    public MChatVoiceReader(MChatVoiceServerReader serverReader) {
        try {
            this.serverReader = serverReader;
            audioFormat = MChatSharedMethods.getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, audioFormat);
            speakerDataLine = (SourceDataLine)
                    AudioSystem.getLine(dataLineInfo);
            speakerDataLine.open(audioFormat);
            speakerDataLine.start();
            this.setPriority(Thread.NORM_PRIORITY + 3);
            this.start();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            if (serverReader.isConnected()) {
                read();
                sleepThread(10);
            } else {
                sleepThread(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            sleepThread(5000);
        }
    }

    private void sleepThread(int iTime) {
        try {
            Thread.sleep(iTime);
        } catch (Exception e) {
        }
    }

    private void read() {
        try {
            byte tempBuffer[] = new byte[MChatSettings.VOICE_BLOCK_SIZE];
            while (serverReader.in.read(tempBuffer) != -1) {
                speakerDataLine.write(tempBuffer, 0, MChatSettings.VOICE_BLOCK_SIZE);
            }
        } catch (Exception e) {
        }
    }
}
