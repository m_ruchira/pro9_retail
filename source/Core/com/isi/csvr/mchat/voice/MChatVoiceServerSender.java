package com.isi.csvr.mchat.voice;

import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.mchat.shared.MChatSharedMethods;
import com.isi.csvr.mchat.shared.MChatUniqueID;

import javax.sound.sampled.*;
import java.io.*;
import java.net.Socket;

public class MChatVoiceServerSender {
    public boolean isConnected = false;
    protected Socket cSocket = null;
    protected OutputStream out = null;
    protected InputStream bufIn = null;
    AudioFormat audioFormat;
    TargetDataLine micDataLine;
    private String serverIP = null;
    private int serverPort = 0;
    private MChatCallGUIPanel callGUIPanel = null;


    public MChatVoiceServerSender(String serverIP, int serverPort, MChatCallGUIPanel callGUIPanel) {
        this.serverIP = serverIP;
        this.serverPort = serverPort;
        this.callGUIPanel = callGUIPanel;
    }

    public MChatCallGUIPanel getCallGUIPanel() {
        return callGUIPanel;
    }

    public boolean isConnected() {
        return isConnected;
    }

    private synchronized void write(byte[] b, int off, int len) {
        try {
            if (isConnected && !cSocket.isClosed() && out != null) {
                out.write(b, off, len);
                out.flush();
            }
        } catch (Exception e) {
            try {
                close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    public boolean connect() {
        try {
            cSocket = createSocket(serverIP, serverPort);
            bufIn = new BufferedInputStream(cSocket.getInputStream());
            out = new BufferedOutputStream(cSocket.getOutputStream());
            isConnected = true;
        } catch (Exception e) {
            isConnected = false;
            e.printStackTrace();
        }
        return isConnected;
    }

    public Socket createSocket(String ip, int port) throws Exception {
        Socket socket;
        try {
            socket = new Socket(ip, port);
        } catch (Exception e) {
            return null;
        }
        return socket;
    }

    public void flush() {
        try {
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void close() {
        if (bufIn != null) {
            try {
                bufIn.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bufIn = null;
        if (out != null) {
            try {
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        out = null;
        cSocket = null;
    }

    public void sendCallRequest(String desUser) {
        try {
            byte[] buf = new byte[MChatMeta.VOICE_LEN];
            byte[] temp;
            String messageType = MChatMeta.VOICE_CALL_REQUEST;
            String source = MChatMeta.MUBASHER_ID;
            String destination = desUser;
            String requestID = "" + MChatUniqueID.get();

            try {
                MChatVoiceDataStore.getSharedInstance().addToSenderTable(requestID, this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());
            temp = null;

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());
            temp = null;

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());
            temp = null;

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());
            temp = null;

            write(buf, 0, 52);  //write file headers
            MChatVoiceDataStore.getSharedInstance().addToSenderTable(requestID, this);
            //wait untill file accepted or rejest request comes
            try {
                synchronized (this) {
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            captureAudio(requestID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void captureAudio(String requestID) {
        try {
            Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();
            System.out.println("Available mixers:");
            for (int cnt = 0; cnt < mixerInfo.length; cnt++) {
                System.out.println(mixerInfo[cnt].getName());
            }
            audioFormat = MChatSharedMethods.getAudioFormat();
            DataLine.Info dataLineInfo = new DataLine.Info(
                    TargetDataLine.class, audioFormat);
            Mixer mixer = AudioSystem.getMixer(mixerInfo[3]);

            micDataLine = (TargetDataLine) mixer.getLine(dataLineInfo);
            micDataLine.open(audioFormat);
            micDataLine.start();

            Thread captureThread = new CaptureThread(requestID);
            captureThread.start();
        } catch (Exception e) {
            System.out.println(e);
//            System.exit(0);
        }
    }

    class CaptureThread extends Thread {
        String requestID = null;
        byte tempBuffer[] = new byte[MChatSettings.VOICE_BLOCK_SIZE];

        CaptureThread(String requestID) {
            this.requestID = requestID;
        }

        public void run() {
            try {
                while (true) {
                    int cnt = micDataLine.read(tempBuffer, 0,
                            tempBuffer.length);
                    if (cnt > 0)
                        out.write(tempBuffer);
                }
            } catch (Exception e) {
                close();
                MChatVoiceDataStore.getSharedInstance().removeFromSender(requestID);
                System.out.println(e);
//                System.exit(0);
            }
        }
    }
}
