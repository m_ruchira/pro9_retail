package com.isi.csvr.mchat.voice;

import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatCallAcceptGUIPanel extends JPanel implements ActionListener, Runnable {

    private JPanel iconPanel = null;
    private JPanel fileButtonpanel = null;
    private JTextPane displayArea = null;
    private JButton okButton = null;
    private JButton noButton = null;
    private String source = null;
    private String reqID = null;
    private String nickName = null;
    private JLabel callinglabel = null;
    private Icon callingIcon = null;
    private Icon inCallingIcon = null;
    private boolean ringing = true;
    private Thread ringSoundThread = null;

    public MChatCallAcceptGUIPanel(String fileSource, String reqID, String nickName) {
        try {
            this.source = fileSource;
            this.reqID = reqID;
            this.nickName = nickName;

            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "30"}, 1, 1));
            setOpaque(false);

            callingIcon = new ImageIcon("images/mchat/mchat_en/Hangup.gif");
            inCallingIcon = new ImageIcon("images/mchat/mchat_en/incall.gif");
            iconPanel = new JPanel();
            iconPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 1, 1));
            iconPanel.setOpaque(false);

            callinglabel = new JLabel(nickName + " " + MChatLanguage.getString("CALLING_1"));
            callinglabel.setIcon(callingIcon);
            if (MChatLanguage.isLTR) {
                callinglabel.setFont(MChatMeta.defaultFont);
            }
            iconPanel.add(callinglabel);

            okButton = new JButton("<HTML><u>" + MChatLanguage.getString("ANSWER") + "</u><HTML>");
            okButton.setContentAreaFilled(false);
            okButton.setBorder(BorderFactory.createEmptyBorder());
            okButton.addActionListener(this);
            okButton.setForeground(Color.blue);
            okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                okButton.setFont(MChatMeta.defaultFont);
            }

            noButton = new JButton("<HTML><u>" + MChatLanguage.getString("DECLINE") + "</u><HTML>");
            noButton.addActionListener(this);
            noButton.setBorder(BorderFactory.createEmptyBorder());
            noButton.setContentAreaFilled(false);
            noButton.setForeground(Color.blue);
            noButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                noButton.setFont(MChatMeta.defaultFont);
            }

            fileButtonpanel = new JPanel();
            fileButtonpanel.setLayout(new FlexGridLayout(new String[]{"60", "60"}, new String[]{"100%"}, 1, 1));
            fileButtonpanel.setOpaque(false);
            fileButtonpanel.add(okButton);
            fileButtonpanel.add(noButton);
            add(iconPanel);
            add(fileButtonpanel);
            GUISettings.applyOrientation(this);
//            ring();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ring() {
        ringSoundThread = new Thread(this, "ringVoiceCall");
        ringSoundThread.start();
    }

    public JTextPane getDisplayArea() {
        return displayArea;
    }

    public void setDisplayArea(JTextPane displayArea) {
        this.displayArea = displayArea;
    }

    public void actionPerformed(ActionEvent e) {
        final MChatVoiceServerReader voiceSender = new MChatVoiceServerReader(MChatSettings.voiceServerIP, MChatSettings.voiceServerPort);
        if (e.getSource() == okButton) {
            try {
                ringing = false;
                okButton.setVisible(false);
                callinglabel.setIcon(inCallingIcon);
                if (voiceSender.connect()) {
                    new Thread() {
                        public void run() {
                            voiceSender.sendVoiceAcceptRequest(reqID, source);
                        }
                    }.start();
                }
            } catch (Exception e1) {
            }
        } else if (e.getSource() == noButton) {
            try {
//                ringSoundThread.stop();
            } catch (Exception e1) {
            }
            ringing = false;
            callinglabel.setText(MChatLanguage.getString("CALL_REJECT_1") + " " + nickName + " " + MChatLanguage.getString("CALL_REJECT_2"));
            callinglabel.setForeground(Color.red);
            callinglabel.setIcon(null);
            if (voiceSender.connect()) {
                new Thread() {
                    public void run() {
                        voiceSender.sendVoiceRejectRequest(reqID, source);
                    }
                }.start();
            }
            fileButtonpanel.setVisible(false);
        }
    }


    public void run() {
        while (ringing) {
            try {
                NativeMethods.play(System.getProperties().get("user.dir") + "/sounds/phone_ringing.wav", 1);
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }
        }
    }
}
