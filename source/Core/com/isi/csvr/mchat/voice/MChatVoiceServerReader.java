package com.isi.csvr.mchat.voice;

import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;

import java.io.*;
import java.net.Socket;

public class MChatVoiceServerReader {
    public boolean isConnected = false;
    protected Socket cSocket = null;
    protected OutputStream out = null;
    protected InputStream in = null;
    private String serverIP = null;
    private int serverPort = 0;

    public MChatVoiceServerReader(String serverIP, int serverPort) {
        this.serverIP = serverIP;
        this.serverPort = serverPort;
    }

    public void sendVoiceAcceptRequest(String reqID, String src) {
        try {
            byte[] temp;
            byte[] buf = new byte[MChatMeta.VOICE_LEN];
            String messageType = MChatMeta.VOICE_CALL_ACCEPT_REQUEST;
            String source = src;
            String destination = MChatSettings.MUBASHER_ID;
            String requestID = "" + reqID;

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());

            write(buf, 0, MChatMeta.VOICE_LEN);
            flush();

            new MChatVoiceReader(this);
        } catch (Exception e) {
        }
    }

    private void write(byte[] b, int off, int len) {
        try {
            if (out != null) {
                out.write(b, off, len);
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean connect() {
        try {
            cSocket = createSocket(serverIP, serverPort);
            in = new BufferedInputStream(cSocket.getInputStream());
            out = new BufferedOutputStream(cSocket.getOutputStream());
            isConnected = true;
        } catch (Exception e) {
            isConnected = false;
            e.printStackTrace();
        }
        return isConnected;
    }

    public Socket createSocket(String ip, int port) throws Exception {
        Socket socket;
        try {
            socket = new Socket(ip, port);
        } catch (Exception e) {
            return null;
        }
        return socket;
    }

    public void flush() {
        try {
            out.flush();
        } catch (Exception e) {
            close();
        }
    }

    public void close() {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        in = null;
        if (out != null) {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        out = null;
        if (cSocket != null) {
            try {
                cSocket.close();
            } catch (Exception e) {
            }
        }
        cSocket = null;
    }

    public void sendVoiceRejectRequest(String reqID, String src) {
        try {
            byte[] temp;
            byte[] buf = new byte[MChatMeta.BYTE_LEN];
            String messageType = MChatMeta.VOICE_CALL_REJECT_REQUEST;
            String source = src;
            String destination = MChatSettings.MUBASHER_ID;
            String requestID = "" + reqID;

            temp = messageType.getBytes();
            System.arraycopy(temp, 0, buf, 0, messageType.length());

            temp = requestID.getBytes();
            System.arraycopy(temp, 0, buf, 4, requestID.length());

            temp = source.getBytes();
            System.arraycopy(temp, 0, buf, 20, source.length());

            temp = destination.getBytes();
            System.arraycopy(temp, 0, buf, 36, destination.length());

            write(buf, 0, 36);
            close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return isConnected;
    }
}
