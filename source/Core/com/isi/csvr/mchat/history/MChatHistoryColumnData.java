package com.isi.csvr.mchat.history;

import com.isi.csvr.mchat.shared.MChatLanguage;

import java.util.Vector;

public class MChatHistoryColumnData {
    public static int colCountMax = 5;
    public static Vector colData = new Vector();

    public Vector getColInfo() {
        for (int colCount = 0; colCountMax > colCount; colCount++) {
            if (colCount == 0) {
                MChatHistoryTableSettings col = new MChatHistoryTableSettings();
                col.setColName(MChatLanguage.getString("HISTORY_DATE"));
                colData.addElement(col);
            } else if (colCount == 1) {
                MChatHistoryTableSettings col = new MChatHistoryTableSettings();
                col.setColName(MChatLanguage.getString("HISTORY_TIME"));
                colData.addElement(col);
            } else if (colCount == 2) {
                MChatHistoryTableSettings col = new MChatHistoryTableSettings();
                col.setColName(MChatLanguage.getString("HISTORY_FROM"));
                colData.addElement(col);
            } else if (colCount == 3) {
                MChatHistoryTableSettings col = new MChatHistoryTableSettings();
                col.setColName(MChatLanguage.getString("HISTORY_TO"));
                colData.addElement(col);
            } else if (colCount == 4) {
                MChatHistoryTableSettings col = new MChatHistoryTableSettings();
                col.setColName(MChatLanguage.getString("HISTORY_MESSAGE"));
                colData.addElement(col);
            }
        }
        return colData;
    }
}
