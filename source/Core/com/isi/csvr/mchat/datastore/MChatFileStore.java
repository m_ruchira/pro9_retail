package com.isi.csvr.mchat.datastore;

import com.isi.csvr.mchat.file.MChatFileServerReader;
import com.isi.csvr.mchat.file.MChatFileServerSender;
import com.isi.csvr.mchat.shared.Disposable;

import java.util.Hashtable;

public class MChatFileStore implements Disposable {
    private static Hashtable<String, MChatFileServerSender> fileSenderRequestTable = null;
    private static Hashtable<String, MChatFileServerReader> fileReaderRequestTable = null;
    private static MChatFileStore self = null;

    public MChatFileStore() {
        fileSenderRequestTable = new Hashtable<String, MChatFileServerSender>();
        fileReaderRequestTable = new Hashtable<String, MChatFileServerReader>();
    }

    public static MChatFileStore getSharedInstance() {
        if (self == null) {
            self = new MChatFileStore();
        }
        return self;
    }

    public void addToSenderTable(String id, MChatFileServerSender sender) {
        try {
            fileSenderRequestTable.put(id, sender);
        } catch (Exception e) {
        }
    }

    public void removeFromSender(String id) {
        try {
            fileSenderRequestTable.remove(id);
        } catch (Exception e) {
        }
    }

    public MChatFileServerSender getSender(String id) {
        return fileSenderRequestTable.get(id);
    }

    public void addToReaderTable(String id, MChatFileServerReader readerMChat) {
        try {
            fileReaderRequestTable.put(id, readerMChat);
        } catch (Exception e) {
        }
    }

    public void removeFromReaderTable(String id) {
        try {
            fileReaderRequestTable.remove(id);
        } catch (Exception e) {
        }
    }

    public MChatFileServerReader getReader(String id) {
        return fileReaderRequestTable.get(id);
    }

    public void destroySelf() {
        fileSenderRequestTable = null;
        fileReaderRequestTable = null;
        self = null;
    }
}
