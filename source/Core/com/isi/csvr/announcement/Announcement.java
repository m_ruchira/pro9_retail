package com.isi.csvr.announcement;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 3, 2005
 * Time: 10:31:42 AM
 */

public class Announcement implements Serializable {

    private static final char BATE_ANN_SYMBOL = 'A';
    private static final char BATE_ANN_TIME = 'B';
    private static final char BATE_ANN_NUMBER = 'C';
    private static final char BATE_ANN_LANGUAGE = 'D';
    private static final char BATE_ANN_HEADLINE = 'E';
    private static final char BATE_ANN_MESSAGE = 'F';
    private static final char BATE_ANN_URL = 'G';
    private static final char BATE_ANN_INSTRUMENT = 'N';
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmm");
    public String exchange = null;
    private String symbol = "";
    private int instrumentType = 0;
    private String key = null;
    private long announcementTime;
    private String announcementNo = null;
    private String language = null;
    private String headLine = null;
    private String message = null;
    private String url = null;
    private int newMessage;

    public Announcement(String sExchange) {
        exchange = sExchange;
    }

    public void setData(String[] data) throws Exception {
        if (data == null)
            return;
        char tag;
        for (int i = 0; i < data.length; i++) {
            if (data[i] == null)
                continue;
            if (data[i].length() <= 1)
                continue;
            tag = data[i].charAt(0);
            switch (tag) {
                case BATE_ANN_SYMBOL:
                    symbol = data[i].substring(1);
                    key = SharedMethods.getKey(exchange, symbol, instrumentType);
                    break;
                case BATE_ANN_INSTRUMENT:
                    instrumentType = Integer.parseInt(data[i].substring(1));
                    key = SharedMethods.getKey(exchange, symbol, instrumentType);
                    break;
                case BATE_ANN_TIME:
//                    System.out.println("announcement time for symbol="+key + " , time = "+data[i].substring(1));
                    announcementTime = ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, timeFormat.parse(data[i].substring(1)).getTime()); //bug id <#0040> (8.20.001)
                    break;
                case BATE_ANN_NUMBER:
                    announcementNo = data[i].substring(1);
                    break;
                case BATE_ANN_LANGUAGE:
                    language = data[i].substring(1);
                    break;
                case BATE_ANN_HEADLINE:
                    headLine = UnicodeUtils.getNativeString(data[i].substring(1));
                    break;
                case BATE_ANN_MESSAGE:
                    setMessage(UnicodeUtils.getNativeStringFromCompressed(data[i].substring(1)));
                    break;
                case BATE_ANN_URL:
                    url = data[i].substring(1);
                    break;
            }
        }
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
        key = SharedMethods.getKey(exchange, symbol, instrumentType);
    }

//    public void setExchange(String exchange) {
//        this.exchange = exchange;
//        key = exchange + Constants.KEY_SEPERATOR_CHARACTER + symbolx;
//    }

    public String getAnnouncementNo() {
        return announcementNo;
    }

    public void setAnnouncementNo(String announcementNo) {
        this.announcementNo = announcementNo;
    }

    public long getAnnouncementTime() {
        return announcementTime;
    }

    public void setAnnouncementTime(long announcementTime) {
        this.announcementTime = ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, announcementTime); //bug id <#0040> (8.20.001)
//        this.announcementTime = announcementTime;
    }

    public String getExchange() {
        return exchange;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if (message.toUpperCase().startsWith(Constants.NO_TRANSLATION_ANNOUNCEMENT.toUpperCase())) {
            this.message = "<A HREF='--BODY/" + exchange + "/" + announcementNo + "/AR'>" + message + "</A>";
        } else {
            this.message = message;
        }
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
//        symbol.setSymbol(newsymbol);
        this.symbol = symbol;
        key = SharedMethods.getKey(exchange, symbol, instrumentType);
    }

    public String getUrl() {
        return url;
    }

    public String getKey() {
        return key;
    }

    public int getNewMessage() {
        return newMessage;
    }

    public void setNewMessage() {
        this.newMessage = 1;
    }

    public void setOldMessage() {
        this.newMessage = 0;
    }

    public String getShortDescription() {
        String sKey = SharedMethods.getKey(exchange, symbol, instrumentType);
        if (sKey != null && !sKey.isEmpty()) {
            Stock stk = DataStore.getSharedInstance().getStockObject(sKey);
            if (stk != null) {
                return stk.getShortDescription();
            }
        }
        return "";
    }

    public String toString() {
        return "Announcement{" +
                "announcementNo='" + announcementNo + "'" +
                ", exchange='" + exchange + "'" +
                ", symbol='" + symbol + "'" +
                ", instrument Type='" + instrumentType + "'" +
                ", announcementTime=" + announcementTime +
                ", language='" + language + "'" +
                ", headLine='" + headLine + "'" +
                ", message='" + message + "'" +
                ", url='" + url + "'" +
                ", msg length='" + message.length() + "'" +
                "}";
    }
}

