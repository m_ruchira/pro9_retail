package com.isi.csvr.announcement;

import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.shared.DownArrow;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 12, 2003
 * Time: 10:38:30 AM
 * To change this template use Options | File Templates.
 */
public class DateCombo extends JPanel implements Themeable,
        ActionListener, DateSelectedListener, LayoutManager, FocusListener {
    private final DownArrow downArrow = new DownArrow();
    private DatePicker datePicker = null;
    private DateSelectedListener parentDateSelectedListener = null;
    private FocusListener parentFocusListener = null;
    private JLabel label;
    private String date;
    private long dateLong = 0L;
    private SimpleDateFormat dateFormat;
    private JButton button;
    //private Component parent;

    public DateCombo(JFrame parent) {
        //this.parent = parent;
        datePicker = new DatePicker(parent, true, true);
        dateFormat = new SimpleDateFormat("yyyyMMdd");
        init();
    }

    public DateCombo(JDialog parent) {
        //this.parent = parent;
        datePicker = new DatePicker(parent, true, true);
        dateFormat = new SimpleDateFormat("yyyyMMdd");
        init();
    }

    private void init() {

        datePicker.getCalendar().setSelfDispose(false);
        datePicker.getCalendar().addDateSelectedListener(this);

//        String[] widths = {"100%", "20"};
//        FlexFlowLayout layout = new FlexFlowLayout(widths);
//        setLayout(layout);// new BorderLayout());

        this.setLayout(this);
        this.setBorder(UIManager.getBorder("TextField.border"));

        label = new JLabel();
        label.setHorizontalAlignment(SwingUtilities.TRAILING);
        button = new JButton(downArrow);
        button.addFocusListener(this);
        button.setBorder(BorderFactory.createEmptyBorder());
        button.addActionListener(this);
        button.setActionCommand("BTN");

        add(button);
        add(label);
        Theme.registerComponent(this);
        registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
    }

    public void clearDate() {
        label.setText("");
        date = null;
        dateLong = 0;
    }

    public void addDateSelectedListene(DateSelectedListener parent) {
        this.parentDateSelectedListener = parent;
    }

    public String getText() {
        return label.getText();
    }

    public String getDateString() {
        return date;
    }

    public long getDateLong() {
        return dateLong;
    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getActionCommand().equals("BTN")) {
                Point point = this.getLocation();
                SwingUtilities.convertPointToScreen(point, this.getParent());
                datePicker.setLocation((int) point.getX(), (int) point.getY() + (int) this.getHeight());
                datePicker.showDialog();
                point = null;
            } else {
            }
        } catch (Exception e1) {

        }
    }

    public void setDate(int iYear, int iMonth, int iDay) {
        dateSelected(null, iYear, iMonth, iDay);
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        label.setText(" " + iDay + " / " + (iMonth + 1) + " / " + iYear);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, iYear);
        cal.set(Calendar.MONTH, iMonth);
        cal.set(Calendar.DATE, iDay);
        dateLong = cal.getTime().getTime();
        date = dateFormat.format(cal.getTime());
        datePicker.hide();
        if (parentDateSelectedListener != null)
            parentDateSelectedListener.dateSelected(source, iYear, iMonth, iDay);
    }

    public void addLayoutComponent(String name, Component comp) {
    }

    public void removeLayoutComponent(Component comp) {
    }

    public Dimension preferredLayoutSize(Container parent) {
        return null;
    }

    public Dimension minimumLayoutSize(Container parent) {
        return null;
    }

    public void layoutContainer(Container parent) {
        Border border = ((JComponent) parent).getBorder();
        Insets insets = null;
        try {
            insets = border.getBorderInsets((parent));
        } catch (Exception e) {
            insets = new Insets(0, 0, 0, 0);
        }

        if (parent.getComponentOrientation() == ComponentOrientation.LEFT_TO_RIGHT) {
            JComponent component = (JComponent) parent.getComponent(0); // button
            component.setLocation(parent.getWidth() - parent.getHeight() + insets.left, insets.top);
            component.setSize(parent.getHeight() - insets.left - insets.right,
                    parent.getHeight() - insets.top - insets.bottom);

            component = (JComponent) parent.getComponent(1); // label
            component.setLocation(insets.left, insets.top);
            component.setSize(parent.getWidth() - insets.left - insets.right - parent.getHeight(),
                    parent.getHeight() - insets.top - insets.bottom);
        } else {
            JComponent component = (JComponent) parent.getComponent(0); // button
            component.setLocation(insets.left, insets.top);
            component.setSize(parent.getHeight() - insets.left - insets.right,
                    parent.getHeight() - insets.top - insets.bottom);

            component = (JComponent) parent.getComponent(1); // label
            component.setLocation(parent.getHeight() + insets.left - 5, insets.top);
            component.setSize(parent.getWidth() - insets.left - insets.right - parent.getHeight(),
                    parent.getHeight() - insets.top - insets.bottom);
        }
        border = null;
        insets = null;
    }

    public void addFocusListener(FocusListener parent) {
        parentFocusListener = parent;
    }

    public void focusGained(FocusEvent e) {
        if (parentFocusListener != null) {
            e.setSource(this);
            parentFocusListener.focusGained(e);
        }
    }

    public void focusLost(FocusEvent e) {
        if (parentFocusListener != null) {
            e.setSource(this);
            parentFocusListener.focusLost(e);
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(datePicker.getCalendar());
        datePicker.applyTheme();
    }

    public void updateUI() {
        super.updateUI();
        this.setBorder(UIManager.getBorder("TextField.border"));
    }

    public boolean isEnable() {
        return button.isEnabled();
    }

    public void setEnable(boolean status) {
        button.setEnabled(status);
        if (status) {
            button.setIcon(downArrow);
        } else {
            button.setIcon(null);
        }
    }

}
