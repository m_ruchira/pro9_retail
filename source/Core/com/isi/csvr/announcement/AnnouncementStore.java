// Copyright (c) 2000 Home
package com.isi.csvr.announcement;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;

public class AnnouncementStore {
    private static AnnouncementStore self = null;
    //private static Vector g_oDataStore;
    private AnnouncementList dataStore;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat bodyDateFormat = null;
    private Date date = null;
    private String template = null;
    private long lastAlrtTime = 0;

    /**
     * Constructor
     */
    private AnnouncementStore() {
        dateFormat = new SimpleDateFormat("yyyyMMdd");
        bodyDateFormat = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");//" dd:MM:yyyy '-' HH:mm:ss ");
        date = new Date();
        loadData();
        loadTemplate(null);
    }

    public static AnnouncementStore getSharedInstance() {
        if (self == null) {
            self = new AnnouncementStore();
        }
        return self;
    }

    public synchronized void clearAll() {
        dataStore.clearAll();
    }

    public synchronized void clear(String exchange) {
        dataStore.clear(exchange);
    }

    public synchronized void applyAnnouncementBulbs() {
        try {
            String announceDate;
            String currentDate = "";
            for (int i = 0; i < getLastAnnouncements().size(); i++) {
                try {
                    Announcement announcement = (Announcement) getLastAnnouncements().get(i);
                    announceDate = dateFormat.format(new Date(announcement.getAnnouncementTime()));
                    currentDate = dateFormat.format(ExchangeStore.getSharedInstance().getExchange(announcement.getExchange()).getMarketDate());
                    if ((currentDate.equals(announceDate))) {
                        DataStore.getSharedInstance().getStockObject(announcement.getExchange(),
                                announcement.getSymbol(), announcement.getInstrumentType()).setAnnouncementAvailability(Constants.ITEM_NOT_READ);
                    }
                    announcement = null;
                    announceDate = null;
                    currentDate = null;
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public synchronized void applyAnnouncementBulbs(String exchange) {
        try {
            if (exchange == null) return;

            try {
                Enumeration<String> keys = DataStore.getSharedInstance().getSymbols(exchange);
                while (keys.hasMoreElements()) {
                    DataStore.getSharedInstance().getStockObject(keys.nextElement()).setAnnouncementAvailability(Constants.ITEM_NOT_AVAILABLE);
                }
                keys = null;
            } catch (Exception e) {
//                    e.printStackTrace();
            }

            String announceDate;
            String currentDate = "";
            for (int i = 0; i < getLastAnnouncements().size(); i++) {
                try {
                    Announcement announcement = (Announcement) getLastAnnouncements().get(i);
                    announceDate = dateFormat.format(new Date(announcement.getAnnouncementTime()));

                    currentDate = dateFormat.format(ExchangeStore.getSharedInstance().getExchange(exchange).getMarketDate());
                    if ((announcement.getExchange().equals(exchange))) {
                        if ((currentDate.equals(announceDate))) {
                            DataStore.getSharedInstance().getStockObject(announcement.getExchange(),
                                    announcement.getSymbol(), announcement.getInstrumentType()).setAnnouncementAvailability(Constants.ITEM_NOT_READ);
                        }
                        announcement = null;
                    }
                    announceDate = null;
                    currentDate = null;
                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    /**
     * Save the vector table as an object to the disk
     */
    public synchronized void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/announcements.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(dataStore);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the saved vector table from the disk as an object
     */
    public synchronized void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/announcements.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            dataStore = (AnnouncementList) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            dataStore = new AnnouncementList();
        }
    }

    public String getFilter() {
        return dataStore.getFilter();
    }

    public void setFilter(String filter) {
        dataStore.setFilter(filter);
    }

    public void applyFilter() {
        dataStore.applyFilter();
    }

    public synchronized void addAnnouncement(Announcement announcement) {
        Date dt = new Date(announcement.getAnnouncementTime());
        String announceDate = dateFormat.format(dt);
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(announcement.getExchange());
        String currentDate = null;
        String key = SharedMethods.getKey(announcement.getExchange(), announcement.getSymbol(), announcement.getInstrumentType());

        if (exchange != null) {
            currentDate = dateFormat.format(exchange.getMarketDate());
        } else {
            currentDate = "";
        }
        String symbol = announcement.getExchange();
        if (!announcement.getSymbol().equals("")) {
            symbol = SharedMethods.getKey(announcement.getExchange(), announcement.getSymbol(), announcement.getInstrumentType());
        }
        if (dataStore.addAnnouncement(key, announcement)) {
            if (currentDate.equals(announceDate)) {
                announcement.setNewMessage();
                // focus the window only if the user is interested in reading announcements
                if ((dataStore.getFilter() == null) || (dataStore.getFilter().indexOf(announcement.getExchange()) >= 0)) {
                    if ((!Settings.isScrollingOn()) && (Settings.isPopupAnnounceWindow())) {
                        Client.getInstance().focusAnnouncementWindow();
                        playSound();
                    }
                }
            }
        }
        if ((currentDate.equals(announceDate)) && (DataStore.getSharedInstance().isValidSymbol(key))) {
            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            stock.setAnnouncementAvailability(Constants.ITEM_NOT_READ);
            stock = null;
        }
        exchange = null;
        currentDate = null;
        dt = null;
        announceDate = null;
    }

    public Announcement getAnnouncement(String id) {
        for (int i = 0; i < getLastAnnouncements().size(); i++) {
            Announcement announcement = (Announcement) getLastAnnouncements().get(i);
            if ((announcement.getAnnouncementNo() != null) && (announcement.getAnnouncementNo().equals(id)))
                return announcement;
            announcement = null;
        }
        return null;
    }

    public void showAnnouncementAlert(String id) {

        Announcement announcement;
        String sDescr;
        try {
            announcement = getAnnouncement(id);
            sDescr = announcement.getMessage();

            if ((sDescr == null) || ((sDescr.equals("")))) {
                if (Settings.isConnected()) {
                    SendQFactory.addAnnouncementBodyRequest(announcement.getExchange(), id, Language.getLanguageTag());
                }
                announcement = null;
            } else {
                AnnouncementStore.getSharedInstance().showAnnouncement(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            announcement = null;
            sDescr = null;
        }
    }

    /*public boolean generateFile(String id) {
        LinkedList list = dataStore.getLastAnnouncements();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                Announcement announcement = (Announcement) list.get(i);
                if (announcement.getAnnouncementNo().equals(id)) {
                    generateFile(announcement);
                    return true;
                }
            }
        }
        return false;
    }*/

    /*public void generateFile(Announcement announcement) {
        generateTempFile(announcement);
    }*/

    public String generateTempFile(Announcement announcement) {

        try {
            loadTemplate(announcement.getLanguage());
            date.setTime(announcement.getAnnouncementTime());
            String title = announcement.getHeadLine();
            String body = announcement.getMessage();
            String id = announcement.getAnnouncementNo();
            body = body.replaceAll("\r\n", "<br>"); // replace the line break with HTML line break
//            body = body.replaceAll("\n", "<br>");   // replace the line break with HTML line break

            String page = replaceString(template, "[NEWS_TITLE]", title);

            page = replaceString(page, "[ID]", id);

            if (body == null) {
                page = replaceString(page, "[NEWS_BODY]", title);
            } else {
                page = replaceString(page, "[NEWS_BODY]", body);
            }
            page = replaceString(page, "[NEWS_DATE]", bodyDateFormat.format(date));
            return page;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void showAnnouncement(String id) {
        Announcement announcement = getAnnouncement(id);
        showAnnouncement(announcement);
    }

    public void showSymbolAnnouncement(String symbol, String id) {
        Announcement announcement = getSymbolAnnouncement(symbol, id);
        showAnnouncement(announcement);
    }

    private void showAnnouncement(Announcement announcement) {
        String body = generateTempFile(announcement);
        String company = UnicodeUtils.getNativeString(DataStore.getSharedInstance().getShortDescription(announcement.getKey()));
        Client.getInstance().getBrowserFrame().setContentType("text/html");
        Client.getInstance().getBrowserFrame().setTitle(Language.getString("ANNOUNCEMENT") + " " + company);
        Client.getInstance().getBrowserFrame().setText(body);
        Client.getInstance().getBrowserFrame().setBody(body);
        Client.getInstance().getBrowserFrame().setNewsTitle(Language.getString("ANNOUNCEMENT") + " " + company);
        Client.getInstance().getBrowserFrame().setMoreInfoURL(announcement.getUrl());
        Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_ANNOUNCEMENT);
        Client.getInstance().getBrowserFrame().bringToFont();
        announcement.setOldMessage();
        announcement = null;
    }

    private String replaceString(String buffer, String tag, String str) {
        int index = buffer.indexOf(tag);
        if (index >= 0) {
            return buffer.substring(0, index) + str + buffer.substring(index + tag.length());
        }
        return buffer;
    }

    private void loadTemplate(String language) {
        StringBuilder buffer = new StringBuilder();
        byte[] temp = new byte[1000];
        int count = 0;

        try {
            if ((language == null) || (language.equals(""))) {
                language = Language.getLanguageTag();
            }
            InputStream in = new FileInputStream(".\\Templates\\Announcement_" + language + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            if (language == null) {
                if (Language.isLTR())
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            } else {
                if (language.toUpperCase().equals("EN"))
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            }
            buffer = null;
            in = null;
            temp = null;
        } catch (Exception ex) {
            template = "";
        }
    }

    public synchronized DynamicArray getSymbolAnnouncements(String symbol) {
        return dataStore.getSymbolAnnouncements(symbol);
    }

    public Announcement getSymbolAnnouncement(String symbol, String id) {
        DynamicArray array = getSymbolAnnouncements(symbol);
        for (int i = 0; i < array.size(); i++) {
            Announcement announcement = (Announcement) array.get(i);
            if (announcement.getAnnouncementNo().equals(id))
                return announcement;
            announcement = null;
        }
        return null;
    }

    public synchronized LinkedList getLastAnnouncements() {
        return dataStore.getLastAnnouncements();
    }

    public synchronized DynamicArray getFilteredAnnouncements() {
        return dataStore.getFilteredAnnouncements();
    }

    public synchronized int getAnnouncementCount(String symbol) {
        DynamicArray list = getSymbolAnnouncements(symbol);
        if (list == null)
            return 0;
        else
            return list.size();
    }

    public void playSound() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    if ((System.currentTimeMillis() - lastAlrtTime) > 2000) { // gap between 2 consecative sounds
                        lastAlrtTime = System.currentTimeMillis();
                        NativeMethods.play(System.getProperties().get("user.dir") + "\\sounds\\announcement.wav", 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}


