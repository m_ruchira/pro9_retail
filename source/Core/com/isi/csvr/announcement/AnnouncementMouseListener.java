// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.announcement;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class AnnouncementMouseListener extends MouseAdapter
        implements ListSelectionListener {


    public static final int ONLINE = 0;
    public static final int SEARCHED = 1;
    public static final int COMPANY = 2;

    private JTable table;
    private String message;
    private long time;
    private String title;
    private ANNOUNCMENT_TYPE mode;

    public AnnouncementMouseListener(JTable oTable, ANNOUNCMENT_TYPE mode) {
        this.table = oTable;
        this.mode = mode;
    }

    static byte[] charToAscii(char[] c) throws Exception {

        Charset cs = Charset.forName("ASCII");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(bos, cs);
        //
        osw.write(c, 0, c.length);
        osw.flush();
        byte[] b = bos.toByteArray();
        return b;
    }

    /**
     * Called from the mouse listener
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            selected();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Body");
        }
    }

    /**
     * Called from the list selection listener
     */
    public void valueChanged(ListSelectionEvent e) {
        //selected();
    }

    public void selected() {
        try {
            int iRow = table.getSelectedRow();

            if (iRow >= 0) {
                // it is assumed that the  url is in the last column of the table
                message = (String) table.getModel().getValueAt(table.getSelectedRow(), -3);
                title = UnicodeUtils.getUnicodeString((String) table.getModel().getValueAt(table.getSelectedRow(), table.getModel().getColumnCount() - 1));

                try {
                    time = Long.parseLong((String) table.getModel().getValueAt(table.getSelectedRow(), 0));
                } catch (Exception ex) {
                    time = 0;
                }

                Thread thread = new Thread("AnnouncementMouseListener-Select") {
                    public void run() {
                        if ((message == null) || ((message.equals("")))) {
                            String id = (String) table.getModel().getValueAt(table.getSelectedRow(), -2);
                            Announcement announcement;
                            String language;
                            if (mode == ANNOUNCMENT_TYPE.ALL) {
                                announcement = AnnouncementStore.getSharedInstance().getAnnouncement(id);
                                language = Language.getLanguageTag();
                                if ((language == null) || (language.equals(""))) {
                                    language = Language.getLanguageTag();
                                }
                            } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                                announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(id);

//                                language = SearchedAnnouncementStore.getSharedInstance().getSelectedLanguage();
                                language = announcement.getLanguage();
                                if ((language == null) || (language.equals(""))) {
                                    language = Language.getLanguageTag();
                                }
                            } else {
                                String symbol = (String) table.getModel().getValueAt(table.getSelectedRow(), -4);
                                announcement = AnnouncementStore.getSharedInstance().getSymbolAnnouncement(symbol, id);
                                language = Language.getLanguageTag();
                                if ((language == null) || (language.equals(""))) {
                                    language = Language.getLanguageTag();
                                }
                            }

                            if (Settings.isConnected()) {
                                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_ANNOUNCEMENT);
                                SendQFactory.addAnnouncementBodyRequest(announcement.getExchange(), id, language);
                            }
                            announcement = null;
                        } else {
                            String id = (String) table.getModel().getValueAt(table.getSelectedRow(), -2);
                            if (mode == ANNOUNCMENT_TYPE.ALL) {
                                AnnouncementStore.getSharedInstance().showAnnouncement(id);
                            } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                                SearchedAnnouncementStore.getSharedInstance().showAnnouncement(id);
                            } else {
                                String symbol = (String) table.getModel().getValueAt(table.getSelectedRow(), -4);
                                AnnouncementStore.getSharedInstance().showSymbolAnnouncement(symbol, id);
                            }
                        }
                    }
                };
                thread.start();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}

