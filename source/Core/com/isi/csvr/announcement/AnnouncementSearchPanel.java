package com.isi.csvr.announcement;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.symbolselector.CompanyTable;
import com.isi.csvr.symbolselector.SymbolListener;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 8, 2003
 * Time: 3:10:17 PM
 * To change this template use Options | File Templates.
 */
public class AnnouncementSearchPanel extends InternalFrame
        implements FocusListener, ActionListener, Themeable, ExchangeListener, SymbolListener, ConnectionListener {

    private static final String NULL_EXCHANGE = "";
    private static AnnouncementSearchPanel self;
    String[][] periods = {
            {Language.getString("1_MONTH"), "" + Calendar.MONTH, "1"},
            {Language.getString("3_MONTH"), "" + Calendar.MONTH, "3"},
            {Language.getString("6_MONTH"), "" + Calendar.MONTH, "6"},
            {Language.getString("YTD"), "-1", "1"},
            {Language.getString("1_YEAR"), "" + Calendar.YEAR, "1"},
            {Language.getString("2_YEAR"), "" + Calendar.YEAR, "2"},
            {Language.getString("3_YEAR"), "" + Calendar.YEAR, "3"},
            {Language.getString("5_YEAR"), "" + Calendar.YEAR, "5"},
            {Language.getString("10_YEAR"), "" + Calendar.YEAR, "10"},
            {Language.getString("ALL"), "" + Calendar.YEAR, "100"}};
    private JRadioButton rbSymbol = new JRadioButton(Language.getString("SYMBOL"));
    private JRadioButton rbSector = new JRadioButton(Language.getString("SECTOR"));
    private JRadioButton rbMarket = new JRadioButton(Language.getString("MARKET"));
    private JRadioButton rbAll = new JRadioButton(Language.getString("ALL"));
    private JLabel lbExchange = new JLabel(Language.getString("EXCHANGE"));
    private TWTextField txtSymbols;
    private int instrumentType = -1;
    private TWButton btnDownArraow;
    private TWComboBox cmbSectors;
    private TWComboBox cmbExchanges;
    private TWComboBox languageCombo;
    private JPanel symbolPanel;
    private boolean isSearching;
    private boolean isconnected;
    private JRadioButton rbPreDef = new JRadioButton(Language.getString("PRE_DEFINED"));
    private JRadioButton rbCustom = new JRadioButton(Language.getString("CUSTOM_NEWS"));
    private JComboBox cmbPreDefined;
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private String companyName = null;
    private ArrayList<TWComboItem> exchanges;
    private ArrayList<TWComboItem> sectors;
    private String searchString;

    private AnnouncementSearchPanel() {
        super(false);
        //super(Client.getInstance().getFrame(), Language.getString("WINDOW_TITLE_NEWS_SEARCH_PANEL"));
        try {
            setTitle(Language.getString("WINDOW_TITLE_NEWS_SEARCH_PANEL"));
            Theme.registerComponent(this);
            //setModal(false);
            jbInit();
            setSelectedLanguage(Language.getSelectedLanguage());
            GUISettings.applyOrientation(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    public void show(){
//        cmbSymbols = new JComboBox();
//        cmbSymbols.setEditable(true);
//        symbolPanel.add(cmbSymbols);
////        ((JTextField)cmbSymbols.getEditor()).addFocusListener(this);
////        cmbSymbols.addFocusListener(this);
//        super.show();
//    }

//    public void hide() {
//        try {
//            ((JTextField)cmbSymbols.getEditor()).removeFocusListener(this);
//            cmbSymbols.removeFocusListener(this);
//            super.hide();
//            cmbSymbols = null;
//        } catch (Exception e) {}
//    }

    public static AnnouncementSearchPanel getSharedInstance() {
        if (self == null) {
            self = new AnnouncementSearchPanel();
            Client.getInstance().getDesktop().add(self);
        }
        return self;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    private void populateExchanges() {
        cmbExchanges.removeActionListener(this);
        exchanges.clear();
        //exchanges.add(new TWComboItem(NULL_EXCHANGE, Language.getString("ANY")));

        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeObjects.hasMoreElements()) {
            Exchange exchange = (Exchange) exchangeObjects.nextElement();
            if (!exchange.isExpired() && !exchange.isInactive()) {
                exchanges.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
            }
            exchange = null;
        }
        exchangeObjects = null;
        Collections.sort(exchanges);

        try {
            cmbExchanges.setSelectedIndex(0);
            populateSectors(exchanges.get(0).getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbExchanges.updateUI();
        cmbExchanges.addActionListener(this);
    }

    public void jbInit() {

        this.getContentPane().setLayout(new BorderLayout());
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        // Sources panel
        JPanel sourcePanel = new JPanel(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        sourcePanel.setBorder(BorderFactory.createTitledBorder(Language.getString("SOURCE")));
//        sourcePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(0, 10, 0, 10)));

        sectors = new ArrayList<TWComboItem>();
        cmbSectors = new TWComboBox(new TWComboModel(sectors));//getSectorsModel());
        cmbSectors.setEnabled(false);
        rbSector.setEnabled(false);
        exchanges = new ArrayList<TWComboItem>();
        //languages = new ArrayList<TWComboItem>();
        lbExchange.setPreferredSize(new Dimension(102, 20));
        cmbExchanges = new TWComboBox(new TWComboModel(exchanges));
        cmbExchanges.addFocusListener(this);
        cmbExchanges.setActionCommand("EXCHANGES");
        cmbExchanges.setPreferredSize(new Dimension(150, 20));
        cmbSectors.addFocusListener(this);
        symbolPanel = createSymbolPanel();

        JPanel exchangePanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        exchangePanel.add(lbExchange);
        exchangePanel.add(cmbExchanges);

        // symbol types
        JPanel typePanel = new JPanel();
        String[] typeWidths = {"98", "150"};
        String[] typeHeights = {"20", "20", "20", "20"};
        typePanel.setLayout(new FlexGridLayout(typeWidths, typeHeights, 5, 5, true, true));
        typePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), Language.getString("TYPE")));
        typePanel.add(rbSymbol);
        typePanel.add(symbolPanel);
        typePanel.add(rbSector);
        typePanel.add(cmbSectors);
        typePanel.add(rbMarket);
        typePanel.add(new JLabel());
        typePanel.add(rbAll);
        typePanel.add(new JLabel());


        //JLabel lbl1 = new JLabel(Language.getString("SOURCE"));
        //lbl1.setFont(Theme.getDefaultFont(Font.BOLD, 12));
        //sourcePanel.add(lbl1);
        sourcePanel.add(exchangePanel);
        sourcePanel.add(typePanel);

        ButtonGroup group1 = new ButtonGroup();
        group1.add(rbSymbol);
        group1.add(rbSector);
        group1.add(rbAll);
        group1.add(rbMarket);
        getContentPane().add(sourcePanel, BorderLayout.NORTH);
        rbSymbol.setSelected(true);
        // Period panel
        JPanel combinePanel = new JPanel();
        combinePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "60"}, 0, 0));
        JPanel periodPanel = new JPanel();
        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
//        periodPanel.setBorder(BorderFactory.createEtchedBorder());
        String[] periodWidths = {"93", "150"};
        String[] periodHeights = {"20", "20", "20", "20"};
        periodPanel.setLayout(new FlexGridLayout(periodWidths, periodHeights, 10, 10, true, true));

        String[] dateWidths = {"30", "120"};
        String[] dateHeights = {"20"};
        cmbPreDefined = new JComboBox(getPeriodsModel());
        cmbPreDefined.addFocusListener(this);
        cmbPreDefined.setSelectedIndex(4);
        //JLabel lbl2 = new JLabel(Language.getString("PERIOD"));
        //lbl2.setFont(Theme.getDefaultFont(Font.BOLD, 12));
        //periodPanel.add(lbl2);
//        periodPanel.add(new JLabel());
        periodPanel.add(rbPreDef);
        periodPanel.add(cmbPreDefined);
        periodPanel.add(rbCustom);
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        fromDateCombo.addFocusListener(this);
        JPanel fromPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 0, true, true));
        fromPanel.add(new JLabel(Language.getString("FROM")));
        fromPanel.add(fromDateCombo);
        periodPanel.add(fromPanel);
        periodPanel.add(new JLabel());
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        toDateCombo.addFocusListener(this);
        JPanel toPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 0, true, true));
        toPanel.add(new JLabel(Language.getString("TO")));
        toPanel.add(toDateCombo);
        periodPanel.add(toPanel);

        rbPreDef.setSelected(true);

        ButtonGroup group2 = new ButtonGroup();
        group2.add(rbPreDef);
        group2.add(rbCustom);
        ///////////////////////////////////////////////////////////////////
        JPanel languagepanel = new JPanel();
        JLabel languageLabel = new JLabel(Language.getString("SELECT_LANGUAGE"));
        languageCombo = new TWComboBox();
        languageCombo.addActionListener(this);
        languageCombo.setActionCommand("LANGUAGE");
        languageLabel.setPreferredSize(new Dimension(102, 20));
        languageCombo.setPreferredSize(new Dimension(150, 20));
        populateLanguageCombo();
        languagepanel.setBorder(BorderFactory.createTitledBorder(Language.getString("LANGUAGE_CAPTION")));
//        String[] languageWidths = {"100%","10","140",};
//        String[] languageHeights = {"100%"};
        languagepanel.setLayout(new FlowLayout(FlowLayout.LEADING));//(new FlexGridLayout(languageWidths, languageHeights, 10, 10, true, true));
//        languagepanel.add(new JLabel());
        languagepanel.add(languageLabel);
//        languagepanel.add(new JLabel());
        languagepanel.add(languageCombo);
//        languagepanel.add(new JLabel());
        ///////////////////////////////////////////////////////////////////
        combinePanel.add(periodPanel);
        combinePanel.add(languagepanel);
        getContentPane().add(combinePanel, BorderLayout.CENTER);
//        getContentPane().add(languagepanel, BorderLayout.SOUTH);

        //getContentPane().add(periodPanel, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        TWButton btnOK = new TWButton(Language.getString("SEARCH"));
        btnOK.addActionListener(this);
        btnOK.setActionCommand("OK");
        btnOK.setPreferredSize(new Dimension(80, 25));
        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        btnCancel.setActionCommand("CANCEL");
        btnCancel.setPreferredSize(new Dimension(80, 25));
        buttonPanel.add(btnOK);
        buttonPanel.add(btnCancel);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        ExchangeStore.getSharedInstance().addExchangeListener(this);

        setResizable(false);
        setSize(295, 440);
        this.setClosable(true);
        this.setIconifiable(true);
//        setLocationRelativeTo(Client.getInstance().getDesktop());
        setTitle(Language.getString("ANNOUNCEMENT_SEARCH"));
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        toDateCombo.applyTheme();
        fromDateCombo.applyTheme();
        ConnectionNotifier.getInstance().addConnectionListener(this);
        populateExchanges();
        getContentPane().doLayout();
    }

    private void populateLanguageCombo() {
        String[] saLanguages;
        String[] saLanguageIDs;

        saLanguages = Language.getLanguageCaptions();
        saLanguageIDs = Language.getLanguageIDs();

        for (int i = 0; i < saLanguages.length; i++) {
            TWComboItem item = new TWComboItem(saLanguageIDs[i], saLanguages[i]);
            languageCombo.addItem(item);
            if (item.getId().equals(Language.getSelectedLanguage())) {
//                System.out.println("KKKKKKKKKKKKKKKKKKKKKKKKKK");
                languageCombo.setSelectedItem(item);
            }
        }
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);

        JPanel panel = new JPanel(layout);
        txtSymbols = new TWTextField();
        txtSymbols.addFocusListener(this);
        panel.add(txtSymbols);
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        panel.add(btnDownArraow);

        return panel;
    }

    private void populateSectors(String exchange) {
        sectors.clear();
        if ((exchange == null) || (exchange.equals(NULL_EXCHANGE))) {
            sectors.add(new TWComboItem(NULL_EXCHANGE, ""));
            cmbSectors.setEnabled(false);
            rbSector.setEnabled(false);
        } else {
            Enumeration<Sector> sectorlist = SectorStore.getSharedInstance().getSectors(exchange);
            while (sectorlist.hasMoreElements()) {
                Sector sector = sectorlist.nextElement();
                TWComboItem item = new TWComboItem(sector.getId(), sector.getDescription());
                sectors.add(item);
                sector = null;
            }
            if (sectors.size() == 0) { // no sectors for the exchange
                sectors.add(new TWComboItem(NULL_EXCHANGE, ""));
            }
            cmbSectors.setEnabled(true);
            rbSector.setEnabled(true);
        }
        Collections.sort(sectors);
        cmbSectors.updateUI();
        try {
            cmbSectors.setSelectedIndex(0);
        } catch (Exception e) {
        }
    }

    /**
     * Returns the data model for the sectors combo
     */
    /*private DefaultComboBoxModel getSectorsModel()
    {
        String[] saSectors = null;
        Indices oIndices = IndexStore.getAllIndices();
        oIndices.setFilter("S");
        saSectors = oIndices.getFilteredSymbols();
        oIndices = null;
        Index oIndex = null;
        sectors = new String[saSectors.length+1][2];

        sectors[0][0]=Language.getString("ALL");
        sectors[0][1]="*";
        for (int i=0;i<saSectors.length;i++){
            oIndex = IndexStore.getIndexObject(saSectors[i]);
            sectors[i+1][0] = oIndex.getDescription();
            sectors[i+1][1] = saSectors[i];
            oIndex = null;
        }
        saSectors = null;

        return (new DefaultComboBoxModel(){
            public Object getElementAt(int index){
               return sectors[index][0];
            }

            public int getSize(){
                return sectors.length;
            }
        });
    }*/

    /**
     * Returns the data model for the sectors combo
     */
    private DefaultComboBoxModel getPeriodsModel() {
//        String[] saSectors = null;
//        Indices oIndices = IndexStore.getAllIndices();
//        oIndices.setFilter("S");
//        saSectors = oIndices.getFilteredSymbols();
//        oIndices = null;
//        Index oIndex = null;
//        sectors = new String[saSectors.length+1];

//        sectors[0]=Language.getString("ALL") + Meta.FS;
//        for (int i=0;i<saSectors.length;i++){
//            oIndex = IndexStore.getIndexObject(saSectors[i]);
//            sectors[i+1] = oIndex.getDescription() + Meta.FS + saSectors[i];
//            oIndex = null;
//        }

        return (new DefaultComboBoxModel() {
            public Object getElementAt(int index) {
                return periods[index][0];
            }

            public int getSize() {
                return periods.length;
            }
        });
    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == cmbSectors) {
            rbSector.setSelected(true);
//        } else if (e.getSource() == ) {
//            rbAll.setSelected(true);
        } else if (e.getSource() == txtSymbols) {
            rbSymbol.setSelected(true);
        } else if (e.getSource() == fromDateCombo) {
            rbCustom.setSelected(true);
        } else if (e.getSource() == toDateCombo) {
            rbCustom.setSelected(true);
        } else if (e.getSource() == cmbPreDefined) {
            rbPreDef.setSelected(true);
        }
    }

    public void focusLost(FocusEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            if (Settings.isConnected()) {
                search();
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            setVisible(false);
        } else if (e.getActionCommand().equals("RELOAD")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doReSearch();
        } else if (e.getActionCommand().equals("PREVIOUS")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doPreviuosSearch();
        } else if (e.getActionCommand().equals("SEARCH")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            Client.getInstance().showAnnouncementSearchPanel();
        } else if (e.getActionCommand().equals("NEXT")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            SearchedAnnouncementStore.getSharedInstance().doNextSearch();
        } else if (e.getActionCommand().equals("EXCHANGES")) {
            populateSectors(((TWComboItem) cmbExchanges.getSelectedItem()).getId());
        } else if (e.getActionCommand().equals("ARROW")) {
            searchSymbol();
            /*Point location = txtSymbols.getLocation();
            SwingUtilities.convertPointToScreen(location, txtSymbols.getParent());
            CompanyTable.getSharedInstance().addSymbolListener(this);
            CompanyTable.getSharedInstance().show(location.x, location.y + txtSymbols.getHeight(), false, CompanyTable.WINDOW_FIXED);*/
        } else {
            setSelectedLanguage(e.getActionCommand());
        }
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
//            symbols.appendSymbol(SharedMethods.getKey(((TWComboItem) cmbExchanges.getSelectedItem()).getId(), txtSymbols.getText().toUpperCase(), instrumentType));
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setTitle(Language.getString("ANNOUNCEMENT_SEARCH"));
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSelectedExchange(((TWComboItem) cmbExchanges.getSelectedItem()).getId());
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            String key = symbols.getSymbols()[0];
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);
            instrumentType = SharedMethods.getInstrumentTypeFromKey(key);

            txtSymbols.setText(symbol);

            cmbExchanges.setSelectedIndex(0);
            for (TWComboItem item : exchanges) {
                if (item.getId().equals(exchange)) {
                    cmbExchanges.setSelectedItem(item);
                    break;
                }
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void search() {

        String symbol;
        String exchange;
        String startDate;
        String endDate;
        String symbolType;
        String language = ((TWComboItem) languageCombo.getSelectedItem()).getId();


        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateFormatyyyy = new SimpleDateFormat("yyyy");

        Calendar calendar = Calendar.getInstance();

        if (isValidInput()) {
            if (rbSymbol.isSelected()) {
                symbol = txtSymbols.getText().toUpperCase();
                symbolType = "" + Meta.TYPE_SYMBOL;
                exchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            } else if (rbMarket.isSelected()) {
                symbol = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
                symbolType = "" + Meta.TYPE_SYMBOL;
                exchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            } else if (rbAll.isSelected()) {
                symbol = "*";
                symbolType = "" + Meta.TYPE_SYMBOL;
                exchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            } else {
                symbol = ((TWComboItem) cmbSectors.getSelectedItem()).getId();
                symbolType = "" + Meta.TYPE_SECTOR;
                exchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            }

            if (rbPreDef.isSelected()) {
                int index = cmbPreDefined.getSelectedIndex();
                int field = Integer.parseInt(periods[index][1]);
                if (field == -1) {// ytd type
                    endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                    startDate = dateFormatyyyy.format(new Date(System.currentTimeMillis())) + "0101";
                } else {
                    endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                    calendar.add(Integer.parseInt(periods[index][1]), -Integer.parseInt(periods[index][2]));
                    startDate = dateFormatFull.format(calendar.getTime());
                }
            } else {
                endDate = fromDateCombo.getDateString();
                startDate = toDateCombo.getDateString();
            }

            long lStart = Long.parseLong(startDate);
            long lEnd = Long.parseLong(endDate);
            endDate = "" + Math.max(lStart, lEnd);
            startDate = "" + Math.min(lStart, lEnd);

//            searchString  = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + symbolType +  Meta.FD +
//                      symbol + Meta.FD + startDate + Meta.FD + endDate;

            searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + symbolType + Meta.FD +
                    exchange + Meta.FD + symbol + Meta.FD + startDate + Meta.FD + endDate;

            SearchedAnnouncementStore.getSharedInstance().initSearch();
            SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
            setCompanyName(null);
            SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, language, Integer.MAX_VALUE, exchange);

            SearchedAnnouncementStore.getSharedInstance().disableButtons();
//            Client.getInstance().showAnnouncementSearchWIndow("");
            Client.getInstance().setAnnouncementSearchTitle(SharedMethods.getDisplayKey(exchange, symbol), Language.getString("SEARCHING"));

            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Search");

            symbolType = null;
            symbol = null;
            dateFormatFull = null;
            dateFormatyyyy = null;
            calendar = null;
            startDate = null;
            endDate = null;

            setVisible(false);
        }
    }

    public void searchForSymbol(String key, int interval, int intervalCount) {
        String searchString;

        String symbol = SharedMethods.getSymbolFromKey(key).toUpperCase();
        String exchange = SharedMethods.getExchangeFromKey(key);
        instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");

        Calendar calendar = Calendar.getInstance();
        String endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
        calendar.add(interval, -intervalCount);
        String startDate = dateFormatFull.format(calendar.getTime());
        txtSymbols.setText(symbol);
        for (TWComboItem item : exchanges) {
            if (item.getId().equals(exchange)) {
                cmbExchanges.setSelectedItem(item);
                break;
            }
        }
//        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
//                symbol + Meta.FD + startDate + Meta.FD + endDate;

        searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
                exchange + Meta.FD + symbol + Meta.FD + startDate + Meta.FD + endDate;

        SearchedAnnouncementStore.getSharedInstance().initSearch();
        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE, exchange);
        SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
        SearchedAnnouncementStore.getSharedInstance().disableButtons();
//        Client.getInstance().showAnnouncementSearchWIndow("");
        Client.getInstance().setAnnouncementSearchTitle(SharedMethods.getDisplayKey(exchange, symbol), Language.getString("SEARCHING"));

        searchString = null;
        calendar = null;
        startDate = null;
        endDate = null;
    }


    private boolean isValidInput() {
        boolean returnValue = false;
        if (rbSymbol.isSelected()) {
            if (DataStore.getSharedInstance().isValidCompany(txtSymbols.getText().toUpperCase(), ((TWComboItem) cmbExchanges.getSelectedItem()).getId())) {
                returnValue = true;
            } else {
                new ShowMessage(Language.getString("INVALID_SYMBOL"), "E");
                returnValue = false;
                return returnValue;
            }
        } else {
            returnValue = true;
        }

//        if (rbCustom.isSelected()){
//            if (fromDateCombo.getDateString().compareTo(toDateCombo.getDateString()) < 0){
//                returnValue = false;
//                new ShowMessage(Language.getString("MSG_INVALID_DATE_RANGE"),"E");
//            }else{
//                returnValue = true;
//            }
//        }

        return returnValue;
    }

    private void setTWDecorations() {
        //setUndecorated(true);
        getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
    }

    public String getSelectedLanguage() {
        return SearchedAnnouncementStore.getSharedInstance().getSelectedLanguage();
    }

    public void setSelectedLanguage(String selectedLanguage) {
        SearchedAnnouncementStore.getSharedInstance().setSelectedLanguage(selectedLanguage);
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
//        SwingUtilities.updateComponentTreeUI(fromDateCombo);
//        SwingUtilities.updateComponentTreeUI(toDateCombo);
        toDateCombo.applyTheme();
        fromDateCombo.applyTheme();
    }

    public boolean isConnected() {
        return isconnected;
    }

    public void twConnected() {
        isconnected = true;
    }

    public void twDisconnected() {
        isconnected = false;
        SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(false);
        Client.getInstance().setAnnouncementSearchTitle(null, null);
        Client.getInstance().getDesktop().repaint();
    }

    /* Exchange Listener Methods */

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
//        cmbExchanges.removeActionListener(this);
//        exchanges.clear();
//        //exchanges.add(new TWComboItem(NULL_EXCHANGE, Language.getString("ANY")));
//
//        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
//        while (exchangeObjects.hasMoreElements()) {
//            Exchange exchange = (Exchange) exchangeObjects.nextElement();
//            exchanges.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
//            exchange = null;
//        }
//        exchangeObjects = null;
//        Collections.sort(exchanges);
//
//        try {
//            cmbExchanges.setSelectedIndex(0);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        cmbExchanges.addActionListener(this);
        populateExchanges();

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionCanceled() {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void symbolSelected(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        txtSymbols.setText(symbol);

        cmbExchanges.setSelectedIndex(0);
        for (TWComboItem item : exchanges) {
            if (item.getId().equals(exchange)) {
                cmbExchanges.setSelectedItem(item);
                break;
            }
        }

        CompanyTable.getSharedInstance().removeSymbolListener();
        CompanyTable.getSharedInstance().hide();
    }


}
