package com.isi.csvr.announcement;

import java.io.Serializable;
import java.util.Comparator;

public class AnnouncementComparator implements Comparator, Serializable {

    public static int TYPE_SORT = 0;
    private int type = TYPE_SORT;
    public static int TYPE_SEARCH = 1;

    public AnnouncementComparator(int type) {
        this.type = type;
    }

    public int compare(Object o1, Object o2) {
        /*Announcement announcement1 = (Announcement) o1;
        Announcement announcement2 = (Announcement) o2;

        if (announcement2.getAnnouncementTime() > announcement1.getAnnouncementTime())
            return 1;
        else if (announcement2.getAnnouncementTime() < announcement1.getAnnouncementTime())
            return -1;
        else
            return 0;*/ // Bug id <#0035> announcements dissapear when the time is same (8.20.001)
        Announcement announcement1 = (Announcement) o1;
        Announcement announcement2 = (Announcement) o2;

        if (type == TYPE_SORT) {
            if (announcement2.getAnnouncementTime() > announcement1.getAnnouncementTime())
                return 1;
            else if (announcement2.getAnnouncementTime() < announcement1.getAnnouncementTime())
                return -1;
            else {
                if ((Long.parseLong(announcement2.getAnnouncementNo())) > (Long.parseLong(announcement1.getAnnouncementNo()))) {
                    return 1;
                } else if ((Long.parseLong(announcement2.getAnnouncementNo())) < (Long.parseLong(announcement1.getAnnouncementNo()))) {
                    return -1;
                } else {
                    return 0;
                }
            }
        } else {
            if ((Long.parseLong(announcement2.getAnnouncementNo())) > (Long.parseLong(announcement1.getAnnouncementNo()))) {
                return 1;
            } else if ((Long.parseLong(announcement2.getAnnouncementNo())) < (Long.parseLong(announcement1.getAnnouncementNo()))) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}