package com.isi.csvr.announcement;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenu;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWCustomCheckBox;
import com.isi.csvr.table.AnnouncementModel;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 29, 2005
 * Time: 2:49:23 PM
 */
public class AnnouncementWindow implements ExchangeListener, MouseListener, ActionListener {
    private InternalFrame announcementFrame;
    private TWMenu filter;
    private Table table;
    private JPanel panel;
    private JCheckBox chkDoNotAutoPopup;

    public AnnouncementWindow() {
        table = new Table();
        AnnouncementModel model = new AnnouncementModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_ANNOUNCEMENT");
        model.setViewSettings(oSetting);
        table.setModel(model);
        //todo
        table.setAutoResize(false);
        AnnouncementMouseListener oSelectionListener = new AnnouncementMouseListener(table.getTable(),
                ANNOUNCMENT_TYPE.ALL);
        table.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table.getTable().addMouseListener(oSelectionListener);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(500, 100));

        model.setDataStore(AnnouncementStore.getSharedInstance().getFilteredAnnouncements());
        table.setWindowType(ViewSettingsManager.ANNOUNCEMENT_VIEW);
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();

        announcementFrame = new InternalFrame(table);
        announcementFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(announcementFrame);
        announcementFrame.getContentPane().add(table);
        //oTopDesktop.add(announcementFrame);
        announcementFrame.setResizable(true);
        announcementFrame.setClosable(true);
        announcementFrame.setMaximizable(true);
        announcementFrame.setIconifiable(true);
        announcementFrame.setColumnResizeable(true);
        announcementFrame.setDetachable(true);
        announcementFrame.setPrintable(true);
        announcementFrame.setTitle(Language.getString(oSetting.getCaptionID()));
        announcementFrame.updateUI();
        announcementFrame.applySettings();
        announcementFrame.setLayer(GUISettings.TOP_LAYER);
        announcementFrame.setVisible(false);

        chkDoNotAutoPopup = new JCheckBox(Language.getString("DONOT_POPUP_ANNOUNCE_WINDOW"), !Settings.isPopupAnnounceWindow());
        chkDoNotAutoPopup.setActionCommand("DONOT_POPUP_ANNOUNCE_WINDOW");
        chkDoNotAutoPopup.addActionListener(this);
        announcementFrame.getContentPane().add(chkDoNotAutoPopup, BorderLayout.SOUTH);
        Client.getInstance().getMenus().addAnnouncementAutoPopupModeListener(chkDoNotAutoPopup);

        filter = new TWMenu(Language.getString("EXCHANGES"), "filter.gif");
        table.getPopup().add(filter);
        createFilterPanel();

        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public void setAutoPopupMode(boolean status) {
        chkDoNotAutoPopup.setEnabled(status);
    }

    private void createFilterPanel() {
        String filterString = AnnouncementStore.getSharedInstance().getFilter();
        if (panel == null) {
            panel = new JPanel();
        }
        panel.removeAll();
        ColumnLayout layout = new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE);
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        panel.setLayout(layout);
        panel.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                TWCustomCheckBox item = new TWCustomCheckBox(exchange.getSymbol(), SwingConstants.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
                item.setAlignmentX(Component.CENTER_ALIGNMENT);
//                item.setPreferredSize(new Dimension(60,15));
                panel.add(item);
                if ((filterString == null) || (filterString.indexOf(exchange.getSymbol()) >= 0)) {
                    item.setSelected(true);
                }
            }
            exchange = null;
        }
        exchanges = null;

        panel.add(layout.createGap(5));

        JLabel button = new JLabel("  " + Language.getString("OK") + "  ", SwingConstants.CENTER);
        button.addMouseListener(this);
        button.setAlignmentX(Component.CENTER_ALIGNMENT);
        button.setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
        panel.add(button);

        filter.setLayout(new FlowLayout());
        filter.add(panel);
        filter.getPopupMenu().pack();

        if ((filterString == null) || (filterString.equals(""))) {
            filterString = null;
            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS"));
        } else {
            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS") + " (" + filterString.trim() + ")");
        }
        GUISettings.applyOrientation(filter);
        GUISettings.applyOrientation(panel);
    }

    public InternalFrame getFrame() {
        return announcementFrame;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("DONOT_POPUP_ANNOUNCE_WINDOW")) {
            Client.getInstance().getMenus().setAnnouncementAutoPopupMode(!chkDoNotAutoPopup.isSelected());
            Settings.setPopupAnnounceWindow(!chkDoNotAutoPopup.isSelected());
        }
    }

    // Exchange Listeners
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        createFilterPanel();
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
//        if (e.getSource() instanceof TWCustomCheckBox){
//            TWCustomCheckBox TWCustomCheckBox = (TWCustomCheckBox)e.getSource();
//            TWCustomCheckBox.setSelected(!TWCustomCheckBox.isSelected());
//            TWCustomCheckBox.repaint();
//        }else{
//        if (!(e.getSource() instanceof TWCustomCheckBox)){
        StringBuilder buffer = new StringBuilder();
        Component[] components = panel.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof TWCustomCheckBox) {
                TWCustomCheckBox component = (TWCustomCheckBox) components[i];
                if (component.isSelected()) {
                    buffer.append(component.getText());
                    buffer.append(",");
                }
            }

        }
        String filterString = buffer.toString();
        try {
            if (filterString.substring(filterString.length() - 1).equals(",")) {
                filterString = filterString.substring(0, filterString.length() - 1);
            }
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (filterString.equals("")) {
            filterString = null;
            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS"));
        } else {
            announcementFrame.setTitle(Language.getString("ANNOUNCEMENTS") + " (" + filterString.trim() + ")");
        }
        AnnouncementStore.getSharedInstance().setFilter(filterString);
        AnnouncementStore.getSharedInstance().applyFilter();
        table.getPopup().setVisible(false);
//        }
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }


}
