package com.isi.csvr.announcement;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWSocket;
import com.isi.util.TypeConvert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 28, 2003
 * Time: 10:59:49 AM
 * To change this template use Options | File Templates.
 */
public class AnnouncementBodyDownloader {
    private static AnnouncementBodyDownloader self = null;

    private AnnouncementBodyDownloader() {

    }

    public synchronized static AnnouncementBodyDownloader getSharedInstance() {
        if (self == null) {
            self = new AnnouncementBodyDownloader();
        }
        return self;
    }

    public synchronized boolean download(String id, String lang) {
        boolean bSucess = false;
        try {
            int i;
            int len;
            int iValue;
            int rs = Meta.RS.charAt(0);
            StringBuffer sFrame = new StringBuffer();

            Socket oURL = TWSocket.createSocket(Settings.getActiveIP(), Settings.DOWNLOAD_PORT);
            OutputStream out = oURL.getOutputStream();
            if (lang == null) {
                lang = SearchedAnnouncementStore.getSharedInstance().getLastSearchedLanguage();
            }
            String url;
            url = Meta.ANNOUNCEMENT_BODY_SEARCH + Meta.DS + id + Meta.FD +
                    lang + Meta.FS +
                    Meta.USER_SESSION + Meta.DS + Settings.getSessionID() + "\n";
            SharedMethods.printLine(url);

            out.write(url.getBytes());

            InputStream oIn = oURL.getInputStream();

            // read the first in to get the size of the file to be read
            byte[] length = new byte[4];
            for (int j = 0; (j < 4) && (Settings.isConnected()); j++) {
                length[j] = (byte) oIn.read();
            }
            len = TypeConvert.byteArrayToInt(length, 0, 4);
            length = null;

            // read the file into the byte array of given length.
            byte[] bytData = new byte[len];
            byte[] temp = new byte[1000];
            len = 0;
            while (Settings.isConnected()) {
                i = oIn.read(temp);

                if (i == -1) break;
                System.arraycopy(temp, 0, bytData, len, i);
                len += i;
                bSucess = true;
            }

            // check if the full file is read
            if (len != bytData.length)
                throw new RuntimeException("Incomplete announcement body\n Given length : "
                        + bytData.length + " Length read : " + len);

            // unzip the byte array to get actual records
            ByteArrayInputStream byteIn = new ByteArrayInputStream(bytData);
            ZipInputStream zIn = new ZipInputStream(byteIn);
            zIn.getNextEntry();
            while (Settings.isConnected()) {
                iValue = zIn.read();
                if (iValue == -1) break; // end of streeam
                if (iValue != rs) {
                    sFrame.append((char) iValue);
                    continue;
                }

                bSucess = true;
            }
            zIn.closeEntry();
            zIn.close();
            SearchedAnnouncementStore.getSharedInstance().setBody(sFrame.toString());

            temp = null;
            byteIn = null;
            bytData = null;
            zIn = null;
            oIn = null;
            if (!Settings.isConnected()) {
                bSucess = false;
            }
            sFrame = null;
            oURL.close();
            return bSucess;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
