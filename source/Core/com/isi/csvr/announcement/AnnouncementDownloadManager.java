package com.isi.csvr.announcement;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.downloader.HistoryArchiveFile;
import com.isi.csvr.downloader.HistoryArchiveRegister;
import com.isi.csvr.downloader.ZipFileDownloader;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.zip.ZipInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 3, 2005
 * Time: 10:35:07 AM
 */
public class AnnouncementDownloadManager implements Runnable, ConnectionListener {
    private static AnnouncementDownloadManager self = null;
    private ZipFileDownloader titleDownloader;
    private boolean titleDownloading;
    private ArrayList<Item> exchanges;
    private int maxcount = 10;//Integer.MAX_VALUE;

    public AnnouncementDownloadManager() {
        exchanges = new ArrayList<Item>();
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static synchronized AnnouncementDownloadManager getSharedInstance() {
        if (self == null) {
            self = new AnnouncementDownloadManager();
        }
        return self;
    }

    public void addExchange(String exchange, long size) {
        exchanges.add(new Item(exchange, size));
    }

    public void download(int maxTrys) {
        maxcount = maxTrys;
        Thread thread = new Thread(this, "AnnouncementDownloadManager");
        thread.start();
    }

    public void run() {
        downloadTitles();
    }

    public synchronized void downloadTitles() {
        titleDownloading = true;
        //AnnouncementStore.getSharedInstance().clear();


        while (titleDownloading) {
            for (int j = 0; (j < exchanges.size()) && titleDownloading; j++) {
                Item item = exchanges.get(0);
                File[] files = null;
                String url = null;
                try {
                    if (Settings.hasWriteAccess()) {
                        if (ExchangeStore.getSharedInstance().isDefault(item.exchange) && !ExchangeStore.isExpired(item.exchange) && !ExchangeStore.isInactive(item.exchange)) {
                            while (maxcount > 0) {
                                HistoryArchiveRegister.getSharedInstance().addEntry(item.exchange, "Announcements_" + Language.getLanguageTag(), 0, HistoryArchiveFile.START);
                                System.out.println("[Announcement Download] Started " + item.exchange + SharedMethods.getMemoryDetails());
                                url = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.ALL_ANNOUNCMENTS +
                                        Meta.FD + item.exchange + Meta.FD + Language.getLanguageTag() + Meta.EOL;
                                titleDownloader = null;
                                titleDownloader = new ZipFileDownloader(url, item.exchange, "Announcements");
                                files = titleDownloader.downloadFiles();
                                titleDownloader = null;

                                AnnouncementStore.getSharedInstance().clear(item.exchange);

                                for (int i = 0; i < files.length; i++) {
                                    if (files[i].length() > 0) {
                                        files[i].length();
                                        extractFile(files[i], item.exchange);
                                        //AnnouncementStore.getSharedInstance().applyAnnouncementBulbs(item.exchange);
                                    }
                                    try {
                                        files[i].delete();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                maxcount--;
                                files = null;
                                url = null;
                                break;
                            }
                        }
                        exchanges.remove(0);
                        HistoryArchiveRegister.getSharedInstance().addEntry(item.exchange, "Announcements_" + Language.getLanguageTag(), item.size, HistoryArchiveFile.END);
                        System.out.println("[Announcement Download] Completed " + item.exchange + SharedMethods.getMemoryDetails());
                        item = null;
                    } else {
                        System.out.println("Not sending Announcements Download requests");
                        System.out.println("No write access to the loacal Disk to write Announcements archives");
                        break;
                    }
                } catch (Exception e) {
                    HistoryArchiveRegister.getSharedInstance().addEntry(item.exchange, "Announcements_" + Language.getLanguageTag(), 0, HistoryArchiveFile.END);
                    e.printStackTrace();
                    break;
                }

                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void extractFile(File file, String exchange) {
        try {
            int rs = Meta.RS.charAt(0);
            StringBuffer sFrame = new StringBuffer();
            ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
            zIn.getNextEntry();
            int iValue;


            while (true) {
                iValue = zIn.read();
                if (iValue == -1) break; // end of streeam
                if (iValue != rs) {
                    sFrame.append((char) iValue);
                    continue;
                }

                if ((sFrame != null) && (sFrame.length() > 0)) {
                    Announcement announcement = new Announcement(exchange);
                    announcement.setData(sFrame.toString().split(Meta.FD));
                    if ((announcement.getHeadLine() != null) &&
                            ((announcement.getLanguage() == null) || (announcement.getLanguage().equals(Language.getLanguageTag())))) {
                        AnnouncementStore.getSharedInstance().addAnnouncement(announcement); //SharedMethods.getKey(exchange,announcement.getSymbol(), announcement.getInstrumentType()),
                    }
                    announcement = null;
                    sFrame = null;
                    sFrame = new StringBuffer();
                }
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            zIn.closeEntry();
            zIn.close();
            zIn = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTitleDownload() {
        try {
            exchanges.clear();
            titleDownloading = false;
            if (titleDownloader != null)
                titleDownloader.interrupt();
            titleDownloader = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void twConnected() {

    }

    public void twDisconnected() {
        stopTitleDownload();
    }

    private class Item {
        public String exchange;
        public long size;

        public Item(String exchange, long size) {
            this.exchange = exchange;
            this.size = size;
        }
    }
}
