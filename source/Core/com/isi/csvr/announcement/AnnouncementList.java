package com.isi.csvr.announcement;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;

import java.io.Serializable;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

public class AnnouncementList implements Serializable {
    private Hashtable companyAnnouncements = null;
    private LinkedList<Announcement> marketAnnouncements = null;
    private DynamicArray filteredMarketAnnouncements = null;
    private AnnouncementComparator sortComparator;
    private AnnouncementComparator searchComparator;
    private String[] filter;

    AnnouncementList() {
        companyAnnouncements = new Hashtable();
        marketAnnouncements = new LinkedList<Announcement>();//Settings.BTW_ANNOUNCEMENT_DB_SIZE);//,
        filteredMarketAnnouncements = new DynamicArray(Settings.BTW_ANNOUNCEMENT_DB_SIZE);//,
        sortComparator = new AnnouncementComparator(AnnouncementComparator.TYPE_SORT);
        searchComparator = new AnnouncementComparator(AnnouncementComparator.TYPE_SEARCH);

    }

    public void clearAll() {
        companyAnnouncements.clear();
        marketAnnouncements.clear();
        filteredMarketAnnouncements.clear();
    }

    public void clear(String exchange) {
        synchronized (this) {
            // clear company announcement store
            try {
                Enumeration keys = companyAnnouncements.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    if (key.indexOf(exchange + Constants.KEY_SEPERATOR_CHARACTER) == 0) {
                        companyAnnouncements.remove(key);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // clear filtered announcements
            try {
                for (int i = filteredMarketAnnouncements.size() - 1; i >= 0; i--) {
                    Announcement announcement = (Announcement) filteredMarketAnnouncements.get(i);
                    if (announcement.exchange.equals(exchange)) {
                        filteredMarketAnnouncements.remove(i);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // clear market announcements
            try {
                for (int i = marketAnnouncements.size() - 1; i >= 0; i--) {
                    Announcement announcement = marketAnnouncements.get(i);
                    if (announcement.exchange.equals(exchange)) {
                        marketAnnouncements.remove(i);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean addAnnouncement(String symbol, Announcement announcement) {
        synchronized (this) {
            boolean announcementAdded = false;
            DynamicArray list = getSymbolAnnouncements(symbol);
            if (list == null) {
                list = new DynamicArray(Settings.SYMBOL_ANNOUNCEMENT_DB_SIZE);
                companyAnnouncements.put(symbol, list);
            }
            list.insert(announcement, sortComparator);

            int location = Collections.binarySearch(marketAnnouncements, announcement, searchComparator);
            if (location < 0) { // new item
                location = Collections.binarySearch(marketAnnouncements, announcement, sortComparator);
                if (location < 0) {
                    marketAnnouncements.add(-(location + 1), announcement);
                }
            } else {
                marketAnnouncements.set(location, announcement);
            }

            if ((filter == null)) {
                filteredMarketAnnouncements.insert(announcement, sortComparator);
            } else {
                for (int i = 0; i < filter.length; i++) {
                    if (announcement.getExchange().equals(filter[i])) {
                        filteredMarketAnnouncements.insert(announcement, sortComparator);
                    }
                }
            }
            announcementAdded = true;

            return announcementAdded;
        }
    }

    public String getFilter() {
        try {
            String filterString = "";
            for (int i = 0; i < filter.length; i++) {
                filterString = filterString + filter[i];
                if (i != (filter.length - 1)) {
                    filterString = filterString + ",";
                }
            }
            return filterString;
        } catch (Exception e) {
            return null;
        }
    }

    public void setFilter(String filter) {
        if (filter == null) {
            this.filter = null;
        } else {
            this.filter = filter.trim().split(",");
        }

//        this.filter = filter;
    }

    public synchronized void applyFilter() {
        filteredMarketAnnouncements.clear();

        for (Announcement item : marketAnnouncements) {
            if ((filter == null)) {  // || (filter.indexOf(item.getExchange()) >= 0)) {
                filteredMarketAnnouncements.insert(item, sortComparator);
            } else {
                for (int i = 0; i < filter.length; i++) {
                    if (item.getExchange().equals(filter[i])) {
                        filteredMarketAnnouncements.insert(item, sortComparator);
                    }
                }
            }
        }
    }

    public DynamicArray getSymbolAnnouncements(String symbol) {
        return (DynamicArray) companyAnnouncements.get(symbol);
    }

    public LinkedList<Announcement> getLastAnnouncements() {
        return marketAnnouncements;
    }

    public DynamicArray getFilteredAnnouncements() {
        return filteredMarketAnnouncements;
    }

    private boolean isAlreadyIn(String id, Object[] list, int size) {
        Announcement item;
        for (int i = 0; i < size; i++) {
            item = (Announcement) list[i];
            if (item.getAnnouncementNo().equals(id))
                return true;
        }
        item = null;
        return false;
    }
}