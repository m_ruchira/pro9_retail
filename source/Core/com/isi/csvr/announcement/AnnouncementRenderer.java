package com.isi.csvr.announcement;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Date;

public class AnnouncementRenderer extends TWBasicTableRenderer implements TableCellRenderer {
    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oNewLine;
    private static Color hotNewsFG;
    private static Color tempNewsBG;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private String[] g_asMarketStatus = new String[4];
    private String g_sNA = "NA";
    private boolean newLine;
    private Color foreground, background;
    private DefaultTableCellRenderer lblRenderer;
    private long longValue;
    private Date date;
    private boolean fontChanged = true;
    private Font normalFont;
    private Font newLineFont;
    private boolean ishotNews;
    private boolean isTemparary;

    public AnnouncementRenderer(int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();
        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        date = new Date();

        try {
            g_asMarketStatus[0] = Language.getString("STATUS_PREOPEN");
            g_asMarketStatus[1] = Language.getString("STATUS_OPEN");
            g_asMarketStatus[2] = Language.getString("STATUS_CLOSE");
            g_asMarketStatus[3] = Language.getString("STATUS_PRECLOSE");
        } catch (Exception e) {
            g_asMarketStatus[0] = g_sNA;
            g_asMarketStatus[1] = g_sNA;
            g_asMarketStatus[2] = g_sNA;
            g_asMarketStatus[3] = g_sNA;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        hotNewsFG = Color.black;
        tempNewsBG = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oNewLine = Theme.getColor("ANNOUNCEMENT_NEW_LINE_FGCOLOR");
            hotNewsFG = Theme.getColor("NEWS_HOTNEWS_FGCOLOR");
            tempNewsBG = Theme.getColor("NEWS_TEMPARARY_NEWS_BGCOLOR");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oNewLine = Color.black;
            hotNewsFG = Color.black;
            tempNewsBG = Color.white;
        }
    }

    public void propertyChanged(int property) {
        if (property == PROPERTY_FONT_CHANGED) {
            fontChanged = true;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        newLine = (table.getModel().getValueAt(row, -1)).equals("1");

        if (fontChanged) {
            normalFont = lblRenderer.getFont().deriveFont(Font.PLAIN);
            newLineFont = lblRenderer.getFont().deriveFont(Font.BOLD);
            fontChanged = false;
        }

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (newLine) {
                foreground = sett.getNewAnnouncementFG();
                lblRenderer.setFont(newLineFont);
            } else {
                lblRenderer.setFont(normalFont);
                if (isSelected) {
                    foreground = sett.getSelectedColumnFG();
                } else {
                    if (row % 2 == 0) {
                        foreground = sett.getRowColor1FG();
                    } else {
                        foreground = sett.getRowColor2FG();
                    }
                }
            }
            if (isSelected) {
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    background = sett.getRowColor1BG();
                } else {
                    background = sett.getRowColor2BG();
                }
            }
            hotNewsFG = sett.getNewsHotNewsColorFG();
            tempNewsBG = sett.getNewsTemporaryNewsColorBG();
        } else {
            if (newLine) {
                foreground = g_oNewLine;
                lblRenderer.setFont(newLineFont);
            } else {
                lblRenderer.setFont(normalFont);
                if (isSelected) {
                    foreground = g_oSelectedFG;
                } else {
                    if (row % 2 == 0) {
                        foreground = g_oFG1;
                    } else {
                        foreground = g_oFG2;
                    }
                }
            }
            if (isSelected) {
                background = g_oSelectedBG;
            } else {
                if (row % 2 == 0) {
                    background = g_oBG1;
                } else {
                    background = g_oBG2;
                }
            }
        }

        try {
            ishotNews = Boolean.parseBoolean("" + table.getModel().getValueAt(row, -6));
        } catch (Exception e) {
            ishotNews = false;
        }
        try {
            isTemparary = Boolean.parseBoolean("" + table.getModel().getValueAt(row, -5));
        } catch (Exception e) {
            isTemparary = false;
        }
        if (ishotNews) {
            foreground = hotNewsFG;
        }
        if (isTemparary) {
            background = tempNewsBG;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);
        lblRenderer.setIcon(null);
        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 8:
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 'L': // LONG DATE for Net change calculator
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lblRenderer;
    }


    private long toLongValue(Object oValue) throws Exception {
        try {
            return Long.parseLong((String) oValue);
        } catch (NumberFormatException e) {
            return 0;
        }
    }
}
