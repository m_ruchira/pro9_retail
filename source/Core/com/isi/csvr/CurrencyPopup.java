package com.isi.csvr;

import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.event.CurrencyListener;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Apr 27, 2006
 * Time: 5:29:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class CurrencyPopup extends JPopupMenu implements CurrencyListener {
    private JPanel basePanel;
    private JPanel btnPanel;
    private JPanel centerPanel;
    private JCheckBox[] checkPFs;
    private TWButton btnOK;
    private TWButton btnCancel;
    private CurrencyWindow parent;
    private ArrayList<String> selected;

    public CurrencyPopup(CurrencyWindow parentIn, ArrayList<String> selIDs) {
        this.parent = parentIn;
        this.selected = selIDs;
        basePanel = new JPanel(new BorderLayout());
        ArrayList<String> pfRecords = CurrencyStore.getCurrencyIndex();
        //basePanel = new JPanel(new BorderLayout());
        int noOfElems = pfRecords.size();
        centerPanel = new JPanel(new GridLayout(noOfElems, 1));
        //btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        checkPFs = new JCheckBox[noOfElems];
        String mess;
        for (int i = 0; i < pfRecords.size(); i++) {
            try {
                mess = pfRecords.get(i) + " ( " + CurrencyStore.getSharedInstance().getCurrencyDescription(pfRecords.get(i)) + " ) ";
                checkPFs[i] = new JCheckBox(mess);
                if ((selIDs != null) && (selIDs.size() > 0)) {
                    if (selIDs.contains(pfRecords.get(i))) {
                        checkPFs[i].setSelected(true);
                    }
                }
                centerPanel.add(checkPFs[i]);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        mess = null;
        pfRecords = null;

        btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 4, 2));
        btnOK = new TWButton(Language.getString("OK"));
        btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyCustomFilter();
                disposePopup();
            }
        });
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                disposePopup();
            }
        });

        btnOK.setPreferredSize(new Dimension(80, 20));
        btnCancel.setPreferredSize(new Dimension(80, 20));

        GUISettings.setSameSize(btnOK, btnCancel);
        btnPanel.add(btnOK);
        btnPanel.add(btnCancel);
        basePanel.add(centerPanel, BorderLayout.CENTER);
        basePanel.add(btnPanel, BorderLayout.SOUTH);

        this.add(basePanel);
        GUISettings.applyOrientation(this);
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
    }

    private void setCurrencyList() {
        centerPanel.removeAll();
        ArrayList<String> pfRecords = CurrencyStore.getCurrencyIndex();
        int noOfElems = pfRecords.size();
        centerPanel.setLayout(new GridLayout(noOfElems, 1));
        checkPFs = null;
        String mess;
        checkPFs = new JCheckBox[noOfElems];
        for (int i = 0; i < pfRecords.size(); i++) {
            mess = pfRecords.get(i) + " ( " + CurrencyStore.getSharedInstance().getCurrencyDescription(pfRecords.get(i)) + " ) ";
            checkPFs[i] = new JCheckBox(mess);
            centerPanel.add(checkPFs[i]);
            if ((selected != null) && (selected.size() > 0)) {
                if (selected.contains(pfRecords.get(i))) {
                    checkPFs[i].setSelected(true);
                }
            }
        }
        mess = null;
        pfRecords = null;
        centerPanel.doLayout();
    }

    /*private void applyCustomFilter() {
        ArrayList<String> selected = new ArrayList<String>();
        int noOfItemsSelected = 0;
        for (int i = 0; i < checkPFs.length; i++) {
            String str[] ;
            if (checkPFs[i].isSelected()) {
                System.out.println(" S1 = "+checkPFs[i].getText());
                System.out.println(" S2 = "+ (checkPFs[i].getText()).split(" \\( ")[0]);
                str = (checkPFs[i].getText()).split(" \\( ");
                selected.add(str[0]);
                noOfItemsSelected++;
            }
        }
        parent.setNoOfSellCurrencies(noOfItemsSelected);
        parent.setSelectedCurrencies(selected);
    }*/

    private void applyCustomFilter() {
        //change start
//        ArrayList<String> selected = new ArrayList<String>();
        selected.clear();
        int noOfItemsSelected = 0;
        for (int i = 0; i < checkPFs.length; i++) {
            try {
                String str[];
                if (checkPFs[i].isSelected()) {
                    str = (checkPFs[i].getText()).split(" \\( ");
                    if (!selected.contains(str[0])) {
                        selected.add(str[0]);
                        noOfItemsSelected++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        parent.setNoOfSellCurrencies(noOfItemsSelected);
        parent.setSelectedCurrencies(selected);
    }

    private void disposePopup() {
        this.setVisible(false);
        //change start
//        selected.clear();
//        selected = null;
        //change end
        checkPFs = null;
        parent = null;
    }

    public void currencyAdded() {
        setCurrencyList();
    }
}