// Copyright (c) 2000 ISI Sri Lanka
package com.isi.csvr.shared;

/**
 * A Class class.
 * <p/>
 *
 * @author Yasantha
 */
public class
        Meta {
    /* STX,FS,RS, END OF LINE*/
    public static final String EOL = "\n";          // 13
    public static final String FS = "\u001C";       // 28
    public static final String RS = "\u001E";       // 30
    public static final char RS_CHAR = '\u001E';  // 30
    public static final String DS = "\u0002";       //  2
    public static final char ETX = (char) 3;
    public static final String SRD = "" + (char) 27;        // 126 - Start Record Delimeter
    public static final char ERD = (char) 26;             // 94  - End Record Delimeter
    public static final String FD = "" + (char) 25;
    public static final String ID = "" + (char) 24;
    public static final String US = "\u001F";
    public static final String EXT3 = "\u0012";
    public static final String DD = "" + (char) 30;         // 126 - Delimeter for data within data
    public static final String GS = "\u001D";
    public static final int SAT_EOF = '\u0001';       //  1


    public static final short MARKET_PREOPEN = 1;
    public static final short MARKET_OPEN = 2;
    public static final short MARKET_CLOSE = 3;
    public static final short MARKET_PRECLOSE = 4;

    //==================MORMUBMKT-1041==================  
    public static final int PRECLOSE_WITH_TRADES = 18;
    public static final int CLOSE_WITH_TRADES = 19;
    public static final int TRADING_AT_LAST = 20;
    //==================================================

    /* Common tags */
    public static final short INVALID = -1;
    public static final short NO = 0;
    public static final short YES = 1;
    public static final short SAFE_MODE = 2;

    /* Equity and Index Related*/
    /* Yasantha */

    /* Symbol related information*/
    public static final int FRAME_DATA = 3;
    public static final int FRAME_DATA2 = 13;
    public static final short SEQUENCE = 179;
    public static final short SYMBOL = 1;
    public static final int SYMBOL_CODE = 4;
    public static final short EXCHANGE = 5;
    public static final short EXCHANGE_CODE = 6;
    public static final short FULL_FRAME = 8;
    public static final short INSTRUMENT_TYPE = 10;
    public static final short DECIMAL_CORRECTION_FACTOR = 11;   // Change 11/05/07 - To implement Decimal Correction Factor
    public static final short MARKET_CENTER = 12;   // Change 11/05/07 - To implement Decimal Correction Factor


    public static final short MESSAGE_TYPE = 2;

    public static final short MESSAGE_TYPE_ALL = 1;
    public static final short MESSAGE_TYPE_COMBINED_TRADE = 2;
    public static final short MESSAGE_TYPE_EQUITY = 3;
    public static final short MESSAGE_TYPE_INDEX = 4;
    public static final short MESSAGE_TYPE_MARKET = 5;
    public static final short MESSAGE_TYPE_SUB_MARKET = 20;
    public static final short MESSAGE_TYPE_OHLC_TRADE = 6;
    public static final short MESSAGE_TYPE_DEPTH_REGULAR = 7;
    public static final short MESSAGE_TYPE_DEPTH_SPECIAL = 8;
    public static final short MESSAGE_TYPE_DEPTH_PRICE_ODDLOT = 21;
    public static final short MESSAGE_TYPE_DEPTH_PRICE = 9;
    public static final short MESSAGE_TYPE_ANNOUNCEMENT = 11;
    public static final short MESSAGE_TYPE_ALL_TRADES = 12;
    public static final short MESSAGE_TYPE_HISTORY = 14;
    public static final short MESSAGE_TYPE_OHLC_HISTORY = 15;
    public static final short MESSAGE_TYPE_ALL_ANNOUNCEMENT = 16;
    public static final short MESSAGE_TYPE_INVEST = 22;
    public static final short MESSAGE_TYPE_DETAILDE_MARKET_SUMMARY = 135;
    public static final short MESSAGE_TYPE_OPTION = 17;
    public static final short MESSAGE_TYPE_REGIONALQUOTE = 50;
    public static final short MESSAGE_TYPE_OPTION_CHAIN = 51;
    //    public static final short MESSAGE_TYPE_FUNDAMENTAL_DATA = 52;
    public static final short MESSAGE_TYPE_ORDER = 1000;
    public static final short MESSAGE_TYPE_BROKER = 100;
    public static final short MESSAGE_TYPE_PERIODIC_INDICATOR = 164;



    /*public static final short OHLC_OPEN = 409;
    public static final short OHLC_HIGH = 410;
    public static final short OHLC_LOW = 411;
    public static final short OHLC_CLOSE = 412;
    public static final short OHLC_VOLUME = 413;
    public static final short OHLC_MINUTE = 414;*/

    /*public static final short SYMBOL_SESSION = 9;
    public static final short SYMBOL_STATUS = 10;
    public static final short SYMBOL_LOW_PRICE_52WK = 12;
    public static final short SYMBOL_LOW_PRICE_52WK_DATE = 13;
    public static final short SYMBOL_MAX_PRICE = 14;
    public static final short SYMBOL_MIN_PRICE = 15;
    public static final short SYMBOL_NO_OF_ASKS = 16;
    public static final short SYMBOL_NO_OF_BIDS = 17;
    public static final short SYMBOL_NO_OF_TRADES = 18;
    public static final short SYMBOL_ADJ_CLOSE_PRICE = 19;
    public static final short SYMBOL_AVG_PRICE_52WK = 20;
    public static final short SYMBOL_AVG_VOLUME_52WK = 21;
    public static final short SYMBOL_CLOSE_PRICE_52WK_AGO = 22;
    public static final short SYMBOL_CUM_RETURN_52WK = 23;
    public static final short SYMBOL_HIGH_PRICE_52WK = 25;
    public static final short SYMBOL_HIGH_PRICE_52WK_DATE = 26;
    public static final short SYMBOL_ASSET_CLASS = 202;
    public static final short SYMBOL_SECTOR_CODE = 203;
    public static final short SYMBOL_LAST_UPDATED_TIME = 205;
    public static final short SYMBOL_PER = 206;
    public static final short SYMBOL_PBR = 207;
    public static final short SYMBOL_MARKETCAP = 209;
    public static final short SYMBOL_YEILD = 210;
    public static final short SYMBOL_BEST_ASK_PRICE = 211;
    public static final short SYMBOL_BEST_ASK_QTY = 212;
    public static final short SYMBOL_BEST_BID_PRICE = 213;
    public static final short SYMBOL_BEST_BID_QTY = 214;
    public static final short SYMBOL_LAST_TRADED_TIME = 404;
    public static final short SYMBOL_LAST_TRADED_PRICE = 405;

    public static final short SYMBOL_TOTAL_BID_QTY = 215;
    public static final short SYMBOL_TOTAL_ASK_QTY = 216;
    public static final short SYMBOL_HIGH_ASK = 217;
    public static final short SYMBOL_LOW_BID = 218;
    public static final short SYMBOL_LAST_ASK_PRICE = 219;
    public static final short SYMBOL_LAST_BID_PRICE = 220;
    public static final short SYMBOL_LAST_ASK_QTY = 226;
    public static final short SYMBOL_LAST_BID_QTY = 227;
    public static final short SYMBOL_BID_ASK_RATIO = 221;
    public static final short SYMBOL_SIMPLE_AVG_ASK = 222;
    public static final short SYMBOL_SIMPLE_AVG_BID = 223;
    public static final short SYMBOL_WEIGHTED_AVG_ASK = 224;
    public static final short SYMBOL_WEIGHTED_AVG_BID = 225;

    public static final short SYMBOL_MARKET_CODE = 250;
    public static final short SYMBOL_LISTED_SHARES = 251;
    public static final short SYMBOL_PAID_SHARES = 252;
    public static final short SYMBOL_FACE_VALUE = 253;
    public static final short SYMBOL_COUPON_VALUE = 254;
    public static final short SYMBOL_COUPON_DATE = 255;
    public static final short SYMBOL_NET_PROFIT = 256;
    public static final short SYMBOL_NET_PROFIT_DATE = 257;
    public static final short SYMBOL_EPS = 258;
    public static final short SYMBOL_LAST_TRADE_DATE = 259;
    public static final short SYMBOL_TODAYS_CLOSED = 260;
    public static final short SYMBOL_LAST_52WK_TURNOVER = 28;
    public static final short SYMBOL_LAST_52WK_VOLUME = 29;
    public static final short SYMBOL_RETURN_RANK_52WK = 30;*/

    /* Trade related info */
    /*public static final short TRADE_BUYING_BROKER = 31;
    public static final short TRADE_CHANGE = 32;
    public static final short TRADE_LAST_PRICE = 33;
    public static final short TRADE_AVG_PRICE = 183;
    public static final short TRADE_AVG_VOLUME = 184;
    public static final short TRADE_LAST_VOLUME = 34;
    public static final short TRADE_LAST_TIME = 35;
    public static final short TRADE_PERCENT_CHANGE = 36;
    public static final short TRADE_PREV_CLOSED = 37;

    public static final short TRADE_TICKET_NUMBER = 38;
    public static final short TRADE_BUY_ORDER_NUMBER = 39;
    public static final short TRADE_SELL_ORDER_NUMBER = 40;

    public static final short TRADE_SELLING_BOKER = 41;
    public static final short TRADE_TODAYS_OPEN = 42;
    public static final short TRADE_TICK = 43;
    public static final short TRADE_TURNOVER = 44;
    public static final short TRADE_VOLUME = 45;
    public static final short TRADE_VWAP_CHANGE = 46;
    public static final short TRADE_HIGH_PRICE = 24;
    public static final short TRADE_LOW_PRICE = 11;
    public static final short TRADE_PRICE_52WK_CHANGED = 27;
    public static final short TRADE_LOW_PRICE_52WK_CHANGED = 185;
    public static final short TRADE_SESSION = 195;
    public static final short TRADE_ODDLOT = 84;
    public static final short TRADE_MARKET_CODE = 228;
    public static final short TRADE_SPLITS = 332;*/

    /* Bid-Ask related information */
    public static final short MARKET_DEPTH = 47;

    /*public static final short BID_PRICE = 48;
    public static final short BID_QUANTITY = 49;
    public static final short BID_BROKER = 50;
    public static final short BID_TICK = 51;
    public static final short BID_TIME = 53;
    public static final short BID_ODDLOT = 54;
    public static final short BID_SESSION = 192;
    public static final short BID_ORDER_CONDITION = 196;

    public static final short ASK_PRICE = 55;
    public static final short ASK_QUANTITY = 56;
    public static final short ASK_BROKER = 57;
    public static final short ASK_TICK = 58;
    public static final short ASK_TIME = 60;
    public static final short ASK_ODDLOT = 61;
    public static final short ASK_SESSION = 193;
    public static final short ASK_ORDER_CONDITION = 197;

    public static final short BIDASK_PRICE = 186;
    public static final short BIDASK_QUANTITY = 187;
    public static final short BIDASK_BROKER = 188;
    public static final short BIDASK_TICK = 189;
    public static final short BIDASK_TIME = 190;
    public static final short BIDASK_ODDLOT = 191;
    public static final short BIDASK_SESSION = 194;
    public static final short BIDASK_ORDER_CONDITION = 200;*/

    /* Index Related*/
    /*public static final short INDEX_VALUE = 62;
    public static final short INDEX_ADJ_PREV_CLOSED = 63;
    public static final short INDEX_HIGH_PRICE_52WK = 64;
    //replaced with 'A' public static final short  INDEX_HIGH_PRICE_52WK_DATE           = 65;
    public static final short INDEX_HIGH = 66;
    public static final short INDEX_LOW = 67;
    public static final short INDEX_52WK_HIGH_CHANGED = 69;
    public static final short INDEX_52WK_LOW_CHANGED = 70;
    public static final short INDEX_LOW_PRICE_52WK = 71;
    public static final short INDEX_LOW_PRICE_52WK_DATE = 72;
    public static final short INDEX_NET_CHANGE = 73;
    public static final short INDEX_NO_OF_DOWNS = 74;
    public static final short INDEX_NO_OF_NO_CHANGES = 75;
    public static final short INDEX_NO_OF_TRADES = 76;
    public static final short INDEX_NO_OF_NO_TRADES = 77;
    public static final short INDEX_NO_OF_UPS = 78;
    public static final short INDEX_PERCENT_CHANGE = 79;
    public static final short INDEX_PREV_CLOSED = 80;
    public static final short INDEX_TODAY_INITIAL = 81;
    public static final short INDEX_TURNOVER = 82;
    public static final short INDEX_VOLUME = 83;*/

    // Order related Codes
    /*public static final short ORDER_NO = 85;
    public static final short ORDER_MARKETCODE = 86;
    public static final short ORDER_PRICE = 87;
    public static final short ORDER_QTY = 88;
    public static final short ORDER_TIME = 89;
    public static final short ORDER_TYPE = 90;
    public final static String ORDER_TYPE_ASK = "A";
    public final static String ORDER_TYPE_BID = "B";
    public static final short ORDER_FILLFLAG = 91;
    public static final short ORDER_TIFFALG = 92;
    public static final short ORDER_MINAMT = 93;
    public static final short ORDER_CLIENTACTION = 94;
    public static final short ORDER_CLIENTACTION_INSERT = 1;
    public static final short ORDER_CLIENTACTION_UPDATE = 2;
    public static final short ORDER_CLIENTACTION_DELETE = 3;*/

    /* Currency information  */

    public static final short CURRENCY_RATE = 100;

    /* Asset class information  */
    /*public static final short ASSET_TURNOVER = 119;
    public static final short ASSET_VOLUME = 198;
    public static final short ASSET_NO_OF_DOWNS = 128;
    public static final short ASSET_NO_OF_NO_CHANGE = 133;
    public static final short ASSET_NO_OF_SYMBOLS_TRADED = 138;
    public static final short ASSET_NO_OF_TRADES = 143;
    public static final short ASSET_NO_OF_UPS = 148;
    public static final short ASSET_MARKET_STATUS = 149;
    public static final short ASSET_NO_OF_SYMBOLS = 153;*/

    /* Announcements */
    /*public static final short ANNOUNCEMENT_NO = 154;
    public static final short ANNOUNCEMENT_TIME = 155;
    public static final short ANNOUNCEMENT_DESCRIPTION = 156;
    public static final short ANNOUNCEMENT_BODY = 157;
    public static final short ANNOUNCEMENT_LANGUAGE = 158;
    public static final short ANNOUNCEMENT_SYMBOLDESCRIPTION = 159;
    public static final short ANNOUNCEMENT_URL = 160;*/

    /*Authentication Related*/
    public static final short USER_NAME = 20;
    public static final short USER_PASSWORD = 21;
    public static final short NEWPASSWORD = 163;
    public static final short AUTH_RESULT = 30;
    public static final short SSO_ENABLED = 9;
    public static final short INSTITUTION_ID = 92;
    public static final short AUTHENTICATION_REQUEST_VERSION = 150;
    public static final short EXCG_PROPERTY_FILE = 208;
    public static final String PROTOCOL_VERSION = "1";

    //subset of 30
    /* public static final short AUTH_MSG_KILL = -1;
public static final short AUTH_MSG_BUSY = 0;
public static final short AUTH_MSG_SUCCESSFUL = 1;
public static final short AUTH_MSG_INVALID_USER = 2;
public static final short AUTH_MSG_INVALID_PW = 3;
public static final short AUTH_MSG_EXPIRED = 4;
public static final short AUTH_MSG_NOT_ACTIVATED = 5;
public static final short AUTH_MSG_MULTIPLE = 6;
public static final short AUTH_MSG_INVALID_TW_VERSION = 7;*/
    //end subset 30
    public static final short AUTH_BLOCKING = 31;
    public static final short AUTH_MESSAGE = 32;
    public static final short EXCHANGE_LIST = 33;
    public static final short WINDOW_TYPES = 34;
    public static final short USERID = 35;
    public static final short TRADING_BROKERS = 37;
    public static final short EXCHANGE_PARAMETERS = 39;
    public static final short DES_KEY = 40;
    public static final short BILLING_INSTITUTE = 113;
    public static final short IP_SYMBOL_DRIVEN_CM = 114;
    public static final short SERVER_LANGUAGE_LIST = 115;
    public static final short IP_GLOBAL_CONTENT_PROVIDER_CM = 116;
    public static final short IP_REGIONAL_CONTENT_PROVIDER_CM = 118;
    public static final short INSTITUTION_CODE = 129;

    public static final short NEWER_VERSION = 165;
    public static final short LAST_MARKET_DATE = 166;
    public static final short LAST_CONNECTED_DATE = 167;
    public static final short NO_OF_TRADES_RECEIVED = 201;

    public static final short HISTORY_DATA = 168;
    public static final int HISTORY_REFRESH_DATA = 331;

    //subset HISTORY_DATA HISTORY_REFRESH_DATA
    public static final short HISTORY_FILE = 0;
    public static final short HISTORY_ALL = 1;
    public static final short HISTORY_ON = 2;
    public static final short HISTORY_NONE = 3;
    //end
    public static final short STOCKMASTER_VERSION = 169;
    public static final short INITIALIZE_STOCKMASTER = 170;

    public static final short TRADE_PORT = 171;
    public static final short USER_VERSION = 23;

    //Server Busy Codes
    public static final short SERVER_BUSY = 173;
    //subset 67
    public static final short BUSY_NULL_ZIP = 1;
    public static final short BUSY_DBFAILED = 2;
    public static final short BUSY_DISCONNECT = 3;
    public static final short BUSY_ZIPREQUEST_FAILED = 4;
    //end 67

    //Zip status
    public static final short ZIP_STATUS = 174;
    //subset 69
    public static final short ZIP_AVAILABLE = 1;
    public static final short ZIP_NOTAVAILABLE = 0;
    public static final short ZIP_RECEIVED = 2;
    public static final short ZIP_REQUEST = 3;
    //end 69

    public static final short ZIP_SIZE = 401;

    // Snapshot status
    public static final short SNAPSHOT_STATUS = 175;
    //subset 58
    public static final short SNAPSHOT_ONLINE = 1;
    public static final short SNAPSHOT_OFFLINE = 0;

    public static final short TRADES_STATUS = 176;
    //Reconnect
    public static final short RECONNECT = 177;

    public static final short MKT_STATUS = 178;


    public static final short PULSE = 0;

    /*Broadcast Messages*/
    //public static final short  BROADCAST_MEG                      = 180;
    public static final short BROADCAST_MEG = 85;
    public static final short BROADCAST_MEG_NEW = 98;
    public static final short BROADCAST_MSG_HISTORY = 97;

    public static final short CHAT_NICKNAME = 181;

    /* only for the use of analyser */
    public static final short ZIP_DATA_AVAILABLE = 204;
    public static final short SECONDARY_ZIP_ON = 4;
    public static final short SECONDARY_ZIP_OFF = 5;

    public static final short MARKET_INITIALIZE = 230;

    public static final short LAST_EOD_DATE = 231;

    public static final short DOWNLOAD_URL = 232;

    public static final short CHAT_IP = 233;

    public static final short CHAT_PORT = 234;

    public static final short HOME_URL = 235;

    public static final short XML_IP = 236;

    public static final short SHOW_ORDER_BOOK = 237;

    public static final short SHOW_ANNOUNCEMENTS = 238;

    public static final short SHOW_INTRADAY_CHART = 239;

    public static final short VERSION_CHECK = 240;
    //check values
    public static final short VERSION_OK = 1;
    public static final short VERSION_UPDATE_OK = 2;
    public static final short VERSION_KILL = 4;

    public static final short NO_OF_CONNECTIONS = 241;

    public static final short IP = 242;

    public static final short MESSAGEID = 243;

    public static final short SCMID = 244;

    public static final short LOGOUT = 245;

    public static final short USER_TYPE = 24;
    public static final short USER_LANGUAGE = 26;
    public static final short COMPRESSESSOPN_SUPPORTED = 103;
    //check values
    public static final short USER_TYPE_TW = 1;
    public static final short USER_TYPE_NEW_TW = 30;   //30 - new

    /* Tags for TradePusher   */
    public static final short ADD_TRADE_REQUEST = 300;
    public static final short REMOVE_TRADE_REQUEST = 301;
    public static final short ADD_MARKETDEPTH_REQUEST = 302;
    public static final short REMOVE_MARKETDEPTH_REQUEST = 303;
    public static final short TICKER_MODE = 304;
    public static final short TICKER_MODE_NORMAL = 1;
    public static final short TICKER_MODE_CUSTOMIZED = 2;

    public static final short ADD_OHLC_REQUEST = 305;
    public static final short REMOVE_OHLC_REQUEST = 306;
    public static final short INFO_SUBSCRIPTION = 307;
    public static final short SYMBOL_SUBSCRIPTION = 308;
    public static final short DEFAULT_TIME_OFFSET = 309;
    public static final short REQUEST_CATEGORY = 310;
    public static final short USER_SESSION = 22;
    public static final short SUBSCRIPTION_ID = 313;
    public static final short CM_ID = 28;

    public static final short CLEAR_ORDERS = 314;
    public static final short USER_INFO = 25;
    public static final short CONNECTION_PATH = 102;    // Added 30-04-08

    public static final short DAYS_TO_EXPIRE = 38;
    public static final short EXPIRY_DATE = 36;

    public static final short ZIPFILE = 403;
    public static final int HISTORY_AVAILABLE = 330;
    public static final String forexPrefix = "X:S";

    public static final int WINDOW_TYPE_CATEGORY_INVALID = -1;
    public static final int WINDOW_TYPE_CATEGORY_SYSTEM = 0;
    public static final int WINDOW_TYPE_CATEGORY_EXCHANGE = 1;
    public static final int WINDOW_TYPE_CATEGORY_BOTH = 2;

    //Exchange Window Types
    public static final int IT_SymbolTimeAndSales = 2;
    public static final int IT_MarketDepthByPrice = 3;
    public static final int IT_CompanyProfile = 11;
    public static final int IT_DepthCalculator = 14;
    public static final int IT_SpecialOrderBook = 18;
    public static final int IT_MarketDepthByOrder = 20;
    public static final int IT_TimeAndSalesBacklog = 22;
    public static final int IT_MarketTimeAndSales = 23;
    public static final int IT_StockReports = 32;
    public static final int IT_RegionalQuotes = 35;
    public static final int IT_FundamentalData = 36;
    public static final int IT_CorporateActions = 37;
    public static final int IT_OptionChain = 38;
    public static final int IT_FuturesChain = 40;
    public static final int IT_CashFlow = 41;
    public static final int IT_FullQuote = 42;
    public static final int IT_TimeAndSales_Summary = 45;
    public static final int IT_SnapQuote = 46;
    public static final int IT_Topstock = 50;
    public static final int IT_Market_Summary = 51;
    public static final int IT_Market_indices = 52;
    public static final int IT_Currency_Window = 53;
    public static final int IT_CustomerSupport_email = 54;
    public static final int IT_OptionSymbolBuilder = 55;
    public static final int IT_IndexAnalyzer = 58;
    public static final int IT_Strategy_Watchlist = 59;
    public static final int IT_TWAP_ClosingVWAP = 116;
    //System Window Types
    public static final int IT_HistoryCharting = 4;    // sys
    public static final int IT_ProfessionalCharting = 5;    // sys
    public static final int IT_MarketMap = 6;    // sys
    public static final int IT_TimeAndSalesHistory = 7;    // sys
    public static final int IT_Alerts = 8;    // sys
    public static final int IT_Alerts_SMS = 9;    // sys
    public static final int IT_Portfolio = 10;    // sys
    public static final int IT_FunctionBuilder = 12;    // sys
    public static final int IT_AnnouncementSearch = 13;    // sys
    public static final int IT_MetaStock = 16;    // sys
    public static final int IT_HistoryAnalyzer = 17;    // sys
    public static final int IT_LatencyWindow = 19;    // sys
    public static final int IT_Chat_Support = 21;    // sys
    public static final int IT_DDE_Link = 27;    // sys
    public static final int IT_TradeTicker = 28;    // sys
    public static final int IT_OHLCHistory = 29;    // sys
    public static final int IT_Google = 39;    // sys
    public static final int IT_PriceAPI = 43;    // sys
    public static final int IT_CashFlow_History = 47;    // sys
    public static final int IT_VolumnWatch_View = 48;    // sys
    public static final int IT_Global_Market_Summary = 49;    // sys
    public static final int IT_Bandwidth_Optimizer = 56;    // sys
    public static final int IT_TASI_FORCAST = 64;    // sys
    public static final int IT_MEDIA_PLAYER = 65;    // sys
    public static final int IT_CHART_STRATEGIES = 66;    // sys
    public static final int IT_SCANNER = 67;    // sys
    public static final int IT_SECTOR_OVERVIEW = 68;    // sys
    public static final int IT_ADVANCE_TICKER = 69;    // sys
    public static final int IT_OUTLOOK_REMINDER = 70;    // sys
    public static final int IT_CHART_PATTERNS = 72;    // sys
    public static final int IT_COM_INDEX = 73;    // sys
    public static final int IT_IMPORT_EXPORT_USER_DATA = 75;    // sys
    public static final int IT_TRADE_STATION = 76;    // sys

    public static final int IT_Config = 201;    // sys
    public static final int IT_SaveWSP = 202;    // sys
    public static final int IT_ChangePassword = 203;    // sys
    public static final int WT_ConfigPortfolio = 204;    // sys
    public static final int WT_ConfigWatchList = 205;    // sys
    public static final int WT_OpenWSP = 206;    // sys
    public static final int WT_NewWatchList = 207;    // sys
    public static final int WT_NewPortfolio = 208;    // sys
    public static final int WT_NewForexWatchList = 209;    // sys
    public static final int IT_News = 213;    // sys
    public static final int WT_MutualFund = 215;    // sys
    public static final int IT_AllowDynamicIPs = 218;    // sys
    public static final int IT_ForcePrimaryIP = 219;    // sys
    public static final int IT_VAS = 220;    // sys
    public static final int IT_Alert_EMail = 221;    // sys
    public static final int IT_Announcements = 300;    // sys

    //Common Window Types
    //30,31 are missing
    public static final int IT_EconomicCalendar = 33;    // sys
    public static final int IT_Earningscalendar = 34;    // sys


    public static final short DDE_ON = 86;
    public static final short DDE_OFF = 87;


    /* For internal identification purpose only */
    public static final byte WT_MarketView = 'M';
    public static final byte WT_WatchList = 'W';
    public static final byte WT_FilteredWatchList = 'F';
    public static final byte WT_SectorView = 'S';
    public static final byte WT_Ticker = 'T';

    // request
    public static final byte TYPE_SYMBOL = 0;
    public static final byte TYPE_SECTOR = 1;
    public static final byte TYPE_MKT = 2;

    //public static final short   SYMBOL               = 1; already defined
    public static final short PERIOD = 'P';
    public static final short PREDEFINED = 0;
    public static final short CUSTOM = 1;
    public static final short FROM_DATE = 'F'; //YYYYMMDD
    public static final short TO_DATE = 'T'; //YYYYMMDD
    public static final short START_SEQ = 'S'; //YYYYMMDD

    // reply
    //public static final int IT_Customer_support = 301;

    public static final short ANNOUNCEMENT_BODY_SEARCH = 318;
    public static final short TRADE_HISTORY_LIST_REQUEST = 321;


    public static final short ZIP_TYPE = 247;
    //check values
    public static final short FULL_ZIP = 0;
    public static final short PARTIAL_ZIP = 1;

    public static final short SNAPSHOT_FRAME = 248;
    public static final short BACKLOG_FRAME = 249;

    // alert module
    public static final int ALERT_BID = 1;
    public static final int ALERT_ASK = 2;
    public static final int ALERT_LASTTRADEPRICE = 3;
    public static final int ALERT_LASTTRADEVOL = 4;
    public static final int ALERT_OFFERQTY = 5;
    public static final int ALERT_BIDQTY = 6;
    public static final int ALERT_CUMVOLUME = 7;
    public static final int ALERT_CHANGE = 8;
    public static final int ALERT_PERCENTCHANGE = 9;
    public static final int ALERT_VOLUME = 10;
    public static final int ALERT_OPEN_INTREST = 11;

    public static final int ALERT_EQUAL = 1;
    public static final int ALERT_GRATERTHAN = 2;
    public static final int ALERT_LESSTHAN = 3;
    public static final int ALERT_GREATERTHANOREQUAL = 4;
    public static final int ALERT_LESSTHANEQUAL = 5;

    public static final int ALERT_UP = 1;
    public static final int ALERT_DOWN = 2;

    public static final int ALERT_EXPIRED_UNTIL_CANCEL = 2;
    public static final int ALERT_EXPIRED_ONE_TIME = 1;

    public static final int ALERT_MODE_TW = 1;
    public static final int ALERT_MODE_SMS = 2;
    public static final int ALERT_MODE_EMAIL = 4;
    public static final int ALERT_MODE_STREAMER = 8;
    public static final int ALERT_MODE_WEB = 16;

    public static final String ALERT = "A";
    public static final String ALERT_READ_RECIEPT = "DS";
    public static final String SALERT_READ_RECIEPT = "RR";
    public static final String ALERT_TRIGGERED = "T";
    public static final String ALERT_DELETE = "D";
    public static final String ALERT_HIS = "RH";
    public static final String ALERT_UPDATE = "U";
    public static final String ALERT_CONTROL = "C";
    public static final String ALERT_TW_CONFIG = "TC";
    public static final String ALERT_TW_REJECTED = "R";
    public static final String ALERT_OPERATOR_AND = "AND";
    public static final String ALERT_OPERATOR_OR = "OR";
    public static final String ALERT_STATUS_TRIGGERED_STRING = "T";
    public static final String ALERT_STATUS_INVALIED_STRING = "I";
    public static final String ALERT_STATUS_PEANDIN_STRING = "P";
    public static final String ALERT_STATUS_UPDATE_TEJECTED_STRING = "R";
    public static final String ALERT_STATUS_UPDATE_ACCEPTED_STRING = "S";

    public static final short ALERT_STATUS_NOT_TRIGGERED = 0;
    public static final short ALERT_STATUS_TRIGGERED = 1;

    public static final int NEWS = 77;
    public static final int NEWS_SEARCH_FOR_SYMBOL = 83;
    public static final int NEWS_BODY_REQUEST = 78;
    public static final int NEWS_SOURCES = 112;
    public static final int NEWS_SOURCES_NEW = 119;
    public static final short NEWS_SEARCH_ALL = 84;

    public static final short COMPRESSED_NEWS_BODY_REQUEST = 79;
    public static final short COMPRESSED_ANNOUNCEMENT_BODY_REQUEST = 82;

    public static final int COMPANY_PROFILE = 76;
    public static final int TRADE_ZIPFILE = 329;
    public static final int HISTORY_UPDATE_AVAILABLE = 330;
    public static final int ACTIVE_TRADING_DATE = 334;
    public static final int HDD_ID = 343;

    public static final int OHLC = 0;
    //  public static final int HISTORY                             = 1;
    public static final int TRADES = 2;
    public static final int METASTOCK_INTRADAY = 4;
    public static final int METASTOCK_HISTORY = 5;
    public static final int NONE = 3;

//  public static final int TRADING_BROKERS                     = 345;

    public static final int TAG = 0;
    public static final int VALUE = 1;

    public static final short ADD_EXCHANGE_REQUEST = 40;
    public static final short REMOVE_EXCHANGE_REQUEST = 41;
    public static final short REQUEST_ID = 42;
    public static final short SNAPSHOT_BEGIN = 43;
    public static final short SNAPSHOT_END = 46;

    public static final short HISTORY_FILE_LIST = 500;
    public static final short INTRADAY_FILE_LIST = 501;
    public static final short ARCHIVE_FILE = 51;
    public static final short YTD_HISTORY_FILE = 52;
    public static final short RECENT_HISTORY_FILE = 53;
    public static final short OHLC_HISTORY_FILE = 511;//54; //
    public static final short STOCK_MASTER_FILE = 55;
    public static final short PROPERTY_FILE = 56;
    public static final short CURRENCY_DATA = 57;
    public static final short BACKLOG_PATH_TEST = 59;
    public static final short ANNOUNCEMENT_BODY_REQUEST = 60;
    public static final short CHANGE_PASSWORD = 61;
    public static final short VALIDATE_SYMBOL = 63;
    public static final short VALIDATE_SYMBOL_BULK = 223;
    public static final short TOP_STOCKS = 64;
    public static final short ADD_OPTIMIZED_REQUEST = 160;
    public static final short REMOVE_OPTIMIZED_REQUEST = 161;
    // Top Stock Types                        
    public static final short TOP_GAINERS_BY_CHANGE = 0;
    public static final short TOP_GAINERS_BY_PCNTCHANGE = 1;
    public static final short TOP_LOSERS_BY_CHANGE = 2;
    public static final short TOP_LOSERS_BY_PCNTCHANGE = 3;
    public static final short MOST_ACTIVE_BY_VOLUME = 4;
    public static final short MOST_ACTIVE_BY_TRADES = 5;
    public static final short MOST_ACTIVE_BY_VALUE = 6;
    public static final short LOW_REACHED_52_WEEKS = 7;
    public static final short HIGH_REACHED_52_WEEKS = 8;


    public static final short DAILY_TRADES = 65;
    public static final short ANNOUNCEMENT_SEARCH = 67;
    public static final short ALERT_MESSAGE = 68;
    public static final short SMART_ALERT_MESSAGE = 220;

    //    public static final short   ADD_EXCHANGE_REQUEST                = 40;
    //    public static final short   REMOVE_EXCHANGE_REQUEST             = 41;
    public static final short ADD_SYMBOL_REQUEST = 80;
    public static final short REMOVE_SYMBOL_REQUEST = 81;

    //Add symbol request types
    //    public static final byte QUOTE                  = 0;
    public static final byte COMBINED_TRADE = 1;
    public static final byte TRADE = 1;
    public static final byte MUTUALFUND = 2;
    public static final byte MARKETDEPTH = 3;
    public static final byte INTRADAYTRADES = 5;
    public static final byte FUNDAMENTALDATA = 6;
    //    public static final byte INDEX                  = 7;
    public static final byte NEWSFORSYMBOL = 8;
    public static final byte REGIONALQUOTE = 9;
    public static final byte OPTIONS = 10;
    public static final byte REGIONALOPRAQUOTE = 11;
    public static final byte CORPORATEACTIONS = 12;
    public static final byte FUNDAMENTALDATATABLE = 13;
    public static final byte FOREX = 14;
    public static final byte MOBILEQUOTE = 15;
    public static final byte MOBILEDETAILQUOTE = 16;
    public static final byte INTRADAYOHLC = 17;
    public static final byte INTRADAYOHLCHISTORY = 18;
    public static final byte MESSAGE = 19;
    public static final byte OPTIONLIST = 20;
    public static final byte REGIONALOPTIONLIST = 21;
    public static final byte DEPTH_PRICE_ODDLOT = 40;
    public static final byte REGIONALQUOTELIST = 22;
    public static final byte STOCKREPORT = 23;
    public static final byte TIMEANDSALES = 24;
    public static final byte BLOCKTRADES = 25;
    public static final byte ICAP = 26;
    public static final byte ANNOUNCEMENTS = 27;
    public static final byte ALL = 28;
    public static final byte REGULAR_DEPTH = 29;
    public static final byte PRICE_DEPTH = 30;
    public static final byte SPECIAL_DEPTH = 31;
    public static final byte MARKET = 32;
    public static final byte ALL_ANNOUNCMENTS = 33;
    public static final byte ALL_TRADES = 34;
    public static final byte ALL_OHLC = 35;
    public static final byte HISTORY = 36;
    public static final byte OHLC_HISTORY = 37;
    public static final byte CASHFLOW_HISTORY = 41;
    public static final byte CASHFLOW_RECENT_HISTORY = 42;
    public static final byte CASHFLOW_SUMMARY = 43;
    public static final byte INDEXOHLC = 39;
    public static final short FUTURES = 68;
    public static final short SYMBOL_SEARCH = 69;
    public static final short TRADES_SUMMARY = 70;

    //Instruemnt Types  - Quotes
    public static final short SYMBOL_TYPE_EQUITY = 0;
    public static final short INSTRUMENT_QUOTE = 0;
    public static final short INSTRUMENT_EQUITY = 0;
    public static final short INSTRUMENT_COMMON_STOCK = 61;
    public static final short INSTRUMENT_PREFERRED_STOCK = 62;
    public static final short INSTRUMENT_WARRANT = 63;
    public static final short INSTRUMENT_PREMIUM = 64;
    public static final short INSTRUMENT_TRUST = 65;
    public static final short INSTRUMENT_RIGHT = 66;
    public static final short INSTRUMENT_WARRANT_RIGHT = 67;
    public static final short INSTRUMENT_EXCHANGE_TRADED_FUNDS = 86;
    //Instruemnt Types  - Options
    public static final short SYMBOL_TYPE_OPTIONS = 10;
    public static final short INSTRUMENT_FUTURE = 68;
    public static final short INSTRUMENT_FUTURE_SPREAD = 69;
    public static final short INSTRUMENT_OPTION = 10;
    public static final short INSTRUMENT_EQUITY_OPTION = 71;
    public static final short INSTRUMENT_INDEX_OPTION = 72;
    public static final short INSTRUMENT_FUTURE_OPTION = 73;
    //Instruemnt Types  - Mutual Funds
    public static final short SYMBOL_TYPE_MUTUALFUND = 2;
    public static final short INSTRUMENT_MUTUALFUND = 2;

    //Instruemnt Types  - Bonds
    public static final short SYMBOL_TYPE_BONDS = 75;
    public static final short INSTRUMENT_FIXED_INCOME = 74;
    public static final short INSTRUMENT_BOND = 75;
    public static final short INSTRUMENT_CONVERTIBLE_BOND = 76;
    public static final short INSTRUMENT_MBS = 77;
    public static final short INSTRUMENT_GOV_BOND = 78;
    public static final short INSTRUMENT_CORP_BOND = 79;
    public static final short INSTRUMENT_US_AGENCY_BOND = 80;
    public static final short INSTRUMENT_US_TREASURY_BILL = 81;
    public static final short INSTRUMENT_US_TREASURY_COUPON = 82;
    public static final short INSTRUMENT_MONEY_MARKET = 83;
    public static final short INSTRUMENT_CD = 84;
    //Instruemnt Types  - Forex
    public static final short SYMBOL_TYPE_FOREX = 14;
    public static final short INSTRUMENT_FOREX = 14;
    public static final short INSTRUMENT_FOREX_FRA = 88;
    public static final short INSTRUMENT_FOREX_DEPOSIT = 89;
    public static final short INSTRUMENT_FOREX_FORWARD = 90;
    public static final short INSTRUMENT_FOREX_STATISTICS = 91;
    //Instruemnt Types  - Index
    public static final short SYMBOL_TYPE_INDEX = 7;
    public static final short INSTRUMENT_INDEX = 7;

    public static final byte SEARCH_SYMBOL = 0;
    public static final byte SEARCH_CONTAINS_DESCRIPTION = 1;
    public static final byte SEARCH_STARTS_WITH_DESCRIPTION = 2;
    public static final byte SEARCH_ALL_NAMES = 3;

    public static final byte SEARCH_ALL = 102;
    public static final short TW_VERSION = 110;
    public static final short TW_AUTOUPDATE_FILE = 72;

    public static final int TWV9_AUTOUPDATE_FILE = 513;


    public static final short CUSTOMER_SUPPORT_REQUEST = 73;

    public static final int TR_TYPE_PENDING = 0;
    public static final int TR_TYPE_APPROVED = 1;
    public static final int TR_TYPE_REJECTED = 2;

    public static final int EXCHANGE_TIME_REQUEST = 90;
    public static final int EXCHANGE_TIME_LAG_THRESHOLD_REQUEST = 91;
    public static final short CHAT_SERVER_DETAILS = 93;

    public static final String PUBLIC_KEY_SEPERATOR = "#";
    public static final String RSA_PRODUCT_TYPE = "Pro";

    public static final int OHLC_DOWNLOAD = 511;
    public static final int OHLC_LIST = 491;
    public static final int ADV_HISTORY_FILE_LIST = 508;
    public static final int SYMBOL_HISTORY_FILE = 510;


    public static final int SYMBOL_STATE_AVAILABLE = 0;
    public static final int SYMBOL_STATE_EXPIRED = 1;
    public static final int SYMBOL_STATE_DELISTED = 2;
    public static final int SYMBOL_STATE_SUSPENDED = 3;
    public static final int SYMBOL_STATE_HATLED = 4;
    public static final int SYMBOL_STATE_DELAYED = 5;
    public static final int SYMBOL_STATE_SHUTTING_DOWN = 6;

    public static final String EVENT_TYPE_SYMBOL_STATE = "1";
    public static final String EVENT_TYPE_FINANCIAL_CALENDAR = "2";
    public static final String EVENT_TYPE_NEWS_INDICATOR = "3";
    //  public static final String EVENT_TYPE_ANNOUNCEMENT_INDICATOR= "4";
    public static final String EVENT_TYPE_RESEARCH_MATERIAL = "5";
    public static final String EVENT_TYPE_INSIDER_TRADES = "4";
    public static final String EVENT_TYPE_SPOT_STATE = "6";

//    public static final short VAS_EVENT_BODY_REQUEST        = 101;
//    public static final short VAS_EVENT_SEARCH_REQUEST      = 102;

    //Cooperate action related
    public static final short MESSAGE_TYPE_CORPORATE_ACTIONS = 109;
    public static final short COOP_ACTION_BODY_REQUEST = 102;
    public static final short OPTION_SYMBOL_SEARCH = 216;

    //Fundamental Data:
    public static final short MESSAGE_TYPE_FUNDAMENTAL_DATA = 110;
    public static final short MESSAGE_TYPE_REGIONAL_QUOTE = 111;
    public static final short OPTION_REQUEST = 112;
    public static final short ADD_EXCHANGE_EXPIARYDATE_REQUEST = 113;
    public static final short GLOBAL_MARKET_ADD_REQUEST = 217;
    public static final short GLOBAL_MARKET_REMOVE_REQUEST = 218;

    public static final short STOCK_REPORT_SEARCH = 202;
    public static final short STOCK_REPORT_BODY = 203;
    public static final short CORP_ACTION_SEARCH_REQUEST = 205;
    public static final short CORP_ACTION_BODY_REQUEST = 206;
    public static final short CALENDER_REQUEST = 207;

    public static final byte ECONOMIC_CALENDER = 1;
    public static final byte EARNINGS_CALENDER = 2;

    public static final short USER_PAYMENT_TYPE = 120;
    public static final short EXPIRED_EXCHANGES = 121;
    public static final short INACTIVE_EXCHANGE_LIST = 126;
    public static final short INACTIVE_EXCHANGE_MESSAGE = 127;
    public static final short IP_GLOBAL_NEWS_CONTENT_PROVIDER_CM = 128;
    public static final short USER_PAYMENT_TYPE_PAID = 0;
    public static final short USER_PAYMENT_TYPE_TRIAL = 1;
    public static final short USER_PAYMENT_TYPE_DEMO = 2;

    public static final byte MESSAGE_TYPE_GLOBAL_INDEX = 124;
    public static final byte MESSAGE_TYPE_GLOBAL_INDEX_RESPONCE = 101;

    public static final byte OPTION_SYMBOL_BUILDER_PUT_TYPE = 0;
    public static final byte OPTION_SYMBOL_BUILDER_CALL_TYPE = 1;


    //internation,primary, secondary connection data
    public static final short CONNECTION_TYPE = 125;
    public static final short CONN_TYPE_INTERNATIONAL = 1;
    public static final short CONN_TYPE_REGIONAL = 2;
    public static final short CONN_TYPE_LOCAL = 3;


    public static final short CONNECTION_PATH_PRIMARY = 1;
    public static final short CONNECTION_PATH_SECONDARY = 2;

    public static final int INVESTOR_HISTORY = 517;
    public static final byte INVESTER_TYPE_DOMESTIC = 110;
    public static final byte INVESTER_TYPE_FOREIGN = 111;
    public static final byte INVESTER_TYPE_ARAB = 112;
    public static final byte INVESTER_TYPE_INSTITUTIONAL = 113;
    public static final byte INVESTER_TYPE_INDIVIDUAL = 114;

    public static final short MESSAGE_TYPE_CUST_INDEX = 125;
    public static final short CUST_INDEX = 133;
    public static final short EXCG_FILE_NOT_AVAILABLE = 0;
    public static final short FULL_EXCG_FILE_AVAILABLE = 1;
    public static final short PART_EXCG_FILE_AVAILABLE = 2;

    public static final short FILE_TYPE_MSF = 0;
    public static final short FILE_TYPE_CSV = 1;
    public static final short FILE_TYPE_TXT = 2;
}
