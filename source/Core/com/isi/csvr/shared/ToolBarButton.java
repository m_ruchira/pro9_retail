package com.isi.csvr.shared;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 30, 2005
 * Time: 12:13:49 PM
 */
public class ToolBarButton extends JLabel implements MouseListener {

    private ActionListener actionListener;
    private String actionCommand;
    private Icon disabledIcon = null;
    private Icon enabledIcon = null;
    private Icon rollOverIcon = null;
    private Icon toggledIcon = null;         // the icon representing a pressed button
    private Icon toggledIconMouseOver = null;   // the icon when the mouse is over the pressed button
    private boolean enabled = true;
    private boolean mouseOver = false;
    private boolean toggleEnabled = false;
    private boolean switchOn = false;          // the status of the toggle button

    public ToolBarButton() {
        this.addMouseListener(this);

    }

   /* public ToolBarButton(Icon image) {
        super(image);
        enabledIcon = image;
        this.addMouseListener(this);
    }

     public ToolBarButton(Icon imageEnabled, Icon imageDisabled) { 
        super(imageEnabled);
        enabledIcon = imageEnabled;
        disabledIcon = imageDisabled;
        this.addMouseListener(this);
    }*/

    public ToolBarButton(String text) {
        super(text);
        this.addMouseListener(this);
    }

    public boolean isToggleEnabled() {
        return toggleEnabled;
    }

    public void setToggleEnabled(boolean toggleEnabled) {
        this.toggleEnabled = toggleEnabled;
    }

    public boolean isSwitchOn() {
        return switchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        this.switchOn = switchOn;
        repaint();
    }

    public void setToggledIconMouseOver(Icon toggledIconMouseOver) {
        this.toggledIconMouseOver = toggledIconMouseOver;
    }

    public void setRollOverIcon(Icon rollOverIcon) {
        if (rollOverIcon.getIconHeight() > 0) {
            this.rollOverIcon = rollOverIcon;
        } else {
            this.rollOverIcon = null;
        }
    }

    public void setToggledIcon(Icon toggledIcon) {
        if (toggledIcon.getIconHeight() > 0) {
            this.toggledIcon = toggledIcon;
        } else {
            this.toggledIcon = null;
        }
    }

    public void addActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void setActionCommand(String command) {
        this.actionCommand = command;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        mouseOver = false;
    }

    public void setVisible(boolean aFlag) {
        super.setVisible(aFlag);
        mouseOver = false;
    }

    public Icon getIcon() {
        if (enabled) {
            if (mouseOver) {
                if (toggledIconMouseOver != null && switchOn && toggleEnabled)
                    return toggledIconMouseOver;
                else if (toggledIcon != null && switchOn && toggleEnabled)
                    return toggledIcon;
                else if (rollOverIcon != null)
                    return rollOverIcon;

                else
                    return enabledIcon;
            } else if (toggleEnabled) {


                if (toggledIcon != null && switchOn)
                    return toggledIcon;
                else
                    return enabledIcon;
            } else {
                return enabledIcon;
            }
        } else {
            if (disabledIcon == null) {
                disabledIcon = UIManager.getLookAndFeel().getDisabledIcon(this, enabledIcon);
            }
            return disabledIcon;
        }
    }

//    public boolean isEnabled(){
//        return this.enabled;
//    }

    public void setIcon(Icon icon) {
        enabledIcon = icon;
    }

    /*public boolean isEnabled() {
        return enabled;
    }*/

    public void mouseClicked(MouseEvent e) {
        if (enabled) {
            if (actionListener != null) {
                ActionEvent event = new ActionEvent(this, 0, actionCommand);
                actionListener.actionPerformed(event);
            }
            if (toggleEnabled) {
                if (switchOn)
                    switchOn = false;
                else {
                    switchOn = true;
                }
                repaint();
            }
        }
    }

    public void mouseEntered(MouseEvent e) {
        mouseOver = true;
        repaint();
    }

    public void mouseExited(MouseEvent e) {
        mouseOver = false;
        repaint();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }
}
