package com.isi.csvr.shared;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 20, 2007
 * Time: 12:04:17 PM
 */
public class KotuKotuLayout implements LayoutManager {

    private ArrayList<Component> components;
    private int width;
    private int height;

    public KotuKotuLayout(ArrayList components, int width, int height) {
        this.components = components;
        this.width = width;
        this.height = height;
    }

    public void addLayoutComponent(String name, Component comp) {
        System.out.println(comp);
    }

    public void layoutContainer(Container parent) {
        int parentWidth = parent.getWidth();
        int compX = 0;
        int compY = 0;
        int horizCount = parentWidth / width;
        int compCount = components.size();
        for (int i = 0; i < compCount; i++) {
            compX = (i % horizCount) * width;
            compY = ((i / horizCount)) * height;
            components.get(i).setLocation(compX, compY);
            components.get(i).setSize(width, height);
        }

            /*for (Component component: components) {
                  if ((compX + width) <= parentWidth ){
                      component.setLocation(compX, compY);
                      component.setSize(width, height);
                      compX += width;
                  } else {
                      compX = 0;
                      compY += height;
                      component.setLocation(compX, compY);
                      component.setSize(width, height);
                  }

            }*/
    }

    public Dimension minimumLayoutSize(Container parent) {
        return null;
    }

    public Dimension preferredLayoutSize(Container parent) {
        return new Dimension(100, 100);
    }

    public void removeLayoutComponent(Component comp) {

    }
}
