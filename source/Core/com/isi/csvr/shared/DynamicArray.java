package com.isi.csvr.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class DynamicArray<E> implements Serializable {

    private ArrayList<E> list;
    private Comparator comparator;

    public DynamicArray() {
        list = new ArrayList<E>();
    }

    public DynamicArray(int capacity) {
        list = new ArrayList<E>(capacity);
    }

    public void setComparator(Comparator comparator) {
        this.comparator = comparator;
    }

    public ArrayList<E> getList() {
        return list;
    }

    public ArrayList<E> getUnfilteredStore() {
        return list;
    }

    /**
     * Trims the capacity of this <tt>ArrayList</tt> instance to be the
     * list's current size.  An application can use this operation to minimize
     * the storage of an <tt>ArrayList</tt> instance.
     */
    public synchronized void trimToSize() {
        list.trimToSize();
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list.
     */
    public synchronized int size() {
        return list.size();
    }

    /**
     * Tests if this list has no elements.
     *
     * @return <tt>true</tt> if this list has no elements;
     * <tt>false</tt> otherwise.
     */
    public synchronized boolean isEmpty() {
        return list.size() == 0;
    }

    /**
     * Returns <tt>true</tt> if this list contains the specified element.
     *
     * @param elem element whose presence in this List is to be tested.
     * @return <code>true</code> if the specified element is present;
     * <code>false</code> otherwise.
     */
    public synchronized boolean contains(E elem) {
        if (comparator == null)
            return list.indexOf(elem) >= 0;
        else
            return Collections.binarySearch(list, elem, comparator) >= 0;

    }

    /**
     * Searches for the first occurence of the given argument, testing
     * for equality using the <tt>equals</tt> method.
     *
     * @param elem an object.
     * @return the index of the first occurrence of the argument in this
     * list; returns <tt>-1</tt> if the object is not found.
     * @see Object#equals(Object)
     */
    public synchronized int indexOf(E elem) {
        return list.indexOf(elem);
    }

    public synchronized int indexOfContainingValue(E value) {
        int location = Collections.binarySearch(list, value, comparator);
        return location;
    }

    /**
     * Returns the index of the last occurrence of the specified object in
     * this list.
     *
     * @param elem the desired element.
     * @return the index of the last occurrence of the specified object in
     * this list; returns -1 if the object is not found.
     */
    public synchronized int lastIndexOf(E elem) {
        return list.lastIndexOf(elem);
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of element to return.
     * @return the element at the specified position in this list.
     * @throws IndexOutOfBoundsException if index is out of range <tt>(index
     *                                   &lt; 0 || index &gt;= size())</tt>.
     */
    public synchronized E get(int index) {
        return list.get(index);
    }

    /**
     * Replaces the element at the specified position in this list with
     * the specified element.
     *
     * @param index   index of element to replace.
     * @param element element to be stored at the specified position.
     * @return the element previously at the specified position.
     * @throws IndexOutOfBoundsException if index out of range
     *                                   <tt>(index &lt; 0 || index &gt;= size())</tt>.
     */
    public synchronized E set(int index, E element) {
        return list.set(index, element);
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * @param o element to be appended to this list.
     * @return <tt>true</tt> (as per the general contract of Collection.add).
     */
    public synchronized boolean add(E o) {
        return list.add(o);
    }

    /**
     * Inserts the specified element at the specified position in this
     * list. Shifts the element currently at that position (if any) and
     * any subsequent elements to the right (adds one to their indices).
     *
     * @param index   index at which the specified element is to be inserted.
     * @param element element to be inserted.
     * @throws IndexOutOfBoundsException if index is out of range
     *                                   <tt>(index &lt; 0 || index &gt; size())</tt>.
     */
    public synchronized void add(int index, E element) {
        list.add(index, element);
    }

    /**
     * Inserts the element to the correct location specified by the comparator
     *
     * @param element    element to be inserted
     * @param comparator comparator used to find the correct insertion point
     * @return the location at which the item eas inserted.
     */
    public synchronized int insert(E element, Comparator comparator) {
        int location = Collections.binarySearch(list, element, comparator);
        if (location >= 0) {
            list.set(location, element); // replace the element
        } else {
            location = ((location + 1) * -1);
            list.add(location, element); // insert the element
        }
        return location;
    }

    /**
     * Inserts the element to the correct location specified by the comparator
     *
     * @param element element to be inserted
     * @return the location at which the item eas inserted.
     */
    public synchronized int insert(E element) {
        //if (comparator == null)
        //   throw new Exception("Comparator not set for insert");
        int location = Collections.binarySearch(list, element, comparator);
        if (location >= 0) {
            list.set(location, element); // replace the element
        } else {
            location = ((location + 1) * -1);
            list.add(location, element); // insert the element
        }
        return location;
    }

    /**
     * Removes the element at the specified position in this list.
     * Shifts any subsequent elements to the left (subtracts one from their
     * indices).
     *
     * @param index the index of the element to removed.
     * @return the element that was removed from the list.
     * @throws IndexOutOfBoundsException if index out of range <tt>(index
     *                                   &lt; 0 || index &gt;= size())</tt>.
     */
    public synchronized E remove(int index) {
        return list.remove(index);
    }

    /**
     * Remove the first occurance of the given object given object
     *
     * @param obj
     * @return true iof the object is removed
     */
    public synchronized boolean remove(E obj) {
        return list.remove(obj);
    }

    /**
     * Removes all of the elements from this list.  The list will
     * be empty after this call returns.
     */
    public synchronized void clear() {
        list.clear();
    }
}