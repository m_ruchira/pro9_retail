package com.isi.csvr.shared;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWDateFormat {
    SimpleDateFormat formater;
    //SimpleDateFormat arabicFormater;
    //NumericShaper shaper = null;

    public TWDateFormat(String format) {
        format = format.contains("MMM") ? format.replace("MMM", "\'MONTH\'") : format;
        formater = new SimpleDateFormat(format);
        /*String[] tags = format.split("/");
          StringBuffer arabicFormat = new StringBuffer();
          for (int i = 0; i < tags.length; i++) {
              arabicFormat.append(tags[tags.length-i-1]);
              if (i<(tags.length-1))
                  arabicFormat.append("\\");
          }

          arabicFormater =  new SimpleDateFormat(arabicFormat.toString());
          tags = null;
          arabicFormat = null;
          shaper = NumericShaper.getShaper(NumericShaper.ARABIC);*/
    }

    public String format(long date) {
        String value = null;
        value = formater.format(date);
        if (value.contains("MONTH")) {
            value = value.replace("MONTH", SharedMethods.getMonthLocaleForDate(new Date(date)));
        }
        if (Settings.isShowArabicNumbers()) {
            //value = formater.format(date);
            return arabize(value);
        } else {

            return value;
        }
    }

    public String format(Date date) {
        String value = null;
        value = formater.format(date);
        if (value.contains("MONTH")) {
            value = value.replace("MONTH", SharedMethods.getMonthLocaleForDate(date));
        }
        if (Settings.isShowArabicNumbers()) {
            // value = formater.format(date);
            return arabize(value);
        } else {
            return value;
        }
    }

    /**
     * Sets the time zone for the calendar of this DateFormat object.
     *
     * @param zone the given new time zone.
     */
    public void setTimeZone(TimeZone zone) {
        formater.setTimeZone(zone);
    }

    public Date parse(String format) throws ParseException {
        return formater.parse(format);
    }


    private String arabize(String str) {

        char[] text = null;
        text = str.toCharArray();
        str = null;

        char base = '\u0660' - '\u0030';
        char minDigit = '\u0030';
        for (int i = 0, e = text.length; i < e; ++i) {
            char c = text[i];
            if (c >= minDigit && c <= '\u0039') {
                text[i] = (char) (c + base);
            }
        }
        return new String(text);
    }
}