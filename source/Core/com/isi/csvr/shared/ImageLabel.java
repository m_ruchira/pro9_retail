package com.isi.csvr.shared;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;


public class ImageLabel extends JMenuItem implements Themeable {


    public static ImageIcon img;
    public String Symbol_name = "";
    public Color fontColor;
    int width;
    FontMetrics fontMetrics;
    private int imageWidth;
    private int imageHeight;


    public ImageLabel(String symbol) {
        this.Symbol_name = symbol;
        fontMetrics = getFontMetrics(new TWFont("ARIAL", 0, 19));
        Theme.registerComponent(this);
    }

    public void setText(String str) {
        this.Symbol_name = str;
    }

    public Dimension getPreferredSize() {

        return new Dimension(Math.max(fontMetrics.stringWidth(Symbol_name), (Constants.TABLE_MENU_TOOLBAR_WIDTH - 10)) + 10, 30);
    }

    public void paint(Graphics g) {
        if (imageWidth > 0) {
            width = this.getWidth();
            for (int i = 0; i < width; i += imageWidth) {
                g.drawImage(img.getImage(), i, 0, imageWidth, this.getHeight(), this);
            }
            super.paintChildren(g);
        } else {
            super.paint(g);
        }
        g.setFont(new TWFont("ARIAL", 0, 19));
        g.setColor(fontColor);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.drawString(Symbol_name, (((this.width) - fontMetrics.stringWidth(Symbol_name)) / 2), (30 - ((this.getHeight() - 14) / 2)));
    }

    public void applyTheme() {
        img = new ImageIcon("images/Theme" + Theme.getID() + "/menu/label_tile_image.gif");
        imageWidth = img.getIconWidth();
        imageHeight = img.getIconHeight();
        fontColor = Theme.getOptionalColor("BOARD_TABLE_CAPTION_FG_COLOR");
        if (fontColor == null) {
            fontColor = Color.black;
        }
    }
}