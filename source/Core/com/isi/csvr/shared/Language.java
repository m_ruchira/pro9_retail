// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.shared;

/**
 * Handles the language properties file for
 * language related operations.
 * @author Uditha Nagahawatta
 */


import javax.swing.*;
import java.io.*;
import java.util.*;

public class Language {
    public static final int CLIENT_ID = 0;
    public static final int SERVER_ID = 0;
    public static Properties g_oProperties;
    private static int g_iLangCount;
    private static String g_sLanguageCap;
    private static String serverLanguage;
    private static int defaultLanguageID = -1;
    private static int selectedLanguageID = -1;
    private static String[] g_saLanguageIDs;
    private static String[] g_saServerLanguageIDs;
    private static String[] g_saLanguageCaptions;
    private static String[] g_startMessages;
    private static String g_sLangTag;
    private static boolean ltr = false;
    private static String folderPath = "Languages/";
    private static Hashtable<String, String> languages = new Hashtable<String, String>();

    /**
     * Constructor
     * loads the property file
     */
    public Language() {
        String line = "";
        try {
            Hashtable<String, String> startMessages = new Hashtable<String, String>();
            File file1 = new File(folderPath);
            String[] langFiles = file1.list();
            Arrays.sort(langFiles);
            int count = 0;
            String langText = null;
            String startMSG = null;
            String langID = "";
            ArrayList<String> serverLangs = new ArrayList<String>();
            if ((Settings.getItem("SERVER_LANGUAGE_LIST") == null) || Settings.getItem("SERVER_LANGUAGE_LIST").equals("")) {
                serverLangs = null;
            } else {
                serverLanguage = Settings.getItem("SERVER_LANGUAGE_LIST").trim();
                String[] langs = serverLanguage.split("\\|");
                g_saServerLanguageIDs = new String[langs.length];
                for (int i = 0; i < langs.length; i++) {
                    String lang = langs[i];
                    serverLangs.add(lang);
                    if (lang.equalsIgnoreCase("EN")) {
                        defaultLanguageID = i;
                    }
                }
            }
            for (int j = 0; j < langFiles.length; j++) {
                FileInputStream oIn = new FileInputStream(folderPath + langFiles[j]);
                BufferedReader in = new BufferedReader(new InputStreamReader(oIn));
                while (count < 3) {
                    String str = in.readLine();
                    if (str == null) {
                        break;
                    }
                    String[] data = str.split("=");
                    if ("LANG_ID".equals(data[0])) {
                        count++;
                        langID = data[1];
                    } else if ("LANG".equals(data[0])) {
                        count++;
                        langText = data[1];
                    } else if ("START_MSG".equals(data[0])) {
                        count++;
                        startMSG = data[1];
                    }

                }
                count = 0;
                if ((serverLangs == null) || (serverLangs.contains(langID))) {
                    languages.put(langID, UnicodeUtils.getNativeString(langText));
                    if (startMSG == null) {
                        startMessages.put(langID, UnicodeUtils.getNativeString(langText));
                    } else {
                        startMessages.put(langID, UnicodeUtils.getNativeString(startMSG));
                    }
                }
//                startMSG = null;
                in.close();
                oIn.close();
                in = null;
                oIn = null;
            }
            g_iLangCount = languages.size();
            g_saLanguageCaptions = new String[g_iLangCount];
            g_startMessages = new String[g_iLangCount];
            g_saLanguageIDs = new String[g_iLangCount];
            int i = 0;
            Enumeration<String> langs = languages.keys();
            String str = "";
            while (langs.hasMoreElements()) {
                str = langs.nextElement();
                g_saLanguageIDs[i] = str;
                g_saLanguageCaptions[i] = languages.get(str);
                g_startMessages[i] = startMessages.get(str);
                i++;
            }

        } catch (Exception e) {
            // to do
            e.printStackTrace();
        }
//        loadLanguage();
    }

    public static void setServerLanguageList(String data) {
        try {
            serverLanguage = data;
            Settings.setItem("SERVER_LANGUAGE_LIST", data);
            String[] langs = data.split("\\|");
            g_saServerLanguageIDs = new String[langs.length];
            for (int i = 0; i < langs.length; i++) {
                String lang = langs[i];
                if (lang.equalsIgnoreCase("EN")) {
                    defaultLanguageID = i;
                }
                if (lang.equals(g_sLangTag)) {
                    selectedLanguageID = i;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static int getEnglishLangID() {
        return defaultLanguageID;
    }

    public static String getStringForLanguage(String language, String langID) {
        String value = "";
        Properties props = new Properties();

        try {
            FileInputStream oIn = new FileInputStream(folderPath + language + ".properties");
            props.load(oIn);
            oIn.close();
            value = props.getProperty(langID);
            props.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        props = null;
        return value;
    }

    public static void loadLanguage() {
        try {
            FileInputStream oIn = null;
            String path = null;
            try {
                path = (System.getProperties().get("user.dir") + "/languages/").replaceAll("\\\\", "/")
                        + g_sLangTag + ".properties";
                oIn = new FileInputStream(path);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "<html>Cannot find the Selected Langauge." +
                        "<br>Application will load in English<br>" + path + "<br>" +
                        e.toString(), "ERROR", JOptionPane.ERROR_MESSAGE);
                Settings.setItem("LANGUAGE", "EN");
                g_sLangTag = "EN";
                g_sLanguageCap = languages.get("EN");
                oIn = new FileInputStream(folderPath + g_sLangTag + ".properties");
            }
            g_oProperties = new Properties();
            g_oProperties.load(oIn);
            oIn.close();

            //added for product name/product company name: loading the Product_ file
            try {
                File fold = new File(folderPath);
                File[] productFiles = fold.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        try {
                            name = name.toUpperCase();
                            return name.startsWith(("Product_" + g_sLangTag + ".PROPERTIES").toUpperCase());
                        } catch (Exception e) {
                            return false;
                        }
                    }
                });
                if (productFiles.length > 0) {
                    oIn = new FileInputStream(productFiles[0]);
                    Properties cusProperties = new Properties();
                    cusProperties.load(oIn);
                    oIn.close();
                    Enumeration enu = cusProperties.keys();
                    String key;
                    while (enu.hasMoreElements()) {
                        key = (String) enu.nextElement();
                        g_oProperties.put(key, cusProperties.getProperty(key));
                    }
                    key = null;
                    System.out.println("Product: " + productFiles[0].getName() + " loaded");
                }
            } catch (Exception e) {
                System.out.println("Product file not loaded");
            }
            //added for product name/product company name


            // load custom language properties to the language file
// load custom language properties to the language file

            try {

                File folder = new File(folderPath);
                File[] files = folder.listFiles(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        try {
                            name = name.toUpperCase();
                            return name.startsWith(("CustomLanguage_" + g_sLangTag + ".PROPERTIES").toUpperCase());
                        } catch (Exception e) {
                            return false;
                        }
                    }
                });

                if (files.length > 0) {
                    oIn = new FileInputStream(files[0]);
                    Properties customProperties = new Properties();
                    customProperties.load(oIn);
                    oIn.close();
                    Enumeration enu = customProperties.keys();
                    String key;
                    while (enu.hasMoreElements()) {
                        key = (String) enu.nextElement();
                        g_oProperties.put(key, customProperties.getProperty(key));
                    }
                    key = null;
                    System.out.println("Custom language: " + files[0].getName() + " loaded");
                }
                //replace special tags

                Enumeration keys = g_oProperties.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    String value = (String) g_oProperties.get(key);

                    if ((value != null) && (value.indexOf("$PATH$") >= 0)) {
                        value = value.replaceAll("\\$PATH\\$", "file:///" + SharedMethods.getWorkingDir());
                        g_oProperties.put(key, value);
                    } else if ((value != null) && (value.indexOf("[PRODUCT_NAME]") >= 0)) {
                        value = value.replaceAll("\\[PRODUCT_NAME\\]", getString("PRODUCT_NAME"));
                        g_oProperties.put(key, value);
                    } else if ((value != null) && (value.indexOf("[PRODUCT_COMPANY_NAME]") >= 0)) {
                        value = value.replaceAll("\\[PRODUCT_COMPANY_NAME\\]", getString("PRODUCT_COMPANY_NAME"));
                        g_oProperties.put(key, value);
                    }
                }
            } catch (Exception e) {

                // custom file may not be available

                System.out.println("Custom Language file not loaded");

            }


        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private static void filterProp() {
        Properties tags = new Properties();

        try {
            FileOutputStream log = new FileOutputStream("FilteredChart.txt");
            tags.load(new FileInputStream("ChartTags.txt"));
            Enumeration e = tags.keys();
            while (e.hasMoreElements()) {
                String tag = (String) e.nextElement();
                String value = g_oProperties.getProperty(tag);
                try {
                    String[] values = value.split("\\|");
                    //System.out.println(g_oProperties.getProperty((String)e.nextElement()));
                    value = values[0];
                    if (values.length > 1) {
                        value += "|";
                        value += UnicodeUtils.getUnicodeString(values[1]);
                    }
                    log.write((tag + "=" + value + "\r\n").getBytes());
                } catch (Exception e1) {
                    log.write((tag + "=" + UnicodeUtils.getUnicodeString(value) + "\r\n").getBytes());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

    private static void trimLanguageTable() {
        Enumeration keys = g_oProperties.keys();
        String key;
        while (keys.hasMoreElements()) {
            key = (String) keys.nextElement();
            if (key.equals("LANGUAGES")) continue;
            g_oProperties.put(key, getLanguageSpecificString(g_oProperties.getProperty(key), "|"));
        }
        key = null;
        keys = null;
    }

    public static String getLanguageTag() {
        return g_sLangTag;
    }

    /**
     * Sets the currently selected language for the system
     */
    public static void setLanguage(String sLanguage) {
        String sElement = null;
        g_sLangTag = sLanguage;
        g_sLanguageCap = languages.get(sLanguage);
        loadLanguage();
//        sLanguage = (String) g_oProperties.get("LANGUAGES");
//        g_iLangID = -1;
//        g_sLanguageCap = "";
//
//        StringTokenizer oTokens = new StringTokenizer(sLanguage, "|");
////        g_iLangCount = (oTokens.countTokens() / 2);
//
//        int i = 0;
////        g_saLanguageCaptions = new String[g_iLangCount];
////        g_saLanguageIDs = new String[g_iLangCount];
//
//        while (oTokens.hasMoreElements()) {
//            sElement = oTokens.nextToken();
//            g_sLanguageCap = oTokens.nextToken();
////            g_saLanguageIDs[i] = sElement;
////            g_saLanguageCaptions[i] = g_sLanguageCap;
//            if (sElement.equals(g_sLanguage)) {
//                g_iLangID = i;
//                g_sLangTag = sElement;
//            }
//            i++;
//        }
//        if (g_iLangID == -1) {
//            g_iLangID = 0;
//            g_sLanguageCap = "English";
//            g_sLangTag = "EN";
//
//        }

//        trimLanguageTable();
//        System.out.println("setServerLanguages =="+serverLanguage);
        setServerLanguageList(serverLanguage);
        loadLanguageDefaults();
        setDirection();
        if (isLTR()) {
            Constants.OUTLOOK_MENU_SIDE = 1;
        } else {
            Constants.OUTLOOK_MENU_SIDE = -1;
        }
    }

    private static void loadLanguageDefaults() {
        UIManager.put("OptionPane.okButtonText", Language.getString("OK"));
        UIManager.put("OptionPane.noButtonText", Language.getString("NO"));
        UIManager.put("OptionPane.cancelButtonText", Language.getString("CANCEL"));
        UIManager.put("OptionPane.yesButtonText", Language.getString("YES"));
    }

    public static String getString(String sID) {
        String sString = null;

        sString = g_oProperties.getProperty(sID);
//        try {
//            if ((sString != null) && (sString.indexOf("$PATH$") >= 0)) {
//                sString = sString.replaceAll("\\$PATH\\$", "file:///" + SharedMethods.getWorkingDir());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        if (sString == null) {
            return "";
        } else {
            return sString;
        }
    }

    public static void putString(String id, String title) {
        g_oProperties.setProperty(id, title);
    }

    public static String[] getList(String sID) {
        String sCols = "";
        String[] saCols = null;
        int i = 0;
//        System.out.println("sID =="+sID);
        sCols = g_oProperties.getProperty(sID);
        try {
            StringTokenizer oTokens = new StringTokenizer(sCols, ",");
            saCols = new String[oTokens.countTokens()];

            i = 0;
            while (oTokens.hasMoreTokens()) {
                saCols[i] = oTokens.nextToken().trim();
                if (saCols[i].equals(".")) {
                    saCols[i] = "";
                }
                i++;
            }
            oTokens = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saCols;

    }

    public static String[] getListx(String sID) {
        String sCols = "";
        String[] saCols = null;
        int i = 0;

        sCols = g_oProperties.getProperty(sID);
        StringTokenizer oTokens = new StringTokenizer(sCols, ",");
        saCols = new String[oTokens.countTokens()];

        i = 0;
        while (oTokens.hasMoreTokens()) {
            saCols[i] = oTokens.nextToken();
            if (saCols[i].equals(".")) {
                saCols[i] = "";
            }
            i++;
        }
        oTokens = null;
        return saCols;
    }

    /**
     * Returns the selected language caption
     */
    public static String getLanguageCaption() {
        return g_sLanguageCap;
    }

    /**
     * Returns the selected language id
     */
    public static int getLanguageID() {
        if (selectedLanguageID == -1) {
            selectedLanguageID = defaultLanguageID;
        }
        return selectedLanguageID;
    }

    /**
     * Returns number of available languages
     */
    public static int getLanguageCount() {
        return g_iLangCount;
    }

    private static void setDirection() {
        if (getString("LANGUAGE_DIRECTIONS").equals("0"))
            ltr = true;
        else
            ltr = false;
    }

    /**
     * Checks whether the alignment is LTR or RTL
     */
    public static boolean isLTR() {
        return ltr;
    }

    /**
     * Returns an array of language captions
     */
    public static String[] getLanguageCaptions() {
        return g_saLanguageCaptions;
    }

    /**
     * Returns an array of start messages captions
     */
    public static String[] getStartMessages() {
        return g_startMessages;
    }

    /**
     * Returns an array of language IDs
     */
    public static String[] getLanguageIDs() {
        return g_saLanguageIDs;
    }

    /**
     * Returns the id of the selected language
     */
    public static String getSelectedLanguage() {
        return g_sLangTag;
    }

    public static String getLanguageSpecificString(String sValue) {
        return getLanguageSpecificString(sValue, "|");
    }

    public static String getEnglishString(String sValue) {
        return getFirstString(sValue, "|");
    }

    /**
     * Returns the portion of the string which is ued by
     * the selected language
     */
    public static String getLanguageSpecificString(String sValue, String sSeperator) {
        String sEngValue = "";
        String sLangValue = "";

        try {
            String[] langs = sValue.split("\\" + sSeperator);
            sEngValue = langs[defaultLanguageID];
            sLangValue = UnicodeUtils.getNativeString(langs[selectedLanguageID]);
            return sLangValue;
        } catch (Exception e) {
            return sEngValue;
        }

    }

    public static String getFirstString(String sValue, String sSeperator) {
        String sEngValue = "";

        try {
            String[] langs = sValue.split("\\" + sSeperator);
            sEngValue = langs[defaultLanguageID];
            return sEngValue;
        } catch (Exception e) {
            return "N/A";
        }

    }
}

