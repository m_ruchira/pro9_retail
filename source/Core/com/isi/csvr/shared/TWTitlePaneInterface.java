package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 14, 2005
 * Time: 3:13:53 PM
 */
public interface TWTitlePaneInterface {

    public boolean isColumnResizeable();

    public boolean isPrintable();

    public void printContents();

    public void showServices();

    public boolean isServicesEnabled();

    public boolean isDetachable();

    public boolean isLinkGroupEnabled();

    public void showLinkGroup();

    public String getLinkGroupIDForTitleBar();

}
