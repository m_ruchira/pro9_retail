package com.isi.csvr.shared;

import java.awt.*;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Feb 12, 2008
 * Time: 3:07:05 PM
 */
public class TWFont extends Font {

    private static final Hashtable<String, String> fontTable = new Hashtable<String, String>();

    public TWFont(String name, int style, int size) {
        this(name, style, size, null);
    }

    public TWFont(String name, int style, int size, String test) {
        super(name, style, size);
        this.name = validateDefaultFont(name, test);
    }

    private String validateDefaultFont(String name, String test) {
        synchronized (fontTable) {
            String getName = fontTable.get(name);
            if (getName != null) {
                return getName;
            }
            Font fontP = new Font(name, Font.PLAIN, 12);
            Font fontB = new Font(name, Font.BOLD, 12);
            if (test == null) {
                test = Language.getString("LANG");
            }
            if (!((fontP.canDisplayUpTo(test) == -1) && (fontB.canDisplayUpTo(test) == -1))) {
                /* Default font is not suitable */
                fontP = new Font("Dialog", Font.PLAIN, 12);
                fontB = new Font("Dialog", Font.BOLD, 12);
                if (((fontP.canDisplayUpTo(test) == -1) && (fontB.canDisplayUpTo(test) == -1))) {
                    fontTable.put(name, "Dialog");
                    return "Dialog";
                } else {
                    fontTable.put(name, "sansserif");
                    return "sansserif";
                }
            } else {
                fontTable.put(name, name);
                return name;
            }
        }
    }
}
