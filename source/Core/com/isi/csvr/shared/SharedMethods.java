package com.isi.csvr.shared;

import bsh.Interpreter;
import com.isi.csvr.Client;
import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.Mubasher;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.announcement.SearchedAnnouncementStore;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.datastore.*;
import com.isi.csvr.ie.BrowserManager;
import com.isi.csvr.news.News;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.news.SearchedNewsStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCFileFilter;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trade.Trade;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.datastore.RuleManager;
import com.isi.csvr.util.MD5;
import com.mubasher.google.GoogleLanguage;
import com.mubasher.google.GoogleUpdator;
import com.mubasher.google.Translater;

import javax.net.ssl.SSLSocket;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.*;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.*;
import java.security.AccessController;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivilegedAction;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;

import static javax.swing.JOptionPane.*;

public class
        SharedMethods {

    public static final TWDecimalFormat[] decimals = {new TWDecimalFormat(" ###,##0 "),
            new TWDecimalFormat(" ###,##0.0 "),
            new TWDecimalFormat(" ###,##0.00 "),
            new TWDecimalFormat(" ###,##0.000 "),
            new TWDecimalFormat(" ###,##0.0000 "),
            new TWDecimalFormat(" ###,##0.00000 "),
            new TWDecimalFormat(" ###,##0.000000 "),
            new TWDecimalFormat(" ###,##0.0000000 "),
            new TWDecimalFormat(" ###,##0.00000000 "),
            new TWDecimalFormat(" ###,##0.000000000 "),
            new TWDecimalFormat(" ###,##0.0000000000 ")};
    /*private static DecimalFormat decimalFormat0 = new DecimalFormat("#,##0");
    private static DecimalFormat decimalFormat1 = new DecimalFormat("#,##0.0");
    private static DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");
    private static DecimalFormat decimalFormat3 = new DecimalFormat("#,##0.000");
    private static DecimalFormat decimalFormat4 = new DecimalFormat("#,##0.0000");*/
    public static final TWDecimalFormat[] decimalsNoComma = {new TWDecimalFormat(" ##0 "),
            new TWDecimalFormat(" ##0.0 "),
            new TWDecimalFormat(" ##0.00 "),
            new TWDecimalFormat(" ##0.000 "),
            new TWDecimalFormat(" ##0.0000 "),
            new TWDecimalFormat(" ##0.00000 "),
            new TWDecimalFormat(" ##0.000000 "),
            new TWDecimalFormat(" ##0.0000000 "),
            new TWDecimalFormat(" ##0.00000000 "),
            new TWDecimalFormat(" ##0.000000000 "),
            new TWDecimalFormat(" ##0.0000000000 ")};
    public static final TWDecimalFormat[] decimalsNoZero = {new TWDecimalFormat(" ###,##0 "),
            new TWDecimalFormat(" ###,##0.# "),
            new TWDecimalFormat(" ###,##0.## "),
            new TWDecimalFormat(" ###,##0.### "),
            new TWDecimalFormat(" ###,##0.#### "),
            new TWDecimalFormat(" ###,##0.##### "),
            new TWDecimalFormat(" ###,##0.###### "),
            new TWDecimalFormat(" ###,##0.####### "),
            new TWDecimalFormat(" ###,##0.######## "),
            new TWDecimalFormat(" ###,##0.######### "),
            new TWDecimalFormat(" ###,##0.########## ")};
    public static final TWDecimalFormat[] decimalsNoZeroNoComma = {new TWDecimalFormat(" ##0 "),
            new TWDecimalFormat(" ##0.# "),
            new TWDecimalFormat(" ##0.## "),
            new TWDecimalFormat(" ##0.### "),
            new TWDecimalFormat(" ##0.#### "),
            new TWDecimalFormat(" ##0.##### "),
            new TWDecimalFormat(" ##0.###### "),
            new TWDecimalFormat(" ##0.####### "),
            new TWDecimalFormat(" ##0.######## "),
            new TWDecimalFormat(" ##0.######### "),
            new TWDecimalFormat(" ##0.########## ")};
    public static final TWDecimalFormat[] decimalsNoZeroNoCommaNoSpace = {new TWDecimalFormat("##0"),
            new TWDecimalFormat("##0.#"),
            new TWDecimalFormat("##0.##"),
            new TWDecimalFormat("##0.###"),
            new TWDecimalFormat("##0.####"),
            new TWDecimalFormat("##0.#####"),
            new TWDecimalFormat("##0.######"),
            new TWDecimalFormat("##0.#######"),
            new TWDecimalFormat("##0.########"),
            new TWDecimalFormat("##0.#########"),
            new TWDecimalFormat("##0.##########")};
    public static final TWDecimalFormat[] decimalsNoCommaNoSpace = {new TWDecimalFormat("##0"),
            new TWDecimalFormat("##0.0"),
            new TWDecimalFormat("##0.00"),
            new TWDecimalFormat("##0.000"),
            new TWDecimalFormat("##0.0000"),
            new TWDecimalFormat("##0.00000"),
            new TWDecimalFormat("##0.000000"),
            new TWDecimalFormat("##0.0000000"),
            new TWDecimalFormat("##0.00000000"),
            new TWDecimalFormat("##0.000000000"),
            new TWDecimalFormat("##0.0000000000")};
    public static SimpleDateFormat TRADE_DATE_FORMAT = new SimpleDateFormat("HHmmss");
    public static Properties sortProp;
    private static long serialID = java.lang.System.currentTimeMillis();
    /*private static DecimalFormat decimal0 = new DecimalFormat("##0");
    private static DecimalFormat decimal1 = new DecimalFormat("##0.0");
    private static DecimalFormat decimal2 = new DecimalFormat("##0.00");
    private static DecimalFormat decimal3 = new DecimalFormat("##0.000");
    private static DecimalFormat decimal4 = new DecimalFormat("##0.0000");*/
    private static DecimalFormat decimal5 = new DecimalFormat("#.####");
    //    private static DecimalFormat longDecimalFormat = new DecimalFormat("#,##0.0000");
    private static DecimalFormat integerFormat = new DecimalFormat("#,##0");
    private static SimpleDateFormat expirationDateFormat = null;
    private static SimpleDateFormat fullDateFormat;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    private static SimpleDateFormat metaStockDate = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat metaStockTime = new SimpleDateFormat("HH:mm:ss");
    private static DecimalFormat heapFormat;
    private static DecimalFormat utilFormat;
    private static SimpleDateFormat timeFormat;
    private static TWDateFormat displayDateFormat = new TWDateFormat("dd/MM/yyyy");
    private static PrintStream out = null;

    public static int intValue(String sValue, int defaultValue) {
        try {
            return (Integer.parseInt(sValue.trim()));
        } catch (Exception e) {
            return -1;
        }
    }

    public static int intValue(String sValue) throws Exception {
//        	return parseInt(sValue.trim().toCharArray(),0,sValue.trim().length(),10);
        return (Integer.parseInt(sValue.trim()));
    }

    public static long longValue(String sValue, long defaultValue) {
        try {
            return (Long.parseLong(sValue.trim()));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static long longValue(String sValue) {
        return (Long.parseLong(sValue.trim()));
    }

    public static float floatValue(String sValue, float defaultValue) {
        try {
            return (Float.parseFloat(sValue.trim()));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static float floatValue(String sValue) {
        return (Float.parseFloat(sValue.trim()));
    }

    public static double doubleValue(String sValue, int defaultValue) {
        try {
            return (Double.parseDouble(sValue.trim()));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static double doubleValue(String sValue) {
        return (Double.parseDouble(sValue.trim()));
    }

    public static boolean booleanValue(String sValue, boolean defaultValue) {
        try {
            return (sValue.trim().equalsIgnoreCase("true"));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Font fontObject(String sFont, Font defaultValue) {
        StringTokenizer oTokens = new StringTokenizer(sFont, ",");
        try {
            return new TWFont(oTokens.nextToken(),
                    intValue(oTokens.nextToken()), intValue(oTokens.nextToken()));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static int[][] getColumnSettings(String sSettings) {
        StringTokenizer oTokens = new StringTokenizer(sSettings, ",");
        if ((oTokens.countTokens() % 3) == 0) {
            int iCount = (int) (oTokens.countTokens() / 3);
            int[][] aiColumnSettings = new int[iCount][3];

            for (int i = 0; i < iCount; i++) {
                aiColumnSettings[i][0] = intValue(oTokens.nextToken(), 0);
                aiColumnSettings[i][1] = intValue(oTokens.nextToken(), 0);
                aiColumnSettings[i][2] = intValue(oTokens.nextToken(), 0);
            }
            return aiColumnSettings;
        } else {
            return null; // to do: safety code
        }
    }

    public static void showInternalFrameMessage(String message, int type) {
        switch (type) {
            case WARNING_MESSAGE:
                showInternalMessageDialog(Client.getInstance().getDesktop(), message, Language.getString("WARNING"), WARNING_MESSAGE);
                break;
            case INFORMATION_MESSAGE:
                showInternalMessageDialog(Client.getInstance().getDesktop(), message, Language.getString("INFORMATION"), WARNING_MESSAGE);
                break;
            case ERROR_MESSAGE:
                showInternalMessageDialog(Client.getInstance().getDesktop(), message, Language.getString("ERROR"), WARNING_MESSAGE);
                break;
        }
    }

    public static void showMessage(String message, int type) {
        switch (type) {
            case WARNING_MESSAGE:
                showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("WARNING"), WARNING_MESSAGE);
                break;
            case INFORMATION_MESSAGE:
                showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("INFORMATION"), INFORMATION_MESSAGE);
                break;
            case ERROR_MESSAGE:
                showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("ERROR"), ERROR_MESSAGE);
                break;
        }
    }

    public static void showHTMLMessage(String title, String message, int messageType, int optionType) {
        // Turn anti-aliasing on
        System.setProperty("awt.useSystemAAFontSettings", "on");
        final JEditorPane editorPane = new JEditorPane();

        // Enable use of custom set fonts
        editorPane.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, Boolean.TRUE);
        editorPane.setOpaque(false);

        editorPane.setEditable(false);
        editorPane.setContentType("text/html");
        editorPane.setText(message);

        // TIP: Make the JOptionPane resizable using the HierarchyListener
        editorPane.addHierarchyListener(new HierarchyListener() {
            public void hierarchyChanged(HierarchyEvent e) {
                Window window = SwingUtilities.getWindowAncestor(editorPane);
                if (window instanceof Dialog) {
                    final Dialog dialog = (Dialog) window;
                    if (!dialog.isResizable()) {
                        dialog.setResizable(true);
                        dialog.addWindowListener(new WindowAdapter() {
                            public void windowOpened(WindowEvent e) {
                                dialog.pack();

                                Dimension size = dialog.getSize();
                                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                                int width, height;
                                if (size.getWidth() > screen.getWidth()) {
                                    width = (int) (screen.getWidth() * .75);
                                } else {
                                    width = (int) (size.getWidth());
                                }
                                if (size.getHeight() >= screen.getHeight()) {
                                    height = (int) (screen.getHeight() * .85);
                                } else {
                                    height = (int) (size.getHeight());
                                }
                                dialog.setSize(width + 20, height); // 20 for the scroll bar : java bug
                                dialog.setLocationRelativeTo(Client.getInstance().getFrame());
                            }
                        });
                    }

                }
            }
        });

        // TIP: Add Hyperlink listener to process hyperlinks
        editorPane.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(final HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            // TIP: Show hand cursor
                            SwingUtilities.getWindowAncestor(editorPane).setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                            // TIP: Show URL as the tooltip
                            editorPane.setToolTipText(e.getURL().toExternalForm());
                        }
                    });
                } else if (e.getEventType() == HyperlinkEvent.EventType.EXITED) {
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            // Show default cursor
                            SwingUtilities.getWindowAncestor(editorPane).setCursor(Cursor.getDefaultCursor());

                            // Reset tooltip
                            editorPane.setToolTipText(null);
                        }
                    });
                } else if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    // TIP: Starting with JDK6 you can show the URL in desktop browser
                    if (Desktop.isDesktopSupported()) {
                        try {
                            Desktop.getDesktop().browse(e.getURL().toURI());
                        } catch (Exception ex) {
                        }
                    }
                    //System.out.println("Go to URL: " + e.getURL());
                }
            }
        });

        JOptionPane optionPane = new JOptionPane();
        JScrollPane scrollPane = new JScrollPane(editorPane);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        optionPane.setMessage(scrollPane);
        optionPane.setOptionType(optionType);

        if ((title == null) || (title.trim().equals(""))) {
            switch (messageType) {
                case WARNING_MESSAGE:
                    optionPane.setMessageType(WARNING_MESSAGE);
                    title = Language.getString("WARNING");
                    break;
                case INFORMATION_MESSAGE:
                    optionPane.setMessageType(INFORMATION_MESSAGE);
                    title = Language.getString("INFORMATION");
                    break;
                case PLAIN_MESSAGE:
                    optionPane.setMessageType(PLAIN_MESSAGE);
                    title = Language.getString("INFORMATION");
                    break;
                case ERROR_MESSAGE:
                    optionPane.setMessageType(ERROR_MESSAGE);
                    title = Language.getString("ERROR");
                    break;
            }
        }

        GUISettings.applyOrientation(optionPane);
        JDialog dialog = optionPane.createDialog(Client.getInstance().getFrame(), title);
        dialog.setVisible(true);
    }

    public static int showConfirmMessage(String message, int type) {
        return showConfirmMessage(message, type, OK_CANCEL_OPTION);
    }

    public static int showOrderConfirmationMessage(Object[] objects, int type) {
        /*JOptionPane optionPane = new JOptionPane(objects, PLAIN_MESSAGE, OK_CANCEL_OPTION, null);
        JDialog dialog = optionPane.createDialog(null, Language.getString("INFORMATION"));
        dialog.setIconImage(null);
        dialog.setVisible(true);
        return ((Integer)optionPane.getValue()).intValue();*/
        //Object[] options = new Object[]{Language.getString("CONFIRM"), Language.getString("CANCEL")};
        Object[] options = new Object[]{Language.getString("YES"), Language.getString("NO")};
        return JOptionPane.showOptionDialog(Client.getInstance().getFrame(), objects[0], Language.getString("INFORMATION"), OK_CANCEL_OPTION, PLAIN_MESSAGE, null, options, null);
    }

    public static int showConfirmMessage(String message, int type, int ButtonType) {
        switch (type) {
            case WARNING_MESSAGE:
                return showConfirmDialog(Client.getInstance().getFrame(), message, Language.getString("WARNING"),
                        ButtonType, WARNING_MESSAGE);
            case INFORMATION_MESSAGE:
                return showConfirmDialog(Client.getInstance().getFrame(), message, Language.getString("INFORMATION"),
                        ButtonType, INFORMATION_MESSAGE);
            case ERROR_MESSAGE:
                return showConfirmDialog(Client.getInstance().getFrame(), message, Language.getString("ERROR"),
                        ButtonType, WARNING_MESSAGE);
        }
        return CANCEL_OPTION;
    }

    public static Point getPoint(String value) {
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(",");
        pair.setData(value);
        try {
            return new Point(intValue(pair.getTag()), intValue(pair.getData()));
        } catch (Exception e) {
            e.printStackTrace();
            return new Point(0, 0);
        }
    }

    public static Dimension getDimension(String value) {
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(",");
        pair.setData(value);
        try {
            return new Dimension(intValue(pair.getTag()), intValue(pair.getData()));
        } catch (Exception e) {
            return new Dimension(0, 0);
        }
    }

    public static String[] objectToStringArray(Object[] objectArray) {
        String[] stringArray = new String[objectArray.length];
        for (int i = 0; i < objectArray.length; i++) {
            stringArray[i] = (String) objectArray[i];
        }
        return stringArray;
    }

    public static boolean isLeftToRight(Component c) {
        return c.getComponentOrientation().isLeftToRight();
    }

    public static void printLine(String message) {
        System.out.println(message);
//        printLine(message, true);
    }

    public static void printLine(String message, boolean doTrace) {
        if (Mubasher.DEBUG) {
            try {
                if (doTrace) {
                    System.out.println("Debug Trace : " + message);
                    StackTraceElement[] trace = new Throwable().getStackTrace();
                    for (int i = 0; i < trace.length; i++) {
                        System.out.println("\t at " + trace[i]);
                    }
                    trace = null;
                } else {
                    System.out.println(message);
                }
            } catch (Exception e) {
                System.out.println(message);
            }
        } else {
            System.out.println(message);
        }
    }

    public static boolean isInCallTree(Class cls) {
        StackTraceElement[] trace = new Throwable().getStackTrace();
        for (StackTraceElement element : trace) {
            if (element.getClassName().equals(cls.getName())) {
                return true;
            }
        }
        return false;
    }

    public static void centerWindow(JDialog window) {
        GUISettings.setLocationRelativeTo(window, Client.getInstance().getFrame());
//        Toolkit oToolkit = Toolkit.getDefaultToolkit();
//        Dimension oDm = oToolkit.getScreenSize();
//        window.setBounds((int) (oDm.getWidth() / 2 - (window.getWidth() / 2)),
//                (int) (oDm.getHeight() / 2 - (window.getHeight() / 2)),
//                window.getWidth(), window.getHeight());
    }

    public static void centerWindow(JFrame window) {
        GUISettings.setLocationRelativeTo(window, Client.getInstance().getFrame());
//        Toolkit oToolkit = Toolkit.getDefaultToolkit();
//        Dimension oDm = oToolkit.getScreenSize();
//        window.setBounds((int) (oDm.getWidth() / 2 - (window.getWidth() / 2)),
//                (int) (oDm.getHeight() / 2 - (window.getHeight() / 2)),
//                window.getWidth(), window.getHeight());
    }

    /*public static long getTradeTime(String exchange String time) {
        try {
            return Client.tradeDateFormat.parse(time).getTime() + MarketStore.getSharedInstance().getMarket();
        } catch (Exception e) {
            return 0;
        }

    }*/

    public static synchronized long getSerialID() {
        return serialID++;
    }

    public static void printTable(JTable table, int type, String title) {
        Object[] objects = new Object[1];
        objects[0] = table;
        printTable(objects, type, title);
    }

    public static void printTable(JTable table, int type, String title, String[][] topString) {
        Object[] objects = new Object[1];
        objects[0] = table;
        printTable(objects, type, title, topString);
    }

    public static void printTable(Object[] objects, int type, String title, String[][] topString) {
        boolean isPreviewAvailable = false;
        PrintManager.getSharedInstance().setParams(objects, title, type, true);
        PrintManager.getSharedInstance().initializeComponents(isPreviewAvailable);
        PrintManager.getSharedInstance().setTopString(topString);
        if (!isPreviewAvailable)
            PrintManager.getSharedInstance().print();
    }

    /*public static double getDouble(String data) {
        if (data == null)
            return -1;
        int pos = data.indexOf(".");
        if (pos < 0)
            return getLong(data);
        double value = 0;
        if (pos > 0)
            value = getLong(data.substring(0, pos));
        data = data.substring(pos + 1);
        value = (getLong(data)/(float)100) + value;
        return value;
    }*/

    public static void printTable(Object[] objects, int type, String title) {
        boolean isPreviewAvailable = false;
        PrintManager.getSharedInstance().setParams(objects, title, type, true);
        PrintManager.getSharedInstance().initializeComponents(isPreviewAvailable);
        if (!isPreviewAvailable)
            PrintManager.getSharedInstance().print();
    }

    // new

    public static void printPFTable(Object[] objects, int type, String title) {
        boolean isPreviewAvailable = false;
        PrintManager.getSharedInstance().setParams(objects, title, type, false);
        PrintManager.getSharedInstance().initializeComponents(isPreviewAvailable);
        if (!isPreviewAvailable)
            PrintManager.getSharedInstance().print();
    }

    /*public static String get128String(double value){
        long    ineger;
        long    fraction;
        boolean isNegative;
        if (value < 0){
            isNegative  = true;
            value       = Math.abs(value);
        }
        else{
            isNegative = false;
        }

        ineger      = (long) Math.floor(value);
        value       = (value - ineger) * 1000;
        if ((value % 10) < 1){ // 2 decimal mumber
            value /= 10;
        }
        fraction    =  Math.round(value);
        if (fraction == 0)
            return (get128String(ineger,isNegative));
        if (ineger == 0)
            return "." + get128String(fraction,isNegative);
        else
            return (get128String(ineger,isNegative) + "." + get128String(fraction));
    }*/

    public static Color[] getChartColors() {
        Color[] ca = {new Color(213, 122, 91), new Color(116, 156, 237),
                new Color(14, 168, 71), new Color(214, 90, 79),
                new Color(121, 113, 153), new Color(83, 38, 143),
                new Color(00, 74, 99), new Color(97, 214, 219),
                new Color(43, 35, 35), new Color(196, 156, 115),
                new Color(247, 115, 173), new Color(155, 131, 128),
                new Color(117, 208, 23), new Color(02, 132, 85),
                new Color(255, 132, 00), new Color(97, 42, 25),
                new Color(74, 201, 234), new Color(99, 00, 49),
                new Color(00, 00, 66), new Color(74, 201, 234)
        };
        return ca;
    }

    public static Font getSymbolFont(String filePath, int size) {
        Font scaledFont = null;
        try {
            FileInputStream fileStream = new FileInputStream(filePath); //"fonts/wingding.ttf"
            Font font = Font.createFont(Font.TRUETYPE_FONT, fileStream);
            scaledFont = font.deriveFont(Font.PLAIN, size);
        } catch (Exception ex) {
            return Theme.getDefaultFont(Font.PLAIN, size);
        }
        return scaledFont;
    }

    // new Mthod

    public static int parseInt(char[] data, int offset, int len, int radix) {
        if (data == null) {
            throw new NumberFormatException("null");
        }

        if (radix < Character.MIN_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " less than Character.MIN_RADIX");
        }

        if (radix > Character.MAX_RADIX) {
            throw new NumberFormatException("radix " + radix +
                    " greater than Character.MAX_RADIX");
        }

        int result = 0;
        boolean negative = false;
        int i = offset, max = len;
        int limit;
        int multmin;
        int digit;

        if (max > 0) {
            if (data[i] == '-') { // 0th pos
                negative = true;
                limit = Integer.MIN_VALUE;
                i++;
            } else {
                limit = -Integer.MAX_VALUE;
            }
            multmin = limit / radix;
            if (i < max) {
                digit = Character.digit(data[i++], radix);
                if (digit < 0) {
                    throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
                } else {
                    result = -digit;
                }
            }
            while (i < max) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                digit = Character.digit(data[i++], radix);
                if (digit < 0) {
                    throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
                }
                if (result < multmin) {
                    throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
                }
                result *= radix;
                if (result < limit + digit) {
                    throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
                }
                result -= digit;
            }
        } else {
            throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
        }
        if (negative) {
            if (i > 1) {
                return result;
            } else {    /* Only got "-" */
                throw new NumberFormatException("For input string: \"" + new String(data, offset, len) + "\"");
            }
        } else {
            return -result;
        }
    }

    // old method

    public static long getDateLongFromDateString(String date) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(date.substring(4, 6)) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6, 8)));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime();
    }

    public static long getTimeLongFromTimeString(String date) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1970);
        cal.set(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(date.substring(0, 2)));
        cal.set(Calendar.MINUTE, Integer.parseInt(date.substring(2, 4)));
        cal.set(Calendar.SECOND, Integer.parseInt(date.substring(4, 6)));
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().getTime();
    }

    public static long getLong(String data) {
        if (data == null)
            return -1;
        boolean isNegative = false;
        if (data.charAt(0) == '-') {
            data = data.substring(1);
            isNegative = true;
        }
        byte[] bData = data.getBytes();
        long value = 0;
        for (int i = 0; i < bData.length; i++) {
            if (i == 0)
                value = decode(bData[i]);
            else {
                value = value + (decode(bData[i]) * (long) Math.pow(64, i));
            }
        }
        if (isNegative)
            return (0 - value);
        else
            return value;
    }

    public static double getFraction(double data) {
        boolean status = false;
        if (data < 10)
            status = true;
        while (data > 1) {
            data = data / 10;
        }
        if (status)
            data = data / 10;
        return data;
    }

    public static String get128StringTest(double value) {
        long ineger;
        long fraction;
        boolean isNegative;
        if (value < 0) {
            isNegative = true;
            value = Math.abs(value);
        } else {
            isNegative = false;
        }

        ineger = (long) Math.floor(value);
        value = (value - ineger) * 1000;
        if ((value % 10) < 1) { // 2 decimal mumber
            value /= 10;
        }
        fraction = Math.round(value);
        if (fraction == 0)
            return (get128String(ineger, isNegative));
        if (ineger == 0)
            return "." + get128String(fraction, isNegative);
        else
            return (get128String(ineger, isNegative) + "." + get128String(fraction));
    }

    public static String get128String(double value) {
        long ineger;
        long fraction;
        boolean isNegative;
        if (value < 0) {
            isNegative = true;
            value = Math.abs(value);
        } else {
            isNegative = false;
        }

        ineger = (long) Math.floor(value) * 1000;
        value = Math.round(value * 1000) - ineger;
        if ((value % 10) < 1) { // 2 decimal mumber
            value /= 10;
        }
        fraction = Math.round(value);
        if (fraction == 0)
            return (get128String(ineger / 1000, isNegative));
        if (ineger == 0)
            return "." + get128String(fraction, isNegative);
        else
            return (get128String(ineger / 1000, isNegative) + "." + get128String(fraction));
    }

    public static String get128String(long volume, boolean isNegative) {
        byte[] b = new byte[20];
        byte x;
        int count = 0;
        while (volume >= 64) {
            x = (byte) (volume % 64);
            volume = (volume - x) / 64;
            b[count] = encode(x);
            count++;
        }
        b[count] = encode((byte) volume);
        if (isNegative) {
            return "-" + new String(b, 0, (count + 1));
        } else {
            return new String(b, 0, (count + 1));
        }
    }

    public static byte encode(byte value) {
        if (value >= 64) {
            value = (byte) (value - 128);
        } else {
            value = (byte) (value + 62);
        }
        return value;
    }

    public static double getDoubleNew(String data) {
        if (data == null)
            return -1;
        int pos = data.indexOf(".");
        if (pos < 0)
            return getLong(data);
        double value = 0;
        if (pos > 0)
            value = getLong(data.substring(0, pos));
        data = data.substring(pos + 1);
        long fraction = getLong(data);
//        if (fraction > 99){
        if (value < 0)
            value = value - (getLong(data) / (double) 1000);
        else
            value = value + (getLong(data) / (double) 1000);
        /*}else {
            if (value < 0)
                value = value - (fraction / (double) 100);
            else
                value = value + (fraction / (double) 100);
        }*/
        return value;
    }

    public static double getDouble(String data) {
        if (data == null)
            return -1;
        int pos = data.indexOf(".");
        if (pos < 0)
            return getLong(data);
        double value = 0;
        if (pos > 0)
            value = getLong(data.substring(0, pos));
        data = data.substring(pos + 1);
        //long fraction =
        if (value < 0)
            value = value - (getLong(data) / (double) 100);
        else
            value = value + (getLong(data) / (double) 100);
        return value;
    }

    public static byte decode(byte value) {
        if (value < 0)
            value = (byte) (value + 128);
        else
            value = (byte) (value - 62);

        return value;
    }

    public synchronized static String encrypt(String data) {
        byte[] inData;
        byte[] outData;
        try {
            byte key;
            byte OFFSET = 24;
            int x;
            while (true) {
                key = (byte) Math.round(Math.random() * 96);
                if (key < 32) {
                    key = (byte) (key + 32);
                } else if (key > 64) {
                    key = (byte) (key - 32);
                }
                if (key == 61)
                    continue;
                else
                    break;
            }
            inData = data.getBytes();

            if (inData[0] == 25) // check if already encrypted
                return data;

            inData = UnicodeUtils.getUnicodeString(data).getBytes();

            outData = new byte[inData.length + 2];
            outData[0] = 25;
            outData[1] = key;
            for (int i = 2; i < outData.length; i++) {
                x = (inData[i - 2] + OFFSET + key);
                if (x > 126)
                    x = x - 86;
                outData[i] = (byte) x;
            }
            return new String(outData);
        } catch (Exception e) {
            //e.printStackTrace();
            return data;
        } finally {
            inData = null;
            outData = null;
        }
    }

    public synchronized static String decrypt(String data) {
        byte[] inData;
        byte[] outData;
        try {
            inData = data.getBytes();
            outData = new byte[inData.length - 2];
            byte key;
            byte OFFSET = 24;
            int x;
            if (inData[0] != 25)
                return data;
            key = inData[1];
            for (int i = 2; i < inData.length; i++) {
                x = inData[i] - OFFSET - key;
                if (x < 48)
                    x = x + 86;
                outData[i - 2] = (byte) x;
            }
            inData = null;
            String out = new String(outData);
            return UnicodeUtils.getNativeString(out);
        } catch (Exception e) {
            //e.printStackTrace();
            return "";
        } finally {
            inData = null;
            outData = null;
        }
    }

    public static String toMD5String(String message) {
        MessageDigest md5;
        byte[] digestMsgByte;

        try {
            md5 = MessageDigest.getInstance("MD5");
            digestMsgByte = md5.digest(message.getBytes());

            StringBuffer verifyMsg = new StringBuffer();
            int hexChar;
            for (int i = 0; i < digestMsgByte.length; i++) {
                hexChar = 0xFF & digestMsgByte[i];
                String hexString = Integer.toHexString(hexChar);
                if (hexString.length() == 1) {
                    verifyMsg.append("0");
                    verifyMsg.append(hexString);
                } else {
                    verifyMsg.append(hexString);
                }
                hexString = null;
            }
            return verifyMsg.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            return null;
        } finally {
            md5 = null;
            digestMsgByte = null;
        }
    }

    public static synchronized String getMD5File(String filename) {
        return getMD5File(new File(filename));
    }

    public static synchronized String getMD5File(File file) {
        String md5 = null;
        try {
            md5 = MD5.getHashString(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return md5;
    }

    public static String getSSlSession(SSLSocket socket) {
        byte[] digestMsgByte;

        try {
            digestMsgByte = socket.getSession().getId();

            StringBuffer verifyMsg = new StringBuffer();
            int hexChar;
            for (int i = 0; i < digestMsgByte.length; i++) {
                hexChar = 0xFF & digestMsgByte[i];
                String hexString = Integer.toHexString(hexChar);
                if (hexString.length() == 1) {
                    verifyMsg.append("0");
                    verifyMsg.append(hexString);
                } else {
                    verifyMsg.append(hexString);
                }
                hexString = null;
            }
            return verifyMsg.toString().toUpperCase();
        } catch (Exception e) {
            return null;
        } finally {
            digestMsgByte = null;
        }
    }

    public static byte[] offsetEncrypt(byte[] source, int offset) {

        for (int i = 0; i < source.length; i++) {
            source[i] = (byte) ((source[i] + offset) % 256);
        }

        return source;
    }

    public static byte[] offsetDecrypt(byte[] source, int offset) {

        for (int i = 0; i < source.length; i++) {
            source[i] = (byte) ((source[i] + 256 - offset) % 256);
        }

        return source;
    }

    public static String toDateString(long date) {
        return dateFormat.format(new Date(date));
    }

    public static String toAllTradeString(Trade trade) {
        //Time,Symbol,Price,Volume,Chg.,% Chg.,Ticket Number,Buy Order Number,Sell Order Number,Market,Side
        String sellSide = Language.getString("TRADE_SIDE_SELL");
        String buySide = Language.getString("TRADE_SIDE_BUY");
        Date date = new Date();
        TWDateFormat g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));
        float priceModifFac = ExchangeStore.getSharedInstance().getExchange(trade.getExchange()).getPriceModificationFactor();
        long tradeDate = ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(trade.getExchange(), trade.getTradeTime());
        date.setTime(tradeDate);
        try {
            return g_oTimeFormat.format(date) + "," +
                    trade.getSymbol() + "," +
                    trade.getTradePrice() + "," +
                    trade.getTradeQuantity() + "," +
                    trade.getNetChange() + "," +
                    trade.getPrecentNetChange() + "," +
                    (trade.getTicketNumber() == null ? "" : trade.getTicketNumber()) + "," +
                    (trade.getBuyOrderNumber() == null ? "" : trade.getBuyOrderNumber()) + "," +
                    (trade.getSellOrderNumber() == null ? "" : trade.getSellOrderNumber()) + "," +
                    (trade.getMarketCenter() == null ? "" : trade.getMarketCenter()) + "," +
                    (trade.getSide() == Constants.TRADE_SIDE_BUY ? buySide : trade.getSide() == Constants.TRADE_SIDE_SELL ? sellSide : "") + "," +
                    trade.getTurnover() / priceModifFac;
        } finally {
            date = null;
        }
    }

    public static String toCompanyTradeString(Trade trade) {
        //Time,Symbol,Price,Volume,Chg.,% Chg.,Ticket Number,Buy Order Number,Sell Order Number,Market,Side
        String sellSide = Language.getString("TRADE_SIDE_SELL");
        String buySide = Language.getString("TRADE_SIDE_BUY");
        float priceModifFac = ExchangeStore.getSharedInstance().getExchange(trade.getExchange()).getPriceModificationFactor();
        Date date = new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(trade.getExchange(), trade.getTradeTime()));

        try {
            return formatter.format(date) + "," +
                    trade.getTradePrice() + "," +
                    trade.getTradeQuantity() + "," +
                    trade.getNetChange() + "," +
                    trade.getPrecentNetChange() + "," +
                    (trade.getTicketNumber() == null ? "" : trade.getTicketNumber()) + "," +
                    (trade.getBuyOrderNumber() == null ? "" : trade.getBuyOrderNumber()) + "," +
                    (trade.getSellOrderNumber() == null ? "" : trade.getSellOrderNumber()) + "," +
                    (trade.getMarketCenter() == null ? "" : trade.getMarketCenter()) + "," +
                    (trade.getSide() == Constants.TRADE_SIDE_BUY ? buySide : trade.getSide() == Constants.TRADE_SIDE_SELL ? sellSide : "") + "," +
                    trade.getTurnover() / priceModifFac;
        } finally {
            date = null;
        }
    }

    public static String toMetaStockString(Trade trade) {
        Date date = new Date(trade.getTradeTime());
        StringBuffer buffer = new StringBuffer();

        // "<date>,<time>,<per>,<ticker>,<close>,<vol>"
        buffer.append(metaStockDate.format(date));
        buffer.append(",");
        buffer.append(metaStockTime.format(date));
        buffer.append(",I,");
        buffer.append(trade.getSymbol());
        buffer.append(",");
        buffer.append(trade.getTradePrice());
        buffer.append(",");
        buffer.append(trade.getTradeQuantity());

        try {
            return buffer.toString();
        } finally {
            date = null;
            buffer = null;
        }
    }

    /*public static String getKey(String exchange, String symbol) {
        return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
    }*/

    public static String toMetaStockString(IntraDayOHLC ohlc) {
        Date date = new Date(ohlc.getTime());
        StringBuffer buffer = new StringBuffer();

        // "<date>,<time>,<per>,<ticker>,<close>,<vol>"
        buffer.append(metaStockDate.format(date));
        buffer.append(",");
        buffer.append(metaStockTime.format(date));
        buffer.append(",I,");
        buffer.append(ohlc.getSymbol());
        buffer.append(",");
        buffer.append(ohlc.getClose());
        buffer.append(",");
        buffer.append(ohlc.getVolume());

        try {
            return buffer.toString();
        } finally {
            date = null;
            buffer = null;
        }
    }

    public static long getMarketTime() {
        return Client.getInstance().getMarketTime();
    }

    public static String getExchangeKey(String symbol, int instrument) {
        return symbol + Constants.KEY_SEPERATOR_CHARACTER + instrument;
    }

    /**
     * This method is to get the Symbol from Symbol~Instrument pair
     *
     * @param sKey
     * @return
     */
    public static String getSymbolFromExchangeKey(String sKey) {
        try {
            return sKey.split(Constants.KEY_SEPERATOR_CHARACTER)[0];
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * This method is to get the Instrument from Symbol~Instrument pair
     *
     * @param sKey
     * @return
     */
    public static int getInstrumentFromExchangeKey(String sKey) {
        try {
            return Integer.parseInt(sKey.split(Constants.KEY_SEPERATOR_CHARACTER)[1]);
        } catch (Exception ex) {
            return -1;
        }
    }

    public static String getKey(String exchange, String symbol, int instrument) {
        return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol + Constants.KEY_SEPERATOR_CHARACTER + instrument;
    }

    public static String getTradeKey(String exchange, String symbol, int instrument, String bookKeeper) {
        if ((bookKeeper == null) || bookKeeper.equals("")) {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol + Constants.KEY_SEPERATOR_CHARACTER + instrument;
        } else {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol + Constants.KEY_SEPERATOR_CHARACTER + instrument + Constants.KEY_SEPERATOR_CHARACTER + bookKeeper;
        }
    }

    public static String getTradeKey(String exchange, String symbol, String bookKeeper) {
        if ((bookKeeper == null) || bookKeeper.equals("")) {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
        } else {
            return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol + Constants.KEY_SEPERATOR_CHARACTER + bookKeeper;
        }
    }

    public static String getKey(String sKey) {
        return sKey;
    }

    public static String getSymbolFromKey(String sKey) {
        try {
            return sKey.split(Constants.KEY_SEPERATOR_CHARACTER)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getExchangeFromKey(String sKey) {
        try {
            return sKey.split(Constants.KEY_SEPERATOR_CHARACTER)[0];
        } catch (Exception ex) {
            return null;
        }
    }

    public static int getInstrumentTypeFromKey(String sKey) {
        try {
            return Integer.parseInt(sKey.split(Constants.KEY_SEPERATOR_CHARACTER)[2]);
        } catch (Exception ex) {
            return -1;
        }
    }

    public static String getDisplayKey(String exchange, String symbol) {
        return exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
    }

    public static String getDisplayKey(String key) {
        return getExchangeFromKey(key) + Constants.KEY_SEPERATOR_CHARACTER + getSymbolFromKey(key);
    }

    public static String getExchangeKey(String key) {
        return getSymbolFromKey(key) + Constants.KEY_SEPERATOR_CHARACTER + getInstrumentTypeFromKey(key);
    }

    public static boolean isFullKey(String key) {
        return key.split(Constants.KEY_SEPERATOR_CHARACTER).length == 3;
    }

    public static String getExchWithSubMktKey(String exchange, String marketCode) {
        if (marketCode == null) {
            return exchange;
        } else {
            return exchange + Constants.MARKET_SEPERATOR_CHARACTER + marketCode;
        }
    }

    public static String getExchangeFromExchWithSubMktKey(String key) {
        try {
            return key.split(Constants.MARKET_SEPERATOR_CHARACTER)[0];
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getSubMarketFromExchWithSubMktKey(String key) {
        try {
            return key.split(Constants.MARKET_SEPERATOR_CHARACTER)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getRQuoteKey(String key, String marketCenter) {
        if (marketCenter == null) {
            return key;
        } else {
            String symbol = SharedMethods.getSymbolFromKey(key) + "." + marketCenter;
            return getKey(getExchangeFromKey(key), symbol, getInstrumentTypeFromKey(key));
        }
    }

    public static String getSymbolKeyFromRQuoteKey(String key) {
        try {
            return key.split(Constants.MARKET_CENTER_SEPERATOR_CHARACTER)[0];
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getMktCenterFromRQuoteKey(String key) {
        try {
            return key.split(Constants.MARKET_CENTER_SEPERATOR_CHARACTER)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getRQuoteSymbol(String exchange, String symbol, int instruemnt) {
//        String symbol = null;
        try {
            return getKey(exchange, symbol.split("\\.")[0], instruemnt);
        } catch (Exception ex) {
            return getKey(exchange, symbol, instruemnt);
        }
    }

    public static int booleToInt(boolean boolValue) {
        if (boolValue)
            return 1;
        else
            return 0;
    }

    public static boolean intToBool(String value) {
        int intVal = -1;
        try {
            intVal = Integer.parseInt(value);
        } catch (Exception e) {
            return false;
        }
        if ((intVal <= 0)) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean intToBool(int value) {
        if (value <= 0) {
            return false;
        } else {
            return true;
        }
    }

    public static String toDeciamlFoamat(float value) {
        return decimals[2].format(value);
    }

    public static String toDeciamlFoamat(double value) {
        return decimals[2].format(value);
    }

    public static String toLongDeciamlFoamat(float value) {
        return decimals[4].format(value);
    }

    public static String toLongDeciamlFoamat(double value) {
        return decimals[4].format(value);
    }

    public static String toIntegerFoamat(int value) {
        return integerFormat.format(value);
    }

    public static String toIntegerFoamat(long value) {
        return integerFormat.format(value);
    }

    public static String toFulldateFormat(long date) {
        if (fullDateFormat == null)
            fullDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        return fullDateFormat.format(date);
    }

    public static String getLocalIP() {
        try {
            long time = System.currentTimeMillis();
            System.out.println("Getting local ip ");
            InetAddress local = InetAddress.getLocalHost();
            System.out.println("IP of localhost: " + local.getHostAddress());
            System.out.println("Time " + (System.currentTimeMillis() - time));
            return local.getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "N/A";
        }
    }

    public static long getExpirationDate(String date) {
        if (expirationDateFormat == null) {
            expirationDateFormat = new SimpleDateFormat("MMddyy");
        }
        try {
            return expirationDateFormat.parse(date).getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    /*public static String getSymbol(String key){
        try {
            return key.split(ESPConstants.KEY_SEPERATOR_CHARACTER)[0];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getExchangeSymbol(String key){
        try {
            return key.split(ESPConstants.KEY_SEPERATOR_CHARACTER)[1];
        } catch (Exception e) {
            return null;
        }
    }*/

    public static String readLine(InputStream in) throws Exception {
        int iValue;
        int index = 0;
        byte[] buffer = new byte[1000];

        while (true) {
            iValue = in.read();

            if (iValue == -1) {
                return null;
            }

            if ((iValue != (int) '\n') && (iValue != -1)) {
                buffer[index] = (byte) iValue;
                index++;
            } else {
                String line = new String(buffer, 0, index);
                buffer = null;
                return line;
            }
        }
    }

    public static void copFile(File source, File destination) throws Exception {
        FileInputStream sourceIn = new FileInputStream(source);
        FileOutputStream destinationOut = new FileOutputStream(destination);
        byte[] data = new byte[1000];
        int len = 1;

        while (len > 0) {
            len = sourceIn.read(data);
            if (len > 0) {
                destinationOut.write(data, 0, len);
            }
        }

        destinationOut.flush();
        destinationOut.close();
        sourceIn.close();

        destinationOut = null;
        sourceIn = null;
        data = null;
    }

    public static void showNonBlockingMessage(final Object message, final int type) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if ((type & JOptionPane.INFORMATION_MESSAGE) == JOptionPane.INFORMATION_MESSAGE) {
                    JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("INFORMATION"), type);
                } else if ((type & JOptionPane.WARNING_MESSAGE) == JOptionPane.WARNING_MESSAGE) {
                    JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("WARNING"), type);
                } else if ((type & JOptionPane.ERROR_MESSAGE) == JOptionPane.ERROR_MESSAGE) {
                    JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("ERROR"), type);
                }
            }
        });

    }

    public static void showBlockingMessage(final String message, final int type) {
        if ((type & JOptionPane.INFORMATION_MESSAGE) == JOptionPane.INFORMATION_MESSAGE) {
            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("INFORMATION"), type);
        } else if ((type & JOptionPane.WARNING_MESSAGE) == JOptionPane.WARNING_MESSAGE) {
            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("WARNING"), type);
        } else if ((type & JOptionPane.ERROR_MESSAGE) == JOptionPane.ERROR_MESSAGE) {
            JOptionPane.showMessageDialog(Client.getInstance().getFrame(), message, Language.getString("ERROR"), type);
        }
    }

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static synchronized int getStockBulbStatus(int newsAvailability, int announcementAvailability) {
//        switch (announcementAvailability) {
//            case Constants.ANNOUCEMENT_NOT_AVAILABLE:
//                switch (newsAvailability) {
//                    case Constants.NEWS_NOT_AVAILABLE:
//                        return 0;
//                    case Constants.NEWS_NOT_READ:
//                        return 1;
//                    case Constants.NEWS_READ:
//                        return 2;
//                }
//            case Constants.ANNOUCEMENT_NOT_READ:
//                switch (newsAvailability) {
//                    case Constants.NEWS_NOT_AVAILABLE:
//                        return 3;
//                    case Constants.NEWS_NOT_READ:
//                        return 4;
//                    case Constants.NEWS_READ:
//                        return 5;
//                }
//            case Constants.ANNOUCEMENT_READ:
//                switch (newsAvailability) {
//                    case Constants.NEWS_NOT_AVAILABLE:
//                        return 6;
//                    case Constants.NEWS_NOT_READ:
//                        return 7;
//                    case Constants.NEWS_READ:
//                        return 8;
//                }
//        }
        return 0;
    }

    public static String[] searchTradingSymbols(String title, boolean singleMode, boolean showNondefaultExchanges, boolean showDefaultMarketOnly) {

        Symbols symbols = new Symbols();
        SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
        oCompanies.setSingleMode(singleMode);
        oCompanies.setDefaultMarketOnly(showDefaultMarketOnly);
        oCompanies.setIndexSearchMode(false);
        oCompanies.setSymbols(symbols);
        oCompanies.init();
        oCompanies.setTitle(title);
        oCompanies.setShowDefaultExchangesOnly(!showNondefaultExchanges);
        oCompanies.showDialog(true);

        return symbols.getSymbols();
    }

    public static String[] searchSymbols(String title, boolean singleMode, boolean showNondefaultExchanges) {

        Symbols symbols = new Symbols();
        SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
        oCompanies.setSingleMode(singleMode);
        oCompanies.setIndexSearchMode(false);
        oCompanies.init();
        oCompanies.setSymbols(symbols);
        oCompanies.setTitle(title);
        oCompanies.setShowDefaultExchangesOnly(!showNondefaultExchanges);
        oCompanies.showDialog(true);

        return symbols.getSymbols();
    }

    public static File[] getIntrdayPaths(String exchange) {
        try {
            OHLCFileFilter fileFilter = new OHLCFileFilter();
            File root = new File(Settings.getAbsolutepath() + "ohlc/" + exchange + "/");
            File[] folders = root.listFiles(fileFilter);
            Arrays.sort(folders, fileFilter);
            root = null;
            fileFilter = null;

            return folders;
        } catch (Exception e) {
            return null;
        }
    }

    public static File getIntrdayPaths(String exchange, String date) {
        try {
            File root = new File(Settings.getAbsolutepath() + "ohlc/" + exchange + "/" + date + "/");


            return root;
        } catch (Exception e) {
            return null;
        }
    }

    public static String removeImageTags(String sText) {
        int fromIndex = 0;
        int imgTagStartPos = 0;
        int imgTagEndPos = 0;
        String sImageTag = "<IMG";
        StringBuilder sBuff = new StringBuilder();

        if (sText.indexOf(sImageTag, fromIndex) <= 0)
            return sText;

        while (sText.indexOf(sImageTag, fromIndex) > 0) {
            imgTagStartPos = sText.indexOf(sImageTag, fromIndex);
            imgTagEndPos = sText.indexOf(">", imgTagStartPos);

            sBuff.append(sText.substring(fromIndex, imgTagStartPos));
            fromIndex = imgTagEndPos + 1;
        }
        sBuff.append(sText.substring(fromIndex));

        sImageTag = null;
        return sBuff.toString();
    }

    public static String getMemoryDetails() {
        try {
            double total = 0;
            double newUtilization = 0;
            double free;

            if (heapFormat == null)
                heapFormat = new DecimalFormat("###,##0.00'M'");
            if (utilFormat == null)
                utilFormat = new DecimalFormat("##0.00%");
            if (timeFormat == null)
                timeFormat = new SimpleDateFormat("HH:mm:ss");

            total = Runtime.getRuntime().totalMemory();
            free = Runtime.getRuntime().freeMemory();
            newUtilization = (total - free) / total;

            return " <" + heapFormat.format(total / 1048576L) + " " + utilFormat.format(newUtilization) + " " + timeFormat.format(new Date()) + ">";
        } catch (Exception e) {
            return "<No Memory Data>";
        }
    }

    public static byte[] getFormatedTradeString(boolean allTrades, String exchange, String trade, long columns) {

        String sTime = null;
        String sChange = null;
        String sPChange = null;
        String sVolume = null;
        String sPrice = null;
        String sSymbol = null;


        StringTokenizer oTokenizer = new StringTokenizer(trade, ",");
        sTime = (String) oTokenizer.nextElement();
        if (allTrades) {
            sSymbol = (String) oTokenizer.nextElement();
        }
        sPrice = (String) oTokenizer.nextElement();
        sVolume = (String) oTokenizer.nextElement();
        sChange = (String) oTokenizer.nextElement();
        sPChange = (String) oTokenizer.nextElement();

        StringBuffer buffer = new StringBuffer("");

        if ((columns & 1) == 1) {
            if (!allTrades) {
                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, Long.parseLong(sTime))); //Bug ID <#0013>
                sTime = formatter.format(date);
            }
            buffer.append(",");
            buffer.append(sTime);
        }
        if (allTrades) {
            columns >>= 1;
            buffer.append(",");
            buffer.append(sSymbol);
        }
        columns >>= 1;
        if ((columns & 1) == 1) {
            buffer.append(",");
            buffer.append(sPrice);
        }
        columns >>= 1;
        if ((columns & 1) == 1) {
            buffer.append(",");
            buffer.append(sVolume);
        }
        columns >>= 1;
        if ((columns & 1) == 1) {
            buffer.append(",");
            buffer.append(sChange);
        }
        columns >>= 1;
        if ((columns & 1) == 1) {
            buffer.append(",");
            buffer.append(sPChange);
        }

        buffer.append("\r\n");

        sTime = null;
        sChange = null;
        sPChange = null;
        sVolume = null;
        sPrice = null;

        return buffer.substring(1).getBytes();
    }


//    public static boolean dde = true;

    public static byte[] getFormatedTradeString(boolean allTrades, String exchange, String trade, BigInteger columns) {

        String sTime = null;
        String sChange = null;
        String sPChange = null;
        String sVolume = null;
        String sPrice = null;
        String sSymbol = null;
        String ticketNumber = null;
        String buyOrderNumber = null;
        String sellOrderNumber = null;
        String market = null;
        String side = null;
        String value = null;

//        StringTokenizer oTokenizer = new StringTokenizer(trade, ",");
        String[] fields = trade.split(",", 12);
        sTime = fields[0];
        if (allTrades) {
            sSymbol = fields[1];
            sPrice = fields[2];
            sVolume = fields[3];
            sChange = fields[4];
            sPChange = fields[5];
            ticketNumber = fields[6];
            buyOrderNumber = fields[7];
            sellOrderNumber = fields[8];
            market = fields[9];
            side = fields[10];
            value = fields[11];
        } else {
            sPrice = fields[1];
            sVolume = fields[2];
            sChange = fields[3];
            sPChange = fields[4];
            ticketNumber = fields[5];
            buyOrderNumber = fields[6];
            sellOrderNumber = fields[7];
            market = fields[8];
            side = fields[9];
            value = fields[10];
        }


        StringBuffer buffer = new StringBuffer("");
        //change start
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//        if ((columns & 1) == 1) {
            /*if (!allTrades) {
                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, Long.parseLong(sTime))); //Bug ID <#0013>
                sTime = formatter.format(date);
            }*/
            buffer.append(",");
            buffer.append(sTime);
        }
        if (allTrades) {
            columns = columns.shiftRight(1);
            buffer.append(",");
            buffer.append(sSymbol);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sPrice);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sVolume);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sChange);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sPChange);
        }

        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(ticketNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(buyOrderNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sellOrderNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(market);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(side);
        }


        buffer.append("\r\n");

        sTime = null;
        sChange = null;
        sPChange = null;
        sVolume = null;
        sPrice = null;

        return buffer.substring(1).getBytes();
    }

    public static String getFormatedTradeStr(boolean allTrades, String exchange, String trade, BigInteger columns) {

        String sTime = null;
        String sChange = null;
        String sPChange = null;
        String sVolume = null;
        String sPrice = null;
        String sSymbol = null;
        String ticketNumber = null;
        String buyOrderNumber = null;
        String sellOrderNumber = null;
        String market = null;
        String side = null;
        String value = null;

//        StringTokenizer oTokenizer = new StringTokenizer(trade, ",");
        String[] fields = trade.split(",", 12);
        sTime = fields[0];
        if (allTrades) {
            sSymbol = fields[1];
            sPrice = fields[2];
            sVolume = fields[3];
            sChange = fields[4];
            sPChange = fields[5];
            ticketNumber = fields[6];
            buyOrderNumber = fields[7];
            sellOrderNumber = fields[8];
            market = fields[9];
            side = fields[10];
            value = fields[11];
        } else {
            sPrice = fields[1];
            sVolume = fields[2];
            sChange = fields[3];
            sPChange = fields[4];
            ticketNumber = fields[5];
            buyOrderNumber = fields[6];
            sellOrderNumber = fields[7];
            market = fields[8];
            side = fields[9];
            value = fields[10];
        }


        StringBuffer buffer = new StringBuffer("");
        //change start
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
//        if ((columns & 1) == 1) {
            /*if (!allTrades) {
                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date date = new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, Long.parseLong(sTime))); //Bug ID <#0013>
                sTime = formatter.format(date);
            }*/
            buffer.append(",");
            buffer.append(sTime);
        }
        if (allTrades) {
            columns = columns.shiftRight(1);
            buffer.append(",");
            buffer.append(sSymbol);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sPrice);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sVolume);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sChange);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sPChange);
        }

        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(ticketNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(buyOrderNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(sellOrderNumber);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(market);
        }
        columns = columns.shiftRight(1);
        if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
            buffer.append(",");
            buffer.append(side);
        }


        buffer.append("\r\n");

        sTime = null;
        sChange = null;
        sPChange = null;
        sVolume = null;
        sPrice = null;

        return buffer.substring(1);
    }

    public static double getCommission(String exchange, double price, long quantity, String marketCode, String portfolio, String key, String list, double currencyrate) {
        double commission = 0;
        Rule rule = RuleManager.getSharedInstance().getCommissionRule(portfolio, exchange);
        if (rule == null) {
            rule = RuleManager.getSharedInstance().getCommissionRule("*", exchange);
        }

        if (rule != null) {
            try {
                Stock stock = DataStore.getSharedInstance().getStockObject(key);
                Interpreter interpreter = new Interpreter();
                interpreter.set("symbol", stock.getSymbolCode());
                interpreter.set("sector", stock.getSectorCode());
                interpreter.set("instrumentType", stock.getInstrumentType());
                interpreter.set("min", stock.getMinPrice());
                interpreter.set("max", stock.getMaxPrice());
                interpreter.set("open", stock.getTodaysOpen());
                interpreter.set("high", stock.getHigh());
                interpreter.set("low", stock.getLow());
                interpreter.set("tiff", 0);
                interpreter.set("disclosed", 0);
                interpreter.set("minfill", 0);
                interpreter.set("marketCode", stock.getMarketID());
                interpreter.set("refPrice", stock.getRefPrice());
                interpreter.set("bid", stock.getBestBidPrice());
                interpreter.set("offer", stock.getBestAskPrice());
                interpreter.set("direction", 0);
                interpreter.set("quantity", quantity);
                interpreter.set("price", price);
                if (marketCode != null) {
                    interpreter.set("marketCode", marketCode);
                }
                interpreter.set("currencyRate", currencyrate);//CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency())
                interpreter.set("list", list); //TPlusStore.getSharedInstance().getTPlusCommisionObject("EGP","0")
//                double test1=test(TPlusStore.getSharedInstance().getTPlusCommisionObject("EGP","0"),quantity*price,CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
//                System.out.println("TEST RULE : "+rule.getRule());
                try {
                    commission = (Double) interpreter.eval(rule.getRule());
                } catch (ClassCastException ex) {
                    commission = Double.parseDouble("" + interpreter.eval(rule.getRule()));
                }
                return commission;
            } catch (Exception e) {
                e.printStackTrace();
            }
            /*}else{
           return -1;*/
        }
        return 0;
        /*
         Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, instrument);

        Interpreter interpreter = new Interpreter();
        interpreter.set("symbol", stock.getSymbolCode());
        interpreter.set("sector", stock.getSectorCode());
        interpreter.set("price", getPrice());
        interpreter.set("quantity", Integer.parseInt(txtQuantity.getText()));
        interpreter.set("min", stock.getMinPrice());
        interpreter.set("max", stock.getMaxPrice());
        interpreter.set("open", stock.getTodaysOpen());
        interpreter.set("high", stock.getHigh());
        interpreter.set("low", stock.getLow());
        interpreter.set("side", side);
        interpreter.set("tiff", 0);
        interpreter.set("type", orderTypeList.get(cmbTypes.getSelectedIndex()).getId().charAt(0));
        interpreter.set("disclosed", 0);
        interpreter.set("minfill", 0);
        interpreter.set("marketCode", stock.getMarketID());
        interpreter.set("refPrice", stock.getRefPrice());
        interpreter.set("bid", stock.getBestBidPrice());
        interpreter.set("offer", stock.getBestAskPrice());
        interpreter.set("direction", 0);
         */
    }

    public static double test(String list, double val, double rate) {
        double commision = 0;
        double totalCommision = 0;
        String[] arr = list.split("~");
        for (int i = 0; i < arr.length; i++) {
            String[] arr2 = arr[i].split(",");
            double max = Double.parseDouble(arr2[0]);
            double min = Double.parseDouble(arr2[1]);
            double flat = Double.parseDouble(arr2[2]);
            double pct = Double.parseDouble(arr2[3]);
            commision = (val * pct / 100) + (flat * rate);
            if ((commision != 0) && (commision < min)) {
                commision = min;
            } else if ((commision != 0) && (commision > max)) {
                commision = max;
            }
            totalCommision = totalCommision + commision;
        }
        return totalCommision;

    }

    public static boolean isCurrentDay(String exchangeSymbol, long date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            return isCurrentDay(exchangeSymbol, format.format(new Date(date)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isCurrentDay(String exchangeSymbol, String yyyyMMdd) {
        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
            if (exchange != null) {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                if (yyyyMMdd.equals(format.format(new Date(exchange.getMarketDateTime())))) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean checkInstrumentType(String exchange, String symbol, int type){
         try {
            int instrumentType = DataStore.getSharedInstance().getStockObject(exchange, symbol).getInstrumentType();
            return (type == instrumentType);

        } catch (Exception e) {
             return false;
        }
    }*/

    public static boolean isFutureDay(String exchangeSymbol, String yyyyMMdd) {
        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
            if (exchange != null) {

                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                Date excangeDate = format.parse(format.format(new Date(exchange.getMarketDateTime())));
                Date currentDate = format.parse(yyyyMMdd);
                if (currentDate.getTime() > excangeDate.getTime()) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static void addBorders(Container cont) {
        //System.out.println("localizing");
        if (!(cont instanceof Container)) {
            return;
        }
        int n = cont.getComponentCount();
        JLabel btn = null;

        for (int i = 0; i < n; i++) {
            Component comp = cont.getComponent(i);
            if (comp instanceof JLabel) {
                btn = (JLabel) comp;
                btn.setBorder(BorderFactory.createLineBorder(Color.red));
            } else if (comp instanceof Container) {
                addBorders((Container) comp);
            }
        }
    }

    public static boolean checkInstrumentType(String key, int type) {
        try {
            int instrumentType = getInstrumentTypeFromKey(key);
            return (type == instrumentType);

        } catch (Exception e) {
            return false;
        }
    }

    public static void applyDecimalPlaces(DecimalFormat formatter, byte decimalPlaces) {
        try {
            switch (decimalPlaces) {
                case Constants.NO_DECIMAL_PLACES:
                    formatter.applyPattern(Constants.PATTERN_NO_DECIMAL);
                    break;
                case Constants.ONE_DECIMAL_PLACES:
                    formatter.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                    break;
                case Constants.TWO_DECIMAL_PLACES:
                    formatter.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                    break;
                case Constants.THREE_DECIMAL_PLACES:
                    formatter.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                    break;
                case Constants.FOUR_DECIMAL_PLACES:
                    formatter.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                    break;
                default:
                    formatter.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                    break;
            }
        } catch (Exception e) {
            formatter.applyPattern(Constants.PATTERN_TWO_DECIMAL);
        }
    }

    public static String getWorkingDir() {
        return (System.getProperties().get("user.dir") + "/Templates/").replaceAll("\\\\", "/");
    }

    public static String formatToDecimalPlaces(int decimalPlaces, float value) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
            case Constants.ONE_DECIMAL_PLACES:
            case Constants.TWO_DECIMAL_PLACES:
            case Constants.THREE_DECIMAL_PLACES:
            case Constants.FOUR_DECIMAL_PLACES:
            case Constants.FIVE_DECIMAL_PLACES:
            case Constants.SIX_DECIMAL_PLACES:
            case Constants.SEVEN_DECIMAL_PLACES:
            case Constants.EIGHT_DECIMAL_PLACES:
            case Constants.NINE_DECIMAL_PLACES:
            case Constants.TEN_DECIMAL_PLACES:
                return decimals[decimalPlaces].format(value);
            default:
                return decimals[2].format(value);
        }
    }

    public static String formatToDecimalPlaces(int decimalPlaces, double value) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
            case Constants.ONE_DECIMAL_PLACES:
            case Constants.TWO_DECIMAL_PLACES:
            case Constants.THREE_DECIMAL_PLACES:
            case Constants.FOUR_DECIMAL_PLACES:
            case Constants.FIVE_DECIMAL_PLACES:
            case Constants.SIX_DECIMAL_PLACES:
            case Constants.SEVEN_DECIMAL_PLACES:
            case Constants.EIGHT_DECIMAL_PLACES:
            case Constants.NINE_DECIMAL_PLACES:
            case Constants.TEN_DECIMAL_PLACES:
                return decimals[decimalPlaces].format(value);
            default:
                return decimals[2].format(value);
        }
    }

    public static String formatToDecimalPlacesNumeric(int decimalPlaces, float value) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
            case Constants.ONE_DECIMAL_PLACES:
            case Constants.TWO_DECIMAL_PLACES:
            case Constants.THREE_DECIMAL_PLACES:
            case Constants.FOUR_DECIMAL_PLACES:
            case Constants.FIVE_DECIMAL_PLACES:
            case Constants.SIX_DECIMAL_PLACES:
            case Constants.SEVEN_DECIMAL_PLACES:
            case Constants.EIGHT_DECIMAL_PLACES:
            case Constants.NINE_DECIMAL_PLACES:
            case Constants.TEN_DECIMAL_PLACES:
                return decimalsNoComma[decimalPlaces].format(value);
            default:
                return decimalsNoComma[2].format(value);
        }
    }

    public static String formatToDecimalPlacesNumeric(int decimalPlaces, double value) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
            case Constants.ONE_DECIMAL_PLACES:
            case Constants.TWO_DECIMAL_PLACES:
            case Constants.THREE_DECIMAL_PLACES:
            case Constants.FOUR_DECIMAL_PLACES:
            case Constants.FIVE_DECIMAL_PLACES:
            case Constants.SIX_DECIMAL_PLACES:
            case Constants.SEVEN_DECIMAL_PLACES:
            case Constants.EIGHT_DECIMAL_PLACES:
            case Constants.NINE_DECIMAL_PLACES:
            case Constants.TEN_DECIMAL_PLACES:
                return decimalsNoComma[decimalPlaces].format(value);
            default:
                return decimalsNoComma[2].format(value);
        }
    }

    // convert a byte sequence into a number

    public static String fomatAVGCurrency(double value) {
        return decimal5.format(value);

    }

    public static BigInteger bigIntValue(String sValue, BigInteger defaultValue) {
        try {
            BigInteger value = new BigInteger(sValue.trim());
            return value;
        } catch (NumberFormatException e) {
            //System.out.println("value = "+sValue);
            //System.out.println("In SharedMethods big int value method");
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static int byteArrayToInt(byte[] array      //
            , int offset     // start position in array
            , int length     // number of bytes to convert
    ) {
        int rv = 0;
        for (int x = 0; x < length; x++) {
            long bv = array[offset + x];
            if (x > 0 & bv < 0)
                bv += 256;
            rv *= 256;
            rv += bv;
        }
        return rv;
    }

    public static boolean isValidName(String name) {
        String g_sInvalidChars = "~!#&*(),:;\\";
        int iRestrctedLen = g_sInvalidChars.length();
        int i = 0;

        while (i < iRestrctedLen) {
            if (name.indexOf(g_sInvalidChars.substring(i, i + 1)) >= 0)
                return false;
            i++;
        }
        return true;
    }

    /*private static final TWDecimalFormat noDecimal = new TWDecimalFormat(" ###,##0 ");
    private static final TWDecimalFormat oneDecimal = new TWDecimalFormat(" ###,##0.0 ");
    private static final TWDecimalFormat twoDecimal = new TWDecimalFormat(" ###,##0.00 ");
    private static final TWDecimalFormat threeDecimal = new TWDecimalFormat(" ###,##0.000 ");
    private static final TWDecimalFormat fourDecimal = new TWDecimalFormat(" ###,##0.0000 ");*/
    /*private static final TWDecimalFormat oneDecimalNoZero = new TWDecimalFormat(" ###,##0.# ");
    private static final TWDecimalFormat twoDecimalNoZero = new TWDecimalFormat(" ###,##0.## ");
    private static final TWDecimalFormat threeDecimalNoZero = new TWDecimalFormat(" ###,##0.### ");
    private static final TWDecimalFormat fourDecimalNoZero = new TWDecimalFormat(" ###,##0.#### ");*/

    /*private static final TWDecimalFormat noDecimalNoComma = new TWDecimalFormat("##0");
    private static final TWDecimalFormat oneDecimalNoComma = new TWDecimalFormat("##0.0");
    private static final TWDecimalFormat twoDecimalNoComma = new TWDecimalFormat("##0.00");
    private static final TWDecimalFormat threeDecimalNoComma = new TWDecimalFormat("##0.000");
    private static final TWDecimalFormat fourDecimalNoComma = new TWDecimalFormat("##0.0000");
    private static final TWDecimalFormat oneDecimalNoZeroNoComma = new TWDecimalFormat("##0.#");
    private static final TWDecimalFormat twoDecimalNoZeroNoComma = new TWDecimalFormat("##0.##");
    private static final TWDecimalFormat threeDecimalNoZeroNoComma = new TWDecimalFormat("##0.###");
    private static final TWDecimalFormat fourDecimalNoZeroNoComma = new TWDecimalFormat("##0.####");*/

    public static boolean isValidIinformationType(String exchangeSymbol, int type) {
        return ExchangeStore.isValidIinformationType(exchangeSymbol, type);
    }

    public static boolean isValidSystemIinformationType(int type) {
        return ExchangeStore.isValidSystemFinformationType(type);
    }

    public static TWDecimalFormat getDecimalFormat(String exchange, String symbol, int instrument) {
        return getDecimalFormat(getDecimalPlaces(exchange, symbol, instrument));
    }

    public static TWDecimalFormat getDecimalFormat(String key) {
        return getDecimalFormat(getDecimalPlaces(key));
    }

    public static TWDecimalFormat getDecimalFormat(int decimalCount) {
        switch (decimalCount) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return decimals[decimalCount];
            case -1:
            case -2:
            case -3:
            case -4:
            case -5:
            case -6:
            case -7:
            case -8:
            case -9:
            case -10:
                return decimalsNoZero[-decimalCount];
            default:
                return decimals[2];
        }
    }

    public static TWDecimalFormat getDecimalFormatNoComma(String exchange, String symbol, int instrument) {
        return getDecimalFormatNoComma(getDecimalPlaces(exchange, symbol, instrument));
    }

    public static TWDecimalFormat getDecimalFormatNoComma(String key) {
        return getDecimalFormatNoComma(getDecimalPlaces(key));
    }

    public static TWDecimalFormat getDecimalFormatNoCommaNoSpace(String key) {
        return getDecimalFormatNoCommaNoSpace(getDecimalPlaces(key));
    }

    public static TWDecimalFormat getDecimalFormatNoComma(int decimalCount) {
        switch (decimalCount) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return decimalsNoComma[decimalCount];
            case -1:
            case -2:
            case -3:
            case -4:
            case -5:
            case -6:
            case -7:
            case -8:
            case -9:
            case -10:
                return decimalsNoZeroNoComma[-decimalCount];
            default:
                return decimals[2];
        }
    }

    public static TWDecimalFormat getDecimalFormatNoCommaNoSpace(int decimalCount) {
        switch (decimalCount) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return decimalsNoCommaNoSpace[decimalCount];
            case -1:
            case -2:
            case -3:
            case -4:
            case -5:
            case -6:
            case -7:
            case -8:
            case -9:
            case -10:
                return decimalsNoZeroNoCommaNoSpace[-decimalCount];
            default:
                return decimals[2];
        }
    }

    public static byte getDecimalPlaces(String key) {
        try {
            return DataStore.getSharedInstance().getStockObject(key).getDecimalCount();
        } catch (Exception e) {
            return 2;
        }
    }

    public static byte getDecimalPlaces(String exchange, String symbol, int instrument) {
        try {
            return DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument).getDecimalCount();
        } catch (Exception e) {
            return 2;
        }
    }

    public static int getCurrencyDecimalPlaces(String exchange, String symbol, int instrument) {
        try {
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
            String currency = stock.getCurrencyCode();
            return CurrencyStore.getSharedInstance().getDecimalPlaces(currency);
        } catch (Exception e) {
            return 2;
        }
    }

    public static int getCurrencyDecimalPlaces(String currency) {
        try {
            return CurrencyStore.getSharedInstance().getDecimalPlaces(currency);
        } catch (Exception e) {
            return 2;
        }
    }

    public static TWDecimalFormat getCurrencyDecimalFormat(String currency) {
        try {
            return getDecimalFormat(CurrencyStore.getSharedInstance().getDecimalPlaces(currency));
        } catch (Exception e) {
            return getDecimalFormat(2);
        }
    }

    public static TWDecimalFormat getCurrencyDecimalFormat(String exchange, String symbol, int instrument) {
        return getDecimalFormat(getCurrencyDecimalPlaces(exchange, symbol, instrument));
    }

    public static String formatToCurrencyFormat(String currency, double value) {
        return getDecimalFormat(currency).format(value);
    }

    public static String formatToCurrencyFormatNoComma(String currency, double value) {
        return getDecimalFormatNoComma(currency).format(value);
    }

    public static String formatToCurrencyFormatNoCommaNoSpace(String currency, double value) {
        return getDecimalFormatNoCommaNoSpace(currency).format(value);
    }

    public static String toDisplayDateFormat(long date) {
        return displayDateFormat.format(date);
    }

    public static String getBrokerTypeString(char brokerType) {
        switch (brokerType) {
            case Constants.BROKER_TYPE_FOREIGN:
                return Language.getString("BROKER_TYPE_FOREIGN");
            case Constants.BROKER_TYPE_DOMESTIC:
                return Language.getString("BROKER_TYPE_DOMESTIC");
            case Constants.BROKER_TYPE_HOUSE_ACC:
                return Language.getString("BROKER_TYPE_HOUSE_ACC");
            default:
                return Language.getString("NA");
        }
    }

    public static void deleteFiles(File path) {
        File[] files = path.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                File newDir = new File(path + "/" + file.getName());
                deleteFiles(newDir);
                file.delete();
            } else {
                file.delete();
            }
        }
    }

    public static long getDateForex(String sValue) {
        long lTime = 0;
        try {
            Calendar oCal = Calendar.getInstance();
            oCal.setTimeInMillis(0);
            oCal.set(Calendar.YEAR, Integer.parseInt(sValue.substring(0, 2)));
            oCal.set(Calendar.MONTH, Integer.parseInt(sValue.substring(2, 4)) - 1);
            oCal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(sValue.substring(4)));
            lTime = oCal.getTime().getTime();
            oCal = null;
            return lTime;
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return lTime;
        }

    }

    public static String getTextFromHTML(String html) {
        try {
            html = html.replaceAll("<BR>|<br>", "\n");
            html = html.replaceAll("&amp;", "&");
            html = html.replaceAll("&quot;", "\"");
            html = html.replaceAll("&lt;", "<");
            html = html.replaceAll("&gt;", ">");
            JLabel lbl = new JLabel(html);
            View v = (View) lbl.getClientProperty(BasicHTML.propertyKey);
            if (v != null) {
                Document d = v.getDocument();
                return d.getText(0, d.getLength()).trim();
            } else {
                return html;
            }
        } catch (BadLocationException e) {
            return html;
        }
    }

    public static int getDateDiff(Date base, Date target) {

        int difference = 0;
        Calendar baseCal = Calendar.getInstance();
        Calendar targetCal = Calendar.getInstance();

        baseCal.setTime(base);
        targetCal.setTime(target);

        while ((baseCal.get(Calendar.YEAR) != targetCal.get(Calendar.YEAR)) ||
                (baseCal.get(Calendar.DAY_OF_YEAR) != targetCal.get(Calendar.DAY_OF_YEAR))) {
            baseCal.add(Calendar.DATE, 1);
            difference++;
        }
        baseCal = null;
        targetCal = null;
        return difference;

    }

    public static boolean symbolExceedTheGlobalLimit(String key) {
        if (DataStore.isContainedSymbol(key)) {
            // symbol is already registered. No problem in having it in the watchlist
            return false;
        } else {
            if (DataStore.getUnchanedSymbolCount() <= Settings.getMaximumSymbolCount() - 1) {
                return false;
            } else {
                return true;
            }
        }

    }

    public static void activateGoogleAnalytics() {
        GoogleUpdator.getInstance().activate();
    }

    public static void updateGoogleAnalytics(String key) {
        updateGoogleAnalytics(null, key);
    }

    public static void updateGoogleAnalytics(GoogleSection section, String key) {
        if (section != null) {
            GoogleUpdator.getInstance().addTag(section.toString() + "-" + key);
        } else {
            GoogleUpdator.getInstance().addTag(key);
        }
    }

    public static void debugOut(String text) {

        try {
            if (out == null) {
                out = new PrintStream(new FileOutputStream("debug.wtl"));
            }
            out.println(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void applyCustomViewSetting(ViewSetting oSetting) {

        String key = null;
        int iType = -1;
        byte bytMainType;
        byte bytSubType;
        String sID = null;

        Hashtable<String, String> customViews = Settings.getCustomViewSettings();
        Enumeration<String> keys = customViews.keys();
        while (keys.hasMoreElements()) {
            key = keys.nextElement().trim();
            if (key.length() > 0) {
                int iPipe = key.indexOf("|");
                if (iPipe == -1) {
                    iType = Integer.parseInt(key);
                    sID = null;
                } else {
                    iType = Integer.parseInt(key.substring(0, iPipe));
                    try {
                        sID = key.substring(iPipe + 1).trim();
                    } catch (Exception e) {
                        sID = null;
                    }
                }
            }
            bytSubType = (byte) (iType);
            bytMainType = (byte) (iType >>> 8);
            if ((oSetting.getMainType() == ViewSettingsManager.MARKET_VIEW)) {
                if ((oSetting.getID().substring(0, 1).equals(sID))) {
                    String sWorkspaceSetting = customViews.get(key);
                    oSetting.setWorkspaceRecord(sWorkspaceSetting);
                }
            } else if ((oSetting.getMainType() == ViewSettingsManager.SECTOR_VIEW) ||
                    (oSetting.getMainType() == ViewSettingsManager.MARKET_VIEW) ||
                    (oSetting.getMainType() == ViewSettingsManager.FILTERED_VIEW) ||
                    (oSetting.getMainType() == ViewSettingsManager.CUSTOM_VIEW) ||
                    (oSetting.getMainType() == ViewSettingsManager.FOREX_VIEW) ||
                    (bytMainType == ViewSettingsManager.MIST_VIEW)) {
                if ((oSetting.getMainType() == bytMainType)) {
                    String sWorkspaceSetting = customViews.get(key);
                    oSetting.setWorkspaceRecord(sWorkspaceSetting);
                }
            } else if ((oSetting.getMainType() == ViewSettingsManager.PORTFOLIO_VIEW)) {
                if (oSetting.getID().equals(sID)) {
                    String sWorkspaceSetting = customViews.get(key);
                    oSetting.setWorkspaceRecord(sWorkspaceSetting);
                }
            } else if (oSetting.getMainType() == ViewSettingsManager.SUMMARY_VIEW) {
                if (oSetting.getID().equals(sID)) {
                    String sWorkspaceSetting = customViews.get(key);
                    oSetting.setWorkspaceRecord(sWorkspaceSetting);
                }
            } else if (oSetting.getMainType() == ViewSettingsManager.SYMBOL_VIEW) {
                if (oSetting.getType() == iType) {
                    String sWorkspaceSetting = customViews.get(key);
                    oSetting.setWorkspaceRecord(sWorkspaceSetting);
                }
            } else {
                System.out.println("unknown type ==" + oSetting.getType());
            }
//            String sWorkspaceSetting = customViews.get(key);
//            oSetting.setWorkspaceRecord(sWorkspaceSetting);
        }
    }

//    public static

    public static void saveCustomViewSetting(ViewSetting oSetting) {
        String sWorkspaceSetting = oSetting.toWorkspaceString(false);
        String key = "" + oSetting.getType();
        String sID = null;
        if ((oSetting.getMainType() == ViewSettingsManager.MARKET_VIEW)) {
            sID = oSetting.getID().substring(0, 1);
            key = key + "|" + sID;
        } else if ((oSetting.getMainType() == ViewSettingsManager.PORTFOLIO_VIEW)) {
            sID = oSetting.getID();
            key = key + "|" + sID;
        } else if (oSetting.getMainType() == ViewSettingsManager.SUMMARY_VIEW) {
            sID = oSetting.getID();
            key = key + "|" + sID;
        }
        Settings.putCustomViewSetting(key, sWorkspaceSetting);
    }

    public static void showExchangeExpiryMesage(String exchange) {
        try {
            String msg = Language.getString("MSG_EXCHANGE_EXPIRED");
            msg = msg.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
            showMessage(msg, JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showExchangeInactiveMesage(String exchange) {
        try {
            String msg = Language.getString("MSG_EXCHANGE_INACTIVE");
            msg = msg.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
            showMessage(msg, JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showExchangeDelayedmessage(String exchange) {
        String message = Language.getString("EXCHANGE_DELAYED_MESSAGE");
        message = message.replace("[EXCHANGE]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
        try {
            new ShowMessage("<html><b>" + message + "</html>", "E");
        } catch (Exception e) {
            new ShowMessage("<html><b>" + message + "</html>", "E");
        }
    }

    public static boolean contains(Object[] objectArray, Object compare) {
        for (int i = 0; i < objectArray.length; i++) {
            if (objectArray[i].equals(compare)) {
                return true;
            }
        }
        return false;
    }

    public static int passwordStrength(String username, String password) {
        int score = 0;

        //password < 4
        if (password.length() < 4) {
            return 1;
        }

        //password == username
        if (password.toLowerCase().equals(username.toLowerCase())) return 2;

        //password length
        score += password.length() * 4;
        score += (checkRepetition(1, password).length() - password.length());
        score += (checkRepetition(2, password).length() - password.length());
        score += (checkRepetition(3, password).length() - password.length());
        score += (checkRepetition(4, password).length() - password.length());

        //password has 3 numbers
        if (password.matches("(.*[0-9].*[0-9].*[0-9])")) score += 5;

        //password has 2 sybols
        if (password.matches("(.*[!,@,#,$,%,^,&,*,?,_,~].*[!,@,#,$,%,^,&,*,?,_,~].*)")) score += 5;

        //password has Upper and Lower chars
        if (password.matches("(.*[a-z].*[A-Z].*)|(.*[A-Z].*[a-z].*)")) score += 10;

        //password has number and chars
        if (password.matches(".*[a-zA-Z].*") && password.matches(".*[0-9].*")) score += 10;
        //
        //password has number and symbol
        if (password.matches(".*[!,@,#,$,%,^,&,*,?,_,~].*") && password.matches(".*[0-9].*")) score += 10;

        //password has char and symbol
        if (password.matches("(.*[!,@,#,$,%,^,&,*,?,_,~].*)") && password.matches("(.*[a-zA-Z].*)")) score += 10;

        //password is just  numbers or chars
        if (password.matches("[0-9]*") || password.matches("[a-zA-Z]*")) score -= 10;

        if (password.indexOf(username) >= 0) score -= 10;

        //verifing 0 < score < 100
        if (score < 1) score = 1;
        if (score > 100) score = 100;

        //if (score < 34 )  return badPass
        //if (score < 68 )  return goodPass
        return score;
    }

    private static String checkRepetition(int pLen, String str) {
        String res = "";
        int j = 0;
        for (int i = 0; i < str.length(); i++) {
            boolean repeated = true;
            for (j = 0; j < pLen && (j + i + pLen) < str.length(); j++) {
                repeated = repeated && (str.charAt(j + i) == str.charAt(j + i + pLen));
            }
            if (j < pLen) repeated = false;
            if (repeated) {
                i += pLen - 1;
                repeated = false;
            } else {
                res += str.charAt(i);
            }
        }
        return res;
    }

// checkRepetition(1,'aaaaaaabcbc')   = 'abcbc'
// checkRepetition(2,'aaaaaaabcbc')   = 'aabc'
// checkRepetition(2,'aaaaaaabcdbcd') = 'aabcd'

    public static int getPasswordStrength(String username, String password) {
        if (username.equals("") || password.equals("")) {
            return 0;
        }
        return passwordStrength(username, password);
    }

//    public static String getPasswordStrength(String username, String password){
//        if(username.equals("") || password.equals("")){
//            return "";
//        }
//        int result = passwordStrength(username, password);
//        if(result < 1){
//            return Constants.SHORT_PASSWORD;
//        } else if (result < 50) {
//            return Constants.BAD_PASSWORD;
//        } else if( result < 75){
//            return Constants.GOOD_PASSWORD;
//        } else {
//            return Constants.STRONG_PASSWORD;
//        }
//    }

    public static Hashtable<String, String> getNtworkInfo(String ip) {
        try {
            Hashtable<String, String> ipInfo = new Hashtable<String, String>();

            ipInfo.put("ip", ip);

            // call the ipconfig to extract network infomation
            Process p = Runtime.getRuntime().exec("cmd /c ipconfig");
            InputStream in = p.getInputStream();
            byte[] data = new byte[1000];
            StringBuilder buffer = new StringBuilder();
            while (true) {
                int len = in.read(data);
                if (len < 0) {
                    break;
                }
                buffer.append(new String(data, 0, len));
            }
            p.destroy();

            // extract network information
            BufferedReader bin = new BufferedReader(new StringReader(buffer.toString()));
            boolean segmentFound = false;
            boolean subnetPending = true;
            boolean gatewayPending = true;
            while (true) {
                String s = bin.readLine();
                if (s == null) break;

                if (segmentFound) {
                    if (subnetPending) {
                        if (s.toLowerCase().indexOf("subnet") >= 0) {
                            ipInfo.put("subnet", exrtactIP(s));
                            subnetPending = false;
                        }
                    } else if (gatewayPending) {
                        if (s.toLowerCase().indexOf("gate") >= 0) {
                            ipInfo.put("gateway", exrtactIP(s));
                            gatewayPending = false;
                        }
                    }

                    if ((!subnetPending) && (!gatewayPending)) {
                        break;
                    }
                }

                if (s.indexOf(ip) >= 0) { // segment found
                    segmentFound = true;
                }
            }
            return ipInfo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String exrtactIP(String line) {
        int colon = line.indexOf(":");
        return line.substring(colon + 1).trim();
    }

    public static void setComboBoxBorder(Container comboBox, Border border) {
        for (int i = 0; i < comboBox.getComponentCount(); i++) {
            if ((comboBox.getComponent(i) instanceof JComponent) /*&& (((JComponent)comboBox.getComponent(i)).getBorder() != null)*/) {
                if (border == null) {
                    ((JComponent) comboBox.getComponent(i)).setBorder(BorderFactory.createEmptyBorder());
                } else {
                    ((JComponent) comboBox.getComponent(i)).setBorder(border);
                }
            } else if (comboBox.getComponent(i) instanceof Container) {
                setComboBoxBorder((Container) comboBox.getComponent(i), border);
            }
        }
    }

    public static int getSymbolType(int instrumentType) {
        switch (instrumentType) {
//            case Meta.INSTRUMENT_QUOTE:
            case Meta.INSTRUMENT_EQUITY:
            case Meta.INSTRUMENT_COMMON_STOCK:
            case Meta.INSTRUMENT_PREFERRED_STOCK:
            case Meta.INSTRUMENT_WARRANT:
            case Meta.INSTRUMENT_PREMIUM:
            case Meta.INSTRUMENT_TRUST:
            case Meta.INSTRUMENT_RIGHT:
            case Meta.INSTRUMENT_WARRANT_RIGHT:
            case Meta.INSTRUMENT_EXCHANGE_TRADED_FUNDS:
                return Meta.SYMBOL_TYPE_EQUITY;
            case Meta.INSTRUMENT_FUTURE:
            case Meta.INSTRUMENT_FUTURE_SPREAD:
            case Meta.INSTRUMENT_OPTION:
            case Meta.INSTRUMENT_EQUITY_OPTION:
            case Meta.INSTRUMENT_INDEX_OPTION:
            case Meta.INSTRUMENT_FUTURE_OPTION:
                return Meta.SYMBOL_TYPE_OPTIONS;
            case Meta.INSTRUMENT_MUTUALFUND:
                return Meta.SYMBOL_TYPE_MUTUALFUND;
            case Meta.INSTRUMENT_FIXED_INCOME:
            case Meta.INSTRUMENT_CONVERTIBLE_BOND:
            case Meta.INSTRUMENT_MBS:
            case Meta.INSTRUMENT_GOV_BOND:
            case Meta.INSTRUMENT_CORP_BOND:
            case Meta.INSTRUMENT_US_AGENCY_BOND:
            case Meta.INSTRUMENT_US_TREASURY_BILL:
            case Meta.INSTRUMENT_US_TREASURY_COUPON:
            case Meta.INSTRUMENT_MONEY_MARKET:
            case Meta.INSTRUMENT_CD:
            case Meta.INSTRUMENT_BOND:
                return Meta.SYMBOL_TYPE_BONDS;
            case Meta.INSTRUMENT_FOREX:
            case Meta.INSTRUMENT_FOREX_FRA:
            case Meta.INSTRUMENT_FOREX_DEPOSIT:
            case Meta.INSTRUMENT_FOREX_FORWARD:
            case Meta.INSTRUMENT_FOREX_STATISTICS:
                return Meta.SYMBOL_TYPE_FOREX;
            case Meta.INSTRUMENT_INDEX:
                return Meta.SYMBOL_TYPE_INDEX;
            default:
                return Meta.SYMBOL_TYPE_EQUITY;
        }
    }

    public static String getInstrumentType(short symbolType) {
        switch (symbolType) {
            case Meta.INSTRUMENT_EQUITY:
            case Meta.INSTRUMENT_COMMON_STOCK:
            case Meta.INSTRUMENT_PREFERRED_STOCK:
            case Meta.INSTRUMENT_WARRANT:
            case Meta.INSTRUMENT_PREMIUM:
            case Meta.INSTRUMENT_TRUST:
            case Meta.INSTRUMENT_RIGHT:
            case Meta.INSTRUMENT_WARRANT_RIGHT:
            case Meta.INSTRUMENT_EXCHANGE_TRADED_FUNDS:
                return Constants.INS_EQUITY;
            case Meta.INSTRUMENT_MUTUALFUND:
                return Constants.INS_MUTUALFUND;
            case Meta.INSTRUMENT_BOND:
            case Meta.INSTRUMENT_FIXED_INCOME:
            case Meta.INSTRUMENT_CONVERTIBLE_BOND:
            case Meta.INSTRUMENT_MBS:
            case Meta.INSTRUMENT_GOV_BOND:
            case Meta.INSTRUMENT_CORP_BOND:
            case Meta.INSTRUMENT_US_AGENCY_BOND:
            case Meta.INSTRUMENT_US_TREASURY_BILL:
            case Meta.INSTRUMENT_US_TREASURY_COUPON:
            case Meta.INSTRUMENT_MONEY_MARKET:
            case Meta.INSTRUMENT_CD:
                return Constants.INS_BONDS;
            case Meta.SYMBOL_TYPE_INDEX:
                return Constants.INS_INDEX;
            case Meta.INSTRUMENT_FUTURE:
            case Meta.INSTRUMENT_FUTURE_SPREAD:
            case Meta.INSTRUMENT_OPTION:
            case Meta.INSTRUMENT_EQUITY_OPTION:
            case Meta.INSTRUMENT_INDEX_OPTION:
            case Meta.INSTRUMENT_FUTURE_OPTION:
                return Constants.INS_OPTION;
            case Meta.INSTRUMENT_FOREX:
            case Meta.INSTRUMENT_FOREX_FRA:
            case Meta.INSTRUMENT_FOREX_DEPOSIT:
            case Meta.INSTRUMENT_FOREX_FORWARD:
            case Meta.INSTRUMENT_FOREX_STATISTICS:
                return Constants.INS_FOREX;
            default:
                return Constants.INS_EQUITY;
        }
    }

    public static Hashtable<Short, String> getAssestsStore() {
        Hashtable<Short, String> store = new Hashtable<Short, String>();
        store.put(Meta.SYMBOL_TYPE_EQUITY, Constants.INS_EQUITY);
        store.put(Meta.SYMBOL_TYPE_MUTUALFUND, Constants.INS_MUTUALFUND);
        store.put(Meta.SYMBOL_TYPE_INDEX, Constants.INS_INDEX);
        store.put(Meta.SYMBOL_TYPE_OPTIONS, Constants.INS_OPTION);
        store.put(Meta.SYMBOL_TYPE_FOREX, Constants.INS_FOREX);
        store.put(Meta.SYMBOL_TYPE_BONDS, Constants.INS_BONDS);


        return store;
    }

    public static void populateExchangesForTWCombo(ArrayList<TWComboItem> exchanges, boolean fullMarketsOnly) {
        try {
            exchanges.clear();
            Enumeration<Exchange> enuExchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (enuExchanges.hasMoreElements()) {
                Exchange exchange = enuExchanges.nextElement();
                if ((!fullMarketsOnly) || (fullMarketsOnly && exchange.isDefault())) {
                    TWComboItem item = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    exchanges.add(item);
                }
            }
            Collections.sort(exchanges);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void populateExchangesForAnnouncementsCombo(ArrayList<TWComboItem> exchanges, boolean fullMarketsOnly) {
        try {
            exchanges.clear();
            Enumeration<Exchange> enuExchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (enuExchanges.hasMoreElements()) {
                Exchange exchange = enuExchanges.nextElement();
                if (!exchange.isExpired() && !exchange.isInactive() && (ExchangeStore.getSharedInstance().isAnnouncementsAvailableforExchange(exchange.getSymbol()))) {
                    if ((!fullMarketsOnly) || (fullMarketsOnly && exchange.isDefault())) {
                        TWComboItem item = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                        exchanges.add(item);
                    }
                }
            }
            Collections.sort(exchanges);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void populateSubMarketsForTWCombo(ArrayList<TWComboItem> markets, boolean fullMarketsOnly, String exchange) {
        try {
//            markets.clear();
            Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
            for (Market subMarket : exg.getSubMarkets()) {
                TWComboItem item = new TWComboItem(subMarket.getMarketID().trim(), subMarket.getDescription().trim());
                markets.add(item);
            }
            Collections.sort(markets);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void populateSectorsForTWCombo(ArrayList<TWComboItem> exchanges, String exchange) {
        try {
            exchanges.add(new TWComboItem(Language.getString("ALL"), Language.getString("ALL")));
            Exchange selectedEx = ExchangeStore.getSharedInstance().getExchange(exchange);
            Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange);
            while (sectors.hasMoreElements()) {
                Sector sec = sectors.nextElement();


                TWComboItem item = new TWComboItem(sec.getId(), sec.getDescription());
                exchanges.add(item);

            }
            //   Collections.sort(exchanges);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public static void populateExchanges(ArrayList<Exchange> exchanges, boolean fullMarketsOnly) {
        try {
            exchanges.clear();
            Enumeration<Exchange> enuExchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (enuExchanges.hasMoreElements()) {
                Exchange exchange = enuExchanges.nextElement();
                if ((!fullMarketsOnly) || (fullMarketsOnly && exchange.isDefault())) {
                    exchanges.add(exchange);
                }
            }
            Collections.sort(exchanges);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changeTableFont(Font oldFont, Font newBodyFont, Table table, SmartTable smartTable, CommonTableSettings target) {


        if ((newBodyFont != null) && (!newBodyFont.equals(oldFont))) {
            target.setBodyFont(newBodyFont);
            smartTable.setBodyFont();
            try {
                ((CommonTable) smartTable.getModel()).getViewSettings().setFont(newBodyFont);
            } catch (Exception ex) {
                try {
                    table.getModel().getViewSettings().setFont(newBodyFont);
                } catch (Exception ex1) {
                }
            }
            if (smartTable.isUseSameFontForHeader()) {
                target.setHeadingFont(newBodyFont);
                ((CommonTable) smartTable.getModel()).getViewSettings().setHeaderFont(newBodyFont);
                smartTable.updateHeaderUI();
            }

            /* When row count is high, it takes time to update font changes.Application get stucked.
              To avoid that, before call this method, check row count -(Eg:Market Time n Sales) 2009/04/29 -Shanika
            */
            try {
                if (((CommonTable) smartTable.getModel()).getRowCount() < 1000) {
                    smartTable.adjustColumnWidthsToFit(40);
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            table.setFontResized(true);
            //todo - removed  16/01/2007
            if ((table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                    (table.getWindowType() == ViewSettingsManager.SNAP_QUOTE) ||
                    (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) ||
                    (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND) ||
                    (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_FOREX)) {

                table.packFrame();
            }
        } else {
            table.setFontResized(false);
        }
        try {
            ((TWTableRenderer) table.getTable().getDefaultRenderer(Object.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
        } catch (Exception e1) {
        }
        try {
            ((TWTableRenderer) table.getTable().getDefaultRenderer(Number.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
        } catch (Exception e1) {
        }
        try {
            ((TWTableRenderer) table.getTable().getDefaultRenderer(Boolean.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
        } catch (Exception e1) {
        }

    }

    public static void updateComponent(final JComponent component) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                component.updateUI();
            }
        });

    }

    public static String getIntrumentKey(String symbol, int instrumentType) {
        return symbol + Constants.KEY_SEPERATOR_CHARACTER + instrumentType;

    }

    /* public static int showHTMLMessage(String title, String msg, int messageType, int optionType){
        JEditorPane pane = new JEditorPane("text/html", msg);
        pane.addHyperlinkListener(new HyperlinkListener(){
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED){
                    System.out.println(e.getURL());
                    try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (Exception e1) {
                    }
                }
            }
        });

        JOptionPane optionPane = new JOptionPane(pane);
        pane.setOpaque(false);
        optionPane.setOptionType(optionType);
        optionPane.setMessageType(messageType);
        pane.setEditable(false);
        JDialog dialog = optionPane.createDialog(null, title);
        dialog.setVisible(true);
        return (Integer)optionPane.getValue();
    }*/

    public static Image rotateImage(Image inputImage, int angle /*in degrees*/) {
        try {
            BufferedImage sourceBI = new BufferedImage(inputImage.getWidth(null), inputImage
                    .getHeight(null), BufferedImage.TYPE_INT_ARGB);

            Graphics2D g = (Graphics2D) sourceBI.getGraphics();
            g.drawImage(inputImage, 0, 0, null);

            AffineTransform at = new AffineTransform();

            // rotate 45 degrees around image center
            double center = Math.max(sourceBI.getWidth(), sourceBI.getHeight()) / 2.0;
            at.rotate(angle * Math.PI / 180.0, center, center);
//            at.rotate(angle * Math.PI / 180.0, sourceBI.getWidth() / 2.0, sourceBI
//                    .getHeight() / 2.0);

            /*
            * translate to make sure the rotation doesn't cut off any image data
                */
            /*AffineTransform translationTransform;
            translationTransform = findTranslation(at, sourceBI);
            at.preConcatenate(translationTransform);*/

            // instantiate and apply affine transformation filter
            BufferedImageOp bio;
            bio = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);

            BufferedImage destinationBI = bio.filter(sourceBI, null);

            return destinationBI;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    private static AffineTransform findTranslation(AffineTransform at, BufferedImage bi) {
        Point2D p2din, p2dout;

        p2din = new Point2D.Double(0.0, 0.0);
        p2dout = at.transform(p2din, null);
        double ytrans = p2dout.getY();

        p2din = new Point2D.Double(0, bi.getHeight());
        p2dout = at.transform(p2din, null);
        double xtrans = p2dout.getX();

        AffineTransform tat = new AffineTransform();
        tat.translate(-xtrans, -ytrans);
        return tat;
    }

    /*
    * find proper translations to keep rotated image correctly displayed
    */

    /**
     * Unmap a memory map before deletion
     *
     * @param buffer
     */
    public static synchronized void unmap(final Object buffer) {
        try {
            AccessController.doPrivileged(new PrivilegedAction() {
                public Object run() {
                    try {
                        Method getCleanerMethod = buffer.getClass().getMethod("cleaner", new Class[0]);
                        getCleanerMethod.setAccessible(true);
                        sun.misc.Cleaner cleaner = (sun.misc.Cleaner) getCleanerMethod.invoke(buffer, new Object[0]);
                        cleaner.clean();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void translateAnnouncement(final String newsID, final String from,
                                             final String to) {
        new Thread() {
            @Override
            public void run() {
                boolean realTimeNres = true;
                Announcement announcement = AnnouncementStore.getSharedInstance().getAnnouncement(newsID);
                if (announcement == null) {
                    announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(newsID);
                    realTimeNres = false;
                }

                if (announcement != null) {
                    GoogleLanguage fromLanguage = GoogleLanguage.valueOf(from);
                    GoogleLanguage toLanguage = GoogleLanguage.valueOf(to);
                    String title = Translater.translate(announcement.getHeadLine(), fromLanguage, toLanguage);
                    String splitter = getBetSplitter(announcement.getMessage());
                    System.out.println("Splitter " + splitter);
                    String[] paras = announcement.getMessage().toUpperCase().split(splitter);
                    String[] encoded = announcement.getMessage().toUpperCase().split(splitter);
                    String body = "";

                    for (int i = 0; i < encoded.length; i++) {
                        try {
                            encoded[i] = URLEncoder.encode(encoded[i], "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    int index = 0;
                    String buffer = paras[index] + ".";
                    String encodedBuffer = encoded[index] + "xxx";
                    index++;
                    boolean errors = false;
                    while (index < paras.length) {
                        while (index < paras.length) {
                            if ((encodedBuffer.length() + encoded[index].length()) > 1500) {
                                break;
                            } else {
                                buffer += paras[index] + ".";
                                encodedBuffer += "xxx" + encoded[index];
                                index++;
                            }
                        }
                        String translated = Translater.translate(buffer, fromLanguage, toLanguage);
                        if (translated == null) {
                            errors = true;
                            break;
                        } else {
                            body += translated;
                        }
                        if (index < paras.length) {
                            buffer = paras[index];
                            encodedBuffer = encoded[index];
                            index++;
                        } else {
                            buffer = "";
                        }
                    }
                    if (!buffer.equals("")) {
                        String translated = Translater.translate(buffer, fromLanguage, toLanguage);
                        if (translated == null) {
                            errors = true;
                        } else {
                            body += translated;
                        }
                    }

                    if (!errors) {
                        if (title != null)
                            announcement.setHeadLine(title);
                        if (body != null)
                            announcement.setMessage(body);
                        if (realTimeNres) {
                            AnnouncementStore.getSharedInstance().showAnnouncement(newsID);
                        } else {
                            SearchedAnnouncementStore.getSharedInstance().showAnnouncement(newsID);
                        }
                    } else {
                        System.out.println("Errors occured");
                    }
                }
            }
        }.start();
    }

    public static void translateNews(final String newsID, final String from,
                                     final String to) {
        new Thread() {
            @Override
            public void run() {
                boolean realTimeNres = true;
                News news = NewsStore.getSharedInstance().getNews(newsID);
                if (news == null) {
                    news = SearchedNewsStore.getSharedInstance().getSearchedNews(newsID);
                    realTimeNres = false;
                }

                if (news != null) {
                    GoogleLanguage fromLanguage = GoogleLanguage.valueOf(from);
                    GoogleLanguage toLanguage = GoogleLanguage.valueOf(to);
                    String title = Translater.translate(news.getHeadLine(), fromLanguage, toLanguage);
                    String splitter = getBetSplitter(news.getBody());
                    System.out.println("Splitter " + splitter);
                    String[] paras = news.getBody().toUpperCase().split(splitter);
                    String[] encoded = news.getBody().toUpperCase().split(splitter);
                    String body = "";

                    for (int i = 0; i < encoded.length; i++) {
                        try {
                            encoded[i] = URLEncoder.encode(encoded[i], "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    int index = 0;
                    String buffer = paras[index] + ".";
                    String encodedBuffer = encoded[index] + "xxx";
                    index++;
                    boolean errors = false;
                    while (index < paras.length) {
                        while (index < paras.length) {
                            if ((encodedBuffer.length() + encoded[index].length()) > 1500) {
                                break;
                            } else {
                                buffer += paras[index] + ".";
                                encodedBuffer += "xxx" + encoded[index];
                                index++;
                            }
                        }
                        String translated = Translater.translate(buffer, fromLanguage, toLanguage);
                        if (translated == null) {
                            errors = true;
                            break;
                        } else {
                            body += translated;
                        }
                        if (index < paras.length) {
                            buffer = paras[index];
                            encodedBuffer = encoded[index];
                            index++;
                        } else {
                            buffer = "";
                        }
                    }
                    if (!buffer.equals("")) {
                        String translated = Translater.translate(buffer, fromLanguage, toLanguage);
                        if (translated == null) {
                            errors = true;
                        } else {
                            body += translated;
                        }
                    }

                    if (!errors) {
                        if (title != null)
                            news.setHeadLine(title);
                        if (body != null)
                            news.setBody(body);
                        if (realTimeNres) {
                            NewsStore.getSharedInstance().showNews(newsID);
                        } else {
                            SearchedNewsStore.getSharedInstance().showNews(newsID);
                        }
                    } else {
                        System.out.println("Errors occured");
                    }


                }
            }
        }.start();
    }

    private static String getBetSplitter(String text) {
        String splitterDot = "\\.";
        String splitterBR = "<BR>";
        String splitterP = "</R>";

        int dotMax = 0;
        String[] splitted = text.split(splitterDot);
        for (int i = 0; i < splitted.length; i++) {
            try {
                String s = URLEncoder.encode(splitted[i], "UTF-8");
                dotMax = Math.max(dotMax, s.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int brMax = 0;
        splitted = text.split(splitterBR);
        for (int i = 0; i < splitted.length; i++) {
            try {
                String s = URLEncoder.encode(splitted[i], "UTF-8");
                brMax = Math.max(brMax, s.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int pMax = 0;
        splitted = text.split(splitterP);
        for (int i = 0; i < splitted.length; i++) {
            try {
                String s = URLEncoder.encode(splitted[i], "UTF-8");
                pMax = Math.max(pMax, s.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (dotMax < brMax) {
            if (dotMax < pMax) {
                return splitterDot;
            } else {
                return splitterP;
            }
        } else {
            if (brMax < pMax) {
                return splitterBR;
            } else {
                return splitterP;
            }
        }
    }

    public static String getOptionTypeString(int type) {
        switch (type) {
            case 0:
                return Language.getString("OPTION_TYPE_PUT");
            case 1:
                return Language.getString("OPTION_TYPE_CALL");
            default:
                return Constants.DEFAULT_STRING;
        }
    }

    public static URLConnection getURLConnection(String strUrl) {
        URLConnection connection = null;
        if (BrowserManager.getInstance().isProxyAvailable()) {
            try {
                URL url = new URL(strUrl);
                connection = url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(BrowserManager.getInstance().getProxyIP(),
                        BrowserManager.getInstance().getProxyPort())));
                System.out.println("going through proxy...");
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            try {
                URL url = new URL(strUrl);
                connection = url.openConnection();
                System.out.println("no proxy...");
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return connection;
    }

    public static byte[] readFullResponse(URLConnection urlConnection) {
        try {
            urlConnection.setUseCaches(true);
//            urlConnection.setRequestProperty("Accept-Encoding", "gzip");
            String contentEncoding = urlConnection.getContentEncoding();
            byte[] temp = new byte[1000];
            if (contentEncoding != null) {
                contentEncoding = contentEncoding.trim();
            }
            System.out.println("VAS Menu Encoding: " + contentEncoding);
            if (contentEncoding != null) {
                if (contentEncoding.equalsIgnoreCase("gzip")) {
                    ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                    GZIPInputStream gzip = new GZIPInputStream(urlConnection.getInputStream());
                    while (true) {
                        int i = gzip.read(temp);
                        if (i >= 0) {
                            bOut.write(temp, 0, i);
                        } else {
                            break;
                        }
                    }
                    return bOut.toByteArray();
                } else if (contentEncoding.equalsIgnoreCase("deflate")) {
                    ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                    DeflaterInputStream deflate = new DeflaterInputStream(urlConnection.getInputStream());
                    while (true) {
                        int i = deflate.read(temp);
                        if (i >= 0) {
                            bOut.write(temp, 0, i);
                        } else {
                            break;
                        }
                    }
                    return bOut.toByteArray();
                } else {
                    InputStream gzip = urlConnection.getInputStream();
                    ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                    while (true) {
                        int i = gzip.read(temp);
                        if (i >= 0) {
                            bOut.write(temp, 0, i);
                        } else {
                            break;
                        }
                    }
                    return bOut.toByteArray();
                }
            } else {
                InputStream gzip = urlConnection.getInputStream();
                ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                while (true) {
                    int i = gzip.read(temp);
                    if (i >= 0) {
                        bOut.write(temp, 0, i);
                    } else {
                        break;
                    }
                }
                return bOut.toByteArray();
            }
        } catch (Exception e) {
            System.out.println("Exception in reading vas menu");
            e.printStackTrace();
        }
        return null;
    }

    public static String getMonthLocaleForDate(Date date) {
        try {
            String[] monthArray = Language.getList("SHORT_MONTH_LIST");
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int monthIndex = cal.get(Calendar.MONTH);
            return monthArray[monthIndex];
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean indexSortingOrderByDefault(String exchange) {
        sortProp = new Properties();
        boolean status = true;
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/sortorder.dll");
            sortProp.load(oIn);
            oIn.close();
            if (sortProp.containsKey(exchange)) {
                status = false;
            }
        } catch (Exception e) {

        }
        return status;
    }

    public static enum GoogleSection {
        Watchlist, PortfolioSimulator, Depth, DetailQuote, TimeNSales, Chart, ChartIndicator, ChartStrategy, Trading,
        Announcements, News, CorporateActions, Currency, Alert, Workspace, System, Tools, MetaStock, Reports, CashFlow, Scanner, StrategyMonitor
    }
}


