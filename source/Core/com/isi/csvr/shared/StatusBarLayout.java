package com.isi.csvr.shared;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 2, 2005
 * Time: 10:40:44 AM
 */
public class StatusBarLayout implements LayoutManager {

    private double maxWidth = 0;
    private double maxHeight = 0;
    private int top;
    private int left;
    private int bottom;
    private int right;

    private int componentCount = 0;
    private boolean ltr;

    public StatusBarLayout(boolean ltr) {
        this.ltr = ltr;
    }

    public void addLayoutComponent(String name, Component comp) {
        componentCount++;
        if (componentCount > 3) {
            throw new RuntimeException("StatusBar cannot have more than 3 components");
        }
    }

    private void calculateSizes(Container parent) {
        //maxWidth = 0;
        //maxHeight = 0;
        Border border = ((JPanel) parent).getBorder();
        if (border != null) {
            Insets insets = border.getBorderInsets(parent);
            top = insets.top;
            left = insets.left;
            bottom = insets.bottom;
            right = insets.right;
        } else {
            top = 0;
            left = 0;
            bottom = 0;
            right = 0;
        }
    }

    public void layoutContainer(Container parent) {
        calculateSizes(parent);
        Component[] components = parent.getComponents();
        /*for (int i = 0; i < 3; i++) {
            components[i].setBounds(left, y, (int)maxWidth, (int)components[i].getPreferredSize().getHeight());
            y +=components[i].getPreferredSize().getHeight();
        }*/

        try {
            if (ltr) { // letft to right
                Dimension size = components[0].getPreferredSize();
                Dimension parentSize = parent.getSize();
//                components[0].setBounds(left, top, (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = Math.max(maxHeight, size.getHeight());

                components[0].setBounds(left, ((int) maxHeight - (int) size.getHeight()) / 2, (int) size.getWidth(), (int) size.getHeight());

                size = components[1].getPreferredSize();
                components[1].setBounds((int) (parentSize.getWidth() - (int) size.getWidth()) / 2,
                        top, (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = Math.max(maxHeight, size.getHeight());

                size = components[2].getPreferredSize();
                components[2].setBounds((int) (parentSize.getWidth() - (int) size.getWidth() - right), top,
                        (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = Math.max(maxHeight, size.getHeight());

                size = null;
                parentSize = null;
                components = null;
            } else {
                Dimension size = components[2].getPreferredSize();
                Dimension parentSize = parent.getSize();
                components[2].setBounds(left, top, (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = size.getHeight();

                size = components[1].getPreferredSize();
                components[1].setBounds((int) (parentSize.getWidth() - (int) size.getWidth()) / 2,
                        top, (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = Math.max(maxHeight, size.getHeight());

                size = components[0].getPreferredSize();
                components[0].setBounds((int) (parentSize.getWidth() - (int) size.getWidth() - right),
                        ((int) maxHeight - (int) size.getHeight()) / 2,
                        (int) size.getWidth(), (int) size.getHeight());
                maxWidth += size.getWidth();
                maxHeight = Math.max(maxHeight, size.getHeight());

                size = null;
                parentSize = null;
                components = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dimension minimumLayoutSize(Container parent) {
        return new Dimension(0, 0);
    }

    public Dimension preferredLayoutSize(Container parent) {
        return new Dimension((int) maxWidth + left + right, (int) maxHeight + top + bottom);
    }

    public void removeLayoutComponent(Component comp) {

    }
}
