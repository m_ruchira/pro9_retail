package com.isi.csvr.shared;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 3, 2005
 * Time: 7:47:49 AM
 */
public class TWGradientImage implements Icon {
    public static final int MENU = 0;
    private int mode = MENU;
    public static final int BUTTON = 1;
    public static final int TITLEPANE = 2;
    private int width = 30;
    private int height = 10;
    private int halfheight = 5;
    private boolean ltr;

    public TWGradientImage(int width, int height, int mode) {
        this.width = width;
        this.height = height;
        this.halfheight = height / 2;
        this.mode = mode;
        ltr = Language.isLTR();
    }

    public TWGradientImage(int mode) {
        this.mode = mode;
        ltr = Language.isLTR();
    }

    public void flush() {

    }

    public int getIconHeight() {
        return height;
    }

    public int getIconWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
        this.halfheight = height / 2;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    /*public int getMode() {
        return mode;
    }*/

    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (mode == MENU) {
            if (ltr) {
                for (int i = 0; i <= width; i++) {
                    g.setColor(new Color((((Theme.MENU_GRADIENT1_COLOR.getRed() * i) + Theme.MENU_GRADIENT2_COLOR.getRed() * (width - i)) / width),
                            (((Theme.MENU_GRADIENT1_COLOR.getGreen() * i) + Theme.MENU_GRADIENT2_COLOR.getGreen() * (width - i)) / width),
                            (((Theme.MENU_GRADIENT1_COLOR.getBlue() * i) + Theme.MENU_GRADIENT2_COLOR.getBlue() * (width - i)) / width)));
                    g.drawLine(x + i, y, x + i, y + height);
                }
                g.setColor(Color.red);
//                g.drawLine(x+width,y,x+width,y+height);
            } else {
                for (int i = 0; i <= width; i++) {
                    g.setColor(new Color((((Theme.MENU_GRADIENT2_COLOR.getRed() * i) + Theme.MENU_GRADIENT1_COLOR.getRed() * (width - i)) / width),
                            (((Theme.MENU_GRADIENT2_COLOR.getGreen() * i) + Theme.MENU_GRADIENT1_COLOR.getGreen() * (width - i)) / width),
                            (((Theme.MENU_GRADIENT2_COLOR.getBlue() * i) + Theme.MENU_GRADIENT1_COLOR.getBlue() * (width - i)) / width)));
                    g.drawLine(x + i, y, x + i, y + height);
                }
            }
        } else if (mode == BUTTON) {
            Color lightColor;
            Color darkColor;

            if (c instanceof JButton) {
                lightColor = Theme.BUTTON_GRADIENT1_COLOR;
                darkColor = Theme.BUTTON_GRADIENT2_COLOR;
            } else { // if (c instanceof JButton)
                lightColor = ((TWButton) c).getGradientLight();
                darkColor = ((TWButton) c).getGradientDark();
            }


            for (int i = 0; i <= halfheight; i++) {
                g.setColor(new Color((((lightColor.getRed() * (halfheight - i)) + darkColor.getRed() * i) / halfheight),
                        (((lightColor.getGreen() * (halfheight - i)) + darkColor.getGreen() * i) / halfheight),
                        (((lightColor.getBlue() * (halfheight - i)) + darkColor.getBlue() * i) / halfheight)));
                g.drawLine(x, y + i, x + width, y + i);
            }
            for (int i = 0; i <= halfheight; i++) {
                g.setColor(new Color((((lightColor.getRed() * i) + darkColor.getRed() * (halfheight - i)) / halfheight),
                        (((lightColor.getGreen() * i) + darkColor.getGreen() * (halfheight - i)) / halfheight),
                        (((lightColor.getBlue() * i) + darkColor.getBlue() * (halfheight - i)) / halfheight)));
                g.drawLine(x, y + i + halfheight, x + width, y + i + halfheight);
            }
        } else {
            Color lightColor;
            lightColor = Theme.MENU_GRADIENT1_COLOR;

            Color darkColor;
            darkColor = Theme.MENU_GRADIENT2_COLOR;

            for (int i = 0; i <= halfheight; i++) {
                g.setColor(new Color((((lightColor.getRed() * (halfheight - i)) + darkColor.getRed() * i) / halfheight),
                        (((lightColor.getGreen() * (halfheight - i)) + darkColor.getGreen() * i) / halfheight),
                        (((lightColor.getBlue() * (halfheight - i)) + darkColor.getBlue() * i) / halfheight)));
                g.drawLine(x, y + i, x + width, y + i);
            }
            for (int i = 0; i <= halfheight; i++) {
                g.setColor(new Color((((lightColor.getRed() * i) + darkColor.getRed() * (halfheight - i)) / halfheight),
                        (((lightColor.getGreen() * i) + darkColor.getGreen() * (halfheight - i)) / halfheight),
                        (((lightColor.getBlue() * i) + darkColor.getBlue() * (halfheight - i)) / halfheight)));
                g.drawLine(x, y + i + halfheight, x + width, y + i + halfheight);
            }
        }
    }


}
