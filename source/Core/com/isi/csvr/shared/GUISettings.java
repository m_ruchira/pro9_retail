package com.isi.csvr.shared;
// Copyright (c) 2000 Home

import com.isi.csvr.Client;
import com.isi.csvr.chart.ChartSymbolNavigator;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.sideBar.SideBar;
import com.isi.csvr.table.IndexPanel;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.StringTokenizer;

public class GUISettings {

    public static final int INTERNAL_FRAME_TITLEBAR_HEIGHT = 23;

    public static final int SYMBOLBAR_LAYER = 200;
    public static final int TOP_LAYER = 10;
    public static final int DEFAULT_LAYER = 0;
    public static final int TICKER_LAYER = 50;
    public static final int INTERNAL_DIALOG_LAYER = 150;
    public static final int DESKTOP_ICON_LAYER = 10;
    private static final int DRAG_OFFSET_WIDTH = 15;
    private static final int DRAG_OFFSET_HEIGHT = 0;
    private static JPopupMenu currentPopup = null;
    private static BufferedImage bufferedImage = null;

    /**
     * Constructor
     */
    public GUISettings() {
    }

    public static void applyOrientation(Component c) {
        if (Language.isLTR()) {
            applyOrientation(c, ComponentOrientation.LEFT_TO_RIGHT);
        } else {
            applyOrientation(c, ComponentOrientation.RIGHT_TO_LEFT);
        }
    }

    /**
     * Change the compomnent orientation RTL or LTR
     */
    public static void applyOrientation(Component c, ComponentOrientation o) {

        try {
            c.setComponentOrientation(o);

            if (c instanceof JMenu) {
                JMenu menu = (JMenu) c;
                int ncomponents = menu.getMenuComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    applyOrientation(menu.getMenuComponent(i), o);
                }
            } else if (c instanceof javax.swing.JCheckBox) {
                javax.swing.JCheckBox obj = (javax.swing.JCheckBox) c;
                if (o == ComponentOrientation.LEFT_TO_RIGHT) {
                    obj.setHorizontalTextPosition(SwingConstants.RIGHT);
                    obj.setHorizontalAlignment(SwingConstants.LEFT);
                } else {
                    obj.setHorizontalTextPosition(SwingConstants.LEFT);
                    obj.setHorizontalAlignment(SwingConstants.RIGHT);
                }
            } else if (c instanceof javax.swing.JRadioButton) {
                javax.swing.JRadioButton obj = (javax.swing.JRadioButton) c;
                if (o == ComponentOrientation.LEFT_TO_RIGHT) {
                    obj.setHorizontalTextPosition(SwingConstants.RIGHT);
                    obj.setHorizontalAlignment(SwingConstants.LEFT);
                } else {
                    obj.setHorizontalTextPosition(SwingConstants.LEFT);
                    obj.setHorizontalAlignment(SwingConstants.RIGHT);
                }
            } else if (c instanceof java.awt.Container) {
                java.awt.Container container = (java.awt.Container) c;
                int ncomponents = container.getComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    try {
                        applyOrientation(container.getComponent(i), o);
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }


    /*public static void showPopup(JPopupMenu popup, Component invoker, int x, int y) {
        if (Settings.isScrollingOn())
            return;

        Dimension popupSize = popup.getPreferredSize();
        Point point = new Point(x, y);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = oToolkit.getScreenSize();

        SwingUtilities.convertPointToScreen(point, invoker);
        if ((point.getX() + popupSize.getWidth()) > screenSize.getWidth()) {
            x = (int) (x - popupSize.getWidth());
        }
        if ((point.getY() + popupSize.getHeight()) > screenSize.getHeight()) {
            y = 0;//(int) (y - popupSize.getHeight());
        }
        //System.out.println("x " + x);
        currentPopup = popup;
        popup.show(invoker, x, y);
    }*/

    public static void showPopup(JPopupMenu popup, Component invoker, int x, int y) {
        if (Settings.isScrollingOn())
            return;

        Dimension popupSize = popup.getPreferredSize();
        Point point = new Point(x, y);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = oToolkit.getScreenSize();

        SwingUtilities.convertPointToScreen(point, invoker);
        if ((point.getX() + popupSize.getWidth()) > screenSize.getWidth()) {
            x = (int) (x - popupSize.getWidth());
        }
        if ((point.getY() + popupSize.getHeight()) > (screenSize.getHeight())) {
            // y = 0;//(int) (y - popupSize.getHeight());
            point.y = (int) (screenSize.height - (popupSize.getHeight()));
        }
        //System.out.println("x " + x);
        SwingUtilities.convertPointFromScreen(point, invoker);
        currentPopup = popup;
        //   popup.show(invoker, x, y);
        popup.show(invoker, point.x, point.y);
    }

    public static boolean isAPopupMenuVisible() {
        try {
            return currentPopup.isVisible();
        } catch (Exception ex) {
            return false;
        }
    }

    public static void hideCurrentPopup() {
        try {
            currentPopup.setVisible(false);
        } catch (Exception ex) {
        }
    }

    public static void localizeFileChooserHomeButton(Container cont) {
        //System.out.println("localizing");
        if (!(cont instanceof Container)) {
            return;
        }
        int n = cont.getComponentCount();
        AbstractButton btn = null;

        for (int i = 0; i < n; i++) {
            Component comp = cont.getComponent(i);
            if (comp instanceof AbstractButton) {
                btn = (AbstractButton) comp;
                if ((btn.getToolTipText() != null) && (btn.getToolTipText().equals("Desktop"))) {
                    btn.setToolTipText(Language.getString("MY_DOCUMENTS"));
                    return;
                }
            } else if (comp instanceof Container) {
                localizeFileChooserHomeButton((Container) comp);
            }
        }
    }

    public static void createDragImage(String description, Graphics context) {
        Font font = Theme.getDefaultFont();
        FontMetrics fontMetrics = context.getFontMetrics(Theme.getDefaultFont());
        Rectangle2D stringRect = fontMetrics.getStringBounds(description, context);

        bufferedImage = new BufferedImage((int) stringRect.getWidth() + DRAG_OFFSET_WIDTH + 7,
                (int) stringRect.getHeight() + DRAG_OFFSET_HEIGHT + 4,
                BufferedImage.TYPE_4BYTE_ABGR);
        Graphics g = bufferedImage.getGraphics();
        g.setColor(Color.white);
        g.fillRect(DRAG_OFFSET_WIDTH, DRAG_OFFSET_HEIGHT, (int) stringRect.getWidth() + 6, (int) stringRect.getHeight() + 1);
        g.setColor(Color.black);
        g.drawRect(DRAG_OFFSET_WIDTH, DRAG_OFFSET_HEIGHT, (int) stringRect.getWidth() + 6, (int) stringRect.getHeight() + 1);
        g.drawString(description, DRAG_OFFSET_WIDTH + 2, (int) stringRect.getHeight() + DRAG_OFFSET_HEIGHT - 2);

        font = null;
        fontMetrics = null;
        stringRect = null;
        g = null;

    }

    public static BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public static void destroyBufferedImage() {
        bufferedImage = null;
    }

    public static void reloadGUIForNumberChange() {
        SwingUtilities.updateComponentTreeUI(Client.getInstance().getFrame());
        SwingUtilities.updateComponentTreeUI(SideBar.getSharedInstance());
        SwingUtilities.updateComponentTreeUI(ChartSymbolNavigator.getSharedInstance());
//        if(ChartSymbolNavigator.getSharedInstance().isVisible())
//            ChartSymbolNavigator.getSharedInstance().updateTree();
        IndexPanel.getInstance().refresh();
        IndexPanel.getInstance().updateMarketTime();
    }

    public static String arabize(String str) {

        char[] text = null;
        text = str.toCharArray();
        str = null;

        char base = '\u0660' - '\u0030';
        char minDigit = '\u0030';
        for (int i = 0, e = text.length; i < e; ++i) {
            char c = text[i];
            if (c >= minDigit && c <= '\u0039') {
                text[i] = (char) (c + base);
            }
        }
        return new String(text);
    }

    public static void setColumnSettings(ViewSetting oSetting, String sSettings) {


        try {
            oSetting.setColumnSettings(null);
            int iCount = oSetting.getColumnHeadings().length;
            int[][] aiColumnSettings = new int[iCount][3];
            int index = 0;
            try {
                StringTokenizer oTokens = new StringTokenizer(sSettings, ",");
                if ((oTokens.countTokens() % 3) == 0) {
                    for (index = 0; index < iCount; index++) {
                        aiColumnSettings[index][0] = SharedMethods.intValue(oTokens.nextToken());
                        aiColumnSettings[index][1] = SharedMethods.intValue(oTokens.nextToken());
                        aiColumnSettings[index][2] = SharedMethods.intValue(oTokens.nextToken());
                    }

                } else {
                    throw new Exception("Invalid number of tokens");
                }
                oTokens = null;
            } catch (Exception ex) {
                for (int i = index; i < iCount; i++) {
                    aiColumnSettings[i][0] = i;
                    aiColumnSettings[i][1] = 100;
                    aiColumnSettings[i][2] = i;
                }
            }
            oSetting.setColumnSettings(aiColumnSettings);
        } catch (Exception e) {
            System.out.println("OSettings ID==" + oSetting.getID());
            System.out.println("sSettings==" + sSettings);
            e.printStackTrace();
        }
    }

    public static void setSameSize(JComponent comp1, JComponent comp2) {
        double width;
        double height;
        height = Math.max(comp1.getPreferredSize().getHeight(), comp2.getPreferredSize().getHeight());
        width = Math.max(comp1.getPreferredSize().getWidth(), comp2.getPreferredSize().getWidth());

        Dimension size = new Dimension((int) width, (int) height);
        comp1.setPreferredSize(size);
        comp2.setPreferredSize(size);
    }

    public static void setLocationRelativeTo(Component source, Component reference) {
        Dimension parentSize = reference.getSize();
        if (Settings.isDualScreenMode() && (Settings.isMultiScreenEnvr())) {
            if (Settings.isLeftAlignPopups()) {
                source.setBounds((int) ((parentSize.getWidth() / 2 - source.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - source.getHeight()) / 2),
                        source.getWidth(), source.getHeight());
            } else {
                source.setBounds((int) ((parentSize.getWidth() / 2) + (parentSize.getWidth() / 2 - source.getWidth()) / 2),
                        (int) ((parentSize.getHeight() - source.getHeight()) / 2),
                        source.getWidth(), source.getHeight());
            }
        } else {
            if (reference instanceof JDesktopPane) {
                source.setBounds((int) Math.max((parentSize.getWidth() - source.getWidth()) / 2, 0),
                        (int) Math.max((parentSize.getHeight() - source.getHeight()) / 2, 0),
                        source.getWidth(), source.getHeight());
            } else {
                source.setBounds(reference.getX() + (int) ((parentSize.getWidth() - source.getWidth()) / 2),
                        reference.getY() + (int) ((parentSize.getHeight() - source.getHeight()) / 2),
                        source.getWidth(), source.getHeight());
            }
        }
    }


    public static Border getTWFrameBorder() {
        return BorderFactory.createLineBorder(Theme.getColor("INTERNAL_FRAME_BORDER_COLOR"), 2);
    }

    public static Border getTWMainBoardFrameBorder() {
        return BorderFactory.createLineBorder(Theme.getColor("INTERNAL_MAINBOARD_BORDER_COLOR"), 2);
    }

    public static void clearBorders(JComponent component) {
        for (int i = 0; i < component.getComponentCount(); i++) {
            if (component.getComponent(i) instanceof AbstractButton) {
                ((AbstractButton) component.getComponent(i)).setBorderPainted(false);
            }
        }
    }
}