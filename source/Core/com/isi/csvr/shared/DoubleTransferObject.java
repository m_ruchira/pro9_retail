package com.isi.csvr.shared;

public class DoubleTransferObject implements TransferObject {
    private double value;
    private long flag;
    private boolean blank;

    public DoubleTransferObject() {
    }

    public double getValue() {
        return value;
    }

    public DoubleTransferObject setValue(double value) {
        this.value = value;
        return this;
    }

    public long getFlag() {
        return flag;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }


    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }
}