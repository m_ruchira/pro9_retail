package com.isi.csvr.shared;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

// Copyright (c) 2000 Home

import java.io.FileInputStream;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * A Class class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class TWColumnSettings {

    private static Properties prop;
    private static int[] g_aiColWidths = null;

    /**
     * Constructor
     */
    public TWColumnSettings() {
        try {
            prop = new Properties();
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/cl.dll");
            prop.load(oIn);
            oIn.close();
            g_aiColWidths = loadColWidths();
        } catch (Exception s_e) {
            prop = new Properties();
            //System.out.println(s_e+" error in loading Settings file");
        }
    }

    public static int[] getColWidths() {
        return g_aiColWidths;
    }

    public static int[] loadColWidths() {
        int aiCols[] = null;
        int i = 0;

        try {
            StringTokenizer oTokens = new StringTokenizer((String) prop.get("MAIN_BOARD_COLS"), ",");
            aiCols = new int[oTokens.countTokens() / 3];

            while (oTokens.hasMoreTokens()) {
                try {
                    oTokens.nextToken();
                    aiCols[i] = Integer.parseInt(oTokens.nextToken());
                    oTokens.nextToken();
                    i++;
                } catch (Exception e) {
                    return null;
                }
            }
        } catch (Exception e) {
            return null;
        }

        return aiCols;
    }

    public static String getItem(String sItem) {
        String sValue = "";

        sValue = prop.getProperty(sItem);
        return sValue;
    }

}


