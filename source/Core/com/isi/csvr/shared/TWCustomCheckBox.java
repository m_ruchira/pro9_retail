package com.isi.csvr.shared;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 2, 2005
 * Time: 9:06:18 AM
 */
public class TWCustomCheckBox extends JLabel implements ButtonModel, MouseListener {
    public static final int CHECK_ONLY = 0;
    public static final int CHECK_UNCHECK = 1;
    private boolean selected;
    private int type;
    private String tag;

    public TWCustomCheckBox(String text, int horizontalAlignment, int type) {
        super(text, new TWCheckBoxIcon(), horizontalAlignment);
        // super(text,  horizontalAlignment);
        super.setDisabledIcon(super.getIcon());
        super.addMouseListener(this);
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        repaint();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void addActionListener(ActionListener l) {
    }

    public void addChangeListener(ChangeListener l) {

    }

    public void addItemListener(ItemListener l) {

    }

    public String getActionCommand() {
        return null;
    }

    public void setActionCommand(String s) {

    }

    public int getMnemonic() {
        return 0;
    }

    public void setMnemonic(int key) {

    }

    public Object[] getSelectedObjects() {
        return new Object[0];
    }

    public boolean isArmed() {
        return false;
    }

    public void setArmed(boolean b) {

    }

    public boolean isPressed() {
        return false;
    }

    public void setPressed(boolean b) {

    }

    public boolean isRollover() {
        return false;
    }

    public void setRollover(boolean b) {

    }

    public void removeActionListener(ActionListener l) {

    }

    public void removeChangeListener(ChangeListener l) {

    }

    public void removeItemListener(ItemListener l) {

    }

    public void setGroup(ButtonGroup group) {

    }

    public void mouseClicked(MouseEvent e) {
        if (isEnabled()) {
            if (type == CHECK_ONLY) {
                setSelected(true);
            } else {
                setSelected(!isSelected());
            }
            repaint();
        }
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }
}


