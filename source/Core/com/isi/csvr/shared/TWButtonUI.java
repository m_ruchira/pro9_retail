package com.isi.csvr.shared;


import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 10, 2005
 * Time: 9:38:26 PM
 */
public class TWButtonUI extends MetalButtonUI {

    private final static TWButtonUI metalButtonUI = new TWButtonUI();
    private final static TWGradientImage gradientImage = new TWGradientImage(0, 0, TWGradientImage.BUTTON);

    // ********************************
    //          Create PLAF
    // ********************************
    public static ComponentUI createUI(JComponent c) {
        return metalButtonUI;
    }

    // ********************************
    //          Install
    // ********************************
    public void installDefaults(AbstractButton b) {
        super.installDefaults(b);
    }

    public void uninstallDefaults(AbstractButton b) {
        super.uninstallDefaults(b);
    }

    /*public void paint(Graphics g, JComponent c) {

        if ((c instanceof TWButton) && (((TWButton) c).isGradientPainted())
                && (!((TWButton) c).getModel().isPressed())) {
            gradientImage.setWidth(c.getWidth());
            gradientImage.setHeight(c.getHeight());
            gradientImage.paintIcon(c, g, 0, 0);
        *//*} else if ((c instanceof JButton) && (((JButton) c).isContentAreaFilled())) {
            gradientImage.setWidth(c.getWidth());
            gradientImage.setHeight(c.getHeight());
            gradientImage.paintIcon(c, g, 0, 0);*//*
        }
        super.paint(g, c);
    }*/

    public void paint(Graphics g, JComponent c) {

        if ((c instanceof TWButton) && (((TWButton) c).isGradientPainted())
                && (!((TWButton) c).getModel().isPressed())) {
            gradientImage.setWidth(c.getWidth());
            gradientImage.setHeight(c.getHeight());
            gradientImage.paintIcon(c, g, 0, 0);
        } else if ((c instanceof JButton) && (((JButton) c).isContentAreaFilled())) {
            if (UIManager.get("Button.background").equals(c.getBackground())) { // if background color not set, use gradient
                gradientImage.setWidth(c.getWidth());
                gradientImage.setHeight(c.getHeight());
                gradientImage.paintIcon(c, g, 0, 0);
            }
        }
        super.paint(g, c);
    }

    protected void paintText(Graphics g, JComponent c, Rectangle textRect, String text) {
        AbstractButton b = (AbstractButton) c;
        ButtonModel model = b.getModel();
        FontMetrics fm = SwingUtilities2.getFontMetrics(c, g);
        int mnemIndex = b.getDisplayedMnemonicIndex();

        /* Draw the Text */
        if (model.isEnabled()) {
            /*** paint the text normally */
            g.setColor(b.getForeground());
        } else {
            /*** paint the text disabled ***/
            g.setColor(getDisabledTextColor());
        }
        SwingUtilities2.drawStringUnderlineCharAt(c, g, text, mnemIndex,
                textRect.x, textRect.y + fm.getAscent());
    }
}
