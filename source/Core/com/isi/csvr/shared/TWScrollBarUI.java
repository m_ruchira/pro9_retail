package com.isi.csvr.shared;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalScrollBarUI;
import javax.swing.plaf.metal.MetalScrollButton;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Oct 25, 2008
 * Time: 4:08:16 PM
 */

public class TWScrollBarUI extends MetalScrollBarUI {
    boolean rtl;
    private Image decreseButton;
    private Image increseButton;
    private Image trackUp;
    private Image trackDown;
    private Image trackCenter;
    private Image thumbUp;
    private Image thumbDown;
    private Image thumbCenter;
    private Image disabledDecreseButton;
    private Image disabledIncreseButton;
    private Image disabledTrackUp;
    private Image disabledTrackDown;
    private Image disabledTrackCenter;
    private Image disabledThumbUp;
    private Image disabledThumbDown;
    private Image disabledThumbCenter;
    private int trackUpHeight;
    private int trackDownHeight;
    private int trackCenterHeight;
    private int trackDownWidth;
    private int thumbUpHeight;
    private int thumbDownHeight;
    private int thumbCenterHeight;

    public static ComponentUI createUI(JComponent c) {
        return new TWScrollBarUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        rtl = scrollbar.getComponentOrientation() == ComponentOrientation.RIGHT_TO_LEFT;

        decreseButton = ((ImageIcon) (UIManager.getIcon("ScrollBar.buttonIcon"))).getImage();
        disabledDecreseButton = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledButtonIcon"))).getImage();
        if (scrollbar.getOrientation() == JScrollBar.HORIZONTAL) {
            if (rtl) {
                decreseButton = SharedMethods.rotateImage(decreseButton, 90);
                disabledDecreseButton = SharedMethods.rotateImage(disabledDecreseButton, 90);
            } else {
                decreseButton = SharedMethods.rotateImage(decreseButton, -90);
                disabledDecreseButton = SharedMethods.rotateImage(disabledDecreseButton, -90);
            }
        }

        increseButton = ((ImageIcon) (UIManager.getIcon("ScrollBar.buttonIcon"))).getImage();
        disabledIncreseButton = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledButtonIcon"))).getImage();
        if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
            increseButton = SharedMethods.rotateImage(increseButton, 180);
            disabledIncreseButton = SharedMethods.rotateImage(disabledIncreseButton, 180);
        } else {
            if (rtl) {
                increseButton = SharedMethods.rotateImage(increseButton, -90);
                disabledIncreseButton = SharedMethods.rotateImage(disabledIncreseButton, -90);
            } else {
                increseButton = SharedMethods.rotateImage(increseButton, 90);
                disabledIncreseButton = SharedMethods.rotateImage(disabledIncreseButton, 90);

            }
        }

        if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
            trackUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackUpIcon"))).getImage();
            disabledTrackUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackUpIcon"))).getImage();
            trackUpHeight = trackUp.getHeight(incrButton);
            trackDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackDownIcon"))).getImage();
            disabledTrackDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackDownIcon"))).getImage();
            trackDownHeight = trackDown.getHeight(incrButton);
            trackDownWidth = trackDown.getWidth(incrButton);
            trackCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackIcon"))).getImage();
            disabledTrackCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackIcon"))).getImage();
            trackCenterHeight = trackCenter.getHeight(incrButton);

            thumbUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbUpIcon"))).getImage();
            disabledThumbUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbUpIcon"))).getImage();
            thumbUpHeight = thumbUp.getHeight(incrButton);
            thumbDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbDownIcon"))).getImage();
            disabledThumbDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbDownIcon"))).getImage();
            thumbDownHeight = thumbDown.getHeight(incrButton);
            thumbCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbIcon"))).getImage();
            disabledThumbCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbIcon"))).getImage();
            thumbCenterHeight = thumbCenter.getHeight(incrButton);
        } else {
            trackUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackUpIcon"))).getImage();
            disabledTrackUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackUpIcon"))).getImage();
            trackUpHeight = trackUp.getHeight(incrButton);
            trackUp = SharedMethods.rotateImage(trackUp, -90);
            disabledTrackUp = SharedMethods.rotateImage(disabledTrackUp, -90);

            trackDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackDownIcon"))).getImage();
            disabledTrackDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackDownIcon"))).getImage();
            trackDown = SharedMethods.rotateImage(trackDown, -90);
            disabledTrackDown = SharedMethods.rotateImage(disabledTrackDown, -90);
            trackDownHeight = trackDown.getHeight(incrButton);
            trackDownWidth = trackDown.getWidth(incrButton);
            trackCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.trackIcon"))).getImage();
            disabledTrackCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledTrackIcon"))).getImage();
            trackCenterHeight = trackCenter.getHeight(incrButton);
            trackCenter = SharedMethods.rotateImage(trackCenter, -90);
            disabledTrackCenter = SharedMethods.rotateImage(disabledTrackCenter, -90);

            thumbUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbUpIcon"))).getImage();
            disabledThumbUp = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbUpIcon"))).getImage();
            thumbUpHeight = thumbUp.getHeight(incrButton);
            thumbUp = SharedMethods.rotateImage(thumbUp, -90);
            disabledThumbUp = SharedMethods.rotateImage(disabledThumbUp, -90);
            thumbDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbDownIcon"))).getImage();
            disabledThumbDown = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbDownIcon"))).getImage();
            thumbDownHeight = thumbDown.getHeight(incrButton);
            thumbDown = SharedMethods.rotateImage(thumbDown, -90);
            disabledThumbDown = SharedMethods.rotateImage(disabledThumbDown, -90);
            thumbCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.thumbIcon"))).getImage();
            disabledThumbCenter = ((ImageIcon) (UIManager.getIcon("ScrollBar.disabledThumbIcon"))).getImage();
            thumbCenterHeight = thumbCenter.getHeight(incrButton);
            thumbCenter = SharedMethods.rotateImage(thumbCenter, -90);
            disabledThumbCenter = SharedMethods.rotateImage(disabledThumbCenter, -90);
        }
    }

    /**
     * Returns the view that represents the decrease view.
     */
    protected JButton createDecreaseButton(int orientation) {
        decreaseButton = new MetalScrollButton(orientation, scrollBarWidth, isFreeStanding) {
            public void paint(Graphics g) {
                if (getParent().isEnabled()) {
                    g.drawImage(decreseButton, 0, 0, this);
                } else {
                    g.drawImage(disabledDecreseButton, 0, 0, this);
                }
            }
        };
        decreaseButton.setIcon(null);
        return decreaseButton;
    }

    /**
     * Returns the view that represents the increase view.
     */
    protected JButton createIncreaseButton(int orientation) {
        increaseButton = new MetalScrollButton(orientation, scrollBarWidth, isFreeStanding) {
            public void paint(Graphics g) {
                if (getParent().isEnabled()) {
                    g.drawImage(increseButton, 0, 0, this);
                } else {
                    g.drawImage(disabledIncreseButton, 0, 0, this);
                }
            }
        };
        return increaseButton;
    }

    protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {

        if (c.isEnabled()) {
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(trackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight) {
                    g.drawImage(trackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(trackDown, trackBounds.x, trackBounds.y + (int) trackBounds.getHeight() - trackDownHeight, incrButton);
            } else {
                g.drawImage(trackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.x + trackUpHeight; i < trackBounds.x + trackBounds.getWidth() - trackDownWidth; i += trackCenterHeight) {
                    g.drawImage(trackCenter, i, trackBounds.y, incrButton);
                }
                g.drawImage(trackDown, trackBounds.x + (int) trackBounds.getWidth() - trackDownWidth, trackBounds.y, incrButton);
            }
        } else {
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(disabledTrackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight) {
                    g.drawImage(disabledTrackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(disabledTrackDown, trackBounds.x, trackBounds.y + (int) trackBounds.getHeight() - trackDownHeight, incrButton);
            } else {
                g.drawImage(disabledTrackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.x + trackUpHeight; i < trackBounds.x + trackBounds.getWidth() - trackDownWidth; i += trackCenterHeight) {
                    g.drawImage(disabledTrackCenter, i, trackBounds.y, incrButton);
                }
                g.drawImage(disabledTrackDown, trackBounds.x + (int) trackBounds.getWidth() - trackDownWidth, trackBounds.y, incrButton);
            }
        }
    }

    /*protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {

        if (c.isEnabled()){
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(trackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight)
                {
                    g.drawImage(trackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(trackDown, trackBounds.x, trackBounds.y + (int) trackBounds.getHeight() - trackDownHeight, incrButton);
            } else {
                g.drawImage(trackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight)
                {
                    g.drawImage(trackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(trackDown, trackBounds.x + (int) trackBounds.getWidth() - trackDownWidth, trackBounds.y, incrButton);
            }
        } else {
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(disabledTrackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight)
                {
                    g.drawImage(disabledTrackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(disabledTrackDown, trackBounds.x, trackBounds.y + (int) trackBounds.getHeight() - trackDownHeight, incrButton);
            } else {
                g.drawImage(disabledTrackUp, trackBounds.x, trackBounds.y, incrButton);
                for (int i = trackBounds.y + trackUpHeight; i < trackBounds.y + trackBounds.getHeight() - trackDownHeight; i += trackCenterHeight)
                {
                    g.drawImage(disabledTrackCenter, trackBounds.x, i, incrButton);
                }
                g.drawImage(disabledTrackDown, trackBounds.x + (int) trackBounds.getWidth() - trackDownWidth, trackBounds.y, incrButton);
            }
        }
    }*/

    protected void paintThumb(Graphics g, JComponent c, Rectangle thumbBounds) {
        if (c.isEnabled()) {
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(thumbUp, thumbBounds.x, thumbBounds.y, incrButton);
                for (int i = thumbBounds.y + trackUpHeight; i < thumbBounds.y + thumbBounds.getHeight() - thumbDownHeight; i += thumbCenterHeight) {
                    g.drawImage(thumbCenter, thumbBounds.x, i, incrButton);
                }
                g.drawImage(thumbDown, thumbBounds.x, thumbBounds.y + (int) thumbBounds.getHeight() - thumbDownHeight, incrButton);
            } else {
                g.drawImage(thumbUp, thumbBounds.x, thumbBounds.y, incrButton);
                for (int i = thumbBounds.x + thumbUpHeight; i < thumbBounds.x + thumbBounds.getWidth() - thumbDownHeight; i += thumbCenterHeight) {
                    g.drawImage(thumbCenter, i, thumbBounds.y, incrButton);
                }
                g.drawImage(thumbDown, thumbBounds.x + (int) thumbBounds.getWidth() - thumbDownHeight, thumbBounds.y, incrButton);
            }
        } else {
            if (scrollbar.getOrientation() == JScrollBar.VERTICAL) {
                g.drawImage(disabledThumbUp, thumbBounds.x, thumbBounds.y, incrButton);
                for (int i = thumbBounds.y + trackUpHeight; i < thumbBounds.y + thumbBounds.getHeight() - thumbDownHeight; i += thumbCenterHeight) {
                    g.drawImage(disabledThumbCenter, thumbBounds.x, i, incrButton);
                }
                g.drawImage(disabledThumbDown, thumbBounds.x, thumbBounds.y + (int) thumbBounds.getHeight() - thumbDownHeight, incrButton);
            } else {
                g.drawImage(disabledThumbUp, thumbBounds.x, thumbBounds.y, incrButton);
                for (int i = thumbBounds.x + thumbUpHeight; i < thumbBounds.x + thumbBounds.getWidth() - thumbDownHeight; i += thumbCenterHeight) {
                    g.drawImage(disabledThumbCenter, i, thumbBounds.y, incrButton);
                }
                g.drawImage(disabledThumbDown, thumbBounds.x + (int) thumbBounds.getWidth() - thumbDownHeight, thumbBounds.y, incrButton);
            }
        }
    }
}