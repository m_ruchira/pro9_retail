package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 13, 2005
 * Time: 2:17:33 PM
 */

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Metal implementation of JInternalFrame.
 * <p/>
 *
 * @author Steve Wilson
 * @version 1.30 12/19/03
 */
public class TWInternalFrameUI extends BasicInternalFrameUI {

    private static final PropertyChangeListener metalPropertyChangeListener =
            new MetalPropertyChangeHandler();
    private static final Border handyEmptyBorder = new EmptyBorder(0, 0, 0, 0);
    protected static String IS_PALETTE = "JInternalFrame.isPalette";
    private static String FRAME_TYPE = "JInternalFrame.frameType";
    private static String NORMAL_FRAME = "normal";
    private static String PALETTE_FRAME = "palette";
    private static String OPTION_DIALOG = "optionDialog";
    private TWInternalFrameTitlePane titlePane;

    public TWInternalFrameUI(JInternalFrame b) {
        super(b);
    }

    public static ComponentUI createUI(JComponent c) {
        return new TWInternalFrameUI((JInternalFrame) c);
    }

    public void installUI(JComponent c) {
        super.installUI(c);

        Object paletteProp = c.getClientProperty(IS_PALETTE);
        if (paletteProp != null) {
            setPalette(((Boolean) paletteProp).booleanValue());
        }

        Container content = frame.getContentPane();
        stripContentBorder(content);
        //c.setOpaque(false);
    }

    public void uninstallUI(JComponent c) {
        frame = (JInternalFrame) c;

        Container cont = ((JInternalFrame) (c)).getContentPane();
        if (cont instanceof JComponent) {
            JComponent content = (JComponent) cont;
            if (content.getBorder() == handyEmptyBorder) {
                content.setBorder(null);
            }
        }
        super.uninstallUI(c);
    }

    protected void installListeners() {
        super.installListeners();
        frame.addPropertyChangeListener(metalPropertyChangeListener);
    }

    protected void uninstallListeners() {
        frame.removePropertyChangeListener(metalPropertyChangeListener);
        super.uninstallListeners();
    }

    protected void installKeyboardActions() {
        super.installKeyboardActions();
        ActionMap map = SwingUtilities.getUIActionMap(frame);
        if (map != null) {
            // BasicInternalFrameUI creates an action with the same name, we override
            // it as Metal frames do not have system menus.
            map.remove("showSystemMenu");
        }
    }

    protected void uninstallKeyboardActions() {
        super.uninstallKeyboardActions();
    }

    protected void uninstallComponents() {
        super.uninstallComponents();
        //titlePane.uninstallDefaults();
        titlePane = null;
    }

    private void stripContentBorder(Object c) {
        if (c instanceof JComponent) {
            JComponent contentComp = (JComponent) c;
            Border contentBorder = contentComp.getBorder();
            if (contentBorder == null || contentBorder instanceof UIResource) {
                contentComp.setBorder(handyEmptyBorder);
            }
        }
    }


    protected JComponent createNorthPane(JInternalFrame w) {
        titlePane = new TWInternalFrameTitlePane(w);
        return titlePane;
    }


    private void setFrameType(String frameType) {
        if (frameType.equals(OPTION_DIALOG)) {
            LookAndFeel.installBorder(frame, "InternalFrame.optionDialogBorder");
            titlePane.setPalette(false);
        } else if (frameType.equals(PALETTE_FRAME)) {
            LookAndFeel.installBorder(frame, "InternalFrame.paletteBorder");
            titlePane.setPalette(true);
        } else {
            LookAndFeel.installBorder(frame, "InternalFrame.border");
            titlePane.setPalette(false);
        }
    }

    // this should be deprecated - jcs
    public void setPalette(boolean isPalette) {
        if (isPalette) {
            LookAndFeel.installBorder(frame, "InternalFrame.paletteBorder");
        } else {
            LookAndFeel.installBorder(frame, "InternalFrame.border");
        }
        titlePane.setPalette(isPalette);

    }

    private static class MetalPropertyChangeHandler implements
            PropertyChangeListener {
        public void propertyChange(PropertyChangeEvent e) {
            String name = e.getPropertyName();
            JInternalFrame jif = (JInternalFrame) e.getSource();

            if (!(jif.getUI() instanceof TWInternalFrameUI)) {
                return;
            }

            TWInternalFrameUI ui = (TWInternalFrameUI) jif.getUI();

            if (name.equals(FRAME_TYPE)) {
                if (e.getNewValue() instanceof String) {
                    ui.setFrameType((String) e.getNewValue());
                }
            } else if (name.equals(IS_PALETTE)) {
                if (e.getNewValue() != null) {
                    ui.setPalette(((Boolean) e.getNewValue()).booleanValue());
                } else {
                    ui.setPalette(false);
                }
            } else if (name.equals(JInternalFrame.CONTENT_PANE_PROPERTY)) {
                ui.stripContentBorder(e.getNewValue());
            }
        }
    } // end class MetalPropertyChangeHandler
}


