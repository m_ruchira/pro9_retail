package com.isi.csvr.shared;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 2, 2005
 * Time: 4:54:35 PM
 */
public class RequestCounter {

    private Hashtable counterStore;

    public RequestCounter() {
        counterStore = new Hashtable();
    }

    public synchronized int increment(String key) {
        Integer count = (Integer) counterStore.get(key);

        if (count == null) {
            count = new Integer(1);
        } else {
            count = new Integer(count.intValue() + 1);
        }
        counterStore.put(key, count);

        return count.intValue();
    }

    public synchronized int decrement(String key) {
        Integer count = (Integer) counterStore.get(key);

        if (count != null) {
            if (count.intValue() == 1) {
                counterStore.remove(key);
                return 0;
            } else {
                count = new Integer(count.intValue() - 1);
                counterStore.put(key, count);
                return count.intValue();
            }
        }
        return 0;
    }

    public Enumeration getKeys() {
        return counterStore.keys();
    }

    public int getCountFor(String key) {
        try {
            return ((Integer) counterStore.get(key)).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public void clear() {
        counterStore.clear();
    }
}
