package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 1, 2005
 * Time: 12:14:09 PM
 */
public class Symbol {
    private String symbol;
    private String exchange;
    private int instrument;
    private String key;

    public Symbol(String exchange, String symbol, int instrument) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.instrument = instrument;
        key = SharedMethods.getKey(exchange.toUpperCase(), symbol.toUpperCase(), instrument);
    }

    public String getExchange() {
        return exchange;
    }

    public String getKey() {
        return key;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
        key = SharedMethods.getKey(exchange.toUpperCase(), symbol.toUpperCase(), instrument);
//        key = exchange.toUpperCase() + Constants.KEY_SEPERATOR_CHARACTER + symbol.toUpperCase();
    }

    public int getInstrumentType() {
        return instrument;
    }

    public void setInstrumentType(int instrument) {
        this.instrument = instrument;
        key = SharedMethods.getKey(exchange.toUpperCase(), symbol.toUpperCase(), instrument);
    }
}
