// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.shared;

/*
* Converts unicode strings to a native format
* Author: Uditha Nagahawatta
* Date  : 23 Jun 2001
*/

public class UnicodeUtils {
    /*
    * Returns a native string using the
    * unicode string.
    */


    public static String getNativeStringFromCompressed(String sUnicode) {
        int i = 0;
        int buffindex = 0;
        char[] buf = new char[sUnicode.length()];
        int iLen = 0;
        char ch;
        char next;
        String prev = "";

        iLen = sUnicode.length();
        boolean isUnicode = false;
        while (i < iLen) {
            ch = getNext(sUnicode, i++);
            if (ch == '\\') {
                if ((next = getNext(sUnicode, i++)) == 'u') {
                    isUnicode = true;
                    if ((iLen - i) >= 4) {
                        buf[buffindex++] = processUnicode(sUnicode.substring(i, i + 4));
                        prev = sUnicode.substring(i, i + 2);

                        i += 4;
                    } else {
                        buf[buffindex++] = '\\';
                        buf[buffindex++] = 'u';
                        while (i < iLen) buf[buffindex++] = sUnicode.charAt(i++);
                        i = iLen;
                    }
                } else if (next == -1) {
                    return (new String(buf, 0, buffindex));
                } else {
                    buf[buffindex++] = '\\';
                    i--;
                    //buf[buffindex++] = next;
                }
            } else {
                if (isUnicode) {
                    if ((iLen - (i - 1)) >= 2) {
                        String nex = prev + sUnicode.substring(i - 1, i + 1);
                        buf[buffindex++] = processUnicode(nex);
                        i += 1;
                    } else if (getNext(sUnicode, i++) == -1) {
                        return (new String(buf, 0, buffindex));
                    }
                } else {
//                    System.out.println("char ==="+ch);
                    buf[buffindex++] = ch;
//                    i += 1;
                }
            }
        }
        return (new String(buf, 0, buffindex));
    }

    public static String getNativeString(String sUnicode) {
        int i = 0;
        int buffindex = 0;
        char[] buf = new char[sUnicode.length()];
        int iLen = 0;
        char ch;
        char next;

        iLen = sUnicode.length();

        while (i < iLen) {
            ch = getNext(sUnicode, i++);
            if (ch == '\\') {
                if ((next = getNext(sUnicode, i++)) == 'u') {
                    if ((iLen - i) >= 4) {
                        buf[buffindex++] = processUnicode(sUnicode.substring(i, i + 4));
                        i += 4;
                    } else {
                        buf[buffindex++] = '\\';
                        buf[buffindex++] = 'u';
                        while (i < iLen) buf[buffindex++] = sUnicode.charAt(i++);
                        i = iLen;
                    }
                } else if (next == -1) {
                    return (new String(buf, 0, buffindex));
                } else {
                    buf[buffindex++] = '\\';
                    i--;
                    //buf[buffindex++] = next;
                }
            } else {
                buf[buffindex++] = ch;
            }
        }
        return (new String(buf, 0, buffindex));
    }

    public static String getAsciiString(String sUnicode) {
        int i = 0;
        int buffindex = 0;
        StringBuffer buf = new StringBuffer();
        int iLen = 0;
        char ch;
        char next;

        iLen = sUnicode.length();

        while (i < iLen) {
            ch = getNext(sUnicode, i++);
            if (ch == '\\') {
                if ((next = getNext(sUnicode, i++)) == 'u') {
                    if ((iLen - i) >= 4) {
                        //buf[buffindex++] = processAscii(sUnicode.substring(i,i+4));
                        buf.append(processAscii(sUnicode.substring(i, i + 4)));
                        i += 4;
                    } else {
                        //buf[buffindex++] = '\\';
                        //buf[buffindex++] = 'u';
                        buf.append("\\u");
                        while (i < iLen) buf.append(sUnicode.charAt(i++));
                        i = iLen;
                    }
                } else if (next == -1) {
                    return buf.toString();
                } else {
                    buf.append('\\');
                    i--;
                    //buf[buffindex++] = next;
                }
            } else {
                buf.append(ch);
            }
        }
        return buf.toString();
    }

    /*
    * Returns the next char from the string
    */
    private static char getNext(String sUnicode, int i) {
        if (i < sUnicode.length())
            return sUnicode.charAt(i);
        else
            return (char) -1;
    }

    /*
    * Converts the 4 digit code to the native char
    */
    private static char processUnicode(String sUnicode) {
        char ch;
        int d = 0;
        loop:
        for (int i = 0; i < 4; i++) {
            ch = sUnicode.charAt(i);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    d = (d << 4) + ch - '0';
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    d = (d << 4) + 10 + ch - 'a';
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    d = (d << 4) + 10 + ch - 'A';
                    break;
                default:
                    break loop;
            }
        }
        return (char) d;
    }

    private static String processAscii(String sUnicode) {
        char ch;
        int d = 0;
        loop:
        for (int i = 0; i < 4; i++) {
            ch = sUnicode.charAt(i);
            switch (ch) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    d = (d << 4) + ch - '0';
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    d = (d << 4) + 10 + ch - 'a';
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    d = (d << 4) + 10 + ch - 'A';
                    break;
                default:
                    break loop;
            }
        }
        return "&#" + (int) d + ";";
    }

    public static String getCompressedUnicodeString(String sNative) {
        try {
            String prev = "";
            StringBuffer sUnicode = new StringBuffer("");
            char[] acNative = sNative.toCharArray();

            for (int i = 0; i < acNative.length; i++) {
                String str = charToHex(acNative[i]);
                String next = str.substring(0, 2);
                if (next.equals(prev)) {
                    sUnicode.append(str.substring(2));
                } else {
                    sUnicode.append("\\u");
                    sUnicode.append(str);
                    prev = next;
                }
            }
            return sUnicode.toString();
        } catch (Exception ex) {
            return Constants.NULL_STRING;
        }
    }

    public static String getUnicodeString(String sNative) {
        try {
            StringBuffer sUnicode = new StringBuffer("");
            char[] acNative = sNative.toCharArray();

            for (int i = 0; i < acNative.length; i++) {
                sUnicode.append("\\u");
                sUnicode.append(charToHex(acNative[i]));
            }

            return sUnicode.toString();
        } catch (Exception ex) {
            return Constants.NULL_STRING;
        }
    }

    public static String charToHex(char c) {
// Returns hex String representation of char c
        byte hi = (byte) (c >>> 8);
        byte lo = (byte) (c & 0xff);
        return byteToHex(hi) + byteToHex(lo);
    }

    static public String byteToHex(byte b) {
        // Returns hex String representation of byte b
        char hexDigit[] =
                {
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
                };
        char[] array = {hexDigit[(b >> 4) & 0x0f], hexDigit[b & 0x0f]};
        return new String(array);
    }

    public static String getArabicNumbers(String str) {

        char[] text = null;
        text = str.toCharArray();
        str = null;

        char base = '\u0660' - '\u0030';
        char minDigit = '\u0030';
        for (int i = 0, e = text.length; i < e; ++i) {
            char c = text[i];
            if (c >= minDigit && c <= '\u0039') {
                text[i] = (char) (c + base);
            }
        }
        return new String(text);
    }

    public static String asciiToNative(String string) {
//        try {
//            return new String(string.getBytes(), Language.getCharSet());
//        } catch (Exception e) {
        return string;
//        }
    }
}