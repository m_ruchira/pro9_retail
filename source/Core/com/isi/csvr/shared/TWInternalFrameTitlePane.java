package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 13, 2005
 * Time: 2:14:24 PM
 */


import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


/**
 * Class that manages a JLF title bar
 *
 * @author Steve Wilson
 * @author Brian Beck
 * @version 1.57 03/17/04
 * @since 1.3
 */

public class TWInternalFrameTitlePane extends TWBasicInternalFrameTitlePane {

    private static final Border handyEmptyBorder = new EmptyBorder(0, 0, 0, 0);
    protected boolean isPalette = false;
    protected Icon paletteCloseIcon;
    protected int paletteTitleHeight;
    /**
     * Key used to lookup Color from UIManager. If this is null,
     * <code>getWindowTitleBackground</code> is used.
     */
    private String selectedBackgroundKey;
    /**
     * Key used to lookup Color from UIManager. If this is null,
     * <code>getWindowTitleForeground</code> is used.
     */
    private String selectedForegroundKey;
    /**
     * Key used to lookup shadow color from UIManager. If this is null,
     * <code>getPrimaryControlDarkShadow</code> is used.
     */
    private String selectedShadowKey;
    /**
     * Boolean indicating the state of the <code>JInternalFrame</code>s
     * closable property at <code>updateUI</code> time.
     */
    private boolean wasClosable;
    private boolean isForDesktopIcon = false;

    public TWInternalFrameTitlePane(JInternalFrame f) {
        super(f);
    }

    public TWInternalFrameTitlePane(JInternalFrame f, boolean isForDesktopIcon) {
        super(f);
        this.isForDesktopIcon = isForDesktopIcon;
    }

    public void addNotify() {
        super.addNotify();
        // This is done here instead of in installDefaults as I was worried
        // that the BasicInternalFrameUI might not be fully initialized, and
        // that if this resets the closable state the BasicInternalFrameUI
        // Listeners that get notified might be in an odd/uninitialized state.
        updateOptionPaneState();
    }

    protected void installDefaults() {
        super.installDefaults();
        setFont(UIManager.getFont("InternalFrame.titleFont"));
        paletteTitleHeight = UIManager.getInt("InternalFrame.paletteTitleHeight");
        paletteCloseIcon = UIManager.getIcon("InternalFrame.paletteCloseIcon");
        wasClosable = frame.isClosable();
        selectedForegroundKey = selectedBackgroundKey = null;
        setOpaque(true);
    }

    protected void uninstallDefaults() {
        super.uninstallDefaults();
        if (wasClosable != frame.isClosable()) {
            frame.setClosable(wasClosable);
        }
    }

    protected void createButtons() {
        super.createButtons();

        Boolean paintActive = frame.isSelected() ? Boolean.TRUE : Boolean.FALSE;
        iconButton.putClientProperty("paintActive", paintActive);
        iconButton.setBorder(handyEmptyBorder);
        iconButton.setContentAreaFilled(false);
        if (!isForDesktopIcon) {
            maxButton.putClientProperty("paintActive", paintActive);
            maxButton.setBorder(handyEmptyBorder);
            maxButton.setContentAreaFilled(false);
        }


        closeButton.putClientProperty("paintActive", paintActive);
        closeButton.setBorder(handyEmptyBorder);
        closeButton.setContentAreaFilled(false);

        if (!isForDesktopIcon) {
            printButton.putClientProperty("paintActive", paintActive);
            printButton.setBorder(handyEmptyBorder);
            printButton.setContentAreaFilled(false);

            servicesButton.putClientProperty("paintActive", paintActive);
            servicesButton.setBorder(handyEmptyBorder);
            servicesButton.setContentAreaFilled(false);

            linkGroupButton.putClientProperty("paintActive", paintActive);
            linkGroupButton.setBorder(handyEmptyBorder);
            linkGroupButton.setContentAreaFilled(false);

            resizeButton.putClientProperty("paintActive", paintActive);
            resizeButton.setBorder(handyEmptyBorder);
            resizeButton.setContentAreaFilled(false);

            detachButton.putClientProperty("paintActive", paintActive);
            detachButton.setBorder(handyEmptyBorder);
            detachButton.setContentAreaFilled(false);
        }

        // The palette close icon isn't opaque while the regular close icon is.
        // This makes sure palette close buttons have the right background.
        //closeButton.setBackground(MetalLookAndFeel.getPrimaryControlShadow());
    }

    /**
     * Override the parent's method to do nothing. Metal frames do not
     * have system menus.
     */
    protected void assembleSystemMenu() {
    }

    /**
     * Override the parent's method to do nothing. Metal frames do not
     * have system menus.
     */
    protected void addSystemMenuItems(JMenu systemMenu) {
    }

    /**
     * Override the parent's method to do nothing. Metal frames do not
     * have system menus.
     */
    protected void showSystemMenu() {
    }

    /**
     * Override the parent's method avoid creating a menu bar. Metal frames
     * do not have system menus.
     */
    protected void addSubComponents() {
        add(iconButton);
        if (!isForDesktopIcon) {
            add(maxButton);
        }
        add(closeButton);
        if (!isForDesktopIcon) {
            add(printButton);
            add(servicesButton);
            add(resizeButton);
            add(detachButton);
            add(linkGroupButton);
        }
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return new MetalPropertyChangeHandler();
    }

    protected LayoutManager createLayout() {
        return new MetalTitlePaneLayout();
    }

    public void paintPalette(Graphics g) {
        int width = getWidth();
        int height = getHeight();

        Color background = MetalLookAndFeel.getPrimaryControlShadow();
        Color darkShadow = MetalLookAndFeel.getPrimaryControlDarkShadow();

        g.setColor(background);
        g.fillRect(0, 0, width, height);

        g.setColor(darkShadow);
        g.drawLine(0, height - 1, width, height - 1);
    }

    public void paintComponent(Graphics g) {
        if (isPalette) {
            paintPalette(g);
            return;
        }

        boolean leftToRight = SharedMethods.isLeftToRight(frame);
        boolean isSelected = frame.isSelected();

        int width = getWidth();
        int height = getHeight();

        Color foreground = null;
        Color shadow = null;

        if (isSelected) {
            foreground = selectedTextColor;
        } else {
            foreground = notSelectedTextColor; // MetalLookAndFeel.getWindowTitleInactiveForeground();
            shadow = MetalLookAndFeel.getControlDarkShadow();
        }

        g.setColor(shadow);
        g.drawLine(0, height - 1, width, height - 1);
        g.drawLine(0, 0, 0, 0);
        g.drawLine(width - 1, 0, width - 1, 0);


        int titleLength = 0;
        int xOffset = leftToRight ? 5 : width - 5;
        String frameTitle = frame.getTitle();

        paintGradient(g, width, getHeight());

        Icon icon = frame.getFrameIcon();
        if (icon != null) {
            if (!leftToRight)
                xOffset -= icon.getIconWidth();
            int iconY = ((height / 2) - (icon.getIconHeight() / 2));
            icon.paintIcon(frame, g, xOffset, iconY);
            xOffset += leftToRight ? icon.getIconWidth() + 5 : -5;
        }

        if (frameTitle != null) {
            Font f = getFont();
            g.setFont(f);
            FontMetrics fm = SwingUtilities2.getFontMetrics(frame, g, f);
            //int fHeight = fm.getHeight();

            g.setColor(foreground);

            int yOffset = ((height - fm.getHeight()) / 2) + fm.getAscent();

            Rectangle rect = new Rectangle(0, 0, 0, 0);
            if (!isForDesktopIcon) {
                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isLinkGroupEnabled()) {
                    rect = linkGroupButton.getBounds();
                } else if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isServicesEnabled()) {
                    rect = servicesButton.getBounds();
                } else if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isPrintable()) {
                    rect = printButton.getBounds();
                } else if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isColumnResizeable()) {
                    rect = resizeButton.getBounds();
                } else if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isColumnResizeable()) {
                    rect = detachButton.getBounds();
                } else if (frame.isIconifiable()) {
                    rect = iconButton.getBounds();
                } else if (frame.isMaximizable()) {
                    rect = maxButton.getBounds();
                } else if (frame.isClosable()) {
                    rect = closeButton.getBounds();
                }
            } else {
                if (frame.isIconifiable()) {
                    rect = iconButton.getBounds();
                } else if (frame.isClosable()) {
                    rect = closeButton.getBounds();
                }
            }
            int titleW;

            try {
                if (leftToRight) {
                    if (rect.x == 0) {
                        rect.x = frame.getWidth() - frame.getInsets().right - 2;
                    }
                    titleW = rect.x - xOffset - 4;
                    frameTitle = getTitle(frameTitle, fm, titleW);
                } else {
                    titleW = xOffset - rect.x - rect.width - 4;
                    frameTitle = getTitle(frameTitle, fm, titleW);
                    xOffset -= SwingUtilities2.stringWidth(frame, fm, frameTitle);
                }

                titleLength = SwingUtilities2.stringWidth(frame, fm, frameTitle);
                SwingUtilities2.drawString(frame, g, frameTitle, xOffset, yOffset);
                xOffset += leftToRight ? titleLength + 5 : -5;
            } catch (Exception e) {

            }
        }
    }

    private void paintGradient(Graphics g, int width, int height) {

        try {
            int xOffset = 0;
            int iconWidth;
            ImageIcon titleLeft;
            ImageIcon titleRight;
            ImageIcon titleCenter;
            boolean leftToRight = SharedMethods.isLeftToRight(frame);

            if (frame.isSelected()) {
                titleLeft = (ImageIcon) UIManager.getIcon("InternalFrame.gradientImageSelectedLeft");
                titleCenter = (ImageIcon) UIManager.getIcon("InternalFrame.gradientImageSelectedCenter");
                titleRight = (ImageIcon) (UIManager.getIcon("InternalFrame.gradientImageSelectedRight"));
            } else {
                titleLeft = (ImageIcon) UIManager.getIcon("InternalFrame.gradientImageUnSelectedLeft");
                titleCenter = (ImageIcon) UIManager.getIcon("InternalFrame.gradientImageUnSelectedCenter");
                titleRight = (ImageIcon) UIManager.getIcon("InternalFrame.gradientImageUnSelectedRight");
            }
            if (leftToRight) {
                iconWidth = titleLeft.getIconWidth();
                if (iconWidth > 0) {
                    titleLeft.paintIcon(null, g, 0, 0);
                    xOffset = iconWidth;
                }
                iconWidth = titleCenter.getIconWidth();
                if (iconWidth > 0) {
                    for (int i = xOffset; i <= width; i += iconWidth) {
                        titleCenter.paintIcon(null, g, i, 0);
                    }
                }
                iconWidth = titleRight.getIconWidth();
                if (iconWidth > 0) {
                    titleRight.paintIcon(null, g, width - iconWidth, 0);
                }
            } else {
                iconWidth = titleRight.getIconWidth();
                if (iconWidth > 0) {
                    g.drawImage(titleRight.getImage(),
                            titleRight.getIconWidth(), 0, 0, titleRight.getIconHeight(),
                            0, 0, titleRight.getIconWidth(), titleRight.getIconHeight(), this);
                    xOffset = iconWidth;
                }
                iconWidth = titleCenter.getIconWidth();
                if (iconWidth > 0) {
                    for (int i = xOffset; i <= width; i += iconWidth) {
                        titleCenter.paintIcon(null, g, i, 0);
                    }
                }
                iconWidth = titleLeft.getIconWidth();
                if (iconWidth > 0) {
                    g.translate(width - iconWidth, 0);
                    g.drawImage(titleLeft.getImage(),
                            titleLeft.getIconWidth(), 0, 0, titleLeft.getIconHeight(),
                            0, 0, titleLeft.getIconWidth(), titleLeft.getIconHeight(), this);
                    g.translate(-width + iconWidth, 0);
                }
            }

        } catch (Exception e) {
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, width, height);
        }
    }

    public void setPalette(boolean b) {
        isPalette = b;

        if (isPalette) {
            closeButton.setIcon(paletteCloseIcon);
            if (frame.isMaximizable())
                remove(maxButton);
            if (frame.isIconifiable())
                remove(iconButton);
        } else {
            closeButton.setIcon(closeIcon);
            if (frame.isMaximizable())
                add(maxButton);
            if (frame.isIconifiable())
                add(iconButton);
        }
        revalidate();
        repaint();
    }

    /**
     * Updates any state dependant upon the JInternalFrame being shown in
     * a <code>JOptionPane</code>.
     */
    private void updateOptionPaneState() {
        int type = -2;
        boolean closable = wasClosable;
        Object obj = frame.getClientProperty("JInternalFrame.messageType");

        if (obj == null) {
            // Don't change the closable state unless in an JOptionPane.
            return;
        }
        if (obj instanceof Integer) {
            type = ((Integer) obj).intValue();
        }
        switch (type) {
            case JOptionPane.ERROR_MESSAGE:
                selectedBackgroundKey =
                        "OptionPane.errorDialog.titlePane.background";
                selectedForegroundKey =
                        "OptionPane.errorDialog.titlePane.foreground";
                selectedShadowKey = "OptionPane.errorDialog.titlePane.shadow";
                closable = false;
                break;
            case JOptionPane.QUESTION_MESSAGE:
                selectedBackgroundKey =
                        "OptionPane.questionDialog.titlePane.background";
                selectedForegroundKey =
                        "OptionPane.questionDialog.titlePane.foreground";
                selectedShadowKey =
                        "OptionPane.questionDialog.titlePane.shadow";
                closable = false;
                break;
            case JOptionPane.WARNING_MESSAGE:
                selectedBackgroundKey =
                        "OptionPane.warningDialog.titlePane.background";
                selectedForegroundKey =
                        "OptionPane.warningDialog.titlePane.foreground";
                selectedShadowKey = "OptionPane.warningDialog.titlePane.shadow";
                closable = false;
                break;
            case JOptionPane.INFORMATION_MESSAGE:
            case JOptionPane.PLAIN_MESSAGE:
                selectedBackgroundKey = selectedForegroundKey = selectedShadowKey =
                        null;
                closable = false;
                break;
            default:
                selectedBackgroundKey = selectedForegroundKey = selectedShadowKey =
                        null;
                break;
        }
        if (closable != frame.isClosable()) {
            frame.setClosable(closable);
        }
    }

    class MetalPropertyChangeHandler
            extends TWBasicInternalFrameTitlePane.PropertyChangeHandler {
        public void propertyChange(PropertyChangeEvent evt) {
            String prop = (String) evt.getPropertyName();
            if (prop.equals(JInternalFrame.IS_SELECTED_PROPERTY)) {
                Boolean b = (Boolean) evt.getNewValue();
                iconButton.putClientProperty("paintActive", b);
                closeButton.putClientProperty("paintActive", b);
                if (!isForDesktopIcon) {
                    maxButton.putClientProperty("paintActive", b);
                }
                if (!isForDesktopIcon) {
                    resizeButton.putClientProperty("paintActive", b);
                    detachButton.putClientProperty("paintActive", b);
                    printButton.putClientProperty("paintActive", b);
                    servicesButton.putClientProperty("paintActive", b);
                    linkGroupButton.putClientProperty("paintActive", b);
                }

            } else if ("JInternalFrame.messageType".equals(prop)) {
                updateOptionPaneState();
                frame.repaint();
            }
            super.propertyChange(evt);
        }
    }

    class MetalTitlePaneLayout extends TitlePaneLayout {
        public void addLayoutComponent(String name, Component c) {
        }

        public void removeLayoutComponent(Component c) {
        }

        public Dimension preferredLayoutSize(Container c) {
            return minimumLayoutSize(c);
        }

        public Dimension minimumLayoutSize(Container c) {
            // Compute width.
            int width = 50;
            if (frame.isClosable()) {
                width += 21;
            }
            if (frame.isMaximizable()) {
                width += 16 + (frame.isClosable() ? 10 : 4);
            }
            if (!isForDesktopIcon) {
                if (frame.isIconifiable()) {
                    width += 16 + (frame.isMaximizable() ? 2 :
                            (frame.isClosable() ? 10 : 4));
                }
            }
            FontMetrics fm = frame.getFontMetrics(getFont());
            String frameTitle = frame.getTitle();
            int title_w = frameTitle != null ? SwingUtilities2.stringWidth(frame, fm, frameTitle) : 0;
            int title_length = frameTitle != null ? frameTitle.length() : 0;

            if (title_length > 2) {
                int subtitle_w = SwingUtilities2.stringWidth(frame, fm,
                        frame.getTitle().substring(0, 2) + "...");
                width += (title_w < subtitle_w) ? title_w : subtitle_w;
            } else {
                width += title_w;
            }

            // Compute height.
            int height = 0;
            if (isPalette) {
                height = paletteTitleHeight;
            } else {
                int fontHeight = fm.getHeight();
                fontHeight += 7;
                Icon icon = frame.getFrameIcon();
                int iconHeight = 0;
                if (icon != null) {
                    // SystemMenuBar forces the icon to be 16x16 or less.
                    iconHeight = Math.min(icon.getIconHeight(), 16);
                }
                iconHeight += 5;
                height = Math.max(fontHeight, iconHeight);
            }

            return new Dimension(width, height);
        }

        public void layoutContainer(Container c) {
            boolean leftToRight = SharedMethods.isLeftToRight(frame);

            int w = getWidth();
            int x = leftToRight ? w : 0;
            int y = 2;
            int spacing = 2;

            // assumes all buttons have the same dimensions
            // these dimensions include the borders
            int buttonHeight = closeButton.getIcon().getIconHeight();
            int buttonWidth = closeButton.getIcon().getIconWidth();

            if (frame.isClosable()) {
                if (isPalette) {
                    //spacing = 3;
                    x += leftToRight ? -spacing - (buttonWidth + 2) : spacing;
                    closeButton.setBounds(x, y, buttonWidth + 2, getHeight() - 4);
                    if (!leftToRight) x += (buttonWidth + 2);
                } else {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    closeButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }
            }

            if (!isForDesktopIcon) {
                if (frame.isMaximizable() && !isPalette) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    maxButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }
            }

            if (frame.isIconifiable() && !isPalette) {
                x += leftToRight ? -spacing - buttonWidth : spacing;
                iconButton.setBounds(x, y, buttonWidth, buttonHeight);
                if (!leftToRight) x += buttonWidth;
            }

            if (!isForDesktopIcon) {

                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isColumnResizeable()) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    resizeButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }

                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isDetachable()) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    detachButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }

                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isPrintable()) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    printButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }

                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isServicesEnabled()) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    servicesButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }
                if ((frame instanceof TWTitlePaneInterface) && (((TWTitlePaneInterface) frame)).isLinkGroupEnabled()) {
                    x += leftToRight ? -spacing - buttonWidth : spacing;
                    linkGroupButton.setBounds(x, y, buttonWidth, buttonHeight);
                    if (!leftToRight) x += buttonWidth;
                }
            }
        }
    }
}

