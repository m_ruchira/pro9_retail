package com.isi.csvr.shared;

import java.text.DecimalFormat;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWDecimalFormat {

    public boolean flagged;
    DecimalFormat formater;
    //NumericShaper shaper = null;

    public TWDecimalFormat(String format) {
        formater = new DecimalFormat(format);
        //shaper = NumericShaper.getShaper(NumericShaper.ARABIC);
    }

    public void applyPattern(String pattern) {
        if (flagged)
            System.out.println("Apply Pattern " + pattern);
        formater.applyPattern(pattern);
    }

    public String format(double number) {
        String value = formater.format(number);
        if (Settings.isShowArabicNumbers())
            return arabize(value);
        else
            return value;
    }

    public String format(long number) {
        String value = formater.format(number);
        if (Settings.isShowArabicNumbers())
            return arabize(value);
        else
            return value;
    }


    private String arabize(String str) {

        char[] text = null;
        text = str.toCharArray();
        str = null;

        char base = '\u0660' - '\u0030';
        char minDigit = '\u0030';
        for (int i = 0, e = text.length; i < e; ++i) {
            char c = text[i];
            if (c >= minDigit && c <= '\u0039') {
                text[i] = (char) (c + base);
            }
        }
        return new String(text);
    }
}