package com.isi.csvr.shared;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 12, 2006
 * Time: 4:00:54 PM
 */
public class FunctionLoader extends ClassLoader {
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        byte[] b = loadClassData(name);
        return defineClass(name, b, 0, b.length);
    }

    private byte[] loadClassData(String name) {
        try {
            name = name.replaceAll("\\.", "/");
            File file = new File(Settings.getAbsolutepath() + "formula/" + name + ".class");
            byte[] data = new byte[(int) file.length()];
            FileInputStream in = new FileInputStream(file);
            for (int i = 0; i < data.length; i++) {
                data[i] = (byte) in.read();
            }
            in.close();
            in = null;
            try {
                file.delete();
            } catch (Exception e) {
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
