package com.isi.csvr.shared;

import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Feb 5, 2009
 * Time: 10:04:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class OvalBorder implements Border {
    protected int ovalWidth = 8;

    protected int ovalHeight = 8;

    protected Color lightColor = Color.black;

    protected Color darkColor = Color.gray;

    public OvalBorder() {
        ovalWidth = 8;
        ovalHeight = 8;
    }

    public OvalBorder(int w, int h) {
        ovalWidth = w;
        ovalHeight = h;
    }

    public OvalBorder(int w, int h, Color topColor, Color bottomColor) {
        ovalWidth = w;
        ovalHeight = h;
        lightColor = topColor;
        darkColor = bottomColor;
    }

    public OvalBorder(Color topColor, Color bottomColor) {
        lightColor = topColor;
        darkColor = bottomColor;
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(ovalHeight, ovalWidth, ovalHeight, ovalWidth);
//		return new Insets ( 0,0,0,0) ;
    }

    public boolean isBorderOpaque() {
        return true;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width,
                            int height) {
        width--;
        height--;

        g.setColor(lightColor);
        g.drawLine(x, y + height - ovalHeight, x, y + ovalHeight);
        g.drawArc(x, y, 2 * ovalWidth, 2 * ovalHeight, 180, -90);
        g.drawLine(x + ovalWidth, y, x + width - ovalWidth, y);
        g.drawArc(x + width - 2 * ovalWidth, y, 2 * ovalWidth, 2 * ovalHeight,
                90, -90);

//		g.setColor(darkColor);
        g.drawLine(x + width, y + ovalHeight, x + width, y + height
                - ovalHeight);
        g.drawArc(x + width - 2 * ovalWidth, y + height - 2 * ovalHeight,
                2 * ovalWidth, 2 * ovalHeight, 0, -90);
        g
                .drawLine(x + ovalWidth, y + height, x + width - ovalWidth, y
                        + height);
        g.drawArc(x, y + height - 2 * ovalHeight, 2 * ovalWidth,
                2 * ovalHeight, -90, -90);
//		g.setColor(lightColor);
//		g.drawRoundRect ( x , y , width , height , 2, 2) ;
    }
}
