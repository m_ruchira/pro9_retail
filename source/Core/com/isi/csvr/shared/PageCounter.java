package com.isi.csvr.shared;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Jan 23, 2008
 * Time: 12:06:36 PM
 */
public class PageCounter {
    private Hashtable<Integer, Page> pages;
    private int currentPage;

    public PageCounter() {
        pages = new Hashtable<Integer, Page>();
    }

    public void init() {
        pages.clear();
        currentPage = 1;
    }

    public void addPage(String top, String bottom) {
        Page page = new Page(top, bottom);
        if (pages.size() == 0) { // data for the first page
            pages.put(1, page);
            currentPage = 1;
            System.out.println("page " + currentPage + " st " + top + " en " + bottom);
        } else {
            if (currentPage == pages.size()) { // a new page
                pages.put(pages.size() + 1, page);
                currentPage++;
                System.out.println("page " + currentPage + " st " + top + " en " + bottom);
            }
        }
    }

    public String getNext() {
        try {
            Page page = pages.get(currentPage);
            System.out.println("page " + currentPage + " st " + page.top + " en " + page.bottom);
            if (currentPage < pages.size()) { // requesting an old page
                currentPage++;
            }
            return page.bottom;
        } catch (Exception e) {
            Page page = pages.get(currentPage);
            System.out.println("err page " + currentPage + " st " + page.top + " en " + page.bottom);
            if (currentPage < pages.size()) { // requesting an old page
                currentPage++;
            }
            return page.top;
        }
    }

    public String getPrevious() {
        try {
            currentPage--;
            Page page = pages.get(currentPage);
            System.out.println("page " + currentPage + " st " + page.top + " en " + page.bottom);
            return page.top;
        } catch (Exception e) {
            Page page = pages.get(currentPage);
            System.out.println("err page " + currentPage + " st " + page.top + " en " + page.bottom);
            return page.top;
        }
    }

    public boolean hasPreviuos() {
        return (currentPage > 1) && (pages.size() > 1);
    }

    class Page {
        public String top;
        public String bottom;

        Page(String top, String bottom) {
            this.bottom = bottom;
            this.top = top;
        }
    }
}
