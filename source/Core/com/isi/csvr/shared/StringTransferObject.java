package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 16, 2006
 * Time: 12:58:35 PM
 */
public class StringTransferObject implements TransferObject {
    private String value;
    private long flag;
    private boolean blank;

    public StringTransferObject() {
    }

    public String getValue() {
        return value;
    }

    public StringTransferObject setValue(String value) {
        this.value = value;
        return this;
    }

    public long getFlag() {
        return flag;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }


    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }


    public String toString() {
        return value;
    }
}
