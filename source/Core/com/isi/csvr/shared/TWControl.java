// Copyright (c) 2000 Home
package com.isi.csvr.shared;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.win32.NativeMethods;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Vector;

/**
 * A Class class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class TWControl {

    public static int ALL_TIME_AND_SALES = 200000;
    public static SSO_TYPES sSOType = SSO_TYPES.None;
    public static String broker = "";
    private static SmartProperties prop;
    private static Properties custom;
    private static boolean virtualKeyBoardControllable;
    private static boolean newsButtonOnTollbar = false;
    private static boolean useVirtualKeyBoard;
    private static boolean oddLotEnabled;
    private static boolean miniTradeDisabled;
    private static boolean cashLogSearchDisabled;
    private static boolean customerStatementDisabled;
    private static boolean isBetaVersion;
    private static boolean shortDescAvailableForTopStocks;
    private static boolean shortDescAvailableForScanner;
    private static String[] loadBalancerURLs = null;
    private static byte portfolioDecimalCount;
    private static byte defaultPortfolioDecimalCount = 2;
    private static boolean enableDefaultValueDecimalCount = false;
    private static boolean accountSummaryFieldsEnable;
    private static boolean sSOEnabled = false;
    private static boolean isT0OrdersEnabled = true;
    private static boolean priceOnlyVersion;

    public static boolean isMiniTradeDisabled() {
        return miniTradeDisabled;
    }

    public static void setMiniTradeDisabled(boolean miniTradeDisabled) {
        TWControl.miniTradeDisabled = miniTradeDisabled;
    }

    public static boolean isCashLogSearchDisabled() {
        return cashLogSearchDisabled;
    }

    public static void setCashLogSearchDisabled(boolean cashLogSearchDisabled) {
        TWControl.cashLogSearchDisabled = cashLogSearchDisabled;
    }

    public static boolean isCustomerStatementDisabled() {
        return customerStatementDisabled;
    }

    public static void setCustomerStatementDisabled(boolean customerStatementDisabled) {
        TWControl.customerStatementDisabled = customerStatementDisabled;
    }

    public static byte getPortfolioDecimalCount() {
        return portfolioDecimalCount;
    }

    public static void setPortfolioDecimalCount(byte defaultDecimalCount) {
        TWControl.portfolioDecimalCount = defaultDecimalCount;
    }

    public static boolean isEnableDefaultValueDecimalCount() {
        return enableDefaultValueDecimalCount;
    }

    public static void setEnableDefaultValueDecimalCount(boolean enableDefaultValueDecimalCount) {
        TWControl.enableDefaultValueDecimalCount = enableDefaultValueDecimalCount;
    }

    public static boolean isAccountSummaryFieldsEnable() {
        return accountSummaryFieldsEnable;
    }

    public static void setAccountSummaryFieldsEnable(boolean accountSummaryFieldsEnable) {
        TWControl.accountSummaryFieldsEnable = accountSummaryFieldsEnable;
    }

    /**
     * Constructor
     */
    public static void init() {
        try {
            prop = new SmartProperties("=");
            prop.loadCompressed(Settings.CONFIG_DATA_PATH + "/ct.msf");
        } catch (Exception s_e) {
            prop = new SmartProperties("=");
        }

        try {
            custom = new Properties();
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "ServerSideCustomizer.txt");
            custom.load(oIn);
            oIn.close();
        } catch (Exception s_e) {
            custom = new Properties();
        }

        try {
            Vector ips = new Vector();
            DataInputStream oIn = new DataInputStream(new FileInputStream(Settings.CONFIG_DATA_PATH + "/lb.dll"));
            String ip;
            while (true) {
                ip = oIn.readLine();
                if (ip == null) {
                    break;
                }
                ips.add(ip.trim());
            }
            if (ips.size() > 0) {
                loadBalancerURLs = new String[ips.size()];
                for (int i = 0; i < loadBalancerURLs.length; i++) {
                    loadBalancerURLs[i] = (String) ips.elementAt(i);
                }
            }
            ips.clear();
            ips = null;
            oIn.close();
        } catch (Exception s_e) {
            loadBalancerURLs = new String[0];
        }


        String sValue = "";

        sValue = prop.getProperty("VIRTUAL_KEYBOARD");
        try {
            setVirtualKeyBoardControllable((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setVirtualKeyBoardControllable(true);
        }
        sValue = prop.getProperty("SET_NEWS_ON_TOOLBAR");
        try {
            setNewsOnToolBar((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setNewsOnToolBar(true);
        }

        sValue = prop.getProperty("DEFAULT_VIRTUAL_KEYBOARD_ON");
        try {
            setUseVirtualKeyBoard((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setUseVirtualKeyBoard(true);
        }

        sValue = prop.getProperty("ODD_LOT_ENABLED");
        try {
            setOddLotEnabled((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setOddLotEnabled(false);
        }

        sValue = prop.getProperty("BETA_VERSION");
        try {
            setBetaVersion((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setBetaVersion(false);
        }
        sValue = prop.getProperty("SHORT_DESC_FOR_TOPSTOCKS");
        try {
            setShortDescAvailableForTopStocks((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setShortDescAvailableForTopStocks(false);
        }
        sValue = prop.getProperty("SHORT_DESC_FOR_SCANNER");
        try {
            setShortDescAvailableForScanner((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setShortDescAvailableForScanner(false);
        }

        sValue = prop.getProperty("T0_ORDERS_ENABLED");
        try {
            setT0OrdersEnabled((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setT0OrdersEnabled(true);
        }

        sValue = prop.getProperty("PRICE_ONLY_VERSION");
        try {
            setPriceOnlyVersion((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setPriceOnlyVersion(false);
        }

        sValue = prop.getProperty("MINI_TRADE_ENABLE");
        try {
            setMiniTradeDisabled((sValue.trim().equals("0")) || ((sValue.trim().equalsIgnoreCase("FALSE"))));
        } catch (Exception e) {
            setMiniTradeDisabled(false);
        }

        sValue = prop.getProperty("CASH_LOG_SEARCH_ENABLE");
        try {
            setCashLogSearchDisabled((sValue.trim().equals("0")) || ((sValue.trim().equalsIgnoreCase("FALSE"))));
        } catch (Exception e) {
            setCashLogSearchDisabled(false);
        }
        ////////////////////////////////////////////////////////////
        sValue = prop.getProperty("CUSTOMER_STATEMENT_ENABLE");
        try {
            setCustomerStatementDisabled((sValue.trim().equals("0")) || ((sValue.trim().equalsIgnoreCase("FALSE"))));
        } catch (Exception e) {
            setCustomerStatementDisabled(true);
        }
        ///////////////////////////////////////////////////////////
        sValue = prop.getProperty("PORTFOLIO_DECIMAL_COUNT");
        try {
            setPortfolioDecimalCount(Byte.parseByte(sValue));
        } catch (Exception e) {
            setPortfolioDecimalCount(defaultPortfolioDecimalCount);
        }

        sValue = prop.getProperty("ENABLE_DEFAULT_VALUE_DECIMAL_COUNT");
        try {
            setEnableDefaultValueDecimalCount((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setEnableDefaultValueDecimalCount(false);
        }

        sValue = prop.getProperty("ACCOUNT_SUMMARY_FIELDS_ENABLE");
        try {
            setAccountSummaryFieldsEnable((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            setAccountSummaryFieldsEnable(false);
        }

        try {
            if (NativeMethods.getMemorySize() < 520) {
                ALL_TIME_AND_SALES = 100000;
            } else {
                ALL_TIME_AND_SALES = 200000;
            }
            //            ALL_TIME_AND_SALES = Integer.parseInt(prop.getProperty("ALL_TIME_AND_SALES",""+200000));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        if (!Settings.isMktDepthStatusLoaded()) {
            try {
                String value = (prop.getProperty("SHOW_COMBINED_DEPTH"));
                Settings.setShowCombinedOrderBook((value.trim().equals("1")));
            } catch (Exception e) {
                //                Settings.setShowCombinedOrderBook(false);
            }
        }
        loadSSOFile();
    }

    public static void loadSSOFile() {
        SmartProperties tradeProp = null;
        try {
            tradeProp = new SmartProperties("=");
            tradeProp.loadCompressed(Settings.CONFIG_DATA_PATH + "/sso.msf");
            sSOEnabled = true;
        } catch (Exception s_e) {
            sSOEnabled = false;
            sSOType = SSO_TYPES.None;
        }

        try {
            sSOType = SSO_TYPES.valueOf(tradeProp.getProperty("SSO_TYPE"));
        } catch (NullPointerException e) {
            sSOType = SSO_TYPES.Web;
        } catch (Exception e) {
            sSOEnabled = false;
            sSOType = SSO_TYPES.None;
        }

        try {
            broker = tradeProp.getProperty("BROKER");
        } catch (NullPointerException e) {
            //            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            sSOEnabled = false;
            sSOType = SSO_TYPES.None;
        }
        if (sSOEnabled && (sSOType == SSO_TYPES.Direct)) {
            Settings.setOfflineMode(false);
        }
    }

    public static boolean isUseVirtualKeyBoard() {
        return useVirtualKeyBoard;
    }

    public static void setUseVirtualKeyBoard(boolean status) {
        useVirtualKeyBoard = status;
    }

    public static boolean isOddLotEnabled() {
        return oddLotEnabled;
    }

    public static void setOddLotEnabled(boolean oddLotEnabled) {
        TWControl.oddLotEnabled = oddLotEnabled;
    }

    public static String getItem(String sItem) {
        String sValue = "";

        sValue = prop.getProperty(sItem);
        return sValue;
    }

    public static int getIntItem(String sItem) {
        String sValue = "";

        sValue = prop.getProperty(sItem);
        try {
            return Integer.parseInt(sValue);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static int getIntItem(String sItem, int defaultValue) {
        String sValue = "";

        sValue = prop.getProperty(sItem);
        try {
            return Integer.parseInt(sValue);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    public static boolean getBooleanItem(String sItem) {
        String sValue = "";

        sValue = prop.getProperty(sItem);
        try {
            return ((sValue.trim().equals("1")) || ((sValue.trim().equalsIgnoreCase("TRUE"))));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isSingleSingnOn() {
        return sSOEnabled;
    }

    public static boolean isBetaVersion() {
        return isBetaVersion;
    }

    public static void setBetaVersion(boolean betaVersion) {
        isBetaVersion = betaVersion;
    }

    public static Enum getSSOType() {
        return sSOType;
    }

    public static void setWebSSOType() {
        sSOType = SSO_TYPES.Web;
    }

    public static void setDirectSSOType() {
        sSOType = SSO_TYPES.Direct;
    }

    public static String getSSOBrokerDetails() {
        return broker;
    }

    public static String[] getLoadBalancerURLs() {
        return loadBalancerURLs;
    }

    public static boolean isVirtualKeyBoardControllable() {
        return virtualKeyBoardControllable;
    }

    public static void setVirtualKeyBoardControllable(boolean status) {
        virtualKeyBoardControllable = status;
    }

    public static boolean isNewOnToolBar() {
        return newsButtonOnTollbar;
    }

    public static void setNewsOnToolBar(boolean flag) {
        newsButtonOnTollbar = flag;
    }

    public static boolean isArabicNumbersEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("ARABIC_NUMBERS"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }

    }

    public static boolean isCSChatEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("CUSTOMER_SUPPORT_ENABLE"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isMuliInstanceMode() {
        try {
            int status = Integer.parseInt(prop.getProperty("IS_MULTI_INSTANCE_MODE"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isPtoPChatEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("PEER_TO_PEER_CHAT"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isMarketTnSToolbarBtnvisible() {
        try {
            int status = Integer.parseInt(prop.getProperty("TIMENSALES_TOOLBAR_BTN"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isCombinedorderBookEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("COMBINED_ORDERBOOK_ENABLE"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isTykoonTradeEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("TYKOON_TRADE"));
            return (status != 0);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isSaveTimeandSalesonExitEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("SAVE_TIME_AND_SALES_ON_EXIT_ENABLE"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isLinktoExcelInMarketIndicesEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("LINK_TO_EXCEL_MARKET_INDICES"));
            return (status != 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isFullScreenEnable() {
        try {
            int status = Integer.parseInt(prop.getProperty("FULLSCREEN_ENABLE"));
            return (status != 0);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isAutoPopupAnnouncementMenuVisible() {
        try {
            int status = Integer.parseInt(prop.getProperty("AUTO_POPUP_ANNOUNCEMENT"));
            return (status != 0);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isAutoScrollingEnabled() {
        try {
            int status = Integer.parseInt(prop.getProperty("AUTO_SCROLLING_ENABLED"));
            return (status != 0);
        } catch (Exception e) {
            return true;
        }
    }

    public static String[] getLokedBillingCodes() {
        try {
            String codes = prop.getProperty("BILLING_CODES");
            if ((codes == null) || (codes.trim().equals(""))) {
                return null;
            } else {
                return codes.split(",");
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isShow52WkSaveEnabled() {
        try {
            int status = Integer.parseInt(prop.getProperty("SHOW_52WKHIGHLOW"));
            return (status != 0);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isShortDescAvailableForTopStocks() {
        return shortDescAvailableForTopStocks;
    }

    public static void setShortDescAvailableForTopStocks(boolean shortDescAvailableForTopStocks) {
        TWControl.shortDescAvailableForTopStocks = shortDescAvailableForTopStocks;
    }

    public static boolean isShortDescAvailableForScanner() {
        return shortDescAvailableForScanner;
    }

    public static void setShortDescAvailableForScanner(boolean shortDescAvailableForScanner) {
        TWControl.shortDescAvailableForScanner = shortDescAvailableForScanner;
    }

    public static boolean isT0OrdersEnabled() {
        return isT0OrdersEnabled;
    }

    public static void setT0OrdersEnabled(boolean isT0OrdersEnabled) {
        TWControl.isT0OrdersEnabled = isT0OrdersEnabled;
    }

    public static boolean isPriceOnlyVersion() {
        return priceOnlyVersion;
    }

    public static void setPriceOnlyVersion(boolean priceOnlyVersion) {
        TWControl.priceOnlyVersion = priceOnlyVersion;
    }

    public static boolean isEnableAfterMarketValuation() {
        try {
            return (prop.getProperty("AFTER_MARKET_VALUATION").equalsIgnoreCase("1"));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean hideIntradyPositionMoniter() {
        try {
            return (prop.getProperty("HIDE_POSITION_MONITOR").equalsIgnoreCase("1"));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean clearDataBeforeRefresh() { // SAAB requirement for PRO9 done by chandika Hewage
        try {
            return (prop.getProperty("CLEAR_DATA_BEFORE_REFRESH").equalsIgnoreCase("1"));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isShowingMinPriceOnLiquidate() { // SAAB requirement for PRO9 done by chandika Hewage
        try {
            return (prop.getProperty("SHOW_MIN_PRICE_ON_LIQUIDATE").equalsIgnoreCase("1"));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean hideBasketOrders() { // Alinma Requirement    - Randula
        try {
            return (prop.getProperty("HIDE_BASKET_ORDERS").equalsIgnoreCase("1"));
        } catch (Exception e) {
            return false;
        }
    }

    public static enum SSO_TYPES {
        None, Web, Direct
    }
}

