package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2006
 * Time: 10:36:45 AM
 */
public interface FunctionStockInterface {
    String getMarketID();

    int getInstrumentType();

    double getAvgTradePrice();

    long getAvgTradeVolume();

    double getChange();

    String getCompanyCode();

    double getHigh();

    double getHighPriceOf52Weeks();

    long getLastTradeTime();

    long getLastUpdatedTime();

    double getLow();

    double getLowPriceOf52Weeks();

    double getMaxPrice();

    double getMinPrice();

    int getNoOfTrades();

    double getPercentChange();

    double getPreviousClosed();

    String getSymbol();

    String getSymbolCode();

    String getExchange();

    String getKey();

    double getTodaysOpen();

    long getTradeQuantity();

    long getTradeQuantityFlag();

    double getTurnover();

    long getTurnoverFlag();

    long getVolume();

    String getShortDescription();

    String getLongDescription();

    String getAssetClass();

    String getSectorCode();

    double getPBR();

    double getPER();

    double getYield();

    String getParams();

    double getMarketCap();

    double getLastTradeValue();

    double getBestAskPrice();

    long getBestAskPriceFlag();

    double getBestBidPrice();

    long getBestBidPriceFlag();

    long getBestAskQuantity();

    long getBestBidQuantity();

    int getNoOfAsks();

    int getNoOfBids();

    double getSimpleAverageAsk();

    double getSimpleAverageBid();

    double getBidaskRatio();

    double getHighAsk();

    double getLastAskPrice();

    double getLastBidPrice();

    double getLowBid();

    long getTotalAskQty();

    long getTotalBidQty();

    double getWAverageAsk();

    double getWAverageBid();

    String getCurrencyCode();

    double getRange();

    double getSpread();

    double getPctRange();

    double getPctSpread();

    long getLastBidQuantity();

    long getLastAskQuantity();

    double getEPS();

    long getCouponDate();

    double getCouponValue();

    double getFaceValue();

    long getListedShares();

    long getPaidShares();

    long getLastTradeDate();

    double getNetProfit();

    long getNetProfitDate();

    long getLowPriceOf52WeeksDate();

    long getHighPriceOf52WeeksDate();

    double getTodaysClose();

    long getLastTradedDate();

    double getLastTradedPrice();

    double getRefPrice();

    long getOpenInterest();

    long getExpirationDate();

    double getStrikePrice();

    String getOptionBaseSymbol();

    double getContractHigh();

    double getContractLow();

    public double getSimpleMovingAverage();

    public double getExponentialMovingAverage();

    public double getWeightedMovingAverage();

    public double getMACD();

    public double getIMI();

    public double getRSI();

    public double getROC();

    public double getChaikinOsc();

    public double getFastStochastic();

    public double getSlowStochastic();

    public double getBollingerLower();

    public double getBollingerUpper();

    public double getBollingerMiddle();

    /*
    below methods were added according to the Dr. Waleeds request
     */
    public long getCashInOrders();

    public long getCashInVolume();

    public double getCashInTurnover();

    public long getCashOutOrders();

    public long getCashOutVolume();

    public double getCashOutTurnover();

    public double getCashFlowNet();

    public double getCashFlowRatio();

}
