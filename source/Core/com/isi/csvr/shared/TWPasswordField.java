package com.isi.csvr.shared;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.virtualkeyboard.VirtualKeyboardTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Feb 8, 2008
 * Time: 11:40:48 AM
 */
public class TWPasswordField extends JPanel implements KeyListener, Themeable {

    private VirtualKeyboardTextField textField;
    private TWPasswordImage pwdStatus;
    private String username;
    private JTextField usernameField;

    public TWPasswordField(JDialog parent, boolean enableVKeyboard, boolean randomLocation) {
        pwdStatus = new TWPasswordImage();
        textField = new VirtualKeyboardTextField(parent, enableVKeyboard, randomLocation, this);
        this.setLayout(new FlexGridLayout(new String[]{"100%", "60"}, new String[]{"100%"}, 5, 0, false, false));
        add(textField);
        add(pwdStatus);
        this.setBorder(textField.getBorder());
        textField.setBorder(BorderFactory.createEmptyBorder());
        this.setBackground(Theme.getColor("INPUT_TEXT_BGCOLOR"));
        this.setOpaque(true);
        pwdStatus.setOpaque(false);
        textField.addKeyListener(this);
        Theme.registerComponent(this);
    }

    public void setUsername(String username) {
        this.username = username;
        pwdStatus.setDepth(SharedMethods.getPasswordStrength(username, getText()));
//        pwdStatus.setText(SharedMethods.getPasswordStrength(username,getText()));
    }

    public void applyTheme() {
        pwdStatus.setForeground(Theme.getColor("INPUT_TEXT_FGCOLOR"));
        this.setBackground(Theme.getColor("INPUT_TEXT_BGCOLOR"));
    }

    public void setUsernameFiled(JTextField textFiled) {
        usernameField = textFiled;
        textFiled.addKeyListener(this);
    }


    public void setEnabled(boolean enabled) {
        textField.setEnabled(enabled);
    }

    public synchronized void addKeyListener(KeyListener l) {
        textField.addKeyListener(l);
    }

    public synchronized void addMouseListener(MouseListener l) {
        textField.addMouseListener(l);
    }

    public String getText() {
        return textField.getText();
    }

    public void setText(String text) {
        if (textField != null) {
            textField.setText(text);
            if (usernameField != null) {
                pwdStatus.setDepth(SharedMethods.getPasswordStrength(usernameField.getText(), getText()));
            } else {
                pwdStatus.setDepth(SharedMethods.getPasswordStrength(username, getText()));
            }
        }
    }

    public char[] getPassword() {
        return textField.getPassword();
    }

    public void setPasswordMode(boolean passwordMode) {
        textField.setPasswordMode(passwordMode);
    }

    public void setDocumentType() {
        textField.setDocumentType();
    }

    public void showVirtualKeyboard() {
        textField.showVirtualKeyboard();
    }

    public void setUseVirtualKeyboard(boolean status) {
        textField.setUseVirtualKeyboard(status);
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        if (usernameField != null) {
            pwdStatus.setDepth(SharedMethods.getPasswordStrength(usernameField.getText(), getText()));
        } else {
            pwdStatus.setDepth(SharedMethods.getPasswordStrength(username, getText()));
        }
    }

    @Override
    public void requestFocus() {
        textField.requestFocus();
    }

    @Override
    public boolean requestFocusInWindow() {
        return textField.requestFocusInWindow();
    }

    @Override
    public boolean requestFocus(boolean temporary) {
        return textField.requestFocus(temporary);
    }

    public void setCarretPosition(int pos) {

        textField.setCaretPosition(pos);
    }

    public void updateUI() {
        super.updateUI();
        JTextField textField = new JTextField();
        this.setBorder(textField.getBorder());
        textField = null;
    }

    class TWPasswordImage extends JLabel {
        private int value = 0;
        private int min = 0;
        private int max = 100;
        private int width = 100;
        private int height = 10;
        private int pixels = 0;
        private String label = "";

        public void setMin(int min) {
            this.min = min;
        }

        public void setMax(int max) {
            this.max = max;
        }

        public void setDepth(int value) {
            this.value = value;
            width = getWidth();
            height = 3;
            pixels = Math.round(width * value / max);
            setText(getLabel(value));
            label = (getLabel(value));
            repaint();
        }

        private String getLabel(int result) {
            if (result == 0) {
                return "";
            } else if (result < 2) {
                return Constants.SHORT_PASSWORD;
            } else if (result < 45) {
                return Constants.BAD_PASSWORD;
            } else if (result < 75) {
                return Constants.GOOD_PASSWORD;
            } else {
                return Constants.STRONG_PASSWORD;
            }
        }

        public void paint(Graphics g) {
            int ascent = g.getFontMetrics().getAscent();
            g.drawString(label, 0, ascent);
            for (int i = 0; i < pixels; i++) {
                boolean isFirstHalf = (i * 2 < width);
                paintPixel(g, i, isFirstHalf);
            }
        }

        public void paintPixel(Graphics g, int value, boolean isFirstHalf) {
            int rColor = 255 * (width - value) / width;
            int gColor = 255 * (value) / width;
            int bColor = 0;
            g.setColor(new Color(rColor, gColor, bColor));
            g.fillRect(value, getHeight() - height, 1, height);
        }
    }
}
