package com.isi.csvr.shared;

import com.isi.csvr.util.ExtendedComboInterface;
import com.isi.csvr.util.ExtendedComboUI;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 10, 2006
 * Time: 2:29:42 PM
 */
public class TWComboBox extends JComboBox implements ExtendedComboInterface {

    boolean extendedMode;
    ExtendedComboUI ui;

    public TWComboBox() {
        setExtendedMode(true);
    }

    public TWComboBox(ComboBoxModel aModel) {
        super(aModel);
        setExtendedMode(true);
    }

    public TWComboBox(final Object items[]) {
        super(items);
        setExtendedMode(true);
    }

    public TWComboBox(Vector<?> items) {
        super(items);
        setExtendedMode(true);
    }

    public boolean isExtendedMode() {
        return extendedMode;
    }

    public void setExtendedMode(boolean extendedMode) {
        this.extendedMode = extendedMode;
        ui = new ExtendedComboUI();
        setUI(ui);
    }

    public void updateUI() {
        super.updateUI();
        try {
            if (isExtendedMode()) {
                setUI(ui);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dimension getPopupSize() {
        int rows = getModel().getSize();
        int width = 0;
        for (int i = 0; i < rows; i++) {
            String item = getModel().getElementAt(i).toString();
            width = Math.max(width, super.getFontMetrics(getFont()).stringWidth(item));
            item = null;
        }

        if (width + 20 < super.getSize().getWidth()) {
            return new Dimension((int) super.getSize().getWidth(), (int) super.getSize().getHeight());
        } else {
            return new Dimension(width + 20, (int) super.getSize().getHeight());
        }
    }

    public void setArrowButtonIconColor(Color clr) {
//        ExtendedComboUI ui = (ExtendedComboUI) this.getUI();
        try {
            ui.setArrowButtonColor(clr);
        } catch (Exception e) {
        }
    }

    /*public Color getArrowButtonIconColor(){
         ExtendedComboUI ui = (ExtendedComboUI) this.getUI();
       return ui.getArrowColor();
    }*/
}
