package com.isi.csvr.shared;

// Copyright (c) 2000 Home

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

public class DownArrow implements Icon {

    private int size = 9;
    private Color arrowColor;
    private String UIId;

    /**
     * Constructor
     */
    public DownArrow() {
        UIId = "TextField.foreground";
    }

    public DownArrow(Color white) {
        this.arrowColor = white;
    }

    public DownArrow(String uiId) {
        UIId = uiId;
    }

    /**
     * Paint the icon object. This draws the icon for the selected
     * direction
     */
    public void paintIcon(Component c, Graphics g, int x, int y) {
        drawDownArrow(g, x, y);
    }

    /**
     * Returns the width of the icon
     */
    public int getIconWidth() {
        return 10;
    }

    /**
     * Returns the height of the icon
     */
    public int getIconHeight() {
        return 10;
    }

    /**
     * Draws the down arrow using graphics
     */
    private void drawDownArrow(Graphics g, int xo, int yo) {
        if (UIId != null) {
            Color color = UIManager.getColor(UIId);
            if (color != null) {
                g.setColor(color);
            } else {
                g.setColor(Theme.getBlackColor());
            }
        } else if (arrowColor != null) {
            g.setColor(arrowColor);
        } else {
            g.setColor(Theme.getBlackColor());
        }

        yo++;
        g.drawLine(xo, yo, xo + size, yo);
        g.drawLine(xo + 1, yo + 1, xo + size - 1, yo + 1);
        g.drawLine(xo + 2, yo + 2, xo + size - 2, yo + 2);
        g.drawLine(xo + 3, yo + 3, xo + size - 3, yo + 3);
        g.drawLine(xo + 4, yo + 4, xo + size - 4, yo + 4);
    }
}
