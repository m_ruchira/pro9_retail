package com.isi.csvr.shared;

import com.isi.csvr.Client;
import com.isi.csvr.MarketTimer;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jun 28, 2006
 * Time: 2:48:22 PM
 */
public class LatancyIndicator extends JDialog implements Runnable, ApplicationListener, ActionListener {

    private static final long REQUEST_TIME = 40000;
    private long threshold = REQUEST_TIME;
    private long requestTime = REQUEST_TIME;
    private static final long THRESHOLD = 300000;
    private static LatancyIndicator self;
    boolean ispopupAllowed = true;
    boolean ispopupEnabled = true;
    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
    private long clienttime = 0;
    private long servertime = 0;
    private JPanel mainPanel;
    private JPanel bodyPanel;
    private JLabel lblExchange;
    private JLabel lblExchangeTime;
    private JLabel lblDataTIme;
    private JLabel lblDelay;
    private JLabel valClientTime;
    private JLabel valServerTime;
    private JLabel valDelayTIme;
    private JLabel valExchange;
    private JCheckBox chkDoNotShowMsg;
    private Exchange exchange;
    private Thread thread;
    private long ignoretime = 0;
    private boolean active = false;

    private LatancyIndicator() {
        super(Client.getInstance().getFrame(), Language.getString("TIME_LAG_INDICATOR_WINDOW"), false);
        createUI();
        applyColors();
        self = this;
        chkDoNotShowMsg = new JCheckBox(Language.getString("MSG_DO_NOT_DISPLAY_AGAIN"));
        chkDoNotShowMsg.addActionListener(this);
        Application.getInstance().addApplicationListener(this);
        thread = new Thread(this, "Show DelayTime Window");
    }

    public synchronized static LatancyIndicator getSharedInstance() {
        if (self == null) {
            self = new LatancyIndicator();
        }
        return self;
    }

    public void activate() {
        try {
            if (!active) {
                if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                    active = true;
                    thread.start();
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void createUI() {
        this.setSize(300, 140);
        mainPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "190", "50%"}, new String[]{"100%"}));
        bodyPanel = new JPanel(new FlexGridLayout(new String[]{"90", "90"}, new String[]{"22", "22", "22"}, 3, 3));
        this.getContentPane().setLayout(new BorderLayout(0, 2)); // FlexGridLayout(new String[]{"100%"},new String[]{"25","100%"}));

        lblExchange = new JLabel(Language.getString("EXCHANGE"), JLabel.CENTER);
        lblExchange.setForeground(Color.GREEN);
        lblExchangeTime = new JLabel(Language.getString("CLIENT_TIME"), JLabel.LEADING);
        lblExchangeTime.setForeground(Color.GREEN);
        lblDataTIme = new JLabel(Language.getString("EXCHANGE_TIME"), JLabel.LEADING);
        lblDataTIme.setForeground(Color.GREEN);
        lblDelay = new JLabel(Language.getString("DELAY_TIME"), JLabel.LEADING);
        lblDelay.setForeground(Color.GREEN);
        valExchange = new JLabel("", JLabel.CENTER);
        valExchange.setFont(getFont().deriveFont(Font.BOLD, 16));
        valExchange.setForeground(Color.GREEN);
        valClientTime = new JLabel("", JLabel.TRAILING);
        valClientTime.setFont(getFont().deriveFont(Font.BOLD, 14));
        valClientTime.setForeground(Color.GREEN);
        valServerTime = new JLabel("", JLabel.TRAILING);
        valServerTime.setFont(getFont().deriveFont(Font.BOLD, 14));
        valServerTime.setForeground(Color.GREEN);
        valDelayTIme = new JLabel("", JLabel.TRAILING);
        valDelayTIme.setFont(getFont().deriveFont(Font.BOLD, 14));
        valDelayTIme.setForeground(Color.GREEN);

        bodyPanel.add(lblExchangeTime);
        bodyPanel.add(valClientTime);
        bodyPanel.add(lblDataTIme);
        bodyPanel.add(valServerTime);
        bodyPanel.add(lblDelay);
        bodyPanel.add(valDelayTIme);

        mainPanel.add(new JLabel(""));
        mainPanel.add(bodyPanel);
        mainPanel.add(new JLabel(""));

        this.getContentPane().add(valExchange, BorderLayout.NORTH);
        this.getContentPane().add(mainPanel, BorderLayout.CENTER);

        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.setResizable(false);
        GUISettings.applyOrientation(this);
        this.pack();
    }

    private void applyColors() {
        bodyPanel.setBackground(Color.BLACK);
        bodyPanel.setForeground(Color.WHITE);
        mainPanel.setBackground(Color.BLACK);
        mainPanel.setForeground(Color.GREEN);
        this.getContentPane().setBackground(Color.BLACK);
        this.setForeground(Color.GREEN);
        lblExchangeTime.setForeground(Color.GREEN);
        lblDataTIme.setForeground(Color.GREEN);
        lblDelay.setForeground(Color.GREEN);
        valExchange.setForeground(Color.GREEN);
        valClientTime.setForeground(Color.GREEN);
        valServerTime.setForeground(Color.GREEN);
    }

    public Dimension getPreferredSize() {
        if (valExchange.getPreferredSize().width > 160)
            super.setPreferredSize(new Dimension(valExchange.getPreferredSize().width + 50, 140));
        else
            super.setPreferredSize(new Dimension(210, 140));
        return super.getPreferredSize();
    }

    public void setClientTime(long time) {
        Date date = new Date(exchange.getZoneAdjustedTime(time));
        valClientTime.setText(format.format(date));
        date = null;
    }

    public void setServerTime(long time) {
        Date date = new Date(exchange.getZoneAdjustedTime(time));
        valServerTime.setText(format.format(date));
        date = null;
    }

    public void setSelectedExchange() {
        valExchange.setText(exchange.getDescription());
        this.pack();
        mainPanel.doLayout();
//        mainPanel.getLayout().layoutContainer(mainPanel);
    }

    public void setThreshold(String thresholdVlalue) {
        try {
            int threshold = Integer.parseInt(thresholdVlalue.split(Meta.FD)[1]);
            if (threshold <= 0) {
                this.threshold = THRESHOLD;
            } else {
                this.threshold = threshold * 1000;
            }
        } catch (Exception e) {
            this.threshold = THRESHOLD;
        }

        try {
            int requestTime = Integer.parseInt(thresholdVlalue.split(Meta.FD)[2]);
            if (requestTime <= 0) {
                this.requestTime = REQUEST_TIME;
            } else {
                this.requestTime = requestTime * 1000;
            }
        } catch (Exception e) {
            this.requestTime = REQUEST_TIME;
        }
    }

    public void setData(String timeString) {
        if (exchange.getSymbol().equalsIgnoreCase(timeString.split(Meta.FD)[0])) {
            servertime = exchange.getMarketDate() + SharedMethods.getTimeLongFromTimeString(timeString.split(Meta.FD)[1]) + 1000;
            clienttime = MarketTimer.getSharedInstance().getMarketTime(exchange.getSymbol());
            if ((servertime - clienttime) <= ignoretime) {
                servertime = clienttime;
            }
            setClientTime(clienttime);
            setServerTime(servertime);
            setDelayTime();
        } else {
            try {
                thread.interrupt();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void setDelayTime() {
        if ((servertime - clienttime) > 0) {
            valDelayTIme.setForeground(Color.RED.brighter());
            valDelayTIme.setText(format.format(new Date((servertime - clienttime))));
        } else {
            valDelayTIme.setForeground(Color.GREEN);
            valDelayTIme.setText("00:00:00");
        }
        if ((servertime - clienttime) >= threshold) {
            valDelayTIme.setForeground(Color.RED.darker());
            if (ispopupAllowed && ispopupEnabled) {
                Object[] messages = new Object[2];
                messages[0] = Language.getString("MSG_LAG_TIME");
                messages[1] = chkDoNotShowMsg;
                SharedMethods.showNonBlockingMessage(messages, JOptionPane.WARNING_MESSAGE);
                ispopupEnabled = false;
            }
        } else {
            ispopupEnabled = true;
        }
        if ((servertime - clienttime) >= 3600000) {
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                int option = JOptionPane.showConfirmDialog(this, Language.getString("BANDWIDTH_OPTIMIZATION_WARNING"),
                        Language.getString("WARNING"), JOptionPane.YES_NO_OPTION);
                if (option == JOptionPane.YES_OPTION) {
                    Client.getInstance().mnu_SelectedSymbols_ActionPerformed();
                } else {

                }
            }
        }
    }

    public void run() {
        while (active) {
            try {
                if (Settings.isConnected()) {
                    if (ExchangeStore.getSharedInstance().getSelectedExchange().equals(exchange)) {
                        sendRequest((int) (servertime - clienttime) / 1000);
                    } else {
                        exchange = ExchangeStore.getSharedInstance().getSelectedExchange();
                        if (exchange != null) {
                            ispopupEnabled = true;
                            initFields();
                            setSelectedExchange();
                            sendThresholdRequest();
                            sendRequest(0);
                        }
                    }
                }
                Thread.sleep(requestTime);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public void initFields() {
        servertime = 0;
        clienttime = 0;
        valClientTime.setText("");
        valDelayTIme.setText("");
        valServerTime.setText("");
    }

    public void sendThresholdRequest() {
        if (ExchangeStore.getSharedInstance().defaultCount() > 0 && !ExchangeStore.isExpired(exchange.getSymbol()) && !ExchangeStore.isInactive(exchange.getSymbol())) {
            String str = Meta.EXCHANGE_TIME_LAG_THRESHOLD_REQUEST + Meta.DS + exchange.getSymbol();// + Meta.DS + seconds;
            SendQFactory.addData(Constants.PATH_PRIMARY, str);
            str = null;
        }
    }

    public void sendRequest(int seconds) {
        if (ExchangeStore.getSharedInstance().defaultCount() > 0 && !ExchangeStore.isExpired(exchange.getSymbol()) && !ExchangeStore.isInactive(exchange.getSymbol())) {
            String str = Meta.EXCHANGE_TIME_REQUEST + Meta.DS + exchange.getSymbol() + Meta.DS + seconds;
            SendQFactory.addData(Constants.PATH_PRIMARY, str);
            str = null;
        }
    }

    public void sendInitialRequest() {

    }

    public void actionPerformed(ActionEvent e) {
        if (chkDoNotShowMsg.isSelected()) {
            ispopupAllowed = false;
        }
    }

    public void applicationExiting() {
    }

    public void applicationLoaded() {
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
        thread.interrupt();
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {
    }
}
