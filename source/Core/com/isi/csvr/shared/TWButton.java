package com.isi.csvr.shared;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 10, 2005
 * Time: 10:30:40 PM
 */
public class TWButton extends JButton {
    private boolean gradientPainted = true;
    private String gradientLight = null;
    private String gradientDark = null;

    public TWButton() {
        super();
    }

    public TWButton(Action a) {
        super(a);
    }

    public TWButton(Icon icon) {
        super(icon);
    }

    public TWButton(String text) {
        super(text);
    }

    public TWButton(String text, Icon icon) {
        super(text, icon);
    }

    public boolean isGradientPainted() {
        return gradientPainted;
    }

    public void setGradientPainted(boolean gradientPainted) {
        this.gradientPainted = gradientPainted;
    }

    public Color getGradientDark() {
        if (gradientDark == null)
            return Theme.BUTTON_GRADIENT2_COLOR;
        else
            return Theme.getColor(gradientDark);
    }

    public void setGradientDark(String gradientDark) {
        this.gradientDark = gradientDark;
    }

    public Color getGradientLight() {
        if (gradientLight == null)
            return Theme.BUTTON_GRADIENT1_COLOR;
        else
            return Theme.getColor(gradientLight);
    }

    public void setGradientLight(String gradientLight) {
        this.gradientLight = gradientLight;
    }


}
