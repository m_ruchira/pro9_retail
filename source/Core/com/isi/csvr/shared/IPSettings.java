package com.isi.csvr.shared;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 24, 2003
 * Time: 12:07:04 PM
 * To change this template use Options | File Templates.
 */
public class IPSettings {

    private static DynamicArray ips;
    private static int ipIndex = 0;
    private static int activeIndex = 0;
    private static int IP_ROOT = 0;
    private static int currentIPClass;


    public IPSettings() {
        DataInputStream fileIn = null;
        try {
            ips = new DynamicArray(3);
            String[] dynamicIPs = TWControl.getLoadBalancerURLs(); // get load balancer URLs
            for (int i = 0; i < dynamicIPs.length; i++) {
                addIP(dynamicIPs[i], IP.DYNAMIC, IP.PRIMARY);
            }
            dynamicIPs = null;

            if (Settings.getBooleanItem("DYNAMIC_IPS_ALLOWED")) { // get the last dynamic ip
                String lastIP = Settings.getLastDynamicIP();
                if ((lastIP != null) && (lastIP.equals(""))) {
                    addIP(lastIP, IP.LAST, IP.PRIMARY);
                }
            }

            File ipFile = new File(Settings.SYSTEM_PATH + "/ip.dll"); // get static IPs
            if (ipFile.exists()) {
                try {
                    fileIn = new DataInputStream(new FileInputStream(ipFile));
                    String line;
                    boolean first = true;
                    while (true) {
                        line = fileIn.readLine();
                        if (line == null) break;
                        if (!line.trim().equals("")) {
                            if (first)
                                addIP(line.trim(), IP.STATIC, IP.PRIMARY);
                            else
                                addIP(line.trim(), IP.STATIC, IP.SECONDARAY);
                            first = false;
                        }
                    }
                    fileIn.close();
                    fileIn = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                clearOldIPs(); // if no ip file check for the st.dll for IPs
            }
            ipFile = null;

            if (!Settings.getBooleanItem("DYNAMIC_IPS_ALLOWED")) {
                discardDymanicIPs();
                ipIndex = IP_ROOT;
            }
            if (ips.size() == 0) {
                System.err.println("Error: No IPs found to connect");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static int getIPCount() {
        return ips.size();
    }

    public synchronized static IP getIP(int index) {
        return ((IP) ips.get(index));
    }

    public synchronized static String getStaticIP(int index) {
        int counter = 0;
        for (int i = 0; i < ips.size(); i++) {
            IP ip = getIP(i);
            if (ip.getType() == IP.STATIC) {
                if (counter == index) {
                    return ip.getIP();
                }
                counter++;
            }
            ip = null;
        }
        return "";
    }

    public synchronized static void clearStaticIPList() {
        for (int i = ips.size() - 1; i >= 0; i--) {
            IP ip = (IP) ips.get(i);
            if (ip.getType() == IP.STATIC) {
                ips.remove(i);
            }
        }
    }

    /*public static void setIP(int index, String ip) {
        try {
            ips.set(index, ip);
        } catch (Exception e) {
            ips.add(ip);
        }
    }*/

    public synchronized static void addIP(String ip, int type, int ipClass) {
        ips.add(new IP(ip, type, ipClass));
    }

    public synchronized static void save() {
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(Settings.SYSTEM_PATH + "/ip.dll");
            for (int i = 0; i < ips.size(); i++) {
                IP ip = getIP(i);
                if ((ip != null) && (ip.getType() == IP.STATIC)) {
                    fileOut.write(ip.getIP().getBytes());
                    fileOut.write("\r\n".getBytes());
                }
                ip = null;
            }
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileOut = null;
    }

    public synchronized static int getIpIndex() {
        return activeIndex;
    }

    public synchronized static void resetIPIndex() {
        ipIndex = IP_ROOT;
    }

    public synchronized static IP getNextIP() throws Exception {

        IP ip = getIP(ipIndex);
        activeIndex = ipIndex;
        if (ip == null) {
            if (ipIndex == IP_ROOT) {
                throw new Exception("No IPs found");
            } else {
                ip = getIP(--ipIndex);
                activeIndex = ipIndex;
            }
        } else {
            if (ipIndex == (getIPCount() - 1)) {
                ipIndex = IP_ROOT;
            } else {
                ipIndex++;
            }
        }
        currentIPClass = ip.getIpClass();
        return ip;
    }

    public static int getCurrentIPClass() {
        return currentIPClass;
    }

    public static synchronized void discardDymanicIPs() {
        for (int i = 0; i < ips.size(); i++) {
            IP ip = getIP(i);
            if ((ip != null) && (ip.getType() == IP.STATIC)) {
                IP_ROOT = i;
                break;
            }
            ip = null;
        }
    }

    public static void includeDynamicIPs() {
        IP_ROOT = 0;
    }

    /**
     * To enable backward compatibility.
     * Removes IPs from Settings file and add them to the IP file
     */
    private synchronized void clearOldIPs() {
        try {
            boolean cleared = false;
            String ip1 = Settings.getItem("SERVER_IP1");
            if ((ip1 != null) && (!ip1.trim().equals(""))) {
                clearStaticIPList();
                cleared = true;
                addIP(ip1, IP.STATIC, IP.PRIMARY);
            }
            String ip2 = Settings.getItem("SERVER_IP2");
            if ((ip1 != null) && (!ip1.trim().equals(""))) {
                if (!cleared)
                    clearStaticIPList();
                addIP(ip2, IP.STATIC, IP.SECONDARAY);

            }
            ip1 = null;
            ip2 = null;
            Settings.RemoveItem("SERVER_IP1");
            Settings.RemoveItem("SERVER_IP2");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}