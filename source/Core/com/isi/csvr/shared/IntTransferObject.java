package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 3, 2004
 * Time: 2:24:55 PM
 */
public class IntTransferObject implements TransferObject {
    private long flag;
    private int value;
    private boolean blank;

    public IntTransferObject() {
    }

    public int getValue() {
        return value;
    }

    public IntTransferObject setValue(int value) {
        this.value = value;
        return this;
    }

    public long getFlag() {
        return flag;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }

    public int getCellValue() {
        return value;
    }

    public void setCellValue(int value) {
        this.value = value;
    }

    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }
}
