package com.isi.csvr.virtualkeyboard;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.TWFont;
import com.isi.csvr.shared.TWPasswordField;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: Apr 29, 2005
 * Time: 10:57:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class VirtualKeyboard extends JDialog implements ActionListener, WindowFocusListener {

    private JPanel virtual, numPanel, keyPanel1, keyPanel2, keyPanel3;
    //    private jButton A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z;
//    private jButton num1, num2, num3, num4, num5, num6, num7, num8, num9, num0;
//    private jButton syb1, syb2, syb3, syb4, syb5, syb6, syb7, syb8, syb9, syb0, syb10, syb11, syb12, syb13, syb14;
    private JButton close, clear, caps, backspace;
    //private JToggleButton caps;

    private boolean isCapsClicked = false;
    private String[][] charArray = {{"Q", "q"}, {"W", "w"}, {"E", "e"}, {"R", "r"}, {"T", "t"}, {"Y", "y"}, {"U", "u"}, {"I", "i"}, {"O", "o"}, {"P", "p"}, {"{", "["}, {"}", "]"}, {"A", "a"}, {"S", "s"}, {"D", "d"}, {"F", "f"}, {"G", "g"}, {"H", "h"}, {"J", "j"}, {"K", "k"}, {"L", "l"}, {":", ";"}, {"\"", "\'"}, {"Z", "z"}, {"X", "x"}, {"C", "c"}, {"V", "v"}, {"B", "b"}, {"N", "n"}, {"M", "m"}, {"<", ","}, {">", "."}, {"?", "/"}};
    private String[][] numArray = {{"~", "`"}, {"!", "1"}, {"@", "2"}, {"#", "3"}, {"$", "4"}, {"%", "5"}, {"^", "6"}, {"&", "7"}, {"*", "8"}, {"(", "9"}, {")", "0"}, {"_", "-"}, {"+", "="}, {"|", "\\"}};
    private VKButton[] charButton;
    private VKButton[] numButton;
    private String str = "";
    private String keyBuffer = "";
    private String sThemeID;
    private TWPasswordField passwordField;
//    private VirtualKeyboardTextField passwordField;

    private Container container = getContentPane();

    public VirtualKeyboard(TWPasswordField jt, JDialog parent) {
        super(parent);
        setUndecorated(true);
        createUI(jt);
    }

    public VirtualKeyboard(TWPasswordField jt, JFrame parent) {
        super(parent);
        setUndecorated(true);
        createUI(jt);
    }

    public void createUI(TWPasswordField jt) {
        passwordField = jt;

        ((JPanel) getContentPane()).setBorder(BorderFactory.createLineBorder(Color.black));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize(400, 150);
        this.setResizable(false);
        this.setFocusable(true);
        this.addWindowFocusListener(this);

        //container.setLayout(new FlowLayout(2, 0, 0));

        virtual = new JPanel();
        virtual.setLayout(new GridLayout(4, 1, 0, 0));
        Border eched = BorderFactory.createEtchedBorder(Color.blue, Color.gray);
        Border titled = BorderFactory.createTitledBorder(eched, Language.getString("VIRTUAL_KEYBOARD"));
        virtual.setBorder(titled);
        Font font = new TWFont("Arial", Font.BOLD, 16);

        close = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        clear = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        caps = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        backspace = new JButton() {
            public Insets getInsets() {
                return new Insets(1, 1, 1, 1);    //To change body of overridden methods use File | Settings | File Templates.
            }
        };
        clear.setPreferredSize(new Dimension(60, 20));
        close.setPreferredSize(new Dimension(60, 20));
        caps.setPreferredSize(new Dimension(60, 20));
        backspace.setPreferredSize(new Dimension(60, 20));
        clear.setFont(font);
        clear.setText(Language.getString("CLEAR"));
        close.setFont(font);
        close.setText(Language.getString("CLOSE"));
        caps.setFont(font);
        backspace.setFont(font);
        //caps.setIcon();


        close.addActionListener(this);
        clear.addActionListener(this);
        caps.addActionListener(this);
        backspace.addActionListener(this);

        charButton = new VKButton[33];
        for (int i = 0; i < charButton.length; i++) {
            charButton[i] = new VKButton(charArray[i][1]);
            charButton[i].addActionListener(this);
        }

        numButton = new VKButton[14];
        for (int i = 0; i < numButton.length; i++) {
            numButton[i] = new VKButton(numArray[i][1]);
            numButton[i].addActionListener(this);
        }
        caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_up.gif"));
        backspace.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/backspace.gif"));
        numPanel = new JPanel();
        for (int i = 0; i < numButton.length; i++) {
            numPanel.add(numButton[i]);
        }

        keyPanel1 = new JPanel();
        for (int i = 0; i < 12; i++) {
            keyPanel1.add(charButton[i]);
        }
        keyPanel1.add(backspace);
        keyPanel2 = new JPanel();
        keyPanel2.add(caps);
        for (int i = 0; i < 11; i++) {
            keyPanel2.add(charButton[i + 12]);
        }

        keyPanel3 = new JPanel();
        keyPanel3.add(clear);
        for (int i = 0; i < 10; i++) {
            keyPanel3.add(charButton[i + 23]);
        }
        keyPanel3.add(close);

        virtual.add(numPanel, BorderLayout.NORTH);
        virtual.add(keyPanel1, BorderLayout.NORTH);
        virtual.add(keyPanel2, BorderLayout.NORTH);
        virtual.add(keyPanel3, BorderLayout.NORTH);

        container.add(virtual, BorderLayout.NORTH);
    }

    public String editText(String key) {
        str = passwordField.getText() + key;
        return str;
    }

    public void setTextField(TWPasswordField jt) {
        jt.setText(Meta.DS + str);
    }

    public void windowGainedFocus(WindowEvent we) {
        //this.setVisible(true);
    }

    public void windowLostFocus(WindowEvent we) {
        this.setVisible(false);
    }

    public void actionPerformed(ActionEvent ex) {
        Object obj = ex.getSource();
        if (obj == close) {
            this.setVisible(false);
        } else if (obj == clear) {
            str = "";
        } else if (obj == caps) {
            if (isCapsClicked) {
                isCapsClicked = false;
                setKeyboard();
            } else {
                isCapsClicked = true;
                setKeyboard();
            }
        } else if (obj == backspace) {
            try {
                str = str.substring(0, str.length() - 1);
            } catch (StringIndexOutOfBoundsException e) {
                //java.lang.StringIndexOutOfBoundsException: String index out of range: -1
            }
        } else {
            JButton but = (JButton) obj;
            editText(but.getText());
        }

        setTextField(passwordField);
    }

    private void setKeyboard() {
        if (isCapsClicked) {
            for (int i = 0; i < charButton.length; i++) {
                charButton[i].setText(charArray[i][0]);
            }
            for (int i = 0; i < numButton.length; i++) {
                numButton[i].setText(numArray[i][0]);
            }
            caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_down.gif"));
        } else {
            for (int i = 0; i < charButton.length; i++) {
                charButton[i].setText(charArray[i][1]);
            }
            for (int i = 0; i < numButton.length; i++) {
                numButton[i].setText(numArray[i][1]);
            }
            caps.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/caps_up.gif"));
        }
    }

    class VKButton extends JButton {
        public VKButton() {
            setButton();
        }

        public VKButton(String sr) {
            setButton();
            this.setText(sr);
        }

        public void setButton() {
            this.setPreferredSize(new Dimension(20, 20));
            Font font = new TWFont("Arial", Font.BOLD, 16);
            this.setFont(font);
        }

        public Insets getInsets() {
            return new Insets(1, 1, 1, 1);
        }
    }
}
