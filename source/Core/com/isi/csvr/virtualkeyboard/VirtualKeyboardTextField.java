package com.isi.csvr.virtualkeyboard;

import com.isi.csvr.shared.TWPasswordField;
import com.isi.csvr.shared.ValueFormatter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: May 4, 2005
 * Time: 9:17:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class VirtualKeyboardTextField extends JPasswordField implements MouseListener, KeyListener {

    public boolean isRandomLocation = false;
    private VirtualKeyboard jframe;
    private Toolkit tk = getToolkit();
    private Dimension screen;
    private TWPasswordField passwordFiled;
    private boolean useVurtualKeyboard = false;

    /*public VirtualKeyboardTextField(JFrame parent) {
        super("");

        this.addKeyListener(this);
        this.addMouseListener(this);
        setDocument(new ValueFormatter(ValueFormatter.BLANK));
        //this.setEditable(false);
        jframe = new VirtualKeyboard(this, parent);
    }*/

    public VirtualKeyboardTextField(JDialog parent, boolean enableVKeyboard, boolean randomLocation, TWPasswordField passwordFiled) {
        super("");
        useVurtualKeyboard = enableVKeyboard;
        isRandomLocation = randomLocation;
        this.passwordFiled = passwordFiled;
//        parentDialog = parent;
        this.addKeyListener(this);
        this.addMouseListener(this);
        jframe = new VirtualKeyboard(passwordFiled, parent);
        setDocumentType();
    }

    public void setDocumentType() {
        if (useVurtualKeyboard) {
            setDocument(new ValueFormatter(ValueFormatter.BLANK));
        } else {
            setDocument(new ValueFormatter(ValueFormatter.NORMAL));
        }
        passwordFiled.setText("");
    }

    public void setCaretPosition(int position) {

        super.setCaretPosition(position);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void showVirtualKeyboard() {
        createFrame();
    }

    public void setUseVirtualKeyboard(boolean status) {
        useVurtualKeyboard = status;
        setDocumentType();
    }


    private synchronized void createFrame() {
        if (jframe.isVisible()) return;

        Point loc = this.getLocation();
        SwingUtilities.convertPointToScreen(loc, this.getParent());
        if (isRandomLocation) {
            Random rn = new Random();
            screen = tk.getScreenSize();
            int rint = 0;
            rint = rn.nextInt((screen.width - 400));
            jframe.setLocation(rint, loc.y + this.getHeight() + 2);
        } else {
            jframe.setLocation(loc.x, (loc.y + this.getHeight() + 2));
        }
        jframe.setVisible(true);
    }

    public String getText() {
        return new String(this.getPassword());
    }

    public void setPasswordMode(boolean passwordMode) {
        if (passwordMode)
            this.setEchoChar('#');
        else
            this.setEchoChar((char) 0);
    }

    public void mouseEntered(MouseEvent mx) {
    }

    public void mouseExited(MouseEvent mx) {
    }

    public void mouseReleased(MouseEvent mx) {
    }

    public void mouseClicked(MouseEvent mx) {
        if (useVurtualKeyboard)
            showVirtualKeyboard();
        //createFrame();
    }

    public void mousePressed(MouseEvent mx) {
    }

    public void keyPressed(KeyEvent kx) {
        if (useVurtualKeyboard) {
            kx.consume();
            showVirtualKeyboard();
        }
    }

    public void keyReleased(KeyEvent kx) {
    }

    public void keyTyped(KeyEvent kx) {
    }


   /* protected void paintBorder(Graphics g) {
        Border border = getBorder();
        if (border != null) {
            border.paintBorder(this, g, 0, 0, getWidth() - 20, getHeight() - 5);
        }
    }


    public void paint(Graphics g) {
        super.paint(g);    //To change body of overridden methods use File | Settings | File Templates.
    }

    protected void paintChildren(Graphics g) {
        g.setClip(0, 0, getWidth() - 20, getHeight() - 5);
        g.clipRect(0, 0, getWidth() - 20, getHeight() - 5);
        Graphics ga = g.create(0, 0, getWidth() - 20, getHeight() - 5);
        super.paintChildren(g);    //To change body of overridden methods use File | Settings | File Templates.
    }


    protected void paintComponent(Graphics g) {
        g.setClip(0, 0, getWidth() - 20, getHeight() - 5);
        g.clipRect(0, 0, getWidth() - 20, getHeight() - 5);
        Graphics ga = g.create(0, 0, getWidth() - 20, getHeight() - 5);
        super.paintComponent(g);    //To change body of overridden methods use File | Settings | File Templates.
    }*/
}
