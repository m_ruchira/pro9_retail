package com.isi.csvr;

import com.isi.csvr.customindex.CustomIndexListener;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.globalIndex.GlobalIndexModel;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.properties.*;
import com.isi.csvr.sectoroverview.SectorOverviewUI;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.IndicesModal;
import com.isi.csvr.table.Table;
import com.isi.csvr.trade.AllTradesModel;
import com.isi.csvr.trade.TradeStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 5, 2005
 * Time: 9:19:27 AM
 */
public class MainBoardLoader implements ConnectionListener, ExchangeListener, ActionListener, CustomIndexListener {
    public static Hashtable<String, SectorOverviewUI> tableStore = new Hashtable<String, SectorOverviewUI>();
    public LinkedList<JInternalFrame> mainboards;
    public Hashtable<String, JInternalFrame> mainboardList;
    private Client client;
    private Table oSectorTable;
    private Table oGlobalIndexTable;
    private ArrayList exchanges;
    private TWComboBox exchangeCombo;
    private IndicesModal oSectorModel;
    private GlobalIndexModel oGlobalIndexModel;
    private JPanel northPanel;
    private Hashtable<String, InternalFrame> timenSalesFrames;
    private WorkInProgressIndicator workInProgressIndicator;
    private LinkedList<JInternalFrame> nonMainboards;
    private boolean isFirstTime = true;
    private String lastSelectedExchange = null;


    public MainBoardLoader(Client client) {
        this.client = client;
        timenSalesFrames = new Hashtable<String, InternalFrame>();
        mainboards = new LinkedList<JInternalFrame>();
        mainboardList = new Hashtable<String, JInternalFrame>();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        nonMainboards = new LinkedList<JInternalFrame>();
        isFirstTime = true;
    }

    /*public void loadUI() {
        loadInitialWindows();
    }*/

    public void setFirstTimeLoaded(boolean firstTime) {
        isFirstTime = firstTime;
    }

    private void createMarketTables(Exchange exchange, boolean isCreateForNewExchange) {
        /*
        Modified by Chandika
        Adding Pre defined watchlists for exchange nodes
         */


        Vector oViewSettingVec = ViewSettingsManager.getViewsVector();
        Object[] views = oViewSettingVec.toArray();
        ViewSettingComparator comparator = new ViewSettingComparator();
        comparator.setColumn(ViewSettingComparator.CAPTION);
        Arrays.sort(views, comparator);
        DefaultTreeModel oTreeModel = (DefaultTreeModel) client.oTree.getModel();
        DefaultMutableTreeNode exchangeNode = new DefaultMutableTreeNode(exchange.getDescription());
        DefaultMutableTreeNode sectorNode = new DefaultMutableTreeNode(exchange.getDescription());
        DefaultMutableTreeNode parentNode = client.getMarketWatchTreeNode();
        boolean alreadyInMarket = (findNode(parentNode, exchange.getDescription()) != null);
        int i = 0;

        if (!alreadyInMarket) {
            boolean marketTreeAdded = false;
            // add the new node to the corret location in alphabetical order
//            insertTreeNode(parentNode, exchangeNode);
            if ((exchange.isUserSubMarketBreakdown()) && (exchange.getSubMarketCount() > 0)) {
                insertTreeNode(parentNode, exchangeNode);
                marketTreeAdded = true;
                for (Market subMarket : exchange.getSubMarkets()) {
                    DefaultMutableTreeNode subMarketNode = new DefaultMutableTreeNode(subMarket.getDescription());
                    if (exchange.isDefault()) {
                        insertTreeNode(exchangeNode, subMarketNode);
                    }
                    for (int j = 0; j < views.length; j++) {
                        ViewSetting oSettings = ((ViewSetting) views[j]).getObject();
                        if ((oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) && (!oSettings.getID().equals("8")) && (exchange.isDefault())) {   //&&(exchange.isDefault())
                            addMarketTreeNode(exchange, subMarket, subMarketNode, oTreeModel, oSettings, false, isCreateForNewExchange);
                        }
                    }
                }
            } else {
                while (i < views.length) {
                    ViewSetting oSettings = ((ViewSetting) views[i]).getObject();
                    if ((oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) && (!oSettings.getID().equals("8")) && (exchange.isDefault())) {   //&&(exchange.isDefault())
                        if (!marketTreeAdded) {
                            insertTreeNode(parentNode, exchangeNode);
                            marketTreeAdded = true;
                        }
                        addMarketTreeNode(exchange, null, exchangeNode, oTreeModel, oSettings, false, isCreateForNewExchange);
                    }
                    i++;
                }
            }
            try {
//                insertTreeNode(parentNode, exchangeNode);
                int ii = 0;
                while (ii < views.length) {
                    ViewSetting oSettings = ((ViewSetting) views[ii]).getObject();
                    if ((oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) && (oSettings.getID().equals("8")) && (exchange.isExchangehasIndicesWatchListsExists())) {

                        Enumeration exgEnum = exchange.getExchangeIndices().keys();
                        ArrayList<String> sortedKyes = new ArrayList<String>();
                        while (exgEnum.hasMoreElements()) {
                            sortedKyes.add((String) exgEnum.nextElement());
                        }
                        Collections.sort(sortedKyes);
                        for (String key : sortedKyes) {
                            String id = key;
                            Hashtable temphash = (Hashtable) ((Hashtable) exchange.getExchangeIndices()).get(id);
                            Enumeration symbolEnum = temphash.keys();
                            String title;
                            Symbols symbols = new Symbols();
                            while (symbolEnum.hasMoreElements()) {
                                title = (String) symbolEnum.nextElement();
                                String symbolList = (String) temphash.get(title);
                                String[] symbolArray = symbolList.split(",");
                                for (int j = 0; j < symbolArray.length; j++) {
                                    symbols.appendSymbol(symbolArray[j]);
                                }
                                if (!marketTreeAdded) {
                                    insertTreeNode(parentNode, exchangeNode);
                                    marketTreeAdded = true;
                                }
                                addIndicesTreeNode(exchange, null, exchangeNode, oTreeModel, oSettings, false, isCreateForNewExchange, title, symbols, id);
                            }
                        }
                    }
                    ii++;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (exchange.isDefault()) {
            parentNode = client.getSectorsTreeNode();
            boolean alreadyInSectors = (findNode(parentNode, exchange.getDescription()) != null);
            try {
                if (!alreadyInSectors) {
                    if ((exchange.isUserSubMarketBreakdown()) && (exchange.getSubMarketCount() > 0)) {
                        Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                        if (sectorsEnum.hasMoreElements() && ExchangeStore.isValidSystemFinformationType(Meta.IT_SECTOR_OVERVIEW)) {
                            Sector sector1 = new Sector("newSector", Language.getString("SECTOR_OVERVIEW"));
                            addSectorOverviewTreeNode(exchange, null, sector1, sectorNode, oTreeModel, true, isCreateForNewExchange);
                        }
                        for (Market subMarket : exchange.getSubMarkets()) {
                            DefaultMutableTreeNode subMarketNode = new DefaultMutableTreeNode(subMarket.getDescription());
                            insertTreeNode(sectorNode, subMarketNode);
                            Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                            while (sectors.hasMoreElements()) {
                                Sector sector = sectors.nextElement();
                                addSectorTreeNode(exchange, subMarket, sector, subMarketNode, oTreeModel, true, isCreateForNewExchange);
                                sector = null;
                            }
                            if (SymbolList.isUnclassifiedSectorNeeds) {
                                Sector sector = new Sector(Constants.DEFAULT_SECTOR, Language.getString("UNCLASSIFIED_SECTOR"));
                                addSectorTreeNode(exchange, subMarket, sector, subMarketNode, oTreeModel, true, isCreateForNewExchange);
                                LinkedList<Sector> Exsectors = SectorStore.getSharedInstance().getSectorLIst(exchange.getSymbol());
                                Exsectors.add(sector);
                                SectorStore.getSharedInstance().setSectorLIst(exchange.getSymbol(), Exsectors);
                            }

                        }
                    } else {
                        Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                        if (sectors.hasMoreElements() && ExchangeStore.isValidSystemFinformationType(Meta.IT_SECTOR_OVERVIEW)) {
                            Sector sector1 = new Sector("newSector", Language.getString("SECTOR_OVERVIEW"));
                            addSectorOverviewTreeNode(exchange, null, sector1, sectorNode, oTreeModel, true, isCreateForNewExchange);
//                        } else {
//                            insertTreeNode(parentNode, sectorNode);
                        }
                        while (sectors.hasMoreElements()) {
                            Sector sector = sectors.nextElement();
                            addSectorTreeNode(exchange, null, sector, sectorNode, oTreeModel, true, isCreateForNewExchange);
                            sector = null;
                        }
                        if (SymbolList.isUnclassifiedSectorNeeds) {
                            Sector sector = new Sector(Constants.DEFAULT_SECTOR, Language.getString("UNCLASSIFIED_SECTOR"));
                            addSectorTreeNode(exchange, null, sector, sectorNode, oTreeModel, true, isCreateForNewExchange);
                            LinkedList<Sector> Exsectors = SectorStore.getSharedInstance().getSectorLIst(exchange.getSymbol());
                            Exsectors.add(sector);
                            SectorStore.getSharedInstance().setSectorLIst(exchange.getSymbol(), Exsectors);
                        }
                    }
                    if (sectorNode.getChildCount() > 0) {
                        insertTreeNode(parentNode, sectorNode);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //Adding indices tree node

//        parentNode = client.getIndicesTreeNode();
//        boolean alreadyInIndices = (findNode(parentNode, exchange.getDescription()) != null);
//        DefaultMutableTreeNode exchangeIndicesNode = new DefaultMutableTreeNode(exchange.getDescription());
//        try {
//            if (!alreadyInIndices) {
//                insertTreeNode(parentNode, exchangeIndicesNode);
//                int ii = 0;
//                while (ii < views.length) {
//                    ViewSetting oSettings = ((ViewSetting) views[ii]).getObject();
//                    if ((oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) && (oSettings.getID().equals("8")) && (exchange.isExchangehasIndicesWatchLists())) {
//
////                        Enumeration exgEnum = exchange.getExchangeIndices().elements();
//                        Enumeration exgEnum = exchange.getExchangeIndices().keys();
//                        while (exgEnum.hasMoreElements()) {
//                            String id=(String)exgEnum.nextElement();
//                            Hashtable temphash = (Hashtable)((Hashtable) exchange.getExchangeIndices()).get(id);
//                            Enumeration symbolEnum = temphash.keys();
//                            String title;
//                            Symbols symbols = new Symbols();
//                            while (symbolEnum.hasMoreElements()) {
//                                title = (String) symbolEnum.nextElement();
//                                String symbolList=(String)temphash.get(title);
//                                String[] symbolArray = symbolList.split(",");
//                                for (int j = 0; j < symbolArray.length; j++) {
//                                    symbols.appendSymbol(symbolArray[j]);
//                                }
//                                addIndicesTreeNode(exchange, null, exchangeIndicesNode, oTreeModel, oSettings, false, isCreateForNewExchange,title,symbols,id);
//                            }
//                        }
//                    }
//                    ii++;
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    }

    /*private void createMarketTables(Exchange exchange, boolean isCreateForNewExchange) {
        Vector oViewSettingVec = ViewSettingsManager.getViewsVector();
        Object[] views = oViewSettingVec.toArray();
        ViewSettingComparator comparator = new ViewSettingComparator();
        comparator.setColumn(ViewSettingComparator.CAPTION);
        Arrays.sort(views, comparator);

        DefaultTreeModel oTreeModel = (DefaultTreeModel) client.oTree.getModel();
        DefaultMutableTreeNode exchangeNode = new DefaultMutableTreeNode(exchange.getDescription());
        DefaultMutableTreeNode sectorNode = new DefaultMutableTreeNode(exchange.getDescription());

        DefaultMutableTreeNode parentNode = client.g_oMarketWatchTreeNode;
        boolean alreadyInMarket = (findNode(parentNode, exchange.getDescription()) != null);

//        Properties initialSettings = WorkspaceManager.loadInitalWorkspace();

        int i = 0;

        if (!alreadyInMarket) {
            //parentNode.add(exchangeNode);
            // add the new node to the corret location in alphabetical order
            insertTreeNode(parentNode, exchangeNode);
                ///
            if ((exchange.isUserSubMarketBreakdown()) && (exchange.getSubMarketCount() > 0)){
                for (Market subMarket: exchange.getSubMarkets()){
                    DefaultMutableTreeNode subMarketNode = new DefaultMutableTreeNode(subMarket.getDescription());
                    //exchangeNode.add(subMarketNode);
                    insertTreeNode(exchangeNode, subMarketNode);
                    for (int j = 0; j < views.length; j++) {
                        ViewSetting oSettings = ((ViewSetting) views[j]).getObject();
                        if (oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW){
                            addMarketTreeNode(exchange, subMarket, subMarketNode, oTreeModel, oSettings, false, isCreateForNewExchange);
                            sleepThread();
                        }
                    }
                }
            } else {
                //parentNode.add(exchangeNode);
                while (i < views.length) {
                    ViewSetting oSettings = ((ViewSetting) views[i]).getObject();
                    if (oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW){
                        addMarketTreeNode(exchange, null, exchangeNode, oTreeModel, oSettings, false, isCreateForNewExchange);
                        sleepThread();
                    }
                    i++;
                }
            }
        }

        /*int i = 0;
        if (!alreadyInMarket) {
            parentNode.add(exchangeNode);
            while (i < views.length) {
                ViewSetting oSettings = (ViewSetting) views[i];
                if (oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW){
                    addMarketTreeNode(exchange, exchangeNode, oTreeModel, oSettings, false, isCreateForNewExchange);
                    sleepThread();
                }
                i++;
            }
        }*

        parentNode = client.g_oSectorsTreeNode;
        boolean alreadyInSectors = (findNode(parentNode, exchange.getDescription()) != null);

        try {
            if (!alreadyInSectors) {
                Enumeration<Sector> sectors = SectorStore.getSharedInstance().getSectors(exchange.getSymbol());
                while (sectors.hasMoreElements()) {
                    Sector sector = sectors.nextElement();
                    addSectorTreeNode(exchange, sector, sectorNode, oTreeModel, true, isCreateForNewExchange);
                    sector = null;
                    sleepThread();
                }
                if (sectorNode.getChildCount() > 0){
                     insertTreeNode(parentNode, sectorNode);
                    //client.g_oSectorsTreeNode.add(sectorNode);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            client.getTree().updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void insertTreeNode(DefaultMutableTreeNode parent, DefaultMutableTreeNode child) {
        boolean nodeInserted = false;
        if (parent.getChildCount() == 0) {
            parent.add(child);
        } else {
            for (int c = 0; c < parent.getChildCount(); c++) {
                if (parent.getChildAt(c).toString().compareTo(child.toString()) > 0) {
                    parent.insert(child, c);
                    nodeInserted = true;
                    break;
                }
            }
            if (!nodeInserted) {
                parent.add(child);
            }
        }
    }

    public DefaultMutableTreeNode findNode(DefaultMutableTreeNode parentNode, String caption) {
        try {
            DefaultMutableTreeNode treenode = (DefaultMutableTreeNode) parentNode.getFirstChild();
            while (treenode != null) {
                String nodeCaption = (String) treenode.getUserObject();

                if ((nodeCaption != null) && (nodeCaption.equals(caption))) {
                    return treenode;
                }
                treenode = treenode.getNextSibling();
            }
        } catch (Exception e) {

        }
        return null;
    }

    public boolean isIndicesFileUpdated() {
        return true;
    }

    private void loadInitialWindows() {
        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if ((exchange.isDefault()) || (exchange.isExchangehasIndicesWatchListsExists())) { // this is a default exchange
                    createMarketTables(exchange, false);
                    AllTradesModel model = new AllTradesModel();
                    createTimeSalesFrame(exchange, model);
                }
                exchange = null;
            }
        } catch (Exception e) {
        }

        if (client.getMarketWatchTreeNode().getChildCount() == 0) {
            client.removeMarketTreeNode();
        }

        if (client.getSectorsTreeNode().getChildCount() == 0) {
            client.removeSectorTreeNode();
        }
    }

    public void showInitializinMessage(boolean needThread) {
//        if (client.getFrame().getExtendedState() != JFrame.ICONIFIED) {
//
//            if (needThread) {
//                if (workInProgressIndicator == null) {
//                    workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_WORKSPACE);
//                    new Thread("Work in progress Indicator - Mainboard") {
//
//                        public void run() {
//                            workInProgressIndicator.setVisible(true);
//                        }
//                    }.start();
//                }
//            } else {
//                if (workInProgressIndicator == null) {
//                    workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_WORKSPACE);
//                }
//                new Thread("Work in progress Indicator - Mainboard2") {
//
//                    public void run() {
//                        workInProgressIndicator.setVisible(true);
//                    }
//                }.start();
//            }
//        }

        if (client.getFrame().getExtendedState() != JFrame.ICONIFIED) {
            if (needThread) {
                if (workInProgressIndicator == null) {
                    new Thread("Work in progress Indicator - Mainboard") {

                        public void run() {
                            workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.INITIALIZIG);
                            workInProgressIndicator.setVisible(true);
                        }
                    }.start();
//                workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.INITIALIZIG);
                }
            } else {
                if (workInProgressIndicator == null) {
                    workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.INITIALIZIG);
                }
                workInProgressIndicator.setVisible(true);
            }
//            workInProgressIndicator.setVisible(true);
        }

    }

    public void closeInitializinMessage() {
        if (workInProgressIndicator != null) {
            workInProgressIndicator.dispose();
            workInProgressIndicator = null;
        }
    }

    /*private void addSectorTreeNode(Exchange exchange, Sector sector, DefaultMutableTreeNode oParentNode,
                                   DefaultTreeModel oTreeModel, boolean bSortable, boolean isCreateForNewExchange) {

        ViewSetting viewSettings;
        String viewID = sector.getId() + Meta.FD + exchange.getSymbol();

        try {
            viewSettings = (ViewSetting) ViewSettingsManager.getView(0, "1").clone();
        } catch (Exception e) {
            viewSettings = new ViewSetting();
            viewSettings.setColumns(BigInteger.valueOf(1));
        }
        ViewSettingsManager.addMainBoardSetting(viewSettings);
        viewSettings.setType(ViewSettingsManager.SECTOR_VIEW << 8);
        viewSettings.setID(viewID);
        viewSettings.setCaptions(sector.getDescription());
        viewSettings.setRenderingIDs(ViewSettingsManager.getRenderingIDs(ViewSettingsManager.SECTOR_VIEW));
        viewSettings.setColumnHeadings(Language.getList("TABLE_COLUMNS"));
        SymbolList symbolList = new SymbolList(exchange.getSymbol(), sector.getId(), "", "");
        viewSettings.setSymbols(symbolList.getSymbols());
        symbolList = null;


        ClientTable oTable = new ClientTable();
        oTable.setResizable(true);
        oTable.setMaximizable(true);
        oTable.setIconifiable(true);
        oTable.setClosable(true);
        oTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        oTable.setTypeDescription(Language.getString("SECTOR_WATCH") + " - " + exchange.getSymbol());
        //oTable.setDataStore(client.g_oDataStore);
        client.oTopDesktop.add(oTable);
        oTable.setWindowType(Meta.WT_SectorView);
        oTable.setViewSettings(viewSettings);
        oTable.setSymbols(viewSettings.getSymbols());
        oTable.setSortable(bSortable);

        DefaultMutableTreeNode node = new DefaultMutableTreeNode(oTable);
        oParentNode.add(node);

        oTable.setTreePath((Object[]) oTreeModel.getPathToRoot(node));

        oTable.createTable(true);
        oTable.getTable().addMouseListener(client.g_oSymbolMouseListener);
        if (isCreateForNewExchange) {
            GUISettings.setColumnSettings(viewSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));
        }
        oTable.adjustColumns();
        oTable.getScrollPane().getViewport().addMouseListener(client.g_oTableMouseListener);

        oTable.applySettings();
        oTable.updateUI();
    }*/

    private void removeViews(Exchange exchange) {
        Vector views = ViewSettingsManager.getMainBoardViews();
        for (int i = 0; i < views.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (views.elementAt(i));
            //Bug ID <#????> - to remove the sub market views from the mainboard and from the tree view
            String excg = "";
            try {
                excg = (oSetting.getID().split(Meta.FD)[1]).trim();
            } catch (Exception e) {
                try {
                    excg = oSetting.getID().substring(oSetting.getID().lastIndexOf(Meta.FD));
                } catch (Exception e1) {
                    excg = oSetting.getID();
                }
            }
            if (excg.equals(exchange.getSymbol())) {
                //            if (oSetting.getID().endsWith(Meta.FD + exchange.getSymbol())) {
                try {
//                    ViewSettingsManager.removeMainBoardSetting(oSetting);
                    ViewSettingsManager.addToremoveFromMainBoardSettingList(oSetting);
                    ClientTable table = (ClientTable) oSetting.getParent();
                    table.setVisible(false);
                    Client.getInstance().getDesktop().remove(table);

                    // remove from the tree
                    try {
                        DefaultMutableTreeNode parent = (DefaultMutableTreeNode) table.getTreePath().getPath()[table.getTreePath().getPath().length - 2];
                        parent.removeAllChildren();
                        parent.removeFromParent();
                        SharedMethods.updateComponent(Client.getInstance().getTree());
//                        Client.getInstance().getTree().updateUI();
                        /*try {
                            DefaultMutableTreeNode superparent = (DefaultMutableTreeNode) table.getTreePath().getPath()[table.getTreePath().getPath().length - 3];
                            if(superparent.getChildCount()==0){
                                superparent.removeFromParent();
                            }
                            Client.getInstance().getTree().updateUI();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        DefaultMutableTreeNode parentNode = client.getMarketWatchTreeNode();
                        DefaultMutableTreeNode exchangeNode = new DefaultMutableTreeNode(exchange.getDescription());
                        boolean alreadyInExchanges = (findNode(parentNode, exchange.getDescription()) != null);
                        DefaultMutableTreeNode node;
                        if (alreadyInExchanges) {
                            Enumeration childs = parentNode.children();
                            while (childs.hasMoreElements()) {
                                try {
                                    node = (DefaultMutableTreeNode) childs.nextElement();
                                    if (node.toString().equals(exchangeNode.toString())) {
                                        if (node.getChildCount() == 0) {
                                            node.removeFromParent();
                                        }
                                        break;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        }
                        SharedMethods.updateComponent(Client.getInstance().getTree());
//                        Client.getInstance().getTree().updateUI();
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    try {
                        table.setVisible(false);
                        client.getDesktop().remove(table);
                        table.dispose();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    table = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            oSetting = null;
        }
        ViewSettingsManager.removeViewsFromList();
        try {
            DefaultMutableTreeNode parentNode = client.getMarketWatchTreeNode();
            DefaultMutableTreeNode exchangeNode = new DefaultMutableTreeNode(exchange.getDescription());
            boolean alreadyInExchanges = (findNode(parentNode, exchange.getDescription()) != null);
            DefaultMutableTreeNode node;
            if (alreadyInExchanges) {
                Enumeration childs = parentNode.children();
                while (childs.hasMoreElements()) {
                    try {
                        node = (DefaultMutableTreeNode) childs.nextElement();
                        if (node.toString().equals(exchangeNode.toString())) {
//                            if(node.getChildCount()==0){
//                                node.removeFromParent();
//                            }
                            node.removeAllChildren();
                            node.removeFromParent();
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
//            Client.getInstance().getTree().updateUI();
            SharedMethods.updateComponent(Client.getInstance().getTree());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //Bug ID <#????> - to remove the sector views from the tree view
        try {
            DefaultMutableTreeNode parentNode = client.getSectorsTreeNode();
            DefaultMutableTreeNode sectorNode = new DefaultMutableTreeNode(exchange.getDescription());
            boolean alreadyInSectors = (findNode(parentNode, exchange.getDescription()) != null);
            DefaultMutableTreeNode node;
            if (alreadyInSectors) {
                Enumeration childs = parentNode.children();
                while (childs.hasMoreElements()) {
                    try {
                        node = (DefaultMutableTreeNode) childs.nextElement();
                        if (node.toString().equals(sectorNode.toString())) {
                            node.removeAllChildren();
                            node.removeFromParent();
                            break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
//            Client.getInstance().getTree().updateUI();
            SharedMethods.updateComponent(Client.getInstance().getTree());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /*private void removeViews(Exchange exchange) {
        Vector views = ViewSettingsManager.getMainBoardViews();
        for (int i = 0; i < views.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (views.elementAt(i));
            if (oSetting.getID().endsWith(Meta.FD + exchange.getSymbol())) {
                ViewSettingsManager.removeMainBoardSetting(oSetting);
                ClientTable table = (ClientTable) oSetting.getParent();
                table.setVisible(false);
                Client.getInstance().getDesktop().remove(table);

                // remove from the tree
                try {
                    DefaultMutableTreeNode parent = (DefaultMutableTreeNode) table.getTreePath().getPath()[table.getTreePath().getPath().length - 2];
                    parent.removeAllChildren();
                    parent.removeFromParent();
                    Client.getInstance().getTree().updateUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*try {
                    JInternalFrame frame = table.getTimeAndSalesWindow();
                    frame.setVisible(false);
                    client.getDesktop().remove(frame);
                    frame.dispose();
                    frame = null;

                } catch (Exception e) {
                    e.printStackTrace();
                }*
                try {
                    table.setVisible(false);
                    client.getDesktop().remove(table);
                    table.dispose();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                table = null;
            }
            oSetting = null;
        }
    }*/

    public InternalFrame createIndicesFrame() {
        oSectorTable = new Table();
        oSectorTable.setSortingEnabled();
//        oSectorTable.getPopup().showLinkMenus();
        final TWMenuItem mnuIndexGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
        mnuIndexGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.show_indexGraph(oSectorTable);
            }
        });

        //GRAPH REMOVED
        oSectorTable.getPopup().setMenuItem(mnuIndexGraph);

        oSectorTable.setPreferredSize(new Dimension(500, 100));
        oSectorModel = new IndicesModal(null);
        //oSectorModel.setExchange("TDWL");
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_SECTORS");
        oSectorModel.setViewSettings(oSetting);
        oSectorTable.setModel(oSectorModel);
        oSectorModel.setTable(oSectorTable);
        oSectorModel.applyColumnSettings();
        oSectorModel.updateGUI();


        InternalFrame frame = new InternalFrame(oSectorTable) {
            public void setVisible(boolean value) {
                super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
                if (value) {
                    if (Settings.isConnected()) {
                        TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                        oSectorModel.setExchange(comboItem.getId());
                        final String exchange = comboItem.getId();
                        new Thread() {
                            public void run() {
                                sendIndexRequests(exchange);
                            }
                        }.start();

                    }
                } else {
                    if (Settings.isConnected()) {
                        new Thread() {
                            public void run() {
                                removeIndexRequests(lastSelectedExchange);
                            }
                        }.start();

                    }
                }
            }

        };
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        oSetting.setParent(frame);
        frame.getContentPane().add(oSectorTable);
        client.oTopDesktop.add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        frame.updateUI();
//        SharedMethods.updateComponent(frame);
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

        oSectorTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    String symbol = (String) oSectorTable.getTable().getModel().getValueAt(oSectorTable.getTable().getSelectedRow(), 0);
                    client.mnu_DetailQuoteIndex(symbol, false);
                    symbol = null;
                } else if (SwingUtilities.isRightMouseButton(e)) {
                    if (((TWComboItem) exchangeCombo.getSelectedItem()).getId().equals("CUSTOM_INDEX")) {
                        mnuIndexGraph.setVisible(false);
                    } else {
                        mnuIndexGraph.setVisible(true);
                    }
                }
            }
        });

        northPanel = getExchangePanel();
        oSectorTable.getPopup().showLinkMenus();
        oSectorTable.setNorthPanel(northPanel);
        GUISettings.applyOrientation(frame);
//        frame.
//        CustomIndexStore.getSharedInstance().addCustomIndexListener(frame);
        return frame;
    }

    /* public InternalFrame createGlobalIndexFrame(){
       oGlobalIndexTable = new Table();
       oGlobalIndexTable.setSortingEnabled();
//        oSectorTable.getPopup().showLinkMenus();
       final TWMenuItem mnuIndexGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
       mnuIndexGraph.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               client.show_indexGraph(oGlobalIndexTable);
           }
       });

       //GRAPH REMOVED
       oGlobalIndexTable.getPopup().setMenuItem(mnuIndexGraph);
       oGlobalIndexTable.setPreferredSize(new Dimension(500, 100));
       oGlobalIndexModel = new GlobalIndexModel();
       //oSectorModel.setExchange("TDWL");
       ViewSetting oSetting = ViewSettingsManager.getSummaryView("GLOBAL_INDEX");
       oGlobalIndexModel.setViewSettings(oSetting);
       oGlobalIndexTable.setModel(oGlobalIndexModel);
       oGlobalIndexModel.setTable(oGlobalIndexTable);
       oGlobalIndexModel.applyColumnSettings();
       oGlobalIndexModel.updateGUI();


       InternalFrame frame = new InternalFrame(oGlobalIndexTable);
       frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
       frame.setColumnResizeable(true);
       frame.setDetachable(true);
       frame.setPrintable(true);
       oSetting.setParent(frame);
       frame.getContentPane().add(oGlobalIndexTable);
       client.oTopDesktop.add(frame);
       frame.setResizable(true);
       frame.setClosable(true);
       frame.setMaximizable(true);
       frame.setIconifiable(true);
       frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
       frame.updateUI();
       frame.applySettings();
       frame.setLayer(GUISettings.TOP_LAYER);
       frame.setVisible(false);

       oGlobalIndexTable.getTable().addMouseListener(new MouseAdapter() {
           public void mouseClicked(MouseEvent e) {
               if (e.getClickCount() > 1) {
                   String symbol = (String) oGlobalIndexTable.getTable().getModel().getValueAt(oGlobalIndexTable.getTable().getSelectedRow(), 0);
                   client.mnu_DetailQuoteIndex(symbol, false);
                   symbol = null;
               }else if(SwingUtilities.isRightMouseButton(e)){
                   if(((TWComboItem)exchangeCombo.getSelectedItem()).getId().equals("CUSTOM_INDEX")){
                       mnuIndexGraph.setVisible(false);
                   }else{
                       mnuIndexGraph.setVisible(true);
                   }
               }
           }
       });

       northPanel = getExchangePanel();
       oGlobalIndexTable.getPopup().showLinkMenus();
//        oSectorTable.setNorthPanel(northPanel);
       GUISettings.applyOrientation(frame);
        return frame;
    }*/

    public IndicesModal getIndicesModel() {
        return oSectorModel;
    }

    private JPanel getExchangePanel() {
        //JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"110", "200"}, new String[]{"22"}, 6, 6, true, true));

        JLabel label = new JLabel(Language.getString("EXCHANGE"));
        panel.add(label);

        exchanges = new ArrayList();

        exchangeCombo = new TWComboBox(new TWComboModel(exchanges));
//        popuplateExchanges();
//        exchangeCombo.addActionListener(this);
        panel.add(exchangeCombo);
        panel.doLayout();
        return panel;

    }

    private void popuplateExchanges() {
        exchangeCombo.removeActionListener(this);
        exchanges.clear();
        try {
            for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
                try {
                    Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
//                    if (exchange.isDefault()) {
                    if (!exchange.isExpired() && !exchange.isInactive()) {
                        TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                        exchanges.add(comboItem);
                    }
//                    }
                    exchange = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            Collections.sort(exchanges);
            //change start
            exchanges.add(new TWComboItem("CUSTOM_INDEX", Language.getString("CUSTOM_INDEX")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            exchangeCombo.updateUI();
            exchangeCombo.setSelectedIndex(0);
            if (northPanel != null)
                northPanel.doLayout();
        } catch (Exception e) {
        }
        exchangeCombo.addActionListener(this);
    }

    public Table getSectorTable() {
        return oSectorTable;
    }

    /*public InternalFrame createDetailQuoteAccount() {
        Table accountTable = new Table();
        accountTable.hideHeader();
        accountTable.hideCustomizer();
        accountTable.setWindowType(ViewSettingsManager.DETAIL_QUOTE);
//        accountTable.setPreferredSize(new Dimension(300, 100));
        AccountModel accountModel = new AccountModel();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ACCOUNT");
        accountModel.setViewSettings(oSetting);
        accountTable.setModel(accountModel);
        accountModel.setTable(accountTable);
        accountModel.applyColumnSettings();
        accountModel.updateGUI();

        InternalFrame accountFrame = new InternalFrame(accountTable);
        accountFrame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        oSetting.setParent(accountFrame);
        accountFrame.getContentPane().add(accountTable);
        Client.getInstance().getDesktop().add(accountFrame);
        accountFrame.setResizable(true);
        accountFrame.setClosable(true);
        accountFrame.setMaximizable(true);
        accountFrame.setIconifiable(true);
        accountFrame.setTitle(Language.getString(oSetting.getCaptionID()));
        accountFrame.updateUI();
        accountFrame.applySettings();
        accountFrame.setLayer(GUISettings.TOP_LAYER);
        accountFrame.setVisible(false);

        return accountFrame;
    }*/

    private void createTimeSalesFrame(final Exchange exchange, final AllTradesModel model) { //-- Market TNS
        if (timenSalesFrames.get(exchange.getSymbol()) != null) return; // frame already created

        final Table table = new Table();
        table.setThreadID(Constants.ThreadTypes.TIME_N_SALES);
        table.setWindowType(ViewSettingsManager.MARKET_TIMEnSALES_VIEW);
//         AllTradesModel model = new AllTradesModel();
        TWMenuItem exportMenu = new TWMenuItem(Language.getString("EXPORT_ALL_TRADES"), "exporttrades.gif");
        exportMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.exportAllTrades(TradeStore.getMarketTimeNSales(exchange.getSymbol()).getStore(), null, null);
            }
        });
        table.getPopup().setMenuItem(exportMenu);

        TWMenuItem filerMenu = new TWMenuItem(Language.getString("FILTER"), "filter.gif");
        filerMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().filterMarketTimeAndSalesSummary(exchange.getSymbol(), model);
            }
        });
        table.getPopup().setMenuItem(filerMenu);

        final ViewSetting settings = client.getViewSettingsManager().getSymbolView("BTW_TIME&SALES").getObject();
        settings.setID(exchange.getSymbol());

        //__added for tick calculation options   __________________
        ButtonGroup group = new ButtonGroup();
        TWMenu mainMenu = new TWMenu(Language.getString("TICK_CALCULATION"));
        TWRadioButtonMenuItem mnuLastTrade = new TWRadioButtonMenuItem(Language.getString("LAST_TRADE"));
        group.add(mnuLastTrade);
        mnuLastTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setTickMode(Constants.TICK_MODE_LAST_TRADE);
                settings.putProperty(ViewConstants.VC_TICK_MODE, model.getTickMode());
            }
        });
        TWRadioButtonMenuItem mnuPrecClosed = new TWRadioButtonMenuItem(Language.getString("PREVIOUS_CLOSED"));
        group.add(mnuPrecClosed);
        mnuPrecClosed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.setTickMode(Constants.TICK_MODE_PREVIOUS_CLOSED);
                settings.putProperty(ViewConstants.VC_TICK_MODE, model.getTickMode());
            }
        });
        mainMenu.add(mnuLastTrade);
        mainMenu.add(mnuPrecClosed);
        table.getPopup().setMenu(mainMenu);

        mnuLastTrade.setSelected(true);
        model.setTickMode(Constants.TICK_MODE_LAST_TRADE);
        /*  try {
            ViewSetting setting = (ViewSetting)client.getViewSettingsManager().getMarketTimeNSalesViews().get(settings.getType() + "|" + exchange.getSymbol());
            int savedTickMode = SharedMethods.intValue(setting.getProperty(ViewConstants.VC_TICK_MODE));
            model.setTickMode(savedTickMode);
            if (savedTickMode == Constants.TICK_MODE_LAST_TRADE) {
                mnuLastTrade.setSelected(true);
            } else if (savedTickMode == Constants.TICK_MODE_PREVIOUS_CLOSED) {
                mnuPrecClosed.setSelected(true);
            }
        } catch (Exception e) {
            mnuLastTrade.setSelected(true);
            model.setTickMode(Constants.TICK_MODE_LAST_TRADE);
        }*/

        //___________________________________________

        table.setPreferredSize(new Dimension(430, 100));
        model.setDataStore(TradeStore.getMarketTimeNSales(exchange.getSymbol()));

        GUISettings.setColumnSettings(settings, TWColumnSettings.getItem("MARKET_TIME_N_SALES_COLS"));
        client.getViewSettingsManager().putMarketTimenSalesView(settings.getType() + "|" + exchange.getSymbol(), settings);
        model.setViewSettings(settings);
        table.setModel(model);
        model.setTable(table);
        model.applySettings();
        JLabel message = new JLabel(Language.getString("DETAIL_TRADES_AVAILABILITY_MESSAGE"));
        table.setSouthPanel(message);
        table.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    String key = (String) ((AllTradesModel) table.getModel()).getValueAt(table.getTable().getSelectedRow(), -8);
                    int selColumn = table.getTable().convertColumnIndexToModel(table.getTable().getSelectedColumn());
                    if (selColumn == 1) {  //symbol column is selected
                        int selectedFunction = TWControl.getIntItem("TNS_CLICK_FUNCTION");
                        if (selectedFunction == Constants.SHOW_FULL_QUOTE) {
                            client.showFullQuote(key, false, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);
                        } else if (selectedFunction == Constants.SHOW_DETAIL_QUOTE) {
                            client.showDetailQuote(key);
                        } else if (selectedFunction == Constants.SHOW_SYMBOL_TNS) {
                            client.mnu_TimeNSalesSymbol(key, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                        } else if (selectedFunction == Constants.SHOW_DEPTH_BY_ORDER) {
                            client.depthByOrder(key, false, false, LinkStore.LINK_NONE);
                        } else if (selectedFunction == Constants.SHOW_DEPTH_BY_PRICE) {
                            client.depthByPrice(key, false, Constants.MAINBOARD_TYPE);
                        }
                    }
                }
            }
        });
        table.updateGUI();

        final InternalFrame frame = new InternalFrame(table);

        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.getContentPane().add(table);
        client.getDesktop().add(frame);
        settings.setParent(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setColumnResizeable(true);
        frame.setPrintable(true);
        frame.setTitle(Language.getString(settings.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        frame.updateUI();
//        SharedMethods.updateComponent(frame);
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);
        frame.setTitle(settings.getCaption() + " " + exchange);

        if (!settings.isLocationValid()) {
            frame.setLocationRelativeTo(client.getDesktop());
        }

        frame.addInternalFrameListener(new InternalFrameListener() {
            public void internalFrameActivated(InternalFrameEvent e) {

            }

            public void internalFrameClosed(InternalFrameEvent e) {

            }

            public void internalFrameClosing(InternalFrameEvent e) {

            }

            public void internalFrameDeactivated(InternalFrameEvent e) {

            }

            public void internalFrameDeiconified(InternalFrameEvent e) {

            }

            public void internalFrameIconified(InternalFrameEvent e) {

            }

            public void internalFrameOpened(InternalFrameEvent e) {
                /*
               * These few lines commented because, it sends another market time n Sales
               * request each time window opens
               *
               * */
//                System.out.println("Opened -----------------");
//                String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange;
//                client.addMarketTradeRequest(requestID, exchange.getSymbol());
//                frame.setDataRequestID(TradeStore.getSharedInstance(), exchange.getSymbol(), requestID);
            }
        });
        //frame.setDataRequestID(null, exchange.getSymbol(), null);
        GUISettings.applyOrientation(table.getPopup());
        frame.setOrientation();
        timenSalesFrames.put(exchange.getSymbol(), frame);
    }

    private void addMarketTreeNode(Exchange exchange, Market market, DefaultMutableTreeNode oParentNode,
                                   DefaultTreeModel oTreeModel,
                                   ViewSetting viewSettingsRef, boolean bSortable,
                                   boolean isCreateForNewExchange) {
        boolean isNotInMainTree = false;
        boolean sortable = true;
        try {
            ViewSetting viewSettings = (ViewSetting) viewSettingsRef.clone();
            ViewSettingsManager.addMainBoardSetting(viewSettings);
            ClientTable oTable = new ClientTable();
            oTable.setResizable(true);
            oTable.setMaximizable(true);
            oTable.setIconifiable(true);
            oTable.setClosable(true);
            oTable.setLinkWindowControl(true);
            oTable.setMarketCode(exchange.getSymbol());
            oTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
            oTable.setViewSettings(viewSettings);
            oTable.setSortable(bSortable);

            //(Data feed by Tadawul) - A requirement from TDWL
            if (exchange.getDisplayExchange().equalsIgnoreCase("TDWL")) {
                oTable.setTDWLDescription(" " + Language.getString("DATA_FEED_BY_TDWL"));
            } else {
                oTable.setTDWLDescription("");
            }

            if (market != null) {
                oTable.setTypeDescription(Language.getString("MARKET_WATCH") + " : " + market.getDescription() + " : " + exchange.getDisplayExchange());
            } else {
                oTable.setTypeDescription(Language.getString("MARKET_WATCH") + " " + exchange.getDisplayExchange());
            }

            switch (SharedMethods.intValue(viewSettings.getID())) {
                case 6: // today traded
                    try {
                        if (market != null) {
                            oTable.setSymbols(client.getTodaysTrades().getSymbols(exchange.getSymbol() + Constants.MARKET_SEPERATOR_CHARACTER + market.getMarketID())); // today's trades
                        } else {
                            oTable.setSymbols(client.getTodaysTrades().getSymbols(exchange.getSymbol())); // today's trades
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    nonMainboards.add(oTable);
                    break;
                case 7: // full market
                    try {
                        if (market != null) {
                            oTable.setSymbols(DataStore.getSharedInstance().getSymbolsObject(exchange.getSymbol(), market.getMarketID())); // all market
                        } else {
                            oTable.setSymbols(DataStore.getSharedInstance().getSymbolsObject(exchange.getSymbol(), null)); // all market
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    client.defaultBoardtable = oTable;
                    if ((market == null) || ((market != null) && (market.isDefaultMarket()))) {
                        mainboards.add(oTable);
                        mainboardList.put(exchange.getSymbol(), oTable);
                    } else {
                        nonMainboards.add(oTable);
                    }
                    break;
                default:
                    oTable = null;
            }

            if (market != null) {
                viewSettings.setID(viewSettings.getID() + Meta.FD + exchange.getSymbol() + Meta.FD + market.getSymbol());
            } else {
                viewSettings.setID(viewSettings.getID() + Meta.FD + exchange.getSymbol());
            }
            if (oTable != null) {
                if (!isNotInMainTree) {
                    client.oTopDesktop.add(oTable);
                    oTable.setWindowType(Meta.WT_MarketView);
                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(oTable);
                    oParentNode.add(node);
                    oTable.setTreePath(oTreeModel.getPathToRoot(node));
                    node = null;
                }
                oTable.createTable(true);
                oTable.setVisible(false);
                viewSettings.setVisible(false);
                oTable.getTable().addMouseListener(client.getSymbolMouseListener());
                if (isCreateForNewExchange) {
                    GUISettings.setColumnSettings(viewSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));
                }
                oTable.updateClientTable();
                if (market != null) {
                } else {
                }
                oTable.applySettings();
                oTable.applyTheme();
                oTable.getScrollPane().getViewport().addMouseListener(client.getTableMouseListener());
                SharedMethods.updateComponent(oTable);
            }
            oTable = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //  ------by Shanika Added --- For Sector Overvew ----

    private void addIndicesTreeNode(Exchange exchange, Market market, DefaultMutableTreeNode oParentNode,
                                    DefaultTreeModel oTreeModel,
                                    ViewSetting viewSettingsRef,
                                    boolean bSortable,
                                    boolean isCreateForNewExchange,
                                    String title,
                                    Symbols symbols,
                                    String id) {
        boolean isNotInMainTree = false;
        boolean sortable = true;
        try {
            ViewSetting viewSettings = (ViewSetting) viewSettingsRef.clone();
            //viewSettings.setCaptions(title);
            viewSettings.setCaptionID(id);
            Language.putString(id, title);
            viewSettings.setSymbols(symbols);
            ViewSettingsManager.addMainBoardSetting(viewSettings);
            ClientTable oTable = new ClientTable();
            oTable.setResizable(true);
            oTable.setMaximizable(true);
            oTable.setIconifiable(true);
            oTable.setClosable(true);
            oTable.setLinkWindowControl(true);
            oTable.setMarketCode(exchange.getSymbol());
            oTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
            oTable.setViewSettings(viewSettings);
            oTable.setSortable(bSortable);
            if (market != null) {
                //oTable.setTypeDescription(title);
                //viewSettings.setCaptions(title);
            } else {
                //oTable.setTypeDescription(title);
                //viewSettings.setCaptions(title);
            }

            switch (SharedMethods.intValue(viewSettings.getID())) {
                case 6: // today traded
                case 7: // full market
                case 8: // full market
                    try {
                        if (market != null) {
                            oTable.setSymbols(symbols); // today's trades
                        } else {
                            oTable.setSymbols(symbols); // today's trades
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    nonMainboards.add(oTable);
                    break;
                default:
                    oTable = null;
            }

            if (market != null) {
                viewSettings.setID(viewSettings.getID() + Meta.FD + exchange.getSymbol() + Meta.FD + market.getSymbol());
            } else {
                viewSettings.setID(viewSettings.getID() + Meta.FD + exchange.getSymbol() + Meta.FD + id);
            }
            if (oTable != null) {
                if (!isNotInMainTree) {
                    client.oTopDesktop.add(oTable);
                    oTable.setWindowType(Meta.WT_MarketView);
                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(oTable);
                    if (exchange.isEXGWatchListShouldVisible(id)) {
                        oParentNode.add(node);
                    }
                    oTable.setTreePath(oTreeModel.getPathToRoot(node));
                    node = null;
                }
                oTable.createTable(true);
                oTable.setVisible(false);
                viewSettings.setVisible(false);
                oTable.getTable().addMouseListener(client.getSymbolMouseListener());
//                if (isCreateForNewExchange) {
//                    GUISettings.setColumnSettings(viewSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));
//                }
                oTable.updateClientTable();
                oTable.setTitle(title);
                oTable.applySettings();
                oTable.applyTheme();
                oTable.getScrollPane().getViewport().addMouseListener(client.getTableMouseListener());
                SharedMethods.updateComponent(oTable);
            }
            oTable = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addSectorOverviewTreeNode(Exchange exchange, Market market, Sector sector, DefaultMutableTreeNode oParentNode,
                                           DefaultTreeModel oTreeModel, boolean bSortable, boolean isCreateForNewExchange) {
        ViewSetting oSettings = null;
        try {
            oSettings = client.getViewSettingsManager().getSymbolView("SECTOR_OVERVIEW");

            if (oSettings == null) {
                throw (new Exception("View not found"));
            }
            oSettings = oSettings.getObject();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        oSettings.setID(exchange.getSymbol());
        SectorOverviewUI sectorTable = new SectorOverviewUI(oSettings);

        sectorTable.setResizable(true);
        sectorTable.setMaximizable(true);
        sectorTable.setIconifiable(true);
        sectorTable.setClosable(true);
//        sectorTable.setPrintable(true);
        sectorTable.setFont(oSettings.getFont());
        sectorTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        sectorTable.setTypeDescription(Language.getString("SECTOR_OVERVIEW") + " : " + exchange.getDisplayExchange());
        sectorTable.setExchange(exchange.getSymbol());
        sectorTable.setExObject(exchange);

        /*if (!oSettings.isLocationValid()) {
            sectorTable.setLocationRelativeTo(Client.getInstance().oTopDesktop);
            sectorTable.setLocation(oSettings.getLocation());
        } else {
            sectorTable.setLocation(oSettings.getLocation());

        }*/

        oSettings.setParent(sectorTable);
        sectorTable.applySettings();
        sectorTable.applyTheme();
        sectorTable.setLayer(GUISettings.TOP_LAYER);
        client.oTopDesktop.add(sectorTable);
        client.oTopDesktop.setLayer(sectorTable, GUISettings.TOP_LAYER);
        client.getViewSettingsManager().getsecOverViews().put(exchange.getSymbol(), oSettings);
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(sectorTable);
        oParentNode.add(node);
        WorkspaceManager oWSM = new WorkspaceManager(client, client.getViewSettingsManager());
        oWSM.applyInitialWSPForSectorOverView(oSettings);
        SharedMethods.updateComponent(sectorTable);
        tableStore.put(exchange.getSymbol(), sectorTable);
    }

// ------------------ end -----------------------

    private void addSectorTreeNode(Exchange exchange, Market market, Sector sector, DefaultMutableTreeNode oParentNode,
                                   DefaultTreeModel oTreeModel, boolean bSortable, boolean isCreateForNewExchange) {

        ViewSetting viewSettings;
        String viewID = null;
        if (market != null) {
            viewID = sector.getId() + Meta.FD + market.getSymbol() + Meta.FD + exchange.getSymbol();
        } else
            viewID = sector.getId() + Meta.FD + exchange.getSymbol();

        try {
            viewSettings = (ViewSetting) ViewSettingsManager.getView(0, "1").clone();
        } catch (Exception e) {
            viewSettings = new ViewSetting();
            viewSettings.setColumns(BigInteger.valueOf(1));
        }
        ViewSettingsManager.addMainBoardSetting(viewSettings);
        viewSettings.setType(ViewSettingsManager.SECTOR_VIEW << 8);
        viewSettings.setID(viewID);
        viewSettings.setCaptions(sector.getDescription());
        viewSettings.setRenderingIDs(ViewSettingsManager.getRenderingIDs(ViewSettingsManager.SECTOR_VIEW));
        viewSettings.setColumnHeadings(Language.getList("TABLE_COLUMNS"));
        SymbolList symbolList = null;
        if (market != null)
            symbolList = new SymbolList(exchange.getSymbol(), market.getMarketID(), sector.getId(), "", "");
        else
            symbolList = new SymbolList(exchange.getSymbol(), sector.getId(), "", "");

        viewSettings.setSymbols(symbolList.getSymbols());
        symbolList = null;


        ClientTable oTable = new ClientTable();
        oTable.setResizable(true);
        oTable.setMaximizable(true);
        oTable.setIconifiable(true);
        oTable.setClosable(true);
        oTable.setLinkWindowControl(true);
        oTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        if (market != null)
            oTable.setTypeDescription(Language.getString("SECTOR_WATCH") + " : " + market.getDescription() + " : " + exchange.getDisplayExchange());
        else
            oTable.setTypeDescription(Language.getString("SECTOR_WATCH") + " : " + exchange.getDisplayExchange());
        //oTable.setDataStore(client.g_oDataStore);
        client.oTopDesktop.add(oTable);
        oTable.setWindowType(Meta.WT_SectorView);
        oTable.setViewSettings(viewSettings);
        oTable.setSymbols(viewSettings.getSymbols());
        oTable.setSortable(bSortable);

        DefaultMutableTreeNode node = new DefaultMutableTreeNode(oTable);
        oParentNode.add(node);

        oTable.setTreePath(oTreeModel.getPathToRoot(node));

        oTable.createTable(true);
        oTable.getTable().addMouseListener(client.getSymbolMouseListener());
        if (isCreateForNewExchange) {
            GUISettings.setColumnSettings(viewSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));
        }
        oTable.updateClientTable();
        oTable.setVisible(false);
        viewSettings.setVisible(false);
//        oTable.adjustColumns();
        oTable.getScrollPane().getViewport().addMouseListener(client.getTableMouseListener());
        nonMainboards.add(oTable); // added by dilum 22 Aug 2007. Bug: Table header font NULL since initial.wsp not applied
        oTable.applySettings();
        oTable.applyTheme();
//        oTable.updateUI();
        SharedMethods.updateComponent(oTable);
    }


    public InternalFrame getTimeNSalesFrame(String exchange) {
        return timenSalesFrames.get(exchange);
    }

    // Action Listener

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == exchangeCombo) {
                TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                oSectorModel.setExchange(comboItem.getId());
                String exchange = comboItem.getId();

                sendIndexRequests(exchange);


                exchange = null;

            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private void sendIndexRequests(String exchange) {
        try {
// send remove requests to the laset selected
            if ((lastSelectedExchange != null) && (!lastSelectedExchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                DynamicArray indexes = SymbolMaster.indexTable.get(lastSelectedExchange);
                for (int i = 0; i < indexes.size(); i++) {
                    String symbol = (String) indexes.get(i);
                    DataStore.getSharedInstance().removeSymbolRequest(lastSelectedExchange, symbol, Meta.INSTRUMENT_INDEX);
                }
                indexes = null;
            }

            // send add requests to the newly selected
            if ((!exchange.equalsIgnoreCase("CUSTOM_INDEX")) && (!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault())) {
                DynamicArray indexes = SymbolMaster.indexTable.get(exchange);
                for (int i = 0; i < indexes.size(); i++) {
                    String symbol = (String) indexes.get(i);
                    DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                }
                indexes = null;
            }

            lastSelectedExchange = exchange;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void removeIndexRequests(String exchange) {
        try {
// send remove requests to the laset selected
            if ((exchange != null) && (!exchange.equalsIgnoreCase("CUSTOM_INDEX"))) {
                DynamicArray indexes = SymbolMaster.indexTable.get(exchange);
                for (int i = 0; i < indexes.size(); i++) {
                    String symbol = (String) indexes.get(i);
                    DataStore.getSharedInstance().removeSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);
                }
                indexes = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    // Exchange listener

    public void exchangeAdded(Exchange exchange) {
        /* do not use this event to create main board tables. This method fires before the
           exchangesAdded() method, which is used in DataStore to load symbols.
           Calling createMarketTables(...) before the DataStore init will fail to create
           necessary main board windows.
        */
    }

    public void exchangeRemoved(Exchange exchange) {
        removeViews(exchange);
    }

    public void exchangeUpgraded(Exchange exchange) {
        /*if (exchange.isDefault())
            createMarketTables(exchange, true);*/
    }

    public void exchangeDowngraded(Exchange exchange) {
        removeViews(exchange);
    }

    public ClientTable getClientTable(String key) {
        return (ClientTable) mainboardList.get(key);
    }

    public void exchangesAdded(boolean offlineMode) {
        if (!offlineMode) {
            showInitializinMessage(false);
        }
        mainboards.clear();
        nonMainboards.clear();
        int count = ExchangeStore.getSharedInstance().count();
        String exchangelist = "";
        client.getDesktop().setVisible(false);
        for (int i = 0; i < count; i++) {
            try {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
                if ((exchange.isDefault()) || (exchange.isExchangehasIndicesWatchListsExists())) {
                    exchangelist = exchangelist + ",";
                    createMarketTables(exchange, true);
                    AllTradesModel model = new AllTradesModel();
                    createTimeSalesFrame(exchange, model);
                }
                exchange = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        try {
            SharedMethods.updateComponent(client.getTree());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            WorkspaceManager oWSM = new WorkspaceManager(client, client.getViewSettingsManager());
            for (int i = 0; i < mainboards.size(); i++) {
                oWSM.applyInitialWSPForMainboard(((ClientTable) mainboards.get(i)).getViewSettings());
            }
            for (int i = 0; i < nonMainboards.size(); i++) {
                oWSM.applyInitialWSPForMainboard(((ClientTable) nonMainboards.get(i)).getViewSettings());
            }
            oWSM = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (nonMainboards.size() > 0) {
            for (JInternalFrame frame : nonMainboards) {
                frame.setVisible(false);
            }
        }
        boolean isDefaultWSPAvailable = isDefaultWorskpaceAvailable();
        if (offlineMode) {
            if ((Settings.isOfflineMode()) && (!WorkspaceManager.isWorkspaceLoaded())) {
                client.openSavedDefaultWorkSpace(isDefaultWSPAvailable);
                client.getDesktop().setVisible(true);
            }
        } else {
            if (!WorkspaceManager.isWorkspaceLoaded()) {
                client.openSavedDefaultWorkSpace(isDefaultWSPAvailable);
            }
            boolean isFrameSaved = false;  // variable that holds whether any frames are saved other than full market and Today traded views
            for (int i = 0; i < Client.getInstance().getDesktop().getAllFrames().length; i++) {
                try {
                    if (Client.getInstance().getDesktop().getAllFrames()[i].isVisible()) {
                        if ((Client.getInstance().getDesktop().getAllFrames()[i] instanceof ClientTable) && ((!WorkspaceManager.isMarketViewsAvailable()))) {
                            isFrameSaved = false; // full market views should be tiled if the application is run for the very first time
                        } else {
                            isFrameSaved = true;  // full market views should not be tiled if there is atleast one other frame saved and the user is connecting to a different market
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            if (isFrameSaved || (WorkspaceManager.isMarketViewsAvailable())) {

            } else {
                for (int i = 0; i < mainboards.size(); i++) {
                    try {
                        mainboards.get(i).setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            boolean tiled = false;
            if ((mainboards.size() > 0) && (!WorkspaceManager.isMarketViewsAvailable()) && (!isFrameSaved)) { // tile only for the initial login
                WindowArranger.tileAllFrames(mainboards, client.getDesktop(), WindowArranger.HORIZONTAL);
                tiled = true;
            }
            if (tiled) {
                client.mnuSave_ActionPerformed();
            }

            System.out.println("default workspace loaded");
            loadDefaultWSP();
            closeInitializinMessage();
            client.getDesktop().setVisible(true);
            if (((ExchangeStore.getSharedInstance().defaultCount() == 0) && !ExchangeStore.isExchangeWatchListsExists()) || ((client.getMarketWatchTreeNode().getChildCount() == 0) && (ExchangeStore.getSharedInstance().defaultCount() > 0))) {
                client.removeMarketTreeNode();
            }

            if ((ExchangeStore.getSharedInstance().defaultCount() == 0) || ((client.getSectorsTreeNode().getChildCount() == 0) && (ExchangeStore.getSharedInstance().defaultCount() > 0))) {
                client.removeSectorTreeNode();
            }
        }


    }

    private void loadDefaultWSP() {
        try {
            File file = new File(Settings.SYSTEM_PATH + "/Default.wsp");
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            String fileName = in.readLine();
            if ((fileName != null) && (!fileName.trim().equals(""))) {
                client.openWorkSpace(Settings.getAbsolutepath() + fileName);
                client.mnuMakeDefault_ActionPerformed();
                if (!Settings.isDefaultWSPEnable()) {
                    deleteRegionalWorkspace();
                }
                in.close();
                DataOutput out = new DataOutputStream(new FileOutputStream(file));
                String str = "     \n\n\n";
                out.write(str.getBytes(), 0, str.length());

                file.delete();
            } else {
                in.close();
            }
            try {
                file.delete();
            } catch (Exception e) {
                file.deleteOnExit();
            }
            System.out.println("Deleting def wsp");
        } catch (Exception e) {
//            e.printStackTrace();
        }

//        DynamicArray workspaces=  new DynamicArray();
//        try {
//            File file = new File(Settings.getAbsolutepath()+ "system/Default.wsp");
//          //  DataInputStream in = new DataInputStream(new FileInputStream(file));
//            BufferedReader in =  new BufferedReader(new FileReader(file));
//            String fileName="";
//            while((fileName= in.readLine()) != null){
//
//                if ((fileName != null) && (!fileName.trim().equals(""))){
//                    workspaces.add(fileName);
//                 //   client.openWorkSpace(fileName);
//                //    client.mnuMakeDefault_ActionPerformed();
//                }
//            }
//            in.close();
//            file.delete();
//            processWSP(workspaces);
//
//
//        } catch (Exception e) {
////            e.printStackTrace();
//        }

    }

    private boolean isDefaultWorskpaceAvailable() {
        boolean isAvailable = false;
        try {
            File file = new File(Settings.SYSTEM_PATH + "/Default.wsp");
            if (file.exists()) {
                isAvailable = true;
            } else {
                isAvailable = false;
            }
        } catch (Exception e) {
            isAvailable = false;
        }

        return isAvailable;
    }

    private boolean processWSP(DynamicArray wsp) {

        if (wsp != null && !wsp.isEmpty()) {

            Dimension scrSz = Toolkit.getDefaultToolkit().getScreenSize();
            String defaultpth = "";

            for (int i = 0; i < wsp.size(); i++) {

                String path = (String) wsp.get(i);

                String resolution = path.split(";")[1];
                String wspPath = path.split(";")[0];
                if (i == 0) {
                    defaultpth = wspPath;
                }
                int width = Integer.parseInt(resolution.split("x")[0]);
                int height = Integer.parseInt(resolution.split("x")[1]);
                Dimension dim = new Dimension(width, height);

                if (scrSz.equals(dim)) {
                    client.openWorkSpace(wspPath);
                    client.mnuMakeDefault_ActionPerformed();
                    deleteRegionalWorkspace();
                    return true;

                }
            }

            client.openWorkSpace(defaultpth);
            client.mnuMakeDefault_ActionPerformed();
            deleteRegionalWorkspace();

            return true;
        }
        return false;

    }

    private void deleteRegionalWorkspace() {
        //this method is to remove regional wsp after loading the app
        try {
            File file = new File(Settings.getAbsolutepath() + "workspaces/regional.wsp");
            file.delete();
        } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            File file = new File(Settings.getAbsolutepath() + "workspaces/regional.wspx");
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void sleepThread() {
        try {
            Thread.sleep(50);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangesLoaded() {
        popuplateExchanges();
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    // Connection listener

    public void twConnected() {

    }

    public void twDisconnected() {

    }

    public void customIndexAdded(String index) {

    }

    public void customIndexRemoved(String index) {

    }

    public void customIndexEditted(String index) {

    }
}
