package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthOrderModel;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.MarketAskDepthRenderer;
import com.isi.csvr.table.MarketBidDepthRenderer;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 27, 2008
 * Time: 10:55:09 AM
 * To change this template use File | Settings | File Templates.
 */
public class DepthByOrderNormalFrame extends InternalFrame implements LinkedWindowListener {
    boolean loadedFromWorkspace = false;
    byte stockDecimalPlaces = 2;
    byte parentWindowType;
    private Table bidTable;
    private Table askTable;
    private ViewSetting oBidViewSetting = null;
    private ViewSetting oAskViewSetting = null;
    private DepthOrderModel askModel;
    private DepthOrderModel bidModel;
    private String selectedKey;
    private ViewSettingsManager g_oViewSettings;
    private long timeStamp;
    private boolean isLinked;

    public DepthByOrderNormalFrame(String sThreadID, WindowDaemon oDaemon, Table oTable, Table[] oTable2, boolean loadedFromWorkspace, String selectedKey, boolean isLinked, String linkgroup) {
        super(sThreadID, oDaemon, oTable, oTable2);
        this.loadedFromWorkspace = loadedFromWorkspace;
        this.bidTable = oTable;
        this.askTable = oTable2[0];
        //this.parentWindowType = parentWindowType;
        this.selectedKey = selectedKey;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);
        initUI();
    }

    public void initUI() {
        bidTable.setAutoResize(true);
        bidTable.getPopup().showLinkMenus();
        bidTable.getPopup().showSetDefaultMenu();
        bidTable.setWindowType(ViewSettingsManager.DEPTH_BID_VIEW);

        bidModel = new DepthOrderModel(selectedKey, DepthObject.BID);
        bidModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));

        if (oBidViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_BID_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oBidViewSetting = oSetting;
            }
        }
        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_BID_VIEW +
                    "|" + selectedKey);
        }
        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getSymbolView("DEPTH_BID_VIEW");
            oBidViewSetting = oBidViewSetting.getObject();
            // oBidViewSetting.setID(selectedKey);
            GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
            //todo added by Dilum
            SharedMethods.applyCustomViewSetting(oBidViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oBidViewSetting.getType(), oBidViewSetting);
            timeStamp = System.currentTimeMillis();
            oBidViewSetting.setID(timeStamp + "");
            oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
            g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_BID_VIEW +
                    "|" + selectedKey, oBidViewSetting);
        } else {
            loadedFromWorkspace = true;
        }
        bidModel.setViewSettings(oBidViewSetting);
        bidTable.setModel(bidModel);
        bidModel.setTable(bidTable, new MarketBidDepthRenderer(), true);  // Add the renderer...
        //bidModel.applyColumnSettings();

        askTable.setAutoResize(true);
        askTable.getPopup().showLinkMenus();
        askTable.getPopup().showSetDefaultMenu();
        askTable.setWindowType(ViewSettingsManager.DEPTH_ASK_VIEW);

        askModel = new DepthOrderModel(selectedKey, DepthObject.ASK);
        askModel.setDecimalCount(SharedMethods.getDecimalPlaces(selectedKey));

        if (oAskViewSetting == null && loadedFromWorkspace && isLinked) {
            ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_ASK_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
            if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                oAskViewSetting = oSetting;
            }
        }
        if (oAskViewSetting == null) {
            oAskViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_ASK_VIEW +
                    "|" + selectedKey);
        }
        if (oAskViewSetting == null) {
            oAskViewSetting = g_oViewSettings.getSymbolView("DEPTH_ASK_VIEW");
            oAskViewSetting = oAskViewSetting.getObject();
            //oAskViewSetting.setID(selectedKey);
            GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_ORDER_COLS"));
            //todo added by Dilum
            SharedMethods.applyCustomViewSetting(oAskViewSetting);
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oAskViewSetting.getType(), oAskViewSetting);
            oAskViewSetting.setID(timeStamp + "");
            oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
            oAskViewSetting.putProperty(ViewConstants.VC_LINKED, false);
            oAskViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
            g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_ASK_VIEW +
                    "|" + selectedKey, oAskViewSetting);
        }
        askModel.setViewSettings(oAskViewSetting);
        askTable.setModel(askModel);
        askModel.setTable(askTable, new MarketAskDepthRenderer(), false);

        oAskViewSetting.setParent(this);
        oAskViewSetting.setTableNumber(2);
        oBidViewSetting.setParent(this);
        oBidViewSetting.setTableNumber(1);

        this.setShowServicesMenu(true);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(oBidViewSetting.getSize());
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
//            frame.setFocusable(true);
        Client.getInstance().oTopDesktop.add(this);
        this.setTitle(Language.getString("MARKET_DEPTH_BY_ORDER") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);

        JSplitPane splitPane;
        if (Language.isLTR()) {
            this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, bidTable, askTable) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            this.getContentPane().add(BorderLayout.CENTER, splitPane);
            splitPane.updateUI();
        } else {
            this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, askTable, bidTable) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            this.getContentPane().add(BorderLayout.CENTER, splitPane);
            splitPane.updateUI();
        }
        GUISettings.applyOrientation(this);
        bidTable.updateGUI();
        askTable.updateGUI();
        this.applySettings();
        this.setSplitPane(splitPane);
        this.show();
        splitPane.updateUI();
        bidModel.adjustRows();
        askModel.adjustRows();
        setDecimalPlaces();
        this.setLayer(GUISettings.TOP_LAYER);
        if (loadedFromWorkspace && isLinked) {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_ASK_VIEW + "|" +
                    LinkStore.linked + "_" + getLinkedGroupID(), this);
            LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
        } else {
            g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_ASK_VIEW + "|" +
                    selectedKey, this);
        }
        String requestID = Meta.REGULAR_DEPTH + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.REGULAR_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        if (!loadedFromWorkspace) {
            if (!oBidViewSetting.isLocationValid()) {
                this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                oBidViewSetting.setLocation(this.getLocation());
            } else {
                this.setLocation(oBidViewSetting.getLocation());
            }
        }

        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }

    }

    public void setDecimalPlaces() {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        bidTable.getModel().setDecimalCount(stockDecimalPlaces);
        bidTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
        askTable.getModel().setDecimalCount(stockDecimalPlaces);
        askTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
    }

    public void symbolChanged(String sKey) {
        selectedKey = sKey;
        String oldKey = oBidViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldKey);

        }
        String oldReq = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldReq);
        String requestID = Meta.REGULAR_DEPTH + "|" + selectedKey;
        Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.REGULAR_DEPTH);
        this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }
        this.setTitle(Language.getString("MARKET_DEPTH_BY_ORDER") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        askModel.setSymbol(selectedKey);
        bidModel.setSymbol(selectedKey);
        askTable.getTable().updateUI();
        bidTable.getTable().updateUI();
        this.updateUI();
        oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
        oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
    }
}
