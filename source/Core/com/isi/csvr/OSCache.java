package com.isi.csvr;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Jan 16, 2008
 * Time: 11:39:57 AM
 */
public class OSCache {

    public static void doCache() {
        try {
            JDialog frame = new JDialog((JFrame) null, "Initializing Mubasher Pro");
            frame.setSize(100, 100);
            frame.setLocation(-200, -200);
            frame.show();
            JMenu jMenu = new JMenu();
            JTable jTable = new JTable();
            Thread.sleep(5000);
            frame.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
