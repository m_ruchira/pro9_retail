package com.isi.csvr.middlewarehandler;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.shared.nanoxml.XMLElement;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 9, 2010
 * Time: 9:57:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class MWMenuPropagator extends Thread {
    //    private String fileUrl = "http://196.205.23.3/simulation/home.p?method=login&menu=true&lang=en";
    private String fileUrl = "";
    private String brokerId = "";
    private String brokerUrl = "";
    private ArrayList<MWMenuItem> trMenuItems;

    public MWMenuPropagator(String brokerId, String brokerUrl) {
        super("MWMenuDowloader");
        if (!brokerId.equals("")) {
            this.brokerId = brokerId;
        }
        this.brokerUrl = brokerUrl;
        trMenuItems = new ArrayList<MWMenuItem>();
        start();
    }

    public void run() {
        try {
            File menuFile = new File(Settings.SYSTEM_PATH + "/" + "menu_" + Language.getSelectedLanguage() + "_" + brokerId + ".xml");
            if (!menuFile.exists()) {
                createUrlForDownload();
                try {
                    startFileDownload();
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            try {
                loadMenuItems();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            createMWTradeMenus();
//            MWMenuCreationNotifier.getInstance().fireMWTradeMenuCreated();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void createUrlForDownload() {
        fileUrl = brokerUrl + MWConstants.REQUEST_MENU_TAG + MWConstants.LANGUAGE_TAG + Language.getSelectedLanguage();
    }

    private void startFileDownload() throws IOException {
        BufferedInputStream inStream = new BufferedInputStream(new URL(fileUrl).openStream());
        FileOutputStream fos = new FileOutputStream(Settings.SYSTEM_PATH + "/" + "menu_" + Language.getSelectedLanguage() + "_" + brokerId + ".xml");
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);
        byte data[] = new byte[1024];
        while (inStream.read(data, 0, 1024) >= 0) {
            bout.write(data);
        }
        bout.close();
        inStream.close();
    }

    private void loadMenuItems() {
        try {
            FileInputStream in = new FileInputStream(Settings.SYSTEM_PATH + "/menu_" + Language.getSelectedLanguage() + "_" + brokerId + ".xml");
            StringBuilder buffer = new StringBuilder();
            byte[] data = new byte[1000];
            int len = 0;
            while (len >= 0) {
                try {
                    len = in.read(data);
                } catch (IOException e) {
                }
                if (len > 0) {
                    buffer.append(new String(data, 0, len));
                }
            }
            XMLElement root = new XMLElement();
            root.parseString(buffer.toString());
            ArrayList<XMLElement> rootElements = root.getChildren();
            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem = rootElements.get(i);
                if (rootItem.getName().equals("TRADEMENUITEM")) {
                    MWMenuItem menuItem = new MWMenuItem();
                    menuItem.setItemId(rootItem.getAttribute("ID"));
                    if (!rootItem.getAttribute("URL").equalsIgnoreCase("")) {
                        menuItem.setUrl(rootItem.getAttribute("URL"));
                    } else {
                        menuItem.setUrl(brokerUrl);
                    }
                    menuItem.setItemName(rootItem.getAttribute("LABEL"));
                    menuItem.setIconName(rootItem.getAttribute("ICON"));
                    trMenuItems.add(menuItem);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getURLFromItemName(String name) {
        try {
            for (MWMenuItem hItem : trMenuItems) {
                if (hItem.getItemId().equalsIgnoreCase(name)) {
                    return hItem.getUrl();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }

    private void createMWTradeMenus() {
        if (trMenuItems == null) {
            return;
        }
        if (trMenuItems.size() == 0) {
            loadDefaultMenu();
        } else {
            Client.getInstance().getMenus().getMnuMWTrade().removeAll();
            for (final MWMenuItem item : trMenuItems) {
                TWMenuItem menuItem;
                if (item.getIconName() != null) {
                    menuItem = new TWMenuItem(UnicodeUtils.getNativeString(item.getItemName()), item.getIconName());
                    menuItem.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/" + item.getIconName()));
                } else if (item.getItemId().equalsIgnoreCase(MWConstants.TRADE_SELL)) {
                    menuItem = new TWMenuItem(UnicodeUtils.getNativeString(item.getItemName()), "sell.gif");
                    menuItem.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/sell.gif"));
                } else if (item.getItemId().equalsIgnoreCase(MWConstants.TRADE_BUY)) {
                    menuItem = new TWMenuItem(UnicodeUtils.getNativeString(item.getItemName()), "buy.gif");
                    menuItem.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/buy.gif"));
                } else {
                    menuItem = new TWMenuItem(UnicodeUtils.getNativeString(item.getItemName()));
                }
                menuItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (item.getItemId().equalsIgnoreCase(MWConstants.TRADE_BUY)) {
                            MWTradingHandler.getSharedInstance().showWindow(MWConstants.BUY, Client.getInstance().getSelectedSymbol(), item.getUrl(), brokerId);
                        } else if (item.getItemId().equalsIgnoreCase(MWConstants.TRADE_SELL)) {
                            MWTradingHandler.getSharedInstance().showWindow(MWConstants.SELL, Client.getInstance().getSelectedSymbol(), item.getUrl(), brokerId);
                        } else {
                            MWTradingHandler.getSharedInstance().showWindow(MWConstants.BUY, "", item.getUrl(), "");
                        }
                    }
                });
                Client.getInstance().getMenus().getMnuMWTrade().add(menuItem);
            }
        }
//        SharedMethods.updateComponent(Client.getInstance().getMnuMWTrade());
        createPopupMenuTrade();
        GUISettings.applyOrientation(Client.getInstance().getMnuMWTrade());
        GUISettings.applyOrientation(Client.getInstance().getMenus().getMnuMWTrade());
        MWMenuCreationNotifier.getInstance().fireMWTradeMenuCreated();
    }

    private void loadDefaultMenu() {
        TWMenuItem mnuMWBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuMWBuy.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/buy.gif"));
        mnuMWBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MWTradingHandler.getSharedInstance().showWindow(MWConstants.BUY, "", brokerUrl, brokerId);
            }
        });
        TWMenuItem mnuMWSell = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuMWSell.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/sell.gif"));
        mnuMWSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MWTradingHandler.getSharedInstance().showWindow(MWConstants.SELL, "", brokerUrl, brokerId);
            }
        });
        Client.getInstance().getMenus().getMnuMWTrade().removeAll();
        Client.getInstance().getMenus().getMnuMWTrade().add(mnuMWBuy);
        Client.getInstance().getMenus().getMnuMWTrade().add(mnuMWSell);
//        SharedMethods.updateComponent(Client.getInstance().getMenus().getMnuMWTrade());
    }

    private void createPopupMenuTrade() {
        Client.getInstance().getMnuMWTrade().removeAll();

        TWMenuItem mnuMWBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuMWBuy.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/buy.gif"));
        mnuMWBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MWTradingHandler.getSharedInstance().showWindow(MWConstants.BUY, Client.getInstance().getSelectedSymbol(), brokerUrl, brokerId);
            }
        });
        TWMenuItem mnuMWSell = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuMWSell.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/sell.gif"));
        mnuMWSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MWTradingHandler.getSharedInstance().showWindow(MWConstants.SELL, Client.getInstance().getSelectedSymbol(), brokerUrl, brokerId);
            }
        });

        Client.getInstance().getMnuMWTrade().add(mnuMWBuy);
        Client.getInstance().getMnuMWTrade().add(mnuMWSell);
//        SharedMethods.updateComponent(Client.getInstance().getMnuMWTrade());
    }
}
