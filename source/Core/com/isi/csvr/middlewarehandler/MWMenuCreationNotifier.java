package com.isi.csvr.middlewarehandler;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 8, 2010
 * Time: 12:03:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class MWMenuCreationNotifier {
    private static MWMenuCreationNotifier self;
    private Vector listeners;

    public MWMenuCreationNotifier() {
        listeners = new Vector(10, 5);
    }

    public synchronized static MWMenuCreationNotifier getInstance() {
        if (self == null) {
            self = new MWMenuCreationNotifier();
        }
        return self;
    }

    public synchronized void addMenuCreationListener(Object listener) {
        listeners.addElement(listener);
    }

    public synchronized void removeMenuCreationListener(Object listener) {
        listeners.remove(listener);
    }

    public synchronized void fireMWTradeMenuCreated() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                MWMenuCreationListener listener = (MWMenuCreationListener) listeners.get(i);
                listener.mwTradeMenuCreated();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public synchronized void fireMWDisconnected() {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                MWMenuCreationListener listener = (MWMenuCreationListener) listeners.get(i);
                listener.mwServerDisconnected();
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}
