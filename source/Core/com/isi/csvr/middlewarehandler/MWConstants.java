package com.isi.csvr.middlewarehandler;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 9, 2010
 * Time: 3:39:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class MWConstants {
    public static final short BUY = 1;
    public static final short SELL = 2;
    public static final String TRADE_BUY = "TRADE_BUY";
    public static final String TRADE_SELL = "TRADE_SELL";
    public static final String REQUEST_MENU_TAG = "&menu=true";
    public static final String LANGUAGE_TAG = "&lang=";
}
