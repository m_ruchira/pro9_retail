package com.isi.csvr.middlewarehandler;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 4, 2010
 * Time: 11:43:50 AM
 * To change this template use File | Settings | File Templates.
 */
public class MWBorkerStore {
    private static MWBorkerStore self;
    private ArrayList<MWBroker> brokerList;

    public MWBorkerStore() {
        brokerList = new ArrayList<MWBroker>();
    }

    public static MWBorkerStore getSharedInstance() {
        if (self == null) {
            self = new MWBorkerStore();
        }
        return self;
    }

    private void addBroker(String id, String urlList) {
        MWBroker broker = new MWBroker(id, urlList);
        brokerList.add(broker);
    }
}
