package com.isi.csvr.middlewarehandler;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 4, 2010
 * Time: 11:40:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class MWBroker {
    private String id;
    private ArrayList<String> urls;

    public MWBroker(String id, String brokerUrls) {
        urls = new ArrayList<String>();
        this.id = id;
        String[] urlslist = brokerUrls.split(",");
        for (int i = 0; i < urlslist.length; i++) {
            urls.add(urlslist[i]);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }
}
