package com.isi.csvr.ticker.uicomponents;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWCustomCheckBox;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.ticker.advanced.UpperPanelSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Feb 13, 2009
 * Time: 12:11:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class TickerFilterableComponent extends JPanel implements MouseListener {

    private JCheckBox box;
    private JLabel label;
    private JLabel btnDownArrow;

    private JPanel upPanel;
    private DataPanel dataPanel;
    private boolean isPanelVisible = false;
    private String selectedObject;

    private Hashtable<String, InternalFrame> frameStore = new Hashtable<String, InternalFrame>();
    private Hashtable<String, TickerFilterableComponent> objectStore = new Hashtable<String, TickerFilterableComponent>();

    private String selectedframe;

    public TickerFilterableComponent(TWCustomCheckBox box, JLabel label) {
        this.label = label;
    }

    public TickerFilterableComponent() {
        createUI();
    }

    public TickerFilterableComponent(String u_ticker, InternalFrame label) {
        frameStore.put(u_ticker, label);
        this.selectedframe = u_ticker;
        createUI();
    }

    public TickerFilterableComponent(String s, TickerFilterableComponent exchangeComponent) {
        objectStore.put(s, exchangeComponent);
        this.selectedObject = s;
        createUI();
    }

    private void createUI() {
        box = new JCheckBox("");
        box.setBackground(UpperPanelSettings.FILTER_PANEL_COLOR);
        box.setOpaque(false);
        label = new JLabel();
        btnDownArrow = new JLabel();
        btnDownArrow.setPreferredSize(new Dimension(20, 20));
        btnDownArrow.setToolTipText(Language.getString("SEARCH"));
        btnDownArrow.setEnabled(true);
        btnDownArrow.setVisible(true);
        btnDownArrow.addMouseListener(this);
        btnDownArrow.setAlignmentX(Component.RIGHT_ALIGNMENT);
        upPanel = new JPanel();
        dataPanel = new DataPanel();
        dataPanel.setBackground(UpperPanelSettings.FILTER_PANEL_COLOR);
        box.addMouseListener(this);
        label.addMouseListener(this);
        upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.X_AXIS));
        upPanel.add(Box.createHorizontalStrut(5));
        upPanel.add(box);
        upPanel.add(Box.createHorizontalStrut(5));
        upPanel.add(label);
        upPanel.add(Box.createHorizontalGlue());
        upPanel.add(btnDownArrow);
        upPanel.add(Box.createHorizontalStrut(5));

        if (isPanelVisible) {
            btnDownArrow.setIcon(getIconFromString("collapse_chart"));
        } else {
            btnDownArrow.setIcon(getIconFromString("expand_chart"));
        }
        upPanel.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        upPanel.setBorder(BorderFactory.createEtchedBorder());
        BoxLayout box1 = new BoxLayout(dataPanel, BoxLayout.PAGE_AXIS);
        dataPanel.setLayout(box1);
        setLayout(new BorderLayout());

        add(upPanel, BorderLayout.NORTH);
        add(dataPanel, BorderLayout.SOUTH);
    }

    public void setTextBox(String t) {
        box.setText(t);
    }

    public void setTextLable(String tt) {
        label.setText(tt);
    }

    public void addIemsToPanel(Component jlable) {
        dataPanel.add(jlable);
    }

    public JCheckBox getBox() {
        return box;
    }

    public void setBox(JCheckBox box) {
        this.box = box;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    public JPanel getUpPanel() {
        return upPanel;
    }

    public void setUpPanel(JPanel upPanel) {
        this.upPanel = upPanel;
    }

    public DataPanel getDataPanel() {
        return dataPanel;
    }

    public void setDataPanel(DataPanel dataPanel) {
        this.dataPanel = dataPanel;
    }

    public boolean isPanelVisible() {
        return isPanelVisible;
    }

    public void setPanelVisible(boolean panelVisible) {
        this.isPanelVisible = panelVisible;
        if (isPanelVisible) {
            btnDownArrow.setIcon(getIconFromString("collapse_chart"));
        } else {
            btnDownArrow.setIcon(getIconFromString("expand_chart"));
        }
    }

    public boolean isCheckBoxSeleced() {
        return box.isSelected();
    }

    public void setCheckBoxSelected(boolean selected) {
        box.setSelected(selected);

    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(label)) {
            isPanelVisible ^= true;
//            isPanelVisible = !isPanelVisible;
            if (isPanelVisible) {
                btnDownArrow.setIcon(getIconFromString("collapse_chart"));
            } else {
                btnDownArrow.setIcon(getIconFromString("expand_chart"));
            }
            updateUI();
            doLayout();
            repaint();
            InternalFrame frame = frameStore.get(selectedframe);
            ((TickerFilterableComponentListener) frame).resizeFrame();

        } else if (e.getSource().equals(box)) {

            isPanelVisible = true;
            updateUI();
            doLayout();
            repaint();
            InternalFrame frame = frameStore.get(selectedframe);
            ((TickerFilterableComponentListener) frame).changeState(this);
            ((TickerFilterableComponentListener) frame).resizeFrame();

        } else if (e.getSource().equals(btnDownArrow)) {
            isPanelVisible ^= true;
//            isPanelVisible = !isPanelVisible;

            if (isPanelVisible) {
                btnDownArrow.setIcon(getIconFromString("collapse_chart"));
            } else {
                btnDownArrow.setIcon(getIconFromString("expand_chart"));
            }
            updateUI();
            doLayout();
            repaint();
            InternalFrame frame = frameStore.get(selectedframe);
            ((TickerFilterableComponentListener) frame).resizeFrame();

        }
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Dimension getPreferredSize() {
        Dimension dim;
        int fixedWidth = 236;
        if (isPanelVisible) {
            dim = new Dimension(dataPanel.getPreferredSize().width, (dataPanel.getPreferredSize().height + upPanel.getPreferredSize().height)); //dataTable.getPreferredSize().height+ dataTable.getTable().getTableHeader().getHeight()

        } else {
            dim = new Dimension(dataPanel.getPreferredSize().width, upPanel.getPreferredSize().height);
        }

        if (dataPanel.getPreferredSize().width < fixedWidth) {

            if (isPanelVisible) {
                dim = new Dimension(fixedWidth, (dataPanel.getPreferredSize().height + upPanel.getPreferredSize().height)); //dataTable.getPreferredSize().height+ dataTable.getTable().getTableHeader().getHeight()
            } else {
                dim = new Dimension(fixedWidth, upPanel.getPreferredSize().height);
            }
        }
        return dim;
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    class DataPanel extends JPanel {
        public Dimension getPreferredSize() {
            setBackground(UpperPanelSettings.FILTER_PANEL_COLOR);
            int width = 0;
            int hight = 0;
            Component cmp[] = getComponents();
            for (int i = 0; i < cmp.length; i++) {
                hight = hight + (cmp[i]).getPreferredSize().height;
                if (cmp[i] instanceof JLabel) {
                    hight = hight + ((JLabel) cmp[i]).getHeight() + 6;
                }
                if (width < cmp[i].getPreferredSize().width) {
                    width = cmp[i].getPreferredSize().width + 2;
                }
            }
            return new Dimension(width, hight + 3);
        }

    }
}
