/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version StockNet Ticker - Client Version 1.0 Copyright (c) 2001 ISI
 */

package com.isi.csvr.ticker.custom;

import com.isi.csvr.*;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.*;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.ticker.GlobalSettings;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * <P>The main component that create and manage the interactions in the Ticker.
 * Its responsible for handling user actions and loading the stored Stock Lists
 * at the initialisation process.
 */
public class Ticker extends JPanel implements
        MouseListener, InternalFrameListener, Themeable,
        ActionListener, ExchangeListener, WatchlistListener, PopupMenuListener, ApplicationListener {

    //##########################################################################
    // Variable Decleration for the Ticker Object
    //##########################################################################
    public final static int MODE_ALL = 0;
    public final static int MODE_SYMBOL = 2;
    public static final String TITLE_SHOW = "TitleShow";
    public static ViewSetting settings;
    public static int CURRENT_TICKER_SPEED = 1;
    //    private Thread stockThread;
    public static boolean IS_FRAME_DATA_COME = false;
    public static int SLOW_DELAY = 600;
    public static boolean IS_TITLE_SHOW;
    public static Ticker selfRef;
    public static boolean tickerSetToTop = true;
    public static boolean tickerFixedToTopPanel = true;
    public static String CUS = "cus";
    public static String ADV = "adv";
    public static boolean IS_DEFAULT = false;
    public TWMenuItem floatMenu;
    public TWMenu fixMenu;
    public CardLayout cardLayout;
    String previousExchange = "";
    private TradeTicker tt;             // TradeTicker Object
    private JPopupMenu popupMenu;
    private TWMenuItem showHideTitle;
    private TWMenuItem mnuFont;
    private TWMenuItem showHideSymbol;
    private TWMenuItem showHideChange;
    private TWMenuItem mnuCloseTicker;
    private TWRadioButtonMenuItem mnuSpeedSlow;
    private TWRadioButtonMenuItem mnuSpeedMedium;
    private TWRadioButtonMenuItem mnuSpeedFast;
    private TWMenu tickerFilter;
    private TWMenuItem mnuSummaryTicker;
    private TWMenuItem mnuAdvanceTicker;
    private TWMenu mnuSummaryTickerSpeed;
    private ProcessListener parentFrame;
    private TWMenuItem mnuTickerFixedTop;
    private TWMenuItem mnuTickerFixedBottom;
    //##########################################################################
    // Method Definitions for the Ticker Object
    //##########################################################################

    /**
     * Constructs a new instance.
     */
    public Ticker(ViewSetting settings) {
        try {
            this.settings = settings;
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Theme.registerComponent(this);
        popupMenu.addPopupMenuListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        WatchListManager.getInstance().addWatchlistListener(this);
        Application.getInstance().addApplicationListener(this);

        selfRef = this;

    }

    public Ticker() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public static Ticker getInstance() {
        if (selfRef == null) {
            selfRef = new Ticker(new ViewSetting());
        }
        return selfRef;
    }

    public Dimension getPreferredSize() {
        Dimension dim = new Dimension(0, 0);
        if (PanelSetter.upperTicker) {
            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
        }
        if (PanelSetter.middleTicker) {
            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
        }
        if (PanelSetter.lowerTicker) {
            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
        }
        return dim;
    }

    public void setterTickerSize(int tSize) {
        GlobalSettings.TICKER_HEIGHT = tSize;
    }

    public TWMenuItem getTitleBarMenu() {
        return showHideTitle;
    }

    /**
     * <P>Initialize the Ticker object. Creates the sub objects that constitutes
     * the StockeNet Ticker and allowing them to run as seperate processes.
     */
    public void init() throws Exception {
        popupMenu = new JPopupMenu();
        showHideTitle = new TWMenuItem();
        mnuFont = new TWMenuItem();
        showHideSymbol = new TWMenuItem();
        showHideChange = new TWMenuItem();
//        allwaysOnTop = new TWMenuItem();
        if (Language.isLTR()) {
            GlobalSettings.FONT_BIG += 2;
            GlobalSettings.FONT_MEDIUM += 2;
            GlobalSettings.FONT_SMALL += 2;
            GlobalSettings.ENGLISH_VERSION = true;
        } else {
            GlobalSettings.FONT_BIG += 2;
            GlobalSettings.FONT_MEDIUM += 2;
            GlobalSettings.FONT_SMALL += 2;
            GlobalSettings.ENGLISH_VERSION = false;
        }


        showHideTitle.addActionListener(this);
        showHideTitle.setIconFile("showtitle.gif");
        //showHideTitle.setActionCommand("TITLE");
        if (!settings.isHeaderVisible()) {
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
        } else {
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
        }

        mnuFont.addActionListener(this);
        mnuFont.setActionCommand("FONT");
        mnuFont.setIconFile("font.gif");
        mnuFont.setText(Language.getString("FONT"));


        showHideSymbol.addActionListener(this);
        showHideSymbol.setIconFile("showsymbol.gif");
        showHideSymbol.setActionCommand("SHOW_SYMBOL");
        showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));


        showHideChange.addActionListener(this);
        showHideChange.setIconFile("showchange.gif");
        showHideChange.setActionCommand("SHOW_PER_CHANGE");
        showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));

        //allwaysOnTop.addActionListener(this);
        //showHideChange.setIconFile("showchange.gif");
        //allwaysOnTop.setActionCommand("allwaysOnTop");
        //allwaysOnTop.setText("Allways OnTop");
//        if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(),Meta.IT_TradeTicker)) {//Settings.isTradeTickerMode()
        mnuSummaryTicker = new TWMenuItem(Language.getString("SHOW_SUMMARY_TICKER"), "showsmryticker.gif");
        mnuSummaryTicker.addActionListener(this);
        if (Settings.isShowSummaryTicker()) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");

        } else {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY");
        }
        mnuSummaryTicker.setVisible(false);
        popupMenu.add(mnuSummaryTicker);

        mnuAdvanceTicker = new TWMenuItem(Language.getString("ADVANCED_TICKER"), "ticker_ad.gif");
        mnuAdvanceTicker.addActionListener(this);
        mnuAdvanceTicker.setActionCommand("ADVANCED_TICKER");
        popupMenu.add(mnuAdvanceTicker);
//        }else{
//             mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
//             mnuSummaryTicker.setActionCommand("SUMMARY");
        //}
//        mnuSummaryTicker = new TWMenuItem(Language.getString("SHOW_SUMMARY_TICKER"), "showsmryticker.gif");
//        if(Settings.isShowSummaryTicker()){
//           mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
//           mnuSummaryTicker.setActionCommand("SUMMARY");
//
//        }
//        mnuSummaryTicker.addActionListener(this);
//        mnuSummaryTicker.setActionCommand("SUMMARY");
//        popupMenu.add(mnuSummaryTicker);

        // Ticker Speed menu
        mnuSummaryTickerSpeed = new TWMenu(Language.getString("TICKER_SPEED"), "tickerspeed.gif");
        ButtonGroup speedGroup = new ButtonGroup();
        mnuSpeedSlow = new TWRadioButtonMenuItem(Language.getString("SPEED_SLOW"));
        mnuSpeedSlow.addActionListener(this);
        mnuSpeedSlow.setActionCommand("SPEED_SLOW");
        speedGroup.add(mnuSpeedSlow);
        mnuSummaryTickerSpeed.add(mnuSpeedSlow);
        mnuSpeedMedium = new TWRadioButtonMenuItem(Language.getString("SPEED_MEDIUM"));
        mnuSpeedMedium.addActionListener(this);
        mnuSpeedMedium.setActionCommand("SPEED_MEDIUM");
        speedGroup.add(mnuSpeedMedium);
        mnuSummaryTickerSpeed.add(mnuSpeedMedium);
        mnuSpeedFast = new TWRadioButtonMenuItem(Language.getString("SPEED_FAST"));
        mnuSpeedFast.addActionListener(this);
        mnuSpeedFast.setActionCommand("SPEED_FAST");
        speedGroup.add(mnuSpeedFast);
        mnuSummaryTickerSpeed.add(mnuSpeedFast);
        applySavedTickerVariables();
        mnuCloseTicker = new TWMenuItem(Language.getString("CLOSE"), "close.gif");
        mnuCloseTicker.addActionListener(this);
        mnuCloseTicker.setActionCommand("CLOSE_TICKER");

        /*if (Settings.isShowSummaryTicker()) {
            mnuSummaryTicker.setSelected(true);
        } else {
            mnuSummaryTickerSpeed.setEnabled(false);
        }*/

        fixMenu = new TWMenu(Language.getString("FIX_TICKER"), "floatticker.gif");
        floatMenu = new TWMenuItem(Language.getString("FLOAT_TICKER"), "floatticker.gif");
        floatMenu.addActionListener(this);
        floatMenu.setText(Language.getString("FLOAT_TICKER"));
        floatMenu.setActionCommand("FLOAT_TICKER");

        ButtonGroup tickerFixingGroup = new ButtonGroup();
        mnuTickerFixedTop = new TWMenuItem(Language.getString("TICKER_FIXED_TOP"));
        mnuTickerFixedTop.addActionListener(this);
        mnuTickerFixedTop.setActionCommand("FIX_TOP");
        fixMenu.add(mnuTickerFixedTop);
        mnuTickerFixedBottom = new TWMenuItem(Language.getString("TICKER_FIXED_BOTTOM"));
        mnuTickerFixedBottom.addActionListener(this);
        mnuTickerFixedBottom.setActionCommand("FIX_BOTTOM");
        fixMenu.add(mnuTickerFixedBottom);

        tickerFilter = new TWMenu(Language.getString("FILTER"), "filter.gif");
        tickerFilter.getPopupMenu().addPopupMenuListener(this);
        popupMenu.setLightWeightPopupEnabled(false);
        popupMenu.add(showHideTitle);
        //popupMenu.add(allwaysOnTop);
        popupMenu.add(showHideSymbol);
        popupMenu.add(showHideChange);
        popupMenu.add(mnuFont);
        popupMenu.add(floatMenu);
        popupMenu.add(fixMenu);
        popupMenu.add(new TWSeparator());
        popupMenu.add(mnuSummaryTickerSpeed);
        popupMenu.add(tickerFilter);
        popupMenu.add(mnuCloseTicker);
        //tickerFilter.pack();

        cardLayout = new CardLayout(1, 1);
        this.setLayout(cardLayout);
//        this.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.black);

        // Creates the objects and assign them to the seperate Threads
        tt = new TradeTicker();
        //adjustTickerControls();
        tt.addMouseListener(this);
        this.add(tt, CUS);
        this.add(PanelSetter.holderPanel, ADV);
        this.setBackground(UpperPanelSettings.TICKER_BORDER_COLOR);

        // Add Listeners for listening to the resizing evets
        SymComponent aSymComponent = new SymComponent();
        this.addComponentListener(aSymComponent);

        UIManager.addPropertyChangeListener(new UISwitchListener(this));
        applyTheme();
        GUISettings.applyOrientation(popupMenu);

    } // End Method

    private void applySavedTickerVariables() {
        String savedSpeed = Settings.getItem("TICKER_SPEED");
        if ((savedSpeed != null) && (savedSpeed.equals("SPEED_SLOW") || savedSpeed.equals("SPEED_MEDIUM") || savedSpeed.equals("SPEED_FAST"))) {
            setTickerSpeed(savedSpeed);
        } else {
            setTickerSpeed("SPEED_FAST");
        }
        GlobalSettings.isSymbolShowing = Settings.getBooleanItem("TICKER_SHOW_SYMBOL");
        setSymbolCaption();
        GlobalSettings.isChangeMode = Settings.getBooleanItem("TICKER_CHANGE_MODE");
        setChangeOption();
    }

    private void setTickerSpeed(String speed) {
        if (speed.equals("SPEED_SLOW")) {
            mnuSpeedSlow.setSelected(true);
            TradeFeeder.setSummaryTickerDelay(1200);
            CURRENT_TICKER_SPEED = 1;
            TradeTicker.PIXEL_INCREMENT = 1;
        } else if (speed.equals("SPEED_MEDIUM")) {
            mnuSpeedMedium.setSelected(true);
            TradeFeeder.setSummaryTickerDelay(450);
            CURRENT_TICKER_SPEED = 2;
            TradeTicker.PIXEL_INCREMENT = 2;
        } else if (speed.equals("SPEED_FAST")) {
            mnuSpeedFast.setSelected(true);
            TradeFeeder.setSummaryTickerDelay(1);
            CURRENT_TICKER_SPEED = 3;
            TradeTicker.PIXEL_INCREMENT = 4;
        }
        Settings.saveItem("TICKER_SPEED", speed);
    }


    public void applyTheme() {
        try {
            GlobalSettings.TICKER_BACKGROUND = Theme.getColor("TICKER_BGCOLOR");
            GlobalSettings.NO_CHANGE_COLOR = Theme.getColor("TICKER_NO_CHANGE_FGCOLOR");
            GlobalSettings.UP_COLOR = Theme.getColor("TICKER_UP_FGCOLOR");
            GlobalSettings.DOWN_COLOR = Theme.getColor("TICKER_DOWN_FGCOLOR");
            GlobalSettings.SMALL_TRADE_COLOR = Theme.getColor("TICKER_SMALL_TRADE_FGCOLOR");
            GlobalSettings.TICKER_MIN_PRICE_COLOR = Theme.getColor("TICKER_MIN_PRICE_COLOR");
            GlobalSettings.TICKER_MAX_PRICE_COLOR = Theme.getColor("TICKER_MAX_PRICE_COLOR");
            GlobalSettings.TICKER_MSG_COLOR = Theme.getColor("TICKER_MSG_FGCOLOR");
            this.setBackground(UpperPanelSettings.TICKER_BORDER_COLOR);
        } catch (Exception e) {
            e.printStackTrace();
            GlobalSettings.TICKER_BACKGROUND = Color.black;
            GlobalSettings.NO_CHANGE_COLOR = Color.white;
        }
        if (tt != null) {
            try {
//                GlobalSettings.adjustTickerSize(this.getSize().width);
                GlobalSettings.adjustTickerSize((int) Client.getInstance().getDesktop().getSize().getWidth());
                tt.adjustControls();
                tt.redrawTicker();
                adjustTickerControls();

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        SwingUtilities.updateComponentTreeUI(popupMenu);
    }

    /*public void buildExchangeFilterMenu() {
        JPanel panel = filterPanel.getExchangePanel();
        panel.removeAll();
        panel.setBorder(BorderFactory.createEmptyBorder(2, 20, 2, 20));
        ArrayList<String> preferences = new ArrayList<String>();

//        if (ExchangeStore.getSharedInstance().isDefaultExchangesAvailable()) {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                TWCustomCheckBox item = new TWCustomCheckBox(exchange.getDescription(),
                        SwingUtilities.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
                item.setTag(exchange.getSymbol());
                // set initial selections
                if (((Settings.getTickerFilter() != null) &&
                        (Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_EXCHANGE.toString())) &&
                        (Settings.getTickerFilterData().indexOf(exchange.getSymbol()) >= 0)) {
                    item.setSelected(true);
                    preferences.add(exchange.getSymbol());
                } else {
                    item.setSelected(false);
                }
                //item.addMouseListener(filterPanel);
                panel.add(item);
                item = null;
            }
            exchange = null;
        }
        exchanges = null;

        if ((Settings.getTickerFilter() != null) && ((Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_EXCHANGE.toString())))
        {
            filterPanel.setMode(TWTypes.TickerFilter.MODE_EXCHANGE);
        } else {
            filterPanel.unsetMode(TWTypes.TickerFilter.MODE_EXCHANGE);
        }
        GUISettings.applyOrientation(panel);
        panel = null;
    }*/

    /*public void buildWatchlistFilterMenu() {
        JPanel panel = filterPanel.getWatchlistPanel();
        panel.removeAll();

        WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
        for (int i = 0; i < watchlists.length; i++) {
            WatchListStore store = watchlists[i];
            TWCustomCheckBox item = new TWCustomCheckBox(store.getCaption(),
                    SwingUtilities.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
            item.setTag(store.getId());
            if (((Settings.getTickerFilter() != null) &&
                    (Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString())) &&
                    (Settings.getTickerFilterData().indexOf(store.getId()) >= 0)) {
                item.setSelected(true);
            } else {
                item.setSelected(false);
            }
            //item.addMouseListener(filterPanel);
            panel.add(item);
            store = null;
        }

        if ((Settings.getTickerFilter() != null) && ((Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString()))){
            filterPanel.setMode(TWTypes.TickerFilter.MODE_WATCHLIST);
        } else {
            filterPanel.unsetMode(TWTypes.TickerFilter.MODE_WATCHLIST);
        }
    }*/

    /**
     * <P>Adjust the Ticker Controls according to the changed control variables.
     * Handle the component orientation according to the language selected.
     */
    public void adjustTickerControls() {
//        tt.setBounds(new Rectangle(0, 0, this.getWidth(), this.getHeight()));
        tt.setBounds(new Rectangle(0, 0, (int) Client.getInstance().getDesktop().getSize().getWidth(), this.getHeight()));
    }


    /**
     * Default run Method
     */
    /*public void run() {
        // Start all the currently available Threads
        stockThread.start();

    }*/ // End Method
    public JPopupMenu getMenu() {
        return popupMenu;
    }

    //************************************************************************
    //     Section that handles the Mouse Events
    //************************************************************************
    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            if (!settings.isHeaderVisible()) {
                showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
                showHideTitle.setIconFile("showtitle.gif");
            } else {
                showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
                showHideTitle.setIconFile("hidetitle.gif");
            }
            System.out.println("Detachbility : " + Client.getInstance().getTickerFrame().isDetached());

            if (Client.getInstance().isTickerFixed()) {

                if (Ticker.tickerFixedToTopPanel) {
                    mnuTickerFixedTop.setEnabled(false);
                    mnuTickerFixedBottom.setEnabled(true);
                } else {
                    mnuTickerFixedTop.setEnabled(true);
                    mnuTickerFixedBottom.setEnabled(false);
                }
                popupMenu.getComponent(6).setVisible(true);
                popupMenu.getComponent(7).setVisible(true);
            } else {
                mnuTickerFixedBottom.setEnabled(true);
                mnuTickerFixedTop.setEnabled(true);
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(true);
            }

            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(false);
                popupMenu.getComponent(11).setVisible(false);
            } else {
                popupMenu.getComponent(11).setVisible(true);
            }
            if ((Settings.isShowSummaryTicker()) && (ExchangeStore.getSharedInstance().defaultCount() > 0)) {
                if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_MarketTimeAndSales)) {
                        popupMenu.getComponent(0).setVisible(true);
                    } else {
                        popupMenu.getComponent(0).setVisible(false);

                    }
                } else {
                    popupMenu.getComponent(0).setVisible(false);
                }
            } else {
                if ((ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_TradeTicker)) && (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_MarketTimeAndSales))) {
                    popupMenu.getComponent(0).setVisible(true);
                    try {
                        mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
                        mnuSummaryTicker.setActionCommand("SUMMARY");
                    } catch (Exception e1) {
                    }
                }
            }

            if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL.toString())) {
                popupMenu.getComponent(3).setVisible(true);
                popupMenu.getComponent(4).setVisible(true);
                popupMenu.getComponent(9).setVisible(true);
                popupMenu.getComponent(10).setVisible(true);
            } else {
                popupMenu.getComponent(3).setVisible(false);
                popupMenu.getComponent(4).setVisible(false);
                popupMenu.getComponent(9).setVisible(false);
                popupMenu.getComponent(10).setVisible(false);
            }

            GUISettings.showPopup(popupMenu, this, e.getX(), e.getY());
        }
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        IS_DEFAULT = false;
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = exchanges.nextElement();
            if (exchange.isDefault()) {
                IS_DEFAULT = true;
            }
        }
        if (!IS_DEFAULT) {

            TWTypes.TickerFilter tickerMode;
            String currentMode = Settings.getTickerFilter();
            if (currentMode.equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString())) {
                System.out.println("current mode is watchlist");
            } else {
                Settings.setTickerFilter(TWTypes.TickerFilter.MODE_WATCHLIST.toString());
                Symbols[] symbols = new Symbols[0];
                ArrayList<Symbols> templist = new ArrayList<Symbols>();
                StringBuilder selectedIDs = new StringBuilder();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        templist.add(watchlist);
                        selectedIDs.append(watchlist.getId());
                        selectedIDs.append(",");
                        watchlist = null;
                    }
                }
                symbols = templist.toArray(symbols);
                TradeFeeder.setFilter(TWTypes.TickerFilter.MODE_WATCHLIST, symbols);
                Settings.setTickerFilterData(selectedIDs.toString());
            }

        }
        Client.getInstance().getTickerFrame().resizeFrame();
        SharedMethods.updateComponent(Client.getInstance().getTickerFrame());
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (previousExchange != null) {
            if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_MarketTimeAndSales)) {  //Settings.isTradeTickerMode()
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) {
                    mnuSummaryTicker.setVisible(true);
                    if (!Settings.isShowSummaryTicker()) {
                        if (!previousExchange.equals(exchange.getSymbol())) {
                            if (!previousExchange.equals("")) {
                                RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + previousExchange);
                                String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
                                Client.getInstance().addMarketTradeRequest(requestID, ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                            }
                        }
                    }
                }
                previousExchange = exchange.getSymbol();
            } else {
                mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
                Settings.setShowSummaryTicker(true);
                if (TradeFeeder.isVisible()) {
                    TradeFeeder.activateSummaryTickerUpdator(true);
                }
                mnuSummaryTickerSpeed.setEnabled(true);
                mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
                RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
            }
        }
    }

    // Adjust the control details when the Ticker is resized
    public void Ticker_ComponentResized() {
//        GlobalSettings.adjustTickerSize(this.getSize().width);
        GlobalSettings.adjustTickerSize((int) Client.getInstance().getDesktop().getSize().getWidth());
        tt.adjustControls();
        // Redraw the according to the new dimnsions
        tt.redrawTicker();
        SharedMethods.updateComponent(tt);
    }

    public void Ticker_Resized() {
//        GlobalSettings.adjustTickerSize(this.getSize().width);
        GlobalSettings.adjustTickerSize((int) Client.getInstance().getDesktop().getSize().getWidth());
        GlobalSettings.adjustTickerHeight();
        tt.adjustControls();

    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {
        TradeFeeder.setVisible(false);
    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {
        TradeFeeder.setVisible(false);
    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
        TradeFeeder.setVisible(true);
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE_TITLEBAR")) {
            InternalFrame frame = (InternalFrame) settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);
            setHideShowMenuCaption();
            frame = null;
        } else if (e.getActionCommand().equals("SHOW_TITLEBAR")) {
            InternalFrame frame = (InternalFrame) settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);
            setHideShowMenuCaption();
            frame = null;
        } else if (e.getActionCommand().equals("FONT")) {
            FontChooser oFontChooser = new FontChooser(Client.getInstance().getFrame(), true);
            Font oFont = null;
            oFont = new TWFont(GlobalSettings.FONT_TYPE, GlobalSettings.FONT_STYLE, GlobalSettings.FONT_BIG);
            oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"), oFont);
            if (oFont != null) {
                GlobalSettings.FONT_STYLE = oFont.getStyle();
                GlobalSettings.FONT_TYPE = oFont.getName();
                GlobalSettings.FONT_BIG = oFont.getSize();
                GlobalSettings.FONT_MEDIUM = GlobalSettings.FONT_BIG - 3;
                GlobalSettings.FONT_SMALL = GlobalSettings.FONT_BIG - 4;
                GlobalSettings.reloadFont(tt);
                tt.reloadFont();
                tt.adjustControls();
                tt.redrawTicker();
                if (DetachableRootPane.IS_DETACHED) {
                    DetachableRootPane.FONT_SELECTED = true;
                    DetachableRootPane.getInstance().fontResized();
                }
                Client.getInstance().getDesktop().revalidate();
                parentFrame.processCompleted(true, null);
            }
        } else if (e.getActionCommand().equals("SHOW_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_PER_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_SYMBOL")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("SHOW_DESCRIPTION")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("FIX_TOP")) {
            tickerSetToTop = true;
            mnuTickerFixedTop.setSelected(true);
            Client.getInstance().fixTicker();
        } else if (e.getActionCommand().equals("FIX_BOTTOM")) {
            tickerSetToTop = true;
            mnuTickerFixedBottom.setSelected(true);
            Client.getInstance().fixTickerToBottom();
        } else if (e.getActionCommand().equals("FLOAT_TICKER")) {
            Client.getInstance().floatTicker();
            mnuTickerFixedBottom.setSelected(false);
            mnuTickerFixedTop.setSelected(false);
        } else if (e.getActionCommand().equals("SUMMARY")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            Settings.setShowSummaryTicker(true);
            if (TradeFeeder.isVisible()) {
                TradeFeeder.activateSummaryTickerUpdator(true);
            }
            mnuSummaryTickerSpeed.setEnabled(true);
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
//            RequestManager.getSharedInstance().sendRemoveRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|"+ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
            RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());

        } else if (e.getActionCommand().equals("SUMMARY_TRADE")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            Settings.setShowSummaryTicker(false);
            if (TradeFeeder.isVisible()) {
                TradeFeeder.activateSummaryTickerUpdator(false);
            }
            mnuSummaryTickerSpeed.setEnabled(false);
            setTickerSpeed("SPEED_SLOW");
            mnuSummaryTicker.setActionCommand("SUMMARY");
            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
            Client.getInstance().addMarketTradeRequest(requestID, ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());

        } else if (e.getActionCommand().equals("CLOSE_TICKER")) {
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) { //Settings.isTradeTickerMode()
                if (!Settings.isShowSummaryTicker()) {
                    RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());

                }
            }

            SharedSettings.selectCustomTicker(false);
            SharedSettings.custom = SharedSettings.deselect;
            Client.getInstance().mnu_ticker_ActionPerformed();

        } else if (e.getActionCommand().equals("SPEED_SLOW")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_MEDIUM")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_FAST")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("allwaysOnTop")) {
            JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, this);
            System.out.println(parent.getTitle());
            parent.setAlwaysOnTop(true);
        } else if (e.getActionCommand().equals("ADVANCED_TICKER")) {
            SharedSettings.showAdvancedTicker();
        }
    }

    private void setFixFloatCaption() {
        if (Client.getInstance().isTickerFixed()) {
            floatMenu.setText(Language.getString("FLOAT_TICKER"));

            floatMenu.setActionCommand("FLOAT_TICKER");
        } else {
            floatMenu.setText(Language.getString("FIX_TICKER"));
            floatMenu.setActionCommand("FIX_TICKER");
//                    Client.getInstance().fixTicker();
        }
    }

    public void setExchangePreferences(TWCheckBoxMenuItem selectedItem) {
        /*ArrayList<String> preferences = new ArrayList<String>();

        for (int i = 0; i < exchangeFilter.getItemCount(); i++) {
            TWCheckBoxMenuItem item = (TWCheckBoxMenuItem) exchangeFilter.getItem(i);
            if (item.isSelected()) {
                preferences.add(item.getActionCommand().substring(1));
            }
            item = null;
        }
        if (preferences.size() > 0) {
            String[] selectedExchanges = preferences.toArray(new String[0]);
            TradeFeeder.setPreferredExchanges(selectedExchanges);
            String list = "";
            for (int i = 0; i < selectedExchanges.length; i++) {
                list += selectedExchanges[i];
                list += ",";
            }
            Settings.setTickerExchanges(list);
            list = null;
        } else {
            selectedItem.setSelected(true);
        }*/

    }

    public void showFilter() {

    }

//    public void setWatchlistPreferences(TWCheckBoxMenuItem selectedItem){
//        WatchListStore store = WatchListManager.getInstance().getStore(selectedItem.getActionCommand().substring(1));
//        TradeFeeder.setSymbolFilter(store);
//
//
//    }

    private void setSymbolCaption() {
        if (GlobalSettings.isSymbolShowing) {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL_DESCRIPTION"));
            showHideSymbol.setActionCommand("SHOW_DESCRIPTION");
        } else {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));
            showHideSymbol.setActionCommand("SHOW_SYMBOL");
        }
    }

    public void setHideShowSymbolCaption() {
        GlobalSettings.isSymbolShowing ^= true;
//        GlobalSettings.isSymbolShowing = !GlobalSettings.isSymbolShowing;
        setSymbolCaption();
        tt.reloadFont();
        tt.redrawTicker();
        Settings.saveItem("TICKER_SHOW_SYMBOL", "" + GlobalSettings.isSymbolShowing);
    }

    private void setChangeOption() {
        if (GlobalSettings.isChangeMode) {
            showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));
            showHideChange.setActionCommand("SHOW_PER_CHANGE");
        } else {
            showHideChange.setText(Language.getString("SHOW_CHANGE"));
            showHideChange.setActionCommand("SHOW_CHANGE");
        }
    }

    public void setHideShowChangeOption() {
        GlobalSettings.isChangeMode ^= true;
//        GlobalSettings.isChangeMode = !GlobalSettings.isChangeMode;
        setChangeOption();
        tt.reloadFont();
        tt.redrawTicker();
        Settings.saveItem("TICKER_CHANGE_MODE", "" + GlobalSettings.isChangeMode);
    }

    public void setHideShowMenuCaption() {
        if (!settings.isHeaderVisible()) {
            showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
            showHideTitle.setIconFile("showtitle.gif");
            IS_TITLE_SHOW = false;
        } else {
            showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
            showHideTitle.setIconFile("hidetitle.gif");
            IS_TITLE_SHOW = true;
        }
    }

    public void setParent(ProcessListener parent) {
        parentFrame = parent;
    }

    /**
     */
    public String toString() {
        String wsString = "" + GlobalSettings.TICKER_WIDTH + "|" +
                GlobalSettings.STOCK_TICKER_HEIGHT + "|" +
                GlobalSettings.FONT_TYPE + "|" +
                GlobalSettings.FONT_STYLE + "|" +
                GlobalSettings.FONT_BIG;
//System.out.println("---- toString()=" + wsString);
        return wsString;
    }

    public void applyWorkSpace(String wsString) {
        StringTokenizer st = new StringTokenizer(wsString, "|", false);
        try {
            GlobalSettings.TICKER_WIDTH = Integer.parseInt(st.nextToken());
            GlobalSettings.STOCK_TICKER_HEIGHT = Integer.parseInt(st.nextToken());
            GlobalSettings.FONT_TYPE = st.nextToken();
            GlobalSettings.FONT_STYLE = Integer.parseInt(st.nextToken());
            GlobalSettings.FONT_BIG = Integer.parseInt(st.nextToken());

            GlobalSettings.FONT_MEDIUM = GlobalSettings.FONT_BIG - 3;
            GlobalSettings.FONT_SMALL = GlobalSettings.FONT_BIG - 4;
            GlobalSettings.reloadFont(tt);
            tt.reloadFont();
            tt.adjustControls();
            tt.redrawTicker();
        } catch (Exception ex) {
            // System.err.println("****************************");
            ex.printStackTrace();
        }
        st = null;
    }

    /* Popup menu listeners */
    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        tickerFilter.removeAll();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
//        setFixFloatCaption();
        tickerFilter.removeAll();
        tickerFilter.getPopupMenu().setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        tickerFilter.getPopupMenu().setLightWeightPopupEnabled(false);
        FilterPanel filterPanel = new FilterPanel(tickerFilter);
        tickerFilter.add(filterPanel);
        try {
//            JFrame parent = (JFrame) (SwingUtilities.getAncestorOfClass(JFrame.class, (Component) e.getSource()));
            if (((DetachableRootPane) this.getRootPane()).isDetached()) {
                showHideTitle.setVisible(false);
                mnuAdvanceTicker.setVisible(false);
            } else {
                showHideTitle.setVisible(true);
                mnuAdvanceTicker.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
            }
        } catch (Exception e1) {
            showHideTitle.setVisible(true);
        }
        tickerFilter.getPopupMenu().pack();
    }

    /* Exchange listener metods */
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void symbolRemoved(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void watchlistAdded(String listID) {
        //buildWatchlistFilterMenu();
    }

    public void watchlistRenamed(String listID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void watchlistRemoved(String listID) {
        //buildWatchlistFilterMenu();
    }

    /**
     * This class tracks the Ticker resizing event and do the layouting as necessary
     */
    class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object == Ticker.this) {
                if (TradeTicker.IS_FRAME_RESIZED) {
                    Ticker_ComponentResized();
                } else if (TradeTicker.IS_TICKER_RESIZED || Client.ISTICKERFIXED) {
                    Ticker_ComponentResized();
                }

            }
        }
    }


}
