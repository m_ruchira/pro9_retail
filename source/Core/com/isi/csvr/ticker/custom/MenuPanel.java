/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */
package com.isi.csvr.ticker.custom;

import com.isi.csvr.ticker.GlobalSettings;

import javax.swing.*;
import java.awt.*;

/**
 * A Swing-based panel class.
 * <p/>
 *
 * @author Bandula Priyadarshana
 */
public class MenuPanel extends JPanel implements Runnable {
    private static Image imageMenu;
    private static Image menuImage;
    private static Graphics menuG;

    /**
     * Constructs a new instance.
     */
    public MenuPanel() {
        super();
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        imageMenu = this.getToolkit().getImage(GlobalSettings.IMAGE_PATH + GlobalSettings.MENU_IMAGE_FILE + ".gif");
        this.setBackground(Color.black);
        this.setPreferredSize(new Dimension(GlobalSettings.MENU_AREA_WIDTH, GlobalSettings.MENU_IMAGE_HEIGHT));
    }

    public void paint(Graphics g) {
        try {
            //*************************************************************
            // Draw the Menu
            //*************************************************************
            menuImage = this.createImage(GlobalSettings.MENU_AREA_WIDTH, GlobalSettings.MENU_AREA_HEIGHT);
            menuG = menuImage.getGraphics();// Graphics object for DoubleBuffering

            menuG.setColor(GlobalSettings.TICKER_SEPERATOR);
            //menuG.fill3DRect(0, 0, GlobalSettings.MENU_SEPERATOR_WIDTH, GlobalSettings.MENU_AREA_HEIGHT, true);
            // Draw the Image here
            menuG.drawImage(imageMenu, GlobalSettings.MENU_SEPERATOR_WIDTH, 0, GlobalSettings.MENU_IMAGE_WIDTH, GlobalSettings.MENU_IMAGE_HEIGHT, this);

            // Draw the OffScreen Image to the applet
            g.drawImage(menuImage, 0, 0, GlobalSettings.MENU_AREA_WIDTH, GlobalSettings.MENU_AREA_HEIGHT, this);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Errors occurred while drawing Image MenuPanel" + e);
        }
    } // End Method

    public void run() {
        this.repaint();
    }

}
