/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */
package com.isi.csvr.ticker.custom;

import com.isi.csvr.shared.Language;
import com.isi.csvr.ticker.GlobalSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * A Swing-based panel class.
 * <p/>
 *
 * @author Bandula Priyadarshana
 */
public class StatusPanel extends JPanel implements MouseListener {
    private static JLabel modeDisplay;
    private static JLabel resetDisplay;
    private static JLabel nullLabel;
    private static boolean isTickerCustomized;
    private BorderLayout layout = new BorderLayout();

    /**
     * Constructs a new instance.
     */
    public StatusPanel() {
        super();
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void setDisplayText() {
        if (isTickerCustomized) {
            //resetDisplay.setText("   " + Language.getString("TICKER_NORMAL_MODE"));
            modeDisplay.setText("   " + Language.getString("TICKER_CUSTOMIZED"));
        } else {
            //resetDisplay.setText("   " + Language.getString("TICKER_CUSTOMIZED_MODE"));
            modeDisplay.setText("   " + Language.getString("NORMAL_MODE"));
        }
    }

    public static void setStatus(boolean status) {
        isTickerCustomized = status;
        setDisplayText();
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        this.setLayout(layout);
        //this.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.white);

        modeDisplay = new JLabel();
        resetDisplay = new JLabel();
        nullLabel = new JLabel("         ");

        //modeDisplay.setAlignmentX(JLabel.CENTER);
        //modeDisplay.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.white);
        //resetDisplay.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.green);
        //nullLabel.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.black);

        nullLabel.setCursor(new Cursor(Cursor.TEXT_CURSOR));
        modeDisplay.setCursor(new Cursor(Cursor.TEXT_CURSOR));

        modeDisplay.setForeground(GlobalSettings.UP_COLOR); // Color.blue);
        resetDisplay.setForeground(GlobalSettings.DOWN_COLOR); // Color.red);

        //resetDisplay.setFont(new TWFont(UIManager.getFont("Label.font").getFontName(), Font.BOLD, 12));
        //resetDisplay.setFont(new TWFont("Dialog", Font.PLAIN, 12));

        //modeDisplay.addMouseListener(this);
        //resetDisplay.addMouseListener(this);

        if (Language.isLTR()) {
            this.add(modeDisplay, BorderLayout.WEST);
            this.add(nullLabel, BorderLayout.CENTER);
            this.add(resetDisplay, BorderLayout.EAST);
        } else {
            this.add(resetDisplay, BorderLayout.WEST);
            this.add(nullLabel, BorderLayout.CENTER);
            this.add(modeDisplay, BorderLayout.EAST);
        }

        setDisplayText();
        updateUI();
    }

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *     Section that handles the Mouse Events
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    public void mouseEntered(MouseEvent e) {
/*        if( e.getSource() == modeDisplay ) {
            //modeDisplay.setForeground(GlobalSettings.UP_COLOR); // Color.blue);
            //modeDisplay.setFont(new TWFont("Dialog", Font.BOLD, 12));
            //modeDisplay.setFont(new TWFont("Helvetica", Font.BOLD, 12));
        } else if( e.getSource() == resetDisplay ) {
            resetDisplay.setForeground(GlobalSettings.UP_COLOR); // Color.blue);
            //resetDisplay.setFont(new TWFont("Dialog", Font.BOLD, 12));
        }
*/
    }

    public void mouseExited(MouseEvent e) {
/*        if( e.getSource() == modeDisplay ) {
            //modeDisplay.setFont(new TWFont("Dialog", Font.PLAIN, 12));
        } else if( e.getSource() == resetDisplay ) {
            resetDisplay.setForeground(GlobalSettings.DOWN_COLOR); // Color.blue);
            //resetDisplay.setFont(new TWFont("Dialog", Font.PLAIN, 12));
        }
*/
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
/*        if( e.getSource() == resetDisplay ) {
            //System.out.println("Swapping Mode...");
            isTickerCustomized = !isTickerCustomized;
            setDisplayText();
            if (isTickerCustomized) {
                Ticker.searchSymbols();
            } else {
                TradeFeeder.setNormalMode();
            }
        }
*/
    }

    public void setTheme() {

        //modeDisplay.setBackground(Color.magenta);//GlobalSettings.TICKER_BACKGROUND); // Color.white);
        //resetDisplay.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.green);
        //nullLabel.setBackground(GlobalSettings.TICKER_BACKGROUND); // Color.black);

        //modeDisplay.updateUI();
        //this.updateUI();
        //SwingUtilities.updateComponentTreeUI(this); //modeDisplay);
        //SwingUtilities.updateComponentTreeUI(resetDisplay);
        //SwingUtilities.updateComponentTreeUI(nullLabel);
        //repaint();
    }
}
