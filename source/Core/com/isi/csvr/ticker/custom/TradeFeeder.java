/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */

package com.isi.csvr.ticker.custom;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWTypes;

import java.util.LinkedList;
import java.util.TimeZone;

public class TradeFeeder implements Runnable, ApplicationListener {

    //    private static String[] selectedSymbols;
    public static LinkedList<TickerObject> tradeQueue;
    private static boolean isQueueEmpty = true;
    //    private static TWTypes.TickerFilter mode = TWTypes.TickerFilter.MODE_ALL;
    private static TWTypes.TickerFilter mode = null;
    private static TradeTicker trade;
    private static TickerDataInterface symbolSource;
    private static DynamicArray marketSummaryList;
    private static boolean g_bVisible = false;
    private static Symbols[] filter;
    private static int summaryTickerDelay;
    private static SummaryTickerUpdator summaryTickerUpdator;

    private TickerObject readObject;

    /**
     * Constructor
     */
    public TradeFeeder(TradeTicker tradeIn) {
        trade = tradeIn;
        tradeQueue = new LinkedList<TickerObject>();
        marketSummaryList = new DynamicArray(50);
        filter = new Symbols[0];
        Application.getInstance().addApplicationListener(this);
    }

    /*private void loadInitialFilterSettings(){
        String filterData = Settings.getTickerFilterData();
        ArrayList<Symbols> symbols = new ArrayList<Symbols>();

        if ((Settings.getTickerFilter() != null) && ((Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString()))){
            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            if (filterData != null){
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist: watchlists) {
                    if (filterData.indexOf(watchlist.getId()) >=0){
                        symbols.add(watchlist);
                    }
                }
                filter = symbols.toArray(filter);
            }
        } else {
            mode = TWTypes.TickerFilter.MODE_ALL;
            filter = new Symbols[0];
            filter[0] = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());
        }
    }*/

    public static void setFilter(TWTypes.TickerFilter newMode, Symbols[] filterData) {
        filter = filterData;
        if (filter != null) {
            mode = newMode;
        } else {
            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
        }
    }

    public static Symbols[] getFilter() {
        return filter;
    }

    public static TWTypes.TickerFilter getMode() {
        return mode;
    }

    public static void clearOfflineData() {
        marketSummaryList.clear();
        marketSummaryList.trimToSize();
    }

    /*public static boolean isPreferredSymbol(String symbol) {
        if ((mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (mode == TWTypes.TickerFilter.MODE_EXCHANGE)) {
            if (filter == null) { // no preference, play all
                return true;
            } else {
                for (int i = 0; i < filter.length; i++) {
                    if (filter[i].equals(symbol)) {
                        return true;
                    }
                }
            }
        } else {
            return true;
        }
        return false;
    }*/

    /*public static void setSymbolFilter(String[] source) {
        filter = source;
        //selectedSymbols = symbolSource.readTickerData();
        if (filter != null) {
            Ticker.setMode(Ticker.MODE_SYMBOL);
        } else {
            Ticker.setMode(Ticker.MODE_ALL);
        }
    }*/

    public static void clearOnlineData() {
        try {
            tradeQueue.clear();
            tradeQueue = null;
            tradeQueue = new LinkedList<TickerObject>();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (trade != null) {
            trade.clearTickerTape();
        }
    }

    public static void refreshSymbolIndex() {
//        try {
//            if (symbolSource != null)
//                selectedSymbols = symbolSource.readTickerData();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    public static LinkedList<TickerObject> getQueue() {
        return tradeQueue;
    }

    public static void addData(TickerObject oTickerObject) {
        try {
            tradeQueue.add(oTickerObject);
            /*if (tradeQueue.size() > TWControl.TICKER_FRAME_COUNT){
                tradeQueue.remove(0); // remove one object to maintain the max size of list
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private boolean contains(TickerObject tickerObject) {
        if (mode == TWTypes.TickerFilter.MODE_ALL) {
            return true;
        } else {
            if (mode == TWTypes.TickerFilter.MODE_EXCHANGE) {
                return (Arrays.binarySearch(filter, tickerObject.getExchange()) >= 0);
            } else {
                return (Arrays.binarySearch(filter, tickerObject.getKey()) >= 0);
            }
        }
    }*/

    /*public static boolean contains(String key) {
        return (Arrays.binarySearch(filter, key) >= 0);
    }*/

    public static void addMarketSummaryData(TickerObject oTickerObject) {
        marketSummaryList.add(oTickerObject);
    }

    public static void activateSummaryTickerUpdator(boolean activate) {
        if (activate) {
            if (summaryTickerUpdator == null) {
                summaryTickerUpdator = new SummaryTickerUpdator();
            }
        } else {
            if (summaryTickerUpdator != null) {
                summaryTickerUpdator.deactivate();
                summaryTickerUpdator = null;
            }
        }
    }

    public static int size() {
        return tradeQueue.size();
    }

    /* private void readeTradeData() {


        int sleepInterval = 10;

        while (isVisible() && (tradeQueue.size() > 0)) {
            if (TickerFrame.IS_DATA_COME || Ticker.IS_FRAME_DATA_COME) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                TickerFrame.IS_DATA_COME = false;
                Ticker.IS_FRAME_DATA_COME = false;
            }

            readObject = tradeQueue.remove(0);

            if (readObject != null) {
                synchronized (TickerFrame.SYN_RESIZED) {
                    if (TickerFrame.IS_DATA_COME || Ticker.IS_FRAME_DATA_COME) {
                        continue;
                    }
                    trade.createNewTradeTicker(readObject.getExchange(), readObject.getSymbol(), readObject.getPrice() + "",
                            readObject.getQuantity() + "", readObject.getStatus(),
                            readObject.getChange() + "", readObject.getPercentChange() + "", readObject.getSplits(), readObject.getInstrument());
                }

                if (Ticker.CURRENT_TICKER_SPEED == 1) {
                    //value before
                    //            sleepInterval = 10;
                    sleepInterval = 6;
                } else if (Ticker.CURRENT_TICKER_SPEED == 2) {
                    sleepInterval = 3;

                } else if (Ticker.CURRENT_TICKER_SPEED == 3) {
                    sleepInterval = 2;
                }

                trade.setSleepInterval(sleepInterval);

            }
            readObject = null;
        }
        if (!isVisible()) {
            clearOnlineData();
        }
    }*/

    public static int getSummaryTickerDelay() {
        return summaryTickerDelay;
    }

//    private void readTradeData() {
//        int sleepInterval = 20;
//
//        while (isVisible() && (tradeQueue.size() > 0)) {
//
//            // Read the front element from the Queue
//            readObject = tradeQueue.remove(0);
////            if ((readObject != null) && (contains(readObject))) {
//            if (readObject != null) {
//                 trade.createNewTradeTicker(readObject.getExchange(), readObject.getSymbol(), readObject.getPrice() + "",
//                        readObject.getQuantity() + "", readObject.getStatus(),
//                        readObject.getChange() + "", readObject.getPercentChange() + "", readObject.getSplits(),readObject.getInstrument());
//               if(Ticker.CURRENT_TICKER_SPEED ==1){
//                    //value before
//        //            sleepInterval = 10;
//                    sleepInterval = 6;
//                }
//
//                else if(Ticker.CURRENT_TICKER_SPEED ==2){
//                    //value before
////                     sleepingInterval = 5;
//                    sleepInterval = 3;
//
//                }
//
//                else if( Ticker.CURRENT_TICKER_SPEED == 3 ){
//                    sleepInterval =2;
//                }
//
//                trade.setSleepInterval(sleepInterval);
//
//            }
//            readObject = null;
//        }
//        if (!isVisible()) {
//            clearOnlineData();
//        }
//    }

    public static void setSummaryTickerDelay(int summaryTickerDelay) {
        TradeFeeder.summaryTickerDelay = summaryTickerDelay;
    }

    public static void setNormalMode() {
        //isNormalMode = true;
        //selectedSymbols = null;
    }

    public static boolean isVisible() {
        return g_bVisible;
    }

    public static void setVisible(boolean bVisible) {
        g_bVisible = bVisible;
        if (bVisible) {
            if (Settings.isShowSummaryTicker()) {
                activateSummaryTickerUpdator(bVisible);
            }
        }
    }

    public void run() {
        if ((tradeQueue != null) || (marketSummaryList != null)) {
            while (true) {
                if (isVisible() && Settings.isConnected())  // !isDisconnected) {
                {
                    if (isQueueEmpty) {
                        emptyQueue();
                        isQueueEmpty = false;
                    }

                    try {
                        if (tradeQueue.size() > 0) {
                            readTradeData();

                        } else {
                            //   System.out.println("no data------------------");
                            if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL)) {

                            } else {
                                TradeTicker.tradeObjectList.clear();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                try {
                    Thread.sleep(100); //GlobalSettings.STOCK_DATA_READ_INTERVAL);      // Sleep the predefined interval, if the Queue is empty
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void emptyQueue() {
        while (!tradeQueue.isEmpty()) {
            tradeQueue.remove(0);
        }
    }

    private void readTradeData() {
        int sleepInterval = 10;

        while (isVisible() && (tradeQueue.size() > 0) && Settings.isConnected()) {
            if (TickerFrame.IS_DATA_COME || Ticker.IS_FRAME_DATA_COME) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if (TickerFrame.IS_DATA_COME || Ticker.IS_FRAME_DATA_COME) {
                    continue;
                }
            }
            readObject = tradeQueue.remove(0);

            if (readObject != null) {
                Exchange ex = ExchangeStore.getSharedInstance().getExchange(readObject.getExchange());
                if ((!ex.isDefault() && ex.getMarketStatus() == Constants.DEFAULT_STATUS) || ((ex.getMarketStatus() == Constants.OPEN) || (ex.getMarketStatus() == Constants.TRADING_AT_LAST))) {
                    trade.createNewTradeTicker(readObject.getExchange(), readObject.getSymbol(), readObject.getPrice() + "",
                            readObject.getQuantity() + "", readObject.getStatus(),
                            readObject.getChange() + "", readObject.getPercentChange() + "", readObject.getSplits(), readObject.getInstrument());
                }

                if (Ticker.CURRENT_TICKER_SPEED == 1) {
                    //value before
                    //            sleepInterval = 10;
                    sleepInterval = 6;
                } else if (Ticker.CURRENT_TICKER_SPEED == 2) {
                    sleepInterval = 3;

                } else if (Ticker.CURRENT_TICKER_SPEED == 3) {
                    sleepInterval = 2;
                }

                trade.setSleepInterval(sleepInterval);

            }
            readObject = null;

        }
        if (!isVisible()) {
            clearOnlineData();
        }
    }

    public String getCompanyName(String exchange, String symbol, int instrument) {
        return DataStore.getSharedInstance().getShortDescription(exchange, symbol, instrument);
    }

    public void applicationExiting() {
    }

    public void applicationLoaded() {
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
        if (mode == TWTypes.TickerFilter.MODE_ALL) {
            filter = new Symbols[1];
            filter[0] = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());
        }
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {
    }
}

