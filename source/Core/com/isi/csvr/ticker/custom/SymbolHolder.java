/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version StockNet Ticker - Client Version 1.0 Copyright (c) 2001 ISI
 */

package com.isi.csvr.ticker.custom;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.ticker.GlobalSettings;

import java.awt.*;

/**
 * <P>The main object used to store the Trade details and the Message details for the
 * StockNet Ticker Client Version.<br>
 * It does perform the following tasks<br>
 * 01. Holds the Trade Details<br>
 * 02. Holds the Message Details<br>
 * 03. Calculate dimensions and draw the Trade Details<br>
 * 04. Calculate dimensions and draw the Message Details<br>
 */
public class SymbolHolder {

    //private SmartImage  smartImage;             // The reference for the Image List

    private static TWDecimalFormat intFormat = new TWDecimalFormat("#,##0");
    private static TWDecimalFormat floatFormat = new TWDecimalFormat("#,##0.00");
    // Trade details
    private String symbol;
    private String acronym;
    private String exchange;
    private String price;
    private String quantity;
    private String percentChange;
    private String change;
    private int splits;
    private String strSplits;
    private int insType;
    private int status;
    private double max_Price;
    private double min_Price;
    private double lastTrPrice;
    // Message Details
    private String scrollMessage;          // Holds the message
    private String statusOfMessage;        // Holds the message status
    private int symbolLength;
    private int perChaLength;
    private int changeLength;
    private int priceLength;
    private int quantityLength;
    private int splitLength;
    private int tradeAreaWidth;         // Holds the trade Area Width
    private int mesgAreaWidth;          // Holds the message Area Width
    private int scrollingDir;           // holds the Scrolling Direction
    private int earlierStartXPos;            // Previous displayed X Position
    private int perChangeDisplayOffset; // Per Changed display x Position
    private int totLen;
    /*
     *      Status of the Message
     *          0 - Not yet Displayed
     *          1 - The message is displaying
     *          2 - The message is already Displayed
     */
    private int displayStatus;
    private int prevDisplayStatus;
    private boolean isMessageMode;              // Check whether the Message Mode or Not


    /**
     * Constructor
     */
    public SymbolHolder() {
        displayStatus = 0;
        prevDisplayStatus = 0;
        scrollMessage = "";   // Holds the message
        statusOfMessage = "";   // Holds the message status
    }

    public void revalidate() {
        try {
            if (isMessageMode)
                calculateMessageWidth();          // Calculate the display area
            else
                calculateTradeAreaWidth();          // Calculate the display area
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * <P>Draws the Ticker Element at the location passed to the Method.
     *
     * @param xPos      - starting X Position
     * @param direction - Direction of scrolling
     * @param textG-    Graphics object for drawing the element
     */
    public void draw(int xPos, int direction, Graphics textG) {  // int yPos,
        synchronized (TradeTicker.SYN_CHECK) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position

            /*
            *  Used for calculating consecutive message offsets. DO NOT CHANGE
            */
            //    System.out.println("at the first ----"+xPos);
            if (direction == 1) {
                xPos = xPos - TradeTicker.PIXEL_INCREMENT;

                if (!isMessageMode) {      // In the Mesage Display Mode
                    if (xPos <= (-tradeAreaWidth)) { //GlobalSettings.LEFT_SEPERATOR) ) {
                        if (prevDisplayStatus != 0) {
                            displayStatus = 2;
                            prevDisplayStatus = displayStatus;
                        } else {
                            displayStatus = 0;
                            prevDisplayStatus = displayStatus;
                        }
                        return;
                    } else {
                        displayStatus = 1;
                        prevDisplayStatus = displayStatus;
                    }
                } else {

                    if (xPos <= (-mesgAreaWidth)) { //GlobalSettings.LEFT_SEPERATOR)) {
                        if (prevDisplayStatus != 0) {
                            displayStatus = 2;
                            prevDisplayStatus = displayStatus;
                        } else {
                            displayStatus = 0;
                            prevDisplayStatus = displayStatus;
                        }
                        return;
                    } else {
                        displayStatus = 1;
                        prevDisplayStatus = displayStatus;
                    }
                }
            } else if (direction == 2) {

                xPos = xPos + TradeTicker.PIXEL_INCREMENT;

                if (xPos >= (GlobalSettings.TICKER_WIDTH)) {
                    if (prevDisplayStatus != 0) {
                        displayStatus = 2;
                        prevDisplayStatus = displayStatus;
                    } else {
                        displayStatus = 0;
                        prevDisplayStatus = displayStatus;
                    }
                    return;
                } else {
                    displayStatus = 1;
                    prevDisplayStatus = displayStatus;
                }
            }

            earlierStartXPos = xPos;

            if (!GlobalSettings.ENGLISH_VERSION) {

                offsetDistance = xPos + this.tradeAreaWidth - GlobalSettings.LEFT_SEPERATOR;
                offsetDistance = offsetDistance - GlobalSettings.RIGHT_SEPERATOR;

                if ((max_Price > 0) && (min_Price > 0) && (lastTrPrice >= max_Price || lastTrPrice <= min_Price)) {
                    if (lastTrPrice <= min_Price) {
                        textG.setColor(GlobalSettings.TICKER_MIN_PRICE_COLOR);
                        textG.fillRect((offsetDistance - totLen), GlobalSettings.MIN_RECT_DISPLAY_HEIGHT, totLen, GlobalSettings.MAX_MIN_RECT_HEIGHT);
                    } else if (lastTrPrice >= max_Price) {
                        textG.setColor(GlobalSettings.TICKER_MAX_PRICE_COLOR);
                        textG.fillRect((offsetDistance - totLen), GlobalSettings.MAX_RECT_DISPLAY_HEIGHT, totLen, GlobalSettings.MAX_MIN_RECT_HEIGHT);
                    }
                }
            } else {

                offsetDistance = xPos + GlobalSettings.RIGHT_SEPERATOR;

                if ((max_Price > 0) && (min_Price > 0) && (lastTrPrice >= max_Price || lastTrPrice <= min_Price)) {

                    calculateMessageWidth();
                    if (lastTrPrice <= min_Price) {
                        textG.setColor(GlobalSettings.TICKER_MIN_PRICE_COLOR);
                        textG.fillRect((offsetDistance), GlobalSettings.MIN_RECT_DISPLAY_HEIGHT, (totLen + GlobalSettings.RIGHT_SEPERATOR), GlobalSettings.MAX_MIN_RECT_HEIGHT);

                    } else if (lastTrPrice >= max_Price) {
                        textG.setColor(GlobalSettings.TICKER_MAX_PRICE_COLOR);
                        textG.fillRect((offsetDistance), GlobalSettings.MAX_RECT_DISPLAY_HEIGHT, (totLen + GlobalSettings.RIGHT_SEPERATOR), GlobalSettings.MAX_MIN_RECT_HEIGHT);

                    }
                }
            }
            if (isMessageMode) {      // In the Mesage Display Mode
                if (statusOfMessage.equals(GlobalSettings.UP))
                    textG.setColor(GlobalSettings.UP_COLOR);//this.upColor);
                if (statusOfMessage.equals(GlobalSettings.DOWN))
                    textG.setColor(GlobalSettings.DOWN_COLOR);//this.downColor);
                if (statusOfMessage.equals(GlobalSettings.NOCHANGE))
                    textG.setColor(GlobalSettings.TICKER_SEPERATOR);//this.nochangeColor);
                if (statusOfMessage.equals(GlobalSettings.VALNULL))
                    textG.setColor(GlobalSettings.NULL_COLOR);

                textG.setFont(GlobalSettings.bigFont);
                textG.drawString(scrollMessage, xPos, GlobalSettings.SYMBOL_DISPLAY_HEIGHT + GlobalSettings.smallFm.getHeight()); //25);
            } else {                    // Now we are in the trade display mode
                if (status == Constants.TICKER_STATUS_UP)
                    textG.setColor(GlobalSettings.UP_COLOR);
                else if (status == Constants.TICKER_STATUS_DOWN)
                    textG.setColor(GlobalSettings.DOWN_COLOR);
                else if (status == Constants.TICKER_STATUS_NOCHANGE)
                    textG.setColor(GlobalSettings.NO_CHANGE_COLOR);
                else if (status == Constants.TICKER_STATUS_SMALLTRADE)
                    textG.setColor(GlobalSettings.SMALL_TRADE_COLOR);
                else
                    textG.setColor(GlobalSettings.NULL_COLOR);

                // If the Default language is Arabic
                if (!GlobalSettings.ENGLISH_VERSION) {
                    offsetDistance = xPos + this.tradeAreaWidth - GlobalSettings.LEFT_SEPERATOR;

                    // Draw Company Logo

                    offsetDistance = offsetDistance - GlobalSettings.RIGHT_SEPERATOR;

                    textG.setFont(GlobalSettings.bigFont);
                    if (GlobalSettings.isSymbolShowing) {
                        textG.drawString(symbol, offsetDistance - symbolLength, GlobalSettings.SYMBOL_DISPLAY_HEIGHT);
                    } else {
                        textG.drawString(acronym, offsetDistance - symbolLength, GlobalSettings.SYMBOL_DISPLAY_HEIGHT);
                    }

                    textG.setFont(GlobalSettings.smallFont);
                    textG.drawString("" + price, offsetDistance - priceLength, GlobalSettings.DETAIL_DISPLAY_HEIGHT);  // + GlobalSettings.STOCK_SYMBOL_SIZE
                    if (GlobalSettings.isChangeMode) {
                        textG.drawString("" + change, offsetDistance - priceLength - perChaLength - 20, GlobalSettings.DETAIL_DISPLAY_HEIGHT);
                    } else {
                        textG.drawString("" + percentChange + "%", offsetDistance - priceLength - perChaLength - 20, GlobalSettings.DETAIL_DISPLAY_HEIGHT);
                    }

                    textG.setColor(GlobalSettings.NO_CHANGE_COLOR);//this.upColor);
                    textG.drawString("" + quantity, offsetDistance - quantityLength, GlobalSettings.QTY_DISPLAY_HEIGHT);
                    if (splits > 1)
                        textG.drawString("(" + strSplits + ")", offsetDistance - quantityLength - splitLength - 20, GlobalSettings.QTY_DISPLAY_HEIGHT);
                    // If the Default language is English
                } else {
                    // Draw Company Logo
                    offsetDistance = xPos + GlobalSettings.RIGHT_SEPERATOR;
                    // offsetDistance = 100 + GlobalSettings.RIGHT_SEPERATOR;
                    textG.setFont(GlobalSettings.bigFont);
                    if (GlobalSettings.isSymbolShowing) {
                        textG.drawString(symbol, offsetDistance, GlobalSettings.SYMBOL_DISPLAY_HEIGHT);

                    } else
                        textG.drawString(acronym, offsetDistance, GlobalSettings.SYMBOL_DISPLAY_HEIGHT);
                    textG.setFont(GlobalSettings.smallFont);
                    textG.drawString("" + price, offsetDistance, GlobalSettings.DETAIL_DISPLAY_HEIGHT);  // + GlobalSettings.STOCK_SYMBOL_SIZE
                    offsetDistance = offsetDistance + GlobalSettings.RIGHT_SEPERATOR;
                    if (GlobalSettings.isChangeMode) {
                        textG.drawString("" + change, offsetDistance + perChangeDisplayOffset, GlobalSettings.DETAIL_DISPLAY_HEIGHT);
                    } else {
                        textG.drawString("" + percentChange + "%", offsetDistance + perChangeDisplayOffset, GlobalSettings.DETAIL_DISPLAY_HEIGHT);
                    }

                    textG.setColor(GlobalSettings.NO_CHANGE_COLOR);//this.upColor);
                    textG.drawString("" + quantity, offsetDistance - GlobalSettings.RIGHT_SEPERATOR, GlobalSettings.QTY_DISPLAY_HEIGHT);
                    if (splits > 1)
                        textG.drawString("(" + strSplits + ")", offsetDistance + 5 + quantityLength - GlobalSettings.RIGHT_SEPERATOR, GlobalSettings.QTY_DISPLAY_HEIGHT);
                }
            }
        }
    }

    /**
     * <P>Set the Message Details
     *
     * @param message - Message to be displayed
     * @param status  - Status of the message
     */
    public void setData(String message, String status) {
        isMessageMode = true;

        this.scrollMessage = message;
        this.statusOfMessage = status;

        // Calculate the message Length
        calculateMessageWidth();
    }

    /**
     * <P>Set the Trade Details
     *
     * @param symbol    - symbol
     * @param price     - the last trade price
     * @param quantity  - the last trade quantity
     * @param change    - amount of change
     * @param perChange - percent of changed
     * @param status    - Status of the transaction
     */
    public void setData(String symbol, String acronym, String price, String quantity,
                        String change, String perChange, int splits, int status, String exchange, int instrument) {
        isMessageMode = false;
        this.symbol = symbol;
        if ((acronym == null) || (acronym.length() == 0)) {
            acronym = symbol;
        }
        this.acronym = acronym;
        this.price = formatPriceField(price, exchange, symbol, instrument);
        this.quantity = formatField(quantity);
        this.percentChange = formatDecimalField(perChange);
        this.splits = splits;
        this.strSplits = formatField(splits);
        this.status = status;
        this.change = formatPriceField(change, exchange, symbol, instrument);
        this.lastTrPrice = Float.parseFloat(price);
        this.exchange = exchange;

        calculateTradeAreaWidth();          // Calculate the display area
        setMaxMinPrice(symbol, exchange, instrument);
    }

    private String formatPriceField(String sValue, String exchange, String symbol, int instrument) {
        float cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = new Float(sValue);
        }
        value = SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(cellValue);
        return value;
    }

    /**
     * <P>Do the Decimal Field formating for the relevant fields.
     */
    private String formatDecimalField(String sValue) {
        float cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = new Float(sValue);
        }
        value = floatFormat.format(cellValue);
        return value;
    }

    /**
     * <P>Do the Integer Field formating for the relevant fields.
     */
    private String formatField(String sValue) {
        float cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = (new Float(sValue)).floatValue();
        }
        value = intFormat.format(cellValue);
        return value;
    }

    private String formatField(int value) {

        return intFormat.format(value);
    }

    /**
     * Calculate the length of the Message
     */
    private void calculateMessageWidth() {
        mesgAreaWidth = GlobalSettings.symbolFm.stringWidth(this.scrollMessage) + GlobalSettings.LEFT_SEPERATOR;
    }

    /**
     * Calculate the length of the Trade Message
     */
    /*private void calculateTradeAreaWidth() {
        int aggOffset = 0;

        priceLength = GlobalSettings.smallFm.stringWidth(price);
        if (GlobalSettings.isChangeMode) {
            perChaLength = GlobalSettings.smallFm.stringWidth(change);
        } else {
            perChaLength = GlobalSettings.smallFm.stringWidth(percentChange) + GlobalSettings.smallFm.stringWidth("%");
        }
        changeLength = GlobalSettings.smallFm.stringWidth(change);
        quantityLength = GlobalSettings.smallFm.stringWidth(quantity);
        splitLength = GlobalSettings.smallFm.stringWidth("(" + strSplits + ")");

        perChangeDisplayOffset = priceLength; // + 10;
        aggOffset = perChangeDisplayOffset + perChaLength;

        // This has to be changed according to the displaying field
        if (GlobalSettings.isSymbolShowing)
            this.symbolLength = GlobalSettings.symbolFm.stringWidth(symbol); // + 10;
        else
            this.symbolLength = GlobalSettings.symbolFm.stringWidth(acronym); // + 10;

        if (symbolLength > aggOffset) {
            aggOffset = this.symbolLength;
        }

        tradeAreaWidth = aggOffset + GlobalSettings.RIGHT_SEPERATOR * 2 + +GlobalSettings.LEFT_SEPERATOR; // + space;
    }*/
    private void calculateTradeAreaWidth() {
        int aggOffset = 0;

        priceLength = GlobalSettings.smallFm.stringWidth(price);

        if (GlobalSettings.isChangeMode) {
            perChaLength = GlobalSettings.smallFm.stringWidth(change);
        } else {
            perChaLength = GlobalSettings.smallFm.stringWidth(percentChange) + GlobalSettings.smallFm.stringWidth("%");
        }
        changeLength = GlobalSettings.smallFm.stringWidth(change);
        quantityLength = GlobalSettings.smallFm.stringWidth(quantity);
        splitLength = GlobalSettings.smallFm.stringWidth("(" + strSplits + ")");

        perChangeDisplayOffset = priceLength; // + 10;
        aggOffset = perChangeDisplayOffset + perChaLength;
        totLen = perChangeDisplayOffset + perChaLength + GlobalSettings.smallFm.stringWidth("%") + GlobalSettings.smallFm.stringWidth("");

        // This has to be changed according to the displaying field
        if (GlobalSettings.isSymbolShowing)
            this.symbolLength = GlobalSettings.symbolFm.stringWidth(symbol); // + 10;
        else
            this.symbolLength = GlobalSettings.symbolFm.stringWidth(acronym); // + 10;

        if (symbolLength > aggOffset) {
            aggOffset = this.symbolLength;
        }
        if (symbolLength > totLen) {
            totLen = symbolLength;
        }
        if (!GlobalSettings.ENGLISH_VERSION) {
            if (totLen < (priceLength + perChaLength + 20)) {
                totLen = (priceLength + perChaLength + 20);
            }
        }

        tradeAreaWidth = aggOffset + GlobalSettings.RIGHT_SEPERATOR * 2 + +GlobalSettings.LEFT_SEPERATOR; // + space;
    }

    public int getElementLength() {
        if (isMessageMode)
            return this.mesgAreaWidth;
        else
            return this.tradeAreaWidth;
    }

    public int getEarlierXPos() {
        return earlierStartXPos; //this.earlierXPos;
    }

    public void setEarlierXPos(int newPos) {
        earlierStartXPos = newPos;
    }

    public int getScrollingDir() {
        return this.scrollingDir;
    }

    public void setScrollingDir(int newDir) {
        this.scrollingDir = newDir;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public int getDisplayStatus() {
        return displayStatus;
    }

    public boolean isMessageMode() {
        return isMessageMode;
    }

    public int getInsType() {
        return insType;
    }

    public String getExchange() {
        return exchange;
    }

    private void setMaxMinPrice(String symbol, String exchange, int instrument) {
        DataStore dtStore = DataStore.getSharedInstance();
        Stock stk = dtStore.getStockObject(exchange, symbol, instrument);
        try {
            max_Price = stk.getMaxPrice();
        } catch (Exception e) {
            max_Price = Double.MAX_VALUE;
        }
        try {
            min_Price = stk.getMinPrice();
        } catch (Exception e) {
            min_Price = Double.MIN_VALUE;
        }
    }

    /*private int getTotMessageLength(int symbolLength, int percentageOfset, int perChaLength, boolean lang) {
        if (lang) {
            if ((2 * percentageOfset + perChaLength) > symbolLength)
                return (2 * percentageOfset + perChaLength + 5);
            else
                return symbolLength;
        } else {
            if ((percentageOfset + 2 * perChaLength) > symbolLength)
                return (percentageOfset + 2 * perChaLength + 5);
            else
                return symbolLength;
        }

    }*/
}
