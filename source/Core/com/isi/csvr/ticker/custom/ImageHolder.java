/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */
package com.isi.csvr.ticker.custom;

import com.isi.csvr.ticker.GlobalSettings;

import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;
import java.util.StringTokenizer;

class ImageHolder implements Runnable {

    private static Hashtable imageHashtable;
    private static String logoList;
    private static JPanel parentFrame;
    private static GlobalSettings gs;

    /**
     * Constructor
     */
    //public ImageHolder(JFrame parentFrame, GlobalSettings gsIn) {
    public ImageHolder(JPanel parentFrame, GlobalSettings gsIn) {
        this.parentFrame = parentFrame;
        this.gs = gsIn;
        imageHashtable = new Hashtable(5, 1);
    }

    public static SmartImage getImage(String symbol) {
        SmartImage img = null;

        try {
            if (imageHashtable == null) {
                System.out.println("Null Hashtable Found!");
            }

            if (imageHashtable.containsKey(symbol.toUpperCase())) {
                img = (SmartImage) imageHashtable.get(symbol.toUpperCase());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            System.out.println("Error getting the image : " + e2);
        }
        return img;
    }

    private void loadImages() {
        //int idx = 0;
        String symbol = "";
        Image img = null;
        SmartImage smartImg = null;

        try {
            StringTokenizer st = new StringTokenizer(gs.IMAGE_LIST, ";");
            //System.out.println("Number of images : " + st.countTokens());
            while (st.hasMoreTokens()) {
                try {
                    symbol = st.nextToken();
                    img = parentFrame.getToolkit().getImage(gs.IMAGE_PATH + symbol + ".gif"); //new URL(this.applet.getCodeBase(), st.nextToken()); //gs.IMAGE_LIST[idx]); //
                    smartImg = new SmartImage(this.parentFrame);
                    smartImg.setImage(symbol);
                    imageHashtable.put(symbol.substring(4).toUpperCase(), smartImg);
                    //imageHashtable.put(symbol.substring(4).toUpperCase(), img);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error receiving Image : " + e);
                }
            }
            //System.out.println("Number of images : " + imageHashtable.size());
        } catch (Exception e2) {
            e2.printStackTrace();
            System.out.println("Error reading Config File : " + e2);
        }
    }

    public void run() {
        //loadImages();
    }

}