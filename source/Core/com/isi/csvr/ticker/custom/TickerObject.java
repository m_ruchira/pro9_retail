/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */
package com.isi.csvr.ticker.custom;

import com.isi.csvr.shared.SharedMethods;


public class TickerObject {
    // Trade details
    private String symbol;
    private String exchange;
    private int instrument;
    private String key;
    private double price;
    private long quantity;
    private double change;
    private double percentChange;
    private int status;
    private int splits;

    /**
     * Constructor
     */
    public TickerObject() {

    }

    public void setData(String exchange, String symbol, double price,
                        long quantity, double change, double perChange, int status, int splits, int instrument) {
        this.symbol = symbol;
        this.exchange = exchange;
        this.instrument = instrument;
        this.key = SharedMethods.getKey(exchange, symbol, instrument);
        this.price = price;
        this.quantity = quantity;
        this.change = change;
        this.percentChange = perChange;
        this.status = status;
        this.splits = splits;
//        ObjectWatcher.register(this);
    }

    public int getSplits() {
        return this.splits;
    }

    public double getPrice() {
        return this.price;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public String getKey() {
        return key;
    }

    public int getInstrument() {
        return instrument;
    }

    public long getQuantity() {
        return this.quantity;
    }

    public void setQuantity(long qty) {
        this.quantity += qty;
        splits++;
    }

    public double getChange() {
        return this.change;
    }

    public double getPercentChange() {
        return this.percentChange;
    }

    public int getStatus() {
        return this.status;
    }

    protected void finalize() throws Throwable {
//        ObjectWatcher.unregister(this);
        super.finalize();
    }

}