package com.isi.csvr.ticker.custom;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.NonResizeable;
import com.isi.csvr.ticker.GlobalSettings;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;

import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 24, 2006
 * Time: 10:33:25 AM
 */
public class TickerFrame extends InternalFrame implements NonResizeable {

    public static final String SYN_RESIZED = "RESIZED";
    public static boolean IS_DATA_COME = false;


    private static TickerFrame tFrame;

    public TickerFrame(String title, ViewSetting settings) {
        super(title, settings);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        super.hideTitleBarMenu();

        final Ticker t = new Ticker();
        this.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
//
                int tSize;

                if (Client.ISTICKERFIXED) {
                    System.out.println("ticker fixed");
                    if (SharedSettings.IS_ADVANCED_SELECTED) {
                        if (Ticker.IS_TITLE_SHOW) {
                            tSize = GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT;

                            if (PanelSetter.upperTicker) {
                                tSize = tSize + UpperPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.middleTicker) {
                                tSize = tSize + MiddlePanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.lowerTicker) {
                                tSize = tSize + LowerPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            setSize(getWidth(), tSize);
                        } else if (!Ticker.IS_TITLE_SHOW) {
                            tSize = 0;
                            if (PanelSetter.upperTicker) {
                                tSize = tSize + UpperPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.middleTicker) {
                                tSize = tSize + MiddlePanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.lowerTicker) {
                                tSize = tSize + LowerPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            setSize(getWidth(), tSize);
                        }
                    }

                } else if (!Client.ISTICKERFIXED) {
                    if (SharedSettings.IS_CUSTOM_SELECTED) {
                        if (!(getHeight() == GlobalSettings.TICKER_HEIGHT)) {
                            if (Ticker.IS_TITLE_SHOW) {
                                tSize = GlobalSettings.STOCK_TICKER_HEIGHT + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT;
                                setSize(getWidth(), tSize);
                                t.setterTickerSize(tSize);
                            } else if (!Ticker.IS_TITLE_SHOW) {
                                tSize = GlobalSettings.STOCK_TICKER_HEIGHT;
                                setSize(getWidth(), tSize);
                                t.setterTickerSize(tSize);
                            }
                        }
                    } else if (SharedSettings.IS_ADVANCED_SELECTED) {
                        if (Ticker.IS_TITLE_SHOW) {
                            tSize = GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT;

                            if (PanelSetter.upperTicker) {
                                tSize = tSize + UpperPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.middleTicker) {
                                tSize = tSize + MiddlePanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.lowerTicker) {
                                tSize = tSize + LowerPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            setSize(getWidth(), tSize);
                        } else if (!Ticker.IS_TITLE_SHOW) {
                            tSize = 0;
                            if (PanelSetter.upperTicker) {
                                tSize = tSize + UpperPanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.middleTicker) {
                                tSize = tSize + MiddlePanelSettings.INDIA_TICKER_HEIGHT;
                            }
                            if (PanelSetter.lowerTicker) {
                                tSize = tSize + LowerPanelSettings.INDIA_TICKER_HEIGHT;
                            }

                            setSize(getWidth(), tSize);
                        }
                    }
                }

                IS_DATA_COME = true;
//                synchronized (SYN_RESIZED) {
//                try {
                TradeTicker.Ticker_Width_Before = GlobalSettings.TICKER_WIDTH;
                TradeTicker.IS_TICKER_RESIZED = true;
                t.Ticker_ComponentResized(); //todo : Remove new instant and block recursive component resize
//                } catch (Exception e1) {
////                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                }
//                }
            }

            public void componentMoved(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void componentShown(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void componentHidden(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        this.addInternalFrameListener(new InternalFrameAdapter() {
            public void internalFrameClosing(InternalFrameEvent e) {
                if (SharedSettings.IS_CUSTOM_SELECTED) {
                    SharedSettings.IS_CUSTOM_SELECTED = false;
                    SharedSettings.selectCustomTicker(false);
                    SharedSettings.custom = SharedSettings.deselect;
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                        CommonSettings.enablingAdvancedTickers();
                    }
                } else if (SharedSettings.IS_ADVANCED_SELECTED) {
                    SharedSettings.IS_ADVANCED_SELECTED = false;
                    SharedSettings.selectAdvancedTicker(false);
                    SharedSettings.advanced = SharedSettings.deselect;
                    CommonSettings.enablingAdvancedTickers();
                }
            }
        });
    }

//    public void setVisible(boolean value) {
//        SharedMethods.printLine("Ticker frame test : " + value, true);
//        super.setVisible(value);
//    }

    public TickerFrame(TickerFrame tickerFrame) {
        tFrame = tickerFrame;

    }

    public static void setSizeOnFrame() {
        Dimension dim = new Dimension(0, 0);
        if (!Ticker.settings.isHeaderVisible()) {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
            }

        } else {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
            }

            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)));
        }
        HolderPanel.ADV_TICKER_WIDTH = (int) dim.getWidth();
        HolderPanel.ADV_TICKER_HEIGHT = (int) dim.getHeight() - 5;
        System.out.println("Ticker frame dim : " + dim.width + " , " + dim.height);
        setPrefferedSize(new Dimension(dim.width, dim.height));
    }

    public static void setPrefferedSize(Dimension dimension) {

    }

    public Dimension getPreferredSize() {

        Dimension dim = new Dimension(0, 0);
        if (!Ticker.settings.isHeaderVisible()) {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT - 5)));
            }
            return dim;

        } else {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
            }

            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)));
            return dim;
        }
    }

    public void resizeFrame() {
        try {
            Container np = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (SharedSettings.IS_CUSTOM_SELECTED) {
                if (np != null) {
                    setSize(getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                } else {
                    setSize(getWidth(), GlobalSettings.STOCK_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                }
            } else {
                switch (SharedSettings.Frame_Header_Changed_by) {
                    case 0:
                        if (np != null) {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                        } else {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                        }
                        break;
                    case 1:
                        if (Ticker.settings.isHeaderVisible()) {
                            if (np != null) {
                                setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                            } else {
                                setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                            }
                        } else {
                            if (np != null) {
                                setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                            } else {
                                setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                            }
                        }
                        break;

                }
                /*  if (SharedSettings.Frame_Header_Changed) {
                    if (np != null) {
                        setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                    } else {
                        setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                    }
                } else {
                    if (Ticker.settings.isHeaderVisible()) {
                        if (np != null) {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                        } else {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                        }
                    } else {
                        if (np != null) {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + np.getHeight() + getBorder().getBorderInsets(this).top * 2);
                        } else {
                            setSize(getWidth(), HolderPanel.ADV_TICKER_HEIGHT + getBorder().getBorderInsets(this).top * 2);
                        }
                    }
                }*/
//                SharedSettings.Frame_Header_Changed = false;
            }

            np = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
