/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */

package com.isi.csvr.ticker.custom;

import com.isi.csvr.ticker.GlobalSettings;

import javax.swing.*;
import java.awt.*;

public class SmartImage {

    private Image image;
    private int width;
    private MediaTracker tracker;
    //private JFrame          parentPanel;
    private JPanel parentPanel;

    /**
     * Constructor
     */
    public SmartImage(JPanel parentPanel) {
        this.parentPanel = parentPanel;
        tracker = new MediaTracker(parentPanel);
    }

    public int getWidth() {
        return this.width;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(String symbol) { // Image imageIn) {
        image = parentPanel.getToolkit().getImage(GlobalSettings.IMAGE_PATH + symbol + ".gif"); //new URL(this.applet.getCodeBase(), st.nextToken()); //gs.IMAGE_LIST[idx]); //
        tracker.addImage(image, 0);

        try {
            tracker.waitForID(0);
            width = image.getWidth(parentPanel);
        } catch (Exception e) {
            width = 0;
            e.printStackTrace();
            //return;
        }
//System.out.println("Image width after loading at SmartImage... : " + width);
    }

}