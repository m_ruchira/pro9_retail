package com.isi.csvr.ticker.advanced;

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;

import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 30, 2008
 * Time: 10:43:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpperAnnouncementsTickerUpdator extends Thread {

    public static boolean UPPER = false;
    public static boolean MIDDLE = false;
    public static boolean LOWER = false;
    private boolean active = true;
    private LinkedList dataStore;


    public UpperAnnouncementsTickerUpdator() {
        super("Summary UpperTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            UpperTickerFeeder.isQueueEmpty = true;
            UpperTickerFeeder.isAnnouncementModeSelected = false;
            UpperTickerFeeder.announcementQueue.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        while (active) {
            if (UpperTickerFeeder.announcementQueue != null && UpperTickerFeeder.announcementQueue.isEmpty()) {
                announcementModeSelectorLower();
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void announcementModeSelectorLower() {
        dataStore = AnnouncementStore.getSharedInstance().getLastAnnouncements();
        for (int i = 0; i < dataStore.size(); i++) {
            UpperTickerFeeder.addAnnouncementData((Announcement) dataStore.get(i));
        }
        UpperTickerFeeder.isAnnouncementModeSelected = true;
    }

}




