package com.isi.csvr.ticker.advanced;

import com.isi.csvr.TWFrame;
import com.isi.csvr.ticker.SharedSettings;

import javax.swing.*;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jul 18, 2008
 * Time: 9:33:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class PanelSetter {

    public static Hashtable<String, JCheckBox> checkBoxHash;
    public static JPanel tickerPanel;
    public static JPanel watchPanel;
    public static TWFrame frame;
    public static HolderPanel holderPanel = new HolderPanel();
    public static boolean upperTicker = false;
    public static boolean middleTicker = false;
    public static boolean lowerTicker = false;


    public PanelSetter() {
        //To change body of created methods use File | Settings | File Templates.
    }


    public PanelSetter(JPanel passingtickerPanel, JPanel watchP, TWFrame passinfF) {
        tickerPanel = passingtickerPanel;
        watchPanel = watchP;
        frame = passinfF;

        BoxLayout box1 = new BoxLayout(holderPanel, BoxLayout.PAGE_AXIS);
        holderPanel.setLayout(box1);

        holderPanel.add(UpperTickerPanel.getInstance());
        UpperTickerPanel.getInstance().setVisible(false);

        holderPanel.add(MiddleTickerPanel.getInstance());
        MiddleTickerPanel.getInstance().setVisible(false);

        holderPanel.add(LowerTickerPanel.getInstance());
        LowerTickerPanel.getInstance().setVisible(false);

    }


    public void setEquityPanel(boolean isShow) {
        if (isShow) {
            UpperTickerPanel.getInstance().setVisible(true);
            upperTicker = true;
        } else {
            UpperTickerPanel.getInstance().setVisible(false);
            upperTicker = false;
            if ((UpperPanelSettings.S_EQ_STATUS == 0) && (MiddlePanelSettings.S_EQ_STATUS == 0) && (LowerPanelSettings.S_EQ_STATUS == 0)) {
                SharedSettings.switchOff_Advanced_TIcker();
            }
        }
        holderPanel.updateUI();
        UpperTickerFeeder.setVisible(isShow);
        if (UpperPanelSettings.MODE == UpperPanelSettings.exchangelist) {
            UpperTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                UpperTickerFeeder.isExchangeModeSelected = true;
            }
        } else if (UpperPanelSettings.MODE == UpperPanelSettings.watchlist) {
            UpperTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                UpperTickerFeeder.isWatchListModeSelected = true;
            }
        } else if (UpperPanelSettings.MODE == UpperPanelSettings.anslist) {
            UpperTickerFeeder.activateAnnouncementtickerUpdator(isShow);
            if (isShow) {
                UpperTickerFeeder.isAnnouncementModeSelected = true;
            }
        } else if (UpperPanelSettings.MODE == UpperPanelSettings.newslist) {
            UpperTickerFeeder.activateNewstickerUpdator(isShow);
            if (isShow) {
                UpperTickerFeeder.isNewsModeSelected = true;
            }
        }
        watchPanel.updateUI();
        frame.repaint();
    }


    public void setDerivativesPanel(boolean isShow) {
        if (isShow) {
            MiddleTickerPanel.getInstance().setVisible(true);
            middleTicker = true;
        } else {
            MiddleTickerPanel.getInstance().setVisible(false);
            middleTicker = false;
            if ((UpperPanelSettings.S_EQ_STATUS == 0) && (MiddlePanelSettings.S_EQ_STATUS == 0) && (LowerPanelSettings.S_EQ_STATUS == 0)) {
                SharedSettings.switchOff_Advanced_TIcker();
            }
        }
        holderPanel.updateUI();

        MiddleTickerFeeder.setVisible(isShow);
        if (MiddlePanelSettings.MODE == MiddlePanelSettings.exchangelist) {
            MiddleTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                MiddleTickerFeeder.isExchangeModeSelected = true;
            }
        } else if (MiddlePanelSettings.MODE == MiddlePanelSettings.watchlist) {
            MiddleTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                MiddleTickerFeeder.isWatchListModeSelected = true;
            }
        } else if (MiddlePanelSettings.MODE == MiddlePanelSettings.anslist) {
            MiddleTickerFeeder.activateAnnouncementtickerUpdator(isShow);
            if (isShow) {
                MiddleTickerFeeder.isAnnouncementModeSelected = true;
            }
        } else if (MiddlePanelSettings.MODE == MiddlePanelSettings.newslist) {
            MiddleTickerFeeder.activateNewstickerUpdator(isShow);
            if (isShow) {
                MiddleTickerFeeder.isNewsModeSelected = true;
            }
        }
        watchPanel.updateUI();
        frame.repaint();
    }

    public void setIntergratedPanel(boolean isShow) {
        if (isShow) {
            LowerTickerPanel.getInstance().setVisible(true);
            lowerTicker = true;
        } else {
            LowerTickerPanel.getInstance().setVisible(false);

            lowerTicker = false;
            if ((UpperPanelSettings.S_EQ_STATUS == 0) && (MiddlePanelSettings.S_EQ_STATUS == 0) && (LowerPanelSettings.S_EQ_STATUS == 0)) {
                SharedSettings.switchOff_Advanced_TIcker();
            }
        }
        holderPanel.updateUI();

        LowerTickerFeeder.setVisible(isShow);
        if (LowerPanelSettings.MODE == LowerPanelSettings.exchangelist) {
            LowerTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                LowerTickerFeeder.isExchangeModeSelected = true;
            }
        } else if (LowerPanelSettings.MODE == LowerPanelSettings.watchlist) {
            LowerTickerFeeder.activateSummaryTickerUpdator(isShow);
            if (isShow) {
                LowerTickerFeeder.isWatchListModeSelected = true;
            }
        } else if (LowerPanelSettings.MODE == LowerPanelSettings.anslist) {
            LowerTickerFeeder.activateAnnouncementtickerUpdator(isShow);
            if (isShow) {
                LowerTickerFeeder.isAnnouncementModeSelected = true;
            }
        } else if (LowerPanelSettings.MODE == LowerPanelSettings.newslist) {
            LowerTickerFeeder.activateNewstickerUpdator(isShow);
            if (isShow) {
                LowerTickerFeeder.isNewsModeSelected = true;
            }
        }
        watchPanel.updateUI();
        frame.repaint();
    }
}
