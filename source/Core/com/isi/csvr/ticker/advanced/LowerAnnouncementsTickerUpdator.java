package com.isi.csvr.ticker.advanced;

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;

import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 15, 2008
 * Time: 4:35:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class LowerAnnouncementsTickerUpdator extends Thread {
    private boolean active = true;
    private LinkedList dataStore;

    public LowerAnnouncementsTickerUpdator() {
        super("Summary UpperTicker");
        start();

    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            LowerTickerFeeder.isQueueEmpty = true;
            LowerTickerFeeder.isAnnouncementModeSelected = false;
            LowerTickerFeeder.announcementQueue.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        while (active) {
            if (LowerTickerFeeder.announcementQueue != null && LowerTickerFeeder.announcementQueue.isEmpty()) {
                announcementModeSelectorLower();
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void announcementModeSelectorLower() {
        dataStore = AnnouncementStore.getSharedInstance().getLastAnnouncements();
        for (int i = 0; i < dataStore.size(); i++) {
            LowerTickerFeeder.addAnnouncementData((Announcement) dataStore.get(i));
        }
        LowerTickerFeeder.isAnnouncementModeSelected = true;
    }

}

