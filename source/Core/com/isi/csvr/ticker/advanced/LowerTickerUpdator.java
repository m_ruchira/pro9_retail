package com.isi.csvr.ticker.advanced;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 10, 2008
 * Time: 9:26:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class LowerTickerUpdator extends Thread {
    private boolean active = true;

    public LowerTickerUpdator() {
        super("Summary LowerTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            LowerTickerFeeder.isQueueEmpty = true;
            LowerTickerFeeder.isExchangeModeSelected = false;
            LowerTickerFeeder.tradeEQueue.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        int tickerStatus;
        boolean played = false;
        while (active) {
            Symbols[] symbols = LowerTickerFeeder.getFilter();
            try {
                if (LowerTickerFeeder.isVisible() && !(LowerTickerFeeder.isNewsModeSelected)) {
                    for (Symbols symbolsObj : symbols) {
                        if (symbolsObj != null) {

                            played = false;
                            String[] keys = symbolsObj.getSymbols();
                            for (String key : keys) {
                                try {
                                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                                    if ((stock != null && stock.isSymbolEnabled() && LowerFilterPanel.selectedExchangesForEx.contains(stock.getExchange())) || (LowerTickerFeeder.isWatchListModeSelected)) {
                                        Exchange exch = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key));
                                        //---- default load ------


                                        if ((LowerFilterPanel.showExTraded && LowerPanelSettings.MODE == LowerPanelSettings.exchangelist) || (LowerFilterPanel.showWatchListTraded && LowerPanelSettings.MODE == LowerPanelSettings.watchlist)) {
                                            if ((LowerFilterPanel.runExOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (LowerFilterPanel.runWatchListOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                if (stock.getLastTradeValue() > 0) {
//                                                if ((!exch.isDefault()) && stock.isLastTradeUpdatedUpper()) {
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown()))) {
//                                                if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradeValue() > 0) {
//                                                        stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        LowerTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedUpper();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            LowerTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
//                                                }
                                                }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN) || (exch.getMarketStatus() == Constants.TRADING_AT_LAST))) && stock.isLastTradeUpdatedLower()) { //---- Eliminated == || (exch.getMarketStatus() == Constants.OPEN))
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) {
                                                        stock.setLastTradeUpdatedLower();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        LowerTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedLower();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            LowerTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ((LowerFilterPanel.showExAll && LowerPanelSettings.MODE == LowerPanelSettings.exchangelist) || (LowerFilterPanel.showWatchListAll && LowerPanelSettings.MODE == LowerPanelSettings.watchlist)) {
                                            if ((LowerFilterPanel.runExOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (LowerFilterPanel.runWatchListOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                //                                            if ((!exch.isDefault())) { === Eliminated
                                                if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                    //                                                    stock.setLastTradeUpdatedUpper();
                                                    CommonTickerObject oTickerData = new CommonTickerObject();

                                                    if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                    }

                                                    LowerTickerFeeder.addData(oTickerData);
                                                    played = true;
                                                    delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                } else {
                                                    if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {
//                                                            (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                        //                                                        stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        LowerTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                    }
                                                }
                                                //                                            }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN) || (exch.getMarketStatus() == Constants.TRADING_AT_LAST)))) { //--- Eliminated --  || (exch.getMarketStatus() == Constants.OPEN) --Shanika
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                        //                                                    stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        LowerTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {
//                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            //                                                        stock.setLastTradeUpdatedUpper();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            LowerTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(LowerTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //----------------- end default --------------------

                                    }
                                    stock = null;
                                } catch (Exception e) {
                                    System.out.println("Error in key " + key); //todo fix the market code issue
                                    e.printStackTrace();
                                    delay(100);
                                }
                            }
                            if (!played) { // nothing played in this round
                                delay(1000);
                            }
                            keys = null;
                        }
                    }
                } else {
                    delay(2000);
                }
            } catch (Exception e) {
                delay(1000);
            }
            delay(LowerTickerFeeder.getSummaryTickerDelay());
        }
    }

    private void delay(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
