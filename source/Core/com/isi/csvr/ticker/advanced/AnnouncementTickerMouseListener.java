package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;
import com.isi.csvr.announcement.ANNOUNCMENT_TYPE;
import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.announcement.SearchedAnnouncementStore;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 17, 2008
 * Time: 4:28:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class AnnouncementTickerMouseListener extends MouseAdapter
        implements ListSelectionListener {

    public static final int ONLINE = 0;
    public static final int SEARCHED = 1;
    public static final int COMPANY = 2;
    public static boolean UPPER_SELECTED = false;
    public static boolean MIDDLE_SELECTED = false;
    public static boolean LOWER_SELECTED = false;
    public static String message;
    public static long time;
    public static String title;
    public static String symbol;
    public static String ansId;
    private JTable table;
    private ANNOUNCMENT_TYPE mode;

    public AnnouncementTickerMouseListener(JTable oTable, ANNOUNCMENT_TYPE mode) {
        this.table = oTable;
        this.mode = mode;
    }

    public AnnouncementTickerMouseListener(ANNOUNCMENT_TYPE all) {
        this.mode = all;

    }

    /**
     * Called from the mouse listener
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 0 && SwingUtilities.isLeftMouseButton(e)) {
            if ((LowerTickerFeeder.isAnnouncementModeSelected && LOWER_SELECTED) || (UpperTickerFeeder.isAnnouncementModeSelected && UPPER_SELECTED) || (MiddleTickerFeeder.isAnnouncementModeSelected && MIDDLE_SELECTED)) {
                selected();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Body");
                ansId = "";
            }
        }
    }

    /**
     * Called from the list selection listener
     */
    public void valueChanged(ListSelectionEvent e) {
        //selected();
    }

    public void selected() {
        Thread thread = new Thread("AnnouncementMouseListener-Select") {
            public void run() {

                if ((message == null) || ((message.equals("")))) {
                    String id = (String) ansId;
                    Announcement announcement;
                    String language;
                    if (mode == ANNOUNCMENT_TYPE.ALL) {
                        announcement = AnnouncementStore.getSharedInstance().getAnnouncement(id);
                        language = Language.getLanguageTag();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                        announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(id);
                        language = SearchedAnnouncementStore.getSharedInstance().getSelectedLanguage();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    } else {

                        announcement = AnnouncementStore.getSharedInstance().getSymbolAnnouncement(symbol, id);
                        language = Language.getLanguageTag();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    }

                    if (Settings.isConnected()) {
                        if (!id.equals("")) {
                            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_ANNOUNCEMENT);
                            SendQFactory.addAnnouncementBodyRequest(announcement.getExchange(), id, language);
                        }
                    }
                    announcement = null;
                } else {

                    String id = (String) ansId;
                    if (!id.equals("")) {
                        if (mode == ANNOUNCMENT_TYPE.ALL) {
                            AnnouncementStore.getSharedInstance().showAnnouncement(id);
                        } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                            SearchedAnnouncementStore.getSharedInstance().showAnnouncement(id);
                        } else {

                            AnnouncementStore.getSharedInstance().showSymbolAnnouncement(symbol, id);
                        }
                    }
                }
            }
        };
        thread.start();
    }
}
