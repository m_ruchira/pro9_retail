package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 20, 2008
 * Time: 10:09:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class ExchangesTickerMouseListener extends MouseAdapter
        implements ListSelectionListener {

    public static boolean UPPER_SELECTED = false;
    public static boolean MIDDLE_SELECTED = false;
    public static boolean LOWER_SELECTED = false;

    public static String exkey;

    public void valueChanged(ListSelectionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 0 && SwingUtilities.isLeftMouseButton(e)) {
            if (((LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) && LOWER_SELECTED) ||
                    ((MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) && MIDDLE_SELECTED) ||
                    ((UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) && UPPER_SELECTED)) {
                selected();
                exkey = "";
            }
        }
    }

    private void selected() {
        if (exkey != "") {
            Client.getInstance().showDetailQuoteExternal(exkey);
        }
    }
}
