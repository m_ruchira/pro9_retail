package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;
import com.isi.csvr.announcement.ANNOUNCMENT_TYPE;
import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.announcement.SearchedAnnouncementStore;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.news.*;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 30, 2008
 * Time: 9:08:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class LowerTickerPanel extends JPanel implements MouseListener {

    public final static int SCROLL_PAUSED = 0;
    public final static int SCROLL_TO_LEFT = 1;
    public final static int SCROLL_TO_RIGHT = 2;
    private static int scrollingDirection = SCROLL_TO_RIGHT;
    private static final int ROW_2_HEIGHT = 18;
    public static int BIG_FONT_STYLE = Font.BOLD;
    public static int SMALL_FONT_STYLE = Font.PLAIN;
    public static boolean isMouseOver = false;
    public static int xPos;
    public static int yPos;
    public static int FONT_BIG = 12;
    public static int FONT_SMALL = 11;
    public static int FONT_STYLE = Font.BOLD;
    public static String FONT_TYPE = "Helvetica";
    public static DynamicArray tradeIObjectList;
    public static DynamicArray tradeNewsObjectList;        // For News Ticker-------------------
    public static DynamicArray tradeAnnouncementsObjectList;
    public static int PIXEL_INCREMENT;
    public static Font bigFont;
    public static Font smallFont;
    public static FontMetrics smallFm;
    public static FontMetrics bigFm;
    public static boolean INT_DATA = false;
    public static String newsId;
    public static String message;
    public static long time;
    public static String title;
    public static String symbol;
    public static String ansId;
    public static String exkey;
    public static LowerTickerPanel self;
    private static LowerTickerFeeder intfeeder;
    private static Thread feederIntegratedThread;
    private static boolean isNewEntryAdded;
    Toolkit oToolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = oToolkit.getScreenSize();
    private int height = 34;
    private int sleepingInterval;    // Sets the sleeping time during displaying
    private BufferedImage textImage;
    private Graphics2D textG;
    private String newsmessage;
    private ANNOUNCMENT_TYPE mode = ANNOUNCMENT_TYPE.ALL;

    public LowerTickerPanel(String s) {
        //To change body of created methods use File | Settings | File Templates.
    }

    public LowerTickerPanel() {
        if (LowerTicker.CURRENT_TICKER_SPEED == 1)
            sleepingInterval = 6;

        else if (LowerTicker.CURRENT_TICKER_SPEED == 2)
            sleepingInterval = 3;

        else if (LowerTicker.CURRENT_TICKER_SPEED == 3)
            sleepingInterval = 1;

//        CommonSettings.TICKER_WIDTH_LOWER = (int) oToolkit.getScreenSize().getWidth();
        CommonSettings.TICKER_WIDTH_LOWER = (int) Client.getInstance().getDesktop().getSize().getWidth();

        this.addMouseListener(this);
        this.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
//        this.setBorder(null);

        this.setPreferredSize(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT));
        this.setOpaque(true);
        this.setBackground(LowerPanelSettings.TICKER_BACKGROUND);
        this.add(new JLabel("DDDDDDDDDDDDDDDDDDDDdd"));
        tradeIObjectList = new DynamicArray();
        tradeNewsObjectList = new DynamicArray();
        tradeAnnouncementsObjectList = new DynamicArray();

        if (LowerPanelSettings.ENGLISH_VERSION) {
            scrollingDirection = SCROLL_TO_LEFT;
        } else {
            scrollingDirection = SCROLL_TO_RIGHT;
        }
        adjustControls();
        fontAdjuster(this);
        initThread();

    }

    public static LowerTickerPanel getInstance() {
        if (self == null) {
            self = new LowerTickerPanel();
        }
        return self;
    }

    private static boolean getIsAdded() {
        return isNewEntryAdded;
    }//

    private static void setIsAdded(boolean flag) {
        isNewEntryAdded = flag;
    }// end method

    public static void reloadFont(JPanel parentPanel) {
        bigFont = new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.BIG_FONT_STYLE, LowerPanelSettings.FONT_BIG);
        smallFont = new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.SMALL_FONT_STYLE, LowerPanelSettings.FONT_SMALL);
        smallFm = parentPanel.getFontMetrics(new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.SMALL_FONT_STYLE, LowerPanelSettings.FONT_SMALL));
        bigFm = parentPanel.getFontMetrics(new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.BIG_FONT_STYLE, LowerPanelSettings.FONT_BIG));

        LowerPanelSettings.INDIA_TICKER_HEIGHT = bigFm.getHeight() + 10;
        LowerPanelSettings.INDIA_DRAW_HEIGHT = bigFm.getHeight();
    }

    public void setTickerSize() {
        this.setPreferredSize(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT));
    }

    public void setSleepInterval(int sleepInterval) {
        sleepingInterval = sleepInterval;
    }

    public void createNewETradeTicker(String exchange, String symbol, String price,
                                      String quantity, int status,
                                      Double change, String perChange,
                                      int splits, int instrument, String key) {
        if (!symbol.equals("")) {
            SymbolHolderLower lastObject = null;
            SymbolHolderLower tradeObject = new SymbolHolderLower();
            tradeObject.setData(symbol, intfeeder.getCompanyName(exchange, symbol, instrument), price, quantity, change, perChange, splits, status, exchange, instrument, key);

            if (Language.isLTR()) {
                scrollingDirection = SCROLL_TO_LEFT;
            } else {
                scrollingDirection = SCROLL_TO_RIGHT;
            }

            tradeObject.setScrollingDir(scrollingDirection);
            if (scrollingDirection == SCROLL_TO_LEFT) {
                if (tradeIObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeIObjectList.get(tradeIObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() < (CommonSettings.TICKER_WIDTH_LOWER - lastObject.getElementLength())) {
                        tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);   // not needed -- shanika
                    } else {

                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() + lastObject.getElementLength());    // till condition ok-- first drawing
                    }
                } else {
                    tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);            // default drawing - 1276
                }
            } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                if (tradeIObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeIObjectList.get(tradeIObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() > 0) {
                        tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() - tradeObject.getElementLength());
                    }
                } else {
                    tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
                }
            }

            tradeIObjectList.add(tradeObject);
            lastObject = null;
        }
        setIsAdded(true);
        drawTickers();
    }

    private void drawTickers() {

        int listLength = 0;
        int addedElemLength = 0;
        int counter = 0;
        if (getIsAdded()) {
            SymbolHolderLower readTradeObject = null;
            listLength = tradeIObjectList.size() - 1;
            readTradeObject = (SymbolHolderLower) tradeIObjectList.get(listLength);
            addedElemLength = readTradeObject.getElementLength();
            readTradeObject = null;
            LowerPanelSettings.CONTINUE_INT_LOOP = true;
            while ((counter < addedElemLength) &&
                    LowerPanelSettings.CONTINUE_INT_LOOP) {
                doScrolling(counter);

                if (LowerTickerFeeder.getQueue().size() > 0) { // 4 //todo changed
                    if (LowerTicker.CURRENT_TICKER_SPEED == 1) {
                        LowerTickerPanel.PIXEL_INCREMENT = 1;
                    } else if (LowerTicker.CURRENT_TICKER_SPEED == 2) {
                        LowerTickerPanel.PIXEL_INCREMENT = 2;
                    } else if (LowerTicker.CURRENT_TICKER_SPEED == 3) {
                        LowerTickerPanel.PIXEL_INCREMENT = 6;
                    }
                }

                try {
                    Thread.sleep(sleepingInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                counter = counter + LowerTickerPanel.PIXEL_INCREMENT;
            }
            setIsAdded(false);
        }
    }

    public void fontAdjuster(JPanel panel) {
        bigFont = new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.BIG_FONT_STYLE, LowerPanelSettings.FONT_BIG);
        smallFont = new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.SMALL_FONT_STYLE, LowerPanelSettings.FONT_SMALL);
        bigFm = panel.getFontMetrics(new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.BIG_FONT_STYLE, LowerPanelSettings.FONT_BIG));
        smallFm = panel.getFontMetrics(new TWFont(LowerPanelSettings.FONT_TYPE, LowerPanelSettings.SMALL_FONT_STYLE, LowerPanelSettings.FONT_SMALL));
    }

    public void initThread() {
        intfeeder = new LowerTickerFeeder(this);
        feederIntegratedThread = new Thread(intfeeder, "Lower Ticker Feeder");
        feederIntegratedThread.start();
//         feederIntegratedThread.setPriority(Thread.NORM_PRIORITY);
    }

    private void doScrolling(int counter) {
        int startingXPos = 0;        // Holds the starting X position
        int loopCounter = 0;        // Holds the loop Position
        int widthOfLast = 0;        // Holds the loop Position

        SymbolHolderLower readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // UpperTicker is not paused
            textG.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            textG.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, getHeight());

            textG.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            textG.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, getHeight() - 2);
        } else
            return;
        loopCounter = tradeIObjectList.size() - 1;

        while ((loopCounter >= 0) && LowerPanelSettings.CONTINUE_INT_LOOP) {
            readTradeObject = (SymbolHolderLower) tradeIObjectList.get(loopCounter);
            if (readTradeObject.getScrollingDir() == 1) {
                if (readTradeObject != null) {
                    if (CommonSettings.IS_INT_TICKER_RESIZED) {
                        int tickerGap = CommonSettings.Ticker_Width_Before - CommonSettings.TICKER_WIDTH_LOWER;
                        startingXPos = readTradeObject.getEarlierXPos() - tickerGap;
                    } else {
                        startingXPos = readTradeObject.getEarlierXPos();
                    }
                    readTradeObject.draw(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos - LowerTickerPanel.PIXEL_INCREMENT; //readTradeObject.getTradeAreaLength();
                    widthOfLast = readTradeObject.getElementLength();
                    if (startingXPos == -widthOfLast) {
                        tradeIObjectList.remove(loopCounter);

                    }
                }
            }
            if (readTradeObject.getScrollingDir() == 2) {
                if (readTradeObject != null) {
                    startingXPos = readTradeObject.getEarlierXPos();
                    readTradeObject.draw(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos + LowerTickerPanel.PIXEL_INCREMENT; //readTradeObject.getTradeAreaLength();
                    widthOfLast = readTradeObject.getElementLength();
                    if (startingXPos == CommonSettings.TICKER_WIDTH_LOWER) {
                        tradeIObjectList.remove(loopCounter);
                    }
                }
            }
            readTradeObject = null;
            loopCounter--;
        }
        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

    public synchronized void adjustControls() {
        try {
            textImage = null;
            textImage = new BufferedImage(CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT, BufferedImage.TYPE_INT_RGB);
            textG = textImage.createGraphics();
        } catch (Exception e) {
            System.out.println("Error at TradeTicker " + e);
            e.printStackTrace();
        }
    }

    public void adjustTickerControls() {
        this.setBounds(new Rectangle(0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT));
    }

    public void paint(Graphics g) {
        try {
            if (!INT_DATA) {
                g.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
                g.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT);
                g.setColor(LowerPanelSettings.TICKER_BACKGROUND);
                g.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, LowerPanelSettings.INDIA_TICKER_HEIGHT - 2);
            } else {
                g.drawImage(textImage, 0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Errors occurred while drawing Image " + e);
        }
    }

    public void update(Graphics g) {
        paint(g);
    }

    public void mouseClicked(MouseEvent e) {
        LowerTicker t = new LowerTicker();
        if (SwingUtilities.isRightMouseButton(e)) {
            t.tickersMouseActions(e, this);
        }

        if (e.getClickCount() > 0 && SwingUtilities.isLeftMouseButton(e)) {
            if (LowerTickerFeeder.isNewsModeSelected) {
                newsSelected();
//                newsId = "";
            } else if (LowerTickerFeeder.isAnnouncementModeSelected) {
                selected();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Body");
//                ansId = "";
            } else if (((LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected))) {
                exchangeselected();
//                exkey = "";
            }

        }
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        /* AnnouncementTickerMouseListener.UPPER_SELECTED = false;
      AnnouncementTickerMouseListener.MIDDLE_SELECTED = false;
      AnnouncementTickerMouseListener.LOWER_SELECTED = true;

      NewsTickerMouseListener.UPPER_SELECTED = false;
      NewsTickerMouseListener.MIDDLE_SELECTED = false;
      NewsTickerMouseListener.LOWER_SELECTED = true;

      ExchangesTickerMouseListener.UPPER_SELECTED = false;
      ExchangesTickerMouseListener.MIDDLE_SELECTED = false;
      ExchangesTickerMouseListener.LOWER_SELECTED = true;*/

        /*  LowerTickerPanel.exkey = "";
      LowerTickerPanel.ansId = "";
      LowerTickerPanel.newsId = "";*/

        if (LowerTickerFeeder.isNewsModeSelected) {
            isMouseOver = true;
            xPos = e.getX();
            yPos = e.getY();
            SymbolHolderLower.isMouseOverSymbol = true;

            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
//            selectionListener = new NewsTickerMouseListener(NEWS_TYPES.ALL);
//            addMouseListener(selectionListener);
        } else if (LowerTickerFeeder.isAnnouncementModeSelected) {
            isMouseOver = true;
            xPos = e.getX();
            yPos = e.getY();
            SymbolHolderLower.isMouseOverSymbol = true;

            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

            /* ansSelectionListener = new AnnouncementTickerMouseListener(ANNOUNCMENT_TYPE.ALL);
            addMouseListener(ansSelectionListener);*/
        } else if (LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) {
            isMouseOver = true;
            xPos = e.getX();
            yPos = e.getY();
            SymbolHolderLower.isMouseOverSymbol = true;
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            /*  exSelectionListener = new ExchangesTickerMouseListener();
            addMouseListener(exSelectionListener);*/
        }
    }

    public void mouseExited(MouseEvent e) {
        SymbolHolderLower.isScrolable = true;
        LowerTickerPanel.isMouseOver = false;
        SymbolHolderLower.isMouseOverSymbol = false;
    }

    public void reloadFontforExchange() {
        int xPos = 0;
        SymbolHolderLower tradeObject = null;

        try {
            tradeObject = (SymbolHolderLower) tradeIObjectList.get(tradeIObjectList.size() - 1);

            tradeObject.revalidateExchange();
            xPos = tradeObject.getEarlierXPos();
            if (scrollingDirection == SCROLL_TO_LEFT) {
                xPos = this.getWidth() - tradeObject.getElementLength();
                tradeObject.setEarlierXPos(xPos);
            } else {
                xPos = 0;
                tradeObject.setEarlierXPos(xPos);
            }
            if (tradeIObjectList.size() > 1) {
                for (int i = tradeIObjectList.size() - 2; i >= 0; i--) {
                    tradeObject = (SymbolHolderLower) tradeIObjectList.get(i);
                    tradeObject.revalidateExchange();
                    if (scrollingDirection == SCROLL_TO_LEFT) {
                        xPos -= tradeObject.getElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos); //UpperPanelSettings.TICKER_WIDTH_LOWER);
                    } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                        xPos += tradeObject.getElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos); //GlobalSettings.TICKER_WIDTH_LOWER);
                    }
                    tradeObject = null;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        LowerPanelSettings.CONTINUE_INT_LOOP = false;
    }

    public void reloadFontforAnnouncements() {
        int xPos = 0;
        SymbolHolderLower tradeObject = null;
        try {
            tradeObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(tradeAnnouncementsObjectList.size() - 1);
            tradeObject.revalidateAnounce();
            xPos = tradeObject.getEarlierXPos();
            if (scrollingDirection == 1) {
                xPos = this.getWidth() - tradeObject.getAnnouncementElementLength();
                tradeObject.setEarlierXPos(xPos);
            } else {
                xPos = 0;
                tradeObject.setEarlierXPos(xPos);
            }
            if (tradeAnnouncementsObjectList.size() > 1) {
                for (int i = tradeAnnouncementsObjectList.size() - 2; i >= 0; i--) {
                    tradeObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(i);
                    tradeObject.revalidateAnounce();
                    if (scrollingDirection == 1) {
                        xPos -= tradeObject.getAnnouncementElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos);
                    } else if (scrollingDirection == 2) {
                        xPos += tradeObject.getAnnouncementElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos);
                    }
                    tradeObject = null;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        LowerPanelSettings.CONTINUE_INT_LOOP = false;
    }

    public void reloadFontforNews() {
        int xPos = 0;
        SymbolHolderLower tradeObject = null;
        try {
            tradeObject = (SymbolHolderLower) tradeNewsObjectList.get(tradeNewsObjectList.size() - 1);
            tradeObject.revalidateNews();
            xPos = tradeObject.getEarlierXPos();
            if (scrollingDirection == 1) {
                xPos = this.getWidth() - tradeObject.getNewsElementLength();
                tradeObject.setEarlierXPos(xPos);
            } else {
                xPos = 0; //UpperPanelSettings.TICKER_WIDTH_LOWER - tradeObject.getElementLength();
                tradeObject.setEarlierXPos(xPos);
            }
            if (tradeNewsObjectList.size() > 1) {
                for (int i = tradeNewsObjectList.size() - 2; i >= 0; i--) {
                    tradeObject = (SymbolHolderLower) tradeNewsObjectList.get(i);
                    tradeObject.revalidateNews();
                    if (scrollingDirection == 1) {
                        xPos -= tradeObject.getNewsElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos);
                    } else if (scrollingDirection == 2) {
                        xPos += tradeObject.getNewsElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos);
                    }

                    tradeObject = null;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        LowerPanelSettings.CONTINUE_INT_LOOP = false;
    }

    private void stayStill(int counter, Graphics g) {// synchronized

        int loopCounter = 0;            // Holds the loop Position
        int startingXPosition = 0;      // Holds the starting X position
        int widthOfLast = 0;
        SymbolHolderLower readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {
            g.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            g.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT);
            g.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            g.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, LowerPanelSettings.INDIA_TICKER_HEIGHT - 2);
        }

        loopCounter = tradeIObjectList.size() - 1;
        while (loopCounter >= 0) {
            readTradeObject = (SymbolHolderLower) tradeIObjectList.get(loopCounter);
            if (readTradeObject != null) {
                if (scrollingDirection == SCROLL_TO_LEFT) {
                    if (CommonSettings.IS_INT_TICKER_RESIZED) {
                        int tickerGap = CommonSettings.Ticker_Width_Before - CommonSettings.TICKER_WIDTH_LOWER;
                        startingXPosition = readTradeObject.getEarlierXPos() - tickerGap;
                    } else {
                        startingXPosition = readTradeObject.getEarlierXPos();
                    }
                    readTradeObject.draw(startingXPosition, scrollingDirection, textG);
                    widthOfLast = readTradeObject.getElementLength();
                    if (startingXPosition == -widthOfLast) {
                        tradeIObjectList.remove(loopCounter);

                    } else if (readTradeObject.getDisplayStatus() == 2) {
                        tradeIObjectList.remove(loopCounter);
                    }
                }
                if (scrollingDirection == SCROLL_TO_RIGHT) {
                    startingXPosition = readTradeObject.getEarlierXPos();
                    readTradeObject.draw(startingXPosition, scrollingDirection, textG);
                }
            }
            readTradeObject = null;
            loopCounter--;
        }

        setTickerSize();

        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

    public void redrawTicker() {
        try {
            if (LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) {
                stayStill(0, textG);
            } else if (LowerTickerFeeder.isNewsModeSelected) {
                stayStillForNews(0, textG);
            } else if (LowerTickerFeeder.isAnnouncementModeSelected) {
                stayStillForAnnouncements(0, textG);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

// ======================== For News Ticker ======================================

    public void createNewsTicker(String newsProvider, String symnewssymbol, String exchange, String newsID, long newsDate,
                                 String language, String headLine, String body, String keywords, String source, int instrumentType) {

        if (!headLine.equals("")) {
            SymbolHolderLower lastObject = null;
            SymbolHolderLower tradeObject = new SymbolHolderLower();

            tradeObject.setNewsData(newsProvider, symnewssymbol, exchange, newsID, newsDate,
                    language, headLine, body, keywords, source, instrumentType);

            if (Language.isLTR()) {
                scrollingDirection = SCROLL_TO_LEFT;
            } else {
                scrollingDirection = SCROLL_TO_RIGHT;
            }
            tradeObject.setScrollingDir(scrollingDirection);
            if (scrollingDirection == SCROLL_TO_LEFT) {
                if (tradeNewsObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeNewsObjectList.get(tradeNewsObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() < (CommonSettings.TICKER_WIDTH_LOWER - lastObject.getNewsElementLength())) {
                        tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);   // not needed -- shanika
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() + lastObject.getNewsElementLength());    // till condition ok-- first drawing
                    }
                } else {
                    tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);            // default drawing - 1276
                }
            } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                if (tradeNewsObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeNewsObjectList.get(tradeNewsObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() > 0) {
                        tradeObject.setEarlierXPos(0 - tradeObject.getNewsElementLength());
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() - tradeObject.getNewsElementLength());
                    }
                } else {
                    tradeObject.setEarlierXPos(0 - tradeObject.getNewsElementLength());
                }
            }

            tradeNewsObjectList.add(tradeObject);
            lastObject = null;
        }
        setIsAdded(true);
        drawNewsTickers();
    }

    private void drawNewsTickers() {

        int listLength = 0;
        int addedElemLength = 0;
        int counter = 0;
        if (getIsAdded()) {
            SymbolHolderLower readTradeObject = null;
            listLength = tradeNewsObjectList.size() - 1;
            readTradeObject = (SymbolHolderLower) tradeNewsObjectList.get(listLength);
            addedElemLength = readTradeObject.getNewsElementLength();
            readTradeObject = null;
            LowerPanelSettings.CONTINUE_INT_LOOP = true;
            while ((counter < addedElemLength) &&
                    LowerPanelSettings.CONTINUE_INT_LOOP) {
                doScrollingForNews(counter);

                try {
                    Thread.sleep(sleepingInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                counter = counter + 1;
            }
            setIsAdded(false);
        }
    }

    private void doScrollingForNews(int counter) {
        int startingXPos = 0;        // Holds the starting X position
        int loopCounter = 0;        // Holds the loop Position
        int widthOfLast = 0;        // Holds the loop Position

        SymbolHolderLower readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // UpperTicker is not paused
            textG.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            textG.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, getHeight());

            textG.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            textG.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, getHeight() - 2);

        } else
            return;
        loopCounter = tradeNewsObjectList.size() - 1;

        while ((loopCounter >= 0) && LowerPanelSettings.CONTINUE_INT_LOOP) {
            readTradeObject = (SymbolHolderLower) tradeNewsObjectList.get(loopCounter);
            if (readTradeObject != null) {
                if (readTradeObject.getScrollingDir() == 1) {
                    startingXPos = readTradeObject.getEarlierXPos();
                    readTradeObject.drawNews(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos - 1; //readTradeObject.getTradeAreaLength();
                    widthOfLast = readTradeObject.getNewsElementLength();
                    if (startingXPos == -widthOfLast) {
                        tradeNewsObjectList.remove(loopCounter);
                    }
                }
                if (readTradeObject.getScrollingDir() == 2) {
                    startingXPos = readTradeObject.getEarlierXPos();
                    readTradeObject.drawNews(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos + 1; //readTradeObject.getTradeAreaLength();
                    if ((startingXPos) == (CommonSettings.TICKER_WIDTH_LOWER)) {
                        tradeNewsObjectList.remove(loopCounter);
                    }
                }
            }
            readTradeObject = null;
            loopCounter--;
        }
        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

    private void stayStillForNews(int counter, Graphics g) {// synchronized

        int loopCounter = 0;            // Holds the loop Position
        int startingXPosition = 0;      // Holds the starting X position
        int widthOfLast = 0;
        SymbolHolderLower readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // UpperTicker is not paused
            g.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            g.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT);
            g.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            g.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, LowerPanelSettings.INDIA_TICKER_HEIGHT - 2);
        }

        loopCounter = tradeNewsObjectList.size() - 1;
        while (loopCounter >= 0) {
            readTradeObject = (SymbolHolderLower) tradeNewsObjectList.get(loopCounter);
            if (readTradeObject != null) {

                if (scrollingDirection == SCROLL_TO_LEFT) {
                    if (CommonSettings.IS_INT_TICKER_RESIZED) {
                        int tickerGap = CommonSettings.Ticker_Width_Before - CommonSettings.TICKER_WIDTH_LOWER;
                        startingXPosition = readTradeObject.getEarlierXPos() - tickerGap;
                    } else {
                        startingXPosition = readTradeObject.getEarlierXPos();
                    }
                    readTradeObject.drawNews(startingXPosition, scrollingDirection, textG);

                    widthOfLast = readTradeObject.getNewsElementLength();
                    if (startingXPosition == -widthOfLast) {
                        tradeNewsObjectList.remove(loopCounter);

                    } else if (readTradeObject.getDisplayStatus() == 2) {
                        tradeNewsObjectList.remove(loopCounter);
                    }

                } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                    startingXPosition = readTradeObject.getEarlierXPos();
                    readTradeObject.drawNews(startingXPosition, counter, textG);
                }
            }
            readTradeObject = null;
            loopCounter--;
        }
        setTickerSize();
        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

// ================================================== For Announcement Ticker ==================================

    public void createAnnounementTicker(String ex, String symbol, int ins, String key, String no, String lang, String headline,
                                        String message, String url) {

        if (!headline.equals("")) {
            SymbolHolderLower lastObject = null;
            SymbolHolderLower tradeObject = new SymbolHolderLower();
            tradeObject.setAnnouncementData(ex, symbol, ins, key, no, lang, headline,
                    message, url);
            if (Language.isLTR()) {
                scrollingDirection = SCROLL_TO_LEFT;
            } else {
                scrollingDirection = SCROLL_TO_RIGHT;
            }
            tradeObject.setScrollingDir(scrollingDirection);


            if (scrollingDirection == SCROLL_TO_LEFT) {
                if (tradeAnnouncementsObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(tradeAnnouncementsObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() < (CommonSettings.TICKER_WIDTH_LOWER - lastObject.getAnnouncementElementLength())) {
                        // this is where xposition again begin with 1276
                        tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);   // not needed -- shanika
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() + lastObject.getAnnouncementElementLength());    // till condition ok-- first drawing
                    }
                } else {
                    tradeObject.setEarlierXPos(CommonSettings.TICKER_WIDTH_LOWER);            // default drawing - 1276
                }
            } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                if (tradeAnnouncementsObjectList.size() > 0) {
                    lastObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(tradeAnnouncementsObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() > 0) {
                        tradeObject.setEarlierXPos(0 - tradeObject.getAnnouncementElementLength());
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() - tradeObject.getAnnouncementElementLength());
                    }
                } else {

                    tradeObject.setEarlierXPos(0 - tradeObject.getAnnouncementElementLength());
                }
            }
            tradeAnnouncementsObjectList.add(tradeObject);
            lastObject = null;
        }
        setIsAdded(true);
        drawAnnouncementsTickers();
    }

    private void drawAnnouncementsTickers() {

        int listLength = 0;
        int addedElemLength = 0;
        int counter = 0;
        if (getIsAdded()) {
            SymbolHolderLower readTradeObject = null;
            listLength = tradeAnnouncementsObjectList.size() - 1;
            readTradeObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(listLength);
            addedElemLength = readTradeObject.getAnnouncementElementLength();
            readTradeObject = null;
            LowerPanelSettings.CONTINUE_INT_LOOP = true;
            while ((counter < addedElemLength) &&
                    LowerPanelSettings.CONTINUE_INT_LOOP) {
                doScrollingForAnnouncement(counter);

                try {
                    Thread.sleep(sleepingInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                counter = counter + 1;
            }
            setIsAdded(false);
        }
    }

    private void doScrollingForAnnouncement(int counter) {
        int startingXPos = 0;        // Holds the starting X position
        int loopCounter = 0;        // Holds the loop Position
        int widthOfLast = 0;        // Holds the loop Position
        SymbolHolderLower readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // UpperTicker is not paused
            //textImage.flush();
            textG.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            textG.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, getHeight());

            textG.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            textG.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, getHeight() - 2);
        } else
            return;
        loopCounter = tradeAnnouncementsObjectList.size() - 1;

        while ((loopCounter >= 0) && LowerPanelSettings.CONTINUE_INT_LOOP) {
            readTradeObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(loopCounter);
            if (readTradeObject.getScrollingDir() == 1) {
                if (readTradeObject != null) {
                    if (CommonSettings.IS_INT_TICKER_RESIZED) {
                        int tickerGap = CommonSettings.Ticker_Width_Before - CommonSettings.TICKER_WIDTH_LOWER;
                        startingXPos = readTradeObject.getEarlierXPos() - tickerGap;
                    } else {
                        startingXPos = readTradeObject.getEarlierXPos();
                    }
                    readTradeObject.drawAnnouncements(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos - 1; //readTradeObject.getTradeAreaLength();
                    widthOfLast = readTradeObject.getAnnouncementElementLength();
                    if (startingXPos == -widthOfLast) {
                        tradeAnnouncementsObjectList.remove(loopCounter);
                    }
                }
            }
            if (readTradeObject.getScrollingDir() == 2) {
                if (readTradeObject != null) {
                    startingXPos = readTradeObject.getEarlierXPos();
                    readTradeObject.drawAnnouncements(startingXPos, scrollingDirection, textG);
                    startingXPos = startingXPos + 1; //readTradeObject.getTradeAreaLength();
                    widthOfLast = readTradeObject.getAnnouncementElementLength();
                    if (startingXPos == CommonSettings.TICKER_WIDTH_LOWER) {
                        tradeAnnouncementsObjectList.remove(loopCounter);
                    }
                }
            }
            readTradeObject = null;
            loopCounter--;
        }
        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

    private void stayStillForAnnouncements(int counter, Graphics g) {// synchronized
        int loopCounter = 0;            // Holds the loop Position
        int startingXPosition = 0;      // Holds the starting X position
        int widthOfLast = 0;
        SymbolHolderLower readTradeObject = null;
        if (scrollingDirection != SCROLL_PAUSED) {  // UpperTicker is not paused
            g.setColor(LowerPanelSettings.TICKER_BORDER_COLOR);
            g.fillRect(0, 0, CommonSettings.TICKER_WIDTH_LOWER, LowerPanelSettings.INDIA_TICKER_HEIGHT);
            g.setColor(LowerPanelSettings.TICKER_BACKGROUND);
            g.fillRect(1, 1, CommonSettings.TICKER_WIDTH_LOWER - 2, LowerPanelSettings.INDIA_TICKER_HEIGHT - 2);
        }
        loopCounter = tradeAnnouncementsObjectList.size() - 1;
        while (loopCounter >= 0) {
            readTradeObject = (SymbolHolderLower) tradeAnnouncementsObjectList.get(loopCounter);
            if (readTradeObject != null) {
                if (scrollingDirection == SCROLL_TO_LEFT) {
                    if (CommonSettings.IS_INT_TICKER_RESIZED) {
                        int tickerGap = CommonSettings.Ticker_Width_Before - CommonSettings.TICKER_WIDTH_LOWER;
                        startingXPosition = readTradeObject.getEarlierXPos() - tickerGap;
                    } else {
                        startingXPosition = readTradeObject.getEarlierXPos();
                    }
                    readTradeObject.drawAnnouncements(startingXPosition, scrollingDirection, textG);
                    widthOfLast = readTradeObject.getAnnouncementElementLength();
                    if (startingXPosition == -widthOfLast) {
                        tradeAnnouncementsObjectList.remove(loopCounter);
                    } else if (readTradeObject.getDisplayStatus() == 2) {
                        tradeAnnouncementsObjectList.remove(loopCounter);
                    }
                }
                if (scrollingDirection == SCROLL_TO_RIGHT) {
                    startingXPosition = readTradeObject.getEarlierXPos();
                    readTradeObject.drawAnnouncements(startingXPosition, scrollingDirection, textG);
                }
            }
            readTradeObject = null;
            loopCounter--;
        }
        setTickerSize();
        CommonSettings.IS_INT_TICKER_RESIZED = false;
        CommonSettings.IS_INT_DATA_COME = false;
        repaint();
    }

    public void newsSelected() {
        Thread thread = new Thread("NewsMouseListener-Select") {
            public void run() {
                if ((newsmessage == null) || ((newsmessage.equals("")))) {
                    String id = (String) newsId;
//                        String providerID = (String) table.getModel().getValueAt(table.getSelectedRow(), 2);

                    News news = null;
                    String language = null;

                    if (!id.equals("")) {
                        news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                        }
                    } else {
                        System.out.println("--this id--" + id + "---news id ---" + newsId);
                    }
                    if (!id.equals("")) {
                        language = news.getLanguage();
                        if (Settings.isConnected()) {
                            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_NEWS);
//                                NewsProvider np = (NewsProvider)NewsProvidersStore.getSharedInstance().getProvider(providerID.toUpperCase());
                            NewsProvider np = NewsProvidersStore.getSharedInstance().getProvider(news.getDefaultCategory());
                            Enumeration<String> providers = NewsProvidersStore.getSharedInstance().getProviderKeys();
                            while (providers.hasMoreElements()) {
                                String provider = providers.nextElement();
                                if (news.isContainInCategory(provider)) {
                                    np = NewsProvidersStore.getSharedInstance().getProvider(provider);
                                    break;
                                }
                            }
                            Client.getInstance().getBrowserFrame().setTitle(Language.getString("NEWS"));
//                                ServerSupportedNewsProvider ssnp = NewsProvidersStore.getSharedInstance().ssnProviders.get(getRealID(providerID));
                            if (np == null) {
                                SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, null, Constants.CONTENT_PATH_SECONDARY);  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                            } else {
                                SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, np.getContentIp(), np.getPath());  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                            }
                        }
                        news = null;
                    } else {
                        System.out.println("==news id null");
                    }
                } else {
                    String id = (String) newsId;
                    if (!id.equals("")) {
                        News news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        } else {
                            NewsStore.getSharedInstance().showNews(id);
                        }
                        /*if (mode == NEWS_TYPES.ALL) {
                            NewsStore.getSharedInstance().showNews(id);
                        } else if (mode == NEWS_TYPES.SEARCHED) {
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        }*/
                    }
                }
            }
        };
        thread.start();
//        }
    }

    public void selected() {
        Thread thread = new Thread("AnnouncementMouseListener-Select") {
            public void run() {

                if ((message == null) || ((message.equals("")))) {
                    String id = (String) ansId;
                    Announcement announcement;
                    String language;
                    if (mode == ANNOUNCMENT_TYPE.ALL) {
                        announcement = AnnouncementStore.getSharedInstance().getAnnouncement(id);
                        language = Language.getLanguageTag();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                        announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(id);
                        language = SearchedAnnouncementStore.getSharedInstance().getSelectedLanguage();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    } else {

                        announcement = AnnouncementStore.getSharedInstance().getSymbolAnnouncement(symbol, id);
                        language = Language.getLanguageTag();
                        if ((language == null) || (language.equals(""))) {
                            language = Language.getLanguageTag();
                        }
                    }

                    if (Settings.isConnected()) {
                        if (!id.equals("")) {
                            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_ANNOUNCEMENT);
                            SendQFactory.addAnnouncementBodyRequest(announcement.getExchange(), id, language);
                        }
                    }
                    announcement = null;
                } else {

                    String id = (String) ansId;
                    if (!id.equals("")) {
                        if (mode == ANNOUNCMENT_TYPE.ALL) {
                            AnnouncementStore.getSharedInstance().showAnnouncement(id);
                        } else if (mode == ANNOUNCMENT_TYPE.SEARCHED) {
                            SearchedAnnouncementStore.getSharedInstance().showAnnouncement(id);
                        } else {

                            AnnouncementStore.getSharedInstance().showSymbolAnnouncement(symbol, id);
                        }
                    }
                }
            }
        };
        thread.start();
    }

    private void exchangeselected() {
        if (exkey != "") {
            Client.getInstance().showDetailQuoteExternal(exkey);
        }
    }
}
