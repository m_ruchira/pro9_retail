package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;
import com.isi.csvr.DetachableRootPane;
import com.isi.csvr.TWCheckBoxMenuItem;
import com.isi.csvr.TWMenu;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWTypes;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TickerFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 22, 2008
 * Time: 11:47:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommonSettings implements MouseListener, ExchangeListener, ApplicationListener {

    public static final String EQ_SYN_RESIZED = "RESIZED";
    public static final String DERI_SYN_RESIZED = "RESIZED";
    public static final String INT_SYN_RESIZED = "RESIZED";
    public static int Ticker_Width_Before;
    public static boolean IS_EQ_DATA_COME = false;
    public static boolean IS_DERI_DATA_COME = false;
    public static boolean IS_INT_DATA_COME = false;

    public static boolean IS_EQ_TICKER_RESIZED = false;
    public static boolean IS_DERI_TICKER_RESIZED = false;
    public static boolean IS_INT_TICKER_RESIZED = false;
    public static TWCheckBoxMenuItem radioE;
    public static TWCheckBoxMenuItem radioD;
    public static TWCheckBoxMenuItem radioI;

    /*public static int TICKER_WIDTH_LOWER = 600;
    public static int TICKER_WIDTH_MIDDLE = 600;
    public static int TICKER_WIDTH_UPPER = 600;*/
    public static int TICKER_WIDTH_LOWER = (int) Client.getInstance().getDesktop().getSize().getWidth();

    public static boolean IS_EQ_SELECTED = false;
    public static boolean IS_DERI_SELECTED = false;
    public static boolean IS_INT_SELECTED = false;

    public static int EQ_TIK_UP = 1;
    public static int EQ_STATUS = EQ_TIK_UP;
    public static int EQ_TIK_DWN = 0;
    public static int DERI_TIK_UP = 1;
    public static int DERI_STATUS = DERI_TIK_UP;
    public static int DERI_TIK_DWN = 0;
    public static int INT_TIK_UP = 1;
    public static int INT_STATUS = INT_TIK_UP;
    public static int INT_TIK_DWN = 0;
    public static boolean EQ_UP = false;
    public static boolean DERI_UP = false;
    public static boolean INT_UP = false;
    public static int selectedPanelEq = 1;
    public static int selectedPanelDeri = 2;
    public static int selectedPanelInt = 3;
    public static boolean IS_EQ_SET_LOD = false;
    public static boolean IS_DERI_SET_LOD = false;
    public static boolean IS_INT_SET_LOD = false;
    public static int panelSelected = 0;

    public static TWMenu ticker;

    static PanelSetter panelSet = new PanelSetter();

    public CommonSettings() {
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);

    }

    public static void adjustTickerSize(int width) { // synchronized
        if (width > 0) {
            TICKER_WIDTH_LOWER = width;// - MENU_AREA_WIDTH;

        } else {
//            TICKER_WIDTH_LOWER = 1366;// - MENU_AREA_WIDTH;
            TICKER_WIDTH_LOWER = (int) Client.getInstance().getDesktop().getSize().getWidth();// - MENU_AREA_WIDTH;
        }
        CommonSettings.IS_EQ_TICKER_RESIZED = true;
        UpperTickerPanel.getInstance().adjustControls();
        UpperTickerPanel.getInstance().redrawTicker();

        CommonSettings.IS_DERI_TICKER_RESIZED = true;
        MiddleTickerPanel.getInstance().adjustControls();
        MiddleTickerPanel.getInstance().redrawTicker();

        CommonSettings.IS_INT_TICKER_RESIZED = true;
        LowerTickerPanel.getInstance().adjustControls();
        LowerTickerPanel.getInstance().redrawTicker();
    }

    public static void setTicker(TWMenu ticker) {
        CommonSettings.ticker = ticker;
    }

    public static void enablingAdvancedTickers() {
        if (SharedSettings.IS_ADVANCED_SELECTED) {
            if (!CommonSettings.IS_EQ_SET_LOD) {
                UpperPanelSettings.defaultSettingsLoader();
                UpperPanelSettings.DEF_RELOD = false;
            }
            if (!CommonSettings.IS_DERI_SET_LOD) {
                MiddlePanelSettings.defaultSettingsLoader();
                MiddlePanelSettings.DEF_RELOD = false;

            }
            if (!CommonSettings.IS_INT_SET_LOD) {
                LowerPanelSettings.defaultSettingsLoader();
                LowerPanelSettings.DEF_RELOD = false;

            }
            radioE.setEnabled(true);
            radioD.setEnabled(true);
            radioI.setEnabled(true);
        } else {
            radioE.setEnabled(false);
            radioD.setEnabled(false);
            radioI.setEnabled(false);
        }
    }

    public static void deselectingAdvancedTickers() {
        if (SharedSettings.IS_ADVANCED_SELECTED) {
            if (EQ_UP) {
                radioE.setSelected(true);
            }
            if (DERI_UP) {
                radioD.setSelected(true);
            }
            if (INT_UP) {
                radioI.setSelected(true);
            }
        } else {
            radioE.setSelected(false);
            radioD.setSelected(false);
            radioI.setSelected(false);
        }
    }

    public static void advancedPanelDeSetting() {
        UpperPanelSettings.IS_EQ_SELECTED = false;
        UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;
        panelSet.setEquityPanel(false);

        MiddlePanelSettings.IS_EQ_SELECTED = false;
        MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;
        panelSet.setDerivativesPanel(false);

        LowerPanelSettings.IS_EQ_SELECTED = false;
        LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;
        panelSet.setIntergratedPanel(false);
    }

    public static void advancedPanelSetting() {

        if (CommonSettings.EQ_UP) {
            UpperPanelSettings.IS_EQ_SELECTED = true;
            UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_UP;
            UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_TIK_UP;

            panelSet.setEquityPanel(true);
        }

        if (CommonSettings.DERI_UP) {
            MiddlePanelSettings.IS_EQ_SELECTED = true;
            MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;
            MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;

            panelSet.setDerivativesPanel(true);
        }

        if (CommonSettings.INT_UP) {
            LowerPanelSettings.IS_EQ_SELECTED = true;
            LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
            LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;

            panelSet.setIntergratedPanel(true);
        }
    }

    public static TWMenu getTickerMenu() {
        final TWCheckBoxMenuItem menuItemE = new TWCheckBoxMenuItem(Language.getString("UPPER_TICKER"));
        final TWCheckBoxMenuItem menuItemD = new TWCheckBoxMenuItem(Language.getString("MIDDLE_TICKER"));
        final TWCheckBoxMenuItem menuItemI = new TWCheckBoxMenuItem(Language.getString("LOWER_TICKER"));

        radioE = menuItemE;
        radioD = menuItemD;
        radioI = menuItemI;
        menuItemE.setSelected(false);

        menuItemE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (menuItemE.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        EQ_UP = true;
                        panelSet.setEquityPanel(true);

                        UpperPanelSettings.IS_EQ_SELECTED = true;
                        UpperPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
                        UpperPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
                        HolderPanel.getInstance().getPreferredSize();
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();

                        SharedMethods.updateComponent(UpperTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemE.setSelected(false);
                    }

                } else if (!menuItemE.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        EQ_UP = false;
                        UpperPanelSettings.IS_EQ_SELECTED = false;
                        UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;
                        UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;

                        panelSet.setEquityPanel(false);
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();
                        SharedMethods.updateComponent(UpperTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemE.setSelected(true);
                    }
                }
            }
        });
        ticker.add(menuItemE);

        menuItemD.setSelected(false);
        menuItemD.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (menuItemD.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        DERI_UP = true;
                        panelSet.setDerivativesPanel(true);

                        MiddlePanelSettings.IS_EQ_SELECTED = true;
                        MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;
                        MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();
                        SharedMethods.updateComponent(MiddleTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemD.setSelected(false);
                    }
                } else if (!menuItemD.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        DERI_UP = false;
                        MiddlePanelSettings.IS_EQ_SELECTED = false;
                        MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;
                        MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;

                        panelSet.setDerivativesPanel(false);
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();
                        SharedMethods.updateComponent(MiddleTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemD.setSelected(true);
                    }

                }
            }
        });
        ticker.add(menuItemD);

        menuItemI.setSelected(false);
        menuItemI.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (menuItemI.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        INT_UP = true;
                        panelSet.setIntergratedPanel(true);

                        LowerPanelSettings.IS_EQ_SELECTED = true;
                        LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
                        LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();
                        SharedMethods.updateComponent(LowerTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemI.setSelected(false);
                    }
                } else if (!menuItemI.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        INT_UP = false;
                        LowerPanelSettings.IS_EQ_SELECTED = false;
                        LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;
                        LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;

                        panelSet.setIntergratedPanel(false);
                        TickerFrame.setSizeOnFrame();
                        Ticker.getInstance().updateUI();
                        SharedSettings.Frame_Header_Changed_by = 1;
                        Client.getInstance().getTickerFrame().resizeFrame();
                        HolderPanel.getInstance().updateUI();
                        SharedMethods.updateComponent(LowerTickerPanel.getInstance());
                        Client.getInstance().getDesktop().revalidate();
                    } else {
                        menuItemI.setSelected(true);
                    }
                }
            }
        });
        ticker.add(menuItemI);
        return ticker;
    }


    public static void selectEquityTicker(boolean status) {
        radioE.setSelected(status);
        IS_EQ_SELECTED = status;
        if (status) {
            UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_UP;
            UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_TIK_UP;
        } else {
            UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;
            UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;
        }

    }

    public static void selectDeravativesTicker(boolean status) {
        radioD.setSelected(status);
        IS_DERI_SELECTED = status;
        if (status) {
            MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;
            MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_UP;
        } else {
            MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;
            MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;
        }

    }

    public static void selectIntegrityTicker(boolean status) {
        radioI.setSelected(status);
        IS_INT_SELECTED = status;
        if (status) {
            LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
            LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_UP;
        } else {
            LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;
            LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;
        }

    }

    public static void deselectEquityTicker(boolean status) {
        EQ_UP = false;
        radioE.setSelected(status);
        IS_EQ_SELECTED = status;
        UpperPanelSettings.EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;
        UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_TIK_DWN;

    }

    public static void deselectDeravativesTicker(boolean status) {
        DERI_UP = false;
        radioD.setSelected(status);
        IS_DERI_SELECTED = status;
        MiddlePanelSettings.EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;
        MiddlePanelSettings.S_EQ_STATUS = MiddlePanelSettings.EQ_TIK_DWN;


    }

    public static void deselectIntegrityTicker(boolean status) {
        INT_UP = false;
        radioI.setSelected(status);
        IS_INT_SELECTED = status;
        LowerPanelSettings.EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;
        LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_TIK_DWN;

    }


    public static boolean isEquity() {
        return IS_EQ_SELECTED;
    }

    public static boolean isDerivatives() {
        return IS_DERI_SELECTED;
    }

    public static boolean isIntergrated() {
        return IS_INT_SELECTED;
    }


    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeAdded(Exchange exchange) {
//        enablingAdvancedTickers();
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        SharedSettings.radioD.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
        radioD.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
        radioE.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
        radioI.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
        SharedSettings.seperator.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
        if (SharedSettings.IS_ADVANCED_SELECTED && (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER))) {
            SharedSettings.Advanced_Ticker_Selector = true;
            /*UpperPanelSettings.defaultSettingsLoader();
            MiddlePanelSettings.defaultSettingsLoader();
            LowerPanelSettings.defaultSettingsLoader();*/

            if (UpperTickerFeeder.isVisible()) {
                if (UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) {
                    UpperFilterPanel.getInstance().setSelectedExchanges();
                    if (SharedSettings.is_Upper_Very_First_Time) {
                        SharedSettings.is_Upper_Very_First_Time = false;
                        UpperPanelSettings.defaultSettingsLoader();
                    }
                }
            }
            if (MiddleTickerFeeder.isVisible()) {
                if (MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) {
                    MiddleFilterPanel.getInstance().setSelectedExchanges();
                    /* if (SharedSettings.is_Very_First_Time) {
                        MiddlePanelSettings.defaultSettingsLoader();
                    }*/
                } else if (MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_ANNOUNCEMENT) {
                    MiddleFilterPanel.getInstance().setSelectedExchanges();
                    if (SharedSettings.is_Middle_Very_First_Time) {
                        SharedSettings.is_Middle_Very_First_Time = false;
                        MiddlePanelSettings.defaultSettingsLoader();
                    }
                }
            }
            if (LowerTickerFeeder.isVisible()) {
                if (LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) {
                    LowerFilterPanel.getInstance().setSelectedExchanges();
                   /* if (SharedSettings.is_Very_First_Time) {
                        LowerPanelSettings.defaultSettingsLoader();
                    }*/
                } else if (LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_NEWS) {
                    LowerFilterPanel.getInstance().setSelectedExchanges();
                    if (SharedSettings.is_Lower_Very_First_Time) {
                        SharedSettings.is_Lower_Very_First_Time = false;
                        LowerPanelSettings.defaultSettingsLoader();
                    }
                }
            }
            SharedSettings.is_Very_First_Time = false;
            SharedSettings.Frame_Header_Changed_by = 1;
            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            SharedMethods.updateComponent(Ticker.getInstance());
            Client.getInstance().getTickerFrame().resizeFrame();
            SharedMethods.updateComponent(HolderPanel.getInstance());
            SharedMethods.updateComponent(HolderPanel.getInstance());
            Client.getInstance().getDesktop().revalidate();
            Client.getInstance().getTickerFrame().resizeFrame();
            SharedMethods.updateComponent(Client.getInstance().getTickerFrame());
        }

    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
