package com.isi.csvr.ticker.advanced;

import com.isi.csvr.news.News;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.shared.DynamicArray;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 13, 2008
 * Time: 1:55:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class LowerNewsTickerUpdator extends Thread {
    private boolean active = true;
    private DynamicArray dataStore;

    public LowerNewsTickerUpdator() {
        super("Summary LowerTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            LowerTickerFeeder.isQueueEmpty = true;
            LowerTickerFeeder.isNewsModeSelected = false;
            LowerTickerFeeder.newsQueue.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        while (active) {
            if (LowerTickerFeeder.newsQueue != null && LowerTickerFeeder.newsQueue.isEmpty()) {
                newsModeSelector();
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }


    public void newsModeSelector() {
        dataStore = NewsStore.getSharedInstance().getFilteredNewsStore();
        for (int i = 0; i < dataStore.size(); i++) {
            LowerTickerFeeder.addNewsData((News) dataStore.get(i));
        }
        LowerTickerFeeder.isNewsModeSelected = true;
    }
}
