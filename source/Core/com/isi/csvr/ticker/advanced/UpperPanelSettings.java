package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;
import com.isi.csvr.TWRadioButtonMenuItem;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.TWTypes;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;

import java.awt.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 22, 2008
 * Time: 1:59:00 PM
 * To change this template use File | Settings | File Templates.
 */

public class UpperPanelSettings {

    public final static int LEFT_SEPERATOR = 30; //50; //30;
    public final static int RIGHT_SEPERATOR = 30; //10; //5;
    public final static int MENU_IMAGE_HEIGHT = 40; //48;
    public final static int MENU_IMAGE_WIDTH = 20;
    public final static int MENU_AREA_HEIGHT = 48;   //50;
    public final static int MENU_AREA_WIDTH = 20; //25;   //100;
    public final static int MENU_SEPERATOR_WIDTH = 0; //5;
    public final static String IMAGE_PATH = "images/ticker/";
    public final static String MENU_IMAGE_FILE = "icon_buttons";
    public final static String UP = "U";
    public final static String DOWN = "D";
    public final static String NOCHANGE = "N";
    public final static String SMALL = "S";
    public final static String VALNULL = "";
    public static final String TICKER_RECORD_DELIMETER = ";";
    public static Hashtable exchngeHash = new Hashtable();
    public static boolean FIRST_EXCHANGE = false;
    public static boolean FIRST_WATCH = false;
    public static boolean FIRST_ANS = false;
    public static boolean FIRST_NEWS = false;
    public static int UNIT_INCREMENT = 1;        // No of pixel Positions per a move
    public static boolean ENGLISH_VERSION = true; //true;
    public static boolean CONTINUE_LOOP = false;
    public static boolean isSymbolShowing = false;
    public static boolean showSummaryTicker = false;
    public static boolean CONTINUE_EQ_LOOP = false;
    public static boolean CONTINUE_DERI_LOOP = false;
    public static boolean CONTINUE_INT_LOOP = false;
    public static TWRadioButtonMenuItem radioE;
    public static TWRadioButtonMenuItem radioD;
    public static TWRadioButtonMenuItem radioI;
    public static boolean isChangeMode = true;
    public static int STOCK_TICKER_HEIGHT = 90; //42; //30; // 300
    public static int SYMBOL_DISPLAY_HEIGHT = 12;
    public static int QTY_DISPLAY_HEIGHT = 24;
    public static int DETAIL_DISPLAY_HEIGHT = 36;
    public static int MAX_MIN_RECT_HEIGHT = 3;
    public static int MAX_MIN_RECT_ALLOWENCE = 5;
    public static int MAX_RECT_DISPLAY_HEIGHT = 1;
    public static int MIN_RECT_DISPLAY_HEIGHT = 86;
    public static int FONT_MEDIUM = 12;
    public static int FONT_STYLE = Font.BOLD;
    public static FontMetrics symbolFm;
    public static FontMetrics smallFm;
    public static Font bigFont;
    public static Font smallFont;
    public static boolean FIRST_MIXED_MODE = false;
    public static Color UP_COLOR = Color.green.darker();
    public static Color DOWN_COLOR = Color.red;
    public static Color NO_CHANGE_COLOR = Color.black;
    public static Color NULL_COLOR = Color.black;
    public static Color SMALL_TRADE_COLOR = Color.black;
    public static Color TICKER_BORDER_COLOR = Color.black;
    public static Color TICKER_MIN_PRICE_COLOR = Color.black;
    public static Color TICKER_MAX_PRICE_COLOR = Color.black;
    public static Color SYMBOL_HIGHLIGHT_COLOR = Color.MAGENTA;
    public static Color TICKER_BACKGROUND = Color.white;
    public static Color TICKER_SEPERATOR = new Color(51, 102, 153);
    public static Color NEWS_TITLE_COLOR = Color.BLACK;
    public static Color NEWS_HIGHLIGHT_COLOR = Color.RED;
    public static Color ANNOUNCEMENT_TITLE_COLOR = Color.BLACK;
    public static Color ANNOUNCEMENT_HIGHLIGHT_COLOR = Color.RED;

    public static Color FILTER_PANEL_COLOR = Color.WHITE;
    public static Color FILTER_PANEL_HEADER_COLOR = Color.WHITE;

    public static int TICKER_WIDTH = 600;
    public static int INDIA_TICKER_HEIGHT = 29;
    public static int INDIA_DRAW_HEIGHT = 19;
    public static String IMAGE_LIST;
    public static boolean watchListMode = false;
    public static boolean exchnageListMode = false;
    public static boolean checkNoneMode = false;
    public static int exchangelist = 1;
    public static int MODE = exchangelist;
    public static int watchlist = 2;
    public static int anslist = 3;
    public static int newslist = 4;
    public static boolean IS_EQ_SELECTED = false;
    public static boolean IS_DERI_SELECTED = false;
    public static boolean IS_INT_SELECTED = false;
    public static int EQ_TIK_UP = 1;
    public static int EQ_STATUS = EQ_TIK_UP;
    public static int S_EQ_STATUS = EQ_TIK_UP;
    public static int EQ_TIK_DWN = 0;
    public static int FONT_SMALL = 13;
    public static int FONT_BIG = 16;
    public static int BIG_FONT_STYLE = Font.BOLD;
    public static int SMALL_FONT_STYLE = Font.BOLD;
    public static String FONT_TYPE = "Arial";
    public static boolean DEF_RELOD = true;
    static TWTypes.TickerFilter tickerMode;
    static PanelSetter panelSet = new PanelSetter();
    private static String selectedExchangeForTickers;
    private static ArrayList<String> exchangeList = new ArrayList<String>();
    private static ArrayList<String> watchList = new ArrayList<String>();
    private static ArrayList<String> announcementsList = new ArrayList<String>();
    private static ArrayList<String> newsList = new ArrayList<String>();

    public static ArrayList<String> getExchangeList() {
        return exchangeList;
    }

    public static void setExchangeList(ArrayList<String> exchangeLists) {
        exchangeList = exchangeLists;
    }

    public static ArrayList<String> getNewsList() {
        return newsList;
    }

    public static void setNewsList(ArrayList<String> newsList) {
        UpperPanelSettings.newsList = newsList;
    }

    public static ArrayList<String> getAnnouncementsList() {
        return announcementsList;
    }

    public static void setAnnouncementsList(ArrayList<String> announcementsList) {
        UpperPanelSettings.announcementsList = announcementsList;
    }

    public static ArrayList<String> getWatchList() {
        return watchList;
    }

    public static void setWatchList(ArrayList<String> watchList) {
        UpperPanelSettings.watchList = watchList;
    }

    public static void adjustTickerSize(int width) {
        //To change body of created methods use File | Settings | File Templates.
    }

    public static void loadingFromWorkSpace(String indtickerWSString) {

        ArrayList<String> WatchLists = new ArrayList<String>();
        ArrayList<String> ExchangeLists = new ArrayList<String>();

        ArrayList<String> AnsLists = new ArrayList<String>();
        ArrayList<String> NewsLists = new ArrayList<String>();
        DEF_RELOD = true;
        CommonSettings.IS_EQ_SET_LOD = true;
        SharedSettings.Advanced_Ticker_Selector = true;
        String loadString = indtickerWSString;
        ArrayList<String> tickerData = new ArrayList<String>();
        StringTokenizer fields = new StringTokenizer(loadString, Meta.RS);
        while (fields.hasMoreElements()) {
            tickerData.add(fields.nextToken());
        }
        UpperPanelSettings.BIG_FONT_STYLE = Integer.parseInt(tickerData.get(0));
        UpperPanelSettings.SMALL_FONT_STYLE = Integer.parseInt(tickerData.get(1));
        UpperPanelSettings.FONT_BIG = Integer.parseInt(tickerData.get(2));
        UpperPanelSettings.FONT_SMALL = Integer.parseInt(tickerData.get(3));
        UpperPanelSettings.FONT_TYPE = tickerData.get(4);
        UpperPanelSettings.INDIA_DRAW_HEIGHT = Integer.parseInt(tickerData.get(5));
        UpperPanelSettings.INDIA_TICKER_HEIGHT = Integer.parseInt(tickerData.get(6));
        UpperPanelSettings.EQ_STATUS = Integer.parseInt(tickerData.get(7));
        UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_STATUS;
        UpperPanelSettings.MODE = Integer.parseInt(tickerData.get(8));
        String Exchangelists = tickerData.get(9);
        UpperFilterPanel.EXCHANGE_LIST = Exchangelists;
        StringTokenizer exchange = new StringTokenizer(Exchangelists, TICKER_RECORD_DELIMETER);
        while (exchange.hasMoreElements()) {
            ExchangeLists.add(exchange.nextToken());
        }
        setExchangeList(ExchangeLists);
        String Watchlists = tickerData.get(10);
        UpperFilterPanel.WATCH_LIST = Watchlists;

        StringTokenizer watch = new StringTokenizer(Watchlists, TICKER_RECORD_DELIMETER);
        while (watch.hasMoreElements()) {
            WatchLists.add(watch.nextToken());
        }
        setWatchList(WatchLists);
        String Anslists = tickerData.get(11);

        UpperFilterPanel.ANNOUNCE_LIST = Anslists;

        StringTokenizer ans = new StringTokenizer(Anslists, TICKER_RECORD_DELIMETER);
        while (ans.hasMoreElements()) {
            AnsLists.add(ans.nextToken());
        }
        setAnnouncementsList(AnsLists);
        String Newslists = tickerData.get(12);

        UpperFilterPanel.NEWS_LIST = Newslists;

        StringTokenizer news = new StringTokenizer(Newslists, TICKER_RECORD_DELIMETER);
        while (news.hasMoreElements()) {
            NewsLists.add(news.nextToken());
        }
        setNewsList(NewsLists);

        UpperTicker.tickerSpeed = tickerData.get(13);
        Client.getInstance().getUpperTicker().applySavedTickerVariables();
        UpperTickerPanel.reloadFont(UpperTickerPanel.getInstance());

        if (UpperPanelSettings.MODE == 1) {
            UpperFilterPanel.EXG_SAVED = true;
            UpperTickerFeeder.isExchangeModeSelected = true;
            UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_EXCHANGE;
            UpperTickerPanel.getInstance().reloadFontforExchange();
        }
        if (UpperPanelSettings.MODE == 2) {
            UpperTickerFeeder.isWatchListModeSelected = true;
            UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            UpperTickerPanel.getInstance().reloadFontforExchange();
        }
        if (UpperPanelSettings.MODE == 3) {
            UpperFilterPanel.ANS_SAVED = true;
            UpperTickerFeeder.isAnnouncementModeSelected = true;
            UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_ANNOUNCEMENT;
            UpperTickerPanel.getInstance().reloadFontforAnnouncements();
        }
        if (UpperPanelSettings.MODE == 4) {
            UpperFilterPanel.NEWS_SAVED = true;

            UpperTickerFeeder.isNewsModeSelected = true;
            UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_NEWS;
            UpperTickerPanel.getInstance().reloadFontforNews();
        }

        UpperFilterPanel.showExTraded = Boolean.parseBoolean(tickerData.get(14));
        UpperFilterPanel.showExAll = Boolean.parseBoolean(tickerData.get(15));
        UpperFilterPanel.runExOnClose = Boolean.parseBoolean(tickerData.get(16));
        UpperFilterPanel.showWatchListTraded = Boolean.parseBoolean(tickerData.get(17));
        UpperFilterPanel.showWatchListAll = Boolean.parseBoolean(tickerData.get(18));
        UpperFilterPanel.runWatchListOnClose = Boolean.parseBoolean(tickerData.get(19));
        UpperFilterPanel.isVeryFirstTime = Boolean.parseBoolean(tickerData.get(20));


        try {
            UpperFilterPanel.getInstance().applyFilter();
            UpperTickerPanel.getInstance().adjustControls();
            UpperTickerPanel.getInstance().redrawTicker();
            UpperTickerPanel.getInstance().updateUI();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (UpperPanelSettings.EQ_STATUS == 1) {
            CommonSettings.EQ_UP = true;
            panelSet.setEquityPanel(true);
            CommonSettings.selectEquityTicker(true);
            IS_EQ_SELECTED = true;
        } else if (UpperPanelSettings.EQ_STATUS == 0) {
            CommonSettings.EQ_UP = false;
            panelSet.setEquityPanel(false);
            CommonSettings.selectEquityTicker(false);
            IS_EQ_SELECTED = false;
        }
    }

    public static void defaultSettingsLoader() {

        ArrayList<String> ExchangeLists = new ArrayList<String>();
        ArrayList<String> WatchLists = new ArrayList<String>();

        if (!DEF_RELOD && !CommonSettings.IS_EQ_SET_LOD) {
            UpperTicker.IS_DEFAULT = false;

//            UpperFilterPanel.EXG_SAVED = true;
//            UpperPanelSettings.MODE = exchangelist;
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = exchanges.nextElement();
                if (exchange.isDefault()) {
                    ExchangeLists.add(exchange.getSymbol());
                    UpperTicker.IS_DEFAULT = true;
                }
            }
            setExchangeList(ExchangeLists);


            if (UpperTicker.IS_DEFAULT) {
                UpperFilterPanel.EXG_SAVED = true;
                UpperPanelSettings.MODE = exchangelist;
                UpperTickerFeeder.isExchangeModeSelected = true;
                UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_EXCHANGE;
            } else {
                if (!SharedSettings.is_Upper_Very_First_Time) {
                    UpperFilterPanel.WATCHLIST_SAVED = true;
                    WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                    for (WatchListStore watchlist : watchlists) {
                        WatchLists.add(watchlist.getId());
                    }
                    setWatchList(WatchLists);
                    UpperTickerFeeder.isExchangeModeSelected = false;
                    UpperTickerFeeder.isWatchListModeSelected = true;
                    UpperFilterPanel.mode = TWTypes.TickerFilter.MODE_WATCHLIST;
                }
            }
            UpperTickerPanel.getInstance().reloadFontforExchange();
            UpperFilterPanel.getInstance().applyFilter();
            UpperTickerPanel.getInstance().adjustControls();
            UpperTickerPanel.getInstance().redrawTicker();
            UpperTickerPanel.getInstance().updateUI();
            UpperPanelSettings.EQ_STATUS = EQ_TIK_UP;
            UpperPanelSettings.S_EQ_STATUS = UpperPanelSettings.EQ_STATUS;
            if (UpperPanelSettings.EQ_STATUS == 1) {
                CommonSettings.EQ_UP = true;
                panelSet.setEquityPanel(true);
                CommonSettings.selectEquityTicker(true);
                IS_EQ_SELECTED = true;
            }
            if (!SharedSettings.is_Upper_Very_First_Time) {
                CommonSettings.IS_EQ_SET_LOD = true;
                UpperPanelSettings.DEF_RELOD = true;
            }
        }
    }
}

