package com.isi.csvr.ticker.advanced;

import com.isi.csvr.TWMenu;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.news.NewsProvider;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.ticker.uicomponents.TickerFilterableComponent;
import com.isi.csvr.ticker.uicomponents.TickerFilterableComponentListener;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.ColumnLayout;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 9, 2008
 * Time: 5:27:34 PM
 * To change this template use File | Settings | File Templates.
 */


public class MiddleFilterPanel extends InternalFrame implements MouseListener, ActionListener, TWTypes, TickerFilterableComponentListener {

    public static boolean isExchangeMode = false;
    public static boolean isWatchListMode = false;
    public static boolean isNewsMode = false;
    public static boolean isAnnouncementsMode = false;
    public static boolean FIRST_LOAD = false;
    public static boolean NEWS_SAVED = false;
    public static boolean EXG_SAVED = false;
    public static boolean ANS_SAVED = false;
    public static TWTypes.TickerFilter mode;
    public static ArrayList<String> selectedExchanges = new ArrayList<String>();
    public static ArrayList<String> selectedExchangesForEx = new ArrayList<String>();
    public static ArrayList<String> selectedNewsExchanges = new ArrayList<String>();
    public static String WATCH_LIST = null;
    public static String EXCHANGE_LIST = null;
    public static String ANNOUNCE_LIST = null;
    public static String NEWS_LIST = null;
    public static MiddleFilterPanel self;
    public static boolean showExTraded = true;
    public static boolean showExAll = false;
    public static boolean runExOnClose = false;
    public static boolean isExRunClicked = false;
    public static boolean showWatchListTraded = true;
    public static boolean showWatchListAll = false;
    public static boolean runWatchListOnClose = false;
    public static boolean isWatchListRunClicked = false;
    public static boolean isOkClicked = false;
    public static boolean isVeryFirstTime = true;
    private static TickerFilterableComponent exchangeComponent;
    private static TickerFilterableComponent watchlistComponent;
    private static TickerFilterableComponent announcementComponent;
    private static TickerFilterableComponent newsComponent;
    private static ArrayList<String> exchanesList = new ArrayList<String>();
    // ------------- The modes to be filtered -------------------
    private static ArrayList<String> watchList = new ArrayList<String>();
    private static ArrayList<String> announcementsList = new ArrayList<String>();
    private static ArrayList<String> newsList = new ArrayList<String>();
    private static Properties prop;
    private final String U_TICKER = "U";
    public ArrayList<String> newssources;
    Hashtable exchngeHash = new Hashtable();
    private TWButton okbtn;
    private TWButton cancelbtn;
    private JRadioButton showTradedEx;
    private JRadioButton showAllEx;
    private JRadioButton showTradedWatchlist;
    private JRadioButton showAllWatchlist;
    private JPanel exchangeMainPanel;
    private JPanel exchangeOptionPanel;
    private JPanel exchangeOptionPanelHolder;
    private JPanel exchangeRunPanel;
    private JCheckBox runExBox;
    private JCheckBox runWatchBox;
    private JPanel watchListMainPanel;
    private JPanel watchListOptionPanel;
    private JPanel watchListOptionPanelHolder;
    private JPanel watchListRunPanel;
    private JLabel okButton;
    private JPanel btnPanel;
    private JPanel exchangeListPanel;
    private JPanel exchangeListPanelLeft;
    //==== Filtering modes ====
    private JPanel exchangeListPanelHolder;
    private JPanel watchlistPanel;
    private JPanel watchlistPanelLeft;
    private JPanel watchlistPanelHolder;
    private JPanel newsListPanel;
    private JPanel announcementsListPanel;
    private JPanel classicViewtPanel;
    private TWMenu popupMenu;
    private Symbols[] filter;
    private boolean watchListMode = true;
    private ImageIcon checkBox;


    public MiddleFilterPanel() {
//        createUI();
    }

    public MiddleFilterPanel(TWMenu tickerFilter) {
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = exchanges.nextElement();
            exchngeHash.put(exchange.getDescription(), exchange.getSymbol());
        }
        this.popupMenu = tickerFilter;
        createUI();
    }

    public static MiddleFilterPanel getInstance() {
        if (self == null) {
            self = new MiddleFilterPanel(new TWMenu());
        }
        return self;
    }

    public void createUI() {
        this.setTitleVisible(false);
        okbtn = new TWButton(Language.getString("OK"));
        cancelbtn = new TWButton(Language.getString("CANCEL"));

        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        getContentPane().setLayout(new ColumnLayout());
        createExchangeComponenet();
        createWatchListComponent();
        createAnnouncementComponent();
        createNewsComponent();

        btnPanel = new JPanel();
        btnPanel.setBackground(LowerPanelSettings.FILTER_PANEL_COLOR);
        btnPanel.setLayout(new FlexGridLayout(new String[]{"15%", "32%", "6%", "32%", "15%"}, new String[]{"20"}, 2, 0));
        okButton = new JLabel("  " + Language.getString("OK") + "  ", SwingUtilities.RIGHT);
        okButton.setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
        okButton.addMouseListener(this);
        cancelbtn.addActionListener(this);
        okbtn.addActionListener(this);
        btnPanel.add(new JLabel());
        btnPanel.add(okbtn);
        btnPanel.add(new JLabel());
        btnPanel.add(cancelbtn);
        btnPanel.add(new JLabel());
        add(new JSeparator());
        add(btnPanel);
        paintUIOnOpen();
        setVisible(true);
        resizeFrame();
        doLayout();

    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    private void createExchangeComponenet() {
        exchangeComponent = new TickerFilterableComponent(U_TICKER, this);
        exchangeComponent.setTextBox("");
        exchangeComponent.setTextLable(Language.getString("EXCHANGES"));
        if (MiddleTickerFeeder.isExchangeModeSelected) {
            exchangeComponent.setPanelVisible(true);
        }

        showTradedEx = new JRadioButton(Language.getString("SHOW_TRADED"));
        showTradedEx.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        showTradedEx.setEnabled(MiddleTickerFeeder.isExchangeModeSelected);
        showTradedEx.setSelected(showExTraded);
        showTradedEx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showTradedEx.isSelected()) {
                    showExTraded = true;
                    showExAll = false;
                    showTradedEx.setSelected(showExTraded);
                    showAllEx.setSelected(showExAll);
                } else if (!showTradedEx.isSelected()) {
                    showTradedEx.setSelected(true);
                }
            }
        });
        showAllEx = new JRadioButton(Language.getString("SHOW_ALL"));
        showAllEx.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        showAllEx.setEnabled(MiddleTickerFeeder.isExchangeModeSelected);
        showAllEx.setSelected(showExAll);

        showAllEx.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showAllEx.isSelected()) {
                    showExTraded = false;
                    showExAll = true;
                    showTradedEx.setSelected(showExTraded);
                    showAllEx.setSelected(showExAll);
                } else if (!showAllEx.isSelected()) {
                    showAllEx.setSelected(true);
                }
            }
        });
        runExBox = new JCheckBox(Language.getString("TICKER_RUN_MSG"));

        runExBox.setEnabled(MiddleTickerFeeder.isExchangeModeSelected);
        runExBox.setSelected(runExOnClose);
        runExBox.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        runExBox.setFont(new java.awt.Font("Arial", Font.PLAIN, 12));
        runExBox.addMouseListener(this);

        exchangeMainPanel = new JPanel();
        exchangeMainPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeMainPanel.setLayout(new BoxLayout(exchangeMainPanel, BoxLayout.Y_AXIS));
        exchangeMainPanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

        exchangeOptionPanel = new JPanel();
        exchangeOptionPanel.setAlignmentX(0);
        exchangeOptionPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeOptionPanel.setLayout(new FlexGridLayout(new String[]{"7", "50%", "50%"}, new String[]{"25"}, 5, 0));
        exchangeOptionPanel.add(new JLabel());
        exchangeOptionPanel.add(showTradedEx);
        exchangeOptionPanel.add(showAllEx);

        exchangeOptionPanelHolder = new JPanel();
        exchangeOptionPanelHolder.setLayout(new BoxLayout(exchangeOptionPanelHolder, BoxLayout.X_AXIS));

        exchangeListPanelLeft = new JPanel();
        exchangeListPanelHolder = new JPanel();
        exchangeListPanelHolder.setLayout(new BoxLayout(exchangeListPanelHolder, BoxLayout.X_AXIS));
        exchangeListPanelHolder.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeListPanelLeft.setLayout(new BoxLayout(exchangeListPanelLeft, BoxLayout.Y_AXIS));

        exchangeListPanel = new JPanel();
        exchangeListPanel.setAlignmentX(0);
        exchangeListPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeListPanel.setLayout(new ColumnLayout());
        exchangeRunPanel = new JPanel();
        exchangeRunPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeRunPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25"}, 7, 2));

        exchangeOptionPanelHolder.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        exchangeOptionPanelHolder.add(exchangeOptionPanel);
        exchangeRunPanel.add(runExBox);
        exchangeMainPanel.add(exchangeOptionPanelHolder);
        exchangeListPanelHolder.add(Box.createHorizontalStrut(20));
        exchangeListPanelHolder.add(exchangeListPanel);
        exchangeMainPanel.add(exchangeListPanelHolder);
        exchangeMainPanel.add(exchangeRunPanel);
        exchangeComponent.addIemsToPanel(exchangeMainPanel);
        add(exchangeComponent);

        addingExchanges();
    }


    private void createWatchListComponent() {
        watchlistComponent = new TickerFilterableComponent(U_TICKER, this);
        watchlistComponent.setTextBox("");
        watchlistComponent.setTextLable(Language.getString("MY_STOCKS"));
        if (MiddleTickerFeeder.isWatchListModeSelected) {
            watchlistComponent.setPanelVisible(true);
        }
        showTradedWatchlist = new JRadioButton(Language.getString("SHOW_TRADED"));
        showTradedWatchlist.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        showTradedWatchlist.setEnabled(MiddleTickerFeeder.isWatchListModeSelected);
        showTradedWatchlist.setSelected(showWatchListTraded);
        showTradedWatchlist.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showTradedWatchlist.isSelected()) {
                    showWatchListTraded = true;
                    showWatchListAll = false;
                    showTradedWatchlist.setSelected(showWatchListTraded);
                    showAllWatchlist.setSelected(showWatchListAll);
                } else if (!showTradedWatchlist.isSelected()) {
                    showTradedWatchlist.setSelected(true);
                }
            }
        });
        showAllWatchlist = new JRadioButton(Language.getString("SHOW_ALL"));
        showAllWatchlist.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        showAllWatchlist.setEnabled(MiddleTickerFeeder.isWatchListModeSelected);
        showAllWatchlist.setSelected(showWatchListAll);

        showAllWatchlist.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (showAllWatchlist.isSelected()) {
                    showWatchListTraded = false;
                    showWatchListAll = true;
                    showTradedWatchlist.setSelected(showWatchListTraded);
                    showAllWatchlist.setSelected(showWatchListAll);
                } else if (!showAllWatchlist.isSelected()) {
                    showAllWatchlist.setSelected(true);
                }
            }
        });
        runWatchBox = new JCheckBox(Language.getString("TICKER_RUN_MSG"));
        runWatchBox.setEnabled(MiddleTickerFeeder.isWatchListModeSelected);
        runWatchBox.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        runWatchBox.setFont(new java.awt.Font("Arial", Font.PLAIN, 12));
        runWatchBox.setSelected(runWatchListOnClose);
        runWatchBox.addMouseListener(this);

        watchListMainPanel = new JPanel();
        watchListMainPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchListMainPanel.setLayout(new BoxLayout(watchListMainPanel, BoxLayout.Y_AXIS));
        watchListMainPanel.setBorder(BorderFactory.createEmptyBorder(2, 0, 2, 0));

        watchListOptionPanel = new JPanel();
        watchListOptionPanel.setAlignmentX(0);
        watchListOptionPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchListOptionPanel.setLayout(new FlexGridLayout(new String[]{"7", "50%", "50%"}, new String[]{"25"}, 5, 0));
        watchListOptionPanel.add(new JLabel());
        watchListOptionPanel.add(showTradedWatchlist);
        watchListOptionPanel.add(showAllWatchlist);

        watchListOptionPanelHolder = new JPanel();
        watchListOptionPanelHolder.setLayout(new BoxLayout(watchListOptionPanelHolder, BoxLayout.X_AXIS));

        watchlistPanelLeft = new JPanel();
        watchlistPanelHolder = new JPanel();
        watchlistPanelHolder.setLayout(new BoxLayout(watchlistPanelHolder, BoxLayout.X_AXIS));
        watchlistPanelHolder.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchlistPanelLeft.setLayout(new BoxLayout(watchlistPanelLeft, BoxLayout.Y_AXIS));

        watchlistPanel = new JPanel();
        watchlistPanel.setAlignmentX(0);
        watchlistPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchlistPanel.setLayout(new ColumnLayout());

        watchListRunPanel = new JPanel();
        watchListRunPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchListRunPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"25"}, 7, 2));

        watchListOptionPanelHolder.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        watchListOptionPanelHolder.add(watchListOptionPanel);
        watchListRunPanel.add(runWatchBox);
        watchListMainPanel.add(watchListOptionPanelHolder);

        watchlistPanelHolder.add(Box.createRigidArea(new Dimension(20, (int) watchlistPanel.getPreferredSize().getHeight())));
        watchlistPanelHolder.add(watchlistPanel);
        watchListMainPanel.add(watchlistPanelHolder);
        watchListMainPanel.add(watchListRunPanel);
        watchlistComponent.addIemsToPanel(watchListMainPanel);

        add(watchlistComponent);
        watchListLoading();

    }


    private void createAnnouncementComponent() {

        announcementComponent = new TickerFilterableComponent(U_TICKER, this);
        announcementComponent.setTextBox("");
        announcementComponent.setTextLable(Language.getString("ANNOUNCEMENTS"));

        if (MiddleTickerFeeder.isAnnouncementModeSelected) {
            announcementComponent.setPanelVisible(true);
        }
        announcementsListPanel = new JPanel();
        announcementsListPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        announcementsListPanel.setLayout(new BoxLayout(announcementsListPanel, BoxLayout.PAGE_AXIS));
        announcementsListPanel.setBorder(BorderFactory.createEmptyBorder(2, 20, 2, 10));

        announcementComponent.addIemsToPanel(announcementsListPanel);
        add(announcementComponent);

        addingExchangesForAnnouncements();
    }

    private void createNewsComponent() {
        newsComponent = new TickerFilterableComponent(U_TICKER, this);
        newsComponent.setTextBox("");
        newsComponent.setTextLable(Language.getString("NEWS"));
        if (MiddleTickerFeeder.isNewsModeSelected) {
            newsComponent.setPanelVisible(true);
        }
        newsListPanel = new JPanel();
        newsListPanel.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
        newsListPanel.setLayout(new BoxLayout(newsListPanel, BoxLayout.PAGE_AXIS));
        newsListPanel.setBorder(BorderFactory.createEmptyBorder(2, 20, 2, 10));

        newsComponent.addIemsToPanel(newsListPanel);
        add(newsComponent);

        try {
            loadSettings();
            setNewsSources();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        createSources();
    }

    private void paintUIOnOpen() {
        if (MiddleTickerFeeder.isExchangeModeSelected) {
            mode = TickerFilter.MODE_EXCHANGE;

            exchangeComponent.setCheckBoxSelected(true);
            watchlistComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(false);

            isExchangeMode = true;
            changeExchangeListState(true);
            changeWatchlistState(false);
            changeAnnouncementslistState(false);
            changeNewslistState(false);
        }
        if (MiddleTickerFeeder.isWatchListModeSelected) {
            mode = TickerFilter.MODE_WATCHLIST;

            isWatchListMode = true;
            exchangeComponent.setCheckBoxSelected(false);
            watchlistComponent.setCheckBoxSelected(true);
            announcementComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(false);

            changeExchangeListState(false);
            changeWatchlistState(true);
            changeAnnouncementslistState(false);
            changeNewslistState(false);

        }
        if (MiddleTickerFeeder.isAnnouncementModeSelected) {
            mode = TickerFilter.MODE_ANNOUNCEMENT;

            isAnnouncementsMode = true;
            exchangeComponent.setCheckBoxSelected(false);
            watchlistComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(true);
            newsComponent.setCheckBoxSelected(false);

            changeExchangeListState(false);
            changeWatchlistState(false);
            changeAnnouncementslistState(true);
            changeNewslistState(false);

        }
        if (MiddleTickerFeeder.isNewsModeSelected) {
            mode = TickerFilter.MODE_NEWS;

            isNewsMode = true;
            exchangeComponent.setCheckBoxSelected(false);
            watchlistComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(true);

            changeExchangeListState(false);
            changeWatchlistState(false);
            changeAnnouncementslistState(false);
            changeNewslistState(true);
        }
        GUISettings.applyOrientation(watchlistPanel);
        GUISettings.applyOrientation(exchangeListPanel);
        GUISettings.applyOrientation(announcementsListPanel);
        GUISettings.applyOrientation(newsListPanel);
    }


    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(okButton)) {
            getParent().setVisible(false);
            popupMenu.getParent().setVisible(false);
            applyFilter();
        } else if (e.getSource().equals(runExBox)) {
            if (isExchangeMode) {
                isExRunClicked = true;
                runExOnClose ^= true;
//                runExOnClose = !runExOnClose;
                runExBox.setSelected(runExOnClose);
            }
        } else if (e.getSource().equals(runWatchBox)) {
            if (isWatchListMode) {
                isWatchListRunClicked = true;
                runWatchListOnClose ^= true;
//                runWatchListOnClose = !runWatchListOnClose;
                runWatchBox.setSelected(runWatchListOnClose);
            }
        }
    }


    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(okbtn)) {
            getParent().setVisible(false);
            popupMenu.getParent().setVisible(false);
            isOkClicked = true;
            applyFilter();
            isOkClicked = false;
        } else if (e.getSource().equals(cancelbtn)) {
            getParent().setVisible(false);
            popupMenu.getParent().setVisible(false);
        }
    }


    public Dimension getPreferredSize() {
        int width = 0;
        int hight = 0;
        Component cmp[] = getComponents();
        for (int i = 0; i < cmp.length; i++) {
            hight = hight + (cmp[i]).getPreferredSize().height;
            if (cmp[i] instanceof TickerFilterableComponent) {
                hight = hight + ((TickerFilterableComponent) cmp[i]).getHeight() + 6;
            }
            if (width < cmp[i].getPreferredSize().width) {
                width = cmp[i].getPreferredSize().width;
            }
        }
        return new Dimension(width, hight + 3);
    }


    public JPanel getWatchlistPanel() {
        return watchlistPanel;
    }

    public void loadSettings() throws IOException {
        prop = new Properties();

        FileInputStream oIn = null;
        try {
            oIn = new FileInputStream(Settings.getAbsolutepath() + "DataStore/news.dll");
            prop.load(oIn);
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setNewsSources() {
        try {
            newssources = new ArrayList<String>();

            String sources1 = prop.getProperty("SELECTED_SOURCES");
            if (sources1 != null && !sources1.equals("")) {
                String[] sources = sources1.split(",");
                for (int i = 0; i < sources.length; i++) {
                    newssources.add(sources[i]);
                }
            }
            if (newssources.size() == 0) {
                Enumeration<NewsProvider> nProviders = NewsProvidersStore.getSharedInstance().getAllProviders();
                while (nProviders.hasMoreElements()) {
                    NewsProvider np = nProviders.nextElement();
                    newssources.add(np.getDescription());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void watchListLoading() {
        ArrayList<String> gettingList = MiddlePanelSettings.getWatchList();
        try {
            WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
            for (WatchListStore watchlist : watchlists) {
                JCheckBox item = new JCheckBox(watchlist.getCaption());
                item.setText(watchlist.getCaption());
                item.setFont(new java.awt.Font("Arial", Font.PLAIN, 11));
                item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
                item.setName(watchlist.getId());
//                item.setText(watchlist.getId());
                if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                    watchlistPanel.add(item);

                }
                /* else if (watchlist.getListType() == WatchListStore.CLASSIC_LIST_TABLE_TYPE) {
                    classicViewtPanel.add(item);
                }*/

                if (gettingList.contains(watchlist.getId())) {
                    item.setSelected(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addingExchanges() {
        ArrayList<String> gettingList = MiddlePanelSettings.getExchangeList();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {

            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                JCheckBox item = new JCheckBox(exchange.getDescription());
                item.setText(exchange.getDescription());
                item.setFont(new java.awt.Font("Arial", Font.PLAIN, 11));
                item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);

                if (FIRST_LOAD) {
                    item.setSelected(true);
                    FIRST_LOAD = false;
                } else if (!FIRST_LOAD) {
                    for (int i = 0; i < gettingList.size(); i++) {
                        if (exchange.getSymbol().equals(gettingList.get(i))) {
                            item.setSelected(true);
                        }
                    }
                }

                exchangeListPanel.add(item);
                exchange = null;
            }
        }
    }

    public void addingExchangesForAnnouncements() {
        ArrayList<String> gettingList = MiddlePanelSettings.getAnnouncementsList();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {

            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                JCheckBox item = new JCheckBox(exchange.getDescription());
                item.setText(exchange.getDescription());
                item.setFont(new java.awt.Font("Arial", Font.PLAIN, 11));
                item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
                if (FIRST_LOAD) {
                    item.setSelected(true);
                    FIRST_LOAD = false;
                } else if (!FIRST_LOAD) {
                    for (int i = 0; i < gettingList.size(); i++) {
                        if (exchange.getSymbol().equals(gettingList.get(i))) {
                            item.setSelected(true);
                        }
                    }
                }
                announcementsListPanel.add(item);
                exchange = null;
            }
        }
    }


    public void createSources() {

        try {
            NewsProvidersStore.getSharedInstance().loadProviders();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ArrayList<String> gettingList = MiddlePanelSettings.getNewsList();

        try {
            Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
            Enumeration<String> providerObjects = providers.keys();
            while (providerObjects.hasMoreElements()) {
                String id = providerObjects.nextElement();
                NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                if (dnp != null) {
                    JCheckBox cb = new JCheckBox(dnp.getDescription());
                    cb.setText(dnp.getDescription());
                    cb.setFont(new java.awt.Font("Arial", Font.PLAIN, 11));
                    cb.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
                    cb.setName(dnp.getID());
                    newsListPanel.add(cb);
                    if (gettingList.contains(cb.getName().toUpperCase())) {
                        cb.setSelected(true);
                    }
                }
            }
            newsListPanel.updateUI();
        } catch (Exception e) {
        }
    }

    public void createSourceListPanel() {
        try {
            Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
            Enumeration<String> providerObjects = providers.keys();
            while (providerObjects.hasMoreElements()) {
                while (providerObjects.hasMoreElements()) {
                    String id = providerObjects.nextElement();
                    NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                    if (dnp != null) {
                        JCheckBox cb = new JCheckBox(dnp.getDescription());
                        newsListPanel.add(cb);
                    }
                }
            }
            newsListPanel.updateUI();
        } catch (Exception e) {
        }
    }


    public void applyFilter() {
        if (mode == TickerFilter.MODE_ALL) {
            Symbols[] symbols = new Symbols[1];
            symbols[0] = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());
            MiddleTickerFeeder.setFilter(mode, symbols);
        } else if (mode == TickerFilter.MODE_WATCHLIST) {

            MiddlePanelSettings.MODE = MiddlePanelSettings.watchlist;
            MiddleTickerFeeder.isWatchListModeSelected = true;
            MiddleTickerFeeder.isNewsModeSelected = false;
            MiddleTickerFeeder.isAnnouncementModeSelected = false;
            MiddleTickerFeeder.isExchangeModeSelected = false;

            MiddleTickerFeeder.activateSummaryTickerUpdator(true);
            MiddleTickerFeeder.activateNewstickerUpdator(false);
            MiddleTickerFeeder.activateAnnouncementtickerUpdator(false);
            applyWatchlistFilter();

        } else if (mode == TickerFilter.MODE_EXCHANGE) {
            MiddlePanelSettings.MODE = MiddlePanelSettings.exchangelist;
            while (!exchanesList.isEmpty()) {
                exchanesList.remove(0);
            }
            MiddleTickerFeeder.isAnnouncementModeSelected = false;
            MiddleTickerFeeder.isExchangeModeSelected = true;
            MiddleTickerFeeder.isWatchListModeSelected = false;
            MiddleTickerFeeder.isNewsModeSelected = false;
            MiddleTickerFeeder.clearOnlineData();
            MiddleTickerFeeder.activateSummaryTickerUpdator(true);
            MiddleTickerFeeder.activateNewstickerUpdator(false);
            MiddleTickerFeeder.activateAnnouncementtickerUpdator(false);
            exchangeModeSelector();
        } else if (mode == TickerFilter.MODE_NEWS) {
            MiddlePanelSettings.MODE = MiddlePanelSettings.newslist;
            MiddleTickerFeeder.isNewsModeSelected = true;
            MiddleTickerFeeder.isAnnouncementModeSelected = false;
            MiddleTickerFeeder.isExchangeModeSelected = false;
            MiddleTickerFeeder.isWatchListModeSelected = false;
            selectedNewsSources();
            MiddleTickerFeeder.clearOnlineData();
            MiddleTickerFeeder.emptyNewsQueue();
            MiddleTickerFeeder.activateSummaryTickerUpdator(false);
            MiddleTickerFeeder.activateAnnouncementtickerUpdator(false);
            newsModeStarter();
        } else if (mode == TickerFilter.MODE_ANNOUNCEMENT) {
            MiddlePanelSettings.MODE = MiddlePanelSettings.anslist;
            MiddleTickerFeeder.isAnnouncementModeSelected = true;
            MiddleTickerFeeder.isNewsModeSelected = false;
            MiddleTickerFeeder.isExchangeModeSelected = false;
            MiddleTickerFeeder.isWatchListModeSelected = false;

            selectedExchangesForAns();
            MiddleTickerFeeder.clearOnlineData();
            MiddleTickerFeeder.activateSummaryTickerUpdator(false);
            MiddleTickerFeeder.activateNewstickerUpdator(false);
            announcementModeStarter();
        }

    }

    private void selectedNewsSources() {
        if (isOkClicked || !isVeryFirstTime) {
            NEWS_LIST = null;
        }
        selectedNewsExchanges = new ArrayList<String>();
        Component[] exitems = null;
        exitems = newsListPanel.getComponents();
        for (int i = 0; i < exitems.length; i++) {
            JCheckBox item = (JCheckBox) exitems[i];
            if (item.isSelected()) {
                NEWS_LIST = NEWS_LIST + MiddlePanelSettings.TICKER_RECORD_DELIMETER;
                selectedNewsExchanges.add((String) item.getName().toUpperCase());
                String s = (String) item.getName().toUpperCase();
                NEWS_LIST = NEWS_LIST + s;
            }
            item = null;
        }
        if (NEWS_SAVED) {
            selectedNewsExchanges = MiddlePanelSettings.getNewsList();
        } else {
            MiddlePanelSettings.setNewsList(selectedNewsExchanges);
        }
        NEWS_SAVED = false;
    }

    private void selectedExchangesForAns() {
        if (isOkClicked || !isVeryFirstTime) {
            ANNOUNCE_LIST = null;
        }
        isVeryFirstTime = false;
        selectedExchanges = new ArrayList<String>();
        Component[] exitems = null;
        exitems = announcementsListPanel.getComponents();
        for (int i = 0; i < exitems.length; i++) {
            JCheckBox item = (JCheckBox) exitems[i];
            if (item.isSelected()) {
                ANNOUNCE_LIST = ANNOUNCE_LIST + MiddlePanelSettings.TICKER_RECORD_DELIMETER;
                selectedExchanges.add((String) exchngeHash.get(item.getText()));
                String s = (String) exchngeHash.get(item.getText());
                ANNOUNCE_LIST = ANNOUNCE_LIST + s;
            }
            item = null;
        }

        if (MiddleFilterPanel.ANS_SAVED) {
            selectedExchanges = MiddlePanelSettings.getAnnouncementsList();
        } else {
            MiddlePanelSettings.setAnnouncementsList(selectedExchanges);
        }
        ANS_SAVED = false;
    }

    private void newsModeStarter() {
        MiddleTickerFeeder.activateNewstickerUpdator(MiddleTickerFeeder.isNewsModeSelected);
    }

    private void announcementModeStarter() {
        MiddleTickerFeeder.activateAnnouncementtickerUpdator(MiddleTickerFeeder.isAnnouncementModeSelected);
    }


    public void applyWatchlistFilter() {
        if (isOkClicked || !isVeryFirstTime) {
            WATCH_LIST = null;
        }
        Component[] items = null;
        if (watchListMode) {
            items = watchlistPanel.getComponents();

        }
        /*  else {
            items = classicViewtPanel.getComponents();
        }*/
        Symbols[] symbols = new Symbols[0];
        ArrayList<Symbols> templist = new ArrayList<Symbols>();
        StringBuilder selectedIDs = new StringBuilder();
        watchList = new ArrayList<String>();

        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            if (item.isSelected()) {
                WATCH_LIST = WATCH_LIST + MiddlePanelSettings.TICKER_RECORD_DELIMETER;

                WatchListStore watchListStore = WatchListManager.getInstance().getStore(item.getName());
                templist.add(watchListStore);

                selectedIDs.append(watchListStore.getCaption());
                selectedIDs.append(",");
                String s = watchListStore.getId();
                WATCH_LIST = WATCH_LIST + s;
                watchList.add(watchListStore.getId());

                watchListStore = null;
            }
            item = null;
        }

        symbols = templist.toArray(symbols);
        MiddleTickerFeeder.setFilter(mode, symbols);
        MiddlePanelSettings.setWatchList(watchList);
    }

    public void exchangeModeSelector() {
        if (isOkClicked || !isVeryFirstTime) {
            EXCHANGE_LIST = null;
        }
        selectedExchangesForEx = new ArrayList<String>();
        Component[] exitems = null;
        exitems = exchangeListPanel.getComponents();
        ArrayList<String> tempExchanges = new ArrayList<String>();

        Symbols[] exsymbols = new Symbols[0];
        ArrayList<Symbols> templist1 = new ArrayList<Symbols>();

        for (int i = 0; i < exitems.length; i++) {
            JCheckBox item = (JCheckBox) exitems[i];
            if (item.isSelected()) {
                EXCHANGE_LIST = EXCHANGE_LIST + MiddlePanelSettings.TICKER_RECORD_DELIMETER;
                exchanesList.add((String) exchngeHash.get(item.getText()));
                templist1.add(DataStore.getSharedInstance().getSymbolsObject((String) exchngeHash.get(item.getText())));
                String s = (String) exchngeHash.get(item.getText());
                selectedExchangesForEx.add(s);
                EXCHANGE_LIST = EXCHANGE_LIST + s;
            }
            item = null;
        }

        if (EXG_SAVED) {
            selectedExchangesForEx = MiddlePanelSettings.getExchangeList();
            setSelectedExchanges();

        } else {
            MiddlePanelSettings.setExchangeList(exchanesList);
            exsymbols = templist1.toArray(exsymbols);
            MiddleTickerFeeder.setFilter(mode, exsymbols);
        }
        EXG_SAVED = false;

//        exsymbols = templist1.toArray(exsymbols);
//        MiddleTickerFeeder.setFilter(mode, exsymbols);
    }

    public void setSelectedExchanges() {
        Symbols[] exsymbols = new Symbols[0];
        ArrayList<Symbols> templist1 = new ArrayList<Symbols>();
        ArrayList<String> selectedExchanges = new ArrayList<String>();
        selectedExchanges = MiddlePanelSettings.getExchangeList();
        for (int i = 0; i < selectedExchanges.size(); i++) {
            templist1.add(DataStore.getSharedInstance().getSymbolsObject((String) selectedExchanges.get(i)));
        }
        exsymbols = templist1.toArray(exsymbols);
        MiddleTickerFeeder.setFilter(mode, exsymbols);

    }

    private void changeClassicListState(boolean state) {
        Component[] items = classicViewtPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setEnabled(state);
        }
        classicViewtPanel.repaint();
    }

    private void setClassicListState(boolean state) {
        Component[] items = classicViewtPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setSelected(state);
        }
        classicViewtPanel.repaint();
    }

//--------------------------  For exchanges ----------------------

    private void changeExchangeListState(boolean state) {
        Component[] items = exchangeListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);
            item.setEnabled(state);
        }
        exchangeListPanel.repaint();
    }

    private void setExchangeList(boolean state) {
        Component[] items = exchangeListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setSelected(state);
            item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);

        }
        exchangeListPanel.repaint();
    }

    private void changeMyExchangeListState() {
        Component[] items = exchangeListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setBackground(MiddlePanelSettings.FILTER_PANEL_COLOR);

            item.setEnabled(isExchangeMode);
        }
        exchangeListPanel.repaint();
    }

// -------------------------- For watch lists -------------------------

    private void changeWatchlistState(boolean state) {
        Component[] items = watchlistPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(state);
        }
        watchlistPanel.repaint();
    }

    private void setWatchlistState(boolean state) {
        Component[] items = watchlistPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setSelected(state);
        }

        watchlistPanel.repaint();

    }

    private void changeMyWatchlistState() {
        Component[] items = watchlistPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(isWatchListMode);
        }
        watchlistPanel.repaint();
    }

    //  ------------------------ For News ----------------
    private void changeNewslistState(boolean state) {
        Component[] items = newsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(state);
        }
        newsListPanel.repaint();
    }

    private void setNewslistState(boolean state) {
        Component[] items = newsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setSelected(state);
        }

        newsListPanel.repaint();

    }

    private void changeMyNewslistState() {
        Component[] items = newsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(isNewsMode);
        }
        newsListPanel.repaint();
    }

    //  ----------------------------------- For Announcements ----------------
    private void changeAnnouncementslistState(boolean state) {
        Component[] items = announcementsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(state);
        }
        announcementsListPanel.repaint();
    }

    private void setAnnouncementsListState(boolean state) {
        Component[] items = announcementsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setSelected(state);
        }

        announcementsListPanel.repaint();

    }

    private void changeMyAnnouncementslistState() {
        Component[] items = announcementsListPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            JCheckBox item = (JCheckBox) items[i];
            item.setEnabled(isAnnouncementsMode);
        }
        announcementsListPanel.repaint();
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }


    public void changeState(TickerFilterableComponent state) {

        if (state.equals(exchangeComponent)) {
            MiddleTickerPanel.exkey = "";

            MiddleTickerPanel.newsId = "";
            MiddleTickerPanel.ansId = "";
            MiddleTickerPanel.message = "";

            exchangeComponent.setCheckBoxSelected(true);
            watchlistComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(false);

            watchlistComponent.setPanelVisible(false);
            announcementComponent.setPanelVisible(false);
            newsComponent.setPanelVisible(false);

            mode = TWTypes.TickerFilter.MODE_EXCHANGE;
            isExchangeMode = true;
            isWatchListMode = false;
            isNewsMode = false;
            isAnnouncementsMode = false;

            if (exchangeComponent.isPanelVisible()) {
                changeExchangeListState(true);
            }
            if (watchlistComponent.isPanelVisible()) {
                changeWatchlistState(false);
            }
            if (newsComponent.isPanelVisible()) {
                changeNewslistState(false);
            }
            if (announcementComponent.isPanelVisible()) {
                changeAnnouncementslistState(false);
            }

            showTradedWatchlist.setEnabled(false);
            showAllWatchlist.setEnabled(false);
            runWatchBox.setEnabled(false);
            showTradedEx.setEnabled(true);
            showAllEx.setEnabled(true);
            runExBox.setEnabled(true);

        } else if (state.equals(watchlistComponent)) {
            MiddleTickerPanel.exkey = "";
            MiddleTickerPanel.newsId = "";
            MiddleTickerPanel.ansId = "";
            MiddleTickerPanel.message = "";

            watchlistComponent.setCheckBoxSelected(true);
            exchangeComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(false);

            exchangeComponent.setPanelVisible(false);
            newsComponent.setPanelVisible(false);
            announcementComponent.setPanelVisible(false);
            if (exchangeComponent.isPanelVisible()) {
                changeExchangeListState(false);
            }
            if (watchlistComponent.isPanelVisible()) {
                changeWatchlistState(true);
            }
            if (newsComponent.isPanelVisible()) {
                changeNewslistState(false);
            }
            if (announcementComponent.isPanelVisible()) {
                changeAnnouncementslistState(false);
            }

            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            isExchangeMode = false;
            isWatchListMode = true;
            isNewsMode = false;
            isAnnouncementsMode = false;
            showTradedWatchlist.setEnabled(true);
            showAllWatchlist.setEnabled(true);
            runWatchBox.setEnabled(true);
            showTradedEx.setEnabled(false);
            showAllEx.setEnabled(false);
            runExBox.setEnabled(false);

        } else if (state.equals(newsComponent)) {

            MiddleTickerPanel.exkey = "";
            MiddleTickerPanel.newsId = "";
            MiddleTickerPanel.ansId = "";
            MiddleTickerPanel.message = "";

            newsComponent.setCheckBoxSelected(true);
            exchangeComponent.setCheckBoxSelected(false);
            watchlistComponent.setCheckBoxSelected(false);
            announcementComponent.setCheckBoxSelected(false);

            exchangeComponent.setPanelVisible(false);
            watchlistComponent.setPanelVisible(false);
            announcementComponent.setPanelVisible(false);
            if (exchangeComponent.isPanelVisible()) {
                changeExchangeListState(false);
            }
            if (watchlistComponent.isPanelVisible()) {
                changeWatchlistState(false);
            }
            if (newsComponent.isPanelVisible()) {
                changeNewslistState(true);
            }
            if (announcementComponent.isPanelVisible()) {
                changeAnnouncementslistState(false);
            }

            mode = TWTypes.TickerFilter.MODE_NEWS;
            isExchangeMode = false;
            isWatchListMode = false;
            isNewsMode = true;
            isAnnouncementsMode = false;
            showTradedWatchlist.setEnabled(false);
            showAllWatchlist.setEnabled(false);
            runWatchBox.setEnabled(false);
            showTradedEx.setEnabled(false);
            showAllEx.setEnabled(false);
            runExBox.setEnabled(false);

        } else if (state.equals(announcementComponent)) {

            MiddleTickerPanel.exkey = "";
            MiddleTickerPanel.newsId = "";
            MiddleTickerPanel.ansId = "";
            MiddleTickerPanel.message = "";

            announcementComponent.setCheckBoxSelected(true);
            exchangeComponent.setCheckBoxSelected(false);
            watchlistComponent.setCheckBoxSelected(false);
            newsComponent.setCheckBoxSelected(false);
            exchangeComponent.setPanelVisible(false);
            watchlistComponent.setPanelVisible(false);
            newsComponent.setPanelVisible(false);

            if (exchangeComponent.isPanelVisible()) {
                changeExchangeListState(false);
            }
            if (watchlistComponent.isPanelVisible()) {
                changeWatchlistState(false);
            }
            if (newsComponent.isPanelVisible()) {
                changeNewslistState(false);
            }
            if (announcementComponent.isPanelVisible()) {
                changeAnnouncementslistState(true);
            }

            mode = TWTypes.TickerFilter.MODE_ANNOUNCEMENT;
            isExchangeMode = false;
            isWatchListMode = false;
            isNewsMode = false;
            isAnnouncementsMode = true;
            showTradedWatchlist.setEnabled(false);
            showAllWatchlist.setEnabled(false);
            runWatchBox.setEnabled(false);
            showTradedEx.setEnabled(false);
            showAllEx.setEnabled(false);
            runExBox.setEnabled(false);

        }
    }

    public void resizeFrame() {

        if (exchangeComponent.isPanelVisible()) {
            changeMyExchangeListState();
        }
        if (watchlistComponent.isPanelVisible()) {
            changeMyWatchlistState();
        }
        if (newsComponent.isPanelVisible()) {
            changeMyNewslistState();
        }
        if (announcementComponent.isPanelVisible()) {
            changeMyAnnouncementslistState();
        }
        this.pack();
        this.doLayout();
        this.popupMenu.getPopupMenu().pack();

    }
}


