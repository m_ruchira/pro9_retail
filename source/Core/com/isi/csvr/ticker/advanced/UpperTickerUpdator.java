package com.isi.csvr.ticker.advanced;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 25, 2005
 * Time: 2:52:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpperTickerUpdator extends Thread {

    private boolean active = true;

    public UpperTickerUpdator() {
        super("Summary UpperTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            UpperTickerFeeder.isQueueEmpty = true;
            UpperTickerFeeder.isExchangeModeSelected = false;
            UpperTickerFeeder.tradeEQueue.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopThread() {
        active = false;
        super.stop();
    }

    public void run() {
        int tickerStatus;
        boolean played = false;
        while (active) {
            Symbols[] symbols = UpperTickerFeeder.getFilter();
            try {
                if (UpperTickerFeeder.isVisible() && !(UpperTickerFeeder.isNewsModeSelected)) {
                    for (Symbols symbolsObj : symbols) {
                        if (symbolsObj != null) {
                            played = false;
                            String[] keys = symbolsObj.getSymbols();
                            for (String key : keys) {
                                try {
                                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                                    if ((stock != null && stock.isSymbolEnabled() && UpperFilterPanel.selectedExchangesForEx.contains(stock.getExchange())) || (UpperTickerFeeder.isWatchListModeSelected)) {
                                        Exchange exch = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key));
                                        //---- default load ------

//                                        if ((UpperFilterPanel.showExTraded && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (UpperFilterPanel.showWatchListTraded && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
//                                            if ((UpperFilterPanel.runExOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (UpperFilterPanel.runWatchListOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {

                                        if ((UpperFilterPanel.showExTraded && UpperPanelSettings.MODE == UpperPanelSettings.exchangelist) || (UpperFilterPanel.showWatchListTraded && UpperPanelSettings.MODE == UpperPanelSettings.watchlist)) {
                                            if ((UpperFilterPanel.runExOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (UpperFilterPanel.runWatchListOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                if (stock.getLastTradeValue() > 0) {
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown()))) {
                                                        CommonTickerObject oTickerData = new CommonTickerObject();
                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }
                                                        UpperTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedUpper();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            UpperTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN) || (exch.getMarketStatus() == Constants.TRADING_AT_LAST))) && stock.isLastTradeUpdatedUpper()) { //---- Eliminated == || (exch.getMarketStatus() == Constants.OPEN))
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) {
                                                        stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        UpperTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedUpper();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            UpperTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        } else if ((UpperFilterPanel.showExAll && UpperPanelSettings.MODE == UpperPanelSettings.exchangelist) || (UpperFilterPanel.showWatchListAll && UpperPanelSettings.MODE == UpperPanelSettings.watchlist)) {
                                            if ((UpperFilterPanel.runExOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (UpperFilterPanel.runWatchListOnClose && UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                //                                            if ((!exch.isDefault())) { === Eliminated
                                                if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                    //                                                    stock.setLastTradeUpdatedUpper();
                                                    CommonTickerObject oTickerData = new CommonTickerObject();

                                                    if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                    }

                                                    UpperTickerFeeder.addData(oTickerData);
                                                    played = true;
                                                    delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                } else {
                                                    /* if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                  (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {*/
                                                    if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {
                                                        CommonTickerObject oTickerData = new CommonTickerObject();
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        UpperTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                    }
                                                }
                                                //                                            }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN) || (exch.getMarketStatus() == Constants.TRADING_AT_LAST)))) { //--- Eliminated --  || (exch.getMarketStatus() == Constants.OPEN) --Shanika
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                        //                                                    stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }
                                                        UpperTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        /*if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {*/
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {

                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            UpperTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(UpperTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //----------------- end default --------------------

                                    }
                                    stock = null;
                                } catch (Exception e) {
                                    System.out.println("Error in key " + key); //todo fix the market code issue
                                    e.printStackTrace();
                                    delay(100);
                                }
                            }
                            if (!played) { // nothing played in this round
                                delay(1000);
                            }
                            keys = null;
                        }
                    }
                } else {
                    delay(2000);
                }
            } catch (Exception e) {
                delay(1000);
            }
            delay(UpperTickerFeeder.getSummaryTickerDelay());
        }
    }

    private void delay(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
