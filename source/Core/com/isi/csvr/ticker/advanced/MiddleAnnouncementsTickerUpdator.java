package com.isi.csvr.ticker.advanced;

import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementStore;

import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 31, 2008
 * Time: 10:12:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class MiddleAnnouncementsTickerUpdator extends Thread {

    private boolean active = true;
    private LinkedList dataStore;

    public MiddleAnnouncementsTickerUpdator() {
        super("Summary UpperTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            MiddleTickerFeeder.isQueueEmpty = true;
            MiddleTickerFeeder.isAnnouncementModeSelected = false;
            MiddleTickerFeeder.announcementQueue.clear();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        while (active) {
            if (MiddleTickerFeeder.announcementQueue != null && MiddleTickerFeeder.announcementQueue.isEmpty()) {
                announcementModeSelectorLower();
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void announcementModeSelectorLower() {
        dataStore = AnnouncementStore.getSharedInstance().getLastAnnouncements();
        for (int i = 0; i < dataStore.size(); i++) {
            MiddleTickerFeeder.addAnnouncementData((Announcement) dataStore.get(i));
        }
        MiddleTickerFeeder.isAnnouncementModeSelected = true;
    }
}
