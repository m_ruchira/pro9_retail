package com.isi.csvr.ticker.advanced;

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.news.*;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Settings;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 17, 2008
 * Time: 12:50:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class NewsTickerMouseListener extends MouseAdapter
        implements ListSelectionListener {


    public static final int ONLINE = 0;
    public static final int SEARCHED = 1;
    public static final int COMPANY = 2;
    public static boolean UPPER_SELECTED = false;
    public static boolean MIDDLE_SELECTED = false;
    public static boolean LOWER_SELECTED = false;
    public static String newsId;

    private JTable table;
    private String message;
    private long time;
    private String title;
//    private NEWS_TYPES mode;

    public NewsTickerMouseListener(JTable oTable, NEWS_TYPES mode) {
        this.table = oTable;
        //this.mode = mode;
    }

    // ------------- By Shanika-------------------
    public NewsTickerMouseListener(NEWS_TYPES all) {
        //To change body of created methods use File | Settings | File Templates.
    }

    /*public void setMode(NEWS_TYPES mode) {
        this.mode = mode;
    }*/

    /**
     * Called from the mouse listener
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 0 && SwingUtilities.isLeftMouseButton(e)) {
            if ((LowerTickerFeeder.isNewsModeSelected && LOWER_SELECTED) || (UpperTickerFeeder.isNewsModeSelected && UPPER_SELECTED) || (MiddleTickerFeeder.isNewsModeSelected && MIDDLE_SELECTED)) {

                newsSelected();
                newsId = "";
            }
        } else if (SwingUtilities.isRightMouseButton(e)) {
//            NewsSourceSelectionWindow.getSharedInstance();
//            new NewsSourceSelectionWindow();
        }

    }

    /**
     * Called from the list selection listener
     */
    public void valueChanged(ListSelectionEvent e) {
        //selected();
    }


    public void newsSelected() {
        Thread thread = new Thread("NewsMouseListener-Select") {
            public void run() {
                if ((message == null) || ((message.equals("")))) {
                    String id = (String) newsId;
//                        String providerID = (String) table.getModel().getValueAt(table.getSelectedRow(), 2);

                    News news = null;
                    String language = null;
                    /*if (mode == NEWS_TYPES.ALL) {
                        news = NewsStore.getSharedInstance().getNews(id);
                        language = news.getLanguage();
                    } else if (mode == NEWS_TYPES.SEARCHED) {
                        news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                        language = news.getLanguage();
                    }*/
                    if (!id.equals("")) {
                        news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                        }
                    }
                    if (!id.equals("")) {
                        language = news.getLanguage();
                        if (Settings.isConnected()) {
                            Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_NEWS);
//                                NewsProvider np = (NewsProvider)NewsProvidersStore.getSharedInstance().getProvider(providerID.toUpperCase());
                            NewsProvider np = NewsProvidersStore.getSharedInstance().getProvider(news.getDefaultCategory());
                            Enumeration<String> providers = NewsProvidersStore.getSharedInstance().getProviderKeys();
                            while (providers.hasMoreElements()) {
                                String provider = providers.nextElement();
                                if (news.isContainInCategory(provider)) {
                                    np = NewsProvidersStore.getSharedInstance().getProvider(provider);
                                    break;
                                }
                            }
//                                ServerSupportedNewsProvider ssnp = NewsProvidersStore.getSharedInstance().ssnProviders.get(getRealID(providerID));
                            if (np == null) {
                                SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, null, Constants.CONTENT_PATH_SECONDARY);  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                            } else {
                                SendQFactory.addNewsBodyRequest(news.getExchange(), id, language, np.getContentIp(), np.getPath());  //, NewsProvidersStore.getSharedInstance().getProviderPath(news.getNewsProvider()));
                            }
                        }
                        news = null;
                    }
                } else {
                    String id = (String) newsId;
                    if (!id.equals("")) {
                        News news = NewsStore.getSharedInstance().getNews(id);
                        if (news == null) {
                            news = SearchedNewsStore.getSharedInstance().getSearchedNews(id);
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        } else {
                            NewsStore.getSharedInstance().showNews(id);
                        }
                        /*if (mode == NEWS_TYPES.ALL) {
                            NewsStore.getSharedInstance().showNews(id);
                        } else if (mode == NEWS_TYPES.SEARCHED) {
                            SearchedNewsStore.getSharedInstance().showNews(id);
                        }*/
                    }
                }
            }
        };
        thread.start();
//        }
    }


}
