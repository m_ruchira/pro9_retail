package com.isi.csvr.ticker.advanced;

import com.isi.csvr.*;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.*;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TickerFrame;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.StringTokenizer;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jul 7, 2008
 * Time: 10:26:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class UpperTicker extends JPanel implements
        MouseListener, InternalFrameListener, Themeable,
        ActionListener, ExchangeListener, WatchlistListener, PopupMenuListener, ApplicationListener {

    public final static int MODE_ALL = 0;
    public final static int MODE_SYMBOL = 2;
    public static int CURRENT_TICKER_SPEED = 1;
    public static TWMenuItem mnuTickerFixedTop;
    public static TWMenuItem mnuTickerFixedBottom;
    public static int SLOW_DELAY = 600;
    public static boolean IS_DEFAULT = false;
    public static String tickerSpeed = "SPEED_SLOW";
    private static JPopupMenu popupMenu;
    private static TWMenuItem showHideTitle;
    public TWMenuItem floatMenu;
    public TWMenu fixMenu;
    String previousExchange = "";
    PanelSetter panelSetter = new PanelSetter();
    CommonSettings globalS = new CommonSettings();
    private ViewSetting settings;
    private UpperTickerPanel tep;
    private TWMenuItem mnuFont;
    private TWMenuItem showHideSymbol;
    private TWMenuItem showHideChange;
    private TWMenuItem mnuCloseTicker;
    private TWRadioButtonMenuItem mnuSpeedSlow;
    private TWRadioButtonMenuItem mnuSpeedMedium;
    private TWRadioButtonMenuItem mnuSpeedFast;
    private TWMenu tickerFilter;
    private TWMenuItem mnuSummaryTicker;
    private TWMenuItem mnuClassicTicker;
    private TWMenu mnuSummaryTickerSpeed;
    private ProcessListener parentFrame;


    /**
     * Constructs a new instance.
     */
    public UpperTicker(ViewSetting settings) {
        try {
            this.settings = settings;
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Theme.registerComponent(this);
        popupMenu.addPopupMenuListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        WatchListManager.getInstance().addWatchlistListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public UpperTicker() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public void actions_Mouse_Click() {
        {
            if (!Ticker.settings.isHeaderVisible()) {
                showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
                showHideTitle.setActionCommand("SHOW_TITLEBAR");
                showHideTitle.setIconFile("showtitle.gif");
            } else {
                showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
                showHideTitle.setActionCommand("HIDE_TITLEBAR");
                showHideTitle.setIconFile("hidetitle.gif");
            }
            if (Client.getInstance().isTickerFixed()) {
                if (Ticker.tickerFixedToTopPanel) {
                    mnuTickerFixedTop.setEnabled(false);
                    mnuTickerFixedBottom.setEnabled(true);
                } else {
                    mnuTickerFixedTop.setEnabled(true);
                    mnuTickerFixedBottom.setEnabled(false);
                }
                popupMenu.getComponent(6).setVisible(true);
                popupMenu.getComponent(7).setVisible(true);

            } else {
                mnuTickerFixedBottom.setEnabled(true);
                mnuTickerFixedTop.setEnabled(true);
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(true);
            }
            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(false);
                popupMenu.getComponent(11).setVisible(false);
            } else {
                popupMenu.getComponent(11).setVisible(true);
            }

            if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                popupMenu.getComponent(5).setVisible(true);
                popupMenu.getComponent(4).setVisible(true);  // -- change
                popupMenu.getComponent(3).setVisible(true);  //---symbol
                popupMenu.getComponent(9).setVisible(true);

            } else if (UpperTickerFeeder.isAnnouncementModeSelected || UpperTickerFeeder.isNewsModeSelected) {
                popupMenu.getComponent(4).setVisible(false);  // -- change
                popupMenu.getComponent(3).setVisible(false);  //---symbol
                popupMenu.getComponent(9).setVisible(false);//-close

            }
            boolean isTimeNSalesEnable = false;
            if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
                while (exchanges.hasMoreElements()) {
                    Exchange exchange = (Exchange) exchanges.nextElement();
                    if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_TradeTicker)) {   //
                        if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_MarketTimeAndSales)) {
                            isTimeNSalesEnable = true;
                            break;
                        }
                    }
                }
            }
            /*   if ((Settings.isShowSummaryTicker()) && (ExchangeStore.getSharedInstance().defaultCount() > 0)) {
                if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_MarketTimeAndSales)) {
                        popupMenu.getComponent(0).setVisible(true);
                    } else {
                        popupMenu.getComponent(0).setVisible(false);
                    }
                } else {
                    popupMenu.getComponent(0).setVisible(false);
                }
            }*/

            if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                if (isTimeNSalesEnable) {
                    popupMenu.getComponent(0).setVisible(true);
                } else {
                    popupMenu.getComponent(0).setVisible(false);
                }
            } else {
                popupMenu.getComponent(0).setVisible(false);

            }
//            GUISettings.showPopup(popupMenu, this, e.getX(), e.getY());
        }
    }

    public TWMenuItem getTitleBarMenu() {
        return showHideTitle;
    }

    /**
     * <P>Initialize the Ticker object. Creates the sub objects that constitutes
     * the StockeNet Ticker and allowing them to run as seperate processes.
     */
    public void init() throws Exception {
        popupMenu = new JPopupMenu();
        showHideTitle = new TWMenuItem();
        mnuFont = new TWMenuItem();
        showHideSymbol = new TWMenuItem();
        showHideChange = new TWMenuItem();
        if (Language.isLTR()) {
            UpperPanelSettings.ENGLISH_VERSION = true;
        } else {
            UpperPanelSettings.ENGLISH_VERSION = false;
        }
        showHideTitle.addActionListener(this);
        showHideTitle.setIconFile("showtitle.gif");
        if (!Ticker.settings.isHeaderVisible()) {
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
        } else {
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
        }
        mnuFont.addActionListener(this);
        mnuFont.setActionCommand("FONT");
        mnuFont.setIconFile("font.gif");
        mnuFont.setText(Language.getString("FONT"));
        showHideSymbol.addActionListener(this);
        showHideSymbol.setIconFile("showsymbol.gif");
        showHideSymbol.setActionCommand("SHOW_SYMBOL");
        showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));
        showHideChange.addActionListener(this);
        showHideChange.setIconFile("showchange.gif");
        showHideChange.setActionCommand("SHOW_PER_CHANGE");
        showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));
        mnuSummaryTicker = new TWMenuItem(Language.getString("SHOW_SUMMARY_TICKER"), "showsmryticker.gif");
        mnuSummaryTicker.addActionListener(this);
        /* if (UpperPanelSettings.showSummaryTicker) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
        } else {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY");
        }*/
        mnuSummaryTicker.setVisible(true);
        popupMenu.add(mnuSummaryTicker);

        mnuClassicTicker = new TWMenuItem(Language.getString("CUSTOM_TICKER"), "ticker_cla.gif");
        mnuClassicTicker.addActionListener(this);
        mnuClassicTicker.setActionCommand("CUSTOM_TICKER");
        popupMenu.add(mnuClassicTicker);

        mnuSummaryTickerSpeed = new TWMenu(Language.getString("TICKER_SPEED"), "tickerspeed.gif");
        ButtonGroup speedGroup = new ButtonGroup();
        mnuSpeedSlow = new TWRadioButtonMenuItem(Language.getString("SPEED_SLOW"));
        mnuSpeedSlow.addActionListener(this);
        mnuSpeedSlow.setActionCommand("SPEED_SLOW");
        speedGroup.add(mnuSpeedSlow);
        mnuSummaryTickerSpeed.add(mnuSpeedSlow);
        mnuSpeedMedium = new TWRadioButtonMenuItem(Language.getString("SPEED_MEDIUM"));
        mnuSpeedMedium.addActionListener(this);
        mnuSpeedMedium.setActionCommand("SPEED_MEDIUM");
        speedGroup.add(mnuSpeedMedium);
        mnuSummaryTickerSpeed.add(mnuSpeedMedium);
        mnuSpeedFast = new TWRadioButtonMenuItem(Language.getString("SPEED_FAST"));
        mnuSpeedFast.addActionListener(this);
        mnuSpeedFast.setActionCommand("SPEED_FAST");
        speedGroup.add(mnuSpeedFast);
        mnuSummaryTickerSpeed.add(mnuSpeedFast);
        applySavedTickerVariables();
        mnuCloseTicker = new TWMenuItem(Language.getString("CLOSE"), "close.gif");
        mnuCloseTicker.addActionListener(this);
        mnuCloseTicker.setActionCommand("CLOSE_TICKER");

        fixMenu = new TWMenu(Language.getString("FIX_TICKER"), "floatticker.gif");
        floatMenu = new TWMenuItem(Language.getString("FLOAT_TICKER"), "floatticker.gif");
        floatMenu.addActionListener(this);
        floatMenu.setText(Language.getString("FLOAT_TICKER"));
        floatMenu.setActionCommand("FLOAT_TICKER");

        ButtonGroup tickerFixingGroup = new ButtonGroup();
        mnuTickerFixedTop = new TWMenuItem(Language.getString("TICKER_FIXED_TOP"));
        mnuTickerFixedTop.addActionListener(this);
        mnuTickerFixedTop.setActionCommand("FIX_TOP");
//        tickerFixingGroup.add(mnuTickerFixedTop);
        fixMenu.add(mnuTickerFixedTop);


        mnuTickerFixedBottom = new TWMenuItem(Language.getString("TICKER_FIXED_BOTTOM"));
        mnuTickerFixedBottom.addActionListener(this);
        mnuTickerFixedBottom.setActionCommand("FIX_BOTTOM");
//        tickerFixingGroup.add(mnuTickerFixedBottom);
        fixMenu.add(mnuTickerFixedBottom);
        tickerFilter = new TWMenu(Language.getString("FILTER"), "filter.gif");
        tickerFilter.getPopupMenu().addPopupMenuListener(this);
        popupMenu.setLightWeightPopupEnabled(false);
        popupMenu.add(showHideTitle);
        //popupMenu.add(allwaysOnTop);
        popupMenu.add(showHideSymbol);
        popupMenu.add(showHideChange);
        popupMenu.add(mnuFont);
        popupMenu.add(floatMenu);
        popupMenu.add(fixMenu);
        popupMenu.add(new TWSeparator());
        popupMenu.add(mnuSummaryTickerSpeed);
        popupMenu.add(tickerFilter);
        popupMenu.add(mnuCloseTicker);
        this.setLayout(new BorderLayout());
        this.setBackground(UpperPanelSettings.TICKER_BACKGROUND); // Color.black);

        // Add Listeners for listening to the resizing evets
        SymComponent aSymComponent = new SymComponent();
        this.addComponentListener(aSymComponent);

        UIManager.addPropertyChangeListener(new UISwitchListener(this));
        applyTheme();
        GUISettings.applyOrientation(popupMenu);

    } // End Method

    public void applySavedTickerVariables() {
        String savedSpeed = tickerSpeed;
        if ((savedSpeed != null) && (savedSpeed.equals("SPEED_SLOW") || savedSpeed.equals("SPEED_MEDIUM") || savedSpeed.equals("SPEED_FAST"))) {
            setTickerSpeed(savedSpeed);
        } else {
            setTickerSpeed("SPEED_FAST");
        }
        UpperPanelSettings.isSymbolShowing = Settings.getBooleanItem("U_TICKER_SHOW_SYMBOL");
        setSymbolCaption();
        UpperPanelSettings.isChangeMode = Settings.getBooleanItem("U_TICKER_CHANGE_MODE");
        setChangeOption();
        UpperPanelSettings.showSummaryTicker = Settings.getBooleanItem("U_SUMMARY_TICKER");
        setTradeSummeryOptions();

    }

    private void setTradeSummeryOptions() {
        if (UpperPanelSettings.showSummaryTicker) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
        } else {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY");
            mnuSummaryTickerSpeed.setEnabled(false);
            setTickerSpeed("SPEED_SLOW");
        }
    }

    private void setTickerSpeed(String speed) {
        if (speed.equals("SPEED_SLOW")) {
            tickerSpeed = "SPEED_SLOW";
            mnuSpeedSlow.setSelected(true);
            UpperTickerFeeder.setSummaryTickerDelay(1200);
            CURRENT_TICKER_SPEED = 1;
            UpperTickerPanel.PIXEL_INCREMENT = 1;

        } else if (speed.equals("SPEED_MEDIUM")) {
            tickerSpeed = "SPEED_MEDIUM";
            mnuSpeedMedium.setSelected(true);
            UpperTickerFeeder.setSummaryTickerDelay(450);
            CURRENT_TICKER_SPEED = 2;
            UpperTickerPanel.PIXEL_INCREMENT = 2;

        } else if (speed.equals("SPEED_FAST")) {
            tickerSpeed = "SPEED_FAST";
            mnuSpeedFast.setSelected(true);
            UpperTickerFeeder.setSummaryTickerDelay(100);
            CURRENT_TICKER_SPEED = 3;
            UpperTickerPanel.PIXEL_INCREMENT = 6;
        }
        Settings.saveItem("TICKER_SPEED", speed);
    }


    public void applyTheme() {
        try {
            UpperPanelSettings.TICKER_BACKGROUND = Theme.getColor("UPPER_TICKER_BGCOLOR");
            UpperPanelSettings.NO_CHANGE_COLOR = Theme.getColor("UPPER_TICKER_NO_CHANGE_FGCOLOR");
            UpperPanelSettings.UP_COLOR = Theme.getColor("UPPER_TICKER_UP_FGCOLOR");
            UpperPanelSettings.DOWN_COLOR = Theme.getColor("UPPER_TICKER_DOWN_FGCOLOR");
            UpperPanelSettings.TICKER_BORDER_COLOR = Theme.getColor("UPPER_TICKER_BORDER_COLOR");
            UpperPanelSettings.SYMBOL_HIGHLIGHT_COLOR = Theme.getColor("UPPER_SYMBOL_HIGHLIGHT_COLOR");
            UpperPanelSettings.NEWS_TITLE_COLOR = Theme.getColor("UPPER_TICKER_NEWS_TITLE_COLOR");
            UpperPanelSettings.NEWS_HIGHLIGHT_COLOR = Theme.getColor("UPPER_TICKER_NEWS_HIGHLIGHT_COLOR");
            UpperPanelSettings.ANNOUNCEMENT_TITLE_COLOR = Theme.getColor("UPPER_TICKER_ANNOUNCEMENT_TITLE_COLOR");
            UpperPanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR = Theme.getColor("UPPER_TICKER_ANNOUNCEMENT_HIGHLIGHT_COLOR");
            UpperPanelSettings.FILTER_PANEL_COLOR = Theme.getColor("FILTER_PANEL_BGCOLOR");
            UpperPanelSettings.FILTER_PANEL_HEADER_COLOR = Theme.getColor("FILTER_PANEL_HEADER_BGCOLOR");


        } catch (Exception e) {
            e.printStackTrace();
            UpperPanelSettings.TICKER_BACKGROUND = Color.black;
            UpperPanelSettings.NO_CHANGE_COLOR = Color.white;
            UpperPanelSettings.FILTER_PANEL_COLOR = Color.WHITE;
        }
        UpperTickerPanel.getInstance().fontAdjuster(UpperTickerPanel.getInstance());
        UpperTickerPanel.getInstance().adjustControls();
        UpperTickerPanel.getInstance().redrawTicker();
        UpperTickerPanel.getInstance().adjustTickerControls();
        SharedMethods.updateComponent(UpperTickerPanel.getInstance());
        SwingUtilities.updateComponentTreeUI(popupMenu);
    }


    public JPopupMenu getMenu() {
        return popupMenu;
    }

//************************************************************************
//     Section that handles the Mouse Events

    //************************************************************************

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            if (!settings.isHeaderVisible()) {
                showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
                showHideTitle.setIconFile("showtitle.gif");
            } else {
                showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
                showHideTitle.setIconFile("hidetitle.gif");
            }
            if (Client.getInstance().isTickerFixed()) {
                mnuTickerFixedBottom.setEnabled(false);
            } else {
                mnuTickerFixedTop.setEnabled(false);
            }

            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(false);
                popupMenu.getComponent(11).setVisible(false);
            } else {
                popupMenu.getComponent(11).setVisible(true);
            }

            if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                popupMenu.getComponent(5).setVisible(true);
                popupMenu.getComponent(4).setVisible(true);  // -- change
                popupMenu.getComponent(3).setVisible(true);  //---symbol
            } else if (UpperTickerFeeder.isAnnouncementModeSelected || UpperTickerFeeder.isNewsModeSelected) {
                popupMenu.getComponent(5).setVisible(false);  // --speed
                popupMenu.getComponent(4).setVisible(false);  // -- change
                popupMenu.getComponent(3).setVisible(false);  //---symbol
            }
            System.out.println("Detachbility : " + Client.getInstance().getTickerFrame().isDetached());
            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(5).setVisible(false);
            } else {
                popupMenu.getComponent(5).setVisible(true);
            }
            if ((Settings.isShowSummaryTicker()) && (ExchangeStore.getSharedInstance().defaultCount() > 0)) {
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_SymbolTimeAndSales)) {
                        popupMenu.getComponent(0).setVisible(true);
                    } else {
                        popupMenu.getComponent(0).setVisible(false);
                    }
                } else {
                    popupMenu.getComponent(0).setVisible(false);
                }
            }
//            GUISettings.showPopup(popupMenu, this, e.getX(), e.getY());
        }
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        IS_DEFAULT = false;
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = exchanges.nextElement();
            if (exchange.isDefault()) {
                IS_DEFAULT = true;
            }
        }
        if (!IS_DEFAULT) {
            TWTypes.TickerFilter tickerMode;
            String currentMode = Settings.getTickerFilter();
            if (currentMode.equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString())) {
                System.out.println("current mode is watchlist");
            } else {
                Settings.setTickerFilter(TWTypes.TickerFilter.MODE_WATCHLIST.toString());
                Symbols[] symbols = new Symbols[0];
                ArrayList<Symbols> templist = new ArrayList<Symbols>();
                StringBuilder selectedIDs = new StringBuilder();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        templist.add(watchlist);
                        selectedIDs.append(watchlist.getId());
                        selectedIDs.append(",");
                        watchlist = null;
                    }
                }
                symbols = templist.toArray(symbols);
                TradeFeeder.setFilter(TWTypes.TickerFilter.MODE_WATCHLIST, symbols);
                Settings.setTickerFilterData(selectedIDs.toString());
            }
        }
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {

    }

    public void Ticker_ComponentResized() {
    }

    public void setHideShowMenuCaption() {
        if (!Ticker.settings.isHeaderVisible()) {
            showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
            Ticker.IS_TITLE_SHOW = false;
        } else {
            showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
            Ticker.IS_TITLE_SHOW = true;
        }
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }

// Adjust the control details when the UpperTicker is resized

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("CLOSE_TICKER")) {
            globalS.deselectEquityTicker(false);
            panelSetter.setEquityPanel(false);
            SharedSettings.Frame_Header_Changed_by = 1;
            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();

            Client.getInstance().getTickerFrame().resizeFrame();
            HolderPanel.getInstance().updateUI();
            SharedMethods.updateComponent(UpperTickerPanel.getInstance());
            Client.getInstance().getDesktop().revalidate();

        }
        if (e.getActionCommand().equals("HIDE_TITLEBAR")) {
            InternalFrame frame = (InternalFrame) Ticker.settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);

            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();
            SharedSettings.Frame_Header_Changed_by = 1;
            Client.getInstance().getTickerFrame().resizeFrame();
            setHideShowMenuCaption();
            frame = null;
        } else if (e.getActionCommand().equals("SHOW_TITLEBAR")) {
            SharedSettings.Frame_Header_Changed_by = 0;
            InternalFrame frame = (InternalFrame) Ticker.settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);

            setHideShowMenuCaption();
            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();
            Client.getInstance().getTickerFrame().resizeFrame();
            HolderPanel.getInstance().updateUI();
            SharedMethods.updateComponent(UpperTickerPanel.getInstance());
            Client.getInstance().getDesktop().revalidate();
            frame = null;
        } else if (e.getActionCommand().equals("FONT")) {
            FontChooser oFontChooser = new FontChooser(Client.getInstance().getFrame(), true);
            Font oFont = null;
            oFont = new TWFont(UpperPanelSettings.FONT_TYPE, UpperPanelSettings.BIG_FONT_STYLE, UpperPanelSettings.FONT_BIG);
            oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"), oFont);
            if (oFont != null) {
                UpperPanelSettings.BIG_FONT_STYLE = oFont.getStyle();
                UpperPanelSettings.SMALL_FONT_STYLE = oFont.getStyle();
                UpperPanelSettings.FONT_TYPE = oFont.getName();
                UpperPanelSettings.FONT_BIG = oFont.getSize();
                UpperPanelSettings.FONT_SMALL = UpperPanelSettings.FONT_BIG - 3;
                UpperTickerPanel.reloadFont(UpperTickerPanel.getInstance());

                if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                    UpperTickerPanel.getInstance().reloadFontforExchange();
                } else if (UpperTickerFeeder.isNewsModeSelected) {
                    UpperTickerPanel.getInstance().reloadFontforNews();

                } else if (UpperTickerFeeder.isAnnouncementModeSelected) {
                    UpperTickerPanel.getInstance().reloadFontforAnnouncements();
                }

                UpperTickerPanel.getInstance().adjustControls();
                UpperTickerPanel.getInstance().redrawTicker();

                HolderPanel.getInstance().getPreferredSize();
                TickerFrame.setSizeOnFrame();
                Ticker.getInstance().updateUI();
                SharedSettings.Frame_Header_Changed_by = 1;
                Client.getInstance().getTickerFrame().resizeFrame();
                HolderPanel.getInstance().updateUI();

                if (DetachableRootPane.IS_DETACHED) {
                    DetachableRootPane.FONT_SELECTED = true;
                    DetachableRootPane.getInstance().fontResized();
                }
                SharedMethods.updateComponent(UpperTickerPanel.getInstance());
                Client.getInstance().getDesktop().revalidate();
            }
        }
        /*  else if (e.getActionCommand().equals("SUMMARY")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            Settings.setShowSummaryTicker(true);

            mnuSummaryTickerSpeed.setEnabled(true);
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
            RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());

        }*/
        else if (e.getActionCommand().equals("SPEED_SLOW")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_MEDIUM")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_FAST")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SHOW_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_PER_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_SYMBOL")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("SHOW_DESCRIPTION")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("CUSTOM_TICKER")) {
            SharedSettings.showClassicTicker();
        } else if (e.getActionCommand().equals("FIX_TOP")) {
            Ticker.tickerSetToTop = true;
            mnuTickerFixedTop.setSelected(true);
            Client.getInstance().fixTicker();
        } else if (e.getActionCommand().equals("FIX_BOTTOM")) {
            Ticker.tickerSetToTop = true;
            mnuTickerFixedBottom.setSelected(true);
            Client.getInstance().fixTickerToBottom();
        } else if (e.getActionCommand().equals("FLOAT_TICKER")) {
            Client.getInstance().floatTicker();
            mnuTickerFixedBottom.setSelected(false);
            mnuTickerFixedTop.setSelected(false);
        } else if (e.getActionCommand().equals("SUMMARY")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
//            Settings.setShowSummaryTicker(true);
            UpperPanelSettings.showSummaryTicker = true;

            Settings.saveItem("U_SUMMARY_TICKER", "" + UpperPanelSettings.showSummaryTicker);
            if (UpperPanelSettings.MODE == UpperPanelSettings.exchangelist) {
                UpperTickerFeeder.isExchangeModeSelected = true;
            } else if (UpperPanelSettings.MODE == UpperPanelSettings.watchlist) {
                UpperTickerFeeder.isWatchListModeSelected = true;

            }
            if (UpperTickerFeeder.isVisible()) {
                UpperTickerFeeder.activateSummaryTickerUpdator(true);
            }
            mnuSummaryTickerSpeed.setEnabled(true);
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
//            RequestManager.getSharedInstance().sendRemoveRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|"+ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
            ArrayList<String> exchangeList = UpperPanelSettings.getExchangeList();
            for (String anExchangeList : exchangeList) {
                if (ExchangeStore.isValidIinformationType(anExchangeList, Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(anExchangeList, Meta.IT_MarketTimeAndSales)) {
                        RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + anExchangeList);
                    }
                }
            }

        } else if (e.getActionCommand().equals("SUMMARY_TRADE")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
//            Settings.setShowSummaryTicker(false);
            UpperPanelSettings.showSummaryTicker = false;
            Settings.saveItem("U_SUMMARY_TICKER", "" + UpperPanelSettings.showSummaryTicker);

            if (UpperTickerFeeder.isVisible()) {
                UpperTickerFeeder.activateSummaryTickerUpdator(false);
            }

            if (UpperPanelSettings.MODE == UpperPanelSettings.exchangelist) {
                UpperTickerFeeder.isExchangeModeSelected = true;
            } else if (UpperPanelSettings.MODE == UpperPanelSettings.watchlist) {
                UpperTickerFeeder.isWatchListModeSelected = true;

            }
            mnuSummaryTickerSpeed.setEnabled(false);
            setTickerSpeed("SPEED_SLOW");
            mnuSummaryTicker.setActionCommand("SUMMARY");

            String exchangeTNSEnable = "";
            ArrayList<String> exchangeList = new ArrayList<String>();
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = exchanges.nextElement();
                if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_TradeTicker) && exchange.isDefault()) {   //
                    if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_MarketTimeAndSales)) {
                        String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange.getSymbol();
                        Client.getInstance().addMarketTradeRequest(requestID, exchange.getSymbol());
                        exchangeList.add(exchange.getDisplayExchange());
                    }
                }
            }
            for (int r = 0; r < exchangeList.size(); r++) {
                if (r == 0) {
                    exchangeTNSEnable = exchangeList.get(r);
                } else {
                    exchangeTNSEnable = exchangeTNSEnable + "," + exchangeList.get(r);
                }
            }
            JOptionPane.showMessageDialog(this, Language.getString("TICKER_TNS_ENABLE_MSG") + exchangeTNSEnable, Language.getString("INFORMATION"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void setHideShowSymbolCaption() {
        UpperPanelSettings.isSymbolShowing ^= true;
//        UpperPanelSettings.isSymbolShowing = !UpperPanelSettings.isSymbolShowing;
        setSymbolCaption();
        UpperTickerPanel.getInstance().reloadFontforExchange();

        UpperTickerPanel.getInstance().redrawTicker();
        Settings.saveItem("U_TICKER_SHOW_SYMBOL", "" + UpperPanelSettings.isSymbolShowing);
    }

    public void setExchangePreferences(TWCheckBoxMenuItem selectedItem) {

    }

    public void showFilter() {

    }

    private void setSymbolCaption() {
        if (UpperPanelSettings.isSymbolShowing) {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL_DESCRIPTION"));
            showHideSymbol.setActionCommand("SHOW_DESCRIPTION");
        } else {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));
            showHideSymbol.setActionCommand("SHOW_SYMBOL");
        }
    }

    public void setHideShowChangeOption() {
        UpperPanelSettings.isChangeMode ^= true;
//        UpperPanelSettings.isChangeMode = !UpperPanelSettings.isChangeMode;
        setChangeOption();
        UpperTickerPanel.getInstance().reloadFontforExchange();
        UpperTickerPanel.getInstance().redrawTicker();
        Settings.saveItem("U_TICKER_CHANGE_MODE", "" + UpperPanelSettings.isChangeMode);
    }

    private void setChangeOption() {
        if (UpperPanelSettings.isChangeMode) {
            showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));
            showHideChange.setActionCommand("SHOW_PER_CHANGE");
        } else {
            showHideChange.setText(Language.getString("SHOW_CHANGE"));
            showHideChange.setActionCommand("SHOW_CHANGE");
        }
    }

    public void setParent(ProcessListener parent) {
        parentFrame = parent;
    }

    /**
     */
    public String toString() {
        String wsString = "" + UpperPanelSettings.TICKER_WIDTH + "|" +
                UpperPanelSettings.STOCK_TICKER_HEIGHT + "|" +
                UpperPanelSettings.FONT_TYPE + "|" +
                UpperPanelSettings.FONT_STYLE + "|" +
                UpperPanelSettings.FONT_BIG;
        return wsString;
    }

    public void applyWorkSpace(String wsString) {
        StringTokenizer st = new StringTokenizer(wsString, "|", false);
        try {
            UpperPanelSettings.TICKER_WIDTH = Integer.parseInt(st.nextToken());
            UpperPanelSettings.STOCK_TICKER_HEIGHT = Integer.parseInt(st.nextToken());
            UpperPanelSettings.FONT_TYPE = st.nextToken();
            UpperPanelSettings.FONT_STYLE = Integer.parseInt(st.nextToken());
            UpperPanelSettings.FONT_BIG = Integer.parseInt(st.nextToken());

            UpperPanelSettings.FONT_MEDIUM = UpperPanelSettings.FONT_BIG - 3;
            UpperPanelSettings.FONT_SMALL = UpperPanelSettings.FONT_BIG - 4;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        st = null;
    }

    /* Popup menu listeners */
    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        tickerFilter.removeAll();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        tickerFilter.removeAll();
        tickerFilter.getPopupMenu().setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        UpperFilterPanel filterPanel = new UpperFilterPanel(tickerFilter);
        tickerFilter.add(filterPanel);

        /* MainWindow filterPanel = new MainWindow(tickerFilter);
        tickerFilter.add(filterPanel);*/
        try {
            if (((DetachableRootPane) Ticker.getInstance().getRootPane()).isDetached()) {
                showHideTitle.setVisible(false);
                mnuClassicTicker.setVisible(false);
            } else {
                showHideTitle.setVisible(true);
                mnuClassicTicker.setVisible(true);

            }
        } catch (Exception e1) {
            showHideTitle.setVisible(true);
        }
        tickerFilter.getPopupMenu().pack();
    }

    /* Exchange listener metods */
    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void symbolRemoved(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void watchlistAdded(String listID) {
        //buildWatchlistFilterMenu();
    }

    public void watchlistRemoved(String listID) {
        //buildWatchlistFilterMenu();
    }

    public void watchlistRenamed(String listID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tickersMouseActions(MouseEvent e, UpperTickerPanel equityPanel) {
        if (SwingUtilities.isRightMouseButton(e)) {
            actions_Mouse_Click();
            GUISettings.showPopup(popupMenu, equityPanel, e.getX(), e.getY());
        }
    }

    /**
     * This class tracks the Ticker resizing event and do the layouting as necessary
     */
    class SymComponent extends java.awt.event.ComponentAdapter {

    }

}
