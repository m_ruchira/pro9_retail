package com.isi.csvr.ticker.advanced;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDecimalFormat;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 30, 2008
 * Time: 2:02:28 PM
 * To change this template use File | Settings | File Templates.
 */


public class SymbolHolderUpper {

    public static boolean isScrolable = true;
    public static boolean isMouseOverSymbol = true;
    public static boolean isDataCome = false;
    private static TWDecimalFormat intFormat = new TWDecimalFormat("#,##0");
    private static TWDecimalFormat floatFormat = new TWDecimalFormat("#,##0.00");
    //  ------------------ For Announcements Ticker ---------------------
    public String ansexchange = null;
    private String symbol;
    private String acronym;
    private String price;
    private String quantity;
    private String percentChange;
    private String change;
    private int splits;
    private String strSplits;
    private int status;
    private String testString;
    private Double testDouble;
    private double max_Price;
    private double min_Price;
    private double lastTrPrice;
    private String exkey;
    private int splitLength;
    private int sperchangelength;
    private int ssymbollength;
    private int squantitylength;
    private int slastTradedPricelength;
    private int tradeAreaWidth;         // Holds the trade Area Width
    private int mesgAreaWidth;          // Holds the message Area Width
    private int scrollingDir;           // holds the Scrolling Direction
    private int earlierStartXPos;            // Previous displayed X Position
    private String scrollMessage;          // Holds the message
    private String statusOfMessage;        // Holds the message status
    private int displayStatus;
    private int prevDisplayStatus;
    private boolean isMessageMode;
    //  ----------------- For News Ticker -----------------------
    private String newsProvider = null;
    private String newssymbol = null;
    private String exchange = null;
    private String newsID = null;
    private long newsDate = 0;
    private String language = null;
    private String headLine = null;
    private String body = null;
    private String keywords = null;
    private String source = null;
    private int instrumentType = 0;// -1;
    private int newsSymbolLength;
    private int newsAreaWidth;         // Holds the News Area Width
    private String nId = "";
    private String anssymbol = "";
    private int ansinstrumentType = 0;
    private String key = null;
    private long announcementTime;
    private String announcementNo = null;
    private String anslanguage = null;
    private String ansheadLine = null;
    private String message = null;
    private String url = null;
    private int ansSymbolLength;
    private int ansAreaWidth;

    public SymbolHolderUpper() {
        displayStatus = 0;
        prevDisplayStatus = 0;
        scrollMessage = "";   // Holds the message
        statusOfMessage = "";   // Holds the message status
    }

    public void setNewsData(String newsProvider, String newssymbol, String exchange, String newsID, long newsDate,
                            String language, String headLine, String body, String keywords, String source, int instrumentType) {
        this.newsProvider = newsProvider;
        this.newssymbol = newssymbol;
        this.exchange = exchange;
        this.newsID = newsID;
        this.newsDate = newsDate;
        this.language = language;
        this.headLine = headLine;
        this.body = body;
        this.keywords = keywords;
        this.source = source;
        this.instrumentType = instrumentType;// -1;
        calculateNewsArea();
    }

    private void calculateNewsArea() {
        newsSymbolLength = UpperTickerPanel.smallFm.stringWidth(headLine);
        newsAreaWidth = newsSymbolLength + 30;
    }

    public void setAnnouncementData(String ex, String symbol, int ins, String key, String no, String lang, String headline,
                                    String message, String url) {
        this.ansexchange = ex;
        this.anssymbol = symbol;
        this.ansinstrumentType = ins;
        this.key = key;
        this.announcementNo = no;
        this.anslanguage = lang;
        this.ansheadLine = headline;
        this.message = message;
        this.url = url;
        calculateAnsArea();
    }

    private void calculateAnsArea() {
//        ansSymbolLength = UpperTickerPanel.smallFm.stringWidth(ansheadLine);
        ansSymbolLength = MiddleTickerPanel.smallFm.stringWidth(anssymbol) + MiddleTickerPanel.smallFm.stringWidth(":") + MiddleTickerPanel.smallFm.stringWidth(ansheadLine);
        ansAreaWidth = ansSymbolLength + 30;
    }

    public void setData(String symbol, String acronym, String price, String quantity,
                        Double change, String perChange, int splits, int status, String exchange, int instrument, String key) {
        this.symbol = symbol;
        if ((acronym == null) || (acronym.length() == 0)) {
            acronym = symbol;
        }
        this.acronym = acronym;
        this.price = formatPriceField(price, exchange, symbol, instrument);
        this.quantity = formatField(quantity);
        this.percentChange = formatDecimalField(perChange);
        this.splits = splits;
        this.strSplits = formatField(splits);
        this.status = status;
        this.testDouble = change;
        this.testString = String.valueOf(change);
        this.change = formatPriceField(testString, exchange, symbol, instrument);
        this.lastTrPrice = Double.parseDouble(price);
        this.exkey = key;
        this.exchange = exchange;

        calculateTradeAreaWidth();          // Calculate the display area
        setMaxMinPrice(symbol, exchange, instrument);
    }

    public void revalidateExchange() {
        try {
            if (isMessageMode)
                calculateMessageWidth();          // Calculate the display area
            else
                calculateTradeAreaWidth();          // Calculate the display area
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void revalidateNews() {
        try {
            calculateNewsArea();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void revalidateAnounce() {
        try {
            calculateAnsArea();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setMaxMinPrice(String symbol, String exchange, int instrument) {
        DataStore dtStore = DataStore.getSharedInstance();
        Stock stk = dtStore.getStockObject(exchange, symbol, instrument);
        try {
            max_Price = stk.getMaxPrice();
        } catch (Exception e) {
            max_Price = Double.MAX_VALUE;
        }
        try {
            min_Price = stk.getMinPrice();
        } catch (Exception e) {
            min_Price = Double.MIN_VALUE;
        }
    }

    private void calculateTradeAreaWidth() {
        int aggOffset = 0;
        sperchangelength = UpperTickerPanel.smallFm.stringWidth(price);

        if (UpperPanelSettings.isChangeMode) {
            sperchangelength = UpperTickerPanel.smallFm.stringWidth(change);
        } else {
            sperchangelength = UpperTickerPanel.smallFm.stringWidth(percentChange) + UpperTickerPanel.smallFm.stringWidth("%");
        }

        splitLength = UpperTickerPanel.smallFm.stringWidth("(" + strSplits + ")");

        // This has to be changed according to the displaying field
        if (UpperPanelSettings.isSymbolShowing)
            this.ssymbollength = UpperTickerPanel.bigFm.stringWidth(symbol); // + 10;
        else
            this.ssymbollength = UpperTickerPanel.bigFm.stringWidth(acronym); // + 10;

        squantitylength = UpperTickerPanel.smallFm.stringWidth(quantity) + UpperTickerPanel.bigFm.stringWidth(" ");
        slastTradedPricelength = UpperTickerPanel.smallFm.stringWidth(price);

        if (splits > 1) {
            tradeAreaWidth = ssymbollength + splitLength + squantitylength + slastTradedPricelength + sperchangelength + 70; // + space;
        } else {
            tradeAreaWidth = ssymbollength + squantitylength + slastTradedPricelength + sperchangelength + 70; // + space;
        }

    }

    private String formatPriceField(String sValue, String exchange, String symbol, int instrument) {
        double cellValue = 0;
        String value = null;
        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
        value = SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(cellValue);
        return value;
    }

    private String formatField(String sValue) {
        double cellValue = 0;
        String value = null;
        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
//        if (cellValue <= 0) {
//            return " ";
//        }
        value = intFormat.format(cellValue);
        return value;
    }

    private String formatField(int value) {
        return intFormat.format(value);
    }

    private String formatDecimalField(String sValue) {
        double cellValue = 0;
        String value = null;
        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
        value = floatFormat.format(cellValue);
        return value;
    }

    public int getElementLength() {
        return this.tradeAreaWidth;
    }

    public int getNewsElementLength() {
        return this.newsAreaWidth;
    }

    public int getAnnouncementElementLength() {
        return this.ansAreaWidth;
    }

    public int getEarlierXPos() {
        return earlierStartXPos;
    }

    public void setEarlierXPos(int width) {
        earlierStartXPos = width;
    }

    public int getDisplayStatus() {
        return displayStatus;
    }


    public int getScrollingDir() {
        return this.scrollingDir;
    }

    public void setScrollingDir(int newDir) {
        this.scrollingDir = newDir;
    }

    public void draw(int startingXPos, int direction, Graphics textG) {
        if (direction == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - UpperTickerPanel.PIXEL_INCREMENT;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                if (testDouble < 0) {
                    textG.setColor(UpperPanelSettings.DOWN_COLOR);
                } else if (testDouble > 0) {
                    textG.setColor(UpperPanelSettings.UP_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(UpperPanelSettings.NO_CHANGE_COLOR);
                }
                if (UpperPanelSettings.isSymbolShowing) {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (UpperPanelSettings.isChangeMode) {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }
            if (!UpperTickerPanel.isMouseOver) {
                if (testDouble < 0) {
                    textG.setColor(UpperPanelSettings.DOWN_COLOR);
                } else if (testDouble > 0) {
                    textG.setColor(UpperPanelSettings.UP_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(UpperPanelSettings.NO_CHANGE_COLOR);
                }
                if (UpperPanelSettings.isSymbolShowing) {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (UpperPanelSettings.isChangeMode) {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }
            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.SYMBOL_HIGHLIGHT_COLOR);
                        if (UpperPanelSettings.isSymbolShowing) {
                            textG.setFont(UpperTickerPanel.bigFont);
                            textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        } else {
                            textG.setFont(UpperTickerPanel.bigFont);
                            textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        }

                        if (UpperPanelSettings.isChangeMode) {
                            textG.setFont(UpperTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        } else {
                            textG.setFont(UpperTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        }
                        isScrolable = false;
                        UpperTickerPanel.exkey = exkey;
                    }
                }
            }
        }

        if (direction == 2) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + UpperTickerPanel.PIXEL_INCREMENT;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                if (testDouble < 0) {
                    textG.setColor(UpperPanelSettings.DOWN_COLOR);
                } else if (testDouble > 0) {
                    textG.setColor(UpperPanelSettings.UP_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(UpperPanelSettings.NO_CHANGE_COLOR);
                }
                if (UpperPanelSettings.isSymbolShowing) {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (UpperPanelSettings.isChangeMode) {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }

            if (!UpperTickerPanel.isMouseOver) {
                if (testDouble < 0) {
                    textG.setColor(UpperPanelSettings.DOWN_COLOR);
                } else if (testDouble > 0) {
                    textG.setColor(UpperPanelSettings.UP_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(UpperPanelSettings.NO_CHANGE_COLOR);
                }
                if (UpperPanelSettings.isSymbolShowing) {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(UpperTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (UpperPanelSettings.isChangeMode) {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(UpperTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }
            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.SYMBOL_HIGHLIGHT_COLOR);
                        if (UpperPanelSettings.isSymbolShowing) {
                            textG.setFont(UpperTickerPanel.bigFont);
                            textG.drawString(symbol, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        } else {
                            textG.setFont(UpperTickerPanel.bigFont);
                            textG.drawString(acronym, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        }

                        if (UpperPanelSettings.isChangeMode) {
                            textG.setFont(UpperTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            }

                        } else {
                            textG.setFont(UpperTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        }
                        isScrolable = false;
                        UpperTickerPanel.exkey = exkey;
                    }
                }
            }
        }
    }

    private void calculateMessageWidth() {
        mesgAreaWidth = UpperPanelSettings.symbolFm.stringWidth(this.scrollMessage) + UpperPanelSettings.LEFT_SEPERATOR;
    }

    public String getExchange() {
        return exchange;
    }

    public void drawNews(int startingXPos, int scrollingDirection, Graphics2D textG) {
        if (scrollingDirection == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - 1;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (!UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getNewsElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.NEWS_HIGHLIGHT_COLOR);
                        textG.setFont(UpperTickerPanel.smallFont);
                        textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        showNewsWindow(newsID);
                    }
                }
            }

        } else {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + 1;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (!UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }

            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getNewsElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.NEWS_HIGHLIGHT_COLOR);
                        textG.setFont(UpperTickerPanel.smallFont);
                        textG.drawString(headLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        showNewsWindow(newsID);
                    }
                }
            }
        }
    }


    public void drawAnnouncements(int startingXPos, int scrollingDirection, Graphics2D textG) {
        if (scrollingDirection == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - 1;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (!UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getAnnouncementElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR);
                        textG.setFont(UpperTickerPanel.smallFont);
                        textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        UpperTickerPanel.ansId = announcementNo;
                        UpperTickerPanel.time = announcementTime;
                        UpperTickerPanel.message = message;
                        UpperTickerPanel.title = ansheadLine;
                        UpperTickerPanel.symbol = anssymbol;
                    }
                }
            }

        } else {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position

            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + 1;
            }
            earlierStartXPos = startingXPos;
            if (UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }

            if (!UpperTickerPanel.isMouseOver) {
                textG.setColor(UpperPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(UpperTickerPanel.smallFont);
                textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (UpperTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getAnnouncementElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= UpperTickerPanel.xPos) && (UpperTickerPanel.xPos <= xlength)) {
                        textG.setColor(UpperPanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR);
                        textG.setFont(UpperTickerPanel.smallFont);
                        textG.drawString(anssymbol + ":" + ansheadLine, startingXPos, UpperPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        UpperTickerPanel.ansId = announcementNo;
                        UpperTickerPanel.time = announcementTime;
                        UpperTickerPanel.message = message;
                        UpperTickerPanel.title = ansheadLine;
                        UpperTickerPanel.symbol = anssymbol;
                    }
                }
            }
        }
    }

    public void showNewsWindow(String newsID) {
        UpperTickerPanel.newsId = newsID;
    }
}
