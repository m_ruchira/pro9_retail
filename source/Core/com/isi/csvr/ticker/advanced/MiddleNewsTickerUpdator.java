package com.isi.csvr.ticker.advanced;

import com.isi.csvr.news.News;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.shared.DynamicArray;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 31, 2008
 * Time: 10:15:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class MiddleNewsTickerUpdator extends Thread {
    private boolean active = true;
    private DynamicArray dataStore;

    public MiddleNewsTickerUpdator() {
        super("Summary MiddleTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            MiddleTickerFeeder.isQueueEmpty = true;
            MiddleTickerFeeder.isNewsModeSelected = false;
            MiddleTickerFeeder.newsQueue.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        while (active) {
            if (MiddleTickerFeeder.newsQueue != null && MiddleTickerFeeder.newsQueue.isEmpty()) {
                newsModeSelector();
            }
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }


    public void newsModeSelector() {
        dataStore = NewsStore.getSharedInstance().getFilteredNewsStore();
        for (int i = 0; i < dataStore.size(); i++) {
            MiddleTickerFeeder.addNewsData((News) dataStore.get(i));
        }

        MiddleTickerFeeder.isNewsModeSelected = true;
    }

}
