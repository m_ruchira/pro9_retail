package com.isi.csvr.cachestore;

import com.isi.csvr.shared.Settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by IntelliJ IDEA.
 * User: Admin
 * Date: Jun 11, 2007
 * Time: 10:18:44 AM
 * To change this template use File | Settings | File Templates.
 */

public class CachedStoreMasterDetails {
    StringBuilder sTemp = null;
    //	String sFilePathRealTime = null;
    String sFilePathBackLog = null;
    //	File fileRT = null;
    File fileBL = null;
    //	RandomAccessFile raFileRT = null;
    RandomAccessFile raFileBL = null;
    boolean finishWrite = false;
    //	FileChannel rwChannelRT = null;
    FileChannel rwChannelBL = null;
    //	MappedByteBuffer wrBufRT = null;
    MappedByteBuffer wrBufBL = null;

    public CachedStoreMasterDetails(String name) {
//		sFilePathRealTime = getFileName("REAL_TIME_");
        sFilePathBackLog = getFileName("BACK_LOG_", name);
        /*try {
              fileRT = new File(sFilePathRealTime);
          } catch (Exception e) {
              e.printStackTrace();
          }
          try {
              raFileRT = new RandomAccessFile(fileRT, "rw");
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }
          try {
              rwChannelRT = raFileRT.getChannel();
          } catch (Exception e) {
              e.printStackTrace();
          }

          try {
              wrBufRT = raFileRT.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 10);
          } catch (IOException e) {
              e.printStackTrace();
          }*/

        try {
            fileBL = new File(sFilePathBackLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            raFileBL = new RandomAccessFile(fileBL, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            rwChannelBL = raFileBL.getChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            wrBufBL = raFileBL.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public CachedStoreMasterDetails() {
        //To change body of created methods use File | Settings | File Templates.
    }

    private String getFileName(String prefix, String name) {
        long time = UniqueID.get();
        StringBuilder sTemp = new StringBuilder();
        sTemp.append(name);
        String path = "";
        try {
            File folder = new File(Settings.getAbsolutepath() + "\\Temp\\TEMP_FILES\\");
            if (!folder.exists()) {
                folder.mkdir();
                path = Settings.getAbsolutepath() + "\\Temp\\TEMP_FILES\\" + prefix + sTemp.toString();

            } else {
                path = Settings.getAbsolutepath() + "\\Temp\\TEMP_FILES\\" + prefix + sTemp.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//		return Settings.getAbsolutepath() + "\\Temp\\TEMP_FILES\\" + prefix + sTemp.toString();
        return path;
    }

    /*public RandomAccessFile getRandomAccessFileRT() {
         return raFileRT;
     }

     public FileChannel getChannelRT() {
         return rwChannelRT;
     }


     public File getRTFile() {
         return fileRT;
     }

     public MappedByteBuffer remapRT() {
         try {
             return raFileRT.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (raFileRT.length()));
         } catch (IOException e) {
             return null;
         }
     }

     public MappedByteBuffer reSizeRT(MappedByteBuffer buffer) {
         try {
             return raFileRT.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (buffer.limit() + 100000));
         } catch (IOException e) {
             return null;
         }
     }

     public MappedByteBuffer getMemoryMappedFileBufferRT() {
         return wrBufRT;
     }

     public String getFilePathRT() {
         return sFilePathRealTime;
     }*/


    public File getBLFile() {
        return fileBL;
    }

    public RandomAccessFile getRandomAccessFileBL() {
        return raFileBL;
    }

    public RandomAccessFile getRandomAccessFileBL(String name) {

        sFilePathBackLog = getFileName("BACK_LOG_", name);

        try {
            fileBL = new File(sFilePathBackLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            raFileBL = new RandomAccessFile(fileBL, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return raFileBL;
    }

    public FileChannel getChannelBL() {
        return rwChannelBL;
    }

    public MappedByteBuffer remapBL() {
        try {
            return raFileBL.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (raFileBL.length()));
        } catch (IOException e) {
            return null;
        }
    }

    public MappedByteBuffer reSizeBL(MappedByteBuffer buffer) {
        try {
            return raFileBL.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (buffer.limit() + 100000));
        } catch (IOException e) {
            return null;
        }
    }

    public MappedByteBuffer getMemoryMappedFileBufferBL() {
        return wrBufBL;
    }

    public String getFilePathBL() {
        return sFilePathBackLog;
    }
}
