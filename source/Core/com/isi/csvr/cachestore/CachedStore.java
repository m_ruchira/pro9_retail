package com.isi.csvr.cachestore;

import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

public class CachedStore implements DataStoreInterface {
    private static final int BUFFER_SIZE = 15000000;
    private static int fileNumber = 0;
    public final int MAX_NO_OF_RECORDS_RAF_FILE = 10000;
    private final int RECORD_SIZE = 100;
    private Hashtable<Integer, Object> bucket = null;
    private Cacheble dummyObject = null;
    private Cacheble lastObject = null;
    private RandomAccessFile rafBL = null;
    private boolean readToptoBottom = true;
    private int rowNumber = 0;
    private long firstSequence = -1;
    private long lastSequence = 0;
    private byte[] template;
    private byte[] data;
    private MappedByteBuffer buffer = null;
    private FileChannel channel;
    private String name;
    private File file;

    public CachedStore(Cacheble obj, int readMode, String name) {
        try {
            dummyObject = obj;
            this.name = name;
            template = new byte[RECORD_SIZE];
            Arrays.fill(template, (byte) '*');
            template[RECORD_SIZE - 1] = '\n';
            data = new byte[RECORD_SIZE];
            readToptoBottom = (readMode == Constants.READE_MODE_TOP_TO_BOTTOM);
            bucket = new Hashtable<Integer, Object>();
            createStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createStore() {

        bucket.clear();
        File folder = new File(Settings.getAbsolutepath() + "temp/temp_files/");
        if (!folder.exists()) {
            folder.mkdir();
        }
        try {
            file = new File(Settings.getAbsolutepath() + "temp/temp_files/" + name + ".txt");
            file.createNewFile();
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            channel = raf.getChannel();
            buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, BUFFER_SIZE);
            raf.seek(0);
            rowNumber = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean mayOverflow() {
        return ((BUFFER_SIZE - buffer.position()) < RECORD_SIZE);
    }

    public void grow() {
        if (channel == null) return;
        try {
            int remainder = BUFFER_SIZE - buffer.position();
            buffer = channel.map(FileChannel.MapMode.READ_WRITE, channel.size() - remainder, BUFFER_SIZE /*(raFileBL.length())*/);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean saveObject(Cacheble newObject, boolean isBackLog) {
        synchronized (this) {
            if (channel == null) return false;
            String record = null;
            lastObject = newObject;

            try {
                record = newObject.getRecord();
                if (record != null) {
                    System.arraycopy(template, 0, data, 0, RECORD_SIZE);
                    System.arraycopy(record.getBytes(), 0, data, 0, record.length());
                    data[record.length()] = (byte) '#';
                    try {
                        if (mayOverflow()) {
                            grow();
                        }
                        buffer.put(data);

                        rowNumber++;
                    } catch (Exception e) {
                    }
                } else {
                    System.out.println("record null");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public void pack() {
        try {
            if (channel != null) {
//                long t = System.currentTimeMillis();
                buffer = channel.map(FileChannel.MapMode.READ_WRITE, 0, channel.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getLastSequence() {
        return lastSequence;
    }

    public Cacheble getLastObject() {
        return lastObject;
    }

    public Object getCachedObject(int iRow) {
        /*try {
            byte[] temp = new byte[RECORD_SIZE];
            buffer.position(iRow * RECORD_SIZE);
            buffer.get(temp,0,RECORD_SIZE);
            Cacheble newObject = (Cacheble)(dummyObject).clone();
            newObject.setRecord(new String(temp));
            return newObject;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }*/
        if (channel == null) return null;
        int seekRow = 0;
        int bucketRow = 0;
        Object objectStoredInBucket = null;
        StringBuilder sBuff = null;
        String record = null;
        boolean result = true;
        Object newObject = null;
        int firstRowInMap = 0;
        int lastRowInMap = 0;
        int visibleRowCount = 1000;
        MappedByteBuffer readBuffer = null;

        if (!readToptoBottom) {
            bucketRow = getRowCount() - iRow - 1;
        } else {
            bucketRow = iRow;
        }

        readBuffer = buffer;//fileBufferBL;
        if (!readToptoBottom) {
            seekRow = backLogRowCount() - ((iRow - realTimeRowCount())) - 1;
        } else {
            seekRow = iRow;
        }
        objectStoredInBucket = bucket.get(bucketRow);
        if (getRowCount() > 0) {
            if (objectStoredInBucket == null) {
                try {
                    try {
                        if (readBuffer != null) {
                            byte[] b = new byte[1000];
                            readBuffer.position(seekRow * RECORD_SIZE);
                            readBuffer.get(b, 0, RECORD_SIZE);
                            sBuff = new StringBuilder();
                            for (byte aB : b) {
                                sBuff.append((char) aB);
                            }
                            record = sBuff.toString().trim();
                            sBuff = null;
                        }
                    } catch (Exception e) {
                        System.out.println("<DEBUG> Seek Failed " + e.toString());
                        result = false;
                    }

                    if ((record != null) && (record.length() > 0) && (!record.equalsIgnoreCase(""))) {
                        try {
                            newObject = dummyObject.clone();
                            ((Cacheble) newObject).setRecord(record.split("#")[0]);
                            objectStoredInBucket = newObject;
                            record = null;
                        } catch (Exception e) {
                            e.printStackTrace();
                            result = false;
                        }
                        try {
                            bucket.put(bucketRow, newObject);
                        } catch (Exception e) {
                            e.printStackTrace();
                            result = false;
                        }
                    }
                    try {
                        if (bucket.size() > 0) {
                            Object objArray[] = bucket.keySet().toArray();
                            Arrays.sort(objArray);
                            firstRowInMap = (Integer) objArray[0];
                            lastRowInMap = (Integer) objArray[objArray.length - 1];
                            objArray = null;
                        }
                    } catch (Exception e) {
                        result = false;
                    }
                    try {
                        if (bucket.size() >= visibleRowCount) {
                            if (bucketRow >= lastRowInMap) {
                                if (bucket.get(firstRowInMap) != null) {
                                    bucket.remove(firstRowInMap);
                                }
                            } else if (bucketRow < lastRowInMap) {
                                if (bucket.get(lastRowInMap) != null) {
                                    bucket.remove(lastRowInMap);
                                }
                            }
                        }
                    } catch (Exception e) {
                        result = false;
                    }
                } catch (Exception e) {
                    result = false;
                }
            }
            if (!result) {
                return "";
            }
        }
        return objectStoredInBucket;
    }

    public int getRowCount() {
        try {
            long realTimeRecords = realTimeRowCount();
            long backlogRecords = backLogRowCount();
            return Math.round(realTimeRecords + backlogRecords);
        } catch (Exception e) {
            return 0;
        }
    }

    private int realTimeRowCount() {
        return 0;
    }

    private int backLogRowCount() {
        return rowNumber;
    }

    public void deleteCache() {
        synchronized (this) {
            buffer.clear();
            SharedMethods.unmap(buffer);
            Runtime.getRuntime().gc();
            Runtime.getRuntime().gc();
            try {
                channel.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            channel = null;
            try {
                file.delete();
            } catch (Exception e) {
                file.deleteOnExit();
            }
        }
    }


    public void clear(String symbol) {
        try {
            channel.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        buffer.clear();
        channel = null;
        buffer = null;
        createStore();
    }

    public void clear() {

    }

    public long getFirstSequence() {
        return firstSequence;
    }

    private Cacheble getRecordFromFile(int iRow) {
        if (channel == null) return null;

        MappedByteBuffer readBuffer = null;
        StringBuilder sBuff = null;
        String record = null;
        int seekRow = 0;
        Object newObject = null;

        readBuffer = buffer;
        seekRow = backLogRowCount() - ((iRow - realTimeRowCount())) - 1;
        try {
            if (readBuffer != null) {
                byte[] b = new byte[1000];
                readBuffer.position(seekRow * RECORD_SIZE);
                readBuffer.get(b, 0, RECORD_SIZE);
                sBuff = new StringBuilder();
                for (byte aB : b) {
                    sBuff.append((char) aB);
                }
                record = sBuff.toString().trim();
                sBuff = null;
            }
        } catch (Exception e) {
            System.out.println("<DEBUG> Seek Failed " + e.toString());
        }
        if ((record != null) && (record.length() > 0) && (!record.equalsIgnoreCase(""))) {
            try {
                newObject = dummyObject.clone();
                ((Cacheble) newObject).setRecord(record.split("#")[0]);
                record = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ((Cacheble) newObject);
    }

    public Enumeration<Cacheble> getAllCachedObjects() {
        return new CachedList();
    }

    public RandomAccessFile getRafBL() {
        return rafBL;
    }

    public void setRafBL(RandomAccessFile rafBL) {
        this.rafBL = rafBL;
    }

    public void setFileResize(String fileName) {
        if (rafBL.getChannel().isOpen()) {
            try {
                rafBL.getChannel().close();
                try {
                    rafBL.setLength(getRowCount() * RECORD_SIZE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            rafBL.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, (rafBL.length()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        rowNumber = 0;
    }

    private class CachedList implements Enumeration<Cacheble> {
        private int index = getRowCount();

        public boolean hasMoreElements() {
            try {
                if (0 < (index)) {//bug on export trades.
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }
        }

        public Cacheble nextElement() {
            try {
                return getRecordFromFile(--index);
            } catch (Exception e) {
                return null;
            }
        }
    }
}
