package com.isi.csvr.cachestore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;

/**
 * Created by IntelliJ IDEA.
 * User: Admin
 * Date: Jun 11, 2007
 * Time: 10:18:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class CachedStoreFileDetails {
    StringBuilder sTemp = null;
    String sFilePath = null;
    File f = null;
    RandomAccessFile raFile = null;
    boolean finishWrite = false;

    public CachedStoreFileDetails() {
        long time = UniqueID.get();
        sTemp = new StringBuilder();
        sTemp.append(time);
        sFilePath = System.getProperties().get("user.dir") + "\\Temp\\" + sTemp.toString();
        try {
            f = new File(sFilePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            raFile = new RandomAccessFile(f, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public RandomAccessFile getRandomAccessFile() {
        return raFile;
    }

    public void setFinish() {
        finishWrite = true;
    }

    public boolean canWrite() {
        return !finishWrite;
    }

    public String getFilePath() {
        return sFilePath;
    }
}
