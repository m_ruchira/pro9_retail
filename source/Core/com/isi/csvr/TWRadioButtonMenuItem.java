package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 10, 2005
 * Time: 9:38:37 AM
 * To change this template use File | Settings | File Templates.
 */
public class TWRadioButtonMenuItem extends JRadioButtonMenuItem {
    public TWRadioButtonMenuItem() {
        super();
        setUI(new TWRadioButtonMenuItemUI());
    }

    public TWRadioButtonMenuItem(String text) {
        super(text);
        setUI(new TWRadioButtonMenuItemUI());
    }


    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        dimension.setSize(dimension.getWidth() + Constants.OUTLOOK_MENU_COLUMN_WIDTH, dimension.getHeight());
        return dimension;
    }

    public void paint(Graphics g) {
        g.translate(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE, 0);
        super.paint(g);
        g.translate(-(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE), 0);
        g.setColor(Theme.MENU_SELECTION_COLOR);
        /*if (Constants.OUTLOOK_MENU_SIDE > 0)
            g.fillRect(0, 0, Constants.OUTLOOK_MENU_COLUMN_WIDTH, getHeight());
        else
            g.fillRect(getWidth() - Constants.OUTLOOK_MENU_COLUMN_WIDTH, 0, Constants.OUTLOOK_MENU_COLUMN_WIDTH, getHeight());*/
    }

    public void setSelected(boolean b) {
        super.setSelected(b);
    }

    public void updateUI() {
        super.updateUI();
        setUI(new TWRadioButtonMenuItemUI());
    }
}
