package com.isi.csvr.mist;

import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.TableSettings;
import com.isi.csvr.theme.Theme;

import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class MISTTableSettings implements TableSettings {

    private Color gridColor;
    private Color selectedColumnFG;
    private Color selectedColumnBG;

    private Color descColumnFG;
    private Color descColumnBG;
    private Color prevClosedColumnFG;
    private Color prevClosedColumnBG;
    private Color vwapColumnFG;
    private Color vwapColumnBG;
    // Trade Details
    private Color lowTradeColumnFG;
    private Color lowTradeColumnBG;
    private Color highTradeColumnFG;
    private Color highTradeColumnBG;
    private Color volumeColumnFG;
    private Color volumeColumnBG;
    private Color lastColumnFG;
    private Color lastColumnBG;
    private Color tradesColumnFG;
    private Color tradesColumnBG;
    private Color lastTradeQtyColumnFG;
    private Color lastTradeQtyColumnBG;
    // Ask Details
    private Color lowAskColumnFG;
    private Color lowAskColumnBG;
    private Color highAskColumnFG;
    private Color highAskColumnBG;
    private Color lowAskQtyColumnFG;
    private Color lowAskQtyColumnBG;
    private Color totAskQtyColumnFG;
    private Color totAskQtyColumnBG;
    private Color lastAskColumnFG;
    private Color lastAskColumnBG;
    private Color lastAskQtyColumnFG;
    private Color lastAskQtyColumnBG;
    // Bid Details
    private Color lowBidColumnFG;
    private Color lowBidColumnBG;
    private Color highBidColumnFG;
    private Color highBidColumnBG;
    private Color highBidQtyColumnFG;
    private Color highBidQtyColumnBG;
    private Color totBidQtyColumnFG;
    private Color totBidQtyColumnBG;
    private Color lastBidColumnFG;
    private Color lastBidColumnBG;
    private Color lastBidQtyColumnFG;
    private Color lastBidQtyColumnBG;

    private Font bodyFont;
    //private Font  originalBodyFont = null;
    private Font headingFont;
    //private Font  originalHeadingFont = null;
    private boolean gridOn;
    private int bodyGap;

    public MISTTableSettings() {
        init();
    }

    private void init() {
//        gridColor = Color.black;
//        selectedColumnBG = Color.white;
//        selectedColumnFG = Color.black;
//
//        descColumnFG = Color.black;
//        descColumnBG = Color.white;
//        prevClosedColumnFG = Color.black;
//        prevClosedColumnBG = new Color(0xc6c6c6);
//        vwapColumnFG = Color.black;
//        vwapColumnBG = new Color(0xFFFF00);
//        // Trade Details
//        lowTradeColumnFG = Color.black;
//        lowTradeColumnBG = new Color(0xc6c6c6);
//        highTradeColumnFG = Color.black;
//        highTradeColumnBG = Color.white;
//        volumeColumnFG = Color.black;
//        volumeColumnBG = new Color(0x00FF00);
//        lastColumnFG = Color.black;
//        lastColumnBG = new Color(0xFFFF00);
//        tradesColumnFG = Color.black;
//        tradesColumnBG = new Color(0xFF9CCE);
//        lastTradeQtyColumnFG = new Color(0xFFFF00);
//        lastTradeQtyColumnBG = new Color(0x0000FF);
//        // Ask Details
//        lowAskColumnFG = Color.black;
//        lowAskColumnBG = new Color(0xc6c6c6);
//        highAskColumnFG = Color.black;
//        highAskColumnBG = Color.white;
//        lowAskQtyColumnFG = Color.black;
//        lowAskQtyColumnBG = new Color(0xFF9CCE);
//        totAskQtyColumnFG = Color.black;
//        totAskQtyColumnBG = new Color(0x00FF00);
//        lastAskColumnFG = Color.black;
//        lastAskColumnBG = new Color(0xFFFF00);
//        lastAskQtyColumnFG = new Color(0xFFFF00);
//        lastAskQtyColumnBG = new Color(0x0000FF);
//        // Bid Details
//        lowBidColumnFG = Color.black;
//        lowBidColumnBG = new Color(0xc6c6c6);
//        highBidColumnFG = Color.black;
//        highBidColumnBG = Color.white;
//        highBidQtyColumnFG = Color.black;
//        highBidQtyColumnBG = new Color(0xFF9CCE);
//        totBidQtyColumnFG = Color.black;
//        totBidQtyColumnBG = new Color(0x00FF00);
//        lastBidColumnFG = Color.black;
//        lastBidColumnBG = new Color(0xFFFF00);
//        lastBidQtyColumnFG = new Color(0xFFFF00);
//        lastBidQtyColumnBG = new Color(0x0000FF);

        gridColor = Theme.getColor("MIST_VIEW_GRID_COLOR");
        selectedColumnBG = Theme.getColor("MIST_VIEW_SELECTED_BG_COLOR");
        selectedColumnFG = Theme.getColor("MIST_VIEW_SELECTED_FG_COLOR");

        descColumnFG = Theme.getColor("MIST_VIEW_DESC_COL_FG_COLOR");
        descColumnBG = Theme.getColor("MIST_VIEW_DESC_COL_BG_COLOR");
        prevClosedColumnFG = Theme.getColor("MIST_VIEW_PREV_CLOSED_COL_FG_COLOR");
        prevClosedColumnBG = Theme.getColor("MIST_VIEW_PREV_CLOSED_COL_BG_COLOR");
        vwapColumnFG = Theme.getColor("MIST_VIEW_VWAP_COL_FG_COLOR");
        vwapColumnBG = Theme.getColor("MIST_VIEW_VWAP_COL_BG_COLOR");
        // Trade Details
        lowTradeColumnFG = Theme.getColor("MIST_VIEW_TRADE_LOW_COL_FG_COLOR");
        lowTradeColumnBG = Theme.getColor("MIST_VIEW_TRADE_LOW_COL_BG_COLOR");
        highTradeColumnFG = Theme.getColor("MIST_VIEW_TRADE_HIGH_COL_FG_COLOR");
        highTradeColumnBG = Theme.getColor("MIST_VIEW_TRADE_HIGH_COL_BG_COLOR");
        volumeColumnFG = Theme.getColor("MIST_VIEW_TRADE_VOLUME_COL_FG_COLOR");
        volumeColumnBG = Theme.getColor("MIST_VIEW_TRADE_VOLUME_COL_BG_COLOR");
        lastColumnFG = Theme.getColor("MIST_VIEW_TRADE_LAST_COL_FG_COLOR");
        lastColumnBG = Theme.getColor("MIST_VIEW_TRADE_LAST_COL_BG_COLOR");
        tradesColumnFG = Theme.getColor("MIST_VIEW_TRADE_NO_OF_TRADES_COL_FG_COLOR");
        tradesColumnBG = Theme.getColor("MIST_VIEW_TRADE_NO_OF_TRADES_COL_BG_COLOR");
        lastTradeQtyColumnFG = Theme.getColor("MIST_VIEW_TRADE_LAST_QTY_COL_FG_COLOR");
        lastTradeQtyColumnBG = Theme.getColor("MIST_VIEW_TRADE_LAST_QTY_COL_BG_COLOR");
        // Ask Details
        lowAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_LOW_COL_FG_COLOR");
        lowAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_LOW_COL_BG_COLOR");
        highAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_HIGH_COL_FG_COLOR");
        highAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_HIGH_COL_BG_COLOR");
        lowAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_LOW_QTY_COL_FG_COLOR");
        lowAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_LOW_QTY_COL_BG_COLOR");
        totAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_TOT_QTY_COL_FG_COLOR");
        totAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_TOT_QTY_COL_BG_COLOR");
        lastAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_LAST_COL_FG_COLOR");
        lastAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_LAST_COL_BG_COLOR");
        lastAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_LAST_QTY_COL_FG_COLOR");
        lastAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_LAST_QTY_COL_BG_COLOR");
        // Bid Details
        lowBidColumnFG = Theme.getColor("MIST_VIEW_BID_LOW_COL_FG_COLOR");
        lowBidColumnBG = Theme.getColor("MIST_VIEW_BID_LOW_COL_BG_COLOR");
        highBidColumnFG = Theme.getColor("MIST_VIEW_BID_HIGH_COL_FG_COLOR");
        highBidColumnBG = Theme.getColor("MIST_VIEW_BID_HIGH_COL_BG_COLOR");
        highBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_HIGH_QTY_COL_FG_COLOR");
        highBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_HIGH_QTY_COL_BG_COLOR");
        totBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_TOT_QTY_COL_FG_COLOR");
        totBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_TOT_QTY_COL_BG_COLOR");
        lastBidColumnFG = Theme.getColor("MIST_VIEW_BID_LAST_COL_FG_COLOR");
        lastBidColumnBG = Theme.getColor("MIST_VIEW_BID_LAST_COL_BG_COLOR");
        lastBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_LAST_QTY_COL_FG_COLOR");
        lastBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_LAST_QTY_COL_BG_COLOR");
    }

    public Color getDescColumnBG() {
        return descColumnBG;
    }

    public void setDescColumnBG(Color descColumnBG) {
        this.descColumnBG = descColumnBG;
    }

    public Color getDescColumnFG() {
        return descColumnFG;
    }

    public void setDescColumnFG(Color descColumnFG) {
        this.descColumnFG = descColumnFG;
    }

    public Color getHighAskColumnBG() {
        return highAskColumnBG;
    }

    public void setHighAskColumnBG(Color highAskColumnBG) {
        this.highAskColumnBG = highAskColumnBG;
    }

    public Color getHighAskColumnFG() {
        return highAskColumnFG;
    }

    public void setHighAskColumnFG(Color highAskColumnFG) {
        this.highAskColumnFG = highAskColumnFG;
    }

    public Color getHighBidColumnBG() {
        return highBidColumnBG;
    }

    public void setHighBidColumnBG(Color highBidColumnBG) {
        this.highBidColumnBG = highBidColumnBG;
    }

    public Color getHighBidColumnFG() {
        return highBidColumnFG;
    }

    public void setHighBidColumnFG(Color highBidColumnFG) {
        this.highBidColumnFG = highBidColumnFG;
    }

    public Color getHighBidQtyColumnBG() {
        return highBidQtyColumnBG;
    }

    public void setHighBidQtyColumnBG(Color highBidQtyColumnBG) {
        this.highBidQtyColumnBG = highBidQtyColumnBG;
    }

    public Color getHighBidQtyColumnFG() {
        return highBidQtyColumnFG;
    }

    public void setHighBidQtyColumnFG(Color highBidQtyColumnFG) {
        this.highBidQtyColumnFG = highBidQtyColumnFG;
    }

    public Color getHighTradeColumnBG() {
        return highTradeColumnBG;
    }

    public void setHighTradeColumnBG(Color highTradeColumnBG) {
        this.highTradeColumnBG = highTradeColumnBG;
    }

    public Color getHighTradeColumnFG() {
        return highTradeColumnFG;
    }

    public void setHighTradeColumnFG(Color highTradeColumnFG) {
        this.highTradeColumnFG = highTradeColumnFG;
    }

    public Color getLastAskColumnBG() {
        return lastAskColumnBG;
    }

    public void setLastAskColumnBG(Color lastAskColumnBG) {
        this.lastAskColumnBG = lastAskColumnBG;
    }

    public Color getLastAskColumnFG() {
        return lastAskColumnFG;
    }

    public void setLastAskColumnFG(Color lastAskColumnFG) {
        this.lastAskColumnFG = lastAskColumnFG;
    }

    public Color getLastAskQtyColumnBG() {
        return lastAskQtyColumnBG;
    }

    public void setLastAskQtyColumnBG(Color lastAskQtyColumnBG) {
        this.lastAskQtyColumnBG = lastAskQtyColumnBG;
    }

    public Color getLastAskQtyColumnFG() {
        return lastAskQtyColumnFG;
    }

    public void setLastAskQtyColumnFG(Color lastAskQtyColumnFG) {
        this.lastAskQtyColumnFG = lastAskQtyColumnFG;
    }

    public Color getLastBidColumnFG() {
        return lastBidColumnFG;
    }

    public void setLastBidColumnFG(Color lastBidColumnFG) {
        this.lastBidColumnFG = lastBidColumnFG;
    }

    public Color getLastBidQtyColumnBG() {
        return lastBidQtyColumnBG;
    }

    public void setLastBidQtyColumnBG(Color lastBidQtyColumnBG) {
        this.lastBidQtyColumnBG = lastBidQtyColumnBG;
    }

    public Color getLastBidQtyColumnFG() {
        return lastBidQtyColumnFG;
    }

    public void setLastBidQtyColumnFG(Color lastBidQtyColumnFG) {
        this.lastBidQtyColumnFG = lastBidQtyColumnFG;
    }

    public Color getLastColumnBG() {
        return lastColumnBG;
    }

    public void setLastColumnBG(Color lastColumnBG) {
        this.lastColumnBG = lastColumnBG;
    }

    public Color getLastColumnFG() {
        return lastColumnFG;
    }

    public void setLastColumnFG(Color lastColumnFG) {
        this.lastColumnFG = lastColumnFG;
    }

    public Color getLowAskColumnBG() {
        return lowAskColumnBG;
    }

    public void setLowAskColumnBG(Color lowAskColumnBG) {
        this.lowAskColumnBG = lowAskColumnBG;
    }

    public Color getLowAskColumnFG() {
        return lowAskColumnFG;
    }

    public void setLowAskColumnFG(Color lowAskColumnFG) {
        this.lowAskColumnFG = lowAskColumnFG;
    }

    public Color getLowAskQtyColumnBG() {
        return lowAskQtyColumnBG;
    }

    public void setLowAskQtyColumnBG(Color lowAskQtyColumnBG) {
        this.lowAskQtyColumnBG = lowAskQtyColumnBG;
    }

    public Color getLowAskQtyColumnFG() {
        return lowAskQtyColumnFG;
    }

    public void setLowAskQtyColumnFG(Color lowAskQtyColumnFG) {
        this.lowAskQtyColumnFG = lowAskQtyColumnFG;
    }

    public Color getLowBidColumnBG() {
        return lowBidColumnBG;
    }

    public void setLowBidColumnBG(Color lowBidColumnBG) {
        this.lowBidColumnBG = lowBidColumnBG;
    }

    public Color getLowBidColumnFG() {
        return lowBidColumnFG;
    }

    public void setLowBidColumnFG(Color lowBidColumnFG) {
        this.lowBidColumnFG = lowBidColumnFG;
    }

    public Color getLowTradeColumnBG() {
        return lowTradeColumnBG;
    }

    public void setLowTradeColumnBG(Color lowTradeColumnBG) {
        this.lowTradeColumnBG = lowTradeColumnBG;
    }

    public Color getLowTradeColumnFG() {
        return lowTradeColumnFG;
    }

    public void setLowTradeColumnFG(Color lowTradeColumnFG) {
        this.lowTradeColumnFG = lowTradeColumnFG;
    }

    public Color getPrevClosedColumnBG() {
        return prevClosedColumnBG;
    }

    public void setPrevClosedColumnBG(Color prevClosedColumnBG) {
        this.prevClosedColumnBG = prevClosedColumnBG;
    }

    public Color getPrevClosedColumnFG() {
        return prevClosedColumnFG;
    }

    public void setPrevClosedColumnFG(Color prevClosedColumnFG) {
        this.prevClosedColumnFG = prevClosedColumnFG;
    }

    public Color getTotAskQtyColumnBG() {
        return totAskQtyColumnBG;
    }

    public void setTotAskQtyColumnBG(Color totAskQtyColumnBG) {
        this.totAskQtyColumnBG = totAskQtyColumnBG;
    }

    public Color getTotAskQtyColumnFG() {
        return totAskQtyColumnFG;
    }

    public void setTotAskQtyColumnFG(Color totAskQtyColumnFG) {
        this.totAskQtyColumnFG = totAskQtyColumnFG;
    }

    public Color getTotBidQtyColumnBG() {
        return totBidQtyColumnBG;
    }

    public void setTotBidQtyColumnBG(Color totBidQtyColumnBG) {
        this.totBidQtyColumnBG = totBidQtyColumnBG;
    }

    public Color getTotBidQtyColumnFG() {
        return totBidQtyColumnFG;
    }

    public void setTotBidQtyColumnFG(Color totBidQtyColumnFG) {
        this.totBidQtyColumnFG = totBidQtyColumnFG;
    }

    public Color getVolumeColumnBG() {
        return volumeColumnBG;
    }

    public void setVolumeColumnBG(Color volumeColumnBG) {
        this.volumeColumnBG = volumeColumnBG;
    }

    public Color getVolumeColumnFG() {
        return volumeColumnFG;
    }

    public void setVolumeColumnFG(Color volumeColumnFG) {
        this.volumeColumnFG = volumeColumnFG;
    }

    public Color getVwapColumnBG() {
        return vwapColumnBG;
    }

    public void setVwapColumnBG(Color vwapColumnBG) {
        this.vwapColumnBG = vwapColumnBG;
    }

    public Color getVwapColumnFG() {
        return vwapColumnFG;
    }

    public void setVwapColumnFG(Color vwapColumnFG) {
        this.vwapColumnFG = vwapColumnFG;
    }

    public Font getBodyFont() {
        if (bodyFont != null)
            return bodyFont;
        else {
            return Theme.getDefaultFont();
//            UIDefaults oDefaults = UIManager.getDefaults();
//            return oDefaults.getFont("Table.font");
        }
    }

    public void setBodyFont(Font font) {
        if (font != null) {
            bodyFont = font;
//            this.tableSelectedFont = font.deriveFont(Font.BOLD);
//            super.setFont(font);
        }
    }

    public Font getHeadingFont() {
        if (headingFont == null) {
            return Theme.getDefaultFont();
//            UIDefaults oDefaults = UIManager.getDefaults();
//            return oDefaults.getFont("TableHeader.font");
        } else {
            return headingFont;
        }
    }

    public void setHeadingFont(Font font) {
        headingFont = font;
//        SortButtonRenderer rend =	(SortButtonRenderer)getColumnModel().getColumn(0).getHeaderRenderer();
//        rend.setHeaderFont(font);
    }

    public Color getLastBidColumnBG() {
        return lastBidColumnBG;
    }

    public void setLastBidColumnBG(Color lastBidColumnBG) {
        this.lastBidColumnBG = lastBidColumnBG;
    }

    public Color getLastTradeQtyColumnBG() {
        return lastTradeQtyColumnBG;
    }

    public void setLastTradeQtyColumnBG(Color lastTradeQtyColumnBG) {
        this.lastTradeQtyColumnBG = lastTradeQtyColumnBG;
    }

    public Color getLastTradeQtyColumnFG() {
        return lastTradeQtyColumnFG;
    }

    public void setLastTradeQtyColumnFG(Color lastTradeQtyColumnFG) {
        this.lastTradeQtyColumnFG = lastTradeQtyColumnFG;
    }

    public Color getTradesColumnBG() {
        return tradesColumnBG;
    }

    public void setTradesColumnBG(Color tradesColumnBG) {
        this.tradesColumnBG = tradesColumnBG;
    }

    public Color getTradesColumnFG() {
        return tradesColumnFG;
    }

    public void setTradesColumnFG(Color tradesColumnFG) {
        this.tradesColumnFG = tradesColumnFG;
    }

    public Color getGridColor() {
        return gridColor;
    }

    public void setGridColor(Color gridColor) {
        this.gridColor = gridColor;
    }

    public Color getSelectedColumnBG() {
        return selectedColumnBG;
    }

    public void setSelectedColumnBG(Color selectedColumnBG) {
        this.selectedColumnBG = selectedColumnBG;
    }

    public Color getSelectedColumnFG() {
        return selectedColumnFG;
    }

    public void setSelectedColumnFG(Color selectedColumnFG) {
        this.selectedColumnFG = selectedColumnFG;
    }

    public String getWorkspaceString() {
        // order of colors are important
        StringBuffer buffer = new StringBuffer();
        buffer.append(SharedMethods.booleToInt(this.isGridOn())); //1
        buffer.append(",");
        buffer.append(getColorString(this.getGridColor())); //2
        buffer.append(",");
        buffer.append(getColorString(selectedColumnFG)); //3
        buffer.append(",");
        buffer.append(getColorString(selectedColumnBG)); //4
        buffer.append(",");
        buffer.append(getColorString(descColumnFG)); //5
        buffer.append(",");
        buffer.append(getColorString(descColumnBG)); //6
        buffer.append(",");
        buffer.append(getColorString(prevClosedColumnFG)); //7
        buffer.append(",");
        buffer.append(getColorString(prevClosedColumnBG)); //8
        buffer.append(",");
        buffer.append(getColorString(vwapColumnFG)); //9
        buffer.append(",");
        buffer.append(getColorString(vwapColumnBG)); //10
        buffer.append(",");
        buffer.append(getColorString(lowTradeColumnFG)); //11
        buffer.append(",");
        buffer.append(getColorString(lowTradeColumnBG)); //12
        buffer.append(",");
        buffer.append(getColorString(highTradeColumnFG)); //13
        buffer.append(",");
        buffer.append(getColorString(highTradeColumnBG)); //14
        buffer.append(",");
        buffer.append(getColorString(volumeColumnFG)); //15
        buffer.append(",");
        buffer.append(getColorString(volumeColumnBG)); //16
        buffer.append(",");
        buffer.append(getColorString(lastColumnFG)); //17
        buffer.append(",");
        buffer.append(getColorString(lastColumnBG)); //18
        buffer.append(",");
        buffer.append(getColorString(tradesColumnFG)); //19
        buffer.append(",");
        buffer.append(getColorString(tradesColumnBG)); //20
        buffer.append(",");
        buffer.append(getColorString(lastTradeQtyColumnFG)); //21
        buffer.append(",");
        buffer.append(getColorString(lastTradeQtyColumnBG)); //22
        buffer.append(",");
        buffer.append(getColorString(lowAskColumnFG)); //23
        buffer.append(",");
        buffer.append(getColorString(lowAskColumnBG)); //24
        buffer.append(",");
        buffer.append(getColorString(highAskColumnFG)); //25
        buffer.append(",");
        buffer.append(getColorString(highAskColumnBG)); //26
        buffer.append(",");
        buffer.append(getColorString(lowAskQtyColumnFG)); //27
        buffer.append(",");
        buffer.append(getColorString(lowAskQtyColumnBG)); //28
        buffer.append(",");
        buffer.append(getColorString(totAskQtyColumnFG)); //29
        buffer.append(",");
        buffer.append(getColorString(totAskQtyColumnBG)); //30
        buffer.append(",");
        buffer.append(getColorString(lastAskColumnFG)); //31
        buffer.append(",");
        buffer.append(getColorString(lastAskColumnBG)); //32
        buffer.append(",");
        buffer.append(getColorString(lastAskQtyColumnFG)); //33
        buffer.append(",");
        buffer.append(getColorString(lastAskQtyColumnBG)); //34
        buffer.append(",");
        buffer.append(getColorString(lowBidColumnFG)); //35
        buffer.append(",");
        buffer.append(getColorString(lowBidColumnBG)); //36
        buffer.append(",");
        buffer.append(getColorString(highBidColumnFG)); //37
        buffer.append(",");
        buffer.append(getColorString(highBidColumnBG)); //38
        buffer.append(",");
        buffer.append(getColorString(highBidQtyColumnFG)); //39
        buffer.append(",");
        buffer.append(getColorString(highBidQtyColumnBG)); //40
        buffer.append(",");
        buffer.append(getColorString(totBidQtyColumnFG)); //41
        buffer.append(",");
        buffer.append(getColorString(totBidQtyColumnBG)); //42
        buffer.append(",");
        buffer.append(getColorString(lastBidColumnFG)); //43
        buffer.append(",");
        buffer.append(getColorString(lastBidColumnBG)); //44
        buffer.append(",");
        buffer.append(getColorString(lastBidQtyColumnFG)); //45
        buffer.append(",");
        buffer.append(getColorString(lastBidQtyColumnBG)); //46

        return buffer.toString();
    }

    public void setWorkspaceString(String setting) {
        try {
            // order of colors are important

            String[] colors = setting.split(",");

            setGridOn(SharedMethods.intToBool(colors[0]));
            setGridColor(getColor(colors[1]));
            selectedColumnFG = getColor(colors[2]);
            selectedColumnBG = getColor(colors[3]);

            descColumnFG = getColor(colors[4]);
            descColumnBG = getColor(colors[5]);
            prevClosedColumnFG = getColor(colors[6]);
            prevClosedColumnBG = getColor(colors[7]);
            vwapColumnFG = getColor(colors[8]);
            vwapColumnBG = getColor(colors[9]);
            lowTradeColumnFG = getColor(colors[10]);
            lowTradeColumnBG = getColor(colors[11]);
            highTradeColumnFG = getColor(colors[12]);
            highTradeColumnBG = getColor(colors[13]);
            volumeColumnFG = getColor(colors[14]);
            volumeColumnBG = getColor(colors[15]);
            lastColumnFG = getColor(colors[16]);
            lastColumnBG = getColor(colors[17]);
            tradesColumnFG = getColor(colors[18]);
            tradesColumnBG = getColor(colors[19]);
            lastTradeQtyColumnFG = getColor(colors[20]);
            lastTradeQtyColumnBG = getColor(colors[21]);
            lowAskColumnFG = (getColor(colors[22]));
            lowAskColumnBG = (getColor(colors[23]));
            highAskColumnFG = getColor(colors[24]);
            highAskColumnBG = getColor(colors[25]);
            lowAskQtyColumnFG = getColor(colors[26]);
            lowAskQtyColumnBG = getColor(colors[27]);
            totAskQtyColumnFG = getColor(colors[28]);
            totAskQtyColumnBG = getColor(colors[29]);
            lastAskColumnFG = getColor(colors[30]);
            lastAskColumnBG = getColor(colors[31]);
            lastAskQtyColumnFG = getColor(colors[32]);
            lastAskQtyColumnBG = getColor(colors[33]);
            lowBidColumnFG = getColor(colors[34]);
            lowBidColumnBG = getColor(colors[35]);
            highBidColumnFG = getColor(colors[36]);
            highBidColumnBG = getColor(colors[37]);
            highBidQtyColumnFG = getColor(colors[38]);
            highBidQtyColumnBG = getColor(colors[39]);
            totBidQtyColumnFG = getColor(colors[40]);
            totBidQtyColumnBG = getColor(colors[41]);
            lastBidColumnFG = getColor(colors[42]);
            lastBidColumnBG = getColor(colors[43]);
            lastBidQtyColumnFG = getColor(colors[44]);
            lastBidQtyColumnBG = getColor(colors[45]);

        } catch (Exception e) {
        }

    }

    private Color getColor(String value) {
        int intVal = Integer.parseInt(value);
        if (intVal == -2) {
            return null;
        } else {
            return new Color(intVal);
        }
    }

    private String getColorString(Color color) {
        if (color == null)
            return "-2";
        else
            return "" + color.getRGB();
    }

    public boolean isGridOn() {
        return gridOn;
    }

    public void setGridOn(boolean gridOn) {
        this.gridOn = gridOn;
    }


    public int getBodyGap() {
        return bodyGap;
    }

    public void setBodyGap(int bodyGap) {
        this.bodyGap = bodyGap;
    }
}