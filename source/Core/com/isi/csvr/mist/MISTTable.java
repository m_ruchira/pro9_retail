package com.isi.csvr.mist;

import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.util.GroupableTableHeader;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.text.SimpleDateFormat;

public class MISTTable extends InternalFrame
        implements WindowWrapper, MouseListener, ActionListener,
        TWDesktopInterface {//, ItemListener,
    //PopupMenuListener, ActionListener, MouseListener {

    SimpleDateFormat g_oDateFormat = new SimpleDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    ViewSetting oSetting;
    private Table table;
    private JPopupMenu titleBarMenu;
    private int m_maxNumPage = 1;
    private int windowType;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private boolean columnResizeable = true;
    private InternalFrame timeAndSales;

    public MISTTable(ViewSetting oSeeting) {
        super("", oSeeting);
        this.oSetting = oSeeting;
        registerSymbols();
        //SharedMethods.printLine("*****************");
    }

    public void createTable() {
        this.setColumnResizeable(true);
        this.getContentPane().setLayout(new BorderLayout());
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+ Theme.getID()+"/InternalIcon.gif")));
        createTitlebarManu();
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
        table.hideCustomizer();
        //GUISettings.applyOrientation(this.table.getTable(),ComponentOrientation.LEFT_TO_RIGHT);
        this.getContentPane().add(table, BorderLayout.CENTER);
        table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    }

    public void updateGUI() {
        this.setTitle(table.getModel().getViewSettings().getCaption());
//        table.getTable().setToolTipText(table.getModel().getViewSettings().getCaption());
        table.getModel().applySettings();
        GroupableTableHeader header = (GroupableTableHeader) table.getTable().getTableHeader();
        header.updateUI();
        this.updateUI();
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        JMenuItem hideTitlemenu = new JMenuItem(Language.getString("HIDE_TITLEBAR"));
        hideTitlemenu.setActionCommand("HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        addTitlebarManu();

    }

    public void revalidateHeader() {
        try {
            if (table.getModel().getViewSettings() != null) {
                if (!table.getModel().getViewSettings().isHeaderVisible()) {
                    ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                    this.revalidate();
                }
            }
        } catch (Exception e) {
        }
    }

   /* public void addTitlebarManu() {
        Component np = (Component) ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }*/

    /*public void applyTheme(){
         table.gett getTable().updateUI();
         GroupableTableHeader header = (GroupableTableHeader)table.getTable().getTableHeader();
         header.updateUI();
     }*/

    /*private void setHideShowMenuCaption(){
        if (!isTitleVisible()){
            hideShowTitleMenu.setText(Language.getString("SHOW_TITLEBAR"));
        }else{
            hideShowTitleMenu.setText(Language.getString("HIDE_TITLEBAR"));
        }
    }*/

    /*public void toggleTitleBar() {
        setTitleVisible(!isTitleVisible());
        table.getModel().getViewSettings().setHeaderVisible(isTitleVisible());
        //setHideShowMenuCaption();
    }*/

    /*public void updateUI() {
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
    }*/

    public ViewSetting getViewSettings() {
        return table.getModel().getViewSettings();
    }

    /* Table wrapper methods */

    public boolean isWindowVisible() {
        return isVisible();
    }

    /*public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }*/

    public JTable getTable1() {
        try {
            return table.getTable();
        } catch (Exception e) {
            return null;
        }
    }

    public CommonTable getModel1() {
        try {
            return table.getModel();
        } catch (Exception e) {
            return null;
        }
    }

    public JTable getTable2() {
        return null;
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public void applySettings() {
        ViewSetting oSettings = null;
        if (table != null) {
            oSettings = table.getModel().getViewSettings();
            this.setSize(oSettings.getSize());
        }
//        String[] symbols = oSettings.getSymbols().getSymbols();
//        System.out.println("symbols size " + symbols.length);
//        for (int i = 0; i < symbols.length; i++) {
//            System.out.println(">>> " + symbols[i]);
//        }

        this.setLocation(oSettings.getLocation());
        //change start
        if (oSettings.isVisible()) {
            try {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
                } else {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            //change end
            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            }
        }

        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean sortOrder;
//System.out.println("##### SORTING MIST VIEW " + oSettings.getSortColumn() + " " + oSettings.getSortOrder() + " " + oSettings.getCaption());
        if (oSettings.getSortColumn() >= 0) {
//            if (oSettings.getSortOrder() == SortButtonRenderer.DOWN){
//                sortOrder = true;
//            }else
//                sortOrder = false;

            // Todo - To set the inital sorting order to ascending
            if (oSettings.getSortOrder() == SortButtonRenderer.DOWN) {
                sortOrder = false;
            } else
                sortOrder = true;
            oSettings.getSymbols().sortSymbols(oSettings.getSortColumn(), sortOrder);
            //SortButtonRenderer oRend =	(SortButtonRenderer)table.getColumnModel().getColumn(sortCol).getHeaderRenderer();
            //oRend.setSelectedColumn(sortCol,g_oViewSettings.getSortOrder());
//            System.out.println(g_oViewSettings.getID() + " -> " + g_oViewSettings.getSortOrder());
            //oRend = null;
        }
        if (oSettings.getLocation() != null)
            this.setLocation(oSettings.getLocation());
        //System.out.println("My title " + oSettings.getCaptionID());
        this.setVisible(oSettings.isVisible());
        setWindowTitle();
        setDecimalPlaces();
        //revalidateHeader();
//        table.getModel().adjustColumns();
    }

    private void setDecimalPlaces() {
        String decimalPlaces = table.getModel().getViewSettings().getProperty(ViewConstants.VC_DECIMAL_PLACES);
        if (decimalPlaces != null) {
            try {
                ((MISTModel) table.getModel()).setDecimalPlaces(Byte.parseByte(decimalPlaces));
            } catch (Exception ex) {
                ((MISTModel) table.getModel()).setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        }
        decimalPlaces = null;
    }

    /*public boolean isTitleVisible() {
        //if (g_oViewSettings != null){
        return table.getModel().getViewSettings().isHeaderVisible();
        *//*}else{
            return true;
        }*//*
    }*/

    /*public void setTitleVisible(boolean setVisible) {
        ViewSetting settings = table.getModel().getViewSettings();
        if (setVisible) {
            if (settings != null) {
                settings.setHeaderVisible(true);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
//                System.out.println("hiding");
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (settings != null) {
                    settings.setHeaderVisible(false);
                }
            }
        }
        settings = null;
    }*/

    public void windowClosing() {

    }

    /*public Object getDetachedFrame() {
        return null;
    }*/

    /*public boolean isDetached() {
        return false;
    }*/
    
    /* -- */

    public String toString() {
        return table.getModel().getViewSettings().getCaption();
    }

    /* Mouse Listener Methods */

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane)
            if (SwingUtilities.isRightMouseButton(e))
                GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
    }

    public void mousePressed(MouseEvent e) {
        /*try{
            this.setSelected(true);
        }catch(Exception ex){
            ex.printStackTrace();
        }*/

    }

    public void mouseReleased(MouseEvent e) {
        //System.out.println("released");
        //mouseDragging = false;
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
        //System.out.println("dragging");
        //if ((selected != " ") && (e.getStateChange() == e.SELECTED)){
        //mouseDragging = true;
    }

    public void mouseMoved(MouseEvent e) {
        //System.out.println("moving");
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("HIDE")) {
            toggleTitleBar();
        }
    }

    public int getWindowType() {
        return windowType;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }

    public void closeWindow() {
//		if (getDefaultCloseOperation() == HIDE_ON_CLOSE){
//			setVisible(false);
//		}else{
//			dispose();
//		}
        doDefaultCloseAction();
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

//     public void doDefaultCloseAction(){
//         super.doDefaultCloseAction();
//         focusNextWindow();
//     }
//
//     private void focusNextWindow(){
//         TWDesktop desktop = (TWDesktop)Client.getInstance().getDesktop();
//         desktop.focusNextWindow(desktop.getListPosition(this,true),true);
//         desktop = null;
//     }
//

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public void printTable() {
        System.out.println("Inside the prinTable method at MISTTable");
    }

    public InternalFrame getTimeAndSalesWindow() {
        return timeAndSales;
    }

    public void setTimeAndSalesWindow(InternalFrame frame) {
        this.timeAndSales = frame;
        //this.tradeStore = tradeStore;
    }

    public boolean isColumnResizeable() {
        return columnResizeable;
    }

    public void setColumnResizeable(boolean columnResizeable) {
        this.columnResizeable = columnResizeable;
    }

    public boolean isDetachable() {
        return true;
    }

    public boolean isPrintable() {
        return true;
    }

    public void printContents() {
        SharedMethods.printTable(this.getTable().getTable(), PrintManager.TABLE_TYPE_MIST_VIEW, this.getTitle());
    }

    public void showServices() {
    }

    public void showLinkGroup() {

    }

    public String getLinkGroupIDForTitleBar() {
        return null;
    }

    public boolean isServicesEnabled() {
        return false;
    }

    public boolean isLinkGroupEnabled() {
        return false;
    }

    public void setWindowTitle() {
        String title = Language.getString("MIST_VIEW");
        title = title + " - " + getViewSettings().getCaption();
        if ((getViewSettings().getCurrency() != null) && (!getViewSettings().getCurrency().equals("null"))) {
            title = title + " - " + getViewSettings().getCurrency();
        }
        this.setTitle(title);
        title = null;
    }

    private void registerSymbols() {
        String[] symbols = oSetting.getSymbols().getSymbols();

        try {
            for (int i = 0; i < symbols.length; i++) {
                //            Stock stock= DataStore.getSharedInstance().getStockObject(sy)
                DataStore.getSharedInstance().addSymbolRequest(SharedMethods.getExchangeFromKey(symbols[i]), SharedMethods.getSymbolFromKey(symbols[i]),
                        SharedMethods.getInstrumentTypeFromKey(symbols[i]), true, this, -1, true, symbols);
                if (!ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(symbols[i])).isDefault()) {
                    DataStore.getSharedInstance().getStockObject(symbols[i]).setSymbolEnabled(true);
                }
            }
        } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        symbols = null;
    }

    private void unregisterSymbols() {
        String[] symbols = oSetting.getSymbols().getSymbols();

        for (int i = 0; i < symbols.length; i++) {
            DataStore.getSharedInstance().removeSymbolRequest(symbols[i]);
        }
        symbols = null;
    }

    /* Printable method */
    public void internalFrameClosed(InternalFrameEvent e) {
        super.internalFrameClosed(e);    //To change body of overridden methods use File | Settings | File Templates.
        try {
            unregisterSymbols();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void internalFrameIconified(InternalFrameEvent e) {
        super.internalFrameIconified(e);    //To change body of overridden methods use File | Settings | File Templates.
        try {
            unregisterSymbols();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
        super.internalFrameDeiconified(e);    //To change body of overridden methods use File | Settings | File Templates.
        try {
            registerSymbols();
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public boolean finalizeWindow() {
        try {
            unregisterSymbols();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return super.finalizeWindow();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void showWindow() {
        try {
            registerSymbols();
            super.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkForDetach() {
        super.checkForDetach();    //To change body of overridden methods use File | Settings | File Templates.
        this.setLocation(oSetting.getLocation());
    }

    public void setIcon(boolean b) throws PropertyVetoException {
        try {
            if (b) {
                this.setLayer(GUISettings.DESKTOP_ICON_LAYER);
            } else {
                if (Settings.isPutAllToSameLayer()) {
                    this.setLayer(GUISettings.DEFAULT_LAYER);
                    if (oSetting != null) {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSetting.getIndex());
                    } else {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                    }
                } else {
                    this.setLayer(GUISettings.DEFAULT_LAYER);
                    if (oSetting != null) {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSetting.getIndex());
                    } else {
                        this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setIcon(b);    //To change body of overridden methods use File | Settings | File Templates.
    }
}