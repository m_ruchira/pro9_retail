package com.isi.csvr.mist;

import com.isi.csvr.Client;
import com.isi.csvr.FontChooser;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class MISTColorConfigPanel extends JPanel implements ActionListener {

    private int WIDTH = 220; //185;
    private int BUTTON_WIDTH = 60;
    private JColorChooser colorChooser;
    private JDialog colorDialog;
    private FontChooser fontChooser;
    private Font oldFont;
    private Font newBodyFont;
    private Font newHeadingFont;

    private FlowLayout flowLayout1 = new FlowLayout(FlowLayout.TRAILING);

    private JLabel lblDescColumn = new JLabel();
    private JButton btnFgDescColumn = new JButton();
    private JButton btnBgDescColumn = new JButton();

    private JLabel lblPrevClosedColumn = new JLabel();
    private JButton btnFgPrevClosedColumn = new JButton();
    private JButton btnBgPrevClosedColumn = new JButton();

    private JLabel lblVWAPColumn = new JLabel();
    private JButton btnFgVWAPColumn = new JButton();
    private JButton btnBgVWAPColumn = new JButton();
    // Trade Column Settings
    private JLabel lblLowTradeColumn = new JLabel();
    private JButton btnFgLowTradeColumn = new JButton();
    private JButton btnBgLowTradeColumn = new JButton();

    private JLabel lblHighTradeColumn = new JLabel();
    private JButton btnFgHighTradeColumn = new JButton();
    private JButton btnBgHighTradeColumn = new JButton();

    private JLabel lblTradesColumn = new JLabel();
    private JButton btnFgTradesColumn = new JButton();
    private JButton btnBgTradesColumn = new JButton();

    private JLabel lblTradeVolumeColumn = new JLabel();
    private JButton btnFgTradeVolumeColumn = new JButton();
    private JButton btnBgTradeVolumeColumn = new JButton();

    private JLabel lblLastTradeColumn = new JLabel();
    private JButton btnFgLastTradeColumn = new JButton();
    private JButton btnBgLastTradeColumn = new JButton();

    private JLabel lblLastTradeQtyColumn = new JLabel();
    private JButton btnFgLastTradeQtyColumn = new JButton();
    private JButton btnBgLastTradeQtyColumn = new JButton();
    // Ask Column Settings
    private JLabel lblLowAskColumn = new JLabel();
    private JButton btnFgLowAskColumn = new JButton();
    private JButton btnBgLowAskColumn = new JButton();

    private JLabel lblHighAskColumn = new JLabel();
    private JButton btnFgHighAskColumn = new JButton();
    private JButton btnBgHighAskColumn = new JButton();

    private JLabel lblLowAskQtyColumn = new JLabel();
    private JButton btnFgLowAskQtyColumn = new JButton();
    private JButton btnBgLowAskQtyColumn = new JButton();

    private JLabel lblTotAskQtyColumn = new JLabel();
    private JButton btnFgTotAskQtyColumn = new JButton();
    private JButton btnBgTotAskQtyColumn = new JButton();

    private JLabel lblLastAskColumn = new JLabel();
    private JButton btnFgLastAskColumn = new JButton();
    private JButton btnBgLastAskColumn = new JButton();

    private JLabel lblLastAskQtyColumn = new JLabel();
    private JButton btnFgLastAskQtyColumn = new JButton();
    private JButton btnBgLastAskQtyColumn = new JButton();
    // Bid Column Settings
    private JLabel lblLowBidColumn = new JLabel();
    private JButton btnFgLowBidColumn = new JButton();
    private JButton btnBgLowBidColumn = new JButton();

    private JLabel lblHighBidColumn = new JLabel();
    private JButton btnFgHighBidColumn = new JButton();
    private JButton btnBgHighBidColumn = new JButton();

    private JLabel lblHighBidQtyColumn = new JLabel();
    private JButton btnFgHighBidQtyColumn = new JButton();
    private JButton btnBgHighBidQtyColumn = new JButton();

    private JLabel lblTotBidkQtyColumn = new JLabel();
    private JButton btnFgTotBidQtyColumn = new JButton();
    private JButton btnBgTotBidQtyColumn = new JButton();

    private JLabel lblLastBidColumn = new JLabel();
    private JButton btnFgLastBidColumn = new JButton();
    private JButton btnBgLastBidColumn = new JButton();

    private JLabel lblLastBidQtyColumn = new JLabel();
    private JButton btnFgLastBidQtyColumn = new JButton();
    private JButton btnBgLastBidQtyColumn = new JButton();

    private JTabbedPane tabbedPane = new JTabbedPane();
    private JPanel commonPanel = new JPanel();
    private JPanel tradePanel = new JPanel();
    private JPanel bidPanel = new JPanel();
    private JPanel askPanel = new JPanel();
    private JPanel colorPanel = new JPanel();
    private JPanel fontPanel = new JPanel();

    private JButton btnBodyFont = new JButton();
    private JButton btnHeadingFont = new JButton();
    private JLabel lblBodyFont = new JLabel();
    private JLabel lblHeadingFont = new JLabel();

    private Color oldColor;
    private Color newColor;
    private JButton source;
    private String lastCommand = "";
    private JButton btnCLose = new JButton();
    private JCheckBox chkEnable = new JCheckBox();
    private JDialog parent;
    //    private JLabel lblUp = new JLabel();
    private JLabel lblSelected = new JLabel();
    private JButton btnSelectedFG = new JButton();
    private JButton btnSelectedBG = new JButton();
    private JLabel lblGrid = new JLabel();
    private JCheckBox chkGrid = new JCheckBox();
    private JButton btnGrig = new JButton();

    private MISTTableSettings target;
    private SmartTable smartTable;
    private Table table;
    private int bodyGap;
    private JLabel lblBodyGap = new JLabel();
    private TWComboBox btnBodyGap = null;

    public MISTColorConfigPanel(JDialog parent) {
        this.parent = parent;
        try {
            jbInit();
            // pack();
            //Theme.registerComponent(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setTarget(Table sTable) { // MISTTableSettings target){
        this.table = sTable;
        this.smartTable = sTable.getSmartTable();
        this.target = (MISTTableSettings) smartTable.getTableSettings();
        loadInitialSettings();
        enableButtons(commonPanel, chkEnable.isSelected());
        enableButtons(tradePanel, chkEnable.isSelected());
        enableButtons(askPanel, chkEnable.isSelected());
        enableButtons(bidPanel, chkEnable.isSelected());
    }

    void jbInit() throws Exception {
        JLabel cap1 = new JLabel(Language.getString("SETTING"));
        cap1.setFont(cap1.getFont().deriveFont(Font.BOLD));
        cap1.setPreferredSize(new Dimension(160, 20));
        JLabel cap2 = new JLabel(Language.getString("FONT_COLOR"));
        cap2.setFont(cap1.getFont().deriveFont(Font.BOLD));
        JLabel cap3 = new JLabel(Language.getString("CELL_COLOR"));
        cap3.setFont(cap1.getFont().deriveFont(Font.BOLD));
        GUISettings.setSameSize(cap2, cap3);

        JLabel cap4 = new JLabel(Language.getString("SETTING"));
        cap4.setFont(cap1.getFont().deriveFont(Font.BOLD));
        cap4.setPreferredSize(new Dimension(160, 20));
        JLabel cap5 = new JLabel(Language.getString("FONT_COLOR"));
        cap5.setFont(cap1.getFont().deriveFont(Font.BOLD));
        JLabel cap6 = new JLabel(Language.getString("CELL_COLOR"));
        cap6.setFont(cap1.getFont().deriveFont(Font.BOLD));
        GUISettings.setSameSize(cap5, cap6);

        JLabel cap7 = new JLabel(Language.getString("SETTING"));
        cap7.setFont(cap1.getFont().deriveFont(Font.BOLD));
        cap7.setPreferredSize(new Dimension(160, 20));
        JLabel cap8 = new JLabel(Language.getString("FONT_COLOR"));
        cap8.setFont(cap1.getFont().deriveFont(Font.BOLD));
        JLabel cap9 = new JLabel(Language.getString("CELL_COLOR"));
        cap9.setFont(cap1.getFont().deriveFont(Font.BOLD));
        GUISettings.setSameSize(cap8, cap9);

        JLabel cap10 = new JLabel(Language.getString("SETTING"));
        cap10.setFont(cap1.getFont().deriveFont(Font.BOLD));
        cap10.setPreferredSize(new Dimension(160, 20));
        JLabel cap11 = new JLabel(Language.getString("FONT_COLOR"));
        cap11.setFont(cap1.getFont().deriveFont(Font.BOLD));
        JLabel cap12 = new JLabel(Language.getString("CELL_COLOR"));
        cap12.setFont(cap1.getFont().deriveFont(Font.BOLD));
        GUISettings.setSameSize(cap11, cap12);

        BUTTON_WIDTH = (int) cap2.getPreferredSize().getWidth();

        WIDTH += (BUTTON_WIDTH * 2);
        JLabel previewPane = new JLabel(Language.getString("WINDOW_TITLE_CLIENT"));
        previewPane.setFont(new TWFont("Arial", Font.BOLD, 20));
        colorChooser = new JColorChooser(Color.red);
        colorChooser.setPreviewPanel(previewPane);
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);

        fontChooser = new FontChooser(Client.getInstance().getFrame(), true);
        //this.setBorder(BorderFactory.createEtchedBorder());
        //this.setPreferredSize(new Dimension(250, 520));
        BoxLayout topLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
        this.setLayout(topLayout);

        fontPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        fontPanel.setBorder(BorderFactory.createEtchedBorder());
        fontPanel.setPreferredSize(new Dimension(WIDTH, 90));   // 60
        btnBodyFont.setPreferredSize(new Dimension(20, 20));
        btnBodyFont.setActionCommand("BodyFont");
        btnBodyFont.setText("...");
        btnBodyFont.setActionCommand("FONT_BODY");
        btnBodyFont.addActionListener(this);
        btnHeadingFont.setPreferredSize(new Dimension(20, 20));
        btnHeadingFont.setToolTipText("");
        btnHeadingFont.setActionCommand("HeadFont");
        btnHeadingFont.setText("...");
        btnHeadingFont.setActionCommand("FONT_HEADING");
        btnHeadingFont.addActionListener(this);
        lblBodyFont.setPreferredSize(new Dimension(180, 20));
        lblBodyFont.setText(Language.getString("MAIN_BOARD_FONT"));
        lblHeadingFont.setPreferredSize(new Dimension(180, 20));
        lblHeadingFont.setText(Language.getString("HEADING_FONT"));

        lblBodyGap.setPreferredSize(new Dimension(180, 20));
        lblBodyGap.setText(Language.getString("BODY_GAP"));
        ArrayList elements = new ArrayList();
        for (int i = 0; i < 11; i++) {
            elements.add(i);
        }
        TWComboModel model = new TWComboModel(elements);
        btnBodyGap = new TWComboBox(model);
        btnBodyGap.setPreferredSize(new Dimension(50, 20));
        btnBodyGap.setToolTipText("");
        btnBodyGap.setActionCommand("BODY_GAP");
        btnBodyGap.addActionListener(this);

        this.add(fontPanel, null);
        fontPanel.add(lblBodyFont, null);
        fontPanel.add(btnBodyFont, null);
        fontPanel.add(lblHeadingFont, null);
        fontPanel.add(btnHeadingFont, null);
        fontPanel.add(lblBodyGap, null);
        fontPanel.add(btnBodyGap, null);

        btnCLose.setText(Language.getString("CLOSE"));
        btnCLose.addActionListener(this);
        btnCLose.setActionCommand("CLOSE");
        flowLayout1.setHgap(2);

        chkEnable.setPreferredSize(new Dimension(WIDTH, 25));
        chkEnable.setText(Language.getString("ENABLE_CUSTOMIZER_CUSTOM_SETTINGS"));
        chkEnable.addActionListener(this);
        chkEnable.setActionCommand("ENABLE");

        chkGrid.setPreferredSize(new Dimension(WIDTH, 20));
        chkGrid.setText(Language.getString("ENABLE_GRID"));
        chkGrid.setActionCommand("ENABLE_GRID");
        chkGrid.addActionListener(this);

        JPanel pnlChk = new JPanel(new FlowLayout());
        pnlChk.setPreferredSize(new Dimension(WIDTH, 40)); // 55
//        pnlChk.add(chkGrid);
        pnlChk.add(chkEnable);
        this.add(pnlChk, null);
        //this.add(chkEnable, null);

        commonPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        commonPanel.setBorder(BorderFactory.createEtchedBorder());
        commonPanel.setPreferredSize(new Dimension(WIDTH, 220));
//        tabbedPane.addTab(Language.getString("TOP_GAINERS"), commonPanel);
        tabbedPane.addTab(Language.getString("COMMON"), commonPanel);
        tabbedPane.setSelectedIndex(0);

        tradePanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        tradePanel.setBorder(BorderFactory.createEtchedBorder());
        tradePanel.setPreferredSize(new Dimension(WIDTH, 220)); // 390
//        tabbedPane.addTab(Language.getString("TOP_GAINERS"), tradePanel);
        tabbedPane.addTab(Language.getString("TRADE"), tradePanel);

        askPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        askPanel.setBorder(BorderFactory.createEtchedBorder());
        askPanel.setPreferredSize(new Dimension(WIDTH, 220));
//        tabbedPane.addTab(Language.getString("TOP_GAINERS"), askPanel);
        //change start
        tabbedPane.addTab(Language.getString("OFFER"), askPanel);

        bidPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        bidPanel.setBorder(BorderFactory.createEtchedBorder());
        bidPanel.setPreferredSize(new Dimension(WIDTH, 220));
//        tabbedPane.addTab(Language.getString("TOP_GAINERS"), bidPanel);
        tabbedPane.addTab(Language.getString("BID"), bidPanel);

        tabbedPane.setBorder(BorderFactory.createEtchedBorder());
        tabbedPane.setPreferredSize(new Dimension(WIDTH, 220));
        this.add(tabbedPane, null);

        commonPanel.add(cap1);
        commonPanel.add(cap2);
        commonPanel.add(cap3);

//		colorPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
//		colorPanel.setBorder(BorderFactory.createEtchedBorder());
//		colorPanel.setPreferredSize(new Dimension(WIDTH, 390));
//        this.add(colorPanel, null);
//
//		colorPanel.add(cap1);
//        colorPanel.add(cap2);
//        colorPanel.add(cap3);

        lblGrid.setPreferredSize(new Dimension(160, 20));
        lblGrid.setText(Language.getString("GRID_COLOR"));
        btnGrig.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnGrig.setActionCommand("COLOR_GRID");
        btnGrig.addActionListener(this);

        lblSelected.setPreferredSize(new Dimension(160, 20));
        lblSelected.setText(Language.getString("SELECTED_CELLS"));
        btnSelectedFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedFG.setActionCommand("COLOR_FG_SELECTED");
        btnSelectedFG.addActionListener(this);
        btnSelectedBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedBG.setActionCommand("COLOR_BG_SELECTED");
        btnSelectedBG.addActionListener(this);

        lblDescColumn.setPreferredSize(new Dimension(160, 17));
        lblDescColumn.setText(Language.getString("DESCRIPTION"));
        btnFgDescColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgDescColumn.setActionCommand("COLOR_FG_DESC");
        btnFgDescColumn.addActionListener(this);
        btnBgDescColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgDescColumn.setActionCommand("COLOR_BG_DESC");
        btnBgDescColumn.addActionListener(this);

        lblPrevClosedColumn.setPreferredSize(new Dimension(160, 17));
        lblPrevClosedColumn.setText(Language.getString("PREV_CLOSE"));
        btnFgPrevClosedColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgPrevClosedColumn.setActionCommand("COLOR_FG_PREV_CLOSED");
        btnFgPrevClosedColumn.addActionListener(this);
        btnBgPrevClosedColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgPrevClosedColumn.setActionCommand("COLOR_BG_PREV_CLOSED");
        btnBgPrevClosedColumn.addActionListener(this);

        lblVWAPColumn.setPreferredSize(new Dimension(160, 17));
        lblVWAPColumn.setText(Language.getString("VWAP"));
        btnFgVWAPColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgVWAPColumn.setActionCommand("COLOR_FG_VWAP");
        btnFgVWAPColumn.addActionListener(this);
        btnBgVWAPColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgVWAPColumn.setActionCommand("COLOR_BG_VWAP");
        btnBgVWAPColumn.addActionListener(this);

        lblLowTradeColumn.setPreferredSize(new Dimension(160, 17));
        lblLowTradeColumn.setText(Language.getString("LOW_TRADE"));
        btnFgLowTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLowTradeColumn.setActionCommand("COLOR_FG_LOW_TRADE");
        btnFgLowTradeColumn.addActionListener(this);
        btnBgLowTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLowTradeColumn.setActionCommand("COLOR_BG_LOW_TRADE");
        btnBgLowTradeColumn.addActionListener(this);

        lblHighTradeColumn.setPreferredSize(new Dimension(160, 17));
        lblHighTradeColumn.setText(Language.getString("HIGH_TRADE"));
        btnFgHighTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgHighTradeColumn.setActionCommand("COLOR_FG_HIGH_TRADE");
        btnFgHighTradeColumn.addActionListener(this);
        btnBgHighTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgHighTradeColumn.setActionCommand("COLOR_BG_HIGH_TRADE");
        btnBgHighTradeColumn.addActionListener(this);

        lblTradesColumn.setPreferredSize(new Dimension(160, 17));
        lblTradesColumn.setText(Language.getString("NO_OF_TRADES"));
        btnFgTradesColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgTradesColumn.setActionCommand("COLOR_FG_TRADES");
        btnFgTradesColumn.addActionListener(this);
        btnBgTradesColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgTradesColumn.setActionCommand("COLOR_BG_TRADES");
        btnBgTradesColumn.addActionListener(this);

        lblTradeVolumeColumn.setPreferredSize(new Dimension(160, 17));
        lblTradeVolumeColumn.setText(Language.getString("TRADED_VOLUME"));
        btnFgTradeVolumeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgTradeVolumeColumn.setActionCommand("COLOR_FG_TRADE_VOLUME");
        btnFgTradeVolumeColumn.addActionListener(this);
        btnBgTradeVolumeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgTradeVolumeColumn.setActionCommand("COLOR_BG_TRADE_VOLUME");
        btnBgTradeVolumeColumn.addActionListener(this);

        lblLastTradeColumn.setPreferredSize(new Dimension(160, 17));
        lblLastTradeColumn.setText(Language.getString("LAST_TRADE"));
        btnFgLastTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastTradeColumn.setActionCommand("COLOR_FG_LAST_TRADE");
        btnFgLastTradeColumn.addActionListener(this);
        btnBgLastTradeColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastTradeColumn.setActionCommand("COLOR_BG_LAST_TRADE");
        btnBgLastTradeColumn.addActionListener(this);

        lblLastTradeQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblLastTradeQtyColumn.setText(Language.getString("LASTTRADE_QTY"));
        btnFgLastTradeQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastTradeQtyColumn.setActionCommand("COLOR_FG_TRADE_QTY");
        btnFgLastTradeQtyColumn.addActionListener(this);
        btnBgLastTradeQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastTradeQtyColumn.setActionCommand("COLOR_BG_TRADE_QTY");
        btnBgLastTradeQtyColumn.addActionListener(this);

        lblLowAskColumn.setPreferredSize(new Dimension(160, 17));
        lblLowAskColumn.setText(Language.getString("LOW_ASK"));
        btnFgLowAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLowAskColumn.setActionCommand("COLOR_FG_LOW_ASK");
        btnFgLowAskColumn.addActionListener(this);
        btnBgLowAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLowAskColumn.setActionCommand("COLOR_BG_LOW_ASK");
        btnBgLowAskColumn.addActionListener(this);

        lblHighAskColumn.setPreferredSize(new Dimension(160, 17));
        lblHighAskColumn.setText(Language.getString("HIGH_ASK"));
        btnFgHighAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgHighAskColumn.setActionCommand("COLOR_FG_HIGH_ASK");
        btnFgHighAskColumn.addActionListener(this);
        btnBgHighAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgHighAskColumn.setActionCommand("COLOR_BG_HIGH_ASK");
        btnBgHighAskColumn.addActionListener(this);

        lblLowAskQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblLowAskQtyColumn.setText(Language.getString("LOW_ASK_QTY"));
        btnFgLowAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLowAskQtyColumn.setActionCommand("COLOR_FG_LOW_ASK_QTY");
        btnFgLowAskQtyColumn.addActionListener(this);
        btnBgLowAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLowAskQtyColumn.setActionCommand("COLOR_BG_LOW_ASK_QTY");
        btnBgLowAskQtyColumn.addActionListener(this);

        lblTotAskQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblTotAskQtyColumn.setText(Language.getString("TOTAL_OFFER_QTY"));
        btnFgTotAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgTotAskQtyColumn.setActionCommand("COLOR_FG_TOT_ASK_QTY");
        btnFgTotAskQtyColumn.addActionListener(this);
        btnBgTotAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgTotAskQtyColumn.setActionCommand("COLOR_BG_TOT_ASK_QTY");
        btnBgTotAskQtyColumn.addActionListener(this);

        lblLastAskColumn.setPreferredSize(new Dimension(160, 17));
        lblLastAskColumn.setText(Language.getString("LAST_ASK"));
        btnFgLastAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastAskColumn.setActionCommand("COLOR_FG_LAST_ASK");
        btnFgLastAskColumn.addActionListener(this);
        btnBgLastAskColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastAskColumn.setActionCommand("COLOR_BG_LAST_ASK");
        btnBgLastAskColumn.addActionListener(this);

        lblLastAskQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblLastAskQtyColumn.setText(Language.getString("LAST_ASK_QTY"));
        btnFgLastAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastAskQtyColumn.setActionCommand("COLOR_FG_LAST_ASK_QTY");
        btnFgLastAskQtyColumn.addActionListener(this);
        btnBgLastAskQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastAskQtyColumn.setActionCommand("COLOR_BG_LAST_ASK_QTY");
        btnBgLastAskQtyColumn.addActionListener(this);

        // Bid
        lblLowBidColumn.setPreferredSize(new Dimension(160, 17));
        lblLowBidColumn.setText(Language.getString("LOW_BID"));
        btnFgLowBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLowBidColumn.setActionCommand("COLOR_FG_LOW_BID");
        btnFgLowBidColumn.addActionListener(this);
        btnBgLowBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLowBidColumn.setActionCommand("COLOR_BG_LOW_BID");
        btnBgLowBidColumn.addActionListener(this);

        lblHighBidColumn.setPreferredSize(new Dimension(160, 17));
        lblHighBidColumn.setText(Language.getString("HIGH_BID"));
        btnFgHighBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgHighBidColumn.setActionCommand("COLOR_FG_HIGH_BID");
        btnFgHighBidColumn.addActionListener(this);
        btnBgHighBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgHighBidColumn.setActionCommand("COLOR_BG_HIGH_BID");
        btnBgHighBidColumn.addActionListener(this);

        lblHighBidQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblHighBidQtyColumn.setText(Language.getString("HIGH_BID_QTY"));
        btnFgHighBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgHighBidQtyColumn.setActionCommand("COLOR_FG_HIGH_BID_QTY");
        btnFgHighBidQtyColumn.addActionListener(this);
        btnBgHighBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgHighBidQtyColumn.setActionCommand("COLOR_BG_HIGH_BID_QTY");
        btnBgHighBidQtyColumn.addActionListener(this);

        lblTotBidkQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblTotBidkQtyColumn.setText(Language.getString("TOTAL_BID_QTY"));
        btnFgTotBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgTotBidQtyColumn.setActionCommand("COLOR_FG_TOT_BID_QTY");
        btnFgTotBidQtyColumn.addActionListener(this);
        btnBgTotBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgTotBidQtyColumn.setActionCommand("COLOR_BG_TOT_BID_QTY");
        btnBgTotBidQtyColumn.addActionListener(this);

        lblLastBidColumn.setPreferredSize(new Dimension(160, 17));
        lblLastBidColumn.setText(Language.getString("LAST_BID"));
        btnFgLastBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastBidColumn.setActionCommand("COLOR_FG_LAST_BID");
        btnFgLastBidColumn.addActionListener(this);
        btnBgLastBidColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastBidColumn.setActionCommand("COLOR_BG_LAST_BID");
        btnBgLastBidColumn.addActionListener(this);

        lblLastBidQtyColumn.setPreferredSize(new Dimension(160, 17));
        lblLastBidQtyColumn.setText(Language.getString("LAST_BID_QTY"));
        btnFgLastBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgLastBidQtyColumn.setActionCommand("COLOR_FG_LAST_BID_QTY");
        btnFgLastBidQtyColumn.addActionListener(this);
        btnBgLastBidQtyColumn.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgLastBidQtyColumn.setActionCommand("COLOR_BG_LAST_BID_QTY");
        btnBgLastBidQtyColumn.addActionListener(this);

        JPanel closePanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 2, 5));
        closePanel.add(btnCLose);
        this.add(closePanel, null);

        commonPanel.add(lblSelected, null);
        commonPanel.add(btnSelectedFG, null);
        commonPanel.add(btnSelectedBG, null);
        commonPanel.add(lblDescColumn, null);
        commonPanel.add(btnFgDescColumn, null);
        commonPanel.add(btnBgDescColumn, null);
        commonPanel.add(lblPrevClosedColumn, null);
        commonPanel.add(btnFgPrevClosedColumn, null);
        commonPanel.add(btnBgPrevClosedColumn, null);
        commonPanel.add(lblVWAPColumn, null);
        commonPanel.add(btnFgVWAPColumn, null);
        commonPanel.add(btnBgVWAPColumn, null);
//        commonPanel.add(lblGrid, null);
        //commonPanel.add(chkGrid, null);
//        commonPanel.add(btnGrig, null);

        tradePanel.add(cap4);
        tradePanel.add(cap5);
        tradePanel.add(cap6);
        tradePanel.add(lblLowTradeColumn, null);
        tradePanel.add(btnFgLowTradeColumn, null);
        tradePanel.add(btnBgLowTradeColumn, null);
        tradePanel.add(lblHighTradeColumn, null);
        tradePanel.add(btnFgHighTradeColumn, null);
        tradePanel.add(btnBgHighTradeColumn, null);
        tradePanel.add(lblTradesColumn, null);
        tradePanel.add(btnFgTradesColumn, null);
        tradePanel.add(btnBgTradesColumn, null);
        tradePanel.add(lblTradeVolumeColumn, null);
        tradePanel.add(btnFgTradeVolumeColumn, null);
        tradePanel.add(btnBgTradeVolumeColumn, null);
        tradePanel.add(lblLastTradeColumn, null);
        tradePanel.add(btnFgLastTradeColumn, null);
        tradePanel.add(btnBgLastTradeColumn, null);
        tradePanel.add(lblLastTradeQtyColumn, null);
        tradePanel.add(btnFgLastTradeQtyColumn, null);
        tradePanel.add(btnBgLastTradeQtyColumn, null);

        askPanel.add(cap7);
        askPanel.add(cap8);
        askPanel.add(cap9);
        askPanel.add(lblLowAskColumn, null);
        askPanel.add(btnFgLowAskColumn, null);
        askPanel.add(btnBgLowAskColumn, null);
        askPanel.add(lblHighAskColumn, null);
        askPanel.add(btnFgHighAskColumn, null);
        askPanel.add(btnBgHighAskColumn, null);
        askPanel.add(lblLowAskQtyColumn, null);
        askPanel.add(btnFgLowAskQtyColumn, null);
        askPanel.add(btnBgLowAskQtyColumn, null);
        askPanel.add(lblTotAskQtyColumn, null);
        askPanel.add(btnFgTotAskQtyColumn, null);
        askPanel.add(btnBgTotAskQtyColumn, null);
        askPanel.add(lblLastAskColumn, null);
        askPanel.add(btnFgLastAskColumn, null);
        askPanel.add(btnBgLastAskColumn, null);
        askPanel.add(lblLastAskQtyColumn, null);
        askPanel.add(btnFgLastAskQtyColumn, null);
        askPanel.add(btnBgLastAskQtyColumn, null);

        bidPanel.add(cap10);
        bidPanel.add(cap11);
        bidPanel.add(cap12);
        bidPanel.add(lblLowBidColumn, null);
        bidPanel.add(btnFgLowBidColumn, null);
        bidPanel.add(btnBgLowBidColumn, null);
        bidPanel.add(lblHighBidColumn, null);
        bidPanel.add(btnFgHighBidColumn, null);
        bidPanel.add(btnBgHighBidColumn, null);
        bidPanel.add(lblHighBidQtyColumn, null);
        bidPanel.add(btnFgHighBidQtyColumn, null);
        bidPanel.add(btnBgHighBidQtyColumn, null);
        bidPanel.add(lblTotBidkQtyColumn, null);
        bidPanel.add(btnFgTotBidQtyColumn, null);
        bidPanel.add(btnBgTotBidQtyColumn, null);
        bidPanel.add(lblLastBidColumn, null);
        bidPanel.add(btnFgLastBidColumn, null);
        bidPanel.add(btnBgLastBidColumn, null);
        bidPanel.add(lblLastBidQtyColumn, null);
        bidPanel.add(btnFgLastBidQtyColumn, null);
        bidPanel.add(btnBgLastBidQtyColumn, null);

        GUISettings.applyOrientation(this);
        setBorders();
    }

    private void loadInitialSettings() {
        if (target == null) {
//            System.out.println("target Object is Null at loadInitialSettings");
            return;
        }
//		((CustomThemeInterface)((JTable)target).getDefaultRenderer(Object.class)).getCurrentData(target);
        chkEnable.setSelected(smartTable.isCuatomThemeEnabled());
        chkGrid.setSelected(smartTable.getShowHorizontalLines() || (smartTable.getShowVerticalLines()));
//        chkGrid.setSelected(((JTable)target).getShowHorizontalLines() || ((JTable)target).getShowVerticalLines());
//        ((JTable)smartTable).getDefaultRenderer(Object.class);
        btnSelectedBG.setBackground(target.getSelectedColumnBG()); // smartTable.getSelectionBackground());
        btnSelectedFG.setBackground(target.getSelectedColumnFG()); // smartTable.getSelectionForeground());

        btnGrig.setBackground(smartTable.getGridColor());

        btnBgDescColumn.setBackground(target.getDescColumnBG());
        btnFgDescColumn.setBackground(target.getDescColumnFG());
        btnBgVWAPColumn.setBackground(target.getVwapColumnBG());
        btnFgVWAPColumn.setBackground(target.getVwapColumnFG());
        btnBgPrevClosedColumn.setBackground(target.getPrevClosedColumnBG());
        btnFgPrevClosedColumn.setBackground(target.getPrevClosedColumnFG());

        btnBgLowTradeColumn.setBackground(target.getLowTradeColumnBG());
        btnFgLowTradeColumn.setBackground(target.getLowTradeColumnFG());
        btnBgHighTradeColumn.setBackground(target.getHighTradeColumnBG());
        btnFgHighTradeColumn.setBackground(target.getHighTradeColumnFG());
        btnBgTradesColumn.setBackground(target.getTradesColumnBG());
        btnFgTradesColumn.setBackground(target.getTradesColumnFG());
        btnBgTradeVolumeColumn.setBackground(target.getVolumeColumnBG());
        btnFgTradeVolumeColumn.setBackground(target.getVolumeColumnFG());
        btnBgLastTradeColumn.setBackground(target.getLastColumnBG());
        btnFgLastTradeColumn.setBackground(target.getLastColumnFG());
        btnBgLastTradeQtyColumn.setBackground(target.getLastTradeQtyColumnBG());
        btnFgLastTradeQtyColumn.setBackground(target.getLastTradeQtyColumnFG());

        btnBgHighAskColumn.setBackground(target.getHighAskColumnBG());
        btnFgHighAskColumn.setBackground(target.getHighAskColumnFG());
        btnBgLastAskColumn.setBackground(target.getLastAskColumnBG());
        btnFgLastAskColumn.setBackground(target.getLastAskColumnFG());
        btnBgLastAskQtyColumn.setBackground(target.getLastAskQtyColumnBG());
        btnFgLastAskQtyColumn.setBackground(target.getLastAskQtyColumnFG());
        btnBgLowAskColumn.setBackground(target.getLowAskColumnBG());
        btnFgLowAskColumn.setBackground(target.getLowAskColumnFG());
        btnBgLowAskQtyColumn.setBackground(target.getLowAskQtyColumnBG());
        btnFgLowAskQtyColumn.setBackground(target.getLowAskQtyColumnFG());
        btnBgTotAskQtyColumn.setBackground(target.getTotAskQtyColumnBG());
        btnFgTotAskQtyColumn.setBackground(target.getTotBidQtyColumnFG());

        btnBgHighBidColumn.setBackground(target.getHighBidColumnBG());
        btnFgHighBidColumn.setBackground(target.getHighBidColumnFG());
        btnBgLowBidColumn.setBackground(target.getLowBidColumnBG());
        btnFgLowBidColumn.setBackground(target.getLowBidColumnFG());
        btnBgHighBidQtyColumn.setBackground(target.getHighBidQtyColumnBG());
        btnFgHighBidQtyColumn.setBackground(target.getHighBidQtyColumnFG());
        btnBgLastBidColumn.setBackground(target.getLastBidColumnBG());
        btnFgLastBidColumn.setBackground(target.getLastBidColumnFG());
        btnBgTotBidQtyColumn.setBackground(target.getTotBidQtyColumnBG());
        btnFgTotBidQtyColumn.setBackground(target.getTotBidQtyColumnFG());
        btnBgLastBidQtyColumn.setBackground(target.getLastBidQtyColumnBG());
        btnFgLastBidQtyColumn.setBackground(target.getLastBidQtyColumnFG());

        bodyGap = target.getBodyGap();
        btnBodyGap.setSelectedItem(bodyGap);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("ENABLE")) {
            smartTable.setCustomThemeEnabled(chkEnable.isSelected());
            enableButtons(commonPanel, chkEnable.isSelected());
            enableButtons(tradePanel, chkEnable.isSelected());
            enableButtons(askPanel, chkEnable.isSelected());
            enableButtons(bidPanel, chkEnable.isSelected());
            if (chkEnable.isSelected()) {
                loadInitialSettings();
            }
            table.getModel().getViewSettings().putProperty(ViewConstants.CS_CUSTOMIZED, chkEnable.isSelected());
            if (chkEnable.isSelected()) {
                table.getModel().getViewSettings().putProperty(ViewConstants.CS_TABLE_SETTINGS, target.getWorkspaceString());
            } else {
                table.getModel().getViewSettings().removeProperty(ViewConstants.CS_TABLE_SETTINGS);
            }
        } else if (e.getActionCommand().equals("ENABLE_GRID")) {
//            System.out.println("Enable/Disable Grids");
            target.setGridOn(chkGrid.isSelected());
//            smartTable.setGdidOn(chkGrid.isSelected());
            smartTable.updateUI();
        } else if (e.getActionCommand().equals("CLOSE")) {
            table.getModel().getViewSettings().putProperty(ViewConstants.CS_TABLE_SETTINGS, target.getWorkspaceString());
            parent.setVisible(false);
        } else if (e.getActionCommand().equals("BODY_GAP")) {
            bodyGap = Integer.parseInt("" + btnBodyGap.getSelectedItem());
            target.setBodyGap(bodyGap);
            smartTable.setBodyGap(bodyGap);
            smartTable.updateUI();
        } else if (e.getActionCommand().startsWith("COLOR")) {
            source = (JButton) e.getSource();
            oldColor = source.getBackground();
            colorChooser.setColor(oldColor);
            lastCommand = e.getActionCommand();
            disableResetButton();
            colorDialog.show();
        } else if (e.getActionCommand().equals("FONT_BODY")) {
            oldFont = target.getBodyFont();
            newBodyFont = fontChooser.showDialog(Language.getString("FONT_CHOOSER"), target.getBodyFont());
            if (newBodyFont != oldFont) {
                target.setBodyFont(newBodyFont);
                smartTable.setBodyFont();
                table.getModel().getViewSettings().setFont(newBodyFont);
            }
        } else if (e.getActionCommand().equals("FONT_HEADING")) {
            oldFont = target.getHeadingFont();
            newHeadingFont = fontChooser.showDialog(Language.getString("FONT_CHOOSER"), target.getHeadingFont());
            if (newHeadingFont != oldFont) {
                target.setHeadingFont(newHeadingFont);
                smartTable.updateHeaderUI();
                table.getModel().getViewSettings().setHeaderFont(newHeadingFont);
            }
        } else if (e.getActionCommand().equalsIgnoreCase("OK")) {
            newColor = colorChooser.getColor();
            if (oldColor != newColor) {
                source.setBackground(newColor);
                if (lastCommand.equals("COLOR_BG_DESC")) {
                    target.setDescColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_DESC")) {
                    target.setDescColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_VWAP")) {
                    target.setVwapColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_VWAP")) {
                    target.setVwapColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_PREV_CLOSED")) {
                    target.setPrevClosedColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_PREV_CLOSED")) {
                    target.setPrevClosedColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LOW_TRADE")) {
                    target.setLowTradeColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LOW_TRADE")) {
                    target.setLowTradeColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_HIGH_TRADE")) {
                    target.setHighTradeColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_HIGH_TRADE")) {
                    target.setHighTradeColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_TRADES")) {
                    target.setTradesColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_TRADES")) {
                    target.setTradesColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_TRADE_VOLUME")) {
                    target.setVolumeColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_TRADE_VOLUME")) {
                    target.setVolumeColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LAST_TRADE")) {
                    target.setLastColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LAST_TRADE")) {
                    target.setLastColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_TRADE_QTY")) {
                    target.setLastTradeQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_TRADE_QTY")) {
                    target.setLastTradeQtyColumnFG(newColor);

                } else if (lastCommand.equals("COLOR_BG_LOW_ASK")) {
                    target.setLowAskColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LOW_ASK")) {
                    target.setLowAskColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_HIGH_ASK")) {
                    target.setHighAskColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_HIGH_ASK")) {
                    target.setHighAskColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LOW_ASK_QTY")) {
                    target.setLowAskQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LOW_ASK_QTY")) {
                    target.setLowAskQtyColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_TOT_ASK_QTY")) {
                    target.setTotAskQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_TOT_ASK_QTY")) {
                    target.setTotAskQtyColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LAST_ASK")) {
                    target.setLastAskColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LAST_ASK")) {
                    target.setLastAskColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LAST_ASK_QTY")) {
                    target.setLastAskQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LAST_ASK_QTY")) {
                    target.setLastAskQtyColumnFG(newColor);

                } else if (lastCommand.equals("COLOR_BG_LOW_BID")) {
                    target.setLowBidColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LOW_BID")) {
                    target.setLowBidColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_HIGH_BID")) {
                    target.setHighBidColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_HIGH_BID")) {
                    target.setHighBidColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_HIGH_BID_QTY")) {
                    target.setHighBidQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_HIGH_BID_QTY")) {
                    target.setHighBidQtyColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_TOT_BID_QTY")) {
                    target.setTotBidQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_TOT_BID_QTY")) {
                    target.setTotBidQtyColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LAST_BID")) {
                    target.setLastBidColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LAST_BID")) {
                    target.setLastBidColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_LAST_BID_QTY")) {
                    target.setLastBidQtyColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_FG_LAST_BID_QTY")) {
                    target.setLastBidQtyColumnFG(newColor);

                } else if (lastCommand.equals("COLOR_FG_SELECTED")) {
                    target.setSelectedColumnFG(newColor);
                } else if (lastCommand.equals("COLOR_BG_SELECTED")) {
                    //System.err.println("selected clicked");
                    target.setSelectedColumnBG(newColor);
                } else if (lastCommand.equals("COLOR_GRID")) {
                    target.setGridColor(newColor);
//				}else if (lastCommand.equals("FONT_BODY")){
//					target.setGridColor(newColor);
//				}else if (lastCommand.equals("FONT_HEADING")){
//					target.setGridColor(newColor);
                }
//                smartTable.getv
                smartTable.updateUI();
            }
        }
    }

    public Font getBodyFont() {
        return newBodyFont;
    }

    public Font getHeadingFont() {
        return newHeadingFont;
    }

    public int getBodyGap() {
        return bodyGap;
    }

    public void setBorders() {
        Component c;
        int ncomponents = colorPanel.getComponentCount();
        for (int i = 0; i < ncomponents; ++i) {
            c = colorPanel.getComponent(i);
            if (c instanceof JButton) {
                ((JButton) c).setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
            }
        }
        c = null;
    }

    public void enableButtons(Component c, boolean status) {
        if (c instanceof JButton) {
            c.setEnabled(status);
            if (status == false)
                c.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            return;
        }

        if (c instanceof JLabel) {
            c.setEnabled(status);
            return;
        }

        if (c instanceof JCheckBox) {
            c.setEnabled(status);
            return;
        } else if (c instanceof java.awt.Container) {
            java.awt.Container container = (java.awt.Container) c;
            int ncomponents = container.getComponentCount();
            for (int i = 0; i < ncomponents; ++i) {
                enableButtons(container.getComponent(i), status);
            }
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(colorChooser);
        SwingUtilities.updateComponentTreeUI(fontChooser);
        SwingUtilities.updateComponentTreeUI(colorDialog);
        setBorders();
    }

    private void disableResetButton() {

        Component[] comp = colorDialog.getContentPane().getComponents();

        for (int i = 0; i < comp.length; i++) {

            if (comp[i] instanceof JPanel) {

                JPanel btnpan = (JPanel) comp[i];
                Component[] btncomps = btnpan.getComponents();
                String resetString = UIManager.getString("ColorChooser.resetText");

                for (int j = 0; j < btncomps.length; j++) {

                    if (btncomps[j] instanceof JButton && ((JButton) btncomps[j]).getText().equals(resetString)) {
                        btncomps[j].setVisible(false);

                    }
                }
            }
        }
    }
}