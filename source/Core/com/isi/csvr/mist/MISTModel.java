package com.isi.csvr.mist;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.TWActions;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.*;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.ticker.custom.TickerDataInterface;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;

public class MISTModel extends CommonTable
        implements TableModel, CommonTableInterface, TickerDataInterface {

    // Variables used for Auto Scrolling
    private int currentHighlightedRow = 0;
    private int viewportBeginRowindex = 0;
    private int viewportEndRowindex = 0;
    private int visibleColumnindex = 0;
    private int noOfRowsInViewport = 0;
    private String currency;
    private byte decimalPlaces = 2;

    //Symbols symbols;
    //public MISTModel(){
    //    symbols = super.getViewSettings().getSymbols();
    //}
    /* --- Table Modal's metods start from here --- */
    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (super.getViewSettings().getSymbols().getFilteredSymbols().length == 0) {
            return 1;
        } else {
            return super.getViewSettings().getSymbols().getFilteredSymbols().length * 2 + 1;
        }
    }

    public void setSymbol(String symbol) {

    }

    public byte getDecimalPlaces() {
        return decimalPlaces;
    }

    public void setDecimalPlaces(byte decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    public void setCurrency(String currency) {
        try {
            if (currency != null && currency.equalsIgnoreCase("null")) {
                this.currency = null;
            } else {
                this.currency = currency;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object getValueAt(int row, int col) {
        if (row == getRowCount() - 1) {
            return "";
        }
        int symbolRow = (((row + 2) / 2)) - 1;
        int evenOddRow = (row + 1) % 2;
//        if (stock == null){
//            String symbol = SharedMethods.getSymbolFromKey(super.getViewSettings().getSymbols().getFilteredSymbols()[symbolRow]);
//            String exchange = SharedMethods.getExchangeFromKey(super.getViewSettings().getSymbols().getFilteredSymbols()[symbolRow]);
//            Exchange oExchange =  ExchangeStore.getSharedInstance().getExchange(exchange);
//            if (oExchange != null){
//                if ((!oExchange.isExpired() ) && (!oExchange.isInactive())){
//                    SendQFactory.addValidateRequest(symbol, "ClassicView" , exchange);
//                }
//            }
//        }
        //change start
        try {
            Stock stock = DataStore.getSharedInstance().getStockObject(super.getViewSettings().getSymbols().getFilteredSymbols()[symbolRow]);
            this.decimalPlaces = stock.getDecimalCount();
            switch (evenOddRow) {
                case 1:
                    switch (col) {
                        case -13:
                            return (StockData.getObjectData(stock, currency, -13));  // DISABLED SYMBOL
                        case -3:
                            return (StockData.getObjectData(stock, currency, -1));    // SYMBOL CODE
                        case -2:
                            return (StockData.getObjectData(stock, currency, -2));  // EXCHANGE
                        case -1:
                            return (StockData.getObjectData(stock, currency, -11));  // INS_TYPE
                        case 0:
                            return (StockData.getObjectData(stock, currency, 0));    // SYMBOL
                        case 1:
                            return (StockData.getObjectData(stock, currency, -3));    // LONG_DESC
                        case 2:
                            return (StockData.getObjectData(stock, currency, 18));   // LOW
                        case 3:
                            return (StockData.getObjectData(stock, currency, 17));    // HIGH
                        case 4:
                            return (StockData.getObjectData(stock, currency, 12));  // NO_OF_TRADES
                        case 5:
                            return (StockData.getObjectData(stock, currency, 22));  // BEST_ASK_PRICE
                        case 6:
                            return (StockData.getObjectData(stock, currency, 58));  // HIGH_ASK
                        case 7:
                            return (StockData.getObjectData(stock, currency, 23));   // BEST_ASK_QTY
                        case 8:
                            return (StockData.getObjectData(stock, currency, 59));   // LOW_BID
                        case 9:
                            return (StockData.getObjectData(stock, currency, 20));   // BEST_BID_PRICE
                        case 10:
                            return (StockData.getObjectData(stock, currency, 21));   // BEST_BID_QTY
                    }
                    break;
                case 0:
                    switch (col) {
                        case -13:
                            return (StockData.getObjectData(stock, currency, -13));  // DISABLED SYMBOL
                        case -3:
                            return (StockData.getObjectData(stock, currency, 0));    // SYMBOL
                        case -2:
                            return (StockData.getObjectData(stock, currency, -2));  // EXCHANGE
                        case -1:
                            return (StockData.getObjectData(stock, currency, -11));  // INS_TYPE
                        case 0:
                            return (StockData.getObjectData(stock, currency, 19));   // PREV_CLOSED
                        case 1:
                            return (StockData.getObjectData(stock, currency, 9));    //AVG_TRADE_PRICE
                        case 2:
                            return (StockData.getObjectData(stock, currency, 10));  // VOLUME
                        case 3:
                            return (StockData.getObjectData(stock, currency, 4));   // LAST_TRADE
                        case 4:
                            return (StockData.getObjectData(stock, currency, 5));   // TRADE_QTY
                        case 5:
                            return (StockData.getObjectData(stock, currency, 34));    // TOTAL_ASK_QTY
                        case 6:
                            return (StockData.getObjectData(stock, currency, 64));   // LAST_ASK_PRICE xxxx
                        case 7:
                            return (StockData.getObjectData(stock, currency, 65));   // LAST_ASK_QTY xxx
                        case 8:
                            return (StockData.getObjectData(stock, currency, 32));    // TOTAL_BID_QTY
                        case 9:
                            return (StockData.getObjectData(stock, currency, 66));   // LAST_BID_PRICE
                        case 10:
                            return (StockData.getObjectData(stock, currency, 67));   // LAST_BID_QTY
                    }
            }
        } catch (Exception e) {
            if (col == 0) {
                return SharedMethods.getSymbolFromKey(super.getViewSettings().getSymbols().getFilteredSymbols()[symbolRow]);
            } else {
                return "";
            }
        }
        return "";
    }

    public String getColumnName(int iCol) {
        //if (Language.isLTR())
        return super.getViewSettings().getColumnHeadings()[iCol];
        //else
        //    return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - iCol -1 ];
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        //return Object.class;
        switch (iCol) {
            case 0:
            case 1:
                return String.class;
            case 2:
            case 3:
            case 4:
            case 5:
            case 9:
                return Number[].class;
            default:
                return Number.class;
//                return Object.class;
        }
        /*
      switch(super.getViewSettings().getRendererID(iCol))
      {
          case 0:
          case 1:
          case 2:
          case 'B':
              return String.class;
          case 'P':
          case 'Q':
              return Number[].class;
          case 3:
          case 5:
          case 6:
          case 4:
          case 7:
          case 8:
          case 'M':
          case 'S':
              return Number.class;
          default:
              return Object.class;
      }  */
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 0) {
            try {
                if (((String) (getValueAt(row, col))).trim().equals("")) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception ex) {
                return false;
            }
        } else {
            return false;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        System.out.println("input to MIST set value symbol = " + value);
        String sNewSymbol = ((String) value).toUpperCase();
        Symbols symbols = super.getViewSettings().getSymbols();
        JTable table = getTable();
        //change start
        if (sNewSymbol.equals("")) return;
        String exchange;
        String symbol;
        String key;
        int instrumentType;
        exchange = SharedMethods.getExchangeFromKey(sNewSymbol); // break the exchange from key
        symbol = SharedMethods.getSymbolFromKey(sNewSymbol); // break the symbol from key (if not the full key, this becomes null)
        instrumentType = SharedMethods.getInstrumentTypeFromKey(sNewSymbol); // break the symbol from key (if not the full key, this becomes null)

        if ((ExchangeStore.isNoneDefaultExchangesAvailable() && (symbol == null))) {
            symbol = exchange;
            TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
            String requestID = "ClassicView:" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, super.getViewSettings().getSymbols());
        } else {
            exchange = SharedMethods.getExchangeFromKey(sNewSymbol); // break the exchange from key
            symbol = SharedMethods.getSymbolFromKey(sNewSymbol); // remove the symbol part from key
            instrumentType = SharedMethods.getInstrumentTypeFromKey(sNewSymbol); // remove the symbol part from key
//            System.out.println("Symbol = "+ symbol);
            if (symbol == null) {
                // may be the symbol only was typed. no exchange. hence symbol value is in exchange var
                symbol = exchange;
                key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the exchange for this symbol
                exchange = SharedMethods.getExchangeFromKey(key); // break the exchange from key
                symbol = SharedMethods.getSymbolFromKey(key);
                instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
//                System.out.println("Symbol after validation = "+ symbol);
//                System.out.println("Exchange after validation = "+ exchange);
            }

            if (exchange == null) { // bad symbol, not in local files. Must vslidate from the server
                TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");

            } else {
                sNewSymbol = SharedMethods.getKey(exchange, symbol, instrumentType);
//                System.out.println("sNewSymbol = "+ sNewSymbol);
                if (!symbols.appendSymbol(sNewSymbol.toUpperCase())) {
                    int location = symbols.getLocation(sNewSymbol);
                    if (location >= 0) {
                        table.setRowSelectionInterval(location * 2, location * 2);
                        table.setColumnSelectionInterval(0, table.getColumnCount() - 1);
                        GUISettings.createDragImage((String) table.getModel().getValueAt(table.getSelectedRow(), 0), table.getGraphics());
                    }
                }
                currentHighlightedRow = table.getSelectedRow();
                adjustViewPortToSymbol();
                TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                getTable().updateUI();
            }
        }
    }
    //super.setValueAt(value, rowIndex, olumn);
    /*if (column == 0){
        if (DataStore.isValidSymbol(((String)value).toUpperCase())){
            PortfolioRecord record = new PortfolioRecord();
            record.setSymbol(((String)value).toUpperCase());
            record.setHolding(0);
            record.setAvgCost(0);
            dataStore.addRecord(record);
            record = null;
            getTable().updateUI();
        }
    }else if (column == 4){
        try{
            if (!isEmptyRow(rowIndex)){
                PortfolioRecord record = dataStore.getRecord(getSymbol(rowIndex));
                if (record != null){
                    record.setHolding(toLongValue(value));
                }
                record = null;
                getTable().updateUI();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }else if (column == 5){
        try{
            if (!isEmptyRow(rowIndex)){
                PortfolioRecord record = dataStore.getRecord(getSymbol(rowIndex));
                if (record != null){
                    record.setAvgCost((float)toDoubleValue(value));
                }
                record = null;
                getTable().updateUI();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }*/


    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public String[] readTickerData() {
        return super.getViewSettings().getSymbols().getFilteredSymbols();
    }

    public void adjustViewPortToSymbol() {
        JTable table = getTable();
        if (visibleColumnindex == 0)
            getVisibleColumnIndex();

        Rectangle scrollRect = table.getVisibleRect(); // new Rectangle(xPos, yPos, vwPortWidth, vwPortHeight);
        Rectangle selCellRect = table.getCellRect(currentHighlightedRow, visibleColumnindex, true);

        //if (!SwingUtilities.isRectangleContainingRectangle(scrollRect, selCellRect)) {
//        if ((scrollRect.y > selCellRect.y) || ((scrollRect.y + scrollRect.height) < selCellRect.y)) {
        int row = table.rowAtPoint(new Point(scrollRect.x, scrollRect.y));
//        }
        viewportBeginRowindex = row;
        noOfRowsInViewport = (int) (this.getParentTable().getScrollPane().getViewport().getHeight() /
                table.getRowHeight());
        viewportEndRowindex = viewportBeginRowindex + noOfRowsInViewport;

        if ((scrollRect.y > selCellRect.y) || ((scrollRect.y + scrollRect.height) < selCellRect.y)) {
            if (currentHighlightedRow > viewportBeginRowindex) {
//System.out.println("--- aaa " + currentHighlightedRow + " " + viewportBeginRowindex);
                row = (currentHighlightedRow - viewportBeginRowindex) - 1; // 2
                doVerticalScrolling(row, true);
            } else if (currentHighlightedRow < viewportBeginRowindex) {
                row = (viewportBeginRowindex - currentHighlightedRow) + 1; // 2
                doVerticalScrolling(row, false);
            }
            viewportEndRowindex = currentHighlightedRow + noOfRowsInViewport - 1;
//        } else {
//            System.out.println("-- Exit 2");
        }
        scrollRect = null;
        selCellRect = null;

        table.setRowSelectionInterval(currentHighlightedRow, currentHighlightedRow + 1);
        table.setColumnSelectionInterval(0, table.getColumnCount() - 1);
    }

    private void doVerticalScrolling(int noOfRows, boolean isScrollDown) {
        if (this.getParentTable().getScrollPane() != null) {
//System.out.println("---- noOfRows = " + noOfRows + " isScrollDown = " + isScrollDown);
            JScrollBar hBar = this.getParentTable().getScrollPane().getHorizontalScrollBar();
            JScrollBar vBar = this.getParentTable().getScrollPane().getVerticalScrollBar();
            if (isScrollDown)
                vBar.setValue(vBar.getValue() + vBar.getUnitIncrement(2) * noOfRows);
            else
                vBar.setValue(vBar.getValue() - vBar.getUnitIncrement(-2) * noOfRows);
        }
    }

    private void getVisibleColumnIndex() {
        JTable table = getTable();
        for (int i = 0; i < table.getRowCount(); i++) {
            if ((table.getCellRect(0, i, true)).width > 0) {
                visibleColumnindex = i;
                break;
            }
        }
    }

    public void setHighLightedRow(int highlighedRow) {
        currentHighlightedRow = highlighedRow;
    }

}