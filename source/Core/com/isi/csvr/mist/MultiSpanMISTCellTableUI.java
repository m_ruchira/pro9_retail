package com.isi.csvr.mist;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * @version 1.0 11/26/98
 */

public class MultiSpanMISTCellTableUI extends BasicTableUI {

    //private boolean RTL = false;

    public MultiSpanMISTCellTableUI(boolean rtl) {
        super();
        //RTL = rtl;
    }

    public void paint(Graphics g, JComponent c) {
        Rectangle oldClipBounds = g.getClipBounds();
        Rectangle clipBounds = new Rectangle(oldClipBounds);
        int tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);

        int firstIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        int lastIndex = table.getRowCount() - 1;

        Rectangle rowRect = new Rectangle(0, 0,
                tableWidth, table.getRowHeight() + table.getRowMargin());
        rowRect.y = firstIndex * rowRect.height;

        for (int index = firstIndex; index <= lastIndex; index++) {
            if (rowRect.intersects(clipBounds)) {
                //System.out.println();                  // debug
//        System.out.print("@@@@@ " + index +": ");    // row
                paintRow(g, index);
            }
            rowRect.y += rowRect.height;
        }
        g.setClip(oldClipBounds);
    }

    private void paintRow(Graphics g, int row) {
        Rectangle rect = g.getClipBounds();
        boolean drawn = false;
        int numColumns = table.getColumnCount();

        for (int column = 0; column < numColumns; column++) {
            Rectangle cellRect = table.getCellRect(row, column, true);

            if ((row % 2) == 0) {
                if (((column == 0) || (column == 1))) {
                    //System.out.println("***********************");
                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
                }
            }
            if (cellRect.intersects(rect)) {
                drawn = true;
                paintCell(g, cellRect, row, column);
            } else {
                // Commented By Bandula on 01-11-2003 to avoid disappearing of the rest of the column if one column width is set to 0
//System.out.println("----------- " + row + ", " + column + " cdllRect.width " + cellRect.width);
//            if (drawn) break;
            }
        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        int spacingHeight = table.getRowMargin();
        int spacingWidth = table.getColumnModel().getColumnMargin();

        //Color c;// = g.getColor();
        //g.setColor(table.getGridColor());
        //g.drawRect(cellRect.x,cellRect.y,cellRect.width,cellRect.height);
        //g.drawRect(cellRect.x,cellRect.y,cellRect.width-1,cellRect.height-1);
        //g.setColor(c);

        cellRect.setBounds(cellRect.x + spacingWidth / 2,
                cellRect.y + spacingHeight / 2,
                cellRect.width - spacingWidth, cellRect.height - spacingHeight);

        if (table.isEditing() && table.getEditingRow() == row &&
                table.getEditingColumn() == column) {
            Component component = table.getEditorComponent();
            component.setBounds(cellRect);
            component.validate();
        } else {
            TableCellRenderer renderer = table.getCellRenderer(row, column);
            Component component = table.prepareRenderer(renderer, row, column);

            if (component.getParent() == null) {
                rendererPane.add(component);
            }
            rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y,
                    cellRect.width, cellRect.height, true);

            if (table.getShowHorizontalLines()) {
                g.setColor(table.getGridColor());
                g.drawLine(cellRect.x, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                if (table.getRowCount() == row + 1) {
                    g.drawLine(cellRect.x, cellRect.y + cellRect.height, cellRect.x + cellRect.width, cellRect.y + cellRect.height);
                }
            }
            if (table.getShowVerticalLines()) {
                g.setColor(table.getGridColor());
                g.drawLine(cellRect.x, cellRect.y, cellRect.x, cellRect.y + cellRect.height);
                if (table.getColumnCount() == column + 1) {
                    g.drawLine(cellRect.x + cellRect.width, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                }
            }
        }
    }

    private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }
}

