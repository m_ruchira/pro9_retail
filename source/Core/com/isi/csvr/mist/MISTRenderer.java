// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.mist;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class MISTRenderer implements TableCellRenderer {

    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();
    static Color g_oUpFGColor;
    static Color g_oDownFGColor;
    static Color g_oUpBGColor;
    static Color g_oDownBGColor;
    static Color g_oSelectedFG;
    static Color g_oSelectedBG;
    static Border selectedBorder;
    static Border horizBorder;
    static Border verticalBorder;
    static Border elShapedBorder;
    static Border border;
    private static Color gridColor;
    private static Color selectedColumnFG;
    private static Color selectedColumnBG;
    private static Color descColumnFG;
    private static Color descColumnBG;
    private static Color prevClosedColumnFG;
    private static Color prevClosedColumnBG;
//    static Color g_oFG1;
//	static Color g_oBG1;
//    static Color g_oFG2;
//	static Color g_oBG2;
//    static Color g_oTableBGColor;
//    static Color g_oDownColor;
//    static Color g_oUpColor;
    private static Color vwapColumnFG;
    private static Color vwapColumnBG;
    // Trade Details
    private static Color lowTradeColumnFG;
    private static Color lowTradeColumnBG;
    private static Color highTradeColumnFG;
    private static Color highTradeColumnBG;
    private static Color volumeColumnFG;
    private static Color volumeColumnBG;
    private static Color lastColumnFG;
    private static Color lastColumnBG;
    private static Color tradesColumnFG;
    private static Color tradesColumnBG;
    private static Color lastTradeQtyColumnFG;
    private static Color lastTradeQtyColumnBG;
    // Ask Details
    private static Color lowAskColumnFG;
    private static Color lowAskColumnBG;
    private static Color highAskColumnFG;
    private static Color highAskColumnBG;
    private static Color lowAskQtyColumnFG;
    private static Color lowAskQtyColumnBG;
    private static Color totAskQtyColumnFG;
    private static Color totAskQtyColumnBG;
    private static Color lastAskColumnFG;
    private static Color lastAskColumnBG;
    private static Color lastAskQtyColumnFG;
    private static Color lastAskQtyColumnBG;
    // Bid Details
    private static Color lowBidColumnFG;
    private static Color lowBidColumnBG;
    private static Color highBidColumnFG;
    private static Color highBidColumnBG;
    private static Color highBidQtyColumnFG;
    private static Color highBidQtyColumnBG;
    private static Color totBidQtyColumnFG;
    private static Color totBidQtyColumnBG;
    private static Color lastBidColumnFG;
    private static Color lastBidColumnBG;
    private static Color lastBidQtyColumnFG;
    private static Color lastBidQtyColumnBG;
    private static Color disabled_SymbolBG;
    String[] g_asColumns;
    int[] g_asRendIDs;
    boolean g_bLTR;
    int g_iStringAlign;
    int g_iNumberAlign;
    int g_iCenterAlign;
    int g_iID;
    TWDateFormat g_oDateFormat;// = new SimpleDateFormat ("yyyy/MM/dd");
    TWDateFormat g_oDateTimeFormat;// = new SimpleDateFormat ("HH:mm:ss");
    //String           g_sTimeFormat = "HH:mm:ss";
    //SimpleDateFormat g_oDateTimeFormat = new SimpleDateFormat ("yyyy/MM/dd hh:mm");
    String[] g_asSessions = new String[3];
    String g_sNA = "NA";
    String g_sBidMarket;
    String g_sAskMarket;
    TWDecimalFormat oCumReturnFormat;
    TWDecimalFormat oPriceFormat;
    TWDecimalFormat oQuantityFormat;
    TWDecimalFormat oChangeFormat;
    TWDecimalFormat oPChangeFormat;
    ImageIcon g_oUPImage = null;
    ImageIcon g_oDownImage = null;
    ImageIcon g_oBlankImage = null;
    ImageIcon g_oNoChangeImage = null;
    ImageIcon g_oNewsImage = null;
    ImageIcon g_oReadNewsImage = null;
    int g_iDescrCol;
    private MISTModel model;
    private byte decimalPlaces = -1;
    private long now;
    private boolean isSymbolEnable = true;

    public MISTRenderer(String[] asColumns, int[] asRendIDs) {
        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_bLTR = Language.isLTR();

//        if (Language.isLTR())
        g_iDescrCol = 1;
//        else
//            g_iDescrCol = g_asColumns.length -2;

        oCumReturnFormat = new TWDecimalFormat(Language.getString("FMT_CUMULATIVE_RETURN"));
        oPriceFormat = new TWDecimalFormat(Language.getString("FMT_PRICE"));
        oQuantityFormat = new TWDecimalFormat(Language.getString("FMT_QUANTITY"));
        oChangeFormat = new TWDecimalFormat(Language.getString("FMT_CHANGE"));
        oPChangeFormat = new TWDecimalFormat(Language.getString("FMT_PCT_CHANGE"));

        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT")); //yyyy/MM/dd"));
        g_oDateTimeFormat = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT")); //"HH:mm:ss");

        try {
            g_oUPImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            g_oUPImage = null;
        }
        try {
            g_oBlankImage = new ImageIcon("images/Common/blank.gif");
        } catch (Exception e) {
            g_oBlankImage = null;
        }
        try {
            g_oNoChangeImage = new ImageIcon("images/Common/nochange.gif");
        } catch (Exception e) {
            g_oNoChangeImage = null;
        }
        try {
            g_oDownImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            g_oDownImage = null;
        }
        try {
            g_oNewsImage = new ImageIcon("images/Common/news.gif");
        } catch (Exception e) {
            g_oNewsImage = null;
        }
        try {
            g_oReadNewsImage = new ImageIcon("images/Common/news_read.gif");
        } catch (Exception e) {
            g_oReadNewsImage = null;
        }

        try {
            g_sBidMarket = Language.getString("BID");    // BID_MARKET
        } catch (Exception e) {
            g_sBidMarket = " Bid ";
        }

        try {
            g_sAskMarket = Language.getString("ASK");    // ASK_MARKET
        } catch (Exception e) {
            g_sAskMarket = " Ask ";
        }

        reload();
        try {
            g_asSessions[0] = Language.getString("SESSION0");
            g_asSessions[1] = Language.getString("SESSION1");
            g_asSessions[2] = Language.getString("SESSION2");
            g_sNA = Language.getString("NA");
        } catch (Exception e) {
            g_asSessions[0] = "1";
            g_asSessions[1] = "2";
            g_asSessions[2] = "3";
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reload() {
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
//    		g_oFG1			= Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
//			g_oBG1			= Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
//    		g_oFG2			= Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
//			g_oBG2			= Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
//            g_oTableBGColor = Theme.getColor("BOARD_TABLE_BGCOLOR");
//            g_oDownColor	= g_oDownBGColor;
//            g_oUpColor 	    = g_oUpBGColor;
            selectedBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, g_oSelectedFG);
            horizBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, Color.black);

            if (Language.isLTR()) {
                verticalBorder = BorderFactory.createMatteBorder(0, 0, 0, 2, Color.black);
                elShapedBorder = BorderFactory.createMatteBorder(0, 0, 2, 2, Color.black);
            } else {
                verticalBorder = BorderFactory.createMatteBorder(0, 2, 0, 0, Color.black);
                elShapedBorder = BorderFactory.createMatteBorder(0, 2, 2, 0, Color.black);
            }
            gridColor = Theme.getColor("MIST_VIEW_GRID_COLOR");
            selectedColumnBG = Theme.getColor("MIST_VIEW_SELECTED_BG_COLOR");
            selectedColumnFG = Theme.getColor("MIST_VIEW_SELECTED_FG_COLOR");

            descColumnFG = Theme.getColor("MIST_VIEW_DESC_COL_FG_COLOR");
            descColumnBG = Theme.getColor("MIST_VIEW_DESC_COL_BG_COLOR");
            prevClosedColumnFG = Theme.getColor("MIST_VIEW_PREV_CLOSED_COL_FG_COLOR");
            prevClosedColumnBG = Theme.getColor("MIST_VIEW_PREV_CLOSED_COL_BG_COLOR");
            vwapColumnFG = Theme.getColor("MIST_VIEW_VWAP_COL_FG_COLOR");
            vwapColumnBG = Theme.getColor("MIST_VIEW_VWAP_COL_BG_COLOR");
            // Trade Details
            lowTradeColumnFG = Theme.getColor("MIST_VIEW_TRADE_LOW_COL_FG_COLOR");
            lowTradeColumnBG = Theme.getColor("MIST_VIEW_TRADE_LOW_COL_BG_COLOR");
            highTradeColumnFG = Theme.getColor("MIST_VIEW_TRADE_HIGH_COL_FG_COLOR");
            highTradeColumnBG = Theme.getColor("MIST_VIEW_TRADE_HIGH_COL_BG_COLOR");
            volumeColumnFG = Theme.getColor("MIST_VIEW_TRADE_VOLUME_COL_FG_COLOR");
            volumeColumnBG = Theme.getColor("MIST_VIEW_TRADE_VOLUME_COL_BG_COLOR");
            lastColumnFG = Theme.getColor("MIST_VIEW_TRADE_LAST_COL_FG_COLOR");
            lastColumnBG = Theme.getColor("MIST_VIEW_TRADE_LAST_COL_BG_COLOR");
            tradesColumnFG = Theme.getColor("MIST_VIEW_TRADE_NO_OF_TRADES_COL_FG_COLOR");
            tradesColumnBG = Theme.getColor("MIST_VIEW_TRADE_NO_OF_TRADES_COL_BG_COLOR");
            lastTradeQtyColumnFG = Theme.getColor("MIST_VIEW_TRADE_LAST_QTY_COL_FG_COLOR");
            lastTradeQtyColumnBG = Theme.getColor("MIST_VIEW_TRADE_LAST_QTY_COL_BG_COLOR");
            // Ask Details
            lowAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_LOW_COL_FG_COLOR");
            lowAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_LOW_COL_BG_COLOR");
            highAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_HIGH_COL_FG_COLOR");
            highAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_HIGH_COL_BG_COLOR");
            lowAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_LOW_QTY_COL_FG_COLOR");
            lowAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_LOW_QTY_COL_BG_COLOR");
            totAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_TOT_QTY_COL_FG_COLOR");
            totAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_TOT_QTY_COL_BG_COLOR");
            lastAskColumnFG = Theme.getColor("MIST_VIEW_OFFER_LAST_COL_FG_COLOR");
            lastAskColumnBG = Theme.getColor("MIST_VIEW_OFFER_LAST_COL_BG_COLOR");
            lastAskQtyColumnFG = Theme.getColor("MIST_VIEW_OFFER_LAST_QTY_COL_FG_COLOR");
            lastAskQtyColumnBG = Theme.getColor("MIST_VIEW_OFFER_LAST_QTY_COL_BG_COLOR");
            // Bid Details
            lowBidColumnFG = Theme.getColor("MIST_VIEW_BID_LOW_COL_FG_COLOR");
            lowBidColumnBG = Theme.getColor("MIST_VIEW_BID_LOW_COL_BG_COLOR");
            highBidColumnFG = Theme.getColor("MIST_VIEW_BID_HIGH_COL_FG_COLOR");
            highBidColumnBG = Theme.getColor("MIST_VIEW_BID_HIGH_COL_BG_COLOR");
            highBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_HIGH_QTY_COL_FG_COLOR");
            highBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_HIGH_QTY_COL_BG_COLOR");
            totBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_TOT_QTY_COL_FG_COLOR");
            totBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_TOT_QTY_COL_BG_COLOR");
            lastBidColumnFG = Theme.getColor("MIST_VIEW_BID_LAST_COL_FG_COLOR");
            lastBidColumnBG = Theme.getColor("MIST_VIEW_BID_LAST_COL_BG_COLOR");
            lastBidQtyColumnFG = Theme.getColor("MIST_VIEW_BID_LAST_QTY_COL_FG_COLOR");
            lastBidQtyColumnBG = Theme.getColor("MIST_VIEW_BID_LAST_QTY_COL_BG_COLOR");

            disabled_SymbolBG = Theme.getColor("DISABLED_SYMBOL_BG_COLOR");

        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
//            g_oDownColor	= Color.red;
//            g_oUpColor 	    = Color.green;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
//    		g_oFG1			= Color.white;
//			g_oBG1			= Color.black;
//    		g_oFG2			= Color.white;
//			g_oBG2			= Color.black;
            gridColor = Color.black;
            selectedColumnBG = Color.white;
            selectedColumnFG = Color.black;

            descColumnFG = Color.black;
            descColumnBG = Color.white;
            prevClosedColumnFG = Color.black;
            prevClosedColumnBG = new Color(0xc6c6c6);
            vwapColumnFG = Color.black;
            vwapColumnBG = new Color(0xFFFF00);
            // Trade Details
            lowTradeColumnFG = Color.black;
            lowTradeColumnBG = new Color(0xc6c6c6);
            highTradeColumnFG = Color.black;
            highTradeColumnBG = Color.white;
            volumeColumnFG = Color.black;
            volumeColumnBG = new Color(0x00FF00);
            lastColumnFG = Color.black;
            lastColumnBG = new Color(0xFFFF00);
            tradesColumnFG = Color.black;
            tradesColumnBG = new Color(0xFF9CCE);
            lastTradeQtyColumnFG = new Color(0xFFFF00);
            lastTradeQtyColumnBG = new Color(0x0000FF);
            // Ask Details
            lowAskColumnFG = Color.black;
            lowAskColumnBG = new Color(0xc6c6c6);
            highAskColumnFG = Color.black;
            highAskColumnBG = Color.white;
            lowAskQtyColumnFG = Color.black;
            lowAskQtyColumnBG = new Color(0xFF9CCE);
            totAskQtyColumnFG = Color.black;
            totAskQtyColumnBG = new Color(0x00FF00);
            lastAskColumnFG = Color.black;
            lastAskColumnBG = new Color(0xFFFF00);
            lastAskQtyColumnFG = new Color(0xFFFF00);
            lastAskQtyColumnBG = new Color(0x0000FF);
            // Bid Details
            lowBidColumnFG = Color.black;
            lowBidColumnBG = new Color(0xc6c6c6);
            highBidColumnFG = Color.black;
            highBidColumnBG = Color.white;
            highBidQtyColumnFG = Color.black;
            highBidQtyColumnBG = new Color(0xFF9CCE);
            totBidQtyColumnFG = Color.black;
            totBidQtyColumnBG = new Color(0x00FF00);
            lastBidColumnFG = Color.black;
            lastBidColumnBG = new Color(0xFFFF00);
            lastBidQtyColumnFG = new Color(0xFFFF00);
            lastBidQtyColumnBG = new Color(0x0000FF);
        }
    }

    public static void reloadForPrinting() {
//        g_oUpFGColor  	= Color.black;
//        g_oDownFGColor	= Color.black;
//        g_oUpBGColor  	= Color.white;
//        g_oDownBGColor	= Color.white;
//        g_oSelectedFG	= Color.black;
//        g_oSelectedBG	= Color.white;
//        g_oFG1			= Color.black;
//        g_oBG1			= Color.white;
//        g_oFG2			= Color.black;
//        g_oBG2			= Color.white;
//        g_oTableBGColor = Color.white;
//        g_oDownColor	= Color.black;
//        g_oUpColor 	    = Color.black;
    }

    public void setModel(MISTModel model) {
        this.model = model;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        //table.setBackground(g_oTableBGColor);
        JLabel lblRenderer = (JLabel) renderer;
        lblRenderer.setOpaque(true);

        Color foreground, background;

        float[] adPrice;
        long quantity;
        double dPrice;
        long updateDirection;
        boolean isCuatomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        MISTTableSettings sett = null;
        if (isCuatomThemeEnabled)
            sett = (MISTTableSettings) ((SmartTable) table).getTableSettings();

        if (decimalPlaces != (model.getDecimalPlaces())) {
            applyDecimalPlaces(model.getDecimalPlaces());
        }

        border = null;

        try {
            isSymbolEnable = (Boolean) table.getModel().getValueAt(row, -13);
        } catch (Exception e) {
            isSymbolEnable = true;
        }

        if (isSymbolEnable) {
            if (row != table.getRowCount() - 1) {
                if ((row + 1) % 2 == 1) { // row 1
                    if ((column == 0) || (column == 1) ||
                            (column == 4) || (column == 7)) {
                        border = verticalBorder;
                    }
//                if (column == 1) {
//                    Stock oStock = null;
//                    try {
//                        oStock = DataStore.getStockObject((String)table.getModel().getValueAt(row, 0));
//                    } catch (Exception ex) {
//                        oStock = null;
//                    }
//                    if (oStock != null) {
//                        lblRenderer.setIconTextGap(1);
//                       if (oStock.getNewsAvailability() == ESPConstants.NEWS_NOT_READ)
//                           lblRenderer.setIcon(g_oUPImage); // new ImageIcon("images/common/news.gif"));
//                        else if (oStock.getNewsAvailability() == ESPConstants.NEWS_READ)
//                           lblRenderer.setIcon(g_oReadNewsImage); // new ImageIcon("images/common/news_read.gif"));
//                   } else
//                        System.out.println("--- Stock is NULL " + row + " " + column);
//                }
                    if (isSelected) {//isSelected
                        if (isCuatomThemeEnabled) {
//                        if (renderer.getGraphics() != null)
//                            renderer.getGraphics().setXORMode(sett.getSelectedColumnBG());
                            renderer.setForeground(sett.getSelectedColumnFG());
                            renderer.setBackground(sett.getSelectedColumnBG());
                        } else {
                            renderer.setForeground(selectedColumnFG);
                            renderer.setBackground(selectedColumnBG);
                        }
                    } else {
//                    if (renderer.getGraphics() != null){
//                        if (isSelected){
//                            renderer.getGraphics().setXORMode(sett.getSelectedColumnBG());
//                        }else{
//                            renderer.getGraphics().setPaintMode();
//                        }
//                    }
                        switch (column) {
                            case 0:
//                            if (isCuatomThemeEnabled) {
//                                renderer.setForeground(sett.getDescColumnFG()); // Color.black);
//                                renderer.setBackground(sett.getDescColumnBG()); // Color.white);
//                            } else {
//                                renderer.setForeground(Color.black);
//                                renderer.setBackground(Color.white);
//                            }
//                            break;
                            case 1:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getDescColumnFG()); // Color.black);
                                    renderer.setBackground(sett.getDescColumnBG()); // Color.white);
                                } else {
                                    renderer.setForeground(descColumnFG);
                                    renderer.setBackground(descColumnBG);
                                }
                                break;
                            case 2:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLowTradeColumnFG());
                                    renderer.setBackground(sett.getLowTradeColumnBG());
                                } else {
                                    renderer.setForeground(lowTradeColumnFG);
                                    renderer.setBackground(lowTradeColumnBG);
                                }
                                break;
                            case 3:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getHighTradeColumnFG());
                                    renderer.setBackground(sett.getHighTradeColumnBG());
                                } else {
                                    renderer.setForeground(highTradeColumnFG);
                                    renderer.setBackground(highTradeColumnBG);
                                }
                                break;
                            case 4:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getTradesColumnFG());
                                    renderer.setBackground(sett.getTradesColumnBG());
                                } else {
                                    renderer.setForeground(tradesColumnFG);
                                    renderer.setBackground(tradesColumnBG);
                                }
                                break;
                            case 5:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLowAskColumnFG());
                                    renderer.setBackground(sett.getLowAskColumnBG());
                                } else {
                                    renderer.setForeground(lowAskColumnFG);
                                    renderer.setBackground(lowAskColumnBG);
                                }
                                break;
                            case 6:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getHighAskColumnFG());
                                    renderer.setBackground(sett.getHighAskColumnBG());
                                } else {
                                    renderer.setForeground(highAskColumnFG);
                                    renderer.setBackground(highAskColumnBG);
                                }
                                break;
                            case 7:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLowAskQtyColumnFG());
                                    renderer.setBackground(sett.getLowAskQtyColumnBG());
                                } else {
                                    renderer.setForeground(lowAskQtyColumnFG);
                                    renderer.setBackground(lowAskQtyColumnBG);
                                }
                                break;
                            case 8:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLowBidColumnFG());
                                    renderer.setBackground(sett.getLowBidColumnBG());
                                } else {
                                    renderer.setForeground(lowBidColumnFG);
                                    renderer.setBackground(lowBidColumnBG);
                                }
                                break;
                            case 9:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getHighBidColumnFG());
                                    renderer.setBackground(sett.getHighBidColumnBG());
                                } else {
                                    renderer.setForeground(highBidColumnFG);
                                    renderer.setBackground(highBidColumnBG);
                                }
                                break;
                            case 10:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getHighBidQtyColumnFG());
                                    renderer.setBackground(sett.getHighBidQtyColumnBG());
                                } else {
                                    renderer.setForeground(highBidQtyColumnFG);
                                    renderer.setBackground(highBidQtyColumnBG);
                                }
                                break;
                        }
                    }
                } else { // row 2
                    if ((column == 1) ||
                            (column == 4) || (column == 7)) {
                        border = elShapedBorder;
                    } else {
                        border = horizBorder;//unselectedBorder;
                    }
                    if (isSelected) {
                        if (isCuatomThemeEnabled) {
                            renderer.setForeground(sett.getSelectedColumnFG());
                            renderer.setBackground(sett.getSelectedColumnBG());
                        } else {
                            renderer.setForeground(selectedColumnFG);
                            renderer.setBackground(selectedColumnBG);
                        }
                    } else {
                        switch (column) {
                            case 0:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getPrevClosedColumnFG());
                                    renderer.setBackground(sett.getPrevClosedColumnBG());
                                } else {
                                    renderer.setForeground(prevClosedColumnFG);
                                    renderer.setBackground(prevClosedColumnBG);
                                }
                                break;
                            case 1:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getVwapColumnFG());
                                    renderer.setBackground(sett.getVwapColumnBG());
                                } else {
                                    renderer.setForeground(vwapColumnFG);
                                    renderer.setBackground(vwapColumnBG);
                                }
                                break;
                            case 2:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getVolumeColumnFG());
                                    renderer.setBackground(sett.getVolumeColumnBG());
                                } else {
                                    renderer.setForeground(volumeColumnFG);
                                    renderer.setBackground(volumeColumnBG);
                                }
                                break;
                            case 3:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastColumnFG());
                                    renderer.setBackground(sett.getLastColumnBG());
                                } else {
                                    renderer.setForeground(lastColumnFG);
                                    renderer.setBackground(lastColumnBG);
                                }
                                break;
                            case 4:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastTradeQtyColumnFG());
                                    renderer.setBackground(sett.getLastTradeQtyColumnBG());
                                } else {
                                    renderer.setForeground(lastTradeQtyColumnFG);
                                    renderer.setBackground(lastTradeQtyColumnBG);
                                }
                                break;
                            case 5:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getTotAskQtyColumnFG());
                                    renderer.setBackground(sett.getTotAskQtyColumnBG());
                                } else {
                                    renderer.setForeground(totAskQtyColumnFG);
                                    renderer.setBackground(totAskQtyColumnBG);
                                }
                                break;
                            case 6:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastAskColumnFG());
                                    renderer.setBackground(sett.getLastAskColumnBG());
                                } else {
                                    renderer.setForeground(lastAskColumnFG);
                                    renderer.setBackground(lastAskColumnBG);
                                }
                                break;
                            case 7:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastAskQtyColumnFG());
                                    renderer.setBackground(sett.getLastAskQtyColumnBG());
                                } else {
                                    renderer.setForeground(lastAskQtyColumnFG);
                                    renderer.setBackground(lastAskQtyColumnBG);
                                }
                                break;
                            case 8:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getTotBidQtyColumnFG());
                                    renderer.setBackground(sett.getTotBidQtyColumnBG());
                                } else {
                                    renderer.setForeground(totBidQtyColumnFG);
                                    renderer.setBackground(totBidQtyColumnBG);
                                }
                                break;
                            case 9:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastBidColumnFG());
                                    renderer.setBackground(sett.getLastBidColumnBG());
                                } else {
                                    renderer.setForeground(lastBidColumnFG);
                                    renderer.setBackground(lastBidColumnBG);
                                }
                                break;
                            case 10:
                                if (isCuatomThemeEnabled) {
                                    renderer.setForeground(sett.getLastBidQtyColumnFG());
                                    renderer.setBackground(sett.getLastBidQtyColumnBG());
                                } else {
                                    renderer.setForeground(lastBidQtyColumnFG);
                                    renderer.setBackground(lastBidQtyColumnBG);
                                }
                                break;
                        }
                    }
                }
            } else {
                if (isSelected) {
                    renderer.setForeground(selectedColumnFG);
                    renderer.setBackground(selectedColumnBG);
                } else {
                    renderer.setForeground(g_oSelectedFG);
                    renderer.setBackground(g_oSelectedBG);
                }
            }
        } else {
            renderer.setForeground(g_oSelectedFG);
            lblRenderer.setBackground(disabled_SymbolBG);

        }
/*
        String sArray = (String) table.getModel().getValueAt(row, -20);
        if (OptimizedTreeRenderer.selectedSymbols.containsKey(sArray)) {
            if (OptimizedTreeRenderer.selectedSymbols.get(sArray).equals(true)) {
//                lblRenderer.setBackground(background);
            } else {
                lblRenderer.setBackground(disabled_SymbolBG);
            }
        } else {
            lblRenderer.setBackground(disabled_SymbolBG);
//            MainUI.PRESS_OK = false;
        }*/

//        String  sColName = table.getIndexSymbol(column);
        int iRendID = 0;
        if (((row + 1) % 2) == 1) {
            iRendID = g_asRendIDs[column];
        } else {
            iRendID = g_asRendIDs[column + 11];
        }
        //System.out.println(">> " + row + " " + column + " : " + (char)iRendID + " : " + iRendID);

        /* Set the tootip for the row *
        try
        {
            //Stock oStock = DataStore.getStockObject((String)table.getModel().getValueAt(row,col));
            /*lblRenderer.setToolTipText(UnicodeToNative.getNativeString("<html><meta http-equiv='Content-Type' content='text/html; charset=utf-8'>" + oStock.getLongDescription() + "<br>" +
                g_asColumns[5] + " : " + oStock.getLastTrade()[0] + "<br>" +
                (String)table.getModel().getValueAt(row,col)
                ));*
            lblRenderer.setToolTipText((String)table.getModel().getValueAt(row,g_iDescrCol));
        }
        catch(Exception e)
        {
            lblRenderer.setToolTipText(null);
        }*/
        try {
            lblRenderer.setIcon(null);
            lblRenderer.setHorizontalTextPosition(SwingConstants.LEFT);
            //if (((row+1)%2)==0){
            lblRenderer.setBorder(border);
            // }

            if (isSymbolEnable) {
                switch (iRendID) {
                    case 0: // DEFAULT
                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        break;
                    case 1: // SYMBOL
                    case 2: // DESCRIPTION
                        Stock oStock = null;
                        try {
                            oStock = DataStore.getSharedInstance().getStockObject((String) (((MISTModel) model).getValueAt(row, 0)));
                            //                        oStock = DataStore.getStockObject("MIBA"); //(String)(model.getValueAt(row, 0)));
                        } catch (Exception ex) {
                            oStock = null;
                        }
                        if (oStock != null) {
                            //                        lblRenderer.setOpaque(true);
                            //                        lblRenderer.setBackground(Color.cyan);
                            lblRenderer.setIconTextGap(1);
//                        if (oStock.getNewsAvailability() == ESPConstants.NEWS_NOT_READ)
//                           lblRenderer.setIcon(g_oNewsImage); // new ImageIcon("images/common/news.gif"));
//                        else if (oStock.getNewsAvailability() == ESPConstants.NEWS_READ)
//                           lblRenderer.setIcon(g_oReadNewsImage); // new ImageIcon("images/common/news_read.gif"));
                            if (oStock.getBulbStatus() == Constants.ITEM_NOT_READ)
                                lblRenderer.setIcon(new ImageIcon("images/common/news.gif"));
                            else if (oStock.getBulbStatus() == Constants.ITEM_READ)
                                lblRenderer.setIcon(new ImageIcon("images/common/news_read.gif"));
                        }
                        oStock = null;
                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 3: // DECIMAL
                        dPrice = toDoubleValue(value);
                        lblRenderer.setText(oPriceFormat.format(dPrice));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'N': // DESCRIPTION with NEWS STATUS
//                    Stock oStock = null;
//                    try {
//                        oStock = DataStore.getStockObject((String)(((MISTModel)model).getValueAt(row, 0)));
//    //                        oStock = DataStore.getStockObject("MIBA"); //(String)(model.getValueAt(row, 0)));
//                    } catch (Exception ex) {
//                        oStock = null;
//                    }
//                    if (oStock != null) {
//    //                        lblRenderer.setOpaque(true);
//    //                        lblRenderer.setBackground(Color.cyan);
//                        lblRenderer.setIconTextGap(1);
//                        if (oStock.getNewsAvailability() == ESPConstants.NEWS_NOT_READ)
//                           lblRenderer.setIcon(g_oUPImage); // new ImageIcon("images/common/news.gif"));
//                        else if (oStock.getNewsAvailability() == ESPConstants.NEWS_READ)
//                           lblRenderer.setIcon(g_oReadNewsImage); // new ImageIcon("images/common/news_read.gif"));
//                   } else {
//System.out.println("-----------------------------------------------");
//                       System.out.println("--- Stock is NULL2 " + row + " " + column + " -" +
//                            (String)(model.getValueAt(row, 0)) + "-" +
//                            (String)(model.getValueAt(row, 1)) + "-");
//                   }
//                    oStock = null;

                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 'X': // DECIMAL
                        dPrice = toDoubleValue(value);
                        lblRenderer.setText(oPriceFormat.format(dPrice));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        lblRenderer.setIcon(g_oBlankImage);
                        break;
                    case 'W': // VWAP
                        dPrice = toDoubleValue(value);
                        lblRenderer.setIconTextGap(1);
                        lblRenderer.setText(oPriceFormat.format(dPrice));
                        //lblRenderer.seti
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if (((row + 1) % 2) == 0) {
                            if ((dPrice - toDoubleValue(table.getModel().getValueAt(row, 0))) > 0) {
                                lblRenderer.setIcon(g_oUPImage);
                            } else if ((dPrice - toDoubleValue(table.getModel().getValueAt(row, 0))) < 0) {
                                lblRenderer.setIcon(g_oDownImage);
                            } else {
                                lblRenderer.setIcon(g_oNoChangeImage);
                            }
                        }
                        break;
                    case 'P': // PRICE
                        dPrice = ((DoubleTransferObject) value).getValue();
                        lblRenderer.setText(oPriceFormat.format(dPrice));
                        updateDirection = ((DoubleTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            //lblRenderer.setHorizontalAlignment(JLabel.RIGHT);
                            //System.out.println(" inside" );
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(g_oUpBGColor);
                                lblRenderer.setForeground(g_oUpFGColor);
                                //lblRenderer.setBorder(new LineBorder(g_oUpColor, 2, true));
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(g_oDownFGColor);
                                lblRenderer.setBackground(g_oDownBGColor);
                                //lblRenderer.setBorder(new LineBorder(g_oDownColor, 2, true));
                            } //else
                        }
                        //lblRenderer.setBorder(null);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'Q': // Quantity with coloured bg
                        quantity = ((LongTransferObject) value).getValue();
                        lblRenderer.setText(oQuantityFormat.format(quantity));
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        //lblRenderer.setHorizontalAlignment(JLabel.RIGHT);
                        //System.out.println(" inside" );
                        if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(g_oUpBGColor);
                                lblRenderer.setForeground(g_oUpFGColor);
                                //lblRenderer.setBorder(new LineBorder(g_oUpColor, 2, true));
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(g_oDownFGColor);
                                lblRenderer.setBackground(g_oDownBGColor);
                                //lblRenderer.setBorder(new LineBorder(g_oDownColor, 2, true));
                            }//} else
                        }
                        //    lblRenderer.setBorder(null);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 4: // QUANTITY
                        lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                        //lblRenderer.setHorizontalAlignment(JLabel.RIGHT);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    /*case 5: // CHANGE
                    dPrice = toDoubleValue(value);
                    lblRenderer.setText(oChangeFormat.format(dPrice));
                    if (dPrice > 0)
                    {
                        lblRenderer.setForeground(g_oUpColor);
                    }
                    else if (dPrice < 0)
                    {
                        lblRenderer.setForeground(g_oDownColor);
                    }
                    else
                        lblRenderer.setForeground(foreground);
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 6: // % CHANGE
                    dPrice = toDoubleValue(value);
                    lblRenderer.setText(oPChangeFormat.format(dPrice));
                    if (dPrice < 0)
                    {
                        lblRenderer.setForeground(g_oDownColor);
                    }
                    else if (dPrice > 0)
                    {
                        lblRenderer.setForeground(g_oUpColor);
                    }
                    else
                        lblRenderer.setForeground(foreground);
                    //lblRenderer.setHorizontalAlignment(JLabel.RIGHT);
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 7: // DATE
                    long[] lDate = (long[])value;
                    if (lDate[0] == 0)
                        lblRenderer.setText(g_sNA);
                    else
                    {
                        Date dDate = new Date(lDate[0]);
                        lblRenderer.setText(g_oDateFormat.format(dDate));
                        dDate = null;
                    }
                    if (lDate[0] > lDate[1]) {
                        lblRenderer.setBackground(g_oUpBGColor);
                        lblRenderer.setForeground(g_oUpFGColor);
                    } else if (lDate[0] < lDate[1]) {
                        lblRenderer.setForeground(g_oDownFGColor);
                        lblRenderer.setBackground(g_oDownBGColor);
                    }
                    lDate[1] = lDate[0];
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 8: // TIME
                    long[] lDateTime = (long[])value;
                    if (lDateTime[0] == 0)
                        lblRenderer.setText(g_sNA);
                    else
                    {
                        Date dDateTime = new Date(lDateTime[0]);
                        lblRenderer.setText(g_oDateTimeFormat.format(dDateTime));
                    }
                    if (lDateTime[0] > lDateTime[1]) {
                        lblRenderer.setBackground(g_oUpBGColor);
                        lblRenderer.setForeground(g_oUpFGColor);
                    } else if (lDateTime[0] < lDateTime[1]) {
                        lblRenderer.setForeground(g_oDownFGColor);
                        lblRenderer.setBackground(g_oDownBGColor);
                    }
                    lDateTime[1] = lDateTime[0];
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 9: // TICK
                    int iValue = (int)toLongValue(value);
                    switch(iValue)
                    {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(g_oUPImage);
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(g_oDownImage);
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;*/
                    case 'C': // CUM_RETURNOF_52_WK
                        dPrice = ((DoubleTransferObject) value).getValue();
                        lblRenderer.setText(oCumReturnFormat.format(dPrice));
                        //lblRenderer.setForeground(Color.orange);
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    case 'S': // Trade Session
                        g_iID = toIntValue(value);
                        lblRenderer.setText(g_asSessions[g_iID]);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 'B': // Broker ID
                        if (value == null)
                            lblRenderer.setText(g_sNA);
                        else
                            lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;
                    case 'M': // Bid / Ask Market
                        dPrice = toDoubleValue(value);
                        if (dPrice == -1) {
                            lblRenderer.setText(g_sBidMarket);
                            lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        } else if (dPrice == -2) {
                            lblRenderer.setText(g_sAskMarket);
                            lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        } else {
                            lblRenderer.setText(oPriceFormat.format(dPrice));
                            lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        }
                        break;
                    default:
                        lblRenderer.setText("");
                }
            } else {
                switch (iRendID) {
                    case 0: // DEFAULT
                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        break;
                    case 1: // SYMBOL
                    case 2: // DEFAULT
                        lblRenderer.setText((String) value);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                        break;

                    default:
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);

                        lblRenderer.setText("�");
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            lblRenderer.setText("");
        }

        return renderer;
    }

    private void applyDecimalPlaces(byte decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oCumReturnFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                oPChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
        }
        this.decimalPlaces = decimalPlaces;
    }

    private long toLongValue(Object oValue) throws Exception {
        return ((LongTransferObject) (oValue)).getValue();
//    	return Long.parseLong((String)oValue);
    }

//    private float toFloatValue(Object oValue) throws Exception
//    {
//        return ((DoubleTransferObject) (oValue)).getValue();
////    	return Float.parseFloat((String)oValue);
//    }

    private int toIntValue(Object oValue) throws Exception {
        return ((IntTransferObject) (oValue)).getValue();
//    	return Integer.parseInt((String)oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return ((DoubleTransferObject) (oValue)).getValue();
//    	return Double.parseDouble((String)oValue);
    }
}
