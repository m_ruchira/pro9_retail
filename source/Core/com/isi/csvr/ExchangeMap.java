package com.isi.csvr;

import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 13, 2005
 * Time: 1:49:52 PM
 */
public class ExchangeMap {
    private static HashMap<String, String> idmap;
    private static HashMap<String, String> symbolmap;

    public static void initialize() {
        loadData();
    }

    public static String getSymbolFor(String id) {
        return idmap.get(id);
    }

    /*public static String getSymbolFor(char id) {
        return idmap.get("" + id);
    }*/

    public static String getIDFor(String symbol) {
        return symbolmap.get(symbol);//.charAt(0);
    }

    public static void setMapData(String id, String symbol) {
        idmap.put(id, symbol);
        symbolmap.put(symbol, id);

    }

    public static boolean isValidExchange(String id) {
        return idmap.containsKey(id);
    }

    /**
     * Save the hash table as an object to the disk
     */
    public static void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/ExchangeMap.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(idmap);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Loads the saved hash table from the disk as an object
     */
    public static void loadData() {
        symbolmap = new HashMap<String, String>();
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/ExchangeMap.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            idmap = (HashMap<String, String>) oObjIn.readObject();
            oIn.close();
            Collection<String> keys = idmap.keySet();
            for (String key : keys) {
                symbolmap.put(idmap.get(key), key);
            }
            keys = null;
        } catch (Exception e) {
            e.printStackTrace();
            idmap = new HashMap<String, String>();
        }
    }
}
