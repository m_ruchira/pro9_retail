package com.isi.csvr.forex;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Dec 7, 2006
 * Time: 9:53:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class RequsetTimer extends JLabel
        implements MouseListener, ActionListener {
    public boolean isTimerExpired = false;
    long time;
    //   (Time is measured in milliseconds.)
    // while the stopwatch is running
    int time_cons = 60;
    int s = 0;
    int m = 0;
    int h = 0;
    long last = 0;
    private long startTime;   // Start time of stopwatch.
    private boolean running;  // True when the stopwatch is running.
    private Timer timer;  // A timer that will generate events

    public RequsetTimer() {
        // Constructor.
        super(" ", JLabel.CENTER);
        addMouseListener(this);
    }

    public void actionPerformed(ActionEvent evt) {
        // This will be called when an event from the
        // timer is received.  It just sets the stopwatch
        // to show the amount of time that it has been running.
        // Time is rounded down to the nearest second.
        // long t=System.currentTimeMillis();
        //System.out.println("long true :"+t);
        //long time = (t - startTime) / 1000;
        try {
            time = (startTime - System.currentTimeMillis()) / 1000;
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

            if (time != last) {
                setHorizontalAlignment(SwingConstants.CENTER);
                setText(timeFormatter2(time));
                if (timeFormatter2(time).equals("00:00")) {
                    isTimerExpired = true;
                }
                //  System.out.println("long time :"+time);
                //setText(format.format(time));
                timeFormatter2(time);
                last = time;
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String timeFormater(long time) {


        if (time != 0 && time % 60 == 0) {


            m++;
            System.out.println(m);
            if (m != 0 && m % 60 == 0) {

                h = h + 1;
                m = 0;
            } else {


                System.out.println(m);
            }
            if (m < 60 && time % 60 < 10) {
                if (h < 10) {
                    return "" + "0" + h + ":" + "0" + m + ":" + "0" + time % 60;
                } else {
                    return "" + h + ":" + "0" + m + ":" + "0" + time % 60;
                }
            } else {
                //   System.out.println("TTTTT");
                if (h < 10) {
                    return "" + "0" + h + ":" + m + ":" + time % 60;
                } else {
                    return "" + h + ":" + m + ":" + time % 60;
                }
            }
        } else {
            if (time % 60 < 10) {
                if (m < 10) {
                    if (h < 10) {
                        return "" + "0" + h + ":" + "0" + m + ":" + "0" + time % 60;
                    } else {
                        return "" + h + ":" + "0" + m + ":" + "0" + time % 60;
                    }
                } else {
                    if (h < 10) {
                        return "" + "0" + h + ":" + m + ":" + "0" + time % 60;
                    } else {
                        return "" + h + ":" + m + ":" + "0" + time % 60;
                    }
                }

            } else {
                if (m < 10) {
                    if (h < 10) {
                        return "" + "0" + h + ":" + "0" + m + ":" + time % 60;
                    } else {
                        return "" + h + ":" + "0" + m + ":" + time % 60;
                    }
                } else {
                    if (h < 10) {
                        return "" + "0" + h + ":" + m + ":" + time % 60;
                    } else {
                        return "" + h + ":" + m + ":" + time % 60;
                    }
                }
                //return ""+h+":"+m+":"+time%60;
            }
        }
    }

    public String timeFormatter2(long seconds) {
        //public int miliseconds;
        String time = null;


        if (seconds >= 0) {
            String sec;
            sec = "" + (seconds % 60);
            if (seconds % 60 < 10) {
                sec = "0" + (seconds % 60);
            }
            String min;
            if (seconds > 60) {
                min = "" + (seconds / 60 % 60);
                if ((seconds / 60 % 60) < 10) {
                    min = "0" + (seconds / 60 % 60);
                }
            } else {
                min = "00";
            }
            String hours;
            if (seconds / 60 > 60) {
                hours = "" + (seconds / 60 / 60);
                if ((seconds / 60 / 60) < 10) {
                    hours = "0" + (seconds / 60 / 60);
                }

            } else {
                hours = "00";
            }

            time = min + ":" + sec; //"" + hours + ":" +
        }
        return time;
    }

    public void startTimer() {
        if (running == false) {
            startTime = System.currentTimeMillis() + 11000;
            timer = new Timer(1000, this);
            timer.start();
        }

    }

    public void stopTimer() {
        timer.stop();

        running = false;
    }

    public void restartTimer() {
        startTime = System.currentTimeMillis() + last * 1000;
        timer.restart();
    }

    public void mousePressed(MouseEvent evt) {
//        // React when user presses the mouse by
//        // starting or stopping the stopwatch.  Also start
//        // or stop the timer.
//        if (running == false) {
//            // Record the time and start the stopwatch.
//            running = true;
//           // startTime = evt.getWhen();  // Time when mouse was clicked.
//           startTime =evt.getWhen()+300000;
//            //setText("Running:  0 seconds");
//            if (timer == null) {
//                startTime =evt.getWhen()+300000;
//                timer = new Timer(100, this);
//                timer.start();
//            } else{
//                System.out.println("last test : "+last);
//                startTime=evt.getWhen()+last*1000;
//                timer.restart();
//            }
//        } else {
//            // Stop the stopwatch.  Compute the elapsed time since the
//            // stopwatch was started and display it.
//            timer.stop();
//
//            running = false;
//            startTime = evt.getWhen()+last;
//            //double seconds = (endTime - startTime) / 1000.0;
//            //setText("Time: " + seconds + " min.");
//        }
    }

    public void mouseReleased(MouseEvent evt) {
    }

    public void mouseClicked(MouseEvent evt) {
    }

    public void mouseEntered(MouseEvent evt) {
    }

    public void mouseExited(MouseEvent evt) {
    }

}