package com.isi.csvr.forex;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 07-Jan-2008 Time: 12:02:49 To change this template use File | Settings
 * | File Templates.
 */
public class FXTreeObject {
    String firstpart;
    String secondpart;

    public FXTreeObject(String first, String second) {
        this.firstpart = first;
        this.secondpart = second;
    }

    public String getFirstpart() {
        return firstpart;
    }

    public String getSecondpart() {
        return secondpart;
    }

    public String getFXName() {
        return firstpart + "/" + secondpart;
    }

    public String getFXKey() {
        return SharedMethods.getKey("FORX", firstpart + secondpart, Meta.INSTRUMENT_FOREX);
    }


}
