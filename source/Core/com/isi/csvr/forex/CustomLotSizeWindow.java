package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 17-Jun-2008 Time: 13:35:58 To change this template use File | Settings
 * | File Templates.
 */
public class CustomLotSizeWindow extends JDialog implements ActionListener {
    public static CustomLotSizeWindow self;
    String key;
    private JLabel keyLabel;
    private JLabel numberLbl1;
    private JLabel numberLbl2;
    private JLabel numberLbl3;
    private JLabel numberLbl4;
    private JLabel numberLbl5;
    private JLabel numberLbl6;
    private ForexSpiner fdl1;
    private ForexSpiner fdl2;
    private ForexSpiner fdl3;
    private ForexSpiner fdl4;
    private ForexSpiner fdl5;
    private ForexSpiner fdl6;
    private ForexSpiner incrementAmt;
    private ForexSpiner defaultAmt;
    private TWButton applyBtn;
    private TWButton cancelBtn;
    private TWButton resetBtn;
    private JPanel upperPanel;
    private JPanel middlePanel;
    private JPanel lowerPanel;
    private JPanel mainPanel;
    private JPanel middleLeft;
    private JPanel middleRight;
    private JLabel incrementLbl;
    private JLabel defaultLbl;

    public CustomLotSizeWindow() {
        super(Client.getInstance().getFrame());
        this.setSize(370, 175);
        this.setModal(true);
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"22", "100%", "22"}, 0, 0));
        mainPanel.setBorder(BorderFactory.createEtchedBorder());
        this.add(mainPanel);
        createUI();
    }

    public static CustomLotSizeWindow getSharedInstance() {
        if (self == null) {
            self = new CustomLotSizeWindow();
        }
        return self;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setData(String key) {
        keyLabel.setText(SharedMethods.getSymbolFromKey(key) + " : Custom Settings");
        if (CustomSettingsStore.getSharedInstance().isKeyContains(key)) {
            FXSettingsObject fxso = (FXSettingsObject) CustomSettingsStore.getSharedInstance().getSettingsObject(key);
            fdl1.setText(new Double(fxso.getAmtArray()[0]));
            fdl2.setText(new Double(fxso.getAmtArray()[1]));
            fdl3.setText(new Double(fxso.getAmtArray()[2]));
            fdl4.setText(new Double(fxso.getAmtArray()[3]));
            fdl5.setText(new Double(fxso.getAmtArray()[4]));
            fdl6.setText(new Double(fxso.getAmtArray()[5]));
            incrementAmt.setText(new Double(fxso.getUnitIncrement()));
            defaultAmt.setText(new Double(fxso.getDefaultAmmount()));

            fdl1.setUnitIncrement((int) fxso.getUnitIncrement());
            fdl2.setUnitIncrement((int) fxso.getUnitIncrement());
            fdl3.setUnitIncrement((int) fxso.getUnitIncrement());
            fdl4.setUnitIncrement((int) fxso.getUnitIncrement());
            fdl5.setUnitIncrement((int) fxso.getUnitIncrement());
            fdl6.setUnitIncrement((int) fxso.getUnitIncrement());


        } else {
            resetToDefault();
        }

    }

    public void resetToDefault() {
        fdl1.setText(new Double(100000));
        fdl2.setText(new Double(200000));
        fdl3.setText(new Double(300000));
        fdl4.setText(new Double(400000));
        fdl5.setText(new Double(500000));
        fdl6.setText(new Double(600000));
        incrementAmt.setText(new Double(50000));
        defaultAmt.setText(new Double(100000));
    }

    private void createUI() {
        keyLabel = new JLabel();
        numberLbl1 = new JLabel("1.");
        numberLbl2 = new JLabel("2.");
        numberLbl3 = new JLabel("3.");
        numberLbl4 = new JLabel("4.");
        numberLbl5 = new JLabel("5.");
        numberLbl6 = new JLabel("6.");
        fdl1 = new ForexSpiner();
        fdl2 = new ForexSpiner();
        fdl3 = new ForexSpiner();
        fdl4 = new ForexSpiner();
        fdl5 = new ForexSpiner();
        fdl6 = new ForexSpiner();
        incrementAmt = new ForexSpiner();
        defaultAmt = new ForexSpiner();
        applyBtn = new TWButton(Language.getString("APPLY"));
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        resetBtn = new TWButton(Language.getString("RESET"));
        applyBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        resetBtn.addActionListener(this);
        upperPanel = new JPanel();
        middlePanel = new JPanel();
        lowerPanel = new JPanel();
        middleLeft = new JPanel();
        middleRight = new JPanel();
        incrementLbl = new JLabel(Language.getString("INCREMENT"));
        defaultLbl = new JLabel(Language.getString("DEFAULT_AMMOUNT"));
        mainPanel.add(creatUpperPanel());
        mainPanel.add(creatMiddlePanel());
        mainPanel.add(creatLowerPanel());
    }

    private JPanel creatUpperPanel() {
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        upperPanel.add(keyLabel);
        keyLabel.setOpaque(true);
        keyLabel.setBackground(Theme.getColor("INTERNALFRAME_ACTIVE_TITLE_BGCOLOR_LIGHT"));
        return upperPanel;
    }

    private JPanel creatMiddlePanel() {
        middlePanel.setLayout(new FlexGridLayout(new String[]{"67%", "33%"}, new String[]{"100%"}, 0, 0));
        //creating middle left Panel
        middleLeft.setLayout(new FlexGridLayout(new String[]{"10", "100", "100%", "10", "100", "100%"}, new String[]{"33%", "33%", "34%"}, 0, 5));
        middleLeft.setBorder(BorderFactory.createTitledBorder("Ammounts"));
        middleLeft.add(numberLbl1);
        middleLeft.add(fdl1);
        middleLeft.add(new JLabel());
        middleLeft.add(numberLbl4);
        middleLeft.add(fdl4);
        middleLeft.add(new JLabel());
        middleLeft.add(numberLbl2);
        middleLeft.add(fdl2);
        middleLeft.add(new JLabel());
        middleLeft.add(numberLbl5);
        middleLeft.add(fdl5);
        middleLeft.add(new JLabel());
        middleLeft.add(numberLbl3);
        middleLeft.add(fdl3);
        middleLeft.add(new JLabel());
        middleLeft.add(numberLbl6);
        middleLeft.add(fdl6);
        middleLeft.add(new JLabel());

        //creating middle right panel
        middleRight.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20%", "25%", "10%", "20%", "25%"}, 0, 0));
        middleRight.setBorder(BorderFactory.createTitledBorder("Other Settings"));
        middleRight.add(incrementLbl);
        middleRight.add(incrementAmt);
        middleRight.add(new JLabel());
        middleRight.add(defaultLbl);
        middleRight.add(defaultAmt);


        middlePanel.add(middleLeft);
        middlePanel.add(middleRight);
        return middlePanel;
    }

    private JPanel creatLowerPanel() {
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"75", "100%", "75", "10", "75"}, new String[]{"100%"}, 5, 1));
        lowerPanel.add(resetBtn);
        lowerPanel.add(new JLabel());
        lowerPanel.add(applyBtn);
        lowerPanel.add(new JLabel());
        lowerPanel.add(cancelBtn);
        return lowerPanel;
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource().equals(applyBtn)) {
            applyAction();
            this.dispose();
        } else if (e.getSource().equals(cancelBtn)) {
            this.dispose();
        } else if (e.getSource().equals(resetBtn)) {
            resetToDefault();
        }
    }

    public void applyAction() {
        long[] arr = {Long.parseLong(fdl1.getText()), Long.parseLong(fdl2.getText()), Long.parseLong(fdl3.getText()), Long.parseLong(fdl3.getText()), Long.parseLong(fdl4.getText()), Long.parseLong(fdl6.getText())};
        FXSettingsObject fxso = new FXSettingsObject(arr);
        fxso.setDefaultAmmount(Long.parseLong(defaultAmt.getText()));
        fxso.setUnitIncrement(Long.parseLong(incrementAmt.getText()));
        CustomSettingsStore.getSharedInstance().putSettingsObject(this.key, fxso);

    }

}
