package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWButton;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.util.Enumeration;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA. User: admin Date: 30-Aug-2007 Time: 10:04:19 To change this template use File | Settings |
 * File Templates.
 */
public class ForexSearch extends JDialog {
    public static ForexSearch self;
    public ForexBoard parent;
    SmartProperties prop;
    DefaultTreeModel treeModel;
    private JPanel upperPanel;
    private JPanel lowerPanel;
    private JPanel middlePanel;
    private JLabel searchlabel;
    private TWButton searchButton;
    private TWButton okBtn;
    private TWButton cancelBtn;
    private JTextField searchTextField;
    private JScrollPane treeScroll;
    private JTree instrumentTree;

    public ForexSearch(ForexBoard fb) {
        super(Client.getInstance().getFrame(), "Instrument Search", true);
        this.parent = fb;
        loadAllSymbols();
        getContentPane().setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%", "30"}));
        getContentPane().add(createUpperPanel());
        getContentPane().add(createMiddelepanel());
        getContentPane().add(createLowerpanel());
        setSize(300, 450);
        setModal(true);
//        setVisible(true);

    }

    public static ForexSearch getSharedInstance(ForexBoard fb) {
        if (self == null) {
            self = new ForexSearch(fb);
        }
        return self;
    }

    public JPanel createUpperPanel() {
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"70", "100%", "70"}, new String[]{"100%"}, 5, 5));
        searchlabel = new JLabel("Look for : ");
        searchButton = new TWButton("Go");
        searchTextField = new JTextField();
        upperPanel.add(searchlabel);
        upperPanel.add(searchTextField);
        upperPanel.add(searchButton);

        return upperPanel;

    }

    public JPanel createMiddelepanel() {
        middlePanel = new JPanel();
        middlePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 5, 5));
//        instrumentTree=new JTree();
        treeScroll = new JScrollPane(instrumentTree);
        middlePanel.add(treeScroll);
        return middlePanel;
    }

    public JPanel createLowerpanel() {
        lowerPanel = new JPanel();
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%", "70", "70"}, new String[]{"100%"}, 5, 5));
        okBtn = new TWButton("OK");
        cancelBtn = new TWButton("Cancel");
        lowerPanel.add(new JLabel());
        lowerPanel.add(okBtn);
        lowerPanel.add(cancelBtn);
        return lowerPanel;
    }

    public void loadSearchTree() {
        Object[] nodes = new Object[1];
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(new ForexNode(1, "Major Symbols"));
        DefaultMutableTreeNode parent = top;
        nodes[0] = top;

        try {
            Enumeration e = prop.keys();
            while (e.hasMoreElements()) {
                String first = (String) e.nextElement();
                StringTokenizer tokens = new StringTokenizer(prop.getProperty(first), ",");
                while (tokens.hasMoreTokens()) {
                    String[] arr = tokens.nextToken().split("~");
                    System.out.println("CHECK : " + first + " | " + arr[0]);
                    DefaultMutableTreeNode node = new DefaultMutableTreeNode(new ForexNode(0, first + arr[0]));
                    parent.add(node);
                    arr = null;
                }
                first = null;

            }

        } catch (Exception e) {

        }
        treeModel = new DefaultTreeModel(top);
        instrumentTree = new JTree(treeModel);
        instrumentTree.setShowsRootHandles(true);
        instrumentTree.setEditable(false);
        TreePath path = new TreePath(nodes);
        instrumentTree.setSelectionPath(path);
        instrumentTree.addTreeSelectionListener(new InstrumentSelectionListener());

    }

    public void loadAllSymbols() {
        prop = new SmartProperties("=");
        try {
            prop = new SmartProperties("=");
            prop.loadCompressed(Settings.CONFIG_DATA_PATH + "/forex.msf");

        } catch (Exception s_e) {
            s_e.printStackTrace();
        }
        loadSearchTree();
    }

    private void searchTree(DefaultMutableTreeNode node) {

    }

}
