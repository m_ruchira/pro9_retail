package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collections;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 07-Jan-2008 Time: 10:09:09 To change this template use File | Settings
 * | File Templates.
 */
public class ForexSelectionFrame extends InternalFrame implements ActionListener, MouseListener {
    private static ForexSelectionFrame self = null;
    JTree tree;
    TreeCellRenderer renderer;
    Vector commonVector;
    Vector rootVector;
    Object rootNodes[];
    SmartProperties prop;
    TreePath g_oSelectedPath;
    private JPanel mainPanel;
    private Vector sortedSymbols = new Vector();
    private ForexBoard parent;

    public ForexSelectionFrame(ForexBoard parent) {
        setLayout(new BorderLayout());
        this.parent = parent;
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        createUI();
        add(mainPanel, BorderLayout.CENTER);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        setTitle(Language.getString("FOREX_SEARCH"));
        setClosable(true);
        setSize(300, 375);
        setResizable(true);
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
    }

    public static synchronized ForexSelectionFrame getSharedInstance() {
        return self;
    }

    public static void initialize(ForexBoard parent) {
        if (self == null) {
            self = new ForexSelectionFrame(parent);
        }

    }

    public void createUI() {
        rootNodes = loadForexSymbols();
        rootVector = new ForexVector("Root", null, rootNodes);
        tree = new JTree(rootVector);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        renderer = new FXTreeRenderer();
        tree.setCellRenderer(renderer);
        tree.setToggleClickCount(1);
        tree.addMouseListener(this);
        JScrollPane scrollPane = new JScrollPane(tree);
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        GUISettings.applyOrientation(mainPanel);
        tree.updateUI();
        tree.repaint();
        mainPanel.repaint();
        mainPanel.updateUI();
        this.repaint();
    }

    public void setParent(ForexBoard parent) {
        this.parent = parent;

    }

    public ForexBoard getForexBoard() {
        return this.parent;
    }

    private Object[] loadForexSymbols() {
        Vector tempVector = new Vector();
        prop = new SmartProperties("=");
        try {
            prop = new SmartProperties("=");
            prop.loadCompressed(Settings.CONFIG_DATA_PATH + "/forex.msf");
            sortSymbols();
        } catch (Exception s_e) {
            s_e.printStackTrace();
        }
        for (int i = 0; i < sortedSymbols.size(); i++) {
            String forexKey = (String) sortedSymbols.get(i);
            String forexSymbolList = (String) prop.get(forexKey);
            tempVector.add(new ForexVector(forexKey, "_ _ _", getFXSymbolsetForKey(forexKey, forexSymbolList)));
//            System.out.println("FX Test : "+forexSymbolList);
        }
        return tempVector.toArray();
    }

    public void sortSymbols() {
        sortedSymbols = new Vector(prop.keySet());
        Collections.sort(sortedSymbols);

    }

    private Object[] getFXSymbolsetForKey(String firstPart, String FXSymbolList) {
        Vector tempVector = new Vector();
        try {
            String[] symbolArray = FXSymbolList.split(",");
            for (int i = 0; i < symbolArray.length; i++) {
                String[] temp = symbolArray[i].split("~");
                String secondPart = temp[0];
                tempVector.add(new FXTreeObject(firstPart, secondPart));

            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return tempVector.toArray();
    }

    public void actionPerformed(ActionEvent e) {

    }

    public void mousePressed(MouseEvent e) {
        int selRow = tree.getRowForLocation(e.getX(), e.getY());
        g_oSelectedPath = tree.getPathForLocation(e.getX(), e.getY());
        if (selRow != -1) {
            tree.setSelectionPath(g_oSelectedPath);
            valueChanged();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        }
    }

    public void valueChanged() {

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (node == null) return;
        try {
            Object nodeInfo = node.getUserObject();
            if (node.isLeaf()) {
                try {
                    if (nodeInfo instanceof FXTreeObject) {
                        FXTreeObject fxto = (FXTreeObject) nodeInfo;
                        parent.createNewFXViewFromSelection(fxto.getFXKey());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}