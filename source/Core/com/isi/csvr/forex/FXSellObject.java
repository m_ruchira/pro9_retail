package com.isi.csvr.forex;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 30-Apr-2008 Time: 10:05:13 To change this template use File | Settings
 * | File Templates.
 */
public class FXSellObject {
    private double sellPrice;
    private String quoteID;
    private String bestQuoteID;
    private String brockerID;

    public FXSellObject() {

    }

    public double getBuyPrice() {
        return sellPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.sellPrice = buyPrice;
    }

    public String getQuoteID() {
        return quoteID;
    }

    public void setQuoteID(String quoteID) {
        this.quoteID = quoteID;
    }

    public String getBestQuoteID() {
        return bestQuoteID;
    }

    public void setBestQuoteID(String bestQuoteID) {
        this.bestQuoteID = bestQuoteID;
    }

    public String getBrockerID() {
        return brockerID;
    }

    public void setBrockerID(String brockerID) {
        this.brockerID = brockerID;
    }
}
