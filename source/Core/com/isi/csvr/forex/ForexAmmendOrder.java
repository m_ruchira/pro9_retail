package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWFormattedTextField;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Nov-2007 Time: 10:53:34 To change this template use File | Settings
 * | File Templates.
 */
public class ForexAmmendOrder extends InternalFrame implements ActionListener, InternalFrameListener, TradingConnectionListener, ChangeListener, PropertyChangeListener, Themeable {
    //functional
    public static ForexAmmendOrder self;
    final int quoteOrderType = 3;
    final int marketOrderType = 2;
    final int limitOrderType = 1;
    //UI
    public JPanel upperPanel;
    public JPanel midlePanel;
    public JPanel lowerPanel;
    public JPanel quantityPanel;
    public JPanel amendBtnPanel;
    public TWComboBox typeCombo;
    public TWComboBox portfolioCombo;
    public TWButton requestCancelBtn;
    public String symbol = "";
    public int instrument = -1;
    public boolean updatorLock = false;
    FXLimitButton bidLBtn;
    FXLimitButton askLBtn;
    FXButton bidBtn;
    FXButton askBtn;
    Color fxColor = new Color(185, 249, 185);
    int side = 1;   //sell=2 ,buy=1
    ForexBuySellUpdator updator;
    Transaction oldTransaction;
    long[] arrayBlockSizes = new long[9];
    private JLabel symbolLabel;
    private JLabel quantityLabel;
    private JLabel typeLabel;
    private JLabel priceLabel;
    private JLabel pfLabel;
    private TWTextField symbolField;
    private TWTextField priceField;
    private JButton dowwnArrow;
    private TWFormattedTextField tf;
    private NumberFormat amountFormat;
    private double amount = 1000000;
    private JSpinner blockSpinner;
    private TWButton cancelBtn;
    private TWButton submitBtn;
    private long blockSpinnerDefault = 1000000;
    private Color blockItemFGColor;
    private boolean isLimitType = true;

    public ForexAmmendOrder(Transaction tr) {
        super();
        this.oldTransaction = tr;
        this.setTitle(Language.getString("FX_AMEND_CANCEL"));
        this.setSize(270, 340);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(false);
        this.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"125", "30", "100%", "40"}, 0, 0));
        upperPanel = new JPanel();
        midlePanel = new JPanel();
        lowerPanel = new JPanel();
        amendBtnPanel = new JPanel();
        quantityPanel = new JPanel();
        symbolLabel = new JLabel(Language.getString("CUS_INDEX_SYMBOL"));
        priceLabel = new JLabel(Language.getString("PRICE"));
        pfLabel = new JLabel(Language.getString("PORTFOLIO"));
        quantityLabel = new JLabel(Language.getString("WHAT_IF_CALC_QTY"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        symbolField = new TWTextField();
        priceField = new TWTextField();
        typeCombo = new TWComboBox();
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        submitBtn = new TWButton(Language.getString("SUBMIT"));
        portfolioCombo = new TWComboBox();
        cancelBtn.addActionListener(this);
        submitBtn.addActionListener(this);
        requestCancelBtn = new TWButton();

        //limit buttons
        bidLBtn = new FXLimitButton(Language.getString("TRADE_SIDE_BUY"), "EURUSD");
        bidLBtn.setEnabled(false);
        bidLBtn.addActionListener(this);
        bidLBtn.setActionCommand("LIMIT_BID");
        askLBtn = new FXLimitButton(Language.getString("SELL"), "EURUSD");
        askLBtn.setEnabled(false);
        askLBtn.addActionListener(this);
        askLBtn.setActionCommand("LIMIT_ASK");
        bidLBtn.setPreferredSize(new Dimension(70, 60));
        askLBtn.setPreferredSize(new Dimension(70, 60));
        askLBtn.setOpaque(true);
        askLBtn.setBackground(fxColor);
        bidLBtn.setOpaque(true);
        bidLBtn.setBackground(fxColor);

        //market buttons
        bidBtn = new FXButton(Language.getString("TRADE_SIDE_BUY"), "EURUSD");
        bidBtn.setEnabled(false);
        bidBtn.addActionListener(this);
        bidBtn.setActionCommand("MARKET_BID");
        askBtn = new FXButton(Language.getString("SELL"), "EURUSD");
        askBtn.setEnabled(false);
        askBtn.addActionListener(this);
        askBtn.setActionCommand("MARKET_ASK");
        askBtn.setOpaque(true);
        askBtn.setBackground(fxColor);
        bidBtn.setOpaque(true);
        bidBtn.setBackground(fxColor);
        bidBtn.setPreferredSize(new Dimension(70, 60));
        askBtn.setPreferredSize(new Dimension(70, 60));

        populateTypeCombo();
        this.add(createUpperPanel());
        this.add(createMiddlePanel());
        this.add(createLowerPanel());
        this.add(createAmendBtnPanel());
        setData(tr);
        updator = new ForexBuySellUpdator();
        updator.start();
        GUISettings.applyOrientation(this);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
        Theme.registerComponent(this);
    }

    public static ForexAmmendOrder getSharedInstance(Transaction tr) {
        if (self != null) {
            self = null;
            self = new ForexAmmendOrder(tr);
        }
        return new ForexAmmendOrder(tr);
    }

    public void setData(Transaction tr) {
        if (tr != null) {
            this.side = tr.getSide();
            this.symbol = tr.getSymbol();
            this.instrument = tr.getSecurityType();
            symbolField.setText(this.symbol);
            tf.setValue(tr.getOrderQuantity());
            setOrderType(tr.getType(), this.side, tr);
            portfolioCombo.addItem(tr.getPortfolioNo());
            symbolField.setEditable(false);
            portfolioCombo.setEnabled(false);


        }
    }

    public JPanel createQuantityPanel() {
        quantityPanel = new JPanel();
        quantityPanel.setLayout(new FlexGridLayout(new String[]{"85%", "15%"}, new String[]{"100%"}));
        dowwnArrow = new JButton(new DownArrow());
        dowwnArrow.setBorderPainted(false);
        blockSpinner = new JSpinner(new SpinnerNumberModel(1000000, 1000000, 3000000, 500000));
        amountFormat = NumberFormat.getNumberInstance();
        tf = new TWFormattedTextField(amountFormat);
        tf.setValue(new Double(amount));
        tf.setColumns(10);
        tf.addPropertyChangeListener("value", this);
        tf.setHorizontalAlignment(SwingConstants.RIGHT);
//        tf.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
//        tf.setText("" + blockSpinnerDefault);
        tf.setEditable(true);
        blockSpinner.setEditor(tf);
        tf.setBackground(Color.white);
        quantityPanel.add(blockSpinner);
        quantityPanel.add(dowwnArrow);
        dowwnArrow.addActionListener(this);
        blockSpinner.addChangeListener(this);
        return quantityPanel;

    }

    private JPanel createUpperPanel() {
        symbolLabel = new JLabel(Language.getString("CUS_INDEX_SYMBOL"));
        priceLabel = new JLabel(Language.getString("PRICE"));
        pfLabel = new JLabel(Language.getString("PORTFOLIO"));
        quantityLabel = new JLabel(Language.getString("WHAT_IF_CALC_QTY"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        priceField.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        priceField.setHorizontalAlignment(SwingConstants.RIGHT);
        priceField.setText("");
        typeCombo.addActionListener(this);
        symbolField.setEditable(false);
        priceField.setEditable(false);
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"50%", "110", "10", "110", "50%"}, new String[]{"20", "20", "20", "20", "20", "100%"}, 5, 5));
        upperPanel.add(new JLabel());
        upperPanel.add(symbolLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(symbolField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(quantityLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(createQuantityPanel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(typeLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(typeCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(priceLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(priceField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(pfLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(portfolioCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        GUISettings.applyOrientation(upperPanel);

        return upperPanel;
    }

    private JPanel createMiddlePanel() {
        requestCancelBtn.addActionListener(this);
        requestCancelBtn.setText(Language.getString("REQUEST"));
        requestCancelBtn.setActionCommand(Language.getString("REQUEST"));
        requestCancelBtn.setEnabled(false);
        midlePanel.setLayout(new FlexGridLayout(new String[]{"50%", "150", "50%"}, new String[]{"100%"}, 5, 5));
        midlePanel.add(new JLabel());
        midlePanel.add(requestCancelBtn);
        midlePanel.add(new JLabel());//requsetTimer
        return midlePanel;

    }

    private JPanel createLowerPanel() {
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"50%", "70", "70", "50%"}, new String[]{"1", "100%"}));
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("FX_AMEND_BUYSELL")));
        GUISettings.applyOrientation(lowerPanel);
        return lowerPanel;
    }

    public JPanel createAmendBtnPanel() {
        amendBtnPanel.setLayout(new FlexGridLayout(new String[]{"50%", "90", "10", "90", "50%"}, new String[]{"25"}, 5, 5));
        amendBtnPanel.add(new JLabel());
        amendBtnPanel.add(submitBtn);
        amendBtnPanel.add(new JLabel());
        amendBtnPanel.add(cancelBtn);
        amendBtnPanel.add(new JLabel());
        return amendBtnPanel;
    }

    public void populateTypeCombo() {
        typeCombo.addItem(new TWComboItem("1", Language.getString("ORDER_TYPE_MARKET")));
        typeCombo.addItem(new TWComboItem("2", Language.getString("ORDER_TYPE_LIMIT")));
        typeCombo.addItem(new TWComboItem("3", Language.getString("ORDER_TYPE_QUOTE")));
    }

    public void setUIforOrderType(int type) {
        if ((type == quoteOrderType) && (lowerPanel != null)) {
            lowerPanel.removeAll();
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.updateUI();

        } else if ((type == marketOrderType) && (lowerPanel != null)) {
            lowerPanel.removeAll();
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            if (this.side == TradeMeta.BUY) {
                bidBtn.setEnabled(false);
                askBtn.setEnabled(false);
                lowerPanel.add(bidBtn);
                lowerPanel.add(askBtn);
            } else if (this.side == TradeMeta.SELL) {
                bidBtn.setEnabled(false);
                askBtn.setEnabled(false);
                lowerPanel.add(bidBtn);
                lowerPanel.add(askBtn);
            }

            lowerPanel.add(new JLabel());
            lowerPanel.updateUI();

        } else if ((type == limitOrderType) && (lowerPanel != null)) {
            lowerPanel.removeAll();
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            lowerPanel.add(new JLabel());
            if (this.side == TradeMeta.BUY) {
                bidLBtn.setEnabled(false);
                askLBtn.setEnabled(false);
                lowerPanel.add(bidLBtn);
                lowerPanel.add(askLBtn);
            } else if (this.side == TradeMeta.SELL) {
                bidLBtn.setEnabled(false);
                askLBtn.setEnabled(false);
                lowerPanel.add(bidLBtn);
                lowerPanel.add(askLBtn);
            }

            lowerPanel.add(new JLabel());
            lowerPanel.updateUI();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(cancelBtn)) {
            this.setVisible(false);

        } else if (e.getSource().equals(submitBtn)) {
            if (isLimitType) {
                try {
                    oldTransaction.setPrice(Double.parseDouble(priceField.getText()));
                    oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
//                    int side;
//                    if (e.getActionCommand().equals("LIMIT_ASK")) {
//                        side = TradeMeta.SELL;
//                    } else {
//                        side = TradeMeta.BUY;
//                    }
                    if (TradingShared.isConnected()) {
                        TradeMethods.amendOrder(oldTransaction, false, this.side, TradeMeta.ORDER_TYPE_LIMIT, Double.parseDouble(priceField.getText()), "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                                -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                    } else {
                        JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (Exception e1) {
                    System.out.println("Forex feed unavailable");
                    e1.printStackTrace();
                }
            } else {
                try {
//                int side;
                    double price;
//                if (e.getActionCommand().equals("MARKET_ASK")) {
//                    side = TradeMeta.SELL;
//                    price = askBtn.buySellVal;
//                } else {
//                    side = TradeMeta.BUY;
//                    price = bidBtn.buySellVal;
//                }
                    if (this.side == TradeMeta.SELL) {
                        price = askBtn.buySellVal;
                    } else {
                        price = bidBtn.buySellVal;
                    }
                    oldTransaction.setPrice(price);
                    oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                    if (TradingShared.isConnected()) {
                        TradeMethods.amendOrder(oldTransaction, false, side, TradeMeta.ORDER_TYPE_MARKET, price, "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                                -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                    } else {
                        JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (Exception e1) {
                    System.out.println("Forex feed unavailable");
                    e1.printStackTrace();
                }

            }
            this.setVisible(false);

        } else if (e.getSource().equals(typeCombo)) {
            TWComboItem twc = (TWComboItem) typeCombo.getSelectedItem();
            if (twc.getId().equals("1")) {
                if (updator != null) {
                    updatorLock = false;
                }
                priceField.setText("");
                priceField.setEditable(false);
                requestCancelBtn.setEnabled(false);
                setUIforOrderType(marketOrderType);
                isLimitType = false;
            } else if (twc.getId().equals("2")) {
                updatorLock = true;
                priceField.setEditable(true);
                requestCancelBtn.setEnabled(false);
                setUIforOrderType(limitOrderType);
                isLimitType = true;
            } else if (twc.getId().equals("3")) {
                if (updator != null) {
                    updatorLock = false;
                }
                priceField.setText("");
                priceField.setEditable(false);
                requestCancelBtn.setEnabled(true);
                setUIforOrderType(quoteOrderType);
            }

        } else if (e.getSource() == dowwnArrow) {
            Point location = new Point(quantityPanel.getX(), (quantityPanel.getY()) + quantityPanel.getBounds().height);
            SwingUtilities.convertPointToScreen(location, quantityPanel.getParent());
            JPopupMenu jl = createBlockSizeList();
            jl.setLocation(location);
            jl.setPreferredSize(new Dimension(quantityPanel.getWidth(), 130));
            jl.show(quantityPanel, 2, quantityPanel.getHeight() + 2);
        } else if (e.getActionCommand().equals("MARKET_BID") || e.getActionCommand().equals("MARKET_ASK")) {
            try {
                int side;
                double price;
                if (e.getActionCommand().equals("MARKET_ASK")) {
                    side = TradeMeta.SELL;
                    price = askBtn.buySellVal;
                } else {
                    side = TradeMeta.BUY;
                    price = bidBtn.buySellVal;
                }
                oldTransaction.setPrice(price);
                oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                if (TradingShared.isConnected()) {
                    TradeMethods.amendOrder(oldTransaction, false, side, TradeMeta.ORDER_TYPE_MARKET, price, "*",
                            Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                            -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }

        } else if (e.getActionCommand().equals("LIMIT_BID") || e.getActionCommand().equals("LIMIT_ASK")) {
            try {
                oldTransaction.setPrice(Double.parseDouble(priceField.getText()));
                oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                int side;
                if (e.getActionCommand().equals("LIMIT_ASK")) {
                    side = TradeMeta.SELL;
                } else {
                    side = TradeMeta.BUY;
                }
                if (TradingShared.isConnected()) {
                    TradeMethods.amendOrder(oldTransaction, false, side, TradeMeta.ORDER_TYPE_LIMIT, Double.parseDouble(priceField.getText()), "*",
                            Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                            -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }


        }

    }

    public JPopupMenu createBlockSizeList() {
        JPopupMenu blockPopup = new JPopupMenu("Block Popup");
        blockPopup.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        arrayBlockSizes[0] = 1000000;
        arrayBlockSizes[1] = 1500000;
        arrayBlockSizes[2] = 2000000;
        arrayBlockSizes[3] = 2500000;
        arrayBlockSizes[4] = 3000000;
        arrayBlockSizes[5] = 3500000;
        arrayBlockSizes[6] = 4000000;
        arrayBlockSizes[7] = 4500000;
        arrayBlockSizes[8] = 5000000;
        try {
            for (int i = 0; i < arrayBlockSizes.length; i++) {
                final JMenuItem blockItem = new JMenuItem();
                blockItem.setForeground(blockItemFGColor);
                blockItem.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                blockItem.setBorder(BorderFactory.createEmptyBorder());
                blockItem.setText(amountFormat.format(arrayBlockSizes[i]));
                blockItem.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tf.setValue(Long.parseLong(blockSizeTrimer(blockItem.getText())));
                    }
                });
                blockPopup.add(blockItem);
            }

        } catch (Exception e) {
            System.out.println("Error while loading block sizes...");
            e.printStackTrace();
        }
        return blockPopup;
    }

    private void setOrderType(char type, int side, Transaction tr) {
        try {
            switch (type) {
                case 'D':
                    typeCombo.setSelectedIndex(2);
                    typeCombo.setEnabled(false);
                    break;
                case '1':
                    typeCombo.setSelectedIndex(0);
                    requestCancelBtn.setEnabled(false);
                    break;
                case '2':
                    typeCombo.setSelectedIndex(1);
                    priceField.setText(tr.getPrice() + "");
                    priceField.setEditable(true);
                    requestCancelBtn.setEnabled(false);
                    setUIforOrderType(limitOrderType);
                    break;
                case 3:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public String blockSizeTrimer(String longString) {
        String splitpatern = ",";
        String[] parts = longString.split(splitpatern);
        String blocksize = "";
        for (int i = 0; i < parts.length; i++) {
            blocksize = blocksize + parts[i];
        }
        return blocksize.trim();
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        this.setVisible(false);

    }

    public void tradeServerConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void stateChanged(ChangeEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource().equals(blockSpinner)) {
            tf.setValue(blockSpinner.getValue());
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        //To change body of implemented methods use File | Settings | File Templates.
        Object source = evt.getSource();
        if (source == tf) {
            amount = ((Number) tf.getValue()).doubleValue();
        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
    }

    public class ForexBuySellUpdator extends Thread {


        public ForexBuySellUpdator() {
            super("ForexBoard_updator");
        }

        public void run() {
            while (true) {
                try {

                    bidBtn.update(SharedMethods.getKey("FORX", symbol, instrument), Long.parseLong(blockSizeTrimer(tf.getText())), true);
                    askBtn.update(SharedMethods.getKey("FORX", symbol, instrument), Long.parseLong(blockSizeTrimer(tf.getText())), true);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
