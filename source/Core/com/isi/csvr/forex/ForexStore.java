package com.isi.csvr.forex;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;

import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: Chandika Hewage Date: Sep 1, 2007 Time: 12:52:29 PM To change this template use File
 * | Settings | File Templates.
 */


public class ForexStore {

    /**
     * Main store for the forex objects
     */


    public static ForexStore self;
    public static Hashtable<String, FXObject> forexStore = new Hashtable<String, FXObject>();
    public static Hashtable<String, Double> discountStore = new Hashtable<String, Double>();
    public FXObject fxStock;

    public ForexStore() {

    }

    public static ForexStore getSharedInstance() {
        if (self == null) {
            self = new ForexStore();
        }
        return self;
    }

    public FXObject getForexObject(String exchange, String symbol, int instrumentType) {
        String sSymbol;
        try {
            sSymbol = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER)[0];
        } catch (Exception e) {
            sSymbol = symbol;
        }
        String key = SharedMethods.getKey(exchange, sSymbol, instrumentType);
        if (forexStore.containsKey(key)) {
            return forexStore.get(key);
        } else {
            fxStock = new FXObject(symbol, exchange);
            return fxStock;
        }
    }

    public FXObject getForexObject(String key) {
        if (forexStore.containsKey(key)) {
            return forexStore.get(key);
        } else {

            return null;
        }

    }

    public void processForexDicounts(String record) {
        try {
            TradeMessage message = new TradeMessage(record);
            String[] forexData = message.getMessageData().split(TradeMeta.DS);
            try {
                for (String discountrecord : forexData) {
                    try {
                        String[] fxDiscounts = discountrecord.split(TradeMeta.FD);
                        String exchange = null;
                        String symbol = null;
                        int instrumentType = 0;
                        double discount = 0d;
                        char tag;
                        for (int i = 0; i < fxDiscounts.length; i++) {
                            tag = fxDiscounts[i].charAt(0);
                            switch (tag) {
                                case 'A':
                                    exchange = (fxDiscounts[i].substring(1));
                                    break;
                                case 'B':
                                    symbol = (fxDiscounts[i].substring(1));
                                    break;
                                case 'C':
                                    instrumentType = Integer.parseInt(fxDiscounts[i].substring(1));
                                    break;
                                case 'D':
                                    discount = Double.parseDouble(fxDiscounts[i].substring(1));
                                    break;
                            }
                        }
                        setForexDiscountRates(SharedMethods.getKey(exchange, symbol, instrumentType), discount);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setForexDiscountRates(String key, double disCount) {
        FXObject fxo = (FXObject) getForexObject(key);
        fxo.setBuyDiscount(disCount);
        fxo.setSellDiscount(disCount);
    }

    /**
     * Tempory method for the testing purposes
     * just print objects in the forex store
     * and its contents of the block objects
     */
    /*public void printForexStore() {
        try {
            Enumeration en = forexStore.elements();
            while (en.hasMoreElements()) {
                FXObject fxo = (FXObject) en.nextElement();
                System.out.println("=========================================================");
                System.out.println("Currency code : " + fxo.getForexCurrency());
                System.out.println("Deviation     : " + fxo.getForexDeviation());
                System.out.println("Buy Date      : " + fxo.getForexBuyDate());
                System.out.println("Sell Date     : " + fxo.getForexSellDate());
                System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                Hashtable blockHash = fxo.getBlockObjects();
                Enumeration enn = blockHash.elements();
                while (enn.hasMoreElements()) {
                    BlockObect blo = (BlockObect) enn.nextElement();
                    System.out.println("Block Size : " + blo.getForexBlockSize());
                    System.out.println("Base Value : " + blo.getForexBaseValue());
                    System.out.println("Appender   : " + blo.getForexAppender());
                    System.out.println("Quote ID   : " + blo.getForexQuoteID());

                }
                fxo = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.out.println("ForexStore Testing failed....");
        }

    }*/

}
