package com.isi.csvr.forex;

import com.isi.csvr.shared.TWComboBox;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 03-May-2008 Time: 08:32:53 To change this template use File | Settings
 * | File Templates.
 */
public class ForexSymbolView extends JPanel implements ActionListener, MouseListener, KeyListener, ChangeListener, PropertyChangeListener {
    private JPanel currencyPanel;
    private TWComboBox currencyCombo;
    private JLabel timeLabel;

    private JPanel highLowPanel;
    private JLabel highLabel;
    private JLabel spreadLabel;
    private JLabel lowLabel;

    private JPanel buttonPanel;
    private JButton buyButton;
    private JButton sellButton;

    private JPanel blockSizePanel;
    private JLabel buyLabel;
    private JLabel sellLabel;
    private TWComboBox blockSizeCombo;

    private JPanel lowerCombinedPanel;

    public ForexSymbolView() {
        init();
        createUI();


    }

    private void init() {
        currencyPanel = new JPanel();
        currencyCombo = new TWComboBox();
        timeLabel = new JLabel("10:08:46");

        highLowPanel = new JPanel();
        highLabel = new JLabel("161.45");
        spreadLabel = new JLabel("1.0");
        lowLabel = new JLabel("160.46");

        buttonPanel = new JPanel();
        buyButton = new JButton("160.013");
        sellButton = new JButton("161.098");

        blockSizePanel = new JPanel();
        buyLabel = new JLabel("Buy");
        sellLabel = new JLabel("Sell");
        blockSizeCombo = new TWComboBox();

        lowerCombinedPanel = new JPanel();

    }

    private void createUI() {
        this.setPreferredSize(new Dimension(105, 160));
        this.setBorder(BorderFactory.createEtchedBorder());
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "75"}, 0, 0));
        this.add(createCurrencyPanel());
        this.add(createHighLowPanel());
        this.add(createLowerCombinedPanel());

    }

    private JPanel createCurrencyPanel() {
        currencyPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 0, 0));
        currencyPanel.add(currencyCombo);
        currencyPanel.add(timeLabel);
        return currencyPanel;
    }

    private JPanel createHighLowPanel() {
        highLowPanel.setOpaque(true);
        highLowPanel.setBackground(Color.GRAY);
        highLowPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        highLabel.setHorizontalAlignment(SwingConstants.TRAILING);
        lowLabel.setHorizontalAlignment(SwingConstants.LEADING);
        spreadLabel.setHorizontalAlignment(SwingConstants.CENTER);
        spreadLabel.setOpaque(true);
        spreadLabel.setBackground(Color.WHITE);
        highLowPanel.setLayout(new FlexGridLayout(new String[]{"35%", "30%", "35%"}, new String[]{"100%"}, 0, 0));
        highLowPanel.add(lowLabel);
        highLowPanel.add(spreadLabel);
        highLowPanel.add(highLabel);
        return highLowPanel;
    }

    private JPanel createButtonPanel() {
        buyButton.setOpaque(true);
        sellButton.setOpaque(true);
        buyButton.setBackground(Color.GREEN);
        sellButton.setBackground(Color.GREEN);
        buyButton.setBorder(BorderFactory.createEmptyBorder());
        sellButton.setBorder(BorderFactory.createEmptyBorder());
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 1, 0));
        buttonPanel.add(buyButton);
        buttonPanel.add(sellButton);
        return buttonPanel;
    }

    private JPanel createBlockSizePanel() {
        buyLabel.setOpaque(true);
        sellLabel.setOpaque(true);
        buyLabel.setHorizontalAlignment(SwingConstants.CENTER);
        sellLabel.setHorizontalAlignment(SwingConstants.CENTER);
        buyLabel.setBackground(Color.GREEN);
        sellLabel.setBackground(Color.GREEN);
        blockSizeCombo.setBorder(BorderFactory.createEtchedBorder());
        blockSizePanel.setLayout(new FlexGridLayout(new String[]{"25%", "50%", "25%"}, new String[]{"100%"}, 0, 0));
        blockSizePanel.add(buyLabel);
        blockSizePanel.add(blockSizeCombo);
        blockSizePanel.add(sellLabel);
        return blockSizePanel;
    }

    private JPanel createLowerCombinedPanel() {
        lowerCombinedPanel.setBorder(BorderFactory.createEtchedBorder(Color.GRAY, Color.LIGHT_GRAY));
        lowerCombinedPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "30"}, 0, 0));
        lowerCombinedPanel.add(createButtonPanel());
        lowerCombinedPanel.add(createBlockSizePanel());
        return lowerCombinedPanel;
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void stateChanged(ChangeEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void propertyChange(PropertyChangeEvent evt) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
