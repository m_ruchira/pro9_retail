package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TableUpdateListener;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: Chandika Hewage Date: Sep 1, 2007 Time: 12:52:29 PM To change this template use File
 * | Settings | File Templates.
 */
public class ForexBoard extends InternalFrame implements MouseListener, ActionListener, TradingConnectionListener, MouseMotionListener, Themeable, TableUpdateListener {

    public static ForexBoard self;
    static ForexBoard g_oInstance;
    private final int TOL = 3;  //tolerance
    public ViewSetting oSettings = null;
    public String watchListID = "";
    public TWComboBox portfolioCombo;
    public boolean addedToDesktop = false;
    public JPanel combinedPanel;
    ForexBoardUpdator fxUpdator;
    ArrayList<Component> components;
    Symbols symbols = null;
    ViewSetting tableSetting;
    Table table;
    private JPanel mainPanel;
    private JPopupMenu rightClickPopup;
    private TWMenuItem addNewInstrument;
    private TWMenuItem changeViewName;
    private TWMenuItem searchFXSymbol;
    private TWMenuItem removeAllInstruments;
    private TWMenuItem deleteView;
    private JPanel upperPanel;
    private JPanel separatorPanel;
    private JPanel portFolioPanel;
    private JLabel selectionLabel;
    private int MAINBOARD_LAYER = 0;
    private JPanel toolBarpanel;
    private ToolBarButton addNewBtn;
    private ToolBarButton removeAllBtn;
    private ToolBarButton searchBtn;
    //drag and drop
    private JPanel palette;
    private String adding = "";
    private FXSymbolView hitPanel;
    private int deltaX, deltaY, oldX, oldY;
    private Vector componetVec;

    public ForexBoard() {
        g_oInstance = this;
        mainPanel = new JPanel();
        combinedPanel = new JPanel();
        combinedPanel.setSize(750, 450);
        combinedPanel.setLayout(new FlexGridLayout(new String[]{"100%", "205"}, new String[]{"100%"}));
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(createPortfolioSelectionPanel(), BorderLayout.NORTH);
        getContentPane().add(combinedPanel, BorderLayout.CENTER);
        components = new ArrayList<Component>();
        mainPanel.setSize(600, 450);
        mainPanel.setPreferredSize(new Dimension(600, 450));
        mainPanel.setLayout(new KotuKotuLayout(components, 145, 160));
        combinedPanel.add(mainPanel);
        Vector test = new Vector();
        test.add("EUR-JPY");
        test.add("EUR-USD");
        test.add("USD-JPY");
        test.add("AUD-USD");
        combinedPanel.add(getNetExposureTable());
        this.setTitle(Language.getString("FOREX_BOARD"));
        this.setSize(750, 450);
        this.setVisible(true);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(true);
        this.setMaximizable(true);
        this.setLayer(GUISettings.TOP_LAYER);
        this.pack();
        fxUpdator = new ForexBoardUpdator();
        fxUpdator.start();
        mainPanel.addMouseListener(this);
        mainPanel.addMouseMotionListener(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        Theme.registerComponent(this);
        this.addInternalFrameListener(this);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static ForexBoard getInstance() {
        return g_oInstance;
    }

    public JPanel createPortfolioSelectionPanel() {
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "5"}));
        portFolioPanel = new JPanel();
        selectionLabel = new JLabel(Language.getString("PORTFOLIO"));
        portfolioCombo = new TWComboBox();
        portfolioCombo.setPreferredSize(new Dimension(100, 25));
        if (TradingShared.isConnected()) {
            tradeServerConnected();
            portfolioCombo.setEnabled(true);
        } else {
            portfolioCombo.setEnabled(false);
        }
        portFolioPanel.setLayout(new FlexGridLayout(new String[]{"85", "50", "100", "100%"}, new String[]{"25"}, 3, 3));
        portFolioPanel.add(createToolBarPanel());
        portFolioPanel.add(selectionLabel);
        portFolioPanel.add(portfolioCombo);
        portFolioPanel.add(new JLabel());
        separatorPanel = new JPanel();
        separatorPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
        separatorPanel.add(new JSeparator());
        upperPanel.add(portFolioPanel);
        upperPanel.add(separatorPanel);
        return upperPanel;

    }

    public JPanel createToolBarPanel() {
        toolBarpanel = new JPanel();
        toolBarpanel.setLayout(new FlexGridLayout(new String[]{"25", "25", "25"}, new String[]{"100%"}, 3, 0));
        addNewBtn = new ToolBarButton();
        addNewBtn.setToolTipText(Language.getString("FOREX_ADD_NEW"));
        addNewBtn.setFocusable(false);
        addNewBtn.setPreferredSize(new Dimension(25, 25));
//        addNewBtn.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        addNewBtn.addActionListener(this);
        removeAllBtn = new ToolBarButton();
        removeAllBtn.setToolTipText(Language.getString("FOREX_REMOVE_ALL"));
        removeAllBtn.setFocusable(false);
        removeAllBtn.setPreferredSize(new Dimension(25, 25));
//        removeAllBtn.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        removeAllBtn.addActionListener(this);

        searchBtn = new ToolBarButton();
        searchBtn.setToolTipText(Language.getString("SYMBOL_SEARCH"));
        searchBtn.setFocusable(false);
        searchBtn.setPreferredSize(new Dimension(25, 25));
//        removeAllBtn.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        searchBtn.addActionListener(this);

        toolBarpanel.add(addNewBtn);
        toolBarpanel.add(removeAllBtn);
        toolBarpanel.add(searchBtn);
        loadButtonImages(null);
        return toolBarpanel;
    }

    public void loadButtonImages(String id) {
        if (id != null) {
            removeAllBtn.setIcon(new ImageIcon("images/Theme" + id + "/forexremoveallbtn.gif"));
            addNewBtn.setIcon(new ImageIcon("images/Theme" + id + "/forexaddnewbtn.gif"));
            searchBtn.setIcon(new ImageIcon("images/Theme" + id + "/forexsearchbtn.gif"));
        } else {
            String sThemeID = Settings.getLastThemeID();
            addNewBtn.setIcon(new ImageIcon("images/Theme" + sThemeID + "/forexaddnewbtn.gif"));
            removeAllBtn.setIcon(new ImageIcon("images/Theme" + sThemeID + "/forexremoveallbtn.gif"));
            searchBtn.setIcon(new ImageIcon("images/Theme" + sThemeID + "/forexsearchbtn.gif"));

        }
        addNewBtn.updateUI();
        removeAllBtn.updateUI();
        searchBtn.updateUI();
    }

    /**
     * Create new FXView when clicking right click pop up default loading random currency pair
     */

    public void createNewFXView() {
        FXSymbolView fxv = new FXSymbolView(this);
        fxv.addMouseListener(this);
//        fxv.addMouseMotionListener(this);
        mainPanel.add(fxv);
        components.add(fxv);
        ForexBoardUpdator.forexSymbols.add(fxv);
        mainPanel.doLayout();
        mainPanel.repaint();
        mainPanel.updateUI();
        fxv.updateUI();
        fxv.repaint();
        table.updateUI();
        table.updateGUI();

    }

    /**
     * Creating new FXViews to symbols saved in watchlist
     *
     * @param key
     */
    public void createNewFXViewLoading(String key) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String left = symbol.substring(0, 3);
        String right = symbol.substring(3, 6);
        FXSymbolView fxv = new FXSymbolView(this, left, right);
        fxv.firstPart.removeActionListener(fxv);
        fxv.secondPart.removeActionListener(fxv);
        fxv.setInitialSymbol(left, right);
        fxv.addMouseListener(this);
//        fxv.addMouseMotionListener(this);
        fxv.firstPart.addActionListener(fxv);
        fxv.secondPart.addActionListener(fxv);
        mainPanel.add(fxv);
        components.add(fxv);
        ForexBoardUpdator.forexSymbols.add(fxv);
        mainPanel.doLayout();
        mainPanel.repaint();
        mainPanel.updateUI();
        fxv.updateUI();
        fxv.repaint();
        addSymbolsForWatchlistStore(key, watchListID);
        DataStore.getSharedInstance().addSymbolRequest("FORX", symbol, Meta.INSTRUMENT_FOREX);
        table.updateUI();
        table.updateGUI();
    }

    public void createNewFXViewFromSelection(String key) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String left = symbol.substring(0, 3);
        String right = symbol.substring(3, 6);
        FXSymbolView fxv = new FXSymbolView(this, left, right);
        fxv.firstPart.removeActionListener(fxv);
        fxv.secondPart.removeActionListener(fxv);
        fxv.setInitialSymbol(left, right);
        fxv.addMouseListener(this);
        fxv.updateUI();
        fxv.repaint();
        fxv.firstPart.addActionListener(fxv);
        fxv.secondPart.addActionListener(fxv);
        mainPanel.add(fxv);
        components.add(fxv);
        ForexBoardUpdator.forexSymbols.add(fxv);
        mainPanel.doLayout();
        mainPanel.repaint();
        mainPanel.updateUI();
        table.updateUI();
        table.updateGUI();

    }

    /**
     * removing symbol as well as FXView from ForexBoard
     *
     * @param fxview
     */
    public void removeFXView(FXSymbolView fxview) {
        mainPanel.remove(fxview);
        components.remove(fxview);
        ForexBoardUpdator.forexSymbols.remove(fxview);
        mainPanel.doLayout();
        mainPanel.repaint();
        mainPanel.updateUI();
        fxview.updateUI();
        fxview.repaint();
        DataStore.getSharedInstance().removeSymbolRequest(fxview.key, Meta.SYMBOL_TYPE_FOREX);
        removeSymbolsFromWatchListStore(fxview.key);
        table.updateUI();
        table.updateGUI();
    }

    public ViewSetting getViewSetting() {
        return this.oSettings;
    }

    public void setViewSetting(ViewSetting oSetting) {
        this.oSettings = oSetting;
        try {
            this.oSettings.setLayer(this.getLayer());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setWatchListID(String watchListID) {
        this.watchListID = watchListID;

    }

    public void createPopupMenu() {
        rightClickPopup = new JPopupMenu("Right Click");
        addNewInstrument = new TWMenuItem(Language.getString("FOREX_ADD_NEW"));
        changeViewName = new TWMenuItem(Language.getString("EDIT_VIEW"));
        removeAllInstruments = new TWMenuItem(Language.getString("FOREX_REMOVE_ALL"));
        deleteView = new TWMenuItem(Language.getString("DELETE_VIEW"));
        searchFXSymbol = new TWMenuItem(Language.getString("SEARCH_NEW_INSTRUMENT"));
        rightClickPopup.add(addNewInstrument);
        rightClickPopup.add(searchFXSymbol);
        rightClickPopup.add(removeAllInstruments);
        rightClickPopup.add(new JSeparator());
        rightClickPopup.add(changeViewName);
        rightClickPopup.add(deleteView);
        deleteView.addActionListener(this);
        searchFXSymbol.addActionListener(this);
        addNewInstrument.addActionListener(this);
        changeViewName.addActionListener(this);
        removeAllInstruments.addActionListener(this);
        loadPopupImages();
        GUISettings.applyOrientation(rightClickPopup);
    }

    public void loadPopupImages() {
        String sThemeID = Theme.getID();
        try {
            addNewInstrument.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexAddNew.gif"));
            addNewInstrument.updateUI();
            changeViewName.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexChangeName.gif"));
            changeViewName.updateUI();
            removeAllInstruments.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexRemoveAll.gif"));
            removeAllInstruments.updateUI();
            deleteView.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexDeleteView.gif"));
            deleteView.updateUI();
            searchFXSymbol.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexSearch.gif"));
            searchFXSymbol.updateUI();
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
        sThemeID = null;
    }

    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            createPopupMenu();
            rightClickPopup.show(e.getComponent(), e.getX(), e.getY());
        }
//        else if(e.getSource().equals(table)){
//            if(e.getClickCount()>1){
//                Client.getInstance().showDetailQuote(SharedMethods.getKey("FORX",(String)table.getModel().getValueAt(table.getSelectedRow(),0),Meta.INSTRUMENT_FOREX));
//            }
//        }

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addNewInstrument) {
            createNewFXView();
//            createForexSymbolView();

        }
        if (e.getSource() == changeViewName) {
            Client.getInstance().setFXBoardSelected(this);
            Client.getInstance().editView();
        }
        if (e.getSource() == removeAllInstruments) {
            removeAllInstruments();
//           removeAllSymbols();
        }
        if (e.getSource() == deleteView) {
            Client.getInstance().setFXBoardSelected(this);
            Client.getInstance().g_mnuTreePopDelete_Selected(e);
        }
        if (e.getSource() == addNewBtn) {
            createNewFXView();
        }
        if (e.getSource() == removeAllBtn) {
            if (components.size() >= 1) {
                removeAllInstruments();
            }
//           removeAllSymbols();
        }
        if (e.getSource().equals(searchBtn)) {
            ForexSelectionFrame.initialize(this);
//            Client.getInstance().getDesktop().add(ForexSelectionFrame.getSharedInstance());
            ForexSelectionFrame.getSharedInstance().setParent(this);
            ForexSelectionFrame.getSharedInstance().setLayer(GUISettings.TOP_LAYER);
            ForexSelectionFrame.getSharedInstance().show();
            ForexSelectionFrame.getSharedInstance().setLocationRelativeTo(Client.getInstance().getDesktop());
        }
        if (e.getSource().equals(searchFXSymbol)) {
            ForexSelectionFrame.initialize(this);

            ForexSelectionFrame.getSharedInstance().setParent(this);
            ForexSelectionFrame.getSharedInstance().setLayer(GUISettings.TOP_LAYER);
            ForexSelectionFrame.getSharedInstance().show();
            ForexSelectionFrame.getSharedInstance().setLocationRelativeTo(Client.getInstance().getDesktop());
        }

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        if ((ForexSelectionFrame.getSharedInstance() != null) && (ForexSelectionFrame.getSharedInstance().isVisible()) && ((ForexSelectionFrame.getSharedInstance().getForexBoard()).equals(this))) {
            ForexSelectionFrame.getSharedInstance().dispose();
        }


    }

    public void applySettings() {
        this.setSize(oSettings.getSize());
        if (oSettings != null)
            this.setLocation(oSettings.getLocation());
        try {
            if (oSettings.isVisible()) {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings.getIndex());
                } else {
                    this.getDesktopPane().setLayer(this, this.getLayer(), oSettings.getIndex());
                }
                this.setVisible(true);
                try {
                    this.setSelected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                    this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                } else {
                    this.getDesktopPane().setLayer(this, this.getLayer(), 0);
                }
            }
        } catch (Exception e) {
            this.setVisible(oSettings.isVisible());
        }
        try {
            switch (oSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    if (oSettings != null) {
                        this.setSize(oSettings.getSize());
                        this.setLocation(oSettings.getLocation());
                    }
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if (!oSettings.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            } else {
                updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setWindowTitle();
    }

    /**
     * Sets symbol list for the table
     */
    public void setSymbols(Symbols oSymbols, boolean addToregister) {
        /* If the symbols object is already registered, remove it.
            This may happen due to a previouse workspace activity */
        if (symbols != null) {// (!isCustomType()))
            SymbolsRegistry.getSharedInstance().unregister(symbols, isCustomType());
        }
        symbols = oSymbols;
        /* register the new symbols object */
//        if ((!isCustomType()) && (addToregister))
        if (addToregister) {
            SymbolsRegistry.getSharedInstance().register(oSymbols, isCustomType());
        }

    }

    /**
     * Returns symbol list for the table
     */
    public Symbols getSymbols() {
        return symbols;
    }

    /**
     * watchlist store related methods
     */
    public void setSymbols(Symbols oSymbols) {
        setSymbols(oSymbols, true);
    }

    /**
     * Checks if the  type is custom for this table
     */
    public boolean isCustomType() {
        return oSettings.isCutomType();
    }

    /**
     * Trying to add Symbols to watchlists
     */
    public void getSymbolsFromWatchList(WatchListStore store) {
        try {
            String[] symbols = store.getSymbols();
            for (int i = 0; i < symbols.length; i++) {
                createNewFXViewLoading(symbols[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void addSymbolsForWatchlistStore(String symbol, String watchlist) {
        try {
            WatchListStore store = WatchListManager.getInstance().getStore(watchlist);
            store.appendSymbol(symbol);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void removeSymbolsFromWatchListStore(String symbol) {
        try {
            WatchListStore store = WatchListManager.getInstance().getStore(watchListID);
            store.removeSymbol(symbol);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public void tradeServerConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            if (isForexSupportedPF(record.getExchangeString())) {
                portfolioCombo.addItem(new TWComboItem(record.getPortfolioID(), record.getName()));
            }

        }
        portfolioCombo.setEnabled(true);
        try {
            table.updateUI();
            table.updateGUI();
            table.revalidate();
        } catch (Exception e) {
        }
        /*
          ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            if (isForexSupportedPF(record.getExchangeString())) {
            portfolioCombo.addItem(new TWComboItem(record.getPortfolioID(), record.getName()));
            }

        }
        if ((portfolioCombo != null) && (typeCombo != null)) {
            portfolioCombo.setEnabled(true);
            typeCombo.setEnabled(true);
            requestCancelBtn.setEnabled(true);
        }
         */
    }

    public boolean isForexSupportedPF(String exchangeList) {
        try {
            String[] arr = exchangeList.split(",");
            Hashtable<String, String> temp = new Hashtable<String, String>();
            for (int i = 0; i < arr.length; i++) {
                temp.put(arr[i], arr[i]);
            }
            if (temp.containsKey("FORX")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            if (isForexSupportedPF(record.getExchangeString())) {
                portfolioCombo.addItem(new TWComboItem(record.getPortfolioID(), record.getName()));
            }

        }
        portfolioCombo.setEnabled(true);
        table.updateUI();
        table.updateGUI();
        table.revalidate();
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        portfolioCombo.setEnabled(false);
        portfolioCombo.removeAllItems();
    }

    public void setWindowTitle() {
        String arr[] = oSettings.getCaptions().split(",");
        String title = Language.getString("FOREX_BOARD");
        title = title + " - " + arr[0];
        this.setTitle(title);
        title = null;
        arr = null;
    }

    public void updateGUI() {
        setWindowTitle();
        this.applySettings();
        this.updateUI();
    }

    public void removeAllSymbols() {
        try {
            for (int i = 0; i < components.size(); i++) {
                FXSymbolView fxv = (FXSymbolView) components.get(i);
                removeFXV(fxv);

            }
            components.clear();
        } catch (Exception e) {
            System.out.println("Removing all Symbols failed...");
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeFXV(FXSymbolView fxview) {
        mainPanel.remove(fxview);
        ForexBoardUpdator.forexSymbols.remove(fxview);
        mainPanel.doLayout();
        mainPanel.repaint();
        mainPanel.updateUI();
        fxview.updateUI();
        fxview.repaint();
        DataStore.getSharedInstance().removeSymbolRequest(fxview.key, Meta.SYMBOL_TYPE_FOREX);
        removeSymbolsFromWatchListStore(fxview.key);
    }

    private void removeAllInstruments() {
        final StringBuffer result = new StringBuffer();
        TWButton ok = new TWButton(Language.getString("OK"));
        TWButton cancel = new TWButton(Language.getString("CANCEL"));

        Object options[] = {ok, cancel};
        JOptionPane pane = new JOptionPane();
        pane.setMessage(Language.getString("FX_REMOVE_ALL"));
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
        final JDialog removeDialog = pane.createDialog(Client.getInstance().getFrame(), Language.getString("WARNING"));
        removeDialog.setModal(true);
        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeAllSymbols();
                removeDialog.dispose();
            }
        });


        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeDialog.dispose();
            }
        });

        removeDialog.setVisible(true);

    }

    private Table getNetExposureTable() {
        tableSetting = ViewSettingsManager.getSummaryView("FX_NETVAL");
        table = new Table() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
//                    Client.getInstance().showDetailQuote(SharedMethods.getKey("FORX",(String)table.getModel().getValueAt(table.getSelectedRow(),0),Meta.INSTRUMENT_FOREX));


                }
            }
        };
        table.addMouseListener(this);
        FXNetValModel model = new FXNetValModel(components);
        model.setViewSettings(tableSetting);
        table.setModel(model);
        model.setTable(table);
        /*
                tablePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        ipoTable = new Table();
        ipoTable.setPreferredSize(new Dimension(650, 290));
        tableModel = new IPOTableModel();
        try {
            ipoTable.getPopup().setMenuItem(getCopyMenu());
            ipoTable.getPopup().setMenuItem(getCopyWithHeadMenu());
            ipoTable.getPopup().setMenuItem(getIPOCancelMenu());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ipoTable.setWindowType(ViewSettingsManager.IPO_SUBSCRIPTION);
        tableModel.setViewSettings(oSetting);
        ipoTable.setModel(tableModel);
        tableModel.setTable(ipoTable);
        ((SmartTable) ipoTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        tableModel.applyColumnSettings();
        tableModel.updateGUI();
        oSetting.setParent(this);
        setLayer(GUISettings.TOP_LAYER);
        tablePanel.add(ipoTable);
        super.setTable(ipoTable);
        return tablePanel;
         */
        return table;

    }

    public ArrayList removeDuplicateRecords(ArrayList arrayList) {
        try {
            Hashtable temp = new Hashtable();
            ArrayList tempArray = new ArrayList();
            for (int i = 0; i < arrayList.size(); i++) {
                temp.put(((FXSymbolView) arrayList.get(i)).key, arrayList.get(i));
            }
            Enumeration tempE = temp.elements();
            while (tempE.hasMoreElements()) {
                tempArray.add(tempE.nextElement());
            }
            return tempArray;
        } catch (Exception e) {
            return new ArrayList();
        }
    }

    public void mouseDragged(final MouseEvent e) {
        if (hitPanel != null) {
            int x = e.getX();
            int y = e.getY();
            int xDiff = x - oldX;
            int yDiff = y - oldY;
            int xH = hitPanel.getX();
            int yH = hitPanel.getY();
            int w = hitPanel.getWidth();
            int h = hitPanel.getHeight();
            Dimension min = hitPanel.getMinimumSize();
            Dimension max = hitPanel.getMaximumSize();
            int wMin = (int) min.getWidth();
            int wMax = (int) max.getWidth();
            int hMin = (int) min.getHeight();
            int hMax = (int) max.getHeight();
            int cursorType = hitPanel.getCursor().getType();
            if (cursorType == Cursor.W_RESIZE_CURSOR) {           //West resizing
                if ((w <= wMin && xDiff > 0) || (w >= wMax && xDiff < 0)) return;
                hitPanel.setBounds(x, yH, w - xDiff, h);
            } else if (cursorType == Cursor.N_RESIZE_CURSOR) {     //North resizing
                if ((h <= hMin && yDiff > 0) || (h >= hMax && yDiff < 0)) return;
                hitPanel.setBounds(xH, y, w, h - yDiff);
            } else if (cursorType == Cursor.S_RESIZE_CURSOR) {     //South resizing
                if ((h <= hMin && yDiff < 0) || (h >= hMax && yDiff > 0)) return;
                hitPanel.setSize(w, h + yDiff);
            } else if (cursorType == Cursor.E_RESIZE_CURSOR) {     //East resizing
                if ((w <= wMin && xDiff < 0) || (w >= wMax && xDiff > 0)) return;
                hitPanel.setSize(w + xDiff, h);
            } else if (cursorType == Cursor.NW_RESIZE_CURSOR) {     //NorthWest resizing
                if ((h <= hMin && yDiff > 0) || (h >= hMax && yDiff < 0)) return;
                if ((w <= wMin && xDiff > 0) || (w >= wMax && xDiff < 0)) return;
                hitPanel.setBounds(x, y, w - xDiff, h - yDiff);
            } else if (cursorType == Cursor.NE_RESIZE_CURSOR) {     //NorthEast resizing
                if ((h <= hMin && yDiff > 0) || (h >= hMax && yDiff < 0)) return;
                if ((w <= wMin && xDiff < 0) || (w >= wMax && xDiff > 0)) return;
                hitPanel.setBounds(xH, y, w + xDiff, h - yDiff);
            } else if (cursorType == Cursor.SW_RESIZE_CURSOR) {     //SouthWest resizing
                if ((h <= hMin && yDiff < 0) || (h >= hMax && yDiff > 0)) return;
                if ((w <= wMin && xDiff > 0) || (w >= wMax && xDiff < 0)) return;
                hitPanel.setBounds(x, yH, w - xDiff, h + yDiff);
            } else if (cursorType == Cursor.SE_RESIZE_CURSOR) {     //SouthEast resizing
                if ((h <= hMin && yDiff < 0) || (h >= hMax && yDiff > 0)) return;
                if ((w <= wMin && xDiff < 0) || (w >= wMax && xDiff > 0)) return;
                hitPanel.setSize(w + xDiff, h + yDiff);
            } else {          //moving subpanel
                hitPanel.setLocation(x - deltaX, y - deltaY);
            }
            oldX = e.getX();
            oldY = e.getY();

        }
    }

    public void mouseMoved(final MouseEvent e) {
        Component c = mainPanel.getComponentAt(e.getPoint());
        if (c instanceof FXSymbolView) {
            int x = e.getX();
            int y = e.getY();
            int xC = c.getX();
            int yC = c.getY();
            int w = c.getWidth();
            int h = c.getHeight();
            if (y >= yC - TOL && y <= yC + TOL && x >= xC - TOL && x <= xC + TOL) {
                c.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
            } else if (y >= yC - TOL && y <= yC + TOL && x >= xC - TOL + w && x <= xC + TOL + w) {
                c.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
            } else if (y >= yC - TOL + h && y <= yC + TOL + h && x >= xC - TOL && x <= xC + TOL) {
                c.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
            } else if (y >= yC - TOL + h && y <= yC + TOL + h && x >= xC - TOL + w && x <= xC + TOL + w) {
                c.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
            } else if (x >= xC - TOL && x <= xC + TOL) {
                c.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
            } else if (y >= yC - TOL && y <= yC + TOL) {
                c.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
            } else if (x >= xC - TOL + w && x <= xC + TOL + w) {
                c.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
            } else if (y >= yC - TOL + h && y <= yC + TOL + h) {
                c.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
            } else {
                c.setCursor(new Cursor(Cursor.MOVE_CURSOR));
            }
        }
    }

    public void mouseEntered(final MouseEvent e) {
    }

    public void mouseExited(final MouseEvent e) {
    }

    public void mouseReleased(final MouseEvent e) {
        hitPanel = null;

    }

    public void mousePressed(final MouseEvent e) {
        try {
            Component c = (FXSymbolView) mainPanel.getComponentAt(SwingUtilities.convertMouseEvent((FXSymbolView) e.getSource(), e, mainPanel).getPoint());
            if (c instanceof FXSymbolView) {
                hitPanel = (FXSymbolView) c;
                oldX = hitPanel.getX();
                oldY = hitPanel.getY();
                deltaX = e.getX() - oldX;
                deltaY = e.getY() - oldY;
                if (oldX < e.getX() - TOL) oldX += hitPanel.getWidth();
                if (oldY < e.getY() - TOL) oldY += hitPanel.getHeight();
            }
        } catch (Exception e1) {
        }
    }

    public void applyTheme(String iID) {
        loadButtonImages(iID);
    }

    public void tableUpdating() {
        //To change body of implemented methods use File | Settings | File Templates.
        this.updateGUI();
        this.updateUI();
    }
}
