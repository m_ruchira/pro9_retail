package com.isi.csvr.forex;

import com.isi.csvr.table.TWFormattedTextField;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 17-Jun-2008 Time: 16:49:26 To change this template use File | Settings
 * | File Templates.
 */
public class ForexSpiner extends JSpinner implements ChangeListener, PropertyChangeListener {
    private TWFormattedTextField textField;
    private NumberFormat amountFormat;
    private SpinnerNumberModel model;
    private double amount = 100000;

    public ForexSpiner() {
        model = new SpinnerNumberModel(100000, 100000, null, 50000);
        this.setModel(model);
        amountFormat = NumberFormat.getNumberInstance();
        textField = new TWFormattedTextField(amountFormat);
        textField.setColumns(10);
        textField.setValue(new Double(amount));
        textField.setHorizontalAlignment(SwingConstants.RIGHT);
        textField.setEditable(true);
        textField.setBorder(BorderFactory.createEmptyBorder());
        this.setEditor(textField);
        textField.setBackground(Color.white);
        textField.setBorder(BorderFactory.createEmptyBorder());
        textField.addPropertyChangeListener("value", this);
        this.addChangeListener(this);
    }

    public void setUnitIncrement(int step) {
        model.setStepSize(step);
    }

    public void setDefault(int defaultVal) {
        model.setValue(defaultVal);
    }

    public void stateChanged(ChangeEvent e) {
        if (e.getSource() == this) {
            textField.setValue(this.getValue());
        }
    }

    public String getText() {
        return blockSizeTrimer(textField.getText());
    }

    public void setText(Double text) {
        textField.setValue(text);
    }

    public void propertyChange(PropertyChangeEvent evt) {
        Object source = evt.getSource();
        if (source.equals(textField)) {
            amount = ((Number) textField.getValue()).doubleValue();
        }
    }

    public String blockSizeTrimer(String longString) {
        String splitpatern = ",";
        String[] parts = longString.split(splitpatern);
        String blocksize = "";
        for (int i = 0; i < parts.length; i++) {
            blocksize = blocksize + parts[i];
        }
        return blocksize.trim();
    }
}
