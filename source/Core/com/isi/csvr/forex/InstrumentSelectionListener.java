package com.isi.csvr.forex;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * Created by IntelliJ IDEA. User: admin Date: 12-Sep-2007 Time: 14:19:17 To change this template use File | Settings |
 * File Templates.
 */
public class InstrumentSelectionListener implements TreeSelectionListener {


    public void valueChanged(TreeSelectionEvent e) {
        TreePath path = e.getPath();
        Object[] nodes = path.getPath();
        String oid = "";
        for (int k = 0; k < nodes.length; k++) {
            DefaultMutableTreeNode node =
                    (DefaultMutableTreeNode) nodes[k];
            ForexNode nd = (ForexNode) node.getUserObject();
            oid += "." + nd.getName();
        }
        System.out.println("Node name : " + oid);
    }

}
