package com.isi.csvr.forex;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWFont;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Sep 1, 2007
 * Time: 12:52:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class FXButton extends JButton {
    public static ImageIcon img;
    public static ImageIcon imgUp;
    public static ImageIcon imgDown;
    public static ImageIcon imgPressed;
    public double buySellVal;
    String buttonType;
    String value;
    FontMetrics fontMetrics;
    double previousVal = 0.00000;
    int upDownTracker = 0;
    Color fxColor = new Color(185, 249, 185);
    String pips = "4";
    private double baseValue;
    private long appender;
    private String base = "0.00";
    private String factorial = "00";
    private String fifth = "0";
    private FXObject fxo;

    public FXButton(String type, String key) {
        super();
        img = new ImageIcon("images/common/fxbutton.gif");
        imgUp = new ImageIcon("images/common/fxUp.gif");
        imgDown = new ImageIcon("images/common/fxDown.gif");
        imgPressed = new ImageIcon("images/common/fxPressed.gif");
        this.buttonType = type;

    }

    public void valueSeparate(double buysell) {
        if (previousVal > buysell) {
            upDownTracker = 1;
        } else if (previousVal < buysell) {
            upDownTracker = 2;
        }
        if (buysell != 0l) {
            char[] array = getPriceArray("" + buysell);
            String newBuySell = getPriceSrting("" + buysell);
            if (array.length >= 4) {
                this.base = newBuySell.substring(0, 4);
            }
            if (array.length >= 6) {
                this.factorial = newBuySell.substring(4, 6);
            }
            if (array.length >= 7) {
                this.fifth = newBuySell.substring(6, 7);
            }
//            String buysellString=""+buysell;
//            char[] array={'0','0','0','0','0','0','0'};
//            char[] buysellArray=buysellString.toCharArray();
//            for(int i=0;i<buysellArray.length;i++){
//                 array[i]=buysellArray[i];
//            }
//            String newBuySell="";
//            for(int i=0;i<array.length;i++){
//                newBuySell=newBuySell+array[i];
//
//            }
            if (array.length >= 4) {
                this.base = newBuySell.substring(0, 4);
            }
            if (array.length >= 6) {
                this.factorial = newBuySell.substring(4, 6);
            }
            if (array.length >= 7) {
                this.fifth = newBuySell.substring(6, 7);
            }
//            if(buysellString.toCharArray().length>=4){
//            this.base=buysellString.substring(0,4);
//            }
//            if(buysellString.toCharArray().length>=6){
//            this.factorial=buysellString.substring(4,6);
//            }
//            if(buysellString.toCharArray().length>=7){
//            this.fifth =buysellString.substring(6,7);
//            }


        } else {
            this.base = "0.00";
            this.factorial = "00";
            this.fifth = "0";
        }
        previousVal = buysell;

    }

    public String getPriceSrting(String buySell) {
        char[] buysellArray = buySell.toCharArray();
        char[] array = {'0', '0', '0', '0', '0', '0', '0'};
        String newBuySell = "";
        if (buysellArray.length <= 7) {
            for (int i = 0; i < buysellArray.length; i++) {
                array[i] = buysellArray[i];
            }
            for (int i = 0; i < array.length; i++) {
                newBuySell = newBuySell + array[i];
            }
            return newBuySell;
        } else {
            for (int i = 0; i < 7; i++) {
                array[i] = buysellArray[i];
            }
            for (int i = 0; i < array.length; i++) {
                newBuySell = newBuySell + array[i];
            }
            return newBuySell;

        }
    }

    public char[] getPriceArray(String buySell) {
        char[] buysellArray = buySell.toCharArray();
        char[] array = {'0', '0', '0', '0', '0', '0', '0'};
        if (buysellArray.length <= 7) {
            for (int i = 0; i < buysellArray.length; i++) {
                array[i] = buysellArray[i];
            }
            return array;
        } else {
            for (int i = 0; i < 7; i++) {
                array[i] = buysellArray[i];
            }
            return array;
        }
    }

    public void paint(Graphics g) {
        super.paint(g);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//        g.drawImage(imgPressed.getImage(), 1, 1, imgPressed.getIconWidth(), imgPressed.getIconHeight(), this);

        if (getModel().isArmed() && getModel().isPressed()) {
            //painting the pressed image
//            g.drawImage(imgPressed.getImage(), 1, 1, imgPressed.getIconWidth(), imgPressed.getIconHeight(), this);

            this.setBackground(fxColor);
            int buttonWidth = getPreferredSize().width;
            int buttonHeight = getPreferredSize().height;
            fontMetrics = getFontMetrics(getFont());
            int stringWidth = fontMetrics.stringWidth(buttonType);
            int y = buttonHeight / 2 + 20;
            int x = (buttonWidth - stringWidth) / 2;
            g.drawString(buttonType, x, y);

            //painting normal value at top left hand side....
            g.setFont(new TWFont("ARIAL", 1, 14));
            g.drawString(base, 5, 15);

            //painting big and small decimals
            g.setFont(new TWFont("ARIAL", 1, 24));
            int bigWidth = getFontMetrics(g.getFont()).stringWidth("56");
            g.setFont(new TWFont("ARIAL", 1, 14));
            int smallWidth = getFontMetrics(g.getFont()).stringWidth("1");
            int xbig = (buttonWidth - bigWidth - smallWidth) / 2;
            int bigHeight = y + 22;
            g.setFont(new TWFont("ARIAL", 1, 24));
            g.drawString(factorial, xbig, bigHeight);
            g.setFont(new TWFont("ARIAL", 1, 14));
            int smallx = (xbig) + bigWidth;
            g.drawString(fifth, smallx, bigHeight);

        } else {

            g.drawImage(imgPressed.getImage(), 1, 1, imgPressed.getIconWidth(), imgPressed.getIconHeight(), this);
            g.drawImage(img.getImage(), 4, 30, img.getIconWidth(), img.getIconHeight(), this);
            if (upDownTracker == 1) { //down

                g.drawImage(imgDown.getImage(), 45, 3, imgDown.getIconWidth(), imgDown.getIconHeight(), this);
            } else if (upDownTracker == 2) {                //up
                g.drawImage(imgUp.getImage(), 45, 2, imgUp.getIconWidth(), imgUp.getIconHeight(), this);
            }

            //painting buy sell
            int buttonWidth = getPreferredSize().width;
            int buttonHeight = getPreferredSize().height;
            fontMetrics = getFontMetrics(getFont());
            int stringWidth = fontMetrics.stringWidth(buttonType);
            int y = buttonHeight / 2 + 20; //25
            int x = (buttonWidth - stringWidth) / 2;
            g.drawString(buttonType, x, y);

            //painting normal value at top left hand side....
            g.setFont(new TWFont("ARIAL", 1, 14));
            g.drawString(base, 5, 15);

            //painting big and small decimals
            g.setFont(new TWFont("ARIAL", 1, 24));
            int bigWidth = getFontMetrics(g.getFont()).stringWidth("56");
            g.setFont(new TWFont("ARIAL", 1, 14));
            int smallWidth = getFontMetrics(g.getFont()).stringWidth("1");
            int xbig = (buttonWidth - bigWidth - smallWidth) / 2;
            int bigHeight = y + 22;  //27
            g.setFont(new TWFont("ARIAL", 1, 24));
            g.drawString(factorial, xbig, bigHeight);
            g.setFont(new TWFont("ARIAL", 1, 14));
            int smallx = (xbig) + bigWidth;
            g.drawString(fifth, smallx, bigHeight);
        }


    }

    public void update(String key, long blockSize) {
        this.pips = (String) FXSymbolView.pipHash.get(key);
        fxo = ForexStore.getSharedInstance().getForexObject(key);
        if (fxo != null) {

            if (this.buttonType.equals(Language.getString("TRADE_SIDE_BUY"))) {
                this.buySellVal = fxo.getSellValForGivenBlock(fxo.getBlockSizeForGivenQuantity(blockSize));
            } else {
                this.buySellVal = fxo.getBuyValForGivenBlock(fxo.getBlockSizeForGivenQuantity(blockSize));
            }
            this.value = value;
            if (this.buySellVal != 0l) {
                valueSeparate(this.buySellVal);
            } else {
                this.base = "0.00";
                this.factorial = "00";
                this.fifth = "0";
            }


        } else {
            this.base = "0.00";
            this.factorial = "00";
            this.fifth = "0";
        }

        this.repaint();

    }

    public void update(String key, long quantity, boolean isMarketOrder) {
        //this.pips=(String)FXSymbolView.pipHash.get(key);
        fxo = ForexStore.getSharedInstance().getForexObject(key);
        if (fxo != null) {
            if (this.buttonType.equals(Language.getString("TRADE_SIDE_BUY"))) {
                this.buySellVal = fxo.getSellValForGivenBlock(fxo.getBlockSizeForGivenQuantity(quantity));
            } else {
                this.buySellVal = fxo.getBuyValForGivenBlock(fxo.getBlockSizeForGivenQuantity(quantity));
            }
            this.value = value;
            if (this.buySellVal != 0l) {
                valueSeparate(this.buySellVal);
            } else {
                this.base = "0.00";
                this.factorial = "00";
                this.fifth = "0";
            }


        } else {
            this.base = "0.00";
            this.factorial = "00";
            this.fifth = "0";
        }

        this.repaint();

    }
}
