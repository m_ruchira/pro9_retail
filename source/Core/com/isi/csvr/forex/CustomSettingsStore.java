package com.isi.csvr.forex;

import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 18-Jun-2008 Time: 10:09:28 To change this template use File | Settings
 * | File Templates.
 */
public class CustomSettingsStore {
    public static CustomSettingsStore self;
    public Hashtable<String, FXSettingsObject> settingsStore;
    private String key;
    private String symbol;
    private String exchange;
    private int instrumentType;

    private CustomSettingsStore() {
        settingsStore = new Hashtable<String, FXSettingsObject>();
    }

    public static CustomSettingsStore getSharedInstance() {
        if (self == null) {
            self = new CustomSettingsStore();
        }
        return self;
    }

    public FXSettingsObject getSettingsObject(String key) {
        FXSettingsObject fxso = (FXSettingsObject) settingsStore.get(key);
        if (fxso != null) {
            return fxso;
        } else {
            fxso = new FXSettingsObject(new long[]{100000, 200000, 300000, 400000, 500000, 600000});
            putSettingsObject(key, fxso);
            return fxso;
        }

    }

    public void putSettingsObject(String key, FXSettingsObject fxso) {
        settingsStore.put(key, fxso);
    }

    public boolean isKeyContains(String key) {
        return settingsStore.containsKey(key);
    }

    public void save() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath() + "datastore/fxsettings.mdf"));
            out.writeObject(settingsStore);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath() + "datastore/fxsettings.mdf"));
            settingsStore = (Hashtable<String, FXSettingsObject>) in.readObject();
        } catch (Exception e) {
            System.out.println("datastore/fxsettings.mdf not found");
//            e.printStackTrace();
        }
    }
}
