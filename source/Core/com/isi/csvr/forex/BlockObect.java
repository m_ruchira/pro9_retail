package com.isi.csvr.forex;


/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Sep 1, 2007
 * Time: 12:52:29 PM
 */

public class BlockObect {

    //modified
    public String buyQuoteId;
    public String sellQuoteId;
    public String buyBrokerId;
    public String sellBrokerId;
    public double buyValue;
    public double sellValue;
    /**
     * This object keeps the internal records of the
     * forex object such as quote_id,base value and appender
     * with repect to block size
     */

    private long forexBlockSize;
    private String forexBaseValue;
    private String forexAppender;
    private String forexQuoteID;

    public BlockObect() {

    }

    public long getForexBlockSize() {
        return forexBlockSize;
    }

    public void setForexBlockSize(long forexBlockSize) {
        this.forexBlockSize = forexBlockSize;
    }

    public String getForexBaseValue() {
        return forexBaseValue;
    }

    public void setForexBaseValue(String forexBaseValue) {
        this.forexBaseValue = forexBaseValue;
    }

    public String getForexAppender() {
        return forexAppender;
    }

    public void setForexAppender(String forexAppender) {
        this.forexAppender = forexAppender;
    }

    public String getForexQuoteID() {
        return forexQuoteID;
    }

    public void setForexQuoteID(String forexQuoteID) {
        this.forexQuoteID = forexQuoteID;
    }

    public String getBuyQuoteId() {
        return buyQuoteId;
    }

    public void setBuyQuoteId(String buyQuoteId) {
        this.buyQuoteId = buyQuoteId;
    }

    public String getSellQuoteId() {
        return sellQuoteId;
    }

    public void setSellQuoteId(String sellQuoteId) {
        this.sellQuoteId = sellQuoteId;
    }

    public String getBuyBrokerId() {
        return buyBrokerId;
    }

    public void setBuyBrokerId(String buyBrokerId) {
        this.buyBrokerId = buyBrokerId;
    }

    public String getSellBrokerId() {
        return sellBrokerId;
    }

    public void setSellBrokerId(String sellBrokerId) {
        this.sellBrokerId = sellBrokerId;
    }

    public double getBuyValue() {
        return buyValue;

    }

    public void setBuyValue(double buyValue) {
        this.buyValue = buyValue;
    }

    public double getSellValue() {
        return sellValue;
    }

    public void setSellValue(double sellValue) {
        this.sellValue = sellValue;
    }
}
