package com.isi.csvr.forex;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 07-Jan-2008 Time: 14:25:38 To change this template use File | Settings
 * | File Templates.
 */
public class FXTreeRenderer implements TreeCellRenderer {
    private static ImageIcon g_oLeafIcon_sub = null;
    private static ImageIcon g_oLeafIcon_unSub = null;
    private static ImageIcon g_oNewIcon = null;
    private static ImageIcon g_oClosedIcon = null;
    private static ImageIcon g_oExpandedIcon = null;
    JLabel titleLabel;
    JLabel renderer;
    DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    Color backgroundSelectionColor;
    Color backgroundNonSelectionColor;
    Color unsubscribedForgoundColor;
    String path = System.getProperties().getProperty("user.dir").replaceAll("\\\\", "/");

    public FXTreeRenderer() {
        renderer = new JLabel("");
        renderer.setForeground(Color.blue);
        backgroundSelectionColor = defaultRenderer.getBackgroundSelectionColor();
        backgroundNonSelectionColor = defaultRenderer.getBackgroundNonSelectionColor();

        try {
            g_oLeafIcon_sub = new ImageIcon("images/common/tick.gif");
            g_oLeafIcon_unSub = new ImageIcon("images/common/untick.gif");
            g_oNewIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeLeafNew.gif");
            g_oClosedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderClosed.gif");
            g_oExpandedIcon = new ImageIcon("images/theme" + Theme.getID() + "/TreeFolderOpened.gif");
            unsubscribedForgoundColor = Theme.getColor("TAB_PANE_UNSELECTED_FGCOLOR");
        } catch (Exception e) {

        }
    }


    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean selected, boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {
        Component returnValue = null;
        if ((value != null) && (value instanceof DefaultMutableTreeNode)) {
            Object userObject = ((DefaultMutableTreeNode) value).getUserObject();
            if (userObject instanceof FXTreeObject) {
                FXTreeObject fxto = (FXTreeObject) userObject;
                renderer.setForeground(Color.GREEN);
                renderer.setIcon(g_oNewIcon);
                renderer.setText("<HTML><font color=black>" + " " + fxto.getFXName()); //<img SRC='file:"+path+"/images/common/tick.gif'>
                if (selected) {
                    renderer.setBackground(backgroundSelectionColor);
                } else {
                    renderer.setBackground(backgroundNonSelectionColor);
                }
                renderer.setEnabled(tree.isEnabled());
                returnValue = renderer;
            } else if (userObject instanceof ForexVector) {
                ForexVector nv = (ForexVector) userObject;
                if (expanded) {
                    renderer.setIcon(g_oExpandedIcon);
                    renderer.setText("<HTML><B><font color=black>" + " " + nv.toString());

                } else {
                    renderer.setIcon(g_oClosedIcon);
                    renderer.setText("<HTML><B><font color=black>" + " " + nv.toString());

                }
                renderer.setEnabled(tree.isEnabled());
                returnValue = renderer;
            }
        }
        if (returnValue == null) {
            returnValue = defaultRenderer.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        }
        return returnValue;
    }
}
