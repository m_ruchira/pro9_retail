package com.isi.csvr.forex;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 07-Jan-2008 Time: 10:57:47 To change this template use File | Settings
 * | File Templates.
 */
public class ForexVector extends Vector {
    String fxName;
    String id;

    public ForexVector(String name, String id, Object elements[]) {
        this.fxName = name;
        this.id = id;
        for (int i = 0, n = elements.length; i < n; i++) {
            add(elements[i]);
        }
    }

    public String toString() {
        return fxName + "/" + id;

    }
}
