package com.isi.csvr.forex;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 18-Jun-2008 Time: 10:16:37 To change this template use File | Settings
 * | File Templates.
 */
public class FXSettingsObject implements Serializable {
    private long[] amtArray;
    private long defaultAmmount = 100000;
    private long unitIncrement = 50000;

    public FXSettingsObject(long[] amtArray) {
        this.amtArray = amtArray;
    }

    public long getDefaultAmmount() {
        return defaultAmmount;
    }

    public void setDefaultAmmount(long defaultAmmount) {
        this.defaultAmmount = defaultAmmount;
    }

    public long getUnitIncrement() {
        return unitIncrement;
    }

    public void setUnitIncrement(long unitIncrement) {
        this.unitIncrement = unitIncrement;
    }

    public long[] getAmtArray() {
        return amtArray;
    }
}
