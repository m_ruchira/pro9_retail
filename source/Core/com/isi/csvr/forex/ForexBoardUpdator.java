package com.isi.csvr.forex;

import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.ValuationRecord;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Sep 1, 2007
 * Time: 12:52:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForexBoardUpdator extends Thread {
    public static Vector forexSymbols = new Vector();

    public ForexBoardUpdator() {
        super("ForexBoard_updator");
    }

    public void run() {
        while (true) {
            try {
                if (!forexSymbols.isEmpty()) {
                    Enumeration en = forexSymbols.elements();
                    while (en.hasMoreElements()) {
                        FXSymbolView fxs = (FXSymbolView) en.nextElement();
                        //
                        try {
                            ArrayList arraList = TradePortfolios.getInstance().getValuationList();
                            for (int i = 0; i < arraList.size(); i++) {
                                try {
                                    ValuationRecord record = (ValuationRecord) arraList.get(i);
                                    if (fxs.key.equals(record.getSKey())) {
                                        long buyOpen = (long) record.getOpenBuyCount();
                                        long sellOpen = (long) record.getOpenSellCount();
                                        if (buyOpen == 0l) {
                                            fxs.updatenetAmmount(sellOpen);
                                            fxs.setNetExp(sellOpen);
                                        } else {
                                            fxs.updatenetAmmount(buyOpen);
                                            fxs.setNetExp(buyOpen);
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        fxs.bidBtn.update(fxs.key, Long.parseLong(fxs.blockSizeTrimer(fxs.tf.getText())));
                        fxs.askBtn.update(fxs.key, Long.parseLong(fxs.blockSizeTrimer(fxs.tf.getText())));
                        fxs.getTimeLable().setText(getTime(fxs.key, Long.parseLong(fxs.blockSizeTrimer(fxs.tf.getText()))));
                        fxs = null;

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(250);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private String getTime(String key, long blockSize) {
        String time = "00:00:00";
        FXObject fxo = ForexStore.getSharedInstance().getForexObject(key);
        if (fxo != null) {
            time = fxo.getLastUpdateTime();

        }
        return time;
    }
}
