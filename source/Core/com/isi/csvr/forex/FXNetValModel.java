package com.isi.csvr.forex;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.TableUpdateListener;
import com.isi.csvr.trading.datastore.OpenPositionRecord;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.ValuationRecord;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 03-Jul-2008 Time: 14:03:42 To change this template use File | Settings
 * | File Templates.
 */
public class FXNetValModel extends CommonTable
        implements TableModel, CommonTableInterface, TableUpdateListener {

    private ArrayList store;
    private LongTransferObject longTransferObject;
    private ArrayList<OpenPositionRecord> records;

    public FXNetValModel(ArrayList elements) {
        this.store = elements;
        longTransferObject = new LongTransferObject();
//       this.addTableModelListener(this);
    }

    public int getRowCount() {
//       return OpenPositionsStore.getSharedInstance().getRecords().size();  //To change body of implemented methods use File | Settings | File Templates.
//        return OpenPositionsStore.getSharedInstance().getSecondaryRecords().size();  //To change body of implemented methods use File | Settings | File Templates.
        return getRowCountFX(TradePortfolios.getInstance().getValuationList());  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return 2;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        ValuationRecord record = (ValuationRecord) getFilteredArrayList(TradePortfolios.getInstance().getValuationList()).get(rowIndex);
        if (record == null) {
            return ""; //null;
        }
        Stock stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
        try {
            if (record.getSKey().getExchange().equals("FORX")) {
                switch (columnIndex) {

                    case 0:
                        return stock.getSymbol();
                    case 1:
                        if (record.getOpenBuyCount() > 0) {
                            return "" + record.getOpenBuyCount();
                        } else {
                            return "" + record.getOpenSellCount();
                        }

                    default:
                        return " ";
                }
            } else {
                return " ";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }


    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
//        return super.getViewSettings().getColumnHeadings()[iCol];
        return ViewSettingsManager.getSummaryView("FX_NETVAL").getColumnHeadings()[iCol];
    }

    public void tableUpdating() {
        //To change body of implemented methods use File | Settings | File Templates.
        this.getTable().updateUI();
    }

    public int getRowCountFX(ArrayList arrlist) {
        int row = 0;
        for (int i = 0; i < arrlist.size(); i++) {
            ValuationRecord record = (ValuationRecord) TradePortfolios.getInstance().getValuationList().get(i);
            if (record.getSKey().getExchange().equals("FORX")) {
                row = row + 1;
            }
        }
        return row;
    }

    private ArrayList getFilteredArrayList(ArrayList arrlist) {
        ArrayList list = new ArrayList();
        for (int i = 0; i < arrlist.size(); i++) {
            ValuationRecord record = (ValuationRecord) TradePortfolios.getInstance().getValuationList().get(i);
            if (record.getSKey().getExchange().equals("FORX")) {
                list.add(record);
            }
        }
        return list;
    }
}
