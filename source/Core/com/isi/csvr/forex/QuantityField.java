package com.isi.csvr.forex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 21-Nov-2007 Time: 15:22:32 To change this template use File | Settings
 * | File Templates.
 */
public class QuantityField extends JFormattedTextField implements ComboBoxEditor {
    public QuantityField() {
    }

//    public QuantityField(Document doc, String text, int columns) {
//        super(doc, text, columns);
//    }

    public QuantityField(int columns) {
        super(columns);
    }

//    public QuantityField(String text, int columns) {
//        super(text, columns);
//    }

    public void addActionListener(ActionListener l) {
        super.addActionListener(l);
    }

    public void addKeyListener(KeyListener k) {
        super.addKeyListener(k);
    }

    public void removeActionListener(ActionListener l) {
        super.removeActionListener(l);
    }

    public void selectAll() {
        super.selectAll();
    }

    public Object getItem() {
        return getText().trim();
    }

    public void setItem(Object anObject) {
        if (anObject != null)
            setText(anObject.toString());
        else
            setText("");
    }

    public Component getEditorComponent() {
        return this;
    }
}
