package com.isi.csvr.forex;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: Chandika Hewage Date: Sep 1, 2007 Time: 12:52:29 PM
 */
public class FXObject {

    public ArrayList<BlockObect> blockData = new ArrayList<BlockObect>();
    public Hashtable<Long, BestBuySellObject> bestData = new Hashtable<Long, BestBuySellObject>();
    public BlockObect blockObject;
    //recently added
    public String buyQuoteId;
    public String sellQuoteId;
    public String buyBrokerId;
    public String sellBrokerId;
    public int isBestQuote;
    public double buyValue;
    public double sellValue;
    int decimalcorrectionfactor = 0;
    String lastUpdateTime = "00:00:00";
    private String symbol;
    private String symbolCode;
    private String exchange;
    private String key;
    private String forexCurrency;
    private long forexBlockSize;
    private String forexBaseValue;
    private String forexAppender;
    private double forexDeviation;
    private String forexBuyDate;
    private String forexSellDate;
    private String forexQuoteID;
    private double forexBuy;
    private double forexSell;
    private double maxPrice;
    private double minPrice;
    private double sellDiscount = 0;
    private double buyDiscount = 0;
    private long lastTradeTime;
    private double previousClossed;
    private int checkBande = 0;

    public FXObject(String newSymbol, String exchange) {
        symbolCode = newSymbol;
        try {
            symbol = symbolCode.split(Constants.MARKET_SEPERATOR_CHARACTER)[0];
        } catch (Exception e) {
            symbol = newSymbol;
        }
        this.exchange = exchange;
        key = SharedMethods.getKey(exchange, symbolCode, Meta.INSTRUMENT_FOREX);
        ForexStore.forexStore.put(key, this);

    } // end constructor

    public long setData(String[] data, int decimalFactor, boolean fromZip) throws Exception {
        this.decimalcorrectionfactor = decimalFactor;
        long blockSize = 0;
        if (data == null) {
            return 0;
        }
        char tag;
        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case 'a':
                    setForexCurrency(data[i].substring(1));
                    break;
                case 'b':
                    setForexBlockSize(SharedMethods.getLong(data[i].substring(1)));

                    break;
                case 'c':
                    setForexBaseValue(data[i].substring(1));


                    break;
                case 'd':
                    setForexAppender(data[i].substring(1));

                    break;
                case 'e':
                    setForexDeviation(SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'f':
                    setForexQuoteID(data[i].substring(1));

                    break;
                case 'g':
                    setForexBuyDate(data[i].substring(1));
                    break;
                case 'h':
                    setForexSellDate(data[i].substring(1));
                    break;
                case 'i':
                    setBestQuote(Integer.parseInt(data[i].substring(1)));
                    break;
                case 'j':
                    setBuyValue(SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'k':
                    setSellValue(SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'U':
                    setMaxPrice(SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'V':
                    setMinPrice(SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'A':
                    setBuyQuoteId(data[i].substring(1));
                    break;
                case 'B':
                    setSellQuoteId(data[i].substring(1));
                    break;
                case 'C':
                    setBuyBrokerId(data[i].substring(1));
                    break;
                case 'D':
                    setSellBrokerId(data[i].substring(1));
                    break;
                case 'E':
                    setLastTradeTime(data[i].substring(1));
                    break;
                case 'm':
                    setCheckBande(Integer.parseInt(data[i].substring(1)));
                    break;
                case 'P':
                    setPreviousClossed(SharedMethods.getDouble(data[i].substring(1)));
                    break;
            }
        }
        //Populate block data arrayList
//        populateBlockData(forexBlockSize,forexBaseValue,forexAppender,forexQuoteID);
        populateBlockData(forexBlockSize, isBestQuote, buyValue, sellValue, forexQuoteID, buyQuoteId, sellQuoteId, buyBrokerId, sellBrokerId);

//        return blockSize;
        return this.forexBlockSize;
    }

    public void populateBlockData(long blockSize, String basevale, String appender, String quoteID) {
        BlockObect bo = getBlockObject(blockSize);
        bo.setForexBaseValue(basevale);
        bo.setForexAppender(appender);
        bo.setForexBlockSize(blockSize);
        bo.setForexQuoteID(quoteID);

//        System.out.println("Quote ID : "+quoteID);
//        System.out.println("Base Val : "+basevale);
//        System.out.println("Appender : "+appender);
//        System.out.println("Block    : "+blockSize);

    }

    public void populateBlockData(long blockSize, int isBestQuote, double buyVal, double selVal, String quoteID, String buyQuote, String sellQuote, String brokerBuy, String brokerSell) {

        if (isBestQuote == 1) {
            BestBuySellObject bbo = getBestObject(blockSize);
            bbo.setBuyValue(buyVal / decimalcorrectionfactor);
            bbo.setSellValue(selVal / decimalcorrectionfactor);
            bbo.setBuyQuoteId(buyQuote);
            bbo.setSellQuoteId(sellQuote);
            bbo.setBuyBrokerId(brokerBuy);
            bbo.setSellBrokerId(brokerSell);
            bbo.setBlockSize(blockSize);
        } else {
            BlockObect bo = getBlockObject(blockSize);
            bo.setForexBlockSize(blockSize);
            bo.setBuyValue(buyVal);
            bo.setSellValue(selVal);
            bo.setForexQuoteID(quoteID);
        }

    }

    public String getBuyQuoteId() {
        return buyQuoteId;
    }

    public void setBuyQuoteId(String buyQuoteId) {
        this.buyQuoteId = buyQuoteId;
    }

    public String getSellQuoteId() {
        return sellQuoteId;
    }

    public void setSellQuoteId(String sellQuoteId) {
        this.sellQuoteId = sellQuoteId;
    }

    public String getBuyBrokerId() {
        return buyBrokerId;
    }

    public void setBuyBrokerId(String buyBrokerId) {
        this.buyBrokerId = buyBrokerId;
    }

    public String getSellBrokerId() {
        return sellBrokerId;
    }

    public void setSellBrokerId(String sellBrokerId) {
        this.sellBrokerId = sellBrokerId;
    }

    public int isBestQuote() {
        return isBestQuote;
    }

    public void setBestQuote(int bestQuote) {
        isBestQuote = bestQuote;
    }

    public double getBuyValue() {
        return buyValue;
    }

    public void setBuyValue(double buyValue) {
        this.buyValue = buyValue;
    }

    public double getSellValue() {
        return sellValue;
    }

    public void setSellValue(double sellValue) {
        this.sellValue = sellValue;
    }

    public String getForexCurrency() {
        return forexCurrency;
    }

    public void setForexCurrency(String forexCurrency) {
        this.forexCurrency = forexCurrency;
    }

    public long getForexBlockSize() {
        return forexBlockSize;
    }

    public void setForexBlockSize(long forexBlockSize) {
        this.forexBlockSize = forexBlockSize;
    }

    public String getForexBaseValue() {
        return forexBaseValue;
    }

    public void setForexBaseValue(String forexBaseValue) {
        this.forexBaseValue = forexBaseValue;
    }

    public String getForexAppender() {
        return forexAppender;
    }

    public void setForexAppender(String forexAppender) {
        this.forexAppender = forexAppender;
    }

    public double getForexDeviation() {
        return forexDeviation;
    }

    public void setForexDeviation(double forexDeviation) {
        this.forexDeviation = forexDeviation;
    }

    public String getForexBuyDate() {
        return forexBuyDate;
    }

    public void setForexBuyDate(String forexBuyDate) {
        this.forexBuyDate = forexBuyDate;
    }

    public String getForexSellDate() {
        return forexSellDate;
    }

    public void setForexSellDate(String forexSellDate) {
        this.forexSellDate = forexSellDate;
    }

    public String getForexQuoteID() {
        return forexQuoteID;
    }

    public void setForexQuoteID(String forexQuoteID) {
        this.forexQuoteID = forexQuoteID;
    }


    public void setForexBuy(String base, String appen) {
        String buyVal = "" + base + appen;
        forexBuy = (Double.parseDouble(buyVal) + this.forexDeviation) / decimalcorrectionfactor;

    }

    public void setForexSell(String base, String appen) {
        String selVal = "" + base + appen;
        forexSell = (Double.parseDouble(selVal) - this.forexDeviation) / decimalcorrectionfactor;
    }

    public double getForexBuy() {
        return forexBuy;

    }

    public double getForexSell() {
        return forexSell;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice / decimalcorrectionfactor;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice / decimalcorrectionfactor;
    }

    public ArrayList<BlockObect> getBlockObjects() {
        return blockData;
    }

    public Hashtable<Long, BestBuySellObject> getBestObjects() {
        return bestData;
    }

    public boolean isSmallestBlockSize(long blockSize) {
        long smallest = 0;
        for (BlockObect blockObect : blockData) {
            if (blockObect.getForexBlockSize() < smallest) {
                smallest = blockObect.getForexBlockSize();
            }
        }

        return ((smallest == blockSize) || (smallest == 0));
    }


    public BlockObect getBlockObject(long blocksize) {

        BlockObect blockObject = findObject(blocksize);
        if (blockObject != null) {
            return blockObject;
        } else {
            blockObject = new BlockObect();
            blockData.add(blockObject);
            return blockObject;
        }
    }

    public BestBuySellObject getBestObject(long blocksize) {

        BestBuySellObject bestObject = findBestObject(blocksize);
        if (bestObject != null) {
            return bestObject;
        } else {
            bestObject = new BestBuySellObject();
            bestData.put(blocksize, bestObject);
            return bestObject;
        }
    }

    private BlockObect findObject(long blocksize) {
        for (BlockObect blockObect : blockData) {
            if (blockObect.getForexBlockSize() == blocksize) {
                return blockObect;
            } else if (blockObect.getForexBlockSize() < blocksize) {
                return blockObect;
            }
        }
        return null;
    }

    private BestBuySellObject findBestObject(long blocksize) {
//        for (BestBuySellObject bestObect:bestData) {
//            if (bestObect.getBlockSize() == blocksize){
//                return bestObect;
//            }
//        }
//        return null;
        if (bestData.containsKey(blocksize)) {
            return (BestBuySellObject) bestData.get(blocksize);
        } else {
            return null;
        }

    }

    public double getBuyValForGivenBlock(long blockSize) {
        try {
            double buy;
            BlockObect bo = findObject(blockSize);
            return (bo.getBuyValue() / decimalcorrectionfactor) + getBuyDiscount();
        } catch (Exception e) {
            return 0l;
        }
//        if ((bo.getForexBaseValue()!=null)&&(bo.getForexAppender()!=null)) {
//            String buyVal=""+bo.getForexBaseValue()+bo.getForexAppender();
//            buy= Double.parseDouble(buyVal)-this.forexDeviation;
//            return buy/decimalcorrectionfactor;
//        } else {
//            return 0l;
//        }
    }

    public long getBlockSizeForGivenQuantity(long quantity) {
        long[] array = new long[blockData.size()];
        try {
            for (int i = 0; i < blockData.size(); i++) {
                array[i] = ((BlockObect) blockData.get(i)).getForexBlockSize();

            }
            Arrays.sort(array);
            if (quantity <= array[0]) {
                return array[0];
            } else if ((quantity > array[0]) && (quantity <= array[1])) {
                return array[1];
            } else if ((quantity > array[1]) && (quantity <= array[2])) {
                return array[2];
            } else {
                return array[3];
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return array[0];
        }

    }


    public double getSellValForGivenBlock(long blockSize) {
//        double sell;
        try {
            BlockObect bo = findObject(blockSize);
            return (bo.getSellValue() / decimalcorrectionfactor) - getSellDiscount();
        } catch (Exception e) {
            return 0l;
        }
//            if ((bo.getForexBaseValue()!=null)&&(bo.getForexAppender()!=null)) {
//                String buyVal=""+bo.getForexBaseValue()+bo.getForexAppender();
//                sell= Double.parseDouble(buyVal)+this.forexDeviation;
//                return sell/decimalcorrectionfactor;
//            } else {
//                 return 0l;
//            }
//        return sellValue;
    }

    public void frameTest(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (i == 1) {
                System.out.println("Frame segment : " + i + " , " + SharedMethods.getLong(arr[i].substring(1)));
            }
            if (i == 2) {
                System.out.println("Frame segment : " + i + " , " + arr[i].substring(1));
            }

        }

    }
    // modifications

    public double getSellDiscount() {
        return sellDiscount;
    }

    public void setSellDiscount(double sellDiscount) {
        this.sellDiscount = sellDiscount;
    }

    public double getBuyDiscount() {
        return buyDiscount;
    }

    public void setBuyDiscount(double buyDiscount) {
        this.buyDiscount = buyDiscount;
    }

    public int getCheckBande() {
        return checkBande;
    }

    public void setCheckBande(int checkBande) {
        this.checkBande = checkBande;
    }

    public double getPreviousClossed() {
        return previousClossed;
    }

    public void setPreviousClossed(double previousClossed) {
        this.previousClossed = previousClossed;
    }

    public long getLastTradeTime() {
        return lastTradeTime;
    }

    public void setLastTradeTime(String time) {
        try {
            lastTradeTime = getTradeTime(time);
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private long getTradeTime(String sValue) throws Exception {
        long lTime = 0;
        Calendar oCal = Calendar.getInstance();
        oCal.setTimeInMillis(0);
        String time = sValue.split("-")[1];
        oCal.set(Calendar.HOUR, Integer.parseInt(time.substring(0, 2)));
        oCal.set(Calendar.MINUTE, Integer.parseInt(time.substring(3, 5)));
        oCal.set(Calendar.SECOND, Integer.parseInt(time.substring(6)));
        lTime = oCal.getTime().getTime();
        oCal = null;
        lastUpdateTime = time.substring(0, 2) + ":" + time.substring(3, 5) + ":" + time.substring(6);
//        System.out.println("LAST UPDATE : "+lastUpdateTime);
        return lTime;
    }

    public String getLastUpdateTime() {
        return lastUpdateTime;
    }
}
