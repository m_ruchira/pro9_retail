package com.isi.csvr.forex;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWFormattedTextField;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: admin Date: 16-Nov-2007 Time: 19:12:00 To change this template use File | Settings |
 * File Templates.
 */
public class ForexBuySell extends InternalFrame implements ActionListener, TradingConnectionListener, InternalFrameListener, KeyListener, ChangeListener, PropertyChangeListener, Themeable {
    public static ForexBuySell self;
    public static JTextField symbolField;
    public static String sKey;
    public static Vector forexSymbols = new Vector();
    public static ForexBuySell fxBuySell;
    final int quoteOrderType = 3;
    final int marketOrderType = 2;
    final int limitOrderType = 1;
    final int buySellType = 1;
    final int amendType = 2;
    final int cancelType = 3;
    public JPanel upperPanel;
    public JPanel lowerPanel;
    public JPanel middlePanel;
    public JPanel quantityPanel;
    public JPanel amendBtnPanel;
    public JPanel responceTimerPanel;
    public JLabel symbolLabel;
    public JLabel quantityLabel;
    public JLabel typeLabel;
    public JLabel priceLabel;
    public JLabel pfLabel;
    public JLabel sideLabel;
    public TWTextField sideField;
    public TWTextField quantityField;
    public TWTextField priceField;
    public TWComboBox typeCombo;
    public TWButton requestCancelBtn;
    public TWComboBox portfolioCombo;
    public boolean updatorLock = false;
    public RequsetTimer requsetTimer;
    public RequsetTimer responseTimer;
    public String symbol = "";
    ForexBuySellUpdator updator;
    Color fxColor = new Color(185, 249, 185);
    TWDecimalFormat blockSizeFormat = new TWDecimalFormat("###,##0");
    long selectedQty = 0;
    int side = 1;   //sell=2 ,buy=1
    int mode = 1;
    char type = '1';
    Transaction oldTransaction;
    long[] arrayBlockSizes = new long[9];
    private FXButton bidBtn;
    private FXButton askBtn;
    private FXLimitButton bidLBtn;
    private FXLimitButton askLBtn;
    private FXQuoteButton bidQBtn;
    private FXQuoteButton askQBtn;
    private JButton dowwnArrow;
    private TWFormattedTextField tf;
    private NumberFormat amountFormat;
    private JSpinner blockSpinner;
    private TWButton amendBtn;
    private TWButton closeBtn;
    private String portfolioNo;
    private String quotedExchange;
    private String quotedSymbol;
    private String quotedID;
    private String quotedQuantity;
    private double bidQuote = 0;
    private double offerQuote = 0;
    private String twRef = "";
    private String brokerID;
    private String rejectReason;
    private int status = 0;
    private long blockSpinnerDefault = 1000000;
    private double amount = 1000000;
    private Color blockItemSelectedColor;
    private Color blockItemFGColor;

    public ForexBuySell(int mode, Transaction tr) {
        super();
        this.mode = mode;
        this.setTitle(Language.getString("FX_BUYSELL_TITLE"));

        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(false);
        this.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        if ((mode == 2) && (tr != null)) {
            amendBtnPanel = new JPanel();
            amendBtn = new TWButton(Language.getString("AMEND"));
            closeBtn = new TWButton(Language.getString("CLOSE_WINDOW"));
            this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"155", "30", "100%", "35"}, 0, 0));
            this.setSize(270, 255);
            amendBtn.addActionListener(this);
            closeBtn.addActionListener(this);
        } else {
            this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"125", "30", "100%"}, 0, 0));
            this.setSize(270, 300);
        }
        requestCancelBtn = new TWButton();
        requestCancelBtn.setEnabled(false);
        lowerPanel = new JPanel();
        symbolField = new JTextField();
        priceField = new TWTextField();
        sideField = new TWTextField();
        sideLabel = new JLabel(Language.getString("SIDE"));
        bidBtn = new FXButton(Language.getString("TRADE_SIDE_BUY"), "EURUSD");
        bidBtn.addActionListener(this);
        bidBtn.setActionCommand("MARKET_BID");
        askBtn = new FXButton(Language.getString("SELL"), "EURUSD");
        askBtn.addActionListener(this);
        askBtn.setActionCommand("MARKET_ASK");
        bidLBtn = new FXLimitButton(Language.getString("TRADE_SIDE_BUY"), "EURUSD");
        bidLBtn.addActionListener(this);
        bidLBtn.setActionCommand("LIMIT_BID");
        askLBtn = new FXLimitButton(Language.getString("SELL"), "EURUSD");
        askLBtn.addActionListener(this);
        askLBtn.setActionCommand("LIMIT_ASK");
        portfolioCombo = new TWComboBox();
        typeCombo = new TWComboBox();
        typeCombo.setEnabled(false);
        if (TradingShared.isConnected()) {
            tradeServerConnected();
            portfolioCombo.setEnabled(true);
            requestCancelBtn.setEnabled(true);
            typeCombo.setEnabled(true);
        } else {
            portfolioCombo.setEnabled(false);
            requestCancelBtn.setEnabled(false);
            typeCombo.setEnabled(false);
        }
        //Setting prefered portfolio
        try {
            TradingPortfolioRecord portflio = TradeMethods.getPreferredProtfolio("FORX");
            selectPortfolio(portflio.getPortfolioID());
        } catch (Exception e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
        //////////////////////
        askBtn.setOpaque(true);
        askBtn.setBackground(fxColor);
        bidBtn.setOpaque(true);
        bidBtn.setBackground(fxColor);
        askLBtn.setOpaque(true);
        askLBtn.setBackground(fxColor);
        bidLBtn.setOpaque(true);
        bidLBtn.setBackground(fxColor);
        bidBtn.setPreferredSize(new Dimension(70, 60));
        askBtn.setPreferredSize(new Dimension(70, 60));
        bidLBtn.setPreferredSize(new Dimension(70, 60));
        askLBtn.setPreferredSize(new Dimension(70, 60));
        requsetTimer = new RequsetTimer();
        responseTimer = new RequsetTimer();

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);

        reauestTimeTracker();
        if ((mode == 2) && (tr != null)) {
            this.setTitle(Language.getString("FX_AMEND_CANCEL"));
            this.oldTransaction = tr;
            this.add(createUpperPanel(2));
            this.add(createMiddlePanel());
            this.add(new JSeparator());
            this.add(createAmendBtnPanel());
            setData(tr);

        } else {
            this.add(createUpperPanel(1));
            this.add(createMiddlePanel());
            this.add(createLowerPanel());
        }

        updator = new ForexBuySellUpdator();
        updator.start();
        fxBuySell = this;
        blockItemSelectedColor = Theme.getColor("FOREX_POPUP_SELECTED");
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
        GUISettings.applyOrientation(this);


    }

    public static ForexBuySell getSharedInstance(int mode, Transaction tr) {
        if (self == null) {
            self = new ForexBuySell(mode, tr);
        }
        return self;

    }

    public static ForexBuySell getInstance() {
        return fxBuySell;

    }

    public void setKey(String key) {
        this.sKey = key;
        if (this.mode == 1) {
            symbolField.setText(SharedMethods.getSymbolFromKey(sKey));
        }
    }

    public JPanel createAmendBtnPanel() {
        amendBtnPanel.setLayout(new FlexGridLayout(new String[]{"50%", "80", "75", "75", "50%"}, new String[]{"25"}, 5, 5));
        amendBtnPanel.add(new JLabel());
        amendBtnPanel.add(new JLabel());
        amendBtnPanel.add(amendBtn);
        amendBtnPanel.add(closeBtn);
        amendBtnPanel.add(new JLabel());
        return amendBtnPanel;
    }

    public JPanel createQuantityPanel() {
        quantityPanel = new JPanel();
        quantityPanel.setLayout(new FlexGridLayout(new String[]{"85%", "15%"}, new String[]{"100%"}));
        quantityPanel.setBorder(BorderFactory.createEtchedBorder());
        dowwnArrow = new JButton(new DownArrow());
        dowwnArrow.setBorderPainted(false);
        blockSpinner = new JSpinner(new SpinnerNumberModel(1000000, 1000000, 3000000, 500000));
        blockSpinner.setBorder(BorderFactory.createEmptyBorder());
        selectedQty = Long.parseLong(blockSpinner.getValue() + "");
        amountFormat = NumberFormat.getNumberInstance();
        tf = new TWFormattedTextField(amountFormat);
        tf.setHorizontalAlignment(SwingConstants.RIGHT);
        tf.setValue(new Double(amount));
        tf.setColumns(10);
        tf.addPropertyChangeListener("value", this);
        tf.setEditable(true);
        tf.setBorder(BorderFactory.createEmptyBorder());
        blockSpinner.setEditor(tf);
        tf.setBackground(Color.white);
        quantityPanel.add(blockSpinner);
        quantityPanel.add(dowwnArrow);
        dowwnArrow.addActionListener(this);
        blockSpinner.addChangeListener(this);
        return quantityPanel;

    }

    // descriptions
    public JPanel createUpperPanel(int mode) {
        symbolLabel = new JLabel(Language.getString("CUS_INDEX_SYMBOL"));
        priceLabel = new JLabel(Language.getString("PRICE"));
        pfLabel = new JLabel(Language.getString("PORTFOLIO"));
        quantityLabel = new JLabel(Language.getString("WHAT_IF_CALC_QTY"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        priceField.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        priceField.setHorizontalAlignment(SwingConstants.RIGHT);
        priceField.setText("");
        populateTypeCombo();
        typeCombo.addActionListener(this);
        typeCombo.setSelectedIndex(0);
        symbolField.setText(SharedMethods.getSymbolFromKey(sKey));
        symbolField.setEditable(false);
        priceField.setEditable(true);
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"50%", "110", "10", "110", "50%"}, new String[]{"20", "20", "20", "20", "20", "100%"}, 5, 5));
        if (mode == 2) {
            upperPanel.add(new JLabel());
            upperPanel.add(symbolLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(symbolField);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(quantityLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(createQuantityPanel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(typeLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(typeCombo);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(priceLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(priceField);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(sideLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(sideField);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(pfLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(portfolioCombo);
            upperPanel.add(new JLabel());

        } else {
            upperPanel.add(new JLabel());
            upperPanel.add(symbolLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(symbolField);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(quantityLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(createQuantityPanel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(typeLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(typeCombo);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(priceLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(priceField);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(pfLabel);
            upperPanel.add(new JLabel());
            upperPanel.add(portfolioCombo);
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
            upperPanel.add(new JLabel());
        }


        GUISettings.applyOrientation(upperPanel);

        return upperPanel;
    }

    public void populateTypeCombo() {
        typeCombo.addItem(new TWComboItem("2", Language.getString("ORDER_TYPE_LIMIT")));
        typeCombo.addItem(new TWComboItem("1", Language.getString("ORDER_TYPE_MARKET")));
        typeCombo.addItem(new TWComboItem("3", Language.getString("ORDER_TYPE_QUOTE")));
        typeCombo.setSelectedIndex(0);
    }

    public JPanel createLowerPanel() {
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"50%", "70", "70", "50%"}, new String[]{"1", "100%"}));
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        if (mode == 2) {
            lowerPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("FX_AMEND_BUYSELL")));

        } else {
            lowerPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("FX_BUYSELL")));

        }
        GUISettings.applyOrientation(lowerPanel);
        return lowerPanel;
    }

    public void setUIforOrderType(int type, int mode) {
        if (mode == 1) {
            if ((type == quoteOrderType) && (lowerPanel != null)) {
                FXQuoteButton bidQ1Btn = new FXQuoteButton(Language.getString("BUY"), 0.00000);
                FXQuoteButton askQ1Btn = new FXQuoteButton(Language.getString("SELL"), 0.0000);
                bidQ1Btn.addActionListener(this);
                askQ1Btn.addActionListener(this);
                bidQ1Btn.setPreferredSize(new Dimension(70, 60));
                askQ1Btn.setPreferredSize(new Dimension(70, 60));
                askQ1Btn.setOpaque(true);
                askQ1Btn.setBackground(fxColor);
                bidQ1Btn.setOpaque(true);
                bidQ1Btn.setBackground(fxColor);
                bidQ1Btn.setEnabled(false);
                askQ1Btn.setEnabled(false);
                bidQ1Btn.repaint();
                askQ1Btn.repaint();
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(askQ1Btn);
                lowerPanel.add(bidQ1Btn);
                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();

            } else if ((type == marketOrderType) && (lowerPanel != null)) {
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(askBtn);
                lowerPanel.add(bidBtn);
                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();

            } else if ((type == limitOrderType) && (lowerPanel != null)) {
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(askLBtn);
                lowerPanel.add(bidLBtn);
                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();
            }
        } else if (mode == 2) {
            if ((type == quoteOrderType) && (lowerPanel != null)) {
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();

            } else if ((type == marketOrderType) && (lowerPanel != null)) {
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                if (this.side == TradeMeta.BUY) {
                    bidBtn.setEnabled(true);
                    askBtn.setEnabled(false);
                    lowerPanel.add(askBtn);
                    lowerPanel.add(bidBtn);
                } else if (this.side == TradeMeta.SELL) {
                    bidBtn.setEnabled(false);
                    askBtn.setEnabled(true);
                    lowerPanel.add(askBtn);
                    lowerPanel.add(bidBtn);
                }

                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();

            } else if ((type == limitOrderType) && (lowerPanel != null)) {
                lowerPanel.removeAll();
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                lowerPanel.add(new JLabel());
                if (this.side == TradeMeta.BUY) {
                    bidLBtn.setEnabled(true);
                    askLBtn.setEnabled(false);
                    lowerPanel.add(askLBtn);
                    lowerPanel.add(bidLBtn);
                } else if (this.side == TradeMeta.SELL) {
                    bidLBtn.setEnabled(false);
                    askLBtn.setEnabled(true);
                    lowerPanel.add(askLBtn);
                    lowerPanel.add(bidLBtn);
                }

                lowerPanel.add(new JLabel());
                lowerPanel.updateUI();
            }
        }
    }

    public void setData(Transaction tr) {
        if (tr != null) {
            this.side = tr.getSide();
            this.symbol = tr.getSymbol();
            this.type = tr.getType();
            symbolField.setText(this.symbol);
            sideField.setText(getTradeSide(tr.getSide()));
            sideField.setEnabled(false);
            tf.setValue(tr.getOrderQuantity());
            setOrderType(tr.getType(), this.side, tr);
            portfolioCombo.addItem(tr.getPortfolioNo());
            symbolField.setEditable(false);
            portfolioCombo.setEnabled(false);


        }
    }

    private String getTradeSide(int side) {
        if (side == 1) {
            return Language.getString("TRADE_SIDE_BUY");

        } else {
            return Language.getString("SELL");
        }

    }

    private void setOrderType(char type, int side, Transaction tr) {
        try {
            switch (type) {
                case 'D':
                    typeCombo.setSelectedIndex(2);
                    typeCombo.setEnabled(false);
                    break;
                case '1':
                    typeCombo.setSelectedIndex(0);
                    requestCancelBtn.setEnabled(false);
                    break;
                case '2':
                    typeCombo.setSelectedIndex(1);
                    priceField.setText(tr.getPrice() + "");
                    priceField.setEditable(true);
                    requestCancelBtn.setEnabled(false);
                    setUIforOrderType(limitOrderType, this.mode);
                    break;
                case 3:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public JPanel createResponceTimerpanel() {
        responceTimerPanel = new JPanel();
        responceTimerPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "20"}));
        responceTimerPanel.add(new JLabel());
        responceTimerPanel.add(responseTimer);
        return responceTimerPanel;
    }

    public void setQuoteTypeUI(double offer, double bid) {
        bidQBtn = new FXQuoteButton(Language.getString("BUY"), offer);
        askQBtn = new FXQuoteButton(Language.getString("SELL"), bid);
        bidQBtn.addActionListener(this);
        bidQBtn.setActionCommand("QUOTE_BID");
        askQBtn.addActionListener(this);
        askQBtn.setActionCommand("QUOTE_ASK");
        bidQBtn.setPreferredSize(new Dimension(70, 60));
        askQBtn.setPreferredSize(new Dimension(70, 60));
        askQBtn.setOpaque(true);
        askQBtn.setBackground(fxColor);
        bidQBtn.setOpaque(true);
        bidQBtn.setBackground(fxColor);
        bidQBtn.repaint();
        askQBtn.repaint();
        lowerPanel.removeAll();
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(new JLabel());
        lowerPanel.add(askQBtn);
        lowerPanel.add(bidQBtn);
        lowerPanel.add(createResponceTimerpanel());
        responseTimer.isTimerExpired = false;
        responseTimer.startTimer();
        bidQBtn.setEnabled(true);
        askQBtn.setEnabled(true);
        lowerPanel.updateUI();
//        requestCancelBtn.setEnabled(true);
    }

    // buy/sell buttons and request cancel Button
    public JPanel createMiddlePanel() {

        requestCancelBtn.addActionListener(this);
        requestCancelBtn.setText(Language.getString("REQUEST"));
        requestCancelBtn.setActionCommand(Language.getString("REQUEST"));
        //requestCancelBtn.setEnabled(false);
        middlePanel = new JPanel();
        middlePanel.setLayout(new FlexGridLayout(new String[]{"50%", "150", "50%"}, new String[]{"100%"}, 5, 5));
        middlePanel.add(new JLabel());
        middlePanel.add(requestCancelBtn);
        middlePanel.add(requsetTimer);
        return middlePanel;
    }

    public JPopupMenu createBlockSizeList() {
        JPopupMenu blockPopup = new JPopupMenu("Block Popup");
        blockPopup.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        arrayBlockSizes[0] = 1000000;
        arrayBlockSizes[1] = 1500000;
        arrayBlockSizes[2] = 2000000;
        arrayBlockSizes[3] = 2500000;
        arrayBlockSizes[4] = 3000000;
        arrayBlockSizes[5] = 3500000;
        arrayBlockSizes[6] = 4000000;
        arrayBlockSizes[7] = 4500000;
        arrayBlockSizes[8] = 5000000;
        try {
            for (int i = 0; i < arrayBlockSizes.length; i++) {
                final JMenuItem blockItem = new JMenuItem();
                blockItem.setForeground(blockItemFGColor);
                blockItem.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                blockItem.setBorder(BorderFactory.createEmptyBorder());
                blockItem.setText(amountFormat.format(arrayBlockSizes[i]));
                blockItem.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tf.setValue(Long.parseLong(blockSizeTrimer(blockItem.getText())));
                        selectedQty = Long.parseLong(blockSizeTrimer(blockItem.getText()));
                    }
                });
                blockPopup.add(blockItem);
            }

        } catch (Exception e) {
            System.out.println("Error while loading block sizes...");
            e.printStackTrace();
        }
        return blockPopup;
    }

    private void selectPortfolio(String portfolio) {
        try {
            for (int i = 0; i < TradingShared.getTrader().getPortfolioCount(); i++) {
                if (portfolio.equals(TradingShared.getTrader().getPortfolioID(i))) {
                    portfolioCombo.setSelectedIndex(i);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String blockSizeTrimer(String longString) {
        String splitpatern = ",";
        String[] parts = longString.split(splitpatern);
        String blocksize = "";
        for (int i = 0; i < parts.length; i++) {
            blocksize = blocksize + parts[i];
        }
        return blocksize.trim();
    }

    public void reauestTimeTracker() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    while (true) {
                        if (requsetTimer.isTimerExpired) {
                            resetQuoteRequest();
                        }
                        if (responseTimer.isTimerExpired) {
                            resetQuoteResponse();
                        }
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception ex) {
                }

            }
        }).start();

    }

    public void resetQuoteRequest() {
        requestCancelBtn.setText(Language.getString("REQUEST"));
        requestCancelBtn.setActionCommand(Language.getString("REQUEST"));
        requsetTimer.restartTimer();

    }

    public void resetQuoteResponse() {
        bidQBtn.setEnabled(false);
        askQBtn.setEnabled(false);


    }

    public void processQuoteResponse(String frame) {

        try {
            TradeMessage message = new TradeMessage(frame);
            String[] forexQuotes = message.getMessageData().split(TradeMeta.DS);
            TradingPortfolioRecord portflio = null;
            try {
                String[] fxQuoteData = forexQuotes[0].split(TradeMeta.FD);
                for (String fxQuoteField : fxQuoteData) {
                    try {
                        if (fxQuoteField.startsWith("A")) {
                            portfolioNo = fxQuoteField.substring(1);
                            portflio = TradePortfolios.getInstance().getPortfolio(portfolioNo);
                            if (portflio == null) {
                                break;
                            }
                        } else if ((portflio != null) && (fxQuoteField.startsWith("B"))) {
                            quotedExchange = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("C"))) {
                            quotedSymbol = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("D"))) {
                            quotedID = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("E"))) {
                            quotedQuantity = fxQuoteField.substring(1);

                        } else if ((portflio != null) && (fxQuoteField.startsWith("F"))) {
                            offerQuote = Double.parseDouble(fxQuoteField.substring(1));

                        } else if ((portflio != null) && (fxQuoteField.startsWith("G"))) {
                            bidQuote = Double.parseDouble(fxQuoteField.substring(1));
                        } else if ((portflio != null) && (fxQuoteField.startsWith("H"))) {
                            brokerID = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("I"))) {
                            twRef = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("J"))) {
                            rejectReason = fxQuoteField.substring(1);
                        } else if ((portflio != null) && (fxQuoteField.startsWith("K"))) {
                            status = Integer.parseInt(fxQuoteField.substring(1));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                portflio = null;
                fxQuoteData = null;
            } catch (Exception e) {
                e.printStackTrace();
            }


            forexQuotes = null;
            message = null;
            if (status == 1) {
                if (twRef.equals("amend_req")) {
                    if (oldTransaction.getSide() == TradeMeta.BUY) {
                        priceField.setText(bidQuote + "");
                    } else {
                        priceField.setText(offerQuote + "");
                    }

                } else {
                    responseTimer.isTimerExpired = false;
                    setQuoteTypeUI(offerQuote, bidQuote);
                }
            } else {
                String reason = "<HTML><div align\\=left><font color\\=red><B>Sorry!</B></font><BR><b>" + rejectReason + "</b></div>";
                JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), reason, Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            requsetTimer.stopTimer();
            resetQuoteRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        if (actionCommand.equals(Language.getString("REQUEST"))) {
            requestCancelBtn.setText(Language.getString("CANCEL"));
            requestCancelBtn.setActionCommand(Language.getString("CANCEL"));
            System.out.println("QUOTE REQUEST SENT......");
            TradeMessage message = new TradeMessage(TradeMeta.MT_FOREX_QUOTE);
            message.addData("A" + ((TWComboItem) portfolioCombo.getSelectedItem()).getId());
            message.addData("B" + SharedMethods.getExchangeFromKey(sKey));
            message.addData("C" + SharedMethods.getSymbolFromKey(sKey));
            message.addData("D" + blockSizeTrimer(tf.getText()));
            message.addData("E" + quoteOrderType);
            message.addData("F" + TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
            if (mode == 2) {
                message.addData("I" + "amend_req");
            } else {
                message.addData("I" + "quote_req");
            }
            SendQueue.getSharedInstance().addData(message.toString(), Constants.PATH_PRIMARY);
            requsetTimer.isTimerExpired = false;
            requsetTimer.startTimer();


        } else if (actionCommand.equals(Language.getString("CANCEL"))) {
            requestCancelBtn.setText(Language.getString("REQUEST"));
            requestCancelBtn.setActionCommand(Language.getString("REQUEST"));
            requsetTimer.stopTimer();
            requsetTimer.isTimerExpired = false;

        } else if (e.getSource().equals(typeCombo)) {
            TWComboItem twc = (TWComboItem) typeCombo.getSelectedItem();
            if (twc.getId().equals("1")) {
                if (updator != null) {
                    updatorLock = false;
                }
                priceField.setText("");
                priceField.setEditable(false);
                requestCancelBtn.setEnabled(false);
                setUIforOrderType(marketOrderType, this.mode);
            } else if (twc.getId().equals("2")) {

                updatorLock = true;
                priceField.setEditable(true);
                requestCancelBtn.setEnabled(false);
                setUIforOrderType(limitOrderType, this.mode);
            } else if (twc.getId().equals("3")) {
                if (updator != null) {
                    updatorLock = false;
                }
                priceField.setText("");
                priceField.setEditable(false);
                requestCancelBtn.setEnabled(true);
                setUIforOrderType(quoteOrderType, this.mode);
            }

        } else if (e.getSource().equals(dowwnArrow)) {
            Point location = new Point(quantityPanel.getX(), (quantityPanel.getY()) + quantityPanel.getBounds().height);
            SwingUtilities.convertPointToScreen(location, quantityPanel.getParent());
            JPopupMenu jl = createBlockSizeList();
            jl.setLocation(location);
            jl.setPreferredSize(new Dimension(quantityPanel.getWidth(), 130));
            jl.show(quantityPanel, 2, quantityPanel.getHeight() + 2);
            selectedQty = Long.parseLong(blockSpinner.getValue() + "");
        } else if (e.getSource() == blockSpinner) {
            selectedQty = Long.parseLong(blockSpinner.getValue() + "");
        } else if (e.getActionCommand().equals("QUOTE_BID") || e.getActionCommand().equals("QUOTE_ASK")) {
            try {
                int side;
                double price;
                String portfolio = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                if (e.getActionCommand().equals("QUOTE_ASK")) {
                    side = TradeMeta.SELL;
                    price = bidQuote;
                } else {
                    side = TradeMeta.BUY;
                    price = offerQuote;
                }
                if (TradingShared.isConnected()) {
                    boolean success = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, SharedMethods.getExchangeFromKey(sKey), quotedSymbol,
                            side, TradeMeta.ORDER_TYPE_QUOTED, price, 0, "*",
                            Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, quotedID, null, null, 0d, null, 0);
                    if (success) {
                        this.setVisible(false);
                    }
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }

        } else if (e.getActionCommand().equals("LIMIT_BID") || e.getActionCommand().equals("LIMIT_ASK")) {
            try {
                int side;
                String portfolio = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();

                if (e.getActionCommand().equals("LIMIT_ASK")) {
                    side = TradeMeta.SELL;
                } else {
                    side = TradeMeta.BUY;
                }
                if (TradingShared.isConnected()) {
                    if (mode == 2) {
                        oldTransaction.setPrice(Double.parseDouble(priceField.getText()));
                        oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                        oldTransaction.setType('2');
                        TradeMethods.amendOrder(oldTransaction, false, side, TradeMeta.ORDER_TYPE_LIMIT, Double.parseDouble(priceField.getText()), "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                                -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                    } else {
                        boolean success = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey),
                                side, TradeMeta.ORDER_TYPE_LIMIT, Double.parseDouble(priceField.getText()), 0, "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, null, null, null, 0d, null, 0);
                        if (success) {
                            this.setVisible(false);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }

            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }


        } else if (e.getActionCommand().equals("MARKET_BID") || e.getActionCommand().equals("MARKET_ASK")) {
            try {
                FXObject fxo = (FXObject) ForexStore.forexStore.get(sKey);
                int side;
                double price;
                double bestPrice;
                String bestQuoteID;
                String brockerID;
                String portfolio = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                if (e.getActionCommand().equals("MARKET_ASK")) {
                    side = TradeMeta.SELL;
                    price = askBtn.buySellVal;
                } else {
                    side = TradeMeta.BUY;
                    price = bidBtn.buySellVal;
                }
                if (TradingShared.isConnected()) {
                    if (mode == 2) {
                        oldTransaction.setPrice(price);
                        oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                        oldTransaction.setType('1');
                        TradeMethods.amendOrder(oldTransaction, false, side, TradeMeta.ORDER_TYPE_MARKET, price, "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                                -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);
                    } else {

                        boolean success = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey),
                                side, TradeMeta.ORDER_TYPE_MARKET, price, 0, "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, null, quotedID, null, 0d, null, 0);
                        if (success) {
                            this.setVisible(false);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }

        } else if (e.getSource().equals(amendBtn)) {
            try {

                double price = Double.parseDouble(priceField.getText());
                if (TradingShared.isConnected()) {
                    if (mode == 2) {
                        oldTransaction.setPrice(price);
                        oldTransaction.setQuantity(Long.parseLong(blockSizeTrimer(tf.getText())));
                        oldTransaction.setType(type);
                        TradeMethods.amendOrder(oldTransaction, false, side, type, price, "*",
                                Integer.parseInt(blockSizeTrimer(tf.getText())), (short) TradeMeta.TIF_DAY, 0, "-1", "0",
                                -1, null, null, -1, 0, 0, 0, 0, false, 0, false, null, null, null);

                    }
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
                e1.printStackTrace();
            }

        } else if (e.getSource().equals(closeBtn)) {
            this.dispose();
            self = null;
        }
    }

    public void tradeServerConnected() {
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            if (isForexSupportedPF(record.getExchangeString())) {
                portfolioCombo.addItem(new TWComboItem(record.getPortfolioID(), record.getName()));
            }

        }
        if ((portfolioCombo != null) && (typeCombo != null)) {
            portfolioCombo.setEnabled(true);
            typeCombo.setEnabled(true);
            requestCancelBtn.setEnabled(true);
        }
    }

    public boolean isForexSupportedPF(String exchangeList) {
        try {
            String[] arr = exchangeList.split(",");
            Hashtable<String, String> temp = new Hashtable<String, String>();
            for (int i = 0; i < arr.length; i++) {
                temp.put(arr[i], arr[i]);
            }
            if (temp.containsKey("FORX")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public void tradeSecondaryPathConnected() {
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        TradingConnectionNotifier.getInstance().removeConnectionListener(this);
        this.dispose();
        self = null;
    }

    public void tradeServerDisconnected() {
        portfolioCombo.setEnabled(false);
        portfolioCombo.removeAllItems();
        typeCombo.setEnabled(false);
        requestCancelBtn.setEnabled(false);
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void stateChanged(ChangeEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == blockSpinner) {
            tf.setValue(blockSpinner.getValue());
            selectedQty = Long.parseLong(blockSpinner.getValue() + "");
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        //To change body of implemented methods use File | Settings | File Templates.
        Object source = evt.getSource();
        if (source == tf) {
            amount = ((Number) tf.getValue()).doubleValue();
        }
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        blockItemSelectedColor = Theme.getColor("FOREX_POPUP_SELECTED");
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
    }

    public class ForexBuySellUpdator extends Thread {


        public ForexBuySellUpdator() {
            super("ForexBoard_updator");
        }

        public void run() {
            while (true) {
                try {

                    bidBtn.update(sKey, Long.parseLong(blockSizeTrimer(tf.getText())), true);
                    askBtn.update(sKey, Long.parseLong(blockSizeTrimer(tf.getText())), true);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
