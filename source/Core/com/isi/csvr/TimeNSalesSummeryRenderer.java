package com.isi.csvr;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;


/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 29, 2009
 * Time: 12:41:55 PM
 * To change this template use File | Settings | File Templates.
 */

public class TimeNSalesSummeryRenderer extends TWBasicTableRenderer {


    private static ImageBorder imageBorder;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color upColor;
    private static Color downColor;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oTimeFormat = new TWDateFormat(" hh:mm:ss ");
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oNumericFormat;
    private String g_sNA = "NA";
    private double doubleValue;
    private VariationImage variationImage;
    private TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");


    public TimeNSalesSummeryRenderer() {
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
//            imageBorder = new ImageBorder(1, 0, 1, 0, g_oSelectedBG);
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            downColor = Color.red;
            upColor = Color.green;

        }

    }

    public void propertyChanged(int property) {

    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        variationImage = new VariationImage();

        reload();
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");
        oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    }

    public void setBidRenderer(boolean status) {
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        Color foreground, background;
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);
        variationImage.setHeight(table.getRowHeight());


        try {
            try {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            } catch (Exception e) {
                oPriceFormat = (((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalFormat());
            }
        } catch (Exception e) {
            // do nothing
        }

        int col = table.convertColumnIndexToModel(column);
        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 2: // DESCRIPTION
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 3: // PRICE
                    doubleValue = (Double) value;
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 4: // QUANTITY
                    doubleValue = (Double) value;
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oQuantityFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;

                case 5: // CHANGE
                    doubleValue = (Double) value;
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);

                    break;
                case 6: // % CHANGE
                    doubleValue = (Double) value;
                    lblRenderer.setText(oPChangeFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(g_oUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(g_oDownColor);


                    break;
                case 's':
                    if (((String) value).equals("null")) {
                        lblRenderer.setText("");
                    } else {
                        lblRenderer.setText((String) value);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;

                case 'h': // Cash Map
                    doubleValue = ((Double) value);
                    variationImage.setType(VariationImage.TYPE_CASH_MAP);
                    variationImage.setWidth(table.getColumnModel().getColumn(column).getWidth());
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setHorizontalTextPosition(g_iCenterAlign);
                    lblRenderer.setForeground(Color.black);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        variationImage.setValue(0);
                        lblRenderer.setText("");
                    } else {
                        variationImage.setValue(doubleValue);
                        lblRenderer.setIcon(variationImage);
                        lblRenderer.setText(oPChangeFormat.format(doubleValue * 100));
                    }
                    lblRenderer.setBackground(Color.BLACK);
                    break;

                default:
                    lblRenderer.setText("");

            }
        } catch (Exception e) {
            e.printStackTrace();
            lblRenderer.setText("");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }


    private int toIntValue(Object oValue) {
        try {
            return Integer.parseInt((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }

    private double toDoubleValue(Object oValue) {
        try {
            return Double.parseDouble((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }

}

