package com.isi.csvr.broadcastmessage;

import com.isi.csvr.Client;
import com.isi.csvr.Message;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 28, 2006
 * Time: 1:39:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageWindow extends InternalFrame implements MouseListener {

    private static MessageWindow self = null;
    private Table table;
    private SimpleDateFormat bodyDateFormat = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");
    private String template = null;
    private MessageModel model;
    private NewMessageDialog messageDialog;

    private MessageWindow() {
        super();
        table = new Table();
        model = new MessageModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_BROADCAST_MESSAGE");
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(true);
        table.setPreferredSize(new Dimension(500, 300));
        table.setWindowType(ViewSettingsManager.BROADCAST_MESSAGE_VIEW);
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();
        this.setTable(table);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(this);
        this.getContentPane().add(table);
        this.setTitle(Language.getString("BROADCAST_MESSAGE_WINDOW"));
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setTitle(Language.getString(oSetting.getCaptionID()));
        this.updateUI();
        this.applySettings();
        this.setLayer(GUISettings.TOP_LAYER);
        this.setVisible(false);
        table.getTable().addMouseListener(this);
        Client.getInstance().getDesktop().add(this);
    }

    public static MessageWindow getSharedInstance() {
        if (self == null) {
            self = new MessageWindow();
        }
        return self;
    }

    public boolean isDetachable() {
        return false;    //To change body of overridden methods use File | Settings | File Templates.
    }

    public InternalFrame getFrame() {
        return this;
    }

    public void mouseClicked(MouseEvent e) {
        final Message message = (Message) model.getValueAt(table.getTable().getSelectedRow(), -2);
        if (e.getClickCount() > 1) {
            SwingUtilities.invokeLater(new Thread("Broadcast message - id=" + message.getId()) {
                public void run() {
                    showMessage(message);
                }
            });

        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    private void createMessageBodyDialog(Message message) {
        if (messageDialog != null) {
            messageDialog.setVisible(false);
        }
        messageDialog = new NewMessageDialog();
        messageDialog.setTitle(Language.getString("BROADCAST_MESSAGE") + " - " + Language.getLanguageSpecificString(UnicodeUtils.getNativeString(message.getTitile())));
    }

    public synchronized void showMessage(final Message message) {
        try {
            createMessageBodyDialog(message);
            String body = generateTempFile(message);
            messageDialog.setText(body);
            messageDialog.setVisible(true);
            try {
                messageDialog.setSelected(true);
            } catch (PropertyVetoException e) {
            }
            message.setOldMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String replaceString(String buffer, String tag, String str) {
        int index = buffer.indexOf(tag);
        if (index >= 0) {
            return buffer.substring(0, index) + str + buffer.substring(index + tag.length());
        }
        return buffer;
    }

    public synchronized String generateTempFile(Message message) {

        try {
            loadTemplate(Language.getSelectedLanguage());
            String title = message.getTitile();
            String body = message.getBody();
            body = body.replaceAll("\r\n", "<br>"); // replace the line break with HTML line break
            body = body.replaceAll("\n", "<br>");   // replace the line break with HTML line break

            String page = replaceString(template, "[NEWS_TITLE]", Language.getLanguageSpecificString(UnicodeUtils.getNativeString(title)));

            if (body == null) {
                page = replaceString(page, "[NEWS_BODY]", Language.getLanguageSpecificString(UnicodeUtils.getNativeString(title)));
            } else {
                page = replaceString(page, "[NEWS_BODY]", Language.getLanguageSpecificString(UnicodeUtils.getNativeString(body)));
            }

//            Date date = new Date(Long.parseLong(message.getDate()));
//            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy '-' HH:mm");
//            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
//            String mDate = sdf.format(date);
//            bodyDateFormat.setTimeZone(Calendar.getInstance().getTimeZone());
//            String msgTime = bodyDateFormat.format(new Date(Long.parseLong(mDate)));


            page = replaceString(page, "[NEWS_DATE]", bodyDateFormat.format(new Date(Settings.getLocalTimeFor(Long.parseLong(message.getDate())))));
            return page;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void loadTemplate(String language) {
        StringBuilder buffer = new StringBuilder();
        byte[] temp = new byte[1000];
        int count = 0;

        try {
            if ((language == null) || (language.equals(""))) {
                language = Language.getLanguageTag();
            }
            InputStream in = new FileInputStream(".\\Templates\\BroadcastMessage_" + language + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            if (language == null) {
                if (Language.isLTR())
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            } else {
                if (language.toUpperCase().equals("EN"))
                    template = template.replaceFirst("\\[DIR\\]", "LTR");
                else
                    template = template.replaceFirst("\\[DIR\\]", "RTL");
            }
            buffer = null;
            in = null;
            temp = null;
        } catch (Exception ex) {
            template = "";
        }
    }

}
