package com.isi.csvr.broadcastmessage;

import com.isi.csvr.Message;
import com.isi.csvr.MessageList;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 28, 2006
 * Time: 1:44:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class MessageModel extends CommonTable implements TableModel, CommonTableInterface {

    SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
    private LinkedList<Message> dataStore;

    /**
     * Constructor
     */
    public MessageModel() {
        //init();
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        if (MessageList.getSharedInstance().getStore() != null) {
//System.out.println(dataStore. size());
            return MessageList.getSharedInstance().getStore().size();
        } else {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            Message message = MessageList.getSharedInstance().getStore().get(getRowCount() - 1 - iRow);

            switch (iCol) {
                case 0:
                    return "" + Settings.getLocalTimeFor(Long.parseLong(message.getDate())); //formatDate.format(new Date(Long.parseLong(message.getDate())));
                case 1:
                    return Language.getLanguageSpecificString(UnicodeUtils.getNativeString(message.getTitile()));
                case 2:
                    return Language.getLanguageSpecificString(UnicodeUtils.getNativeString(message.getBody()));
                case 3:
                    if (message.isExpired()) {
                        return Language.getString("BRODCAST_MESSAGE_EXPIRED");
                    } else {
                        return Language.getString("BRODCAST_MESSAGE_VALID");
                    }
                case -1:
                    return "" + message.getNewMessage();
                case -2:
                    return message;
                case -3:
                    return message.getId();

            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 3:
                return String.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
//        customizerRecords[4] = new CustomizerRecord(Language.getString("NEW_ANNOUNCEMENTS"),FIELD_NEW_ANNOUNCEMENT, null, Theme.getColor("ANNOUNCEMENT_NEW_LINE_FGCOLOR"));
        return customizerRecords;
    }

    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }
}
