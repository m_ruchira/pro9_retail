package com.isi.csvr.broadcastmessage;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.ContextMenuProvider;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 28, 2006
 * Time: 4:00:09 PM
 */
public class NewMessageDialog extends InternalFrame
        implements Themeable {

    private Browser browserPane;

    public NewMessageDialog() {
        try {
            jbInit();
            Theme.registerComponent(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show(int type) {
        super.show();
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() {
        this.setSize(new Dimension(Settings.BROADCAST_MESS_WIDTH, Settings.BROADCAST_MESS_HEIGHT));
        this.setLayout(new BorderLayout(0, 0));
        browserPane = new Browser();
        browserPane.getProperties().setAllowContextMenu(false);
        browserPane.getProperties().setAllowRunActiveX(true);
        browserPane.getProperties().setAllowScripts(true);
        ContextMenuProvider provider = new ContextMenuProvider() {
            public JPopupMenu getPopupMenu(Element contextElement) {
                return null; // don't show any menu
            }
        };
        browserPane.setContextMenuProvider(provider);
        this.getContentPane().add(browserPane, BorderLayout.CENTER);

        this.setResizable(true);
        this.setClosable(true);
//        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);


        ViewSetting oBrowserSettings = ViewSettingsManager.getSummaryView("BROWSER");
        if (oBrowserSettings != null) {
            setPreferredSize(oBrowserSettings.getSize());
//            setSize(oBrowserSettings.getSize());
        } else {
            setPreferredSize(new Dimension(300, 200));
//            setSize(new Dimension(300, 200));
        }
        setLayer(GUISettings.TOP_LAYER);
        GUISettings.setLocationRelativeTo(this, Client.getInstance().getDesktop());
        setResizable(true);
        setMaximizable(true);
        setIconifiable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setCenterLocation();
        applyTheme();

    }

    public void setCenterLocation() {
        this.setBounds(((Client.getInstance().getDesktop().getWidth() - this.getWidth()) / 2),
                ((Client.getInstance().getDesktop().getHeight() - this.getHeight()) / 2),
                this.getWidth(), this.getHeight());
    }

    public Dimension getPreferredSize() {
        Dimension parentSize = super.getPreferredSize();
        if (parentSize.getWidth() < 250) {
            parentSize.width = 250;
        } else if (parentSize.getWidth() > 300) {
            parentSize.width = 300;
        }
        if (parentSize.height < 150) {
            parentSize.height = 150;
        } else if (parentSize.height > 200) {
            parentSize.height = 200;
        }

        return parentSize;
    }


    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    public String toString() {
        return this.getTitle();
    }

    public void setText(String text) {
        try {
            browserPane.setContent(text);
            browserPane.waitReady();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setWIndowTitle(String title) {
        super.setTitle(title);
    }
}
