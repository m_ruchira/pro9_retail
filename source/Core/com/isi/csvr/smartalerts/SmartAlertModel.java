package com.isi.csvr.smartalerts;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 18, 2009
 * Time: 9:58:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private ArrayList<SmartAlertObject> records;

    public SmartAlertModel(ArrayList<SmartAlertObject> dataArray) {
        this.records = dataArray;
    }

    public SmartAlertModel() {
    }

    public void setRecords(ArrayList<SmartAlertObject> records) {
        this.records = records;
    }

    /* --- Table Model's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
//        return 4;
    }

    public int getRowCount() {
        return records.size();
    }

    public Object getValueAt(int iRow, int iCol) {
        SmartAlertObject smartOb = (SmartAlertObject) records.get(iRow);
        if (smartOb == null) {
            return "fff"; //null;
        }
        switch (iCol) {
            case -4:
                return smartOb.isFirstElement();
            case -1:
                return smartOb.getParameterID();
            case 0:
                return "";
            case 1:
                return "";
            case 2:
                return smartOb.getSymbol();
            case 3:
                return smartOb.getParameter();
            case 4:
                return smartOb.getCriteria();
            case 5:
                return smartOb.getValue();
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public boolean isCellEditable(int row, int col) {
        /* if (col != 1) {
            String symbol = (String) getValueAt(row, 1);
            if (symbol == null || symbol.isEmpty() || symbol.equals(SmartAlertObject.CLICK_MSG)) {
                return false;
            }
        }
        *//*if (row == 0) {
            return true;
        } else if (col != 0) {
            return true;
        }*//*
        if (col == 0) {
            return false;
        } else {
            return true;
        }*/
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String input;

        /*if (columnIndex != 1) {
            String symbol = (String) getValueAt(rowIndex, 1);
            if (symbol == null || symbol.isEmpty() || symbol.equals(SmartAlertObject.CLICK_MSG)) {
                fireTableCellUpdated(rowIndex, columnIndex);
                new ShowMessage(Language.getString("SMART_ALERT_NO_SYMBOL"), "E");
                SmartAlertWindow.getSharedInstance().setSetValErrorShown(true);
                return;
            }
        }
        if (columnIndex == 4) {
            boolean isvalid = validateValueAmt(aValue, rowIndex, columnIndex);
            if (!isvalid) {
                fireTableCellUpdated(rowIndex, columnIndex);
                new ShowMessage(Language.getString("SMART_ALERT_VALUE_MISMATCH"), "E");
                SmartAlertWindow.getSharedInstance().setSetValErrorShown(true);
                return;
            }
        } else if (columnIndex == 2) {
            String val = (String) getValueAt(rowIndex, 4);
            if (val != null) {
                boolean isvalid = validatePatameter(((TWComboItem) aValue).getId(), rowIndex, val);
                if (!isvalid) {
                    fireTableCellUpdated(rowIndex, columnIndex);
                    new ShowMessage(Language.getString("SMART_ALERT_VALUE_MISMATCH"), "E");
                    SmartAlertWindow.getSharedInstance().setSetValErrorShown(true);
                    return;
                }
            }
        }
        if (columnIndex == 3 || columnIndex == 2) {
            try {
                input = ((TWComboItem) aValue).getValue();
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return;
            }
        } else {
            input = ((String) aValue);
        }
        SmartAlertObject alertOb = (SmartAlertObject) records.get(rowIndex);
        if (alertOb == null) {

        }
        switch (columnIndex) {
            case 1:
                alertOb.setSymbol(input);
                // alertOb.setSKey(((TWComboItem)aValue).getId());
                //alertOb.setSKey(((SmartAlertWindow.SpecialTWTextField)aValue).getItem().getId());
                break;
                //===========

                //=========
            case 2:
                alertOb.setParameter(input);
                alertOb.setParameterID(((TWComboItem) aValue).getId());
                break;
            case 3:
                alertOb.setCriteria(input);
                alertOb.setCriteriaID(((TWComboItem) aValue).getId());
                break;
            case 4:
                alertOb.setValue(input);
                break;
        }*/

        fireTableCellUpdated(rowIndex, columnIndex);

    }

    public boolean validateValueAmt(Object aValue, int row, int col) {
        String parmId = (String) getValueAt(row, -1);
        return SmartAlertUtils.isValueValid(parmId, (String) aValue);

    }

    public boolean validatePatameter(String pramID, int row, String aValue) {
        return SmartAlertUtils.isValueValid(pramID, (String) aValue);
    }


    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }

    public void setSymbol(String symbol) {

    }

    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return String.class;
        } else {
            return String.class;
        }

    }
}
