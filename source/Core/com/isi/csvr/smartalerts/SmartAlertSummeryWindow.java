package com.isi.csvr.smartalerts;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 30, 2009
 * Time: 10:42:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertSummeryWindow extends InternalFrame implements ActionListener, Themeable, MouseListener, PopupMenuListener {
    public static SmartAlertSummeryWindow self;
    ViewSetting oSetting1;
    private JPanel mainPanel;
    private Table summaryTable;
    private SmartAlertSummaryModel smodel;
    private JPopupMenu popup;
    private TWMenuItem edit;
    private TWMenuItem delete;
    private String selectdClientAlId = "";


    private ArrayList<SmartAlertObject> dataArray2 = new ArrayList();

    private SmartAlertSummeryWindow() {
        this.setVisible(false);
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        createUI();
        getContentPane().add(mainPanel);

        setSize(500, 300);
        setResizable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        Client.getInstance().getDesktop().add(this);
        if (!oSetting1.isLocationValid()) {
            this.setLocationRelativeTo(Client.getInstance().getDesktop());
        } else {
            this.setLocation(oSetting1.getLocation());
        }
        applyTheme();
        summaryTable.getTable().addMouseListener(this);
        edit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId) != null) {
                    SmartAlertWindow.getSharedInstance().setVisible(true);
                    SmartAlertWindow.getSharedInstance().clearAll();
                    SmartAlertWindow.getSharedInstance().setAlertFrame(SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId));
                    SmartAlertWindow.getSharedInstance().loadSmartAleartForSymbol(SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId).getSKey()); //commented
                    try {
                        SmartAlertWindow.getSharedInstance().setSelected(true);
                    } catch (PropertyVetoException e1) {
                        e1.printStackTrace();
                    }

                }
            }
        });

        delete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId) != null) {
                    SmartAlertManager.getSharedInstance().deleteRecord(selectdClientAlId);
                }
            }
        });
        Theme.registerComponent(this);
        oSetting1.setTableNumber(1);
        this.setTable(summaryTable);
        setTitle(Language.getString("SMART_ALERT_SUMMARY"));
        this.applySettings();
        setLayer(GUISettings.TOP_LAYER);

    }

    public static SmartAlertSummeryWindow getSharedInstance() {
        if (self == null) {
            self = new SmartAlertSummeryWindow();
        }
        return self;
    }

    private void createUI() {
        oSetting1 = ViewSettingsManager.getSummaryView("SMART_ALERT_SUMMARY");
        oSetting1.setParent(this);
        mainPanel.add(getAlertDataTable());
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(mainPanel);
        setLayer(GUISettings.TOP_LAYER);
        popup = new JPopupMenu();
        edit = new TWMenuItem(Language.getString("EDIT"));
        delete = new TWMenuItem(Language.getString("DELETE"));
        popup.add(edit);
        popup.add(delete);
        popup.addPopupMenuListener(this);
        GUISettings.applyOrientation(popup);

    }

    private Table getAlertDataTable() {
        summaryTable = new Table();  //-Table 2
        smodel = new SmartAlertSummaryModel(dataArray2);
        addStoredObjects();
        smodel.setViewSettings(oSetting1);
        oSetting1.setParent(this);
        summaryTable.setModel(smodel);
        smodel.setTable(summaryTable, new SmartAlertSummeryRenderer());
        summaryTable.getTable().getTableHeader().setReorderingAllowed(false);

        Theme.registerComponent(summaryTable);
        summaryTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int col = summaryTable.getTable().columnAtPoint(e.getPoint());
                int row = summaryTable.getTable().rowAtPoint(e.getPoint());

                if (col == 0) { // expander column
                    Object state = smodel.getValueAt(row, col);
                    int status = Integer.parseInt(state.toString());

                    String clientid = (String) smodel.getValueAt(row, -1);

                    if (status == 2) {
                        showExpandedTree(clientid);
                    } else if (status == 1) {
                        showCollapsedTree(clientid);
                    }
                }
                repaint();
            }
        });

        summaryTable.getModel().updateGUI();

        return summaryTable;
    }

    private void addStoredObjects() {
        dataArray2.clear();
        int rownum = 0;
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getSmartAlertsStoreForSymbols();
        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();

            int noOfchildren = 0;
            boolean isBaseob = false;
            if (subObList.size() > 1) {
                noOfchildren = 1;
                isBaseob = true;
            }
            for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                SmartAlertObject smartOb = subObList.get(j);
                smartOb.setAlertrownum(rownum);
                if (noOfchildren == 1) {
                    if (isBaseob) {
                        smartOb.setAlertstatus(2);
                    } else {
                        smartOb.setAlertstatus(0);
                    }
                } else {
                    smartOb.setAlertstatus(0);
                }
                if (j == 0) {
                    dataArray2.add(smartOb);
                    isBaseob = false;
                }
            }
            rownum = rownum + 1;
        }
    }

    private void showExpandedTree(String clientid) {
        dataArray2.clear();
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getSmartAlertsStoreForSymbols();

        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();
            int noOfchildren = 0;
            if (subObList.size() > 1) {
                noOfchildren = 1;
            }
            int childNum = 2;
            boolean isExpanded = false;
            int status = 0;
            String id = alertFrame.getClientAlertID();
            if (id.equals(clientid)) {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getAlertstatus();
                    if (status == 2) {
                        smartOb.setAlertstatus(1);
                    } else {
                        smartOb.setAlertstatus(0);
                    }
                    dataArray2.add(smartOb);
                }
            } else {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getAlertstatus();
                    if (status == 2) {
                        smartOb.setAlertstatus(2);
                    } else if (status == 1) {
                        smartOb.setAlertstatus(1);
                        isExpanded = true;
                    } else {
                        smartOb.setAlertstatus(0);
                    }
                    if (isExpanded) {
                        dataArray2.add(smartOb);
                    } else {
                        if (j == 0) {
                            dataArray2.add(smartOb);
                        }
                    }
                }
            }
        }
    }

    private void showCollapsedTree(String clientid) {
        dataArray2.clear();
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getSmartAlertsStoreForSymbols();

        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();
            int noOfchildren = 0;
            if (subObList.size() > 1) {
                noOfchildren = 1;
            }
            boolean isExpanded = true;
            int status = 0;
            String id = alertFrame.getClientAlertID();
            if (id.equals(clientid)) {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getAlertstatus();
                    if (status == 1) {
                        smartOb.setAlertstatus(2);
                        isExpanded = false;
                    } else {
                        smartOb.setAlertstatus(0);
                    }
                    if (isExpanded) {
                        dataArray2.add(smartOb);
                    } else {
                        if (j == 0) {
                            dataArray2.add(smartOb);
                        }
                    }
                }
            } else {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getAlertstatus();
                    if (status == 2) {
                        smartOb.setAlertstatus(2);
                        isExpanded = false;
                    } else if (status == 1) {
                        smartOb.setAlertstatus(1);
                        isExpanded = true;
                    } else {
                        smartOb.setAlertstatus(0);
                    }
                    if (isExpanded) {
                        dataArray2.add(smartOb);
                    } else {
                        if (j == 0) {
                            dataArray2.add(smartOb);
                        }
                    }
                }
            }
        }
    }


    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        selectdClientAlId = (String) smodel.getValueAt(summaryTable.getTable().getSelectedRow(), -1);
        String trigstatus = SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId).getAleartStatus();
        if (trigstatus != null && !trigstatus.isEmpty()) {
            if (trigstatus.equalsIgnoreCase("P")) {
                edit.setVisible(true);
                delete.setVisible(true);
            } else {
                edit.setVisible(false);
                delete.setVisible(false);
            }
        } else {
            edit.setVisible(false);
            delete.setVisible(false);
        }
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(summaryTable.getTable()) && SwingUtilities.isRightMouseButton(e)) {
            GUISettings.showPopup(popup, summaryTable.getTable(), e.getX(), e.getY());
        } else if (e.getSource().equals(summaryTable.getTable()) && e.getClickCount() > 1) {
            selectdClientAlId = (String) smodel.getValueAt(summaryTable.getTable().getSelectedRow(), -1);
            String trigstatus = SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId).getAleartStatus();
            boolean canedit = false;
            if (trigstatus != null && !trigstatus.isEmpty()) {
                if (trigstatus.equalsIgnoreCase("P")) {
                    canedit = true;
                } else {
                    canedit = false;
                }
            } else {
                canedit = false;
            }
            if (SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId) != null && canedit) {
                SmartAlertWindow.getSharedInstance().setVisible(true);
                SmartAlertWindow.getSharedInstance().clearAll();
                SmartAlertWindow.getSharedInstance().setAlertFrame(SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId));
                SmartAlertWindow.getSharedInstance().loadSmartAleartForSymbol(SmartAlertStore.getSharedInstance().getRecord(selectdClientAlId).getSKey()); //commented

            }
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        this.setVisible(false);
    }

    public void frameAddeded() {
        addStoredObjects();
        summaryTable.getTable().repaint();
    }

    public void applyTheme() {

    }


}
