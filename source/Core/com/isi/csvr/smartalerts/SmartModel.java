package com.isi.csvr.smartalerts;

import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 20, 2009
 * Time: 9:59:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private static final DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private ArrayList<SmartAlertObject> records;


    public SmartModel(ArrayList<SmartAlertObject> dataArray) {
        this.records = dataArray;
    }

    public SmartModel() {
    }

    /* --- Table Model's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
//        return 5;
    }

    public int getRowCount() {
        return records.size();
    }

    public Object getValueAt(int iRow, int iCol) {
        SmartAlertObject smartOb = (SmartAlertObject) records.get(iRow);
        if (smartOb == null) {
            return "fff"; //null;
        }
        switch (iCol) {
            case -2:
                return "" + smartOb.getRownum();
            case -1:
                return smartOb.getClientId();
            case 0:
                return "" + smartOb.getStatus();
            case 1:
                return smartOb.getSymbol();
            case 2:
                return smartOb.getParameter();
            case 3:
                return smartOb.getCriteria();
            case 4:
                return smartOb.getValue();
            case 5:
                return smartOb.getMessageStatus();
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }


    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public void windowClosing() {

    }

    public void setSymbol(String symbol) {

    }

    public void rebuildStore() {
        records.clear();
        records.trimToSize();
    }

//    public Class<?> getColumnClass(int columnIndex) {
//        if (columnIndex == 0) {
//            return Double.class;
//        } else {
//            return String.class;
//        }
//    }
}
