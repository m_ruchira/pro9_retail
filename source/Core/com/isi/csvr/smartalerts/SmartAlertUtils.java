package com.isi.csvr.smartalerts;

import com.isi.csvr.alert.Common_Functions;
import com.isi.csvr.alert.FieldComboItem;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jan 27, 2009
 * Time: 8:39:22 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertUtils {

    private static Hashtable<String, String> fieldFormatsStock;
    private static Hashtable<String, String> conditions;
    private static Hashtable<String, String> validity;
    private static Hashtable<String, String> expiration;
    private static String statusPending;
    private static String statusTriggered;
    private static String[] modes;

    private static DecimalFormat priceFormatter;
    private static DecimalFormat quantityFormatter;

    public static final int EXPANSION_STATUS_NO_CHILDREN = 0;
    public static final int EXPANSION_STATUS_EXPANDED = 1;
    public static final int EXPANSION_STATUS_COLLAPSED = 2;
    public static final int EXPANSION_STATUS_NA = 3;

    static {
        fieldFormatsStock = new Hashtable();
        fieldFormatsStock.put("" + Meta.ALERT_LASTTRADEPRICE, Language.getString("LAST"));
        fieldFormatsStock.put("" + Meta.ALERT_LASTTRADEVOL, Language.getString("LASTTRADE_QTY"));
        fieldFormatsStock.put("" + Meta.ALERT_BID, Language.getString("BID"));
        fieldFormatsStock.put("" + Meta.ALERT_BIDQTY, Language.getString("BID_QTY"));
        fieldFormatsStock.put("" + Meta.ALERT_ASK, Language.getString("OFFER"));
        fieldFormatsStock.put("" + Meta.ALERT_OFFERQTY, Language.getString("OFFER_QTY"));
        fieldFormatsStock.put("" + Meta.ALERT_CUMVOLUME, Language.getString("T_VOLUME"));
        fieldFormatsStock.put("" + Meta.ALERT_CHANGE, Language.getString("PCT_CHANGE"));
        fieldFormatsStock.put("" + Meta.ALERT_CHANGE, Language.getString("CHANGE_SHORT"));
        fieldFormatsStock.put("" + Meta.ALERT_PERCENTCHANGE, Language.getString("PCT_CHANGE"));
        fieldFormatsStock.put("" + Meta.ALERT_OPEN_INTREST, Language.getString("OPEN_INTREST"));

        conditions = new Hashtable();
        conditions.put("" + Meta.ALERT_GRATERTHAN, Language.getString("GREATER_THAN"));
        conditions.put("" + Meta.ALERT_GREATERTHANOREQUAL, Language.getString("GREATER_THAN_OR_EQUAL"));
        conditions.put("" + Meta.ALERT_LESSTHAN, Language.getString("LESS_THAN"));
        conditions.put("" + Meta.ALERT_LESSTHANEQUAL, Language.getString("LESS_THAN_OR_EQUAL"));
        conditions.put("" + Meta.ALERT_EQUAL, Language.getString("EQUAL"));

        validity = new Hashtable();
        validity.put("" + Meta.ALERT_EXPIRED_ONE_TIME, Language.getString("ONCE_ONLY"));
        validity.put("" + Meta.ALERT_EXPIRED_UNTIL_CANCEL, Language.getString("ONCE_A_DAY"));

        expiration = new Hashtable();
        expiration.put("" + 1, Language.getString("1_DAY"));
        expiration.put("" + 2, Language.getString("2_DAYS"));
        expiration.put("" + 3, Language.getString("3_DAYS"));
        expiration.put("" + 7, Language.getString("SAM_1_WEEK"));
        expiration.put("" + 14, Language.getString("SAM_2_WEEK"));
        expiration.put("" + 30, Language.getString("30_DAYS"));

        statusPending = Language.getString("PENDING");
        statusTriggered = Language.getString("TRIGGERED");

        modes = new String[8];
        modes[0] = Language.getString("NA");
        modes[1] = Language.getString("POPUP");
        modes[2] = Language.getString("SMS");
        modes[3] = Language.getString("POPUP") + "/" + Language.getString("SMS");
        modes[4] = Language.getString("EMAIL");
        modes[5] = Language.getString("POPUP") + "/" + Language.getString("EMAIL");
        modes[6] = Language.getString("SMS") + "/" + Language.getString("EMAIL");
        modes[7] = Language.getString("POPUP") + "/" + Language.getString("SMS") + "/" + Language.getString("EMAIL");

        priceFormatter = new DecimalFormat("#,##0.00");
        quantityFormatter = new DecimalFormat("#,##0");

    }

    public static FieldComboItem getFieldItem(int value) {
        return new FieldComboItem((String) fieldFormatsStock.get("" + value), value);
    }

    public static FieldComboItem getConditionItem(int value) {
        return new FieldComboItem((String) conditions.get("" + value), value);
    }

    public static FieldComboItem getValidityItem(int value) {
        return new FieldComboItem((String) validity.get("" + value), value);
    }

    public static FieldComboItem getExpirationItem(int value) {
        return new FieldComboItem((String) expiration.get("" + value), value);
    }

    public static String getCriteria(String value) {
        try {
            return (String) fieldFormatsStock.get("" + value);
        } catch (NumberFormatException e) {
            return "";
        }
    }

    public static String getAlertMode(int mode) {
        try {
            return modes[mode];
        } catch (Exception e) {
            return modes[0];
        }
    }

    public static String getStatusString(boolean triggered) {
        if (triggered)
            return statusTriggered;
        else
            return statusPending;
    }

    private static String formatQuantity(float number) {
        try {
            return quantityFormatter.format(number);
        } catch (Exception e) {
            return "0";
        }
    }

    private static String formatPrice(float number) {
        try {
            return priceFormatter.format(number);
        } catch (Exception e) {
            return "0";
        }
    }

    public static String getFormattedString(float number, int column) {
        switch (column) {
            case Meta.ALERT_LASTTRADEPRICE:
            case Meta.ALERT_BID:
            case Meta.ALERT_ASK:
            case Meta.ALERT_CHANGE:
            case Meta.ALERT_PERCENTCHANGE:
                return formatPrice(number);
            case Meta.ALERT_LASTTRADEVOL:
            case Meta.ALERT_BIDQTY:
            case Meta.ALERT_OFFERQTY:
            case +Meta.ALERT_CUMVOLUME:
                return formatQuantity(number);
            default:
                return "" + number;
        }
    }

    public static void getFilledParameterComboForStock(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_LASTTRADEPRICE, fieldFormatsStock.get("" + Meta.ALERT_LASTTRADEPRICE)));
            comboBox.add(new TWComboItem(Meta.ALERT_LASTTRADEVOL, fieldFormatsStock.get("" + Meta.ALERT_LASTTRADEVOL)));
            comboBox.add(new TWComboItem(Meta.ALERT_BID, fieldFormatsStock.get("" + Meta.ALERT_BID)));
            comboBox.add(new TWComboItem(Meta.ALERT_BIDQTY, fieldFormatsStock.get("" + Meta.ALERT_BIDQTY)));
            comboBox.add(new TWComboItem(Meta.ALERT_ASK, fieldFormatsStock.get("" + Meta.ALERT_ASK)));
            comboBox.add(new TWComboItem(Meta.ALERT_OFFERQTY, fieldFormatsStock.get("" + Meta.ALERT_OFFERQTY)));
            comboBox.add(new TWComboItem(Meta.ALERT_CUMVOLUME, fieldFormatsStock.get("" + Meta.ALERT_CUMVOLUME)));
            comboBox.add(new TWComboItem(Meta.ALERT_CHANGE, fieldFormatsStock.get("" + Meta.ALERT_CHANGE)));
            comboBox.add(new TWComboItem(Meta.ALERT_PERCENTCHANGE, fieldFormatsStock.get("" + Meta.ALERT_PERCENTCHANGE)));
        }
    }

    public static void getFilledParameterComboForStockIndex(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_LASTTRADEPRICE, fieldFormatsStock.get("" + Meta.ALERT_LASTTRADEPRICE)));
            comboBox.add(new TWComboItem(Meta.ALERT_CHANGE, fieldFormatsStock.get("" + Meta.ALERT_CHANGE)));
            comboBox.add(new TWComboItem(Meta.ALERT_PERCENTCHANGE, fieldFormatsStock.get("" + Meta.ALERT_PERCENTCHANGE)));
            comboBox.add(new TWComboItem(Meta.ALERT_CUMVOLUME, fieldFormatsStock.get("" + Meta.ALERT_CUMVOLUME)));
        }
    }

    public static void getFilledParameterComboForStockDerivative(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_LASTTRADEPRICE, fieldFormatsStock.get("" + Meta.ALERT_LASTTRADEPRICE)));
            comboBox.add(new TWComboItem(Meta.ALERT_LASTTRADEVOL, fieldFormatsStock.get("" + Meta.ALERT_LASTTRADEVOL)));
            comboBox.add(new TWComboItem(Meta.ALERT_BID, fieldFormatsStock.get("" + Meta.ALERT_BID)));
            comboBox.add(new TWComboItem(Meta.ALERT_BIDQTY, fieldFormatsStock.get("" + Meta.ALERT_BIDQTY)));
            comboBox.add(new TWComboItem(Meta.ALERT_ASK, fieldFormatsStock.get("" + Meta.ALERT_ASK)));
            comboBox.add(new TWComboItem(Meta.ALERT_OFFERQTY, fieldFormatsStock.get("" + Meta.ALERT_OFFERQTY)));
            comboBox.add(new TWComboItem(Meta.ALERT_CUMVOLUME, fieldFormatsStock.get("" + Meta.ALERT_CUMVOLUME)));
            comboBox.add(new TWComboItem(Meta.ALERT_CHANGE, fieldFormatsStock.get("" + Meta.ALERT_CHANGE)));
            comboBox.add(new TWComboItem(Meta.ALERT_PERCENTCHANGE, fieldFormatsStock.get("" + Meta.ALERT_PERCENTCHANGE)));
            comboBox.add(new TWComboItem(Meta.ALERT_OPEN_INTREST, fieldFormatsStock.get("" + Meta.ALERT_OPEN_INTREST)));
        }
    }

    public static void getFilledCondtionCombo(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_GRATERTHAN, conditions.get("" + Meta.ALERT_GRATERTHAN)));
            comboBox.add(new TWComboItem(Meta.ALERT_GREATERTHANOREQUAL, conditions.get("" + Meta.ALERT_GREATERTHANOREQUAL)));
            comboBox.add(new TWComboItem(Meta.ALERT_LESSTHAN, conditions.get("" + Meta.ALERT_LESSTHAN)));
            comboBox.add(new TWComboItem(Meta.ALERT_LESSTHANEQUAL, conditions.get("" + Meta.ALERT_LESSTHANEQUAL)));
            comboBox.add(new TWComboItem(Meta.ALERT_EQUAL, conditions.get("" + Meta.ALERT_EQUAL)));
        }
    }

    public static void getFilledNotificationCombo(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_EXPIRED_ONE_TIME, validity.get("" + Meta.ALERT_EXPIRED_ONE_TIME)));
            comboBox.add(new TWComboItem(Meta.ALERT_EXPIRED_UNTIL_CANCEL, validity.get("" + Meta.ALERT_EXPIRED_UNTIL_CANCEL)));
        }
    }

    public static void getFilledExpirationCombo(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem("" + 1, expiration.get("" + 1)));
            comboBox.add(new TWComboItem("" + 2, expiration.get("" + 2)));
            comboBox.add(new TWComboItem("" + 3, expiration.get("" + 3)));
            comboBox.add(new TWComboItem("" + 7, expiration.get("" + 7)));
            comboBox.add(new TWComboItem("" + 14, expiration.get("" + 14)));
            comboBox.add(new TWComboItem("" + 30, expiration.get("" + 30)));
        }

    }

    public static void getFilledOperatorCombo(ArrayList<TWComboItem> comboBox) {
        if (comboBox != null) {
            comboBox.add(new TWComboItem(Meta.ALERT_OPERATOR_AND, Language.getString("SMART_MATCH_ALL")));
            comboBox.add(new TWComboItem(Meta.ALERT_OPERATOR_OR, Language.getString("SMART_MATCH_ANY")));
        }

    }

    public static ArrayList<SmartAlertObject> parseParamerString(String condtions, String clientID) {
        ArrayList<SmartAlertObject> alearts = new ArrayList<SmartAlertObject>();
        if (condtions != null && !condtions.isEmpty()) {
            String[] firstLevel = condtions.split(Meta.ID);
            if (firstLevel != null && firstLevel.length > 0) {
                for (int i = 0; i < firstLevel.length; i++) {
                    if (i % 2 == 0) {
                        String parmString = firstLevel[i];
                        String[] list = parmString.split("\\" + String.valueOf(Common_Functions.DELM));
                        if (list.length == 4) {
                            SmartAlertObject obj = new SmartAlertObject();
                            obj.setSKey(list[0]);
                            obj.setSymbol(SharedMethods.getSymbolFromKey(list[0]));
                            obj.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(list[0]));
                            obj.setParameterID(list[1]);
                            obj.setParameter(fieldFormatsStock.get(list[1]));
                            obj.setCriteriaID(list[2]);
                            obj.setCriteria(conditions.get(list[2]));
                            obj.setValue(list[3]);
                            obj.setClientId(clientID);
                            alearts.add(obj);
                        }


                    }
                }

            }
            return alearts;

        }
        return null;
    }

    public static String processTriggerString(String triggerString, String sKey) {
        String trggerMsg = "";
        if (triggerString != null && !triggerString.isEmpty()) {
            String[] firstLevel = triggerString.split(Meta.ID);
            String symbol = SharedMethods.getSymbolFromKey(sKey);
            Stock stk = DataStore.getSharedInstance().getStockObject(sKey);
            String symbol1 = null;
            if (stk != null) {
                symbol1 = stk.getLongDescription();

            } else {
                symbol1 = SharedMethods.getSymbolFromKey(symbol);
            }
            if (symbol1 != null) {
                trggerMsg = symbol1 + "\n";
            }

            if (firstLevel != null && firstLevel.length > 0) {
                for (int i = 0; i < firstLevel.length; i++) {
                    String secondL = firstLevel[i];
                    if (secondL.isEmpty()) {
                        continue;
                    }
                    String[] params = secondL.split("=");
                    if (params != null && params.length == 2) {
                        String idString = fieldFormatsStock.get(params[0]);
                        String valString = params[1];
                        //trggerMsg = trggerMsg + idString +" "+valString  +"\n";

                        trggerMsg = trggerMsg + getFormatedString(Integer.parseInt(params[0]), valString, sKey) + "\n";
                    }


                }
            }

        }
        return trggerMsg;

    }

    private static String getFormatedString(int idString, String valString, String sKey) {
        TWDecimalFormat formatter;

        //formatter = SharedMethods.getDecimalFormat(sKey);
        //  formatter = new TWDecimalFormat();
        String FormatedVal;
        String idstr;
        String finalString = "";
        try {
            switch (idString) {
                case Meta.ALERT_LASTTRADEPRICE:
                case Meta.ALERT_BID:
                case Meta.ALERT_ASK:
                case Meta.ALERT_CHANGE:
                case Meta.ALERT_PERCENTCHANGE:
                    // FormatedVal= formatter.format(Double.parseDouble(valString));
                    FormatedVal = priceFormatter.format(Double.parseDouble(valString));
                    break;
                default:
                    FormatedVal = formatQuantity(Float.parseFloat(valString));//  valString;
            }

            idstr = fieldFormatsStock.get("" + idString);
            finalString = idstr + " " + FormatedVal;
        } catch (NumberFormatException e) {
            finalString = fieldFormatsStock.get("" + idString) + " " + valString;
        }
        return finalString;
    }

    public static boolean isValueValid(String idString, String val) {
        try {
            int id = Integer.parseInt(idString);
            Object obj;

            switch (id) {
                case Meta.ALERT_LASTTRADEPRICE:
                case Meta.ALERT_BID:
                case Meta.ALERT_ASK:
                case Meta.ALERT_CHANGE:
                case Meta.ALERT_PERCENTCHANGE:
                    obj = (Double.parseDouble(val));
                    break;
                default:
                    obj = Integer.parseInt(val);//  valString;
            }

            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static String getNotificationString(int i) {
        if (validity.containsKey(i + "")) {
            return validity.get(i + "");
        } else {
            return "";
        }

    }

    public static String getExpirationString(int i) {
        if (expiration.containsKey(i + "")) {
            return expiration.get(i + "");
        } else {
            return "";
        }
    }
}
