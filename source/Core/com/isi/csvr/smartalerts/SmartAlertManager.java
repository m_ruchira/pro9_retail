package com.isi.csvr.smartalerts;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jan 23, 2009
 * Time: 8:03:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertManager {


    private static SmartAlertManager self;

    private static FileOutputStream aWriter;
    private static FileOutputStream aWriter2;


    private SmartAlertManager() {

    }

    public static SmartAlertManager getSharedInstance() {
        if (self == null) {
            self = new SmartAlertManager();
        }
        return self;
    }

    public static void writeLog(String s) {
//       long time = System.currentTimeMillis() + (330*60*1000);
//         Date now = new Date(time);
//         DateFormat df = new SimpleDateFormat("yyyyMMdd-hh-mm-ss ");
//         String currentTime = df.format(now);
//       try {
//           if(aWriter==null){
//                aWriter =new FileOutputStream("alertReqlog_"+ currentTime.trim() +".txt");
//           }
//           aWriter.write( currentTime.getBytes());
//           aWriter.write( new String("\t").getBytes());
//           aWriter.write( s.getBytes());
//           aWriter.write( new String("\n").getBytes());
//           aWriter.flush();
//       } catch (IOException e) {
//           e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//       }
    }

    public static void writeReplyLog(String s) {
//          long time = System.currentTimeMillis() + (330*60*1000);
//         Date now = new Date(time);
//         DateFormat df = new SimpleDateFormat("yyyyMMdd-hh-mm-ss ");
//         String currentTime = df.format(now);
//       try {
//           if(aWriter2==null){
//          aWriter2 = new FileOutputStream("alertReplylog_"+currentTime.trim() +".txt");
//           }
//            aWriter2.write( currentTime.getBytes());
//           aWriter2.write( new String("\t").getBytes());
//           aWriter2.write( s.getBytes());
//           aWriter2.write( new String("\n").getBytes());
//           aWriter2.flush();
//       } catch (IOException e) {
//           e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//       }
    }

    public void processMessage(String frame) {
        if (frame != null && !frame.isEmpty()) {
            //writeReplyLog(frame);
            SmartAlertFrame smartAleart = new SmartAlertFrame(frame);

            String clientid = smartAleart.getClientAlertID();
            String msgType = smartAleart.getMessageType();
            if (smartAleart.getAleartStatus() != null && smartAleart.getAleartStatus().equals(Meta.ALERT_TW_REJECTED) && isSAMDown(smartAleart.getErrorMsg()) && (!msgType.equals(Meta.ALERT_HIS))) {
                showErrorMsg(smartAleart.getErrorMsg());
                return;
            }

            if (msgType.equalsIgnoreCase(Meta.SALERT_READ_RECIEPT)) {
                if (smartAleart.getAleartStatus() != Meta.ALERT_STATUS_INVALIED_STRING) {
                    SmartAlertStore.getSharedInstance().setRecord(smartAleart);
                    SmartAlertSummeryWindow.getSharedInstance().frameAddeded();
                } else {
                    showErrorMsg(smartAleart.getErrorMsg());
                    //new ShowMessage(smartAleart.getErrorMsg(), "E");
                }
            } else if (msgType.equals(Meta.ALERT)) {
                String aleartStatus = smartAleart.getAleartStatus();
                if (aleartStatus != null && !aleartStatus.isEmpty() && aleartStatus.equals(Meta.ALERT_STATUS_TRIGGERED_STRING)) {
                    SmartAlertFrame record = SmartAlertStore.getSharedInstance().getRecord(smartAleart.getClientAlertID());
                    if (record != null) {

                        record.setTriggeredDate(smartAleart.getTriggeredDate());
                        record.setTriggereMsg(smartAleart.getTriggereMsg());
                        if (record.getNotifyMethod() == Meta.ALERT_MODE_TW || record.getNotifyMethod() == 3 || record.getNotifyMethod() == 5 || record.getNotifyMethod() == 7) {
                            showAlertTriggeredMsg(SmartAlertUtils.processTriggerString(smartAleart.getTriggereMsg(), record.getSKey()));
                        }
                        //new ShowMessage(SmartAlertUtils.processTriggerString(smartAleart.getTriggereMsg(), record.getSKey()), "I");
                        record.setMessageShown(Meta.ALERT_STATUS_TRIGGERED);
                        record.setAleartStatus(aleartStatus);
                        if (smartAleart.getParamString() != null && !smartAleart.getParamString().isEmpty()) {
                            // record.setParamString(smartAleart.getParamString());
                            record.createSubAlertsFromString();
                        }
                        sendReadReciept(record);
                    }

                }
            } else if (msgType.equals(Meta.ALERT_UPDATE)) {
                String aleartStatus = smartAleart.getAcStatus();
                if (aleartStatus.equals(Meta.ALERT_STATUS_UPDATE_TEJECTED_STRING)) {
                    String error = smartAleart.getErrorMsg();
                    if (error != null && !error.isEmpty()) {
                        // new ShowMessage(error, "E");
                        showErrorMsg(smartAleart.getErrorMsg());
                    }
                } else {
                    SmartAlertFrame record = SmartAlertStore.getSharedInstance().getRecord(smartAleart.getClientAlertID());
                    if (record != null) {
                        if (smartAleart.getParamString() != null && !smartAleart.getParamString().isEmpty()) {
                            record.setParamString(smartAleart.getParamString());
                            record.createSubAlertsFromString();
                        }
                        if (smartAleart.getNotifyMethod() != -1) {
                            record.setNotifyMethod(smartAleart.getNotifyMethod());
                        }
                        if (smartAleart.getExpirationDate() != 0) {
                            record.setExpirationDate(smartAleart.getExpirationDate());
                        }
                    }
                }

            } else if (msgType.equals(Meta.ALERT_DELETE)) {
                String aleartStatus = smartAleart.getAcStatus();
                if (aleartStatus.equals(Meta.ALERT_STATUS_UPDATE_TEJECTED_STRING)) {
                    String error = smartAleart.getErrorMsg();
                    if (error != null && !error.isEmpty()) {
                        // new ShowMessage(error, "E");
                        showErrorMsg(smartAleart.getErrorMsg());
                    }
                } else {
                    SmartAlertStore.getSharedInstance().deleteRecord(smartAleart.getClientAlertID());
                    new ShowMessage(Language.getString("SMART_ALERT_DELETED"), "I");
                }

            } else if (msgType.equals(Meta.ALERT_HIS)) {
                String aleartStatus = smartAleart.getAleartStatus();
                int shown = smartAleart.getMessageShown();
                if (aleartStatus.equals(Meta.ALERT_STATUS_TRIGGERED_STRING) && shown == Meta.ALERT_STATUS_NOT_TRIGGERED) {
                    if (smartAleart.getNotifyMethod() == Meta.ALERT_MODE_TW || smartAleart.getNotifyMethod() == 3 || smartAleart.getNotifyMethod() == 5 || smartAleart.getNotifyMethod() == 7) {
                        showAlertTriggeredMsg(SmartAlertUtils.processTriggerString(smartAleart.getTriggereMsg(), smartAleart.getSKey()));
                    }
                    //new ShowMessage(SmartAlertUtils.processTriggerString(smartAleart.getTriggereMsg(), smartAleart.getSKey()), "I");
                    smartAleart.setMessageShown(Meta.ALERT_STATUS_TRIGGERED);
                    sendReadReciept(smartAleart);
                }
                SmartAlertStore.getSharedInstance().setRecord(smartAleart);
                SmartAlertSummeryWindow.getSharedInstance().frameAddeded();
            }

            SmartAlertSummeryWindow.getSharedInstance().frameAddeded();
        }

    }

    private void sendReadReciept(SmartAlertFrame frame) {
        String req = frame.getReadRecieptMessage();
        SendQFactory.addData(Constants.PATH_PRIMARY, req);
        System.out.println("sending RR= " + req);
    }

    public void requestHistory(String userID) {
        String usrID;
        if (userID != null && (!userID.trim().isEmpty())) {
            String[] parms = userID.split(Meta.DS);
            if (parms != null && parms.length == 2) {
                userID = parms[1];
            } else {
                userID = null;
            }
        }
        SmartAlertStore.getSharedInstance().clear();
        SmartAlertFrame msg = new SmartAlertFrame();
        msg.setType("1");

        if (userID == null || userID.trim().isEmpty()) {
            msg.setClientAlertID(System.currentTimeMillis() + "" + Settings.getUserID()); // time+user
            msg.setLogingID(Settings.getUserID());
            usrID = Settings.getUserID();
        } else {
            msg.setClientAlertID(System.currentTimeMillis() + "" + userID); // time+user
            msg.setLogingID(userID);
            usrID = userID;
        }
        SendQFactory.addData(Constants.PATH_PRIMARY, msg.getAlertHistoryMessage(usrID));
        //writeLog(msg.getAlertHistoryMessage(Settings.getUserID()));
        msg = null;
    }

    public void deleteRecord(String alertID) {
        if (alertID != null && !alertID.isEmpty()) {
            SmartAlertFrame frame = SmartAlertStore.getSharedInstance().getRecord(alertID);
            if (frame != null) {
                SendQFactory.addData(Constants.PATH_PRIMARY, frame.getDeleteMessage());
            }


        }

    }

    private void showAlertTriggeredMsg(String msg) {

        if (msg != null && !msg.isEmpty()) {
            ImageIcon icon = new ImageIcon("images/Theme" + Theme.getID() + "/alert_triggered.gif");
            final JButton[] options = new JButton[1];
            options[0] = new TWButton(Language.getString("OK"));
            final JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, icon, options, options[0]);
            //final JOptionPane optionPane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE,JOptionPane.DEFAULT_OPTION, icon);
            final JDialog dialog = optionPane.createDialog(Client.getInstance().getFrame(), Language.getString("SMART_ALERT_SUMMARY_TRIGGERED"));
            dialog.setContentPane(optionPane);
            dialog.setModal(false);
            dialog.setResizable(false);
            dialog.pack();
            dialog.setLocationRelativeTo(Client.getInstance().getFrame());
            options[0].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                }
            });
            dialog.setVisible(true);

        }

    }

    private void showErrorMsg(String msg) {
        String errMsg = "";

        int err = -1;
        try {
            err = Integer.parseInt(msg);
        } catch (NumberFormatException e) {
            err = -1;
        }

        switch (err) {
            case -1008:
                errMsg = Language.getString("SMART_ALERT_UPDATE_ERROR");
                break;
            case -1001:
            case -1002:
            case -1003:
            case -1004:
            case -1005:
            case -1007:
                errMsg = Language.getString("SMART_ALERT_PROCESS_ERROR");
                break;
            case -1010:
                errMsg = Language.getString("SMART_ALERT_MANAGER_DOWN");
            default:
                errMsg = Language.getString("SMART_ALERT_PROCESS_ERROR");
                ;
        }
        new ShowMessage(errMsg, "E");
    }

    private boolean isSAMDown(String msg) {
        int err = -1;
        try {
            err = Integer.parseInt(msg);
        } catch (NumberFormatException e) {
            err = -1;
        }

        if (err == -1010) {
            return true;
        } else {
            return false;
        }

    }
}
