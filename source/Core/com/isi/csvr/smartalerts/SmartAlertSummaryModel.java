package com.isi.csvr.smartalerts;

import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jan 26, 2009
 * Time: 11:58:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertSummaryModel extends CommonTable implements TableModel, CommonTableInterface {
    private static final String TRIGGERED = Language.getString("ALERT_STATUS_TRIGGERED");
    private ArrayList<SmartAlertObject> dataStore;
    private SimpleDateFormat insDateFormat = new SimpleDateFormat("yyyy/MM/dd - hh:mm:ss");
    private SimpleDateFormat trigDateFormat = new SimpleDateFormat("yyyy/MM/dd - hh:mm:ss");

    public SmartAlertSummaryModel(ArrayList<SmartAlertObject> dataArray2) {
        this.dataStore = dataArray2;
    }


    public void setDataStore(ArrayList<SmartAlertObject> dataStore) {
        this.dataStore = dataStore;
    }

    public int getRowCount() {
        return dataStore.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            SmartAlertObject smartOb = (SmartAlertObject) dataStore.get(rowIndex);
            if (smartOb == null) {
                return "fff"; //null;
            }
            switch (columnIndex) {
                case -4:
                    return smartOb.isFirstElement();
                case -3:
                    return "" + smartOb.getRownum();
                // for history
                case -2:
                    return "" + smartOb.getAlertrownum();
                case -1:
                    return smartOb.getClientId();
                case 0:
                    return "" + smartOb.getAlertstatus();
                case 1:
                    return "" + smartOb.getStatus();
                case 2:
                    return smartOb.getSymbol();
                case 3:
                    return smartOb.getParameter();
                case 4:
                    return smartOb.getCriteria();
                case 5:
                    return smartOb.getValue();
                case 6:
                    return smartOb.getMessageStatus();
                case 7:
                    return smartOb.getAlertNotification();
                case 8:
                    return smartOb.getAlertExpiaration();
                case 9:
                    if (smartOb.getAlertInsertDate() > 0) {
                        return insDateFormat.format(new SimpleDateFormat("yyyyMMddhhmmss").parse("" + smartOb.getAlertInsertDate()));
                    } else {
                        return "";
                    }
                case 10:
                    if ((smartOb.getAlertTriggeredDate() > 0) && (smartOb.getMessageStatus().equalsIgnoreCase(TRIGGERED))) {
                        return trigDateFormat.format(new SimpleDateFormat("yyyyMMddhhmmss").parse("" + smartOb.getAlertTriggeredDate()));
                    } else {
                        return Language.getString("NA");
                    }
                default:
                    return "0";
            }
        } catch (Exception e) {
            return "";
        }

        /*  try {
            SmartAlertFrame record = dataStore.get(rowIndex );
            switch (columnIndex){
                case 0:
                    return record.getClientAlertID();
                case 1:
                    return record.getSymbol();
                case 2:
                    return record.getParamString();
                case 3:
                    return record.getExpirationDate()+"";
                default:
                    return "";
            }
        } catch (Exception e) {
            return "";
        }*/

    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int column) {
        return super.getViewSettings().getColumnHeadings()[column];
    }

}
