package com.isi.csvr.smartalerts;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWComboBox;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 19, 2009
 * Time: 11:05:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertObject implements Cloneable {

    public static final String PENDING = Language.getString("ALERT_STATUS_PENDING");//	"Pending";
    public static final String CLICK_MSG = Language.getString("ALERT_CLICK_TO_ADD_SYMBOL");//"Click to add a symbol";
    private static final String TRIGGERED = Language.getString("ALERT_STATUS_TRIGGERED");//"Triggered";
    private static final String EXPIRED = Language.getString("ALERT_STATUS_EXPIRED");//"Expired";
    private String symbol = "";
    private String symbolDescription = "";
    private String sKey = null;
    private String parameter = null;
    private String criteria = null;
    private String value = null;
    private String messageStatus = "";
    private int noOfChildren = 0;
    private int noOfBaseSymbol = 0;
    private TWComboBox cBox;
    private String clientId;
    private int status = 0;
    private int rownum = 0;
    private int alertstatus = 0;
    private int alertrownum = 0;
    private boolean firstElement = false;
    private String parameterID = null;
    private String criteriaID = null;
    private String alertCondtion = "";
    private String alertExpiaration = "";
    private String alertNotification = "";

    private long alertInsertDate = 0;
    private long alertTriggeredDate = 0;

    public TWComboBox getCBox() {
        return cBox;
    }

    public void setCBox(TWComboBox cBox) {
        this.cBox = cBox;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbolDescription() {
        return symbolDescription;
    }

    public void setSymbolDescription(String symbolDescription) {
        this.symbolDescription = symbolDescription;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        if (messageStatus.equalsIgnoreCase("P")) {
            this.messageStatus = PENDING;
        } else if (messageStatus.equalsIgnoreCase("T")) {
            this.messageStatus = TRIGGERED;
        } else if (messageStatus.equalsIgnoreCase("E")) {
            this.messageStatus = EXPIRED;
        } else {
            this.messageStatus = PENDING;
        }

    }

    public String getParameterID() {
        return parameterID;
    }

    public void setParameterID(String parameterID) {
        this.parameterID = parameterID;
    }

    public String getCriteriaID() {
        return criteriaID;
    }

    public void setCriteriaID(String criteriaID) {
        this.criteriaID = criteriaID;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public int getNoOfChildren() {
        return noOfChildren;
    }

    public void setNoOfChildren(int noOfChildren) {
        this.noOfChildren = noOfChildren;
    }

    public int getNoOfBaseSymbol() {
        return noOfBaseSymbol;
    }

    public void setNoOfBaseSymbol(int noOfBaseSymbol) {
        this.noOfBaseSymbol = noOfBaseSymbol;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRownum() {
        return rownum;
    }

    public void setRownum(int rownum) {
        this.rownum = rownum;
    }

    public int getAlertstatus() {
        return alertstatus;
    }

    public void setAlertstatus(int alertstatus) {
        this.alertstatus = alertstatus;
    }

    public int getAlertrownum() {
        return alertrownum;
    }

    public void setAlertrownum(int alertrownum) {
        this.alertrownum = alertrownum;
    }

    public boolean isFirstElement() {
        return firstElement;
    }

    public void setFirstElement(boolean firstElement) {
        this.firstElement = firstElement;
    }

    public String getCriteriaString() {
        if (value != null && parameter != null && criteria != null && symbol != null) {
            return sKey + "|" + parameterID + "|" + criteriaID + "|" + value;

        } else {
            return null;
        }
    }


    public String getAlertCondtion() {
        return alertCondtion;
    }

    public void setAlertCondtion(String alertCondtion) {
        this.alertCondtion = alertCondtion;
    }

    public String getAlertExpiaration() {
        return alertExpiaration;
    }

    public void setAlertExpiaration(String alertExpiaration) {
        this.alertExpiaration = alertExpiaration;
    }

    public String getAlertNotification() {
        return alertNotification;
    }

    public void setAlertNotification(String alertNotification) {
        this.alertNotification = alertNotification;
    }

    public long getAlertInsertDate() {
        return alertInsertDate;
    }

    public void setAlertInsertDate(long alertInsertDate) {
        this.alertInsertDate = alertInsertDate;
    }

    public long getAlertTriggeredDate() {
        return alertTriggeredDate;
    }

    public void setAlertTriggeredDate(long alertTriggeredDate) {
        this.alertTriggeredDate = alertTriggeredDate;
    }
}
