package com.isi.csvr.smartalerts;

import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 30, 2009
 * Time: 9:39:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertSummeryRenderer extends TWBasicTableRenderer {

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static ImageIcon collapseImage;
    private static ImageIcon expandImage;
    private static ImageIcon nochildImage;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oNumericFormat;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private int intValue;
    private double doubleValue;


    public SmartAlertSummeryRenderer() {
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
        try {
            collapseImage = new ImageIcon("images/Theme" + Theme.getID() + "/collapse.gif");
        } catch (Exception e) {
            collapseImage = null;
        }
        try {
            expandImage = new ImageIcon("images/Theme" + Theme.getID() + "/expand.gif");
        } catch (Exception e) {
            expandImage = null;
        }
        try {
            nochildImage = new ImageIcon("images/Theme" + Theme.getID() + "/free.gif");
        } catch (Exception e) {
            expandImage = null;
        }

    }

    public void propertyChanged(int property) {

    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        reload();
        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
        } else {
            g_iStringAlign = JLabel.RIGHT;
        }
//        g_iStringAlign = JLabel.LEADING;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        Color foreground, background;
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        Object rownum = table.getModel().getValueAt(row, -2);
        int rowNumber = 0;
        try {
            rowNumber = toIntValue(rownum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        boolean showSymol = false;
        try {
            showSymol = (Boolean) table.getModel().getValueAt(row, -4);
        } catch (Exception e) {
            showSymol = false;
        }

        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (rowNumber % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (rowNumber % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);
        lblRenderer.setToolTipText(null);
        try {
            try {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            } catch (Exception e) {
                oPriceFormat = (((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalFormat());
            }
        } catch (Exception e) {
            // do nothing
        }

        /*try {
            collapseImage = new ImageIcon("images/Theme" + Theme.getID() + "/collapse.gif");
        } catch (Exception e) {
            collapseImage = null;
        }
        try {
            expandImage = new ImageIcon("images/Theme" + Theme.getID() + "/expand.gif");
        } catch (Exception e) {
            expandImage = null;
        }
        try {
            nochildImage = new ImageIcon("images/Theme" + Theme.getID() + "/free.gif");
        } catch (Exception e) {
            expandImage = null;
        }*/

        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText(" " + value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText(" " + value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 2: // DESCRIPTION
                    if (showSymol) {
                        lblRenderer.setText(" " + value);
                    } else {
                        lblRenderer.setText(" ");
                    }

                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3:
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue) + " ");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // Expander Image
                    switch ((toIntValue(value))) {
                        case 2:
                            lblRenderer.setIcon(expandImage);
                            break;
                        case 1:
                            lblRenderer.setIcon(collapseImage);
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            break;
                    }
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(SwingConstants.CENTER);
                    break;


                default:
                    lblRenderer.setText("");

            }
        } catch (Exception e) {
            e.printStackTrace();
            lblRenderer.setText("");
        }
        return lblRenderer;
    }

    private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }


    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) {
        try {
            return Double.parseDouble((String) oValue);
        } catch (Exception e) {
            return 0;
        }
    }
}
