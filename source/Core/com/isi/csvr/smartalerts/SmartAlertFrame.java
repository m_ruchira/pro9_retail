package com.isi.csvr.smartalerts;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jan 22, 2009
 * Time: 4:00:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertFrame {

    private String clientAlertID = null;     // B
    private String type = null;          // A
    private String serverAlertID = null;   // this is auto generaeted id by AlertManager    //  C
    private String messageType;  // type like trade index       // D
    private String symbol = null;     // E
    private int instrumentType = -1;      // F
    private String marketCode = "";     //G
    private String exchange = null;       //H
    private String logingID;              //I
    private String path = null;
    //    private int field = -1;
    //    private float Value = -1;        //
    //    private int criteria = -1;
    private String paramString;              // J
    private int notifyMethod = -1;            // K
    private long triggeredDate = -1;          // L
    private int expirationDate = 0;           // M
    private String aleartStatus;                // N
    private String acStatus;                     // O
    private int messageShown = -1;             // P
    private long insertDate = -1;              // Q
    private String triggereMsg = "";           // 
    private String errorMsg = "";
    private String audioFileName = null;
    private String sKey;
    private int notification = -1;

    private int noOfChildren = 0;
    private String alertFrame;
    private String operator;
    private ArrayList<SmartAlertObject> subAlertObjects;


    public SmartAlertFrame(String alertFrame) {
        this.alertFrame = alertFrame;
        createAleart();
        createSubAlertsFromString();
    }

    public SmartAlertFrame() {
    }

    private void createAleart() {
        if (alertFrame != null && !alertFrame.isEmpty()) {

            String[] baits = alertFrame.split(Meta.FD);
            if (baits != null && baits.length > 0) {
                char tag;
                for (int i = 0; i < baits.length; i++) {
                    tag = baits[i].charAt(0);
                    switch (tag) {
                        case 'A':
                            type = baits[i].substring(1);
                            break;
                        case 'B':
                            clientAlertID = baits[i].substring(1);
                            break;
                        case 'C':
                            serverAlertID = baits[i].substring(1);
                            break;
                        case 'D':
                            messageType = baits[i].substring(1);
                            break;
                        case 'E':
                            symbol = baits[i].substring(1);
                            break;
                        case 'F':
                            try {
                                instrumentType = Integer.parseInt(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                instrumentType = -1;
                            }
                            break;
                        case 'G':
                            marketCode = baits[i].substring(1);
                            break;
                        case 'H':
                            exchange = baits[i].substring(1);
                            break;
                        case 'I':
                            logingID = baits[i].substring(1);
                            break;
                        case 'J':
                            //paramString = baits[i].substring(1);
                            setParamString(baits[i].substring(1));
                            break;
                        case 'K':
                            try {
                                notifyMethod = Integer.parseInt(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                notifyMethod = -1;
                            }
                            break;
                        case 'L':
                            try {
                                triggeredDate = Long.parseLong(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                triggeredDate = -1;
                            }
                            break;
                        case 'M':
                            try {
                                expirationDate = Integer.parseInt(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                expirationDate = -1;
                            }
                        case 'N':
                            aleartStatus = baits[i].substring(1);
                            break;
                        case 'O':
                            acStatus = baits[i].substring(1);
                            break;
                        case 'P':
                            try {
                                messageShown = Integer.parseInt(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                messageShown = -1;
                            }
                            break;
                        case 'Q':
                            try {
                                insertDate = Long.parseLong(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                insertDate = -1;
                            }
                            break;
                        case 'R':
                            triggereMsg = baits[i].substring(1);
                            break;
                        case 'S':
                            errorMsg = baits[i].substring(1);
                            break;
                        case 'T':
                            audioFileName = baits[i].substring(1);
                            break;
                        case 'U':
                            sKey = baits[i].substring(1);
                            break;
                        case 'V':
                            try {
                                notification = Integer.parseInt(baits[i].substring(1));
                            } catch (NumberFormatException e) {
                                notification = -1;
                            }
                            break;

                    }
                }
            }

        }
    }

    public void createSubAlertsFromString() {
        if (paramString != null && !paramString.isEmpty()) {
            subAlertObjects = SmartAlertUtils.parseParamerString(paramString, clientAlertID);

        }
        if (subAlertObjects != null && subAlertObjects.size() > 1) {
            noOfChildren = 1;
        }
        if (subAlertObjects != null && subAlertObjects.size() > 0) {
            SmartAlertObject smartOb = subAlertObjects.get(0);
            smartOb.setMessageStatus(getAleartStatus());
            smartOb.setAlertNotification(SmartAlertUtils.getNotificationString(getNotification()));
            smartOb.setAlertExpiaration(SmartAlertUtils.getExpirationString(getExpirationDate()));
            smartOb.setFirstElement(true);
            if (getInsertDate() > 0) {
                smartOb.setAlertInsertDate(getZoneAdjustedTime(getInsertDate()));
            }
            if (getTriggeredDate() > 0) {
                smartOb.setAlertTriggeredDate(getZoneAdjustedTime(getTriggeredDate()));
            }
        }
    }


    public String getCreateNewAleartMessage() {
        String Request = Meta.SMART_ALERT_MESSAGE + Meta.DS + createNewAleartMessage() + Meta.EOL;
        return Request;
    }

    private String createNewAleartMessage() {
        String newMsg = "" + "A" + type + Meta.FD +
                "B" + clientAlertID + Meta.FD +
                "DA" + Meta.FD +
                // "E"+symbol+Meta.FD+
                //  "F"+instrumentType+Meta.FD+
                //  "G="+marketCode+Meta.FD+
                //  "H"+exchange+Meta.FD+
                "U" + sKey + Meta.FD +
                "I" + logingID + Meta.FD +
                "J" + paramString + Meta.FD +
                "K" + notifyMethod + Meta.FD +
                "M" + expirationDate + Meta.FD +
                "NP" + Meta.FD +
                "P0" + Meta.FD +
                "Q" + getFormattedInsretDate() + Meta.FD +
                "V" + notification;
        if (audioFileName != null && !audioFileName.isEmpty()) {
            newMsg = newMsg + Meta.FD + "T=" + audioFileName;
        }
        return newMsg;

    }

    public String getUpdateExsistingAleartMessage() {
        String Request = Meta.SMART_ALERT_MESSAGE + Meta.DS + createUpdateExsitingMessage() + Meta.EOL;
        return Request;
    }

    private String createUpdateExsitingMessage() {
        String newMsg = "" + "A" + type + Meta.FD +
                "B" + clientAlertID + Meta.FD +
                "C" + serverAlertID + Meta.FD +
                "DU" + Meta.FD +
                // "E"+symbol+Meta.FD+
                // "F"+instrumentType+Meta.FD+
                // "G"+marketCode+Meta.FD+
                //  "H"+exchange+Meta.FD+
                "U" + sKey + Meta.FD +
                "I" + logingID + Meta.FD +
                "J" + paramString + Meta.FD +
                "M" + expirationDate + Meta.FD +
                "K" + notifyMethod + Meta.FD +
                "M" + expirationDate + Meta.FD +
                "NP" + Meta.FD +
                "P0" + Meta.FD +
                "Q" + insertDate + Meta.FD +
                "V" + notification;
        if (audioFileName != null && !audioFileName.isEmpty()) {
            newMsg = newMsg + Meta.FD + "T=" + audioFileName;
        }
        return newMsg;
    }

    public String getDeleteMessage() {
        String Request = Meta.SMART_ALERT_MESSAGE + Meta.DS + createDeleteMessage() + Meta.EOL;
        return Request;
    }

    private String createDeleteMessage() {
        String newMsg = "" + "A" + type + Meta.FD +
                "B" + clientAlertID + Meta.FD +
                "C" + serverAlertID + Meta.FD +
                "DD" + Meta.FD +
                // "E"+symbol+Meta.FD+
                // "F"+instrumentType+Meta.FD+
                // "G="+marketCode+Meta.FD+
                //   "H"+exchange+Meta.FD+
                "U" + sKey + Meta.FD +
                "I" + logingID;
        return newMsg;
    }

    public String getReadRecieptMessage() {
        String Request = Meta.SMART_ALERT_MESSAGE + Meta.DS + createReadReciptMesssage() + Meta.EOL;
        return Request;
    }

    private String createReadReciptMesssage() {
        String newMsg = "" + "A" + type + Meta.FD +
                "B" + clientAlertID + Meta.FD +
                "C" + serverAlertID + Meta.FD +
                "DRR" + Meta.FD +
                // "E"+symbol+Meta.FD+
                // "F"+instrumentType+Meta.FD+
                // "G="+marketCode+Meta.FD+
                // "H"+exchange+Meta.FD+
                // "U" + sKey + Meta.FD +
                // "I" + logingID + Meta.FD +
                "P1";

        return newMsg;
    }

    public String getAlertHistoryMessage(String ID) {
        String Request = Meta.SMART_ALERT_MESSAGE + Meta.DS + createAlertHistoryMesssage(ID) + Meta.EOL;
        return Request;
    }

    private String createAlertHistoryMesssage(String ID) {
        String newMsg = "" + "A" + type + Meta.FD +
                //"B" + clientAlertID + Meta.FD +
                "D" + Meta.ALERT_HIS + Meta.FD +
                // "E"+symbol+Meta.FD+
                // "F"+instrumentType+Meta.FD+
                // "G="+marketCode+Meta.FD+
                // "H"+exchange+Meta.FD+
                // "U" + sKey + Meta.FD +
                "I" + ID;


        return newMsg;
    }

    public String getTriggereMsg() {
        return triggereMsg;
    }

    public void setTriggereMsg(String triggereMsg) {
        this.triggereMsg = triggereMsg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getServerAlertID() {
        return serverAlertID;
    }

    public void setServerAlertID(String serverAlertID) {
        this.serverAlertID = serverAlertID;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getMarketCode() {
        return marketCode;
    }

    public void setMarketCode(String marketCode) {
        this.marketCode = marketCode;
    }

    public String getClientAlertID() {
        return clientAlertID;
    }

    public void setClientAlertID(String clientAlertID) {
        this.clientAlertID = clientAlertID;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getLogingID() {
        return logingID;
    }

    public void setLogingID(String logingID) {
        this.logingID = logingID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getParamString() {
        return paramString;
    }

    public void setParamString(String paramString) {
        this.paramString = paramString;
        createOperator(paramString);
    }

    private void createOperator(String parmString) {
        if (parmString != null && !parmString.isEmpty()) {
            if (parmString.contains(Meta.ALERT_OPERATOR_AND)) {
                setOperator(Meta.ALERT_OPERATOR_AND);
            } else if (parmString.contains(Meta.ALERT_OPERATOR_OR)) {
                setOperator(Meta.ALERT_OPERATOR_OR);
            }
        }
    }

    public int getNotifyMethod() {
        return notifyMethod;
    }

    public void setNotifyMethod(int notifyMethod) {
        this.notifyMethod = notifyMethod;
    }

    public long getTriggeredDate() {
        return triggeredDate;
    }

    public void setTriggeredDate(long triggeredDate) {
        this.triggeredDate = triggeredDate;
    }

    public int getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(int expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getAleartStatus() {
        return aleartStatus;
    }

    public void setAleartStatus(String aleartStatus) {
        this.aleartStatus = aleartStatus;
    }

    public String getAcStatus() {
        return acStatus;
    }

    public void setAcStatus(String acStatus) {
        this.acStatus = acStatus;
    }

    public int getMessageShown() {
        return messageShown;
    }

    public void setMessageShown(int messageShown) {
        this.messageShown = messageShown;
    }

    public long getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(long insertDate) {
        this.insertDate = insertDate;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getAudioFileName() {
        return audioFileName;
    }

    public void setAudioFileName(String audioFileName) {
        this.audioFileName = audioFileName;
    }

    public String getAlertFrame() {
        return alertFrame;
    }

    public String getSKey() {
        return sKey;
    }

    public void setSKey(String sKey) {
        this.sKey = sKey;
    }

    public int getNotification() {
        return notification;
    }

    public void setNotification(int notification) {
        this.notification = notification;
    }

    private String getFormattedInsretDate() {
        TWDateFormat formater = new TWDateFormat("yyyyMMddHHmmss");
        return formater.format(insertDate);

    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public ArrayList<SmartAlertObject> getSubAlertObjects() {
        return subAlertObjects;
    }

    public int getNoOfChildren() {
        return noOfChildren;
    }

    public void setNoOfChildren(int noOfChildren) {
        this.noOfChildren = noOfChildren;
    }

    private long getZoneAdjustedTime(long date) {
        long adjustedTime = 0;
        adjustedTime = Settings.getLocalTimeFor(getDateTimeInMillis(date));  //getting local time in the client machine
        try {
            return Long.parseLong((new SimpleDateFormat("yyyyMMddhhmmss")).format(new Date(adjustedTime)));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private long getDateTimeInMillis(long formattedDate) {
        long millisDate = 0;
        Date date = null;
        try {
            date = (new SimpleDateFormat("yyyyMMddhhmmss")).parse(String.valueOf(formattedDate));
        } catch (ParseException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (date != null) {
            millisDate = date.getTime();
        }
        return millisDate;
    }
}
