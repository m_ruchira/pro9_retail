package com.isi.csvr;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 15, 2005
 * Time: 4:33:02 PM
 */
public class CustomerSupportMessageWindow extends InternalFrame
        implements ActionListener, ConnectionListener {

    private TWTextField txtName;
    private TWTextField txtContactNumber;
    private TWTextField txtEmail;
    private JComboBox cmbCategories;
    private TWTextArea txtMessage;
    private TWButton btnOk;
    private TWButton btnCancel;

    public CustomerSupportMessageWindow() {
        super(false);
        buildUI();
    }


    private void buildUI() {
        setLayout(new BorderLayout(5, 5));

        String[] widths = {"150", "200"};
        String[] heights = {"20", "20", "20", "20"};
        JPanel topPanel = new JPanel(new FlexGridLayout(widths, heights, 5, 5));

        JLabel lblName = new JLabel(Language.getString("YOUR_NAME"));
        txtName = new TWTextField();
        topPanel.add(lblName);
        topPanel.add(txtName);

        JLabel lblContactNumber = new JLabel(Language.getString("CONTACT_NUMBER"));
        txtContactNumber = new TWTextField();
        topPanel.add(lblContactNumber);
        topPanel.add(txtContactNumber);

        JLabel lblEmail = new JLabel(Language.getString("EMAIL"));
        txtEmail = new TWTextField();
        topPanel.add(lblEmail);
        topPanel.add(txtEmail);

        JLabel lblCatogory = new JLabel(Language.getString("CATEGORY"));
        String[] categories = {Language.getString("SELECT_CATEGORY"), Language.getString("CATEGORY_SUGGESTION"),
                Language.getString("CATEGORY_BUG_REPORT"), Language.getString("CATEGORY_COMPLAINT"),
                Language.getString("CATEGORY_CONNECTIVITY_ISSUE"), Language.getString("CATEGORY_OTHER")};
        cmbCategories = new JComboBox(categories);
        topPanel.add(lblCatogory);
        topPanel.add(cmbCategories);

        add(topPanel, BorderLayout.NORTH);

        JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
        JLabel lblMessage = new JLabel(Language.getString("COMMENTS_OR_SUGGESTIONS"));
        centerPanel.add(lblMessage, BorderLayout.NORTH);

        txtMessage = new TWTextArea();
        txtMessage.setWrapStyleWord(true);
        //txtMessage.setPreferredSize(new Dimension(350, 350));
        JScrollPane scrollPane = new JScrollPane(txtMessage);
        centerPanel.add(scrollPane, BorderLayout.CENTER);


        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 0));
        btnOk = new TWButton(Language.getString("OK"));
        bottomPanel.add(btnOk);
        btnOk.addActionListener(this);
        centerPanel.add(bottomPanel, BorderLayout.SOUTH);
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);
        bottomPanel.add(btnCancel);

        add(centerPanel, BorderLayout.CENTER);

        setResizable(true);
        setIconifiable(false);
        setClosable(true);
        setTitle(Language.getString("CONTACT_CUSTOMER_SUPPORT"));
        centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 5));
        setSize(370, 450);
        GUISettings.applyOrientation(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    private boolean validateInputs() {
        if ((txtContactNumber.getText().trim().equals("")) ||
                (txtEmail.getText().trim().equals("")) ||
                (txtName.getText().trim().equals("")) ||
                (txtMessage.getText().trim().equals("")) ||
                (txtContactNumber.getText().trim().equals(""))) {
            new ShowMessage(Language.getString("MSG_MUST_FILL_ALL_ENTRIES"), "E");
            return false;
        } else if (cmbCategories.getSelectedIndex() == 0) {
            new ShowMessage(Language.getString("MSG_MUSET_SELECT_CATEGORY"), "E");
            return false;
        }
        return true;
    }

    private void sendMessage() {
        //73 DS UserName FD Name(Real Name) FD ContactNumber FD Email FD Category FD MessageBody EOLN
        String url = Meta.CUSTOMER_SUPPORT_REQUEST + Meta.DS + UnicodeUtils.getUnicodeString(Settings.user) + Meta.FD +
                UnicodeUtils.getUnicodeString(txtName.getText()) + Meta.FD +
                UnicodeUtils.getUnicodeString(txtContactNumber.getText()) + Meta.FD +
                UnicodeUtils.getUnicodeString(txtEmail.getText()) + Meta.FD +
                UnicodeUtils.getUnicodeString((String) cmbCategories.getSelectedItem()) + Meta.FD +
                UnicodeUtils.getUnicodeString(txtMessage.getText());
        SendQFactory.addData(Constants.PATH_PRIMARY, url);
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(btnOk)) {
            if (validateInputs()) {
                sendMessage();
                setVisible(false);
            }
        } else if (e.getSource().equals(btnCancel)) {
            setVisible(false);
        }
    }

    /* Connection Listener */
    public void twConnected() {
        btnOk.setEnabled(false);
    }

    public void twDisconnected() {
        btnOk.setEnabled(true);
    }
}
