package com.mubasher.priceAPI;

import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Jan-2008 Time: 17:29:13 To change this template use File | Settings
 * | File Templates.
 */
public class RequestObject {
    String symbols = "";
    String exchange = "";
    String messageType = "";
    String requestType = "";
    String depthMode = "";
    Vector symbolList;
    String error = "";
    long updatedTime = 0l;
    int instrument = -1;

    public RequestObject(String sbl, String exg, int instrument, String mtype) {
        this.symbols = sbl;
        this.exchange = exg;
        this.messageType = mtype;
        this.instrument = instrument;
//        updatedTime= System.currentTimeMillis();
    }

    public RequestObject() {

    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updateTime) {
        this.updatedTime = updateTime;
    }

    public String getSymbol() {
        return symbols;
    }

    public String getExchange() {
        return exchange;
    }

    public int getInstrumentType() {
        return instrument;
    }

    public String getRequestType() {
        return requestType;
    }

    public String getMessageType() {
        return messageType;
    }

    public Vector getSymbolList() {
        symbolList = new Vector();
//        try {
//            String[] temp = symbols.split(",");
//            if (temp.length >= 1) {
//                for (int i = 0; i < temp.length; i++) {
//                    symbolList.add(temp[i]);
//                }
//
//            }
//        } catch (Exception e) {
//            return new Vector();
//        }
        return symbolList;
    }

    public String getDepthMode() {
        return depthMode;
    }

    public void setDepthMode(String depthMode) {
        this.depthMode = depthMode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
