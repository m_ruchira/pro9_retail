package com.mubasher.priceAPI;


import com.isi.csvr.Client;
import com.isi.csvr.RequestManager;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.nanoxml.XMLElement;

import java.io.InputStream;
import java.util.*;


/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Jan-2008 Time: 12:55:56 To change this template use File | Settings
 * | File Templates.
 */
public class Reader extends Thread {

    public static List<RequestObject> requestVector;
    private static Hashtable<String, RequestObject> requestRegister;
    boolean active = true;
    RequestObject request;
    private InputStream in;

    public Reader(InputStream in) {
        this.in = in;
        requestVector = Collections.synchronizedList(new LinkedList<RequestObject>());
        requestRegister = new Hashtable<String, RequestObject>();
        start();
    }

    public static synchronized Hashtable<String, RequestObject> getRequestRegister() {
        return requestRegister;
    }

    public static void setErrorCode(int errorCode) {
        StringBuilder buffer = new StringBuilder();
        RequestObject ro;
        switch (errorCode) {
            case Meta.ERROR_TYPE_PARSE_ERROR:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Parse Error").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_UNKNOWN_TYPE:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Unknown Request Type").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_PARAMETER_MISSING:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Parameter Missing").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_PRICE_DISCONNECTED:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Disconnected From Price Feed").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_PRICE_CONNECTED:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Connected Again to Price Feed").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_INVALID_SYMBOL_EXCHANGE:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Invalid Symbol or Exchange").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;
            case Meta.ERROR_TYPE_PARAMETER_INVALID:
                ro = new RequestObject();
                buffer.append("<ERROR>");
                buffer.append("<Body value=\"").append("Input Parameters are invalid").append("\"/>");
                buffer.append("</ERROR>\n");
                ro.setError(buffer.toString());
                PriceInfoServer.getInstance().write(buffer.toString());
//                requestVector.add(ro);
                break;

        }

    }

    public void run() {
        System.out.println("PriceInfoServer : Reader Thread Started");
        byte[] data = new byte[100];
        try {
            StringBuffer buff = new StringBuffer();
            while (active) {
                int len = in.read(data);
                if (len > 0) {
                    String str = new String(data, 0, len);
                    buff.append(str);
                    if (str.endsWith("\n")) {
                        analyseFrame(buff.toString());
                        buff = null;
                        buff = new StringBuffer();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            closeClient();
        }
    }

    private void closeClient() {
        PriceInfoServer.getInstance().closeClient();
    }

    private void errorMessage(int errorType, String... msg) {

    }

    /*private void analyseFrameOld(String data) {
        try {
            XMLElement root = new XMLElement();
            root.parseString(data);
            ArrayList<XMLElement> rootElements = root.getChildren();
            String requestType = "";
            String mode = "";
            String addRemove = "";
            String depthMode = "";
            String symbolList = "";
            String exchangeList = "";

            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem= rootElements.get(i);
                String name        = rootItem.getName();
                if (name.equals("DATA")) {
                    requestType   = rootItem.getAttribute("TYPE").trim();
                    mode          = rootItem.getAttribute("MODE").trim();
                    addRemove     = rootItem.getAttribute("SIDE").trim();
                    try {
                        symbolList    = rootItem.getAttribute("SYMBOLS").trim().toUpperCase();
                    } catch(Exception e) {
                        symbolList = "";
                    }
                    try {
                        symbolList    = rootItem.getAttribute("COMPANY").trim().toUpperCase();
                    } catch(Exception e) {
//                        symbolList = "";
                    }
                    exchangeList  = rootItem.getAttribute("EXCHANGES").trim().toUpperCase();
                    try {
                        depthMode = rootItem.getAttribute("DEPTHMODE");
                    } catch (NumberFormatException e) {
                        depthMode = "";
                    }
                    System.out.println("XML Request Received ............................");
                    System.out.println("Request Type        = " + requestType);
                    System.out.println("mode                = " + mode);
                    System.out.println("addRemove           = " + addRemove);
                    System.out.println("SymbolList          = " + symbolList);
                    System.out.println("ExchangeList        = " + exchangeList);
                    System.out.println("DepthMode           = " + depthMode);
                }

            }
            if (requestType.equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                processSymbolDataRequest(mode, addRemove, symbolList, exchangeList);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                processMarketDepthRequest(mode, addRemove, symbolList, exchangeList, depthMode);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                processTimeNSalesRequest(mode, addRemove, symbolList, exchangeList);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_OHLC)) {
                processOHLCRequest(mode, addRemove, symbolList, exchangeList);
            } else {
                setErrorCode(Meta.ERROR_TYPE_UNKNOWN_TYPE);
            }

        } catch (Exception e) {
            setErrorCode(Meta.ERROR_TYPE_PARSE_ERROR);
//            e.printStackTrace();
        }
    }*/

    private void analyseFrame(String data) {
        try {
            XMLElement root = new XMLElement();
            root.parseString(data);
            ArrayList<XMLElement> rootElements = root.getChildren();
            String requestType = "";
            String mode = "";
            String addRemove = "";
            String depthMode = "";
            String symbolList = "";
            String exchangeList = "";
            if (!root.getName().equalsIgnoreCase("REQUEST")) {
                setErrorCode(Meta.ERROR_TYPE_UNKNOWN_TYPE);
                return;
            }
            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem = rootElements.get(i);
                String name = rootItem.getName();
                String value = rootItem.getAttribute("VALUE");
                if (name.equals("TYPE")) {
                    requestType = value;
                } else if (name.equals("MODE")) {
                    mode = value;
                } else if (name.equals("SIDE")) {
                    addRemove = value;
                } else if (name.equals("DEPTHMODE")) {
                    depthMode = value;
                } else if (name.equals("EXCHANGE")) {
                    exchangeList = value;
                } else if (name.equals("SYMBOLS")) {
                    ArrayList<XMLElement> symbolElements = rootItem.getChildren();
                    for (int j = 0; j < symbolElements.size(); j++) {
                        XMLElement symbolItem = symbolElements.get(j);
                        String symbol = symbolItem.getName();
                        if (symbol.equals("SYMBOL")) {
                            String sValue = symbolItem.getAttribute("VALUE").toUpperCase();
                            String sID = symbolItem.getAttribute("ITYPE");
                            symbolList = symbolList + "," + SharedMethods.getExchangeKey(sValue, Integer.parseInt(sID));
                        }
                    }
                } else if (name.equals("SYMBOL")) {
                    String sValue = rootItem.getAttribute("VALUE").toUpperCase();
                    String sID = rootItem.getAttribute("ITYPE");
                    symbolList = SharedMethods.getExchangeKey(sValue, Integer.parseInt(sID));
                }
            }

            System.out.println("XML Request Received ............................");
            System.out.println("Request Type        = " + requestType);
            System.out.println("mode                = " + mode);
            System.out.println("addRemove           = " + addRemove);
            System.out.println("SymbolList          = " + symbolList);
            System.out.println("ExchangeList        = " + exchangeList);
            System.out.println("DepthMode           = " + depthMode);

            if (requestType.equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                processSymbolDataRequest(mode, addRemove, symbolList, exchangeList);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                processMarketDepthRequest(mode, addRemove, symbolList, exchangeList, depthMode);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                processTimeNSalesRequest(mode, addRemove, symbolList, exchangeList);
            } else if (requestType.equals(Meta.MESSAGE_TYPE_OHLC)) {
                processOHLCRequest(mode, addRemove, symbolList, exchangeList);
            } else {
                setErrorCode(Meta.ERROR_TYPE_UNKNOWN_TYPE);
            }

        } catch (Exception e) {
            setErrorCode(Meta.ERROR_TYPE_PARSE_ERROR);
//            e.printStackTrace();
        }
    }

    public synchronized void processSymbolDataRequest(String mode, String addOrRemove, String symbolList, String exchangeList) {

        try {
            if (addOrRemove.equals(Constants.ADD_REQUEST)) {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    addRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_SYMBOL_DATA, "");
                } else {
                    addRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_SYMBOL_DATA, "");
                }
            } else {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    removeRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_SYMBOL_DATA, "");
                } else {
                    removeRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_SYMBOL_DATA, "");
                }
            }
        } catch (Exception e) {
            //process symbol data failed......
        }
    }

    public synchronized void processMarketDepthRequest(String mode, String addOrRemove, String symbolList, String exchangeList, String depthMode) {

        try {
            if (addOrRemove.equals(Constants.ADD_REQUEST)) {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    addRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_MARKETDEPTH, depthMode);
                } else {
                    addRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_MARKETDEPTH, depthMode);
                }
            } else {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    removeRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_MARKETDEPTH, depthMode);
                } else {
                    removeRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_MARKETDEPTH, depthMode);
                }
            }
        } catch (Exception e) {
            //process MarketDepth data failed......
        }
    }

    public synchronized void processTimeNSalesRequest(String mode, String addOrRemove, String symbolList, String exchangeList) {
        try {
            if (addOrRemove.equals(Constants.ADD_REQUEST)) {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    addRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_TIMENSALES, "");
                } else {
                    addRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_TIMENSALES, "");

                }
            } else {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    removeRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_TIMENSALES, "");
                } else {
                    removeRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_TIMENSALES, "");
                }
            }
        } catch (Exception e) {
            //process TimenSales data failed......
        }
    }

    public synchronized void processOHLCRequest(String mode, String addOrRemove, String symbolList, String exchangeList) {
        try {
            if (addOrRemove.equals(Constants.ADD_REQUEST)) {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    addRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_OHLC, "");
                } else {
                    addRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_OHLC, "");
                }
            } else {
                if (mode.equals(Constants.SYMBOL_TYPE)) {
                    removeRequest(symbolList, exchangeList, Constants.SYMBOL_TYPE, Meta.MESSAGE_TYPE_OHLC, "");
                } else {
                    removeRequest(null, exchangeList, Constants.EXCHANGE_TYPE, Meta.MESSAGE_TYPE_OHLC, "");
                }
            }
        } catch (Exception e) {
            //process TimenSales data failed......
        }
    }

    public synchronized void addRequest(String symbolList, String exchange, String mode, String requestType, String depthMode) {
        try {
            if (mode.equals(Constants.SYMBOL_TYPE)) {
                String[] temp = symbolList.split(",");
                if (temp.length >= 1) {
                    for (int i = 0; i < temp.length; i++) {
                        if (temp[i].trim().equals("")) {
                            continue;
                        }
                        Stock oStock = DataStore.getSharedInstance().getStockObject(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                        if (oStock == null) {
                            setErrorCode(Meta.ERROR_TYPE_INVALID_SYMBOL_EXCHANGE);
                            return;
                        }
                        request = new RequestObject(SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), exchange, SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()), requestType);

                        if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                            depthMode = Meta.DEPTH_TYPE_NA;
                            String requestID = com.isi.csvr.shared.Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                            Client.getInstance().addTradeRequest(requestID, SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())));
                        } else if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                            request.setDepthMode(depthMode);
                            if (depthMode.equals(Meta.DEPTH_TYPE_PRICE)) {
                                String requestID2 = com.isi.csvr.shared.Meta.PRICE_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                Client.getInstance().addDepthRequest(requestID2, SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())), com.isi.csvr.shared.Meta.PRICE_DEPTH);
                            } else if (depthMode.equals(Meta.DEPTH_TYPE_ORDER)) {
                                String requestID1 = com.isi.csvr.shared.Meta.REGULAR_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                Client.getInstance().addDepthRequest(requestID1, SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())), com.isi.csvr.shared.Meta.REGULAR_DEPTH);
                            } else if (depthMode.equals(Meta.DEPTH_TYPE_SPECIAL)) {
                                String requestID3 = com.isi.csvr.shared.Meta.SPECIAL_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                Client.getInstance().addDepthRequest(requestID3, SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())), com.isi.csvr.shared.Meta.SPECIAL_DEPTH);
                            }
                        } else if (requestType.equals(Meta.MESSAGE_TYPE_OHLC)) {
                            depthMode = Meta.DEPTH_TYPE_NA;
                        } else if (requestType.equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                            depthMode = Meta.DEPTH_TYPE_NA;
                            if (exchange.trim().equals("") || (temp[i].trim().equals(""))) {
                                setErrorCode(Meta.ERROR_TYPE_PARAMETER_MISSING);
                                return;
                            }
                        }
                        getRequestRegister().put(SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())) + "~" + requestType + "~" + depthMode, request);
                    }
                }

            } else {
                if ((requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) || (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH))) {
                    setErrorCode(Meta.ERROR_TYPE_PARAMETER_INVALID);
                    return;
                }
                String[] tempExg = exchange.split(",");
                if (tempExg.length >= 1) {
                    for (int i = 0; i < tempExg.length; i++) {
                        Enumeration symbols = DataStore.getSharedInstance().getSymbols(tempExg[i].trim());
                        while (symbols.hasMoreElements()) {
                            String symbol = (String) symbols.nextElement();
                            if ((symbol != null) && (!symbol.equals(""))) {
                                request = new RequestObject(SharedMethods.getSymbolFromExchangeKey(symbol), tempExg[i].trim(), SharedMethods.getInstrumentFromExchangeKey(symbol), requestType);
                                /*if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                                    request.setDepthMode(depthMode);
                                    if (depthMode.equals(Meta.DEPTH_TYPE_PRICE)) {
                                        String requestID2 = com.isi.csvr.shared.Meta.PRICE_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        Client.getInstance().addDepthRequest(requestID2, tempExg[i].trim() + "~" + symbol, com.isi.csvr.shared.Meta.PRICE_DEPTH);
                                    } else if (depthMode.equals(Meta.DEPTH_TYPE_ORDER)) {
                                        String requestID1 = com.isi.csvr.shared.Meta.REGULAR_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        Client.getInstance().addDepthRequest(requestID1, tempExg[i].trim() + "~" + symbol, com.isi.csvr.shared.Meta.REGULAR_DEPTH);
                                    } else if (depthMode.equals(Meta.DEPTH_TYPE_SPECIAL)) {
                                        String requestID3 = com.isi.csvr.shared.Meta.SPECIAL_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        Client.getInstance().addDepthRequest(requestID3, tempExg[i].trim() + "~" + symbol, com.isi.csvr.shared.Meta.SPECIAL_DEPTH);
                                    }
                                }else */
                                if (requestType.equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                                    depthMode = Meta.DEPTH_TYPE_NA;
                                    if (tempExg[i].trim().trim().equals("") || (symbol.trim().equals(""))) {
                                        setErrorCode(Meta.ERROR_TYPE_PARAMETER_MISSING);
                                        return;
                                    } else {
                                        Stock oStock = DataStore.getSharedInstance().getStockObject(tempExg[i].trim(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));
                                        if (oStock == null) {
                                            setErrorCode(Meta.ERROR_TYPE_INVALID_SYMBOL_EXCHANGE);
                                            return;
                                        }
                                    }
                                    getRequestRegister().put(SharedMethods.getKey(tempExg[i].trim(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)) + "~" + requestType + "~" + depthMode, request);
                                }
                            }
                        }
                        /*if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                            String requestID = com.isi.csvr.shared.Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + tempExg[i].trim();
                            Client.getInstance().addMarketTradeRequest(requestID, tempExg[i].trim());
                        }*/
                    }
                }
            }
        } catch (Exception e) {
            //add request failed
            setErrorCode(Meta.ERROR_TYPE_PARAMETER_MISSING);
            e.printStackTrace();
        }
    }

    public synchronized void removeRequest(String list, String exchange, String mode, String requestType, String depthMode) {
        try {
            if (mode.equals(Constants.SYMBOL_TYPE)) {
                String[] temp = list.split(",");
                if (temp.length >= 1) {
                    for (int i = 0; i < temp.length; i++) {
                        if (temp[i].trim().equals("")) {
                            continue;
                        }
                        if (!requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                            depthMode = Meta.DEPTH_TYPE_NA;
                        }
                        getRequestRegister().remove(SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim())) + "~" + requestType + "~" + depthMode);

                        if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                            String requestId = com.isi.csvr.shared.Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                            RequestManager.getSharedInstance().removeRequest(requestId);
                        } else if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                            if (depthMode.equals(Meta.DEPTH_TYPE_PRICE)) {
                                String requestID2 = com.isi.csvr.shared.Meta.PRICE_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                RequestManager.getSharedInstance().removeRequest(requestID2);
                            } else if (depthMode.equals(Meta.DEPTH_TYPE_ORDER)) {
                                String requestID1 = com.isi.csvr.shared.Meta.REGULAR_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                RequestManager.getSharedInstance().removeRequest(requestID1);
                            } else if (depthMode.equals(Meta.DEPTH_TYPE_SPECIAL)) {
                                String requestID3 = com.isi.csvr.shared.Meta.SPECIAL_DEPTH + "|" + SharedMethods.getKey(exchange, SharedMethods.getSymbolFromExchangeKey(temp[i].trim()), SharedMethods.getInstrumentFromExchangeKey(temp[i].trim()));
                                RequestManager.getSharedInstance().removeRequest(requestID3);
                            }
                        } else if (requestType.equals(Meta.MESSAGE_TYPE_OHLC)) {

                        }
                    }
                }

            } else {
                if ((requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) || (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH))) {
                    setErrorCode(Meta.ERROR_TYPE_PARAMETER_INVALID);
                    return;
                }
                String[] tempExg = exchange.split(",");
                if (tempExg.length >= 1) {
                    for (int i = 0; i < tempExg.length; i++) {
                        Enumeration symbols = DataStore.getSharedInstance().getSymbols(tempExg[i].trim());
                        while (symbols.hasMoreElements()) {
                            String symbol = (String) symbols.nextElement();
                            if ((symbol != null) && (!symbol.equals(""))) {
                                depthMode = Meta.DEPTH_TYPE_NA;
                                getRequestRegister().remove(SharedMethods.getKey(tempExg[i].trim(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol)) + "~" + requestType + "~" + depthMode);

                                /*if (requestType.equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                                    if (depthMode.equals(Meta.DEPTH_TYPE_PRICE)) {
                                        String requestID2 = com.isi.csvr.shared.Meta.PRICE_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        RequestManager.getSharedInstance().removeRequest(requestID2);
                                    } else if (depthMode.equals(Meta.DEPTH_TYPE_ORDER)) {
                                        String requestID1 = com.isi.csvr.shared.Meta.REGULAR_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        RequestManager.getSharedInstance().removeRequest(requestID1);
                                    } else if (depthMode.equals(Meta.DEPTH_TYPE_SPECIAL)) {
                                        String requestID3 = com.isi.csvr.shared.Meta.SPECIAL_DEPTH + "|" + tempExg[i].trim() + "~" + symbol;
                                        RequestManager.getSharedInstance().removeRequest(requestID3);
                                    }
                                }*/
                            }
                        }
                        /*if (requestType.equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                            RequestManager.getSharedInstance().removeRequest(com.isi.csvr.shared.Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + tempExg[i].trim());

                        }*/
                    }
                }

            }

        } catch (Exception e) {
            setErrorCode(Meta.ERROR_TYPE_PARAMETER_MISSING);
            //remove request failed
        }
    }

}
