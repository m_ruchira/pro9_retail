package com.mubasher.priceAPI;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Jan-2008 Time: 14:38:21 To change this template use File | Settings
 * | File Templates.
 */
public class Meta {

    public static final String MESSAGE_TYPE_SYMBOL_DATA = "STOCK";
    public static final String MESSAGE_TYPE_MARKETDEPTH = "DEPTH";
    public static final String MESSAGE_TYPE_TIMENSALES = "TIMENSALES";
    public static final String MESSAGE_TYPE_OHLC = "OHLC";

    public static final String DEPTH_TYPE_SPECIAL = "SPECIAL";
    public static final String DEPTH_TYPE_ORDER = "ORDER";
    public static final String DEPTH_TYPE_PRICE = "PRICE";
    public static final String DEPTH_TYPE_NA = "NA";

    //error codes
    public static final int ERROR_TYPE_PARSE_ERROR = 1;
    public static final int ERROR_TYPE_UNKNOWN_TYPE = 2;
    public static final int ERROR_TYPE_PARAMETER_MISSING = 3;
    public static final int ERROR_TYPE_PRICE_DISCONNECTED = 4;
    public static final int ERROR_TYPE_PRICE_CONNECTED = 5;
    public static final int ERROR_TYPE_INVALID_SYMBOL_EXCHANGE = 6;
    public static final int ERROR_TYPE_PARAMETER_INVALID = 7;

}
