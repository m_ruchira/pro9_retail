package com.mubasher.priceAPI;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Jan-2008 Time: 16:49:07 To change this template use File | Settings
 * | File Templates.
 */
public class Constants {
    public static final String EXCHANGE_TYPE = "EXCHANGE";
    public static final String SYMBOL_TYPE = "SYMBOL";
    public static final String ADD_REQUEST = "ADD";
    public static final String REMOVE_REQUEST = "REMOVE";
}
