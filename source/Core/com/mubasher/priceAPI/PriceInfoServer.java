package com.mubasher.priceAPI;


import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.trade.CompanyTradeStore;
import com.isi.csvr.trade.Trade;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Jan-2008 Time: 12:03:05 To change this template use File | Settings
 * | File Templates.
 */
public class PriceInfoServer implements Runnable, ConnectionListener {


    private static final String LOCK = "PriceInfoServer";
    private static boolean serverActive;
    private static boolean clientActive;
    private static PriceInfoServer self;
    private ServerSocket server;
    private Socket client;
    private boolean isPriceConnected = true;
    private OutputStream out;

    public PriceInfoServer() {
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }


    public static PriceInfoServer getInstance() {
        return self;
    }

    public static void start() {
        try {
            if (self == null) {
                self = new PriceInfoServer();
            }
            serverActive = true;
            clientActive = true;
            Thread t = new Thread(self);
            t.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        serverActive = false;
        clientActive = false;
//        self.buffer.clear();
        self.closeServer();
        self.closeClient();
    }

    private void closeServer() {
        try {
            self.server.close();
            self.server = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void closeClient() {
        System.out.println(" Price API client closed");
        try {
            self.client.close();
            self.client = null;
            clientActive = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        while (serverActive) {
            try {
                server = new ServerSocket(9192, 1);
                while (serverActive) {
                    try {
                        client = server.accept();
                        out = client.getOutputStream();
                        new Reader(client.getInputStream());
//                        new RequestHandler();
                        //client.setSoTimeout(30000);
                        clientActive = true;
                        while (clientActive && serverActive) {
                            Enumeration<String> keys = Reader.getRequestRegister().keys();
                            String key = null;
                            RequestObject ro;
                            while (keys.hasMoreElements()) {
                                try {
                                    key = keys.nextElement();
                                    ro = Reader.getRequestRegister().get(key);
                                    responseWriter(ro);
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                            sleep(1000);
                        }
//                        reader = null;
//                        handler = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        closeClient();
                        sleep(5000);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                closeServer();
                closeClient();
                sleep(5000);
            }
        }
    }

    /*public void responseWriter(OutputStream out) {
        String xmlFile = "";
        try {
            RequestObject ro = Reader.requestVector.remove(0);
            if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                Stock oStock = DataStore.getSharedInstance().getStockObject(ro.getExchange(), ro.getSymbol());
//                if(oStock != null){
                if (oStock.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
//                if (oStock.getLastTradeTime() > ro.getUpdatedTime()) {
                    xmlFile = oStock.getXML();
                    System.out.println("===========================================================");
                    System.out.println("Symbol Data sent......");
                    System.out.println("Symbol " + oStock.getSymbol());
                    ro.setUpdatedTime(oStock.getSnapShotUpdatedTime());
//                    ro.setUpdatedTime(oStock.getLastTradeTime());
//                    ro.setUpdatedTime(System.currentTimeMillis());
                }
            } else if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                xmlFile= DepthStore.getInstance().getXMLFor(ro.getExchange()+"~"+ro.getSymbol(),ro.getDepthMode());
                System.out.println("===========================================================");
                System.out.println("MarketDepth Data sent......");
                System.out.println("Symbol " + ro.getSymbol());
            } else if (ro.getMessageType().equals( Meta.MESSAGE_TYPE_TIMENSALES)) {
                Trade g_oTrade = CompanyTradeStore.getSharedInstance().getLastTrade(ro.getExchange(), ro.getSymbol(), false);
                if (g_oTrade != null) {
                    if (g_oTrade.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
                        xmlFile = g_oTrade.getXML();
                        System.out.println("===========================================================");
                        System.out.println("TimeNSales Data sent......");
                        System.out.println("Symbol " + g_oTrade.getSymbol());
                        ro.setUpdatedTime(g_oTrade.getSnapShotUpdatedTime());
                    }
                }

            }else if(ro.getMessageType().equals(Meta.MESSAGE_TYPE_OHLC)) {
                IntraDayOHLC g_oOhlc = OHLCStore.getInstance().getLastRecord(ro.getExchange(), ro.getSymbol());
                if (g_oOhlc != null) {
                    if (g_oOhlc.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
                        xmlFile = g_oOhlc.getXML();
                        System.out.println("===========================================================");
                        System.out.println("OHLC Data sent......");
                        System.out.println("Symbol " + g_oOhlc.getSymbol());
                        ro.setUpdatedTime(g_oOhlc.getSnapShotUpdatedTime());
                    }

                }

            }else{
                xmlFile=ro.getError();
                
            }
            out.write("\r\n".getBytes());
            out.write(xmlFile.getBytes());
        } catch (IOException e) {
            //response writting failed......
        }
    }*/

    public void responseWriter(RequestObject ro) {
        String xmlFile = "";
        try {
            if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_SYMBOL_DATA)) {
                Stock oStock = DataStore.getSharedInstance().getStockObject(ro.getExchange(), ro.getSymbol(), ro.getInstrumentType());
//                if(oStock != null){
                if (oStock.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
//                if (oStock.getLastTradeTime() > ro.getUpdatedTime()) {
                    xmlFile = oStock.getXML();
                    System.out.println("===========================================================");
                    System.out.println("Symbol Data sent......");
                    System.out.println("Symbol " + oStock.getSymbol());
                    ro.setUpdatedTime(oStock.getSnapShotUpdatedTime());
//                    ro.setUpdatedTime(oStock.getLastTradeTime());
//                    ro.setUpdatedTime(System.currentTimeMillis());
                }
            } else if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_MARKETDEPTH)) {
                xmlFile = DepthStore.getInstance().getXMLFor(SharedMethods.getKey(ro.getExchange(), ro.getSymbol(), ro.getInstrumentType()), ro.getDepthMode());
                System.out.println("===========================================================");
                System.out.println("MarketDepth Data sent......");
                System.out.println("Symbol " + ro.getSymbol());
            } else if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_TIMENSALES)) {
                Trade g_oTrade = CompanyTradeStore.getSharedInstance().getLastTrade(ro.getExchange(), ro.getSymbol(), ro.getInstrumentType(), false);
                if (g_oTrade != null) {
                    if (g_oTrade.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
                        xmlFile = g_oTrade.getXML();
                        System.out.println("===========================================================");
                        System.out.println("TimeNSales Data sent......");
                        System.out.println("Symbol " + g_oTrade.getSymbol());
                        ro.setUpdatedTime(g_oTrade.getSnapShotUpdatedTime());
                    }
                }

            } else if (ro.getMessageType().equals(Meta.MESSAGE_TYPE_OHLC)) {
                IntraDayOHLC g_oOhlc = OHLCStore.getInstance().getLastRecord(ro.getExchange(), ro.getSymbol(), ro.getInstrumentType());
                if (g_oOhlc != null) {
                    if (g_oOhlc.getSnapShotUpdatedTime() > ro.getUpdatedTime()) {
                        xmlFile = g_oOhlc.getXML();
                        System.out.println("===========================================================");
                        System.out.println("OHLC Data sent......");
                        System.out.println("Symbol " + g_oOhlc.getSymbol());
                        ro.setUpdatedTime(g_oOhlc.getSnapShotUpdatedTime());
                    }

                }

            } else {
                xmlFile = ro.getError();

            }
            write(xmlFile);
        } catch (Exception e) {
            //response writting failed......
        }
    }

    public synchronized void write(String data) {
        try {
            out.write("\r\n".getBytes());
            out.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
        }
    }

    public void twConnected() {
        isPriceConnected = true;
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Error>");
        buffer.append("<Body value=\"").append("Connected Again to Price Feed").append("\"/>");
        buffer.append("</Error>\n");
        write(buffer.toString());
    }

    public void twDisconnected() {
        isPriceConnected = false;
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Error>");
        buffer.append("<Body value=\"").append("Disconnected From Price Feed").append("\"/>");
        buffer.append("</Error>\n");
        write(buffer.toString());
    }
}
