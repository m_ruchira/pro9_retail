package com.mubasher.win32;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Apr 27, 2009
 * Time: 1:03:08 PM
 */

public interface Win32IdleTimeListener {
    public void Win32IdleEventOccured(Win32IdleTime.State state);
}
