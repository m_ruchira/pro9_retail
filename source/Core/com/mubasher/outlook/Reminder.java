package com.mubasher.outlook;

/*
 * Copyright (c) 2002-2007 TeamDev Ltd. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * The complete licence text can be found at
 * http://www.teamdev.com/comfyj/license.jsf
 */

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.jniwrapper.Int32;
import com.jniwrapper.win32.automation.IDispatch;
import com.jniwrapper.win32.automation.types.BStr;
import com.jniwrapper.win32.automation.types.Variant;
import com.jniwrapper.win32.automation.types.VariantBool;
import com.jniwrapper.win32.com.ComException;
import com.jniwrapper.win32.com.types.ClsCtx;
import com.jniwrapper.win32.com.types.Date;
import com.jniwrapper.win32.ole.OleFunctions;
import com.jniwrapper.win32.samples.planner.outlook.*;

import javax.swing.*;


/**
 * This sample requires generated stubs for COM type library:
 * Description: Microsoft Outlook 11.0 Object Library
 * ProgID:      Outlook.Application
 * GUID:        {00062FFF-0000-0000-C000-000000000046}
 * In the package: outlook
 * <p/>
 * You can generate stubs using the Code Generator application.
 */
public class Reminder {

    private static _NameSpace _mapiNS;


    private static void init() {
        OleFunctions.oleInitialize();
    }

    private static void shutdown() {
        OleFunctions.oleUninitialize();
    }


    private static void login() throws ComException {
        _Application application = Application.create(ClsCtx.LOCAL_SERVER);
        _mapiNS = application.getNamespace(new BStr("MAPI"));

        _mapiNS.logon(new Variant("Outlook"),
                new Variant(""),
                new Variant(false),
                new Variant(false));
    }

    public static boolean checkAvailability() {
        try {
            init();
            shutdown();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static MAPIFolder getCalendarFolder() throws ComException {
        MAPIFolder result = _mapiNS.getDefaultFolder(new OlDefaultFolders(OlDefaultFolders.olFolderCalendar));
        return result;
    }

    public static void createAppointment(String title, String location, String body,
                                         java.util.Date startTime, java.util.Date endTime, int minutesBefore) throws ComException {

        try {
            init();
            login();

//            MAPIFolder taskFolder = getTaskFolder();
//            createTask(taskFolder);

            MAPIFolder calendarFolder = getCalendarFolder();

            final Variant itemType = new Variant(OlItemType.olAppointmentItem);
            final IDispatch appointment = calendarFolder.getItems().add(itemType);
            _AppointmentItem newAppt = AppointmentItem.queryInterface(appointment);

            //sep up some properties
            newAppt.setSubject(new BStr(title));
            newAppt.setLocation(new BStr(location));
            newAppt.setBody(new BStr(body));

//            Calendar calendar = Calendar.getInstance();
            Date current = new Date(startTime);
//            calendar.add(Calendar.MINUTE, 10);
            Date tmEnd = new Date(endTime);
            newAppt.setStart(current);
            newAppt.setEnd(tmEnd);

            newAppt.setReminderMinutesBeforeStart(new Int32(minutesBefore));

            newAppt.setAllDayEvent(new VariantBool(false));

            newAppt.save();
        } catch (Exception e) {
            SharedMethods.showMessage(Language.getString("OUTLOOK_ERROR"), JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                shutdown();
            } catch (Exception e) {
            }
        }

    }

    private MAPIFolder getTaskFolder() throws ComException {
        MAPIFolder result = _mapiNS.getDefaultFolder(new OlDefaultFolders(OlDefaultFolders.olFolderTasks));
        return result;
    }
}