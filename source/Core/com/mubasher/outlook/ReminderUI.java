package com.mubasher.outlook;


import com.isi.csvr.ShowMessage;
import com.isi.csvr.calendar.CalCombo;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Uditha Nagahawatta
 */
public class ReminderUI extends JDialog implements ActionListener, NonNavigatable {
    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Uditha Nagahawatta
    private JPanel locationPanel;
    private JPanel northPanel;
    private JPanel centerPanel;
    private JPanel titlepanel;
    private JLabel lblLocation;
    private JLabel lblTitle;
    private JTextField txtTitle;
    private JPanel notesPanel;
    private JLabel lblNotes;
    private JScrollPane scrollPane1;
    //    private JEditorPane txtNotes;
    private TWTextArea txtNotes;
    private JPanel southPanel;
    private JLabel lblStartTime;
    private CalCombo txtStartDate;
    private TimePickerPanel txtStartTime;
    private TWTextField txtLocation;
    private JLabel lblEndTime;
    private CalCombo txtEndDate;
    private JLabel lblRemindMe;
    private JSpinner spnDays;
    private JSpinner spnHours;
    private JSpinner spnMinutes;
    private JLabel lblDays;
    private JLabel lblHours;
    private JLabel lblMinutes;
    private TimePickerPanel txtEndTime;
    private TWButton btnOk;
    private TWButton btnCancel;
    public ReminderUI(Frame owner) {
        super(owner);
        setModal(true);
        initComponents();
    }
    public ReminderUI(Dialog owner) {
        super(owner);
        setModal(true);
        initComponents();
    }

    public static void main(String[] args) {
        new ReminderUI((JFrame) null).show();
    }
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Uditha Nagahawatta
        locationPanel = new JPanel();
        northPanel = new JPanel();
        lblLocation = new JLabel();
        centerPanel = new JPanel();
        titlepanel = new JPanel();
        lblTitle = new JLabel();
        txtTitle = new JTextField();
        txtLocation = new TWTextField();
        notesPanel = new JPanel();
        lblNotes = new JLabel();
        txtNotes = new TWTextArea();
//        txtNotes.setContentType("text/html");
//        txtNotes.setForeground(Color.red);//Theme.getColor("INPUT_TEXT_FGCOLOR"));
//        txtNotes = new TWTextArea();
//        txtNotes.setWrapStyleWord(true);
//		 txtNotes.setDocument(new HTMLDocument());
        scrollPane1 = new JScrollPane(txtNotes);
        southPanel = new JPanel();
        lblStartTime = new JLabel();
        txtStartDate = new CalCombo();
        txtStartTime = new TimePickerPanel();
        lblEndTime = new JLabel();
        txtEndDate = new CalCombo();
        lblRemindMe = new JLabel();
        spnDays = new JSpinner();
        spnHours = new JSpinner();
        spnMinutes = new JSpinner();
        lblDays = new JLabel();
        lblHours = new JLabel();
        lblMinutes = new JLabel();
        txtEndTime = new TimePickerPanel();
        btnOk = new TWButton();
        btnCancel = new TWButton();

        //======== this ========
        setTitle(Language.getString("OUTLOOK_WINDOW_TITLE"));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== centerPanel ========
        {
            centerPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            centerPanel.setLayout(new BorderLayout());

            {
                northPanel.setLayout(new BorderLayout());

                //======== titlepanel ========
                {
                    titlepanel.setLayout(new BorderLayout());

                    //---- lblTitle ----
                    lblTitle.setText(Language.getString("OUTLOOK_TITLE"));
                    titlepanel.add(lblTitle, BorderLayout.NORTH);
                    txtTitle.setPreferredSize(new Dimension(1, 25));
                    titlepanel.add(txtTitle, BorderLayout.SOUTH);
                }
                northPanel.add(titlepanel, BorderLayout.NORTH);

                //======== locationPanel ========
                {
                    locationPanel.setLayout(new BorderLayout());

                    //---- lblLocation ----
                    lblLocation.setText(Language.getString("OUTLOOK_LOCATION"));
                    locationPanel.add(lblLocation, BorderLayout.NORTH);
                    txtLocation.setPreferredSize(new Dimension(1, 25));
                    locationPanel.add(txtLocation, BorderLayout.SOUTH);
                }
                northPanel.add(locationPanel, BorderLayout.SOUTH);
            }
            centerPanel.add(northPanel, BorderLayout.NORTH);

            //======== notesPanel ========
            {
                notesPanel.setLayout(new BorderLayout());

                //---- lblNotes ----
                lblNotes.setText(Language.getString("OUTLOOK_NOTE"));
                notesPanel.add(lblNotes, BorderLayout.NORTH);

                /*//======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(txtNotes);
                }*/
                notesPanel.add(scrollPane1, BorderLayout.CENTER);
            }
            centerPanel.add(notesPanel, BorderLayout.CENTER);
        }
        contentPane.add(centerPanel, BorderLayout.CENTER);

        //======== southPanel ========
        {
            southPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            southPanel.setLayout(new FlexNullLayout());

            //---- lblStartTime ----
            lblStartTime.setText(Language.getString("OUTLOOK_START_TIME"));
            southPanel.add(lblStartTime);
            lblStartTime.setBounds(new Rectangle(new Point(6, 0), lblStartTime.getPreferredSize()));
            southPanel.add(txtStartDate);
            txtStartDate.setBorder(UIManager.getBorder("TextField.border"));
//             txtStartDate.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
            txtStartDate.setBounds(6, 17, 135, 25);
            txtStartDate.setDate(new Date());
            southPanel.add(txtStartTime);
            txtStartTime.setBounds(150, 17, 70, 25);
            txtStartTime.setTime(0, 0);

            //---- lblEndTime ----
            lblEndTime.setText(Language.getString("OUTLOOK_END_TIME"));
            southPanel.add(lblEndTime);
            lblEndTime.setBounds(6, 40, 95, 25);
            southPanel.add(txtEndDate);
            txtEndDate.setBounds(6, 60, 135, 25);
            txtEndDate.setBorder(UIManager.getBorder("TextField.border"));
//             txtEndDate.setBorder(BorderFactory.createLineBorder(Theme.getBlackColor()));
            txtEndDate.setDate(new Date());
            southPanel.add(txtEndTime);
            txtEndTime.setBounds(150, 60, 70, 25);
            txtEndTime.setTime(23, 59);

            //---- lblRemindMe ----
            lblRemindMe.setText(Language.getString("OUTLOOK_REMIND_BEFORE"));
            southPanel.add(lblRemindMe);
            lblRemindMe.setBounds(new Rectangle(new Point(6, 87), lblRemindMe.getPreferredSize()));
            southPanel.add(spnDays);

//             JSpinner spinner = new JSpinner(new SpinnerNumberModel(0,0, 5, 1));
            spnDays.setModel(new SpinnerNumberModel(0, 0, 5, 1));
            JFormattedTextField field = ((JSpinner.DefaultEditor) spnDays.getEditor()).getTextField();
            ((DefaultFormatter) field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.
            spnDays.setBounds(6, 104, 51, 22);
            southPanel.add(spnHours);
            spnHours.setBounds(68, 104, 51, 22);
            spnHours.setModel(new SpinnerNumberModel(0, 0, 23, 1));
            field = ((JSpinner.DefaultEditor) spnHours.getEditor()).getTextField();
            ((DefaultFormatter) field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.
            southPanel.add(spnMinutes);
            spnMinutes.setBounds(130, 104, 51, 22);
            spnMinutes.setModel(new SpinnerNumberModel(0, 0, 59, 1));
            field = ((JSpinner.DefaultEditor) spnMinutes.getEditor()).getTextField();
            ((DefaultFormatter) field.getFormatter()).setAllowsInvalid(false);//disable invalid characters.

            //---- lblDays ----
            lblDays.setText(Language.getString("OUTLOOK_DAYS"));
            southPanel.add(lblDays);
            lblDays.setBounds(6, 129, 45, 14);

            //---- lblHours ----
            lblHours.setText(Language.getString("OUTLOOK_HOURS"));
            southPanel.add(lblHours);
            lblHours.setBounds(68, 129, 45, 14);

            //---- lblMinutes ----
            lblMinutes.setText(Language.getString("OUTLOOK_MINUTES"));
            southPanel.add(lblMinutes);
            lblMinutes.setBounds(130, 129, 45, 14);

            //---- btnOk ----
            btnOk.setText(Language.getString("OK"));
            btnOk.addActionListener(this);
            southPanel.add(btnOk);
            btnOk.setBounds(218, 117, 69, btnOk.getPreferredSize().height);

            //---- btnCancel ----
            btnCancel.setText(Language.getString("CANCEL"));
            btnCancel.addActionListener(this);
            southPanel.add(btnCancel);
            btnCancel.setBounds(new Rectangle(new Point(293, 117), btnCancel.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < southPanel.getComponentCount(); i++) {
                    Rectangle bounds = southPanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = southPanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                southPanel.setMinimumSize(preferredSize);
                southPanel.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(southPanel, BorderLayout.SOUTH);
        setSize(380, 450);
        GUISettings.applyOrientation(contentPane);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnOk)) {
            if (isValidInput()) {
                final Calendar start = Calendar.getInstance();
                start.setTime(txtStartDate.getDate().getTime());
                start.set(Calendar.HOUR_OF_DAY, Integer.parseInt(txtStartTime.getTxtHours().getText()));
                start.set(Calendar.MINUTE, Integer.parseInt(txtStartTime.getTxtMins().getText()));
                final Calendar end = Calendar.getInstance();
                end.setTime(txtEndDate.getDate().getTime());
                end.set(Calendar.HOUR_OF_DAY, Integer.parseInt(txtEndTime.getTxtHours().getText()));
                end.set(Calendar.MINUTE, Integer.parseInt(txtEndTime.getTxtMins().getText()));

                int minutesBefore = 0;
                try {
                    minutesBefore = (Integer) spnMinutes.getValue();
                    minutesBefore += (Integer) spnHours.getValue() * 60;
                    minutesBefore += (Integer) spnDays.getValue() * 60 * 24;
                } catch (Exception e1) {
                }

                final int fminutesBefore = minutesBefore;

                dispose();
                final String sContent = txtNotes.getText();

                new Thread() {
                    public void run() {
                        Reminder.createAppointment(txtTitle.getText(),
                                txtLocation.getText(), sContent, start.getTime(), end.getTime(), fminutesBefore);
                    }
                }.start();

            }
        } else {
            dispose();
        }
    }

    private boolean isValidInput() {
        if (txtTitle.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_OUTLOOK_TITLE"), "E");
            return false;
        }
        if (txtLocation.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_OUTLOOK_LOCATION"), "E");
            return false;
        }
//        try {
//            if ((txtNotes.getDocument()).getText(0, (txtNotes.getDocument()).getLength()).trim().equals("")) {
        if (txtNotes.getText().trim().equals("")) {
            new ShowMessage(Language.getString("INVALID_OUTLOOK_NOTE"), "E");
            return false;
        }
//        } catch (BadLocationException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
        return true;
    }

    public void showDiaog() {
        setVisible(true);
    }

    public void showDiaog(String title, String location, String notes) {
        txtTitle.setText(title);
        txtLocation.setText(location);
        if (notes != null && !notes.equals("") && !notes.equals("null")) {
            JEditorPane editorPane = new JEditorPane();
            editorPane.setContentType("text/html");
            editorPane.setText("<HTML>" + notes);
            try {
                txtNotes.setText(editorPane.getDocument().getText(0, (editorPane.getDocument()).getLength()));
            } catch (BadLocationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        setVisible(true);
    }
}
