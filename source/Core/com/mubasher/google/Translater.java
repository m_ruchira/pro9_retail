package com.mubasher.google;


/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Jul 11, 2009
 * Time: 10:09:13 PM
 */
public class Translater {
    public static String translate(String source, GoogleLanguage from, GoogleLanguage to) {
        try {
            String translatedText = GoogleTranslate.translate(source, from, to);
            return translatedText;
        } catch (Exception ex) {
//            ex.printStackTrace();
            return null;
        }
    }
}
