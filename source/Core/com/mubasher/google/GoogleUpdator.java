package com.mubasher.google;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.util.Decompress;
import com.jniwrapper.win32.ie.HeadlessBrowser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NavigationEventListener;

import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 30, 2007
 * Time: 9:38:09 AM
 * <p/>
 * Updates Google Analytics about various feature of Pro
 */
public class GoogleUpdator implements Runnable, NavigationEventListener {

    private static GoogleUpdator self;
    private String httpString;
    private SmartProperties httpParams;
    private String referenceUrl;
    //    private String server;
    private int frequency;
    private Hashtable<String, Boolean> tagTable;
    private boolean active = false;
    private WebBrowser browser;


    private GoogleUpdator() {
        tagTable = new Hashtable<String, Boolean>();
    }

    public static synchronized GoogleUpdator getInstance() {
        if (self == null) {
            self = new GoogleUpdator();
        }
        return self;
    }

    /**
     * Add a new feature for update
     *
     * @param tag fature ID
     */
    public synchronized void addTag(String tag) {
        if (!tagTable.containsKey(tag)) {
            tagTable.put(tag, false);
        }
    }

    /**
     * Start the Google Analytics service
     */
    public synchronized void activate() {
        try {
            if (!active) {
                try {
                    loadTemplate();
                    if ((httpString == null) || (referenceUrl == null)) return;

                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            System.out.println("----------Extracting cookie");
                            browser = new HeadlessBrowser();
                            browser.addNavigationListener(GoogleUpdator.this);
                            browser.navigate(referenceUrl);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Load the URL template and connection settings
     *
     * @throws Exception if failed
     */
    private void loadTemplate() throws Exception {
        try {
            // load the google gif file url
            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            out = decompress.setFiles(Settings.CONFIG_DATA_PATH + "/ggrequest.msf");
            decompress.decompress();

            httpString = new String(out.toByteArray());
            httpString = updateTemplate(httpString);

            // load google requst header params
            httpParams = new SmartProperties(":", true);
            httpParams.loadCompressed(Settings.CONFIG_DATA_PATH + "/ggparams.msf");

            Enumeration eKeys = httpParams.keys();
            while (eKeys.hasMoreElements()) {
                String key = (String) eKeys.nextElement();
                String param = (String) httpParams.get(key);
                param = updateTemplate(param);
                httpParams.put(key, param);
            }

            SmartProperties settings = new SmartProperties("=");
            settings.loadCompressed(Settings.CONFIG_DATA_PATH + "/ggconf.msf");
            referenceUrl = settings.getProperty("REF");
            referenceUrl = referenceUrl.replaceAll("\\[CAMPAIGN\\]", Settings.getBillingCode());
            frequency = Integer.parseInt(settings.getProperty("FREQUENCY"));
            if (frequency <= 0) {
                frequency = 30;
            }
            frequency = frequency * 1000; // convert to millies
            settings = null;
        } catch (Exception e) {
            httpString = null;
        }
    }

    private String updateTemplate(String str) {
        DisplayMode displayMode = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0].getDisplayMode();
        String screenSize = "" + displayMode.getWidth() + "x" + displayMode.getHeight();
        String bitdepth = "" + displayMode.getBitDepth();
        str = str.replaceFirst("\\[SCREEN\\]", screenSize);
        str = str.replaceFirst("\\[DEPTH\\]", bitdepth);
        str = str.replaceFirst("\\[VERSION\\]", Settings.TW_VERSION);
        str = str.replaceFirst("\\[OS\\]", System.getProperty("os.name"));
        str = str.replaceFirst("\\[OSVER\\]", System.getProperty("os.version"));
        str = str.replaceFirst("\\[FILE_ENCODING\\]", System.getProperty("file.encoding").toLowerCase().replaceFirst("cp", "windows-"));
        str = str.replaceFirst("\\[COUNTRY\\]", System.getProperty("user.country"));
        str = str.replaceAll("\\[LANG\\]", System.getProperty("user.language"));
        str = str.replaceAll("\\[PRO_LANG\\]", Language.getLanguageTag());

        return str;
    }

    public void run() {
        int failCount = 0;

        while (failCount < 3) {
            Enumeration<String> keys = tagTable.keys();
            while (keys.hasMoreElements()) {
                String key = keys.nextElement();
                if (!tagTable.get(key)) { // not updated. mist be a new value
                    boolean updated = update(key);
                    tagTable.put(key, updated); // updated
                    if (!updated) {
                        failCount++;
                        break;
                    } else {
                        failCount = 0;
                    }
                    System.out.println("-- Google  " + key);
                }
            }
            try {
                Thread.sleep(frequency);
            } catch (InterruptedException e) {
            }
        }
    }

    /**
     * Sends an update packet to Google site
     *
     * @param key
     * @return
     */
    private boolean update(String key) {
        try {
            String updatedURL;
            int rnd = 2086259697 + (int) (Math.random() * 10000);

            updatedURL = httpString.replaceAll("\\[RND\\]", "" + rnd);
            updatedURL = updatedURL.replaceAll("\\[PAGE\\]", key);

            /* if there is a proxy setting,  must use it to call the url */
            URLConnection connection;
//            if (BrowserManager.getInstance().isProxyAvailable()){ // browser proxy available
//                URL url = new URL(updatedURL);
//                connection = url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(BrowserManager.getInstance().getProxyIP(),
//                        BrowserManager.getInstance().getProxyPort())));
//                System.out.println("Google Connecting through Proxy " + BrowserManager.getInstance().getProxyIP() + " " +
//                        BrowserManager.getInstance().getProxyPort());
//            } else {
            URL url = new URL(updatedURL);
            connection = url.openConnection();
            System.out.println("Google no proxy");
//            }

            // set header params for the url
            Enumeration eKeys = httpParams.keys();
            while (eKeys.hasMoreElements()) {
                String tag = (String) eKeys.nextElement();
                String param = (String) httpParams.get(tag);
                connection.setRequestProperty(tag, param);
            }

            System.out.println(updatedURL);
            byte[] data = new byte[100];
            int i;
            while (true) {
                try {
                    i = connection.getInputStream().read(data);
                } catch (Exception e) {
                    break;
                }
                if (i < 0) {
                    break;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * sets the cookie for this session
     *
     * @param cookie
     */
    private void setCookie(String cookie) {
        try {
            if (cookie.startsWith("__utma")) {
                httpString = httpString.replaceAll("\\[COOKIE\\]", "" + cookie);
            } else {
                cookie = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /* ----- Navigation listener Methods -----*/
    public void documentCompleted(WebBrowser webBrowser, String s) {
    }

    public void downloadBegin() {
    }

    public void downloadCompleted() {

    }

    public void entireDocumentCompleted(WebBrowser webBrowser, String s) {
        try {
            String body = browser.getContent();
            if (body != null) {
                String cookie;
                if (body.startsWith("__utma")) {
                    cookie = body;
                } else {
                    cookie = browser.getDocument().getBody().getText();
                }
                System.out.println("[" + URLEncoder.encode(cookie, "utf-8") + "]");
                GoogleUpdator.getInstance().setCookie(URLEncoder.encode(cookie, "utf-8"));
                active = true;
                Thread thread = new Thread(this, "Google Updator");
                thread.start();
                System.out.println("------------Google activated");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void navigationCompleted(WebBrowser webBrowser, String s) {
    }

    public void progressChanged(int i, int i1) {
    }
}
