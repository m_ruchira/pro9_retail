package com.mubasher.loader;

import java.io.*;
import java.lang.reflect.Constructor;
import java.nio.channels.FileChannel;

/**
 *  Warning -   There should not be any references to com.isi... classes from this class
 *              Doing so may affect the update procedure due to locking of loaded classes 
 */

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 14, 2008
 * Time: 8:44:50 AM
 */
public class Loader {
    public static void main(String[] args) {
        boolean updateFailed = false;
        try {
            File file = new File("AUTOUPDATE/updatable");
            if (file.exists()) { // need to apply update?
                if (hasWriteAccess()) {
                    extract();
                    file = new File("profile");
                    if (file.exists()) { // profile data available?
                        moveProfile(file);
                    }
                    deleteUpdateData();
                } else {
                    updateFailed = true;
                }
            } else {
                file = new File("profile");
                if (file.exists()) { // profile data available?
                    moveProfile(file);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadApp(args, updateFailed);


    }

    private static void loadApp(String[] args, boolean updateFailed) {
        try {
            Class loaded = Class.forName("com.isi.csvr.Mubasher");
            Class[] stringClassArray = new Class[]{String[].class, boolean.class};
            Object[] stringObjectArray = new Object[]{args, updateFailed};
            Constructor constructor = loaded.getConstructor(stringClassArray);

            constructor.newInstance(stringObjectArray);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean hasWriteAccess() {
        try {
            File check = new File("./writeCheck");
            OutputStream out = new FileOutputStream(check);
            out.write(0);
            out.close();
            check.deleteOnExit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void extract() {
        // check if OS is Win98 (or Windows Me)
        String os = System.getProperty("os.name");
        boolean osIs98;
        try {
            osIs98 = false;
            if (os == null)
                osIs98 = false;
            else if ((os.indexOf("98") >= 0) || (os.toUpperCase().indexOf("WINDOWS ME") >= 0))
                osIs98 = true;
        } catch (Exception e) {
            osIs98 = false;
        }

        try {
            Process p;
            if (osIs98)
                p = Runtime.getRuntime().exec("command.com /C .\\autoupdate\\update.bat");
            else
                p = Runtime.getRuntime().exec("cmd /C .\\autoupdate\\update.bat");


            if (!osIs98) {
                InputStream in = p.getInputStream();
                byte[] data = new byte[1000];
                while (true) {
                    int i = in.read(data);
                    if (i > 0)
                        System.out.println(new String(data, 0, i));
                    else
                        break;
                }
            }
            p.waitFor();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static void moveProfile(File profile) throws Exception {
//		String userHome = System.getenv("USERPROFILE") + "\\Application Data";
        String userHome = System.getenv("APPDATA");
        String folderStruct = "";

        try {
//			BufferedReader in = new BufferedReader(new FileReader("fstructure.dat"));
            BufferedReader in = new BufferedReader(new FileReader("appdata.dat"));
            String str;
            while ((str = in.readLine()) != null) {
                folderStruct = folderStruct + str;
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            folderStruct = "";
        }
        userHome = userHome + folderStruct;


        File[] files = profile.listFiles();
        for (File file : files) {
            moveFiles(file.getAbsolutePath(), userHome + "/" + file.getName());
        }
        files = profile.listFiles();
        for (File file : files) {
            file.delete();
        }
        profile.delete();
    }

    private static void moveFiles(String strPath, String dstPath) throws IOException {
        File src = new File(strPath);
        File dest = new File(dstPath);

        if (src.isDirectory()) {
            dest.mkdirs();
            String list[] = src.list();
            for (String aList : list) {
                String dest1 = dest.getAbsolutePath() + "\\" + aList;
                String src1 = src.getAbsolutePath() + "\\" + aList;
                moveFiles(src1, dest1);
                new File(src1).delete();
            }
        } else {
            FileChannel sourceChannel = new FileInputStream(src).getChannel();
            FileChannel targetChannel = new FileOutputStream(dest).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(), targetChannel);
            sourceChannel.close();
            targetChannel.close();
            sourceChannel = null;
            targetChannel = null;
            src.delete();
        }
    }

    private static void deleteUpdateData() {
        File file1 = null;
        try {
            file1 = new File("AUTOUPDATE/updatable");
            file1.delete();
        } catch (Exception e) {
            if (file1 != null)
                file1.deleteOnExit();
        }

        File file2 = null;
        try {
            file2 = new File("AUTOUPDATE/update.msf");
            file2.delete();
        } catch (Exception e) {
            if (file2 != null)
                file2.deleteOnExit();
        }

        File file3 = null;
        try {
            file3 = new File("AUTOUPDATE/update.mdf");
            file3.delete();
        } catch (Exception e) {
            if (file3 != null)
                file3.deleteOnExit();
        }
    }
}
