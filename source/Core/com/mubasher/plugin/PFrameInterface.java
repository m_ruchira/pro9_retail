package com.mubasher.plugin;

import com.isi.csvr.table.Table;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Aug 21, 2007
 * Time: 3:14:04 PM
 */
public interface PFrameInterface {

    boolean isDetachable();

    void setDetachable(boolean detachableable);

    boolean isDetached();

    void setDetached(boolean detached);

    void setTable(Table table);

    void applySettings();

    void toggleTitleBar();

    boolean isTitleVisible();

    void setTitleVisible(boolean setVisible);

    int getWindowStyle();

    JTable getTable1();
}
